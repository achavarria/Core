﻿namespace Monibyte.Arquitectura.Poco.Generales
{
    public class PocIdioma
    {
        public int IdIdioma { get; set; }
        public string Idioma { get; set; }
        public int IdEstado { get; set; }
        public string Abreviatura { get; set; }
        public string CodCultura { get; set; }
    }
}

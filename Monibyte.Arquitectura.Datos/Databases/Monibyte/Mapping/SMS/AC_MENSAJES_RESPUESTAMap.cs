﻿using Monibyte.Arquitectura.Dominio.SMS;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.SMS
{
    public class AC_MENSAJES_RESPUESTAMap : EntityTypeConfiguration<AC_MENSAJES_RESPUESTA>
    {
        public AC_MENSAJES_RESPUESTAMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.NUMERO_RESPUESTA)
                .HasMaxLength(20);

            this.Property(t => t.DETALLE_MENSAJE)
                .HasMaxLength(300);

            this.Property(t => t.RESPUESTA_MENSAJE)
                .HasMaxLength(160);

            // Table & Column Mappings
            this.ToTable("AC_MENSAJES_RESPUESTA", "SMS");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.NUMERO_RESPUESTA).HasColumnName("NUMERO_RESPUESTA");
            this.Property(t => t.FECHA_HORA).HasColumnName("FECHA_HORA");
            this.Property(t => t.DETALLE_MENSAJE).HasColumnName("DETALLE_MENSAJE");
            this.Property(t => t.RESPUESTA_MENSAJE).HasColumnName("RESPUESTA_MENSAJE");
            this.Property(t => t.IND_ESTADO).HasColumnName("IND_ESTADO");
        }
    }
}

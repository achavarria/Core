﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Seguridad;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco.Configuracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public class AppConfiguracionGrps : AplicacionBase, IAppConfiguracionGrps
    {
        private IRepositorioTarjeta _repTarjeta;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<TC_vwTarjetasCuenta> _repVwTarjetasCuenta;
        private IRepositorioVwTarjetaUsuario _repVwTarjetaUsuario;
        private IRepositorioMnb<MB_GruposEmpresa> _repGruposEmpresa;
        private IAppConfiguracionApl _appConfigurarApl;
        private IAppConfiguracionCtrl _appConfigurarCtrl;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<MB_AdministradoresGrupo> _repAdministradoresGrupo;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _repVwEmpresasUsuario;
        private IRepositorioMnb<CL_EmpresasUsuario> _repEmpresasUsuario;
        private IAppAutorizacion _appAutorizacion;
        private IRepositorioMnb<TC_TarjetaGrupo> _repTarjetaGrupo;

        public AppConfiguracionGrps(
            IRepositorioTarjeta repTarjeta,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<TC_vwTarjetasCuenta> repVwTarjetasCuenta,
            IRepositorioVwTarjetaUsuario repVwTarjetaUsuario,
            IRepositorioMnb<MB_GruposEmpresa> repGruposEmpresa,
            IAppConfiguracionApl appConfigurarApl,
            IAppConfiguracionCtrl appConfigurarCtrl,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<MB_AdministradoresGrupo> repAdministradoresGrupo,
            IRepositorioMnb<CL_vwEmpresasUsuario> vwEmpresasUsuario,
            IRepositorioMnb<CL_EmpresasUsuario> repEmpresasUsuario,
            IAppAutorizacion appAutorizacion,
            IRepositorioMnb<TC_TarjetaGrupo> repTarjetaGrupo)
        {
            _repTarjeta = repTarjeta;
            _repCatalogo = repCatalogo;
            _repVwTarjetasCuenta = repVwTarjetasCuenta;
            _repVwTarjetaUsuario = repVwTarjetaUsuario;
            _repGruposEmpresa = repGruposEmpresa;
            _appConfigurarApl = appConfigurarApl;
            _appConfigurarCtrl = appConfigurarCtrl;
            _repPersona = repPersona;
            _repAdministradoresGrupo = repAdministradoresGrupo;
            _repVwEmpresasUsuario = vwEmpresasUsuario;
            _repEmpresasUsuario = repEmpresasUsuario;
            _appAutorizacion = appAutorizacion;
            _repTarjetaGrupo = repTarjetaGrupo;
        }

        public PocGruposEmpresa ObtenerGrupoEmpresa(int idGrupo)
        {
            var idEmpresa = InfoSesion.Info.IdEmpresa;
            var criteria = new Criteria<MB_GruposEmpresa>();
            criteria.And(m => m.IdGrupo == idGrupo);
            criteria.And(m => m.IdEmpresa == idEmpresa);
            var resultado = ObtenerGruposEmpresa(criteria);
            return resultado.FirstOrDefault();
        }

        public List<PocGruposEmpresa> ObtenerGruposEmpresa(int idEmpresa, int idTipoGrupo)
        {
            var criteria = new Criteria<MB_GruposEmpresa>();
            criteria.And(m => m.IdEmpresa == idEmpresa);
            criteria.And(x => x.IdTipoGrupo == idTipoGrupo);
            var resultado = ObtenerGruposEmpresa(criteria);
            return resultado.OrderBy(p => p.Descripcion).ToList();
        }

        public int RegistrarConfiguracionGrupo(PocGruposEmpresa entrada)
        {
            if (entrada != null)
            {
                entrada.LimiteCredito = entrada.LimiteCredito > 0 ?
                    entrada.LimiteCredito : null;
                if (entrada.IdGrupo <= 0)
                {
                    return IncluirNuevoGrupo(entrada);
                }
                else
                {
                    return ModificarGrupo(entrada);
                }
            }
            return -1;
        }

        public int? DuplicarConfiguracionGrupo(PocGrupoDuplica entrada)
        {
            var validation = new ValidationResponse();
            if (entrada == null)
            {
                validation.AddError("Err_0035");
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Error al duplicar grupo", validation: validation);
            }
            var grupoDuplica = _repGruposEmpresa.SelectById(entrada.IdGrupoEmpresa);
            var pocGrupoDuplica = grupoDuplica.ProyectarComo<PocGruposEmpresa>();
            pocGrupoDuplica.Descripcion = entrada.NombreNuevo;
            return IncluirNuevoGrupo(pocGrupoDuplica, true);
        }

        public void EliminarConfiguracionGrupo(int idGrupo, int idTipoGrupo)
        {
            var esNuevoGrupo = false;
            var validacion = new ValidationResponse();
            if (idGrupo <= 0) { validacion.AddError("Err_0025"); }

            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes al eliminar grupo", validation: validacion);
            }
            else
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    //Excluye todas las tarjetas del grupo y les aplica la configuracion sin restriccion
                    var tarjetasGrp = ObtenerTarjetasPorGrupo(idGrupo, idTipoGrupo);

                    var grupoPendiente = _appAutorizacion.ObtieneAutorizacionPendiente(
                        (int)EnumTipoAutorizacion.RestriccionesGrupo, idGrupo.ToString());
                    grupoPendiente = grupoPendiente == null ? _appAutorizacion.ObtieneAutorizacionPendiente(
                        (int)EnumTipoAutorizacion.GrupoOperativo, idGrupo.ToString()) : grupoPendiente;
                    if (grupoPendiente != null)
                    {
                        esNuevoGrupo = _appAutorizacion.EsNuevoGrupo(idGrupo.ToString());
                        if (!esNuevoGrupo)
                        {
                            var actualAut = _appAutorizacion.ObtieneDetalleAutorizacion((int)EnumTipoAutorizacion.RestriccionesGrupo,
                                idGrupo.ToString());
                            if (actualAut == null)
                            {
                                actualAut = _appAutorizacion.ObtieneDetalleAutorizacion((int)EnumTipoAutorizacion.GrupoOperativo,
                                                                idGrupo.ToString());
                            }
                            if (actualAut != null && actualAut.Contains("ViejoNombre"))
                            {
                                var dictionary = actualAut.FromJson<IDictionary<string, object>>();
                                var nombre = dictionary["ViejoNombre"].ToString();
                                if (actualAut.Contains("ViejoMonto"))
                                {
                                    var limite = (double)dictionary["ViejoMonto"];
                                    var limteR = limite.ToString("R");
                                    var enDecimal = decimal.Parse(limteR);
                                    _repGruposEmpresa.UpdateOn(x => x.IdGrupo == idGrupo,
                                        z => new MB_GruposEmpresa
                                        {
                                            Descripcion = nombre,
                                            LimiteCredito = enDecimal
                                        });
                                }
                                else
                                {
                                    _repGruposEmpresa.UpdateOn(x => x.IdGrupo == idGrupo,
                                        z => new MB_GruposEmpresa
                                        {
                                            Descripcion = nombre
                                        });
                                }
                            }
                        }
                    }

                    if (Context.IsEditor)
                    {
                        if (esNuevoGrupo || grupoPendiente != null)
                        {
                            //Procede a eliminar el grupo
                            _appAutorizacion.AnularAutorizacionesGrupo(idGrupo.ToString());
                            if (esNuevoGrupo)
                            {
                                _repGruposEmpresa.DeleteOn(p => p.IdGrupo == idGrupo);
                            }
                        }
                        else if (!esNuevoGrupo && grupoPendiente == null)
                        {
                            _appAutorizacion.PrepararAutorizacion((int)EnumTipoAutorizacion.EliminarGrupo,
                                idGrupo.ToString(), idGrupo.ToJson());
                        }
                    }
                    else if (grupoPendiente != null && esNuevoGrupo)
                    {
                        //Procede a eliminar el grupo
                        _repGruposEmpresa.DeleteOn(p => p.IdGrupo == idGrupo);
                        _appAutorizacion.AprobarAutorizacion((int)EnumTipoAutorizacion.EliminarGrupo,
                            idGrupo.ToString());
                        //Procede a eliminar Autorizaciones pendientes del grupo
                        _appAutorizacion.RechazarAutorizacion((int)EnumTipoAutorizacion.RestriccionesGrupo,
                            idGrupo.ToString(), null);
                        _appAutorizacion.RechazarAutorizacion((int)EnumTipoAutorizacion.GrupoOperativo,
                            idGrupo.ToString(), null);
                    }
                    else
                    {
                        if (grupoPendiente == null || esNuevoGrupo)
                        {
                            foreach (var item in tarjetasGrp)
                            {
                                ExcluirTarjetaDeGrupo(item.IdTarjeta, idGrupo, idTipoGrupo);
                            }

                            _repTarjeta.UnidadTbjo.Save();
                            //Procede a eliminar los administradores del grupo
                            var admins = _repAdministradoresGrupo.Table.Where(m => m.IdGrupoEmpresa == idGrupo).ToList();
                            _repAdministradoresGrupo.DeleteOn(m => m.IdGrupoEmpresa == idGrupo);
                            var catalog = _repCatalogo.SelectById((int)EnumTipoUsuario.Adicional);
                            foreach (var item in admins)
                            {
                                var count = _repAdministradoresGrupo.Table.Count(m => m.IdUsuario == item.IdUsuario);
                                if (count == 0)
                                {
                                    _repEmpresasUsuario.UpdateOn(m => item.IdUsuario == m.IdUsuario
                                        && m.IdEmpresa == InfoSesion.Info.IdEmpresa,
                                        up => new CL_EmpresasUsuario
                                        {
                                            IdTipoUsuario = catalog.IdCatalogo,
                                            IdRole = Int32.Parse(catalog.Referencia2)
                                        });
                                }
                            }
                            //Procede a eliminar el grupo
                            _repGruposEmpresa.DeleteOn(p => p.IdGrupo == idGrupo);
                        }
                        InfoSesion.IncluirSesion("APLICAAUTORIZACION", !Context.IsEditor);
                        var idEliminar = _appAutorizacion.AprobarAutorizacion((int)EnumTipoAutorizacion.EliminarGrupo,
                            idGrupo.ToString());
                        ////Procede a eliminar Autorizaciones pendientes del grupo
                        _appAutorizacion.EliminarPendientes(idGrupo.ToString(), null);
                        _repGruposEmpresa.UnidadTbjo.Save();
                        //Procede a eliminar la configuracion del grupo
                        if (grupoPendiente == null || esNuevoGrupo)
                        {
                            _appConfigurarApl.EliminarPlantilla(idGrupo);
                        }
                    }
                    ts.Complete();
                }
            }
        }

        #region grupos operativos

        public void GuardarGrupoOperativo(PocGruposEmpresa entrada)
        {
            var nuevoGrupo = new MB_GruposEmpresa();
            nuevoGrupo.Descripcion = entrada.Descripcion;
            nuevoGrupo.IdGrupo = entrada.IdGrupo > 0 ? entrada.IdGrupo :
                (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdGrupo");
            nuevoGrupo.IdTipoGrupo = (int)EnumTipoGrupo.Operativo;
            nuevoGrupo.IdEmpresa = InfoSesion.Info.IdEmpresa;
            nuevoGrupo.IdEstado = (int)EnumIdEstado.Activo;
            nuevoGrupo.FecInclusionAud = DateTime.Now;
            nuevoGrupo.IdUsuarioIncluyeAud = InfoSesion.Info.IdUsuario;


            if (Context.IsEditor)
            {
                var detalle = "";
                if (entrada.IdGrupo == 0)
                {
                    detalle = new { IdGrupo = nuevoGrupo.IdGrupo, EsNuevo = true }.ToJson();
                }
                else
                {
                    var previo = _repGruposEmpresa.SelectById(entrada.IdGrupo);
                    detalle = new { IdGrupo = nuevoGrupo.IdGrupo, ViejoNombre = previo.Descripcion }.ToJson();
                }
                var cond = _appAutorizacion.PrepararAutorizacion((int)EnumTipoAutorizacion.GrupoOperativo,
                    nuevoGrupo.IdGrupo.ToString(), detalle);
            }
            else
            {
                _appAutorizacion.AprobarAutorizacion((int)EnumTipoAutorizacion.GrupoOperativo,
                        entrada.IdGrupo.ToString());
            }
            if (entrada.IdGrupo > 0)
            {
                _repGruposEmpresa.UpdateOn(x => x.IdGrupo == entrada.IdGrupo, z => new MB_GruposEmpresa
                {
                    Descripcion = entrada.Descripcion
                });
            }
            else
            {
                _repGruposEmpresa.Insert(nuevoGrupo);
            }
            _repGruposEmpresa.UnidadTbjo.Save();
        }

        public PocGruposEmpresa AutorizacionGrupoPendiente(int idGrupo)
        {
            var autorizacion = _appAutorizacion.ObtenerAutorizacion((int)EnumTipoAutorizacion.GrupoOperativo,
                idGrupo.ToString());
            if (autorizacion != null)
            {
                var dataAut = autorizacion.DetalleAutorizacion.FromJson<PocGruposEmpresa>();
                dataAut.Observaciones = autorizacion.Observaciones;

                return dataAut;
            }
            return null;
        }

        #endregion

        #region definicion de administradores de grupo

        public List<PocUsuarioEmpresa> ObtenerUsuariosGrupo(int idEmpresa, int idGrupoEmpresa)
        {
            List<PocUsuarioEmpresa> usuariosAutorizacion = null;
            if (InfoSesion.ObtenerSesion<bool?>("BUSCARAUTORIZACION") == true)
            {
                var detAutorizacion = _appAutorizacion.ObtieneDetalleAutorizacion(
                    (int)EnumTipoAutorizacion.AdministradoresGrupo, idGrupoEmpresa.ToString());
                if (!string.IsNullOrEmpty(detAutorizacion))
                {
                    usuariosAutorizacion = detAutorizacion.FromJson<List<PocUsuarioEmpresa>>();
                }
            }

            var criteriaUsuario = new Criteria<CL_vwEmpresasUsuario>();
            criteriaUsuario.And(usr => usr.IdEmpresa == idEmpresa);
            criteriaUsuario.And(usr => usr.IdTipoUsuario == (int)EnumTipoUsuario.Adicional ||
                    usr.IdTipoUsuario == (int)EnumTipoUsuario.SubAdministrador);

            var usuariosAdministradores = _repVwEmpresasUsuario.SelectBy(criteriaUsuario)
                .Join(_repPersona.Table,
                    usr => usr.IdPersona,
                    per => per.IdPersona,
                    (usr, per) => new { usr, per })
                .GroupJoin(_repAdministradoresGrupo.Table,
                    tmp => new { tmp.usr.IdUsuario, IdGrupoEmpresa = idGrupoEmpresa },
                    adm => new { adm.IdUsuario, adm.IdGrupoEmpresa },
                    (tmp, adm) => new { tmp.usr, tmp.per, adm = adm.FirstOrDefault() })
                .Select(sel => new PocUsuarioEmpresa
                {
                    IdEmpresa = sel.usr.IdEmpresa,
                    IdUsuario = sel.usr.IdUsuario,
                    IdEstado = sel.usr.IdEstado,
                    CodUsuario = sel.usr.CodUsuario,
                    Nombre = sel.per.NombreCompleto,
                    Descripcion = sel.usr.CodUsuario + "-" + sel.usr.AliasUsuario ?? sel.usr.CodUsuario,
                    EsAutorizacion = false,
                    EsAdministrador = sel.adm != null ? true : false,
                    Autoriza = sel.adm != null ? sel.adm.IdAutoriza == (int)EnumIdSiNo.Si : false,
                    SoloConsulta = sel.adm != null ? sel.adm.IdSoloConsulta == (int)EnumIdSiNo.Si : false
                }).ToList();

            if (usuariosAutorizacion != null)
            {
                usuariosAdministradores.Update(up =>
                {
                    var aut = usuariosAutorizacion.FirstOrDefault(c => c.IdUsuario == up.IdUsuario);
                    if (aut != null)
                    {
                        up.EsAutorizacion = true;
                        up.EsAdministrador = aut.EsAdministrador;
                        up.Autoriza = aut.Autoriza;
                        up.SoloConsulta = aut.SoloConsulta;
                    }
                });
            }

            usuariosAdministradores = usuariosAdministradores
                .Where(x => x.IdEstado != (int)EnumEstadoUsuario.Bloqueado &&
                    x.IdEstado != (int)EnumEstadoUsuario.Inactivo ||
                    x.EsAdministrador == true).ToList();

            return usuariosAdministradores;
        }

        public void DefinirAdministradores(int idGrupo, int idTipoGrupo, List<PocUsuarioEmpresa> entrada)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var validacion = new ValidationResponse();
                    if (idGrupo <= 0) { validacion.AddError("Err_0025"); }
                    if (!validacion.IsValid)
                    {
                        throw new CoreException("Datos faltantes al definir adm de grupo", validation: validacion);
                    }
                    else
                    {
                        if (Context.IsEditor)
                        {
                            _appAutorizacion.PrepararAutorizacion(
                                (int)EnumTipoAutorizacion.AdministradoresGrupo,
                                idGrupo.ToString(), entrada.ToJson());
                        }
                        else
                        {
                            var catalog = _repCatalogo.SelectById((int)EnumTipoUsuario.Adicional);
                            foreach (PocUsuarioEmpresa item in entrada.Where(w => !w.EsAdministrador && w.EsAutorizacion))
                            {
                                _repAdministradoresGrupo.DeleteOn(m =>
                                    m.IdGrupoEmpresa == idGrupo && m.IdUsuario == item.IdUsuario);
                                //Se cambia el tipo y rol del usuario solo si no tiene otros grupos asociados
                                var count = _repAdministradoresGrupo.Table.Count(m => m.IdUsuario == item.IdUsuario);
                                if (count == 0)
                                {
                                    _repEmpresasUsuario.UpdateOn(m => item.IdUsuario == m.IdUsuario
                                        && m.IdEmpresa == InfoSesion.Info.IdEmpresa,
                                        up => new CL_EmpresasUsuario
                                        {
                                            IdTipoUsuario = catalog.IdCatalogo,
                                            IdRole = Int32.Parse(catalog.Referencia2)
                                        });
                                }
                            }

                            catalog = _repCatalogo.SelectById((int)EnumTipoUsuario.SubAdministrador);
                            foreach (PocUsuarioEmpresa item in entrada.Where(w => w.EsAdministrador && w.EsAutorizacion))
                            {
                                if (idTipoGrupo == (int)EnumTipoGrupo.PanelControl)
                                {
                                    _repEmpresasUsuario.UpdateOn(m =>
                                        item.IdUsuario == m.IdUsuario &&
                                        m.IdEmpresa == InfoSesion.Info.IdEmpresa,
                                        up => new CL_EmpresasUsuario
                                        {
                                            IdTipoUsuario = catalog.IdCatalogo,
                                            IdRole = Int32.Parse(catalog.Referencia2)
                                        });
                                }
                                var existente = _repAdministradoresGrupo.Table.FirstOrDefault(m =>
                                    m.IdGrupoEmpresa == idGrupo && m.IdUsuario == item.IdUsuario);
                                if (existente != null)
                                {
                                    existente.IdAutoriza = item.Autoriza ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
                                    existente.IdSoloConsulta = item.SoloConsulta ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
                                    _repAdministradoresGrupo.Update(existente);
                                }
                                else
                                {
                                    var idSeq = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdAdminGrupo");
                                    _repAdministradoresGrupo.Insert(new MB_AdministradoresGrupo
                                    {
                                        IdAdminGrupo = idSeq,
                                        IdGrupoEmpresa = idGrupo,
                                        IdUsuario = item.IdUsuario,
                                        IdEstado = (int)EnumIdEstado.Activo,
                                        IdAutoriza = item.Autoriza ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No,
                                        IdSoloConsulta = item.SoloConsulta ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No
                                    });
                                }
                            }
                            _appAutorizacion.AprobarAutorizacion(
                                (int)EnumTipoAutorizacion.AdministradoresGrupo, idGrupo.ToString());
                            _repAdministradoresGrupo.UnidadTbjo.Save();
                            _repEmpresasUsuario.UnidadTbjo.Save();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                ts.Complete();
            }
        }

        public bool EsAutorizadorGrupo(int idUsuario, int idGrupo)
        {
            var user = _repAdministradoresGrupo.Table.FirstOrDefault(
                x => x.IdUsuario == idUsuario && x.IdGrupoEmpresa == idGrupo);
            return user != null && user.IdAutoriza == (int)EnumIdSiNo.Si;
        }

        #endregion

        #region definicion de miembros de grupo

        public List<PocTarjetaEmpresa> ObtenerTarjetasPorGrupo(int? idGrupoEmpresa,
            int idTipoGrupo, bool soloAgrupadas = true)
        {
            var operativa = idTipoGrupo == (int)EnumTipoGrupo.Operativo;

            List<PocTarjetaEmpresa> tarjetasAutorizacion = null;
            if (InfoSesion.ObtenerSesion<bool?>("BUSCARAUTORIZACION") == true)
            {
                var detAutorizacion = _appAutorizacion.ObtieneDetalleAutorizacion(
                    (int)EnumTipoAutorizacion.AgruparTarjetas, idGrupoEmpresa.ToString(),
                    idTipoGrupo);
                if (!string.IsNullOrEmpty(detAutorizacion))
                {
                    tarjetasAutorizacion = detAutorizacion.FromJson<List<PocTarjetaEmpresa>>();
                }
            }
            var criteria = new Criteria<TC_vwTarjetaUsuario>();
            if (idGrupoEmpresa.HasValue && soloAgrupadas)
            {
                criteria.And(m => m.IdGrupoEmpresa == idGrupoEmpresa);
            }
            else if (!idGrupoEmpresa.HasValue && soloAgrupadas)
            {
                criteria.And(m => m.IdGrupoEmpresa.HasValue);
            }
            var tarjetasGrupo = _repVwTarjetaUsuario
                .TarjetasPermitidas(criterias: criteria, incluirOperativas: operativa,
                    ignorarPanelCtrl: operativa, ignorarIdGrupo: false)
                .GroupJoin(_repGruposEmpresa.Table,
                    tar => tar.IdGrupoEmpresa,
                    grp => grp.IdGrupo,
                    (tar, grp) => new { tar, grp = grp.DefaultIfEmpty() })
                .SelectMany(m => m.grp, (a, grp) => new { a.tar, grp })
                  .Select(m => new PocTarjetaEmpresa
                     {
                         IdEmpresa = m.tar.IdEmpresa,
                         IdCuenta = m.tar.IdCuenta,
                         IdTarjeta = m.tar.IdTarjeta,
                         NumTarjeta = m.tar.NumTarjeta,
                         NombreImpreso = m.tar.NombreImpreso,
                         Agrupada = m.grp != null ? m.grp.IdTipoGrupo == idTipoGrupo && idGrupoEmpresa.HasValue ?
                            (m.tar.IdGrupoEmpresa.HasValue && m.tar.IdGrupoEmpresa == idGrupoEmpresa) : false : false,
                         IdGrupoEmpresa = m.grp != null ? m.grp.IdTipoGrupo == idTipoGrupo &&
                             (m.tar.IdGrupoEmpresa.HasValue) ?
                              m.grp.IdGrupo as int? : null : null,
                         DescripcionGrupo = m.grp != null ? m.grp.Descripcion : string.Empty
                     }).OrderBy(p => p.NombreImpreso).ToList();

            if (tarjetasAutorizacion != null)
            {
                tarjetasGrupo.Update(up =>
                {
                    var aut = tarjetasAutorizacion.FirstOrDefault(c => c.IdTarjeta == up.IdTarjeta);
                    if (aut != null && idTipoGrupo == (int)EnumTipoGrupo.PanelControl)
                    {
                        up.DescripcionGrupo = aut.DescripcionGrupo;
                        up.IdGrupoEmpresa = aut.IdGrupoEmpresa;
                        up.EsAutorizacion = true;
                        up.Agrupada = aut.Agrupada;
                    }
                    if (aut != null &&
                       idTipoGrupo == (int)EnumTipoGrupo.Operativo)
                    {
                        up.DescripcionGrupo = aut.DescripcionGrupo;
                        up.IdGrupoEmpresa = aut.IdGrupoEmpresa;
                        up.EsAutorizacion = aut.IdGrupoEmpresa == idGrupoEmpresa ? aut.EsAutorizacion : false;
                        up.Agrupada = aut.Agrupada;
                    }
                });
            }

            //(tarjetasAutorizacion != null && idTipoGrupo == (int)EnumTipoGrupo.Operativo)

            if (idGrupoEmpresa.HasValue && idTipoGrupo == (int)EnumTipoGrupo.PanelControl)
            {
                tarjetasGrupo = tarjetasGrupo.Where(m => m.IdGrupoEmpresa == idGrupoEmpresa ||
                    !m.IdGrupoEmpresa.HasValue).ToList();
            }
            tarjetasGrupo = tarjetasGrupo
                .OrderByDescending(t => t.Agrupada)
                .DistinctBy(t => t.IdTarjeta)
                .OrderBy(t => t.NumTarjeta)
               .ToList();
            return tarjetasGrupo;
        }

        public PocCondicion AgruparTarjetas(List<PocTarjetaEmpresa> entrada, int idGrupoEmpresa, int idTipoGrupo)
        {
            PocCondicion resultado = null;
            using (TransactionScope ts = new TransactionScope())
            {
                var validacion = new ValidationResponse();
                if (idGrupoEmpresa <= 0) { validacion.AddError("Err_0025"); }
                if (!validacion.IsValid)
                {
                    throw new CoreException("Datos faltantes al agrupar tarjetas a grupo", validation: validacion);
                }
                else
                {

                    var grupo = _repGruposEmpresa.SelectById(idGrupoEmpresa);
                    var limiteGrupo = grupo.LimiteCredito;
                    ValidarLimiteGrupo(idGrupoEmpresa, limiteGrupo, entrada);
                    if (Context.IsEditor)
                    {
                        _appAutorizacion.PrepararAutorizacion((int)EnumTipoAutorizacion.AgruparTarjetas,
                            idGrupoEmpresa.ToString(), entrada.ToJson());
                    }
                    else
                    {
                        _appAutorizacion.AprobarAutorizacion((int)EnumTipoAutorizacion.AgruparTarjetas,
                            idGrupoEmpresa.ToString());
                        foreach (PocTarjetaEmpresa item in entrada.Where(w => !w.Agrupada))
                        {
                            ExcluirTarjetaDeGrupo(item.IdTarjeta, idGrupoEmpresa, idTipoGrupo);
                        }
                        var tarjetas = entrada.Where(w => w.Agrupada);
                        if (tarjetas.Count() > 0)
                        {
                            foreach (PocTarjetaEmpresa item in tarjetas)
                            {
                                if (idTipoGrupo == (int)EnumTipoGrupo.Operativo)
                                {
                                    var exist = _repTarjetaGrupo.Table.Any(x =>
                                        x.IdTarjeta == item.IdTarjeta &&
                                        x.IdGrupo == idGrupoEmpresa);
                                    if (!exist)
                                    {
                                        _repTarjetaGrupo.Insert(new TC_TarjetaGrupo
                                        {
                                            IdTarjeta = item.IdTarjeta,
                                            IdGrupo = idGrupoEmpresa
                                        });
                                    }
                                }
                                else
                                {
                                    var exist = _repTarjeta.Table.Any(x =>
                                        x.IdTarjeta == item.IdTarjeta &&
                                        x.IdGrupoEmpresa == idGrupoEmpresa);
                                    if (!exist)
                                    {
                                        _repTarjeta.UpdateOn(x =>
                                            x.IdTarjeta == item.IdTarjeta, z => new TC_Tarjeta
                                                {
                                                    IdGrupoEmpresa = idGrupoEmpresa
                                                });
                                    }
                                }
                            }
                            if (idTipoGrupo == (int)EnumTipoGrupo.PanelControl)
                            {
                                resultado = AplicarConfiguracionGrupo(idGrupoEmpresa, tarjetas.ToArray());
                            }
                        }
                    }
                    _repTarjetaGrupo.UnidadTbjo.Save();
                }
                ts.Complete();
            }
            return resultado;
        }

        public void DesagruparTarjeta(PocAgrupacionTarjetasItem entrada)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var validacion = new ValidationResponse();
                    if (entrada.IdGrupoEmpresa <= 0) { validacion.AddError("Err_0025"); }
                    if (!validacion.IsValid)
                    {
                        throw new CoreException("Datos faltantes al Desagrupar tarjetas a grupo", validation: validacion);
                    }
                    else
                    {
                        if (Context.IsEditor)
                        {
                            _appAutorizacion.PrepararAutorizacion((int)EnumTipoAutorizacion.AgruparTarjetas,
                                                                    entrada.IdGrupoEmpresa.ToString(),
                                                                    entrada.ToJson());
                        }
                        else
                        {
                            var grupoOperativo = _repTarjetaGrupo.Table
                                .Any(x => x.IdGrupo == entrada.IdGrupoEmpresa &&
                                     x.IdTarjeta == entrada.IdTarjeta);
                            if (grupoOperativo)
                            {
                                var usuariosGrupo = entrada.ProyectarComo<TC_TarjetaGrupo>();
                                _repTarjetaGrupo.Delete(usuariosGrupo);
                            }
                            else
                            {
                                var usuariosGrupo = entrada.ProyectarComo<TC_Tarjeta>();
                                usuariosGrupo.IdGrupoEmpresa = null;
                                _repTarjeta.Update(usuariosGrupo, usr => usr.IdGrupoEmpresa);
                            }
                        }
                        _repTarjeta.UnidadTbjo.Save();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                ts.Complete();
            }
        }

        private PocCondicion AplicarConfiguracionGrupo(int? idPlantilla, PocTarjetaEmpresa[] tarjetasArray)
        {
            var resultado = new PocCondicion();
            var validation = new ValidationResponse();
            if (idPlantilla == null || idPlantilla <= 0)
            {
                validation.AddError("Err_0036");
            }
            if (tarjetasArray == null || !tarjetasArray.Any())
            {
                validation.AddError("Err_0037");
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Error al asociar plantilla", validation: validation);
            }
            using (var ts = new TransactionScope())
            {
                resultado = _appConfigurarApl.ObtenerCondicion(idPlantilla.Value, true);
                foreach (var item in tarjetasArray)
                {
                    var copiaPlantilla = resultado.GetClone();
                    copiaPlantilla.IdEmpresa = item.IdEmpresa;
                    copiaPlantilla.NumTarjeta = item.NumTarjeta;
                    var condicionPrevia = _appConfigurarCtrl.ObtenerCondicion(item.NumTarjeta);
                    copiaPlantilla.IdCondicion = condicionPrevia != null ? condicionPrevia.IdCondicion : 0;
                    InfoSesion.IncluirSesion("CONDICIONPREVIA", condicionPrevia);
                    _appConfigurarCtrl.EstablecerCondicion(copiaPlantilla, true);
                }
                ts.Complete();
            }
            return resultado;
        }

        #endregion

        #region Metodos privados

        private IQueryable<PocGruposEmpresa> ObtenerGruposEmpresa(Criteria<MB_GruposEmpresa> criteria)
        {
            var esSubAdmin = InfoSesion.Info.IdTipoUsuario ==
                (int)EnumTipoUsuario.SubAdministrador;
            var queryable = _repGruposEmpresa.SelectBy(criteria)
                .Join(_repCatalogo.List("LACTINACTIVA"),
                    ge => ge.IdEstado,
                    es => es.IdCatalogo,
                    (ge, es) => new { ge, es })
                .GroupJoin(_repAdministradoresGrupo.Table,
                    tmp => new { tmp.ge.IdGrupo, InfoSesion.Info.IdUsuario },
                    adm => new { IdGrupo = adm.IdGrupoEmpresa, adm.IdUsuario },
                    (tmp, adm) => new { tmp.ge, tmp.es, adm = adm.FirstOrDefault() })
                .Where(m => (!esSubAdmin || (esSubAdmin && m.adm != null)))
                .Select(m => new PocGruposEmpresa
                {
                    IdGrupo = m.ge.IdGrupo,
                    IdEstado = m.ge.IdEstado,
                    IdTipoGrupo = m.ge.IdTipoGrupo,
                    Descripcion = m.ge.Descripcion,
                    DescripcionEstado = m.es.Descripcion,
                    LimiteCredito = m.ge.LimiteCredito,
                    Autoriza = m.adm != null ? m.adm.IdAutoriza == (int)EnumIdSiNo.Si : !Context.IsEditor,
                    SoloConsulta = m.adm != null ? m.adm.IdSoloConsulta == (int)EnumIdSiNo.Si : false,
                }).AsQueryable();
            return queryable;
        }

        private int IncluirNuevoGrupo(PocGruposEmpresa entrada, bool duplicar = false)
        {
            var validacion = new ValidationResponse();
            if (string.IsNullOrEmpty(entrada.Descripcion)) { validacion.AddError("Err_0020"); }
            if (entrada.IdEmpresa <= 0) { validacion.AddError("Err_0024"); }
            if (entrada.Configuracion == null && !duplicar) { validacion.AddError("Err_0073"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes al registrar grupo", validation: validacion);
            }
            else
            {
                var nuevoGrupo = entrada.ProyectarComo<MB_GruposEmpresa>();
                nuevoGrupo.IdEmpresa = entrada.IdEmpresa;
                using (TransactionScope ts = new TransactionScope())
                {
                    nuevoGrupo.IdGrupo = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdGrupo");

                    if (nuevoGrupo.IdTipoGrupo == (int)EnumTipoGrupo.PanelControl)
                    {
                        if (!duplicar)
                        {
                            //Establece la plantilla correspondiente al grupo
                            entrada.IdGrupo = nuevoGrupo.IdGrupo;
                            entrada.Configuracion.IdCondicion = entrada.IdGrupo;
                            var idPlantilla = _appConfigurarApl.EstablecerCondicion(entrada.Configuracion, esNuevoGrupo: true);
                        }
                        else
                        {
                            //Duplica la plantilla indicada
                            var condicionDuplica = new PocCondicionDuplica
                            {
                                IdCondicion = entrada.IdGrupo,
                                NombreNuevo = entrada.Descripcion
                            };
                            var idPlantilla = _appConfigurarApl.DuplicarPlantilla(condicionDuplica, nuevoGrupo.IdGrupo);
                        }
                    }
                    _repGruposEmpresa.Insert(nuevoGrupo);

                    _repGruposEmpresa.UnidadTbjo.Save();
                    ts.Complete();
                    return nuevoGrupo.IdGrupo;
                }
            }
        }

        private int ModificarGrupo(PocGruposEmpresa entrada)
        {
            var entradaGrupo = entrada.ProyectarComo<MB_GruposEmpresa>();
            using (TransactionScope ts = new TransactionScope())
            {
                var validacion = new ValidationResponse();
                if (entrada.Descripcion.Length <= 0) { validacion.AddError("Err_0020"); }
                if (entrada.IdEmpresa <= 0) { validacion.AddError("Err_0024"); }

                if (!validacion.IsValid)
                {
                    throw new CoreException("Datos faltantes al modificar grupo", validation: validacion);
                }
                else
                {
                    ValidarLimiteGrupo(entrada.IdGrupo, entrada.LimiteCredito, null);
                    _repGruposEmpresa.Update(entradaGrupo, m => m.Descripcion, m => m.LimiteCredito);
                    _appConfigurarApl.EstablecerCondicion(entrada.Configuracion);
                    _repGruposEmpresa.UnidadTbjo.Save();
                    if (!Context.IsEditor)
                    {
                        var tarjetas = new List<PocTarjetaEmpresa>();
                        var tarjetasGrp = ObtenerTarjetasPorGrupo(entradaGrupo.IdGrupo, entradaGrupo.IdTipoGrupo);
                        if (!tarjetasGrp.IsNullOrEmpty())
                        {
                            foreach (var item in tarjetasGrp)
                            {
                                //Incluir la tarjeta al arreglo para aplicarle la configuracion
                                tarjetas.Add(new PocTarjetaEmpresa
                                {
                                    IdEmpresa = item.IdEmpresa,
                                    NumTarjeta = item.NumTarjeta
                                });
                            }
                            AplicarConfiguracionGrupo(entradaGrupo.IdGrupo, tarjetas.ToArray());
                        }
                    }
                }
                ts.Complete();
                return entrada.IdGrupo;
            }
        }

        private void ExcluirTarjetaDeGrupo(int idTarjeta, int idGrupo, int idTipoGrupo)
        {
            //Obtener la condición actual de la tarjeta
            if (idTipoGrupo == (int)EnumTipoGrupo.PanelControl)
            {
                var tarjeta = _repTarjeta.SelectById(idTarjeta);
                _appConfigurarCtrl.EstablecerCondicionPorDefecto(
                    (int)tarjeta.IdEmpresa, tarjeta.NumTarjeta);

                _repTarjeta.UpdateOn(x =>
                        x.IdTarjeta == idTarjeta, z => new TC_Tarjeta
                        {
                            IdGrupoEmpresa = null
                        });
            }
            else
            {
                var tarjetaGrupo = new TC_TarjetaGrupo { IdGrupo = idGrupo, IdTarjeta = idTarjeta };
                _repTarjetaGrupo.Delete(tarjetaGrupo);
            }
        }

        private void ValidarLimiteGrupo(int idGrupo, decimal? limite, List<PocTarjetaEmpresa> procesando)
        {
            if (limite.HasValue && limite > 0)
            {
                procesando = procesando ?? new List<PocTarjetaEmpresa>();
                var tarjetasNuevas = procesando.Where(m => m.Agrupada).Select(m => m.NumTarjeta);
                var tarjetasViejas = procesando.Where(m => !m.Agrupada).Select(m => m.NumTarjeta);
                var tarjetas = _repVwTarjetasCuenta.Table
                    .Where(m => m.CodEstadoCta == "V"
                        && (tarjetasNuevas.Contains(m.NumTarjeta) ||
                        (m.IdGrupoEmpresa == idGrupo && !tarjetasViejas.Contains(m.NumTarjeta))) &&
                        (m.IdRelacion == (int)EnumRelacionTarjeta.Adicional ||
                        (m.IdEsCuentaMadre == (int)EnumIdSiNo.Si || m.IdCuentaMadre.HasValue)))
                    .OrderByDescending(m => m.LimiteCreditoTarjetaInter);
                var tarjeta = tarjetas.FirstOrDefault();
                var limiteMayor = tarjeta == null ? 0 : tarjeta.LimiteCreditoTarjetaInter;
                if (limite < limiteMayor)
                {
                    var detalle = string.Format("El limite del grupo {0} de {1} es mayor a {2}",
                        idGrupo, limiteMayor, limite);
                    throw new CoreException(detalle, "Err_0085");
                }
            }
        }

        #endregion
    }
}

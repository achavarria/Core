using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Logs
{
    public partial class LG_LogEvento : EntidadBase
    {
        public int IdEvento { get; set; }
        public int IdTipoEvento { get; set; }
        public System.DateTime FecEvento { get; set; }
        public string CodUsuario { get; set; }
        public Nullable<int> IdSistema { get; set; }
        public System.DateTime FecInicio { get; set; }
        public Nullable<System.DateTime> FecFinal { get; set; }
        public Nullable<System.DateTime> HoraEvento { get; set; }
        public string DireccionIpv4 { get; set; }
        public string CodigoSesion { get; set; }
        public string Detalle { get; set; }
    }
}

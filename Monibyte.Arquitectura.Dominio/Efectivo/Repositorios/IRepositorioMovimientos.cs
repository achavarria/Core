﻿using Monibyte.Arquitectura.Dominio.Nucleo;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Efectivo.Repositorios
{
    public interface IRepositorioMovimientos : IRepositorioMnb<EF_Movimiento>
    {
        IEnumerable<EF_vwMovimiento> ObtenerMovimientos(ParamMovimientosEf parameters);
    }
}

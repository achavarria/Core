﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Poco.Seguridad;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;

namespace Monibyte.Arquitectura.Servicios.Web
{
    public class AppSessionReader : ISessionReader
    {
        public void GetHeaders(System.Net.WebHeaderCollection x)
        {
            if (!string.IsNullOrEmpty(x["IDSISTEMA"]))
            {
                var seg = x["IDSISTEMA"].DecompressStr();
                InfoSesion.IncluirSesion("IDSISTEMA", seg.FromJson<int>());
            }
            if (!string.IsNullOrEmpty(x["IDPROCESADOR"]))
            {
                var seg = x["IDPROCESADOR"].DecompressStr();
                InfoSesion.IncluirSesion("IDPROCESADOR", seg.FromJson<int>());
            }
            if (!string.IsNullOrEmpty(x["SEGURIDADACTIVA"]))
            {
                var seg = x["SEGURIDADACTIVA"].DecompressStr();
                InfoSesion.IncluirSesion("SEGURIDADACTIVA", seg.FromJson<int>());
            }
            if (!string.IsNullOrEmpty(x["FACTORCONVERSION"]))
            {
                var seg = x["FACTORCONVERSION"].DecompressStr();
                InfoSesion.IncluirSesion("FACTORCONVERSION", seg.FromJson<int>());
            }
            if (!string.IsNullOrEmpty(x["BUSCARAUTORIZACION"]))
            {
                var seg = x["BUSCARAUTORIZACION"].DecompressStr();
                InfoSesion.IncluirSesion("BUSCARAUTORIZACION", seg.FromJson<bool?>());
            }
            if (!string.IsNullOrEmpty(x["APLICAAUTORIZACION"]))
            {
                var seg = x["APLICAAUTORIZACION"].DecompressStr();
                InfoSesion.IncluirSesion("APLICAAUTORIZACION", seg.FromJson<bool?>());
            }
            if (!string.IsNullOrEmpty(x["INFOAUTORIZACION"]))
            {
                var seg = x["INFOAUTORIZACION"].DecompressStr();
                InfoSesion.IncluirSesion("INFOAUTORIZACION", seg.FromJson<PocInfoAutorizacion>());
            }
            if (!string.IsNullOrEmpty(x["AUTORIZAPRODUCTO"]))
            {
                var seg = x["AUTORIZAPRODUCTO"].DecompressStr();
                InfoSesion.IncluirSesion("AUTORIZAPRODUCTO", seg.FromJson<bool?>());
            }
            if (!string.IsNullOrEmpty(x["FECHAMAQUINA"]))
            {
                var seg = x["FECHAMAQUINA"].DecompressStr();
                InfoSesion.IncluirSesion("FECHAMAQUINA", seg.FromJson<DateTime?>());
            }
        }
    }
}
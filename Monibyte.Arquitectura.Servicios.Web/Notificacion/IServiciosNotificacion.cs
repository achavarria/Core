﻿using Monibyte.Arquitectura.Poco.Notificacion;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Monibyte.Arquitectura.Servicios.Web.Notificacion
{
    [ServiceContract]
    public interface IServiciosNotificacion : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ObtenerNotificaciones")]
        IEnumerable<PocConfigNotificacion> ObtenerNotificaciones(int? idEmpresa, int? idContexto, int? idEsActivo);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EstablecerNotificacion")]
        IEnumerable<PocConfigNotificacion> EstablecerNotificacion(List<PocConfigNotificacion> notificaciones);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerDestinatariosDefault")]
        IEnumerable<PocDestinatarioDefault> ObtenerDestinatariosDefault(int idEmpresa);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ProcesarNotificaciones")]
        void ProcesarNotificaciones(PocoQueue pocoQueue);
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using System.Threading;

namespace Monibyte.Arquitectura.Datos.Notificacion.Repositorios
{
    public class RepositorioEmail : IRepositorioEmail
    {
        private EmailHelper _emailHelper;
        public RepositorioEmail(string sectionName, bool web = true)
        {
            _emailHelper = new EmailHelper(sectionName, web: web);
        }

        public void EnviarInfoEmail(EmailConfig config)
        {
            ThreadPool.QueueUserWorkItem(state => _emailHelper.SendInfoEmail(config));
        }

        public void EnviarEmail(SenderConfig sender, EmailConfig config)
        {
            ThreadPool.QueueUserWorkItem(state => _emailHelper.SendEmail(sender, config));
        }
        public void EnviarEmailSync(SenderConfig sender, EmailConfig config)
        {
            _emailHelper.SendEmail(sender, config);
        }
    }
}

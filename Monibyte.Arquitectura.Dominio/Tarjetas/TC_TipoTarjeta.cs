using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_TipoTarjeta : EntidadBase
    {
        public TC_TipoTarjeta()
        {
            this.TC_Cuenta = new List<TC_Cuenta>();
            this.TC_CuentaHist = new List<TC_CuentaHist>();
            this.TC_Programa = new List<TC_Programa>();
        }

        public int IdTipoTarjeta { get; set; }
        public int IdClase { get; set; }
        public int IdAlcance { get; set; }
        public string Descripcion { get; set; }
        public int Bin { get; set; }
        public Nullable<int> IdEntidad { get; set; }
        public Nullable<int> IdMarca { get; set; }
        public Nullable<int> IdMonedaLimite { get; set; }
        public Nullable<int> MonLimiteMinimo { get; set; }
        public Nullable<int> MonLimiteMaximo { get; set; }
        public Nullable<int> MonIngresoMínimo { get; set; }
        public Nullable<int> PlazoVctoLineaCredito { get; set; }
        public decimal TasaInteresLocal { get; set; }
        public decimal TasaInteresInternacional { get; set; }
        public decimal TasaIntMoraLocal { get; set; }
        public decimal TasaIntMoraInternacional { get; set; }
        public decimal TasaIntCJInternacional { get; set; }
        public decimal TasaIntCJLocal { get; set; }
        public decimal TasaIntraFinLocal { get; set; }
        public decimal TasaIntraFinInternacional { get; set; }
        public int DiasGraciaMora { get; set; }
        public decimal MonCargoAdminAtraso { get; set; }
        public int IdExcCargoAdmin { get; set; }
        public decimal MonMaxReversionCargo { get; set; }
        public decimal PorcSobregiroAutorizado { get; set; }
        public int MinDiasSobregiro { get; set; }
        public Nullable<int> PlazoIntraFinLocal { get; set; }
        public Nullable<int> PlazoIntraFinInternacional { get; set; }
        public string ConfiguracionProcesador { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string MensajeEstadoCuenta { get; set; }
        public Nullable<int> IdTipoSaldoAFavor { get; set; }
        public decimal SaldoAFavor { get; set; }
        //public virtual GL_Catalogo GL_Catalogo { get; set; }
        //public virtual GL_Catalogo GL_Catalogo1 { get; set; }
        //public virtual GL_Catalogo GL_Catalogo2 { get; set; }
        //public virtual GL_Catalogo GL_Catalogo3 { get; set; }
        //public virtual GL_Entidad GL_Entidad { get; set; }
        public virtual ICollection<TC_Cuenta> TC_Cuenta { get; set; }
        public virtual ICollection<TC_CuentaHist> TC_CuentaHist { get; set; }
        public virtual ICollection<TC_Programa> TC_Programa { get; set; }
        //public virtual TS_Moneda TS_Moneda { get; set; }
        public Nullable<int> IdAplicaMillas { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Functions;
using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales;
using Monibyte.Arquitectura.Dominio.Generales;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<GL_CamposContexto> GL_CamposContexto { get; set; }
        public DbSet<GL_CamposReporte> GL_CamposReporte { get; set; }
        public DbSet<GL_Canton> GL_Canton { get; set; }
        public DbSet<GL_Catalogo> GL_Catalogo { get; set; }
        public DbSet<GL_CierresSistema> GL_CierresSistema { get; set; }
        public DbSet<GL_ColaImpresion> GL_ColaImpresion { get; set; }
        public DbSet<GL_ControlProceso> GL_ControlProceso { get; set; }
        public DbSet<GL_Distrito> GL_Distrito { get; set; }
        public DbSet<GL_Entidad> GL_Entidad { get; set; }
        public DbSet<GL_Equivalencia> GL_Equivalencia { get; set; }
        public DbSet<GL_Formato> GL_Formato { get; set; }
        public DbSet<GL_Idioma> GL_Idioma { get; set; }
        public DbSet<GL_Pais> GL_Pais { get; set; }
        public DbSet<GL_Parametro> GL_Parametro { get; set; }
        public DbSet<GL_ParametroCompania> GL_ParametroCompania { get; set; }
        public DbSet<GL_ParametroEmpresa> GL_ParametroEmpresa { get; set; }
        public DbSet<GL_ParametroEmpresaContexto> GL_ParametroEmpresaContexto { get; set; }
        public DbSet<GL_ParametroEmpresaLista> GL_ParametroEmpresaLista { get; set; }
        public DbSet<GL_ParametroSistema> GL_ParametroSistema { get; set; }
        public DbSet<GL_PlantillasNotificacion> GL_PlantillasNotificacion { get; set; }
        public DbSet<GL_Politica> GL_Politica { get; set; }
        public DbSet<GL_Proceso> GL_Proceso { get; set; }
        public DbSet<GL_Provincia> GL_Provincia { get; set; }
        public DbSet<GL_Reportes> GL_Reportes { get; set; }
        public DbSet<GL_Secuencia> GL_Secuencia { get; set; }
        public DbSet<GL_Sistema> GL_Sistema { get; set; }
        public DbSet<GL_Sucursal> GL_Sucursal { get; set; }
        public DbSet<GL_TipoIdentificacion> GL_TipoIdentificacion { get; set; }
        public DbSet<GL_Traductor> GL_Traductor { get; set; }
        public DbSet<GL_UnidadNegocio> GL_UnidadNegocio { get; set; }
        public DbSet<GL_Usuario> GL_Usuario { get; set; }

        void CreateContextModelGL(DbModelBuilder modelBuilder)
        {
            #region Funciones
            modelBuilder.Conventions.Add(new GL_ObtieneTraduccion());
            #endregion

            #region Tablas
            modelBuilder.Configurations.Add(new GL_CamposContextoMap());
            modelBuilder.Configurations.Add(new GL_CamposReporteMap());
            modelBuilder.Configurations.Add(new GL_CantonMap());
            modelBuilder.Configurations.Add(new GL_CatalogoMap());
            modelBuilder.Configurations.Add(new GL_CierresSistemaMap());
            modelBuilder.Configurations.Add(new GL_ColaImpresionMap());
            modelBuilder.Configurations.Add(new GL_ControlProcesoMap());
            modelBuilder.Configurations.Add(new GL_DistritoMap());
            modelBuilder.Configurations.Add(new GL_EntidadMap());
            modelBuilder.Configurations.Add(new GL_EquivalenciaMap());
            modelBuilder.Configurations.Add(new GL_FormatoMap());
            modelBuilder.Configurations.Add(new GL_IdiomaMap());
            modelBuilder.Configurations.Add(new GL_PaisMap());
            modelBuilder.Configurations.Add(new GL_ParametroMap());
            modelBuilder.Configurations.Add(new GL_ParametroCompaniaMap());
            modelBuilder.Configurations.Add(new GL_ParametroEmpresaMap());
            modelBuilder.Configurations.Add(new GL_ParametroEmpresaContextoMap());
            modelBuilder.Configurations.Add(new GL_ParametroEmpresaListaMap());
            modelBuilder.Configurations.Add(new GL_ParametroSistemaMap());
            modelBuilder.Configurations.Add(new GL_PlantillasNotificacionMap());
            modelBuilder.Configurations.Add(new GL_PoliticaMap());
            modelBuilder.Configurations.Add(new GL_ProcesoMap());
            modelBuilder.Configurations.Add(new GL_ProvinciaMap());
            modelBuilder.Configurations.Add(new GL_ReportesMap());
            modelBuilder.Configurations.Add(new GL_SecuenciaMap());
            modelBuilder.Configurations.Add(new GL_SistemaMap());
            modelBuilder.Configurations.Add(new GL_SucursalMap());
            modelBuilder.Configurations.Add(new GL_TipoIdentificacionMap());
            modelBuilder.Configurations.Add(new GL_TraductorMap());
            modelBuilder.Configurations.Add(new GL_UnidadNegocioMap());
            modelBuilder.Configurations.Add(new GL_UsuarioMap());
            #endregion
        }
    }
}

﻿using System;

namespace Monibyte.Arquitectura.Poco.TEF
{
    public class PocTransEntranteSINPE
    {
        public int CodServicio { get; set; }
        public string CodRefSinpe { get; set; }
        public int? CodRefCoreBanca { get; set; }
        public DateTime? FecValor { get; set; }
        public DateTime? FecLiquidacion { get; set; }

        public string IdentDestino { get; set; }
        public string CCDestino { get; set; }
        public string IdentOrigen { get; set; }
        public string CCOrigen { get; set; }

        public decimal Monto { get; set; }
        public int IdMoneda { get; set; }
        public string Detalle { get; set; }

        public int EstadoSinpe { get; set; }
        public int? CodRechazo { get; set; }
        public string DescripcionRechazo { get; set; }
    }

    public class PocTransaccionSinpe
    {
        public int IdTransaccion { get; set; }
        public int IdOrigenInterno { get; set; }
        public int? CodServicio { get; set; }

        public string CodEntidadOrigen { get; set; }
        public string IdentOrigen { get; set; }
        public string CCOrigen { get; set; }
        public int? IdMonedaOrigen { get; set; }

        public string CodEntidadDestino { get; set; }
        public string IdentDestino { get; set; }
        public string CCDestino { get; set; }
        public int IdMonedaDestino { get; set; }

        public decimal Comision { get; set; }
        public decimal MontoPago { get; set; }
        public decimal MontoTransaccion { get; set; }
        public decimal MontoNoDistribuido { get; set; }
        public string Detalle { get; set; }

        public string CodRefSinpe { get; set; }
        public int? CodRefCoreBancario { get; set; }

        public int IdEstadoTrans { get; set; }
        public int EstadoSINPE { get; set; }
        public string DetalleError { get; set; }
        public int? CodMotivoRechazo { get; set; }
        public string DescripcionRechazo { get; set; }
        public int IdCuenta { get; set; }

        public string UsuarioRegistra { get; set; }
        public System.DateTime? FecValor { get; set; }
        public System.DateTime? FecLiquidacion { get; set; }

        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }

        public int IdPersona { get; set; }
        public string NombrePersona { get; set; }
        public string NumTarjeta { get; set; }
        public string Justificacion { get; set; }
    }

    public class PocPagoTransaccion
    {
        public int IdPagoTrasaccion { get; set; }
        public int IdTransaccion { get; set; }

        public string CodMovimientoMQ { get; set; }
        public int IdTipoProducto { get; set; }
        public int Producto { get; set; }
        public int? SubProducto { get; set; }

        public int IdMoneda { get; set; }
        public decimal MontoPago { get; set; }

        public int IdEstado { get; set; }
        public string DetalleError { get; set; }

        public int? IdModoAplicado { get; set; }
        public string CodRefSiscard { get; set; }
        public string NumComprobanteInterno { get; set; }

        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }

        public string Justificacion { get; set; }
        public DateTime FecConsumo { get; set; }
        public string NumTarjeta { get; set; }
        public string NumExtraFinanciamiento { get; set; }
        public int IndiceControl { get; set; }
    }
}

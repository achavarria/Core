using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_AdministradoresGrupoMap : EntityTypeConfiguration<MB_AdministradoresGrupo>
    {
        public MB_AdministradoresGrupoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdAdminGrupo);

            // Properties
            this.Property(t => t.IdAdminGrupo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MB_AdministradoresGrupo", "Mnb");
            this.Property(t => t.IdAdminGrupo).HasColumnName("IdAdminGrupo");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.IdGrupoEmpresa).HasColumnName("IdGrupoEmpresa");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdSoloConsulta).HasColumnName("IdSoloConsulta");
            this.Property(t => t.IdAutoriza).HasColumnName("IdAutoriza");
        }
    }
}

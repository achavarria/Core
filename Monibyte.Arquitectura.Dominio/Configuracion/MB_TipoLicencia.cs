using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Generales;
using System;

namespace Monibyte.Arquitectura.Dominio.Configuracion
{
    public partial class MB_TipoLicencia : EntidadBase
    {
        public int IdTipoLicencia { get; set; }
        public int IdTipoCargo { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

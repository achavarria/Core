using Monibyte.Arquitectura.Dominio.Gestiones;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Gestiones
{
    public class GE_AccionMap : EntityTypeConfiguration<GE_Accion>
    {
        public GE_AccionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdAccion);

            // Properties
            this.Property(t => t.IdAccion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Detalle)
                .HasMaxLength(2000);

            this.Property(t => t.DetalleRespuesta)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("GE_Accion", "Gestion");
            this.Property(t => t.IdAccion).HasColumnName("IdAccion");
            this.Property(t => t.FecAccion).HasColumnName("FecAccion");
            this.Property(t => t.IdTipoAccion).HasColumnName("IdTipoAccion");
            this.Property(t => t.Detalle).HasColumnName("Detalle");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdGestion).HasColumnName("IdGestion");
            this.Property(t => t.IdResponsable).HasColumnName("IdResponsable");
            this.Property(t => t.IdUsuarioRespuesta).HasColumnName("IdUsuarioRespuesta");
            this.Property(t => t.IdTipoRespuesta).HasColumnName("IdTipoRespuesta");
            this.Property(t => t.FecProgramada).HasColumnName("FecProgramada");
            this.Property(t => t.FecRespuesta).HasColumnName("FecRespuesta");
            this.Property(t => t.DetalleRespuesta).HasColumnName("DetalleRespuesta");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

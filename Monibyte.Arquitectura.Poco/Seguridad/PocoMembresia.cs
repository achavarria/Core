﻿using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.Seguridad
{
    public class PocUsuarioRegistro
    {
        public int IdUsuario { get; set; }
        public string Correo { get; set; }
        public string AliasUsuario { get; set; }
        public int? IdIdiomaPreferencia { get; set; }
        public int? IdPaisPreferencia { get; set; }
        public IEnumerable<PocPreguntaUsuario> ListaPreguntas { get; set; }
    }

    public class PocPregunta
    {
        public int IdPregunta { get; set; }
        public string Descripcion { get; set; }
    }

    public class PocPreguntaUsuario
    {
        //public int IdUsuario { get; set; }
        public string CodUsuario { get; set; }
        public int IdPregunta { get; set; }
        public string Respuesta { get; set; }
        public string Descripcion { get; set; }
    }

    public class PocValidaRenovacion
    {
        public string AliasUsuario { get; set; }
        public string Correo { get; set; }
    }

    public class PocRenovacionPassword
    {
        public int IdUsuario { get; set; }
        public string CodUsuario { get; set; }
        public List<PocPreguntaUsuario> ListaPreguntas { get; set; }
    }
}

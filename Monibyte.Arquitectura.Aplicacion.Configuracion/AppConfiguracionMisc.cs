﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Configuracion.Repositorios;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Logs;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco.Configuracion;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Seguridad;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using AuthorDomain = Monibyte.Arquitectura.Dominio.Autorizador;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public class AppConfiguracionMisc : AplicacionBase, IAppConfiguracionMisc
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IRepositorioMnb<GL_Pais> _repPais;
        private IRepositorioMnb<MB_Region> _repRegion;
        private IRepositorioMnb<MB_PaisesRegion> _repPaisesRegion;
        private IRepositorioTipoComercio _repTipoComercio;
        private IRepositorioMnb<MB_CategoriaTipoComercio> _repCategoriaTipoComercio;
        private IRepositorioMnb<MB_RangoCategoriaTipoComercio> _repRangoCategoriaTipoComercio;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioTarjeta _repTarjeta;
        private IRepositorioMnb<MB_GruposEmpresa> _repGrupoEmpresa;
        private IRepositorioMnb<MB_AdministradoresGrupo> _repAdmGrupo;
        private IRepositorioMnb<LG_BitacoraPanelControl> _repBitacoraPanelControl;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _vwEmpresasUsuario;
        private IRepositorioMnb<SC_Autorizacion> _repAutorizacion;
        private IRepositorioVwTarjetaUsuario _repVwTarjetaUsuario;
        private IRepositorioUsuario _repUsuario;
        private IRepositorioCtrl<AuthorDomain.MB_CategoriasEmpresa> _repCategoriaPorEmpresa;
        private IRepositorioMnb<CL_PerfilEmpresa> _repPerfilEmpresa;

        public AppConfiguracionMisc(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<GL_Entidad> repEntidad,
            IRepositorioMnb<GL_Pais> repPais,
            IRepositorioMnb<MB_Region> repRegion,
            IRepositorioMnb<MB_PaisesRegion> repPaisesRegion,
            IRepositorioTipoComercio repTipoComercio,
            IRepositorioMnb<MB_CategoriaTipoComercio> repCategoriaTipoComercio,
            IRepositorioMnb<MB_RangoCategoriaTipoComercio> repRangoCategoriaTipoComercio,
            IRepositorioMnb<LG_BitacoraPanelControl> repBitacoraPanelControl,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioTarjeta repTarjeta,
            IRepositorioMnb<MB_GruposEmpresa> repGrupoEmpresa,
            IRepositorioMnb<MB_AdministradoresGrupo> repAdmGrupo,
            IRepositorioMnb<CL_vwEmpresasUsuario> vwEmpresasUsuario,
            IRepositorioMnb<SC_Autorizacion> repAutorizacion,
            IRepositorioVwTarjetaUsuario repVwTarjetaUsuario,
            IRepositorioUsuario repUsuario,
            IRepositorioCtrl<AuthorDomain.MB_CategoriasEmpresa> repCategoriaPorEmpresa,
            IRepositorioMnb<CL_PerfilEmpresa> repPerfilEmpresa)
        {
            _repCatalogo = repCatalogo;
            _repEntidad = repEntidad;
            _repPais = repPais;
            _repRegion = repRegion;
            _repPaisesRegion = repPaisesRegion;
            _repTipoComercio = repTipoComercio;
            _repCategoriaTipoComercio = repCategoriaTipoComercio;
            _repRangoCategoriaTipoComercio = repRangoCategoriaTipoComercio;
            _repBitacoraPanelControl = repBitacoraPanelControl;
            _repPersona = repPersona;
            _repTarjeta = repTarjeta;
            _repGrupoEmpresa = repGrupoEmpresa;
            _repAdmGrupo = repAdmGrupo;
            _vwEmpresasUsuario = vwEmpresasUsuario;
            _repAutorizacion = repAutorizacion;
            _repVwTarjetaUsuario = repVwTarjetaUsuario;
            _repUsuario = repUsuario;
            _repCategoriaPorEmpresa = repCategoriaPorEmpresa;
            _repPerfilEmpresa = repPerfilEmpresa;
        }

        public List<PocRegion> ObtenerRegiones()
        {
            var region = _repRegion.Table
                .OrderBy(p => p.Descripcion)
                .Select(item => new PocRegion
                {
                    IdRegion = item.IdRegion,
                    Descripcion = _fGenerales.Traducir(item.Descripcion,
                        "MB_Region", item.IdRegion, Context.Lang.Id, 0, Context.DefaultLang.Id)
                });
            return region.ToList();
        }

        public List<PocPaisRegion> ObtenerPaisesRegion(int IdRegion)
        {
            var entidad = _repEntidad.SelectById(InfoSesion.ObtenerSesion<int>("IDPROCESADOR"));
            var paisReg = from pais in _repPais.Table
                          join paisregion in _repPaisesRegion.Table on pais.IdPais equals paisregion.IdPais
                          where paisregion.IdRegion == IdRegion && paisregion.IdPais != entidad.IdPais
                          select new PocPaisRegion
                          {
                              IdRegion = (int)paisregion.IdRegion,
                              IdPais = (int)paisregion.IdPais,
                              Descripcion = _fGenerales.Traducir(pais.Descripcion,
                                    "GL_Pais", pais.IdPais, Context.Lang.Id, 0, Context.DefaultLang.Id)
                          };
            return paisReg.OrderBy(p => p.Descripcion).ToList();
        }

        public List<PocCategoriaComercio> ObtenerListaCategoriaComercio()
        {
            var catPorEmpresa = _repCategoriaPorEmpresa.Table
                .Where(x => x.IdEmpresa == InfoSesion.Info.IdEmpresa);

            var catPadre = catPorEmpresa.Select(x => x.IdCategoriaPadre);
            var catHijas = catPorEmpresa.Select(x => x.IdCategoria);

            var perfil = _repPerfilEmpresa.Table.FirstOrDefault
                (x => x.IdEmpresa == InfoSesion.Info.IdEmpresa);

            var verSubCat = perfil != null ? perfil.
                IdVerSubCatMcc == (int)EnumIdSiNo.Si : false;

            var catTipCom =
                from cat in _repCategoriaTipoComercio.Table
                select new
                {
                    IdCategoria = cat.IdCategoria,
                    Descripcion = _fGenerales.Traducir(cat.Descripcion,
                        "MB_CategoriaTipoComercio", cat.IdCategoria,
                        Context.Lang.Id, 0, Context.DefaultLang.Id),
                    IdEstado = cat.IdEstado,
                    IdEsGrupo = cat.IdEsGrupo
                };
            var categorias = catTipCom.ToList()
                .GroupJoin(_repCategoriaPorEmpresa.Table,
                    ctc => ctc.IdCategoria,
                    cpe => cpe.IdCategoriaPadre, (ctc, cpe) => new
                    {
                        ctc,
                        cpe = cpe.Select(x => x.IdCategoria)
                    })
                .Where(x => x.ctc.IdCategoria >= 0 && (catPorEmpresa != null &&
                        catPadre.Contains(x.ctc.IdCategoria) ||
                        (verSubCat ? x.ctc.IdEsGrupo == (int)EnumIdSiNo.No :
                            x.ctc.IdEsGrupo == (int)EnumIdSiNo.No &&
                            !catHijas.Contains(x.ctc.IdCategoria))))
                .OrderBy(x => x.ctc.IdCategoria == 0 ? "AAAA" : x.ctc.Descripcion)
                .Select(item => new PocCategoriaComercio
                {
                    IdCategoria = item.ctc.IdCategoria,
                    Descripcion = item.ctc.Descripcion,
                    Texto = item.ctc.Descripcion,
                    IdsSubCat = item.cpe.ToArray()
                });
            return categorias.ToList();
        }

        public List<PocComercio> ObtenerListaComercio(int idCategoria)
        {
            var resultado = new List<PocComercio>();
            var rangos = _repRangoCategoriaTipoComercio.Table.Where(p => p.IdCategoriaMcc == idCategoria);
            foreach (var rango in rangos)
            {
                var comercios = _repTipoComercio.Table
                    .Where(com => com.CodMcc.CompareTo(rango.CodMccDesde) >= 0 &&
                           com.CodMcc.CompareTo(rango.CodMccHasta) <= 0)
                    .Select(poc => new PocComercio
                    {
                        CodMccDesde = poc.CodMcc,
                        CodMccHasta = poc.CodMcc,
                        Descripcion = poc.Descripcion,
                        Texto = string.Format("{0} - {1}", poc.CodMcc, poc.Descripcion)
                    });
                resultado.AddRange(comercios);
            }
            return resultado.OrderBy(res => res.CodMccDesde).ToList();
        }

        public DataResult<PocBitacoraPanelCtrl> ObtenerBitacoraPanelCrtl(PocFiltroBitacoraPanelCtrl filtro, DataRequest request)
        {
            var validation = new ValidationResponse();
            if (filtro == null || (!filtro.IdCondicion.HasValue && string.IsNullOrEmpty(filtro.NumTarjeta)))
            {
                validation.AddError("Err_0035");
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Error en consulta de bitácora", validation: validation);
            }
            var criteria = new Criteria<LG_BitacoraPanelControl>();
            if (filtro.IdCondicion.HasValue)
            {
                criteria.And(item => item.IdCondicion == filtro.IdCondicion);
            }
            criteria.And(item => item.NumTarjeta == filtro.NumTarjeta);
            filtro.FechaHasta = filtro.FechaHasta.AddDays(Time.DayFactor);

            var resultado =
                _repBitacoraPanelControl.SelectBy(criteria)
                .Join(_repUsuario.Table,
                    bit => bit.IdUsuarioIncluyeAud,
                    usr => usr.IdUsuario,
                    (bit, usr) => new { bit, usr })
                .Join(_repPersona.Table,
                    tmp => tmp.usr.IdPersona,
                    per => per.IdPersona,
                    (tmp, per) => new { tmp.bit, per })
                .Where(res =>
                    res.bit.FecInclusionAud >= filtro.FechaDesde &&
                    res.bit.FecInclusionAud <= filtro.FechaHasta)
                .OrderBy(res => res.bit.FecInclusionAud).ToList()
                .Select(res => new PocBitacoraPanelCtrl
                {
                    IdCondicion = res.bit.IdCondicion,
                    NumTarjeta = res.bit.NumTarjeta,
                    DatosBitacora = ObtieneDetalleBitacoraPanelCtrl(res.bit.DatosBitacora.DecompressStr()),
                    FechaInclusion = res.bit.FecInclusionAud,
                    HoraInclusion = new TimeSpan(
                        res.bit.FecInclusionAud.Hour,
                        res.bit.FecInclusionAud.Minute,
                        res.bit.FecInclusionAud.Second),
                    UsuarioInclusion = res.per.NombreCompleto
                });
            return resultado.ToDataResult(request);
        }

        public DataResult<PocUsuario> ObtenerUsuariosPorGrupo(PocUsuariosGrupos filtro, DataRequest request)
        {
            var criteria = new Criteria<CL_Persona>();
            var criteriaUsr = new Criteria<CL_vwEmpresasUsuario>();
            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.NumIdentificacion))
                {
                    criteria.And(m => m.NumIdentificacion.Contains(filtro.NumIdentificacion));
                }
                if (!string.IsNullOrEmpty(filtro.NombreCompleto))
                {
                    criteria.And(m => m.NombreCompleto.Contains(filtro.NombreCompleto));
                }
                if (!string.IsNullOrEmpty(filtro.CodUsuario))
                {
                    criteriaUsr.And(m => m.CodUsuario.Contains(filtro.CodUsuario));
                }
                criteriaUsr.And(m => m.IdEmpresa == InfoSesion.Info.IdEmpresa);
            }
            try
            {
                var resultado =
                    _vwEmpresasUsuario.SelectBy(criteriaUsr)
                    .Join(_repPersona.SelectBy(criteria),
                        usr => usr.IdPersona,
                        per => per.IdPersona,
                        (usr, per) => new { usr, per })
                    .Join(_repCatalogo.List("LTIPOUSUARIO"),
                        item => item.usr.IdTipoUsuario,
                        list => list.IdCatalogo,
                        (item, tipoUsr) => new { item.usr, item.per, tipoUsr })
                    .Join(_repCatalogo.List("LESTADOUSUARIO"),
                        item => item.usr.IdEstado,
                        list => list.IdCatalogo,
                        (item, estadoUsr) => new { item.usr, item.per, item.tipoUsr, estadoUsr })
                    .GroupJoin(_repTarjeta.Table,
                        item => item.per.IdPersona,
                        tar => tar.IdPersona,
                        (item, tar) => new { item.usr, item.per, item.tipoUsr, item.estadoUsr, tar = tar.FirstOrDefault() })
                    .GroupJoin(_repGrupoEmpresa.Table,
                        item => item.tar != null ? item.tar.IdGrupoEmpresa : 0,
                        grp => grp.IdGrupo,
                        (item, grp) => new { item.usr, item.per, item.tipoUsr, item.estadoUsr, item.tar, grp = grp.FirstOrDefault() })
                    .Select(item => new PocUsuario
                    {
                        NumIdentificacion = item.per.NumIdentificacion,
                        NombreCompleto = item.per.NombreCompleto,
                        CodUsuario = item.usr.CodUsuario,
                        Alias = item.usr.AliasUsuario,
                        IdTipoUsuario = (int)item.usr.IdTipoUsuario,
                        TipoUsuario = item.tipoUsr.Descripcion,
                        Estado = item.estadoUsr.Descripcion,
                        Grupo = item.grp != null ? item.grp.Descripcion : string.Empty,
                        IdUsuario = item.usr.IdUsuario,
                        CodUsuarioEmpresa = item.usr.CodUsuarioEmpresa
                    });

                return resultado.ToDataResult(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<PocAutorizacion> ObtenerAutorizaciones()
        {
            return ObtenerAutorizacionesQuery();
        }

        public int ObtenerAutorizacionesCount()
        {
            if (InfoSesion.Info.IdTipoUsuario == (int)EnumTipoUsuario.Adicional)
            {
                return 0;
            }
            return ObtenerAutorizacionesQuery().Count();
        }

        private IQueryable<PocAutorizacion> ObtenerAutorizacionesQuery()
        {
            var idEmpresa = InfoSesion.Info.IdEmpresa;
            var criteria = new Criteria<SC_Autorizacion>();
            criteria.And(x => x.IdEmpresa == idEmpresa);
            criteria.And(x => x.IdEstado == (int)EnumEstadoAutorizacion.Preparada);
            var query = _repAutorizacion.SelectBy(criteria)
                .Join(_vwEmpresasUsuario.Table,
                    aut => new { IdUsuario = aut.IdUsuarioPrepara, aut.IdEmpresa },
                    usr => new { usr.IdUsuario, usr.IdEmpresa },
                    (aut, usr) => new { aut, usr })
                .Join(_repPersona.Table,
                    item => item.usr.IdPersona,
                    per => per.IdPersona,
                    (item, per) => new { item.aut, item.usr, per })
                .Join(_repCatalogo.List("LESTADOAUTORIZACION"),
                    item => item.aut.IdEstado,
                    ct => ct.IdCatalogo,
                    (item, ct) => new { item.aut, item.usr, item.per, ct })
                .Join(_repCatalogo.List("LTIPOAUTORIZACION"),
                    item => item.aut.IdTipoAutorizacion,
                    tipo => tipo.IdCatalogo,
                    (item, tipo) => new { item.aut, item.usr, item.per, item.ct, tipo })
                .Select(x => new PocAutorizacion
                {
                    DetalleAutorizacion = x.aut.DetalleAutorizacion,
                    EstadoAutorizacion = x.ct.Descripcion,
                    FecAutoriza = x.aut.FecAutoriza,
                    FecPrepara = x.aut.FecPrepara,
                    IdAutorizacion = x.aut.IdAutorizacion,
                    IdEmpresa = x.aut.IdEmpresa,
                    IdEstado = x.aut.IdEmpresa,
                    IdTipoAutorizacion = x.aut.IdTipoAutorizacion,
                    UsuarioEditor = x.per.NombreCompleto,
                    NumReferencia = x.aut.NumReferencia,
                    IdUsuarioPrepara = x.aut.IdUsuarioPrepara,
                    IdUsuarioAutoriza = x.aut.IdUsuarioAutoriza,
                    TipoAutorizacion = x.tipo.Descripcion,
                    Descripcion = x.aut.Descripcion
                });
            var criteriaAdm = new Criteria<MB_AdministradoresGrupo>();
            if (InfoSesion.Info.IdTipoUsuario == (int)EnumTipoUsuario.SubAdministrador)
            {
                var sub1 = _repAdmGrupo.Table
                    .Join(_repGrupoEmpresa.Table,
                        adm => adm.IdGrupoEmpresa,
                        gp => gp.IdGrupo,
                        (adm, gp) => new { adm, gp })
                    .Where(m => m.adm.IdAutoriza == (int)EnumIdSiNo.Si &&
                        m.adm.IdUsuario == InfoSesion.Info.IdUsuario &&
                        m.gp.IdEmpresa == idEmpresa)
                    .Select(m => m.adm.IdGrupoEmpresa.ToString());

                var sub2 = _repVwTarjetaUsuario.TarjetasPermitidas()
                    .Where(x => x.IdAutoriza == (int)EnumIdSiNo.Si)
                    .Select(m => m.NumTarjeta);

                query = query.Where(m =>
                    (sub1.Contains(m.NumReferencia) && m.IdTipoAutorizacion ==
                        (int)EnumTipoAutorizacion.RestriccionesGrupo) ||
                    (sub2.Contains(m.NumReferencia)));
            }
            return query.OrderBy(x => x.FecPrepara).AsQueryable();
        }

        public string ObtieneDetalleBitacoraPanelCtrl(string datosBitacora)
        {
            Func<MultilanguageTopic, string> _lam = null;
            _lam = (data) =>
            {
                data.Details.ForEach(detail =>
                {
                    for (int i = 0; i < detail.DetailParams.Length; i++)
                    {
                        if (detail.DetailParams[i] is JObject)
                        {
                            string translation = null;
                            string description = null;
                            var JO = detail.DetailParams[i] as JObject;
                            var context = JO["Context"].ToString();
                            var contextId = (int?)JO["ContextId"];
                            if (contextId.HasValue)
                            {
                                switch (context)
                                {
                                    case "MB_CategoriaTipoComercio":
                                        var cat = _repCategoriaTipoComercio.Table.FirstOrDefault
                                            (x => x.IdCategoria == contextId);
                                        description = cat.Descripcion;
                                        break;
                                    case "LDIASSEMANA":
                                        var dia = _repCatalogo.Table.FirstOrDefault(x =>
                                            x.Codigo == contextId.ToString() &&
                                            x.Lista == context);
                                        contextId = dia.IdCatalogo;
                                        description = dia.Descripcion;
                                        break;
                                    case "MB_Region":
                                        var reg = _repRegion.Table.FirstOrDefault
                                            (x => x.IdRegion == contextId);
                                        description = reg.Descripcion;
                                        break;
                                    case "GL_Pais":
                                        var pais = _repPais.Table.FirstOrDefault
                                            (x => x.IdPais == contextId);
                                        description = pais.Descripcion;
                                        break;
                                }
                                translation = _fGenerales.Traducir(description,
                                    context, contextId.Value, Context.Lang.Id,
                                    0, Context.DefaultLang.Id);
                            }

                            detail.DetailParams[i] = translation;
                        }
                    }
                });
                data.Items.ForEach(item => _lam(item));
                return data.ToString();
            };

            return _lam(datosBitacora.FromJson<MultilanguageTopic>());
        }
    }
}

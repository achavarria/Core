using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_DetalleMovimiento_TT : EntidadBase
    {
        public string IdReferencia { get; set; }
        public int IdTarjeta { get; set; }
        public Nullable<System.DateTime> FecMovimiento { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string CamposDinamicos { get; set; }
    }
}

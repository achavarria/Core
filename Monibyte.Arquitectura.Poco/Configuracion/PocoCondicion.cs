﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Poco.Configuracion
{
    public class PocFiltroBitacoraPanelCtrl
    {
        public int? IdCondicion { get; set; }
        public string NumTarjeta { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public string UsuarioInclusion { get; set; }
    }
    public class PocBitacoraPanelCtrl
    {
        public int IdCondicion { get; set; }
        public string NumTarjeta { get; set; }
        public string DatosBitacora { get; set; }
        public string UsuarioInclusion { get; set; }
        public DateTime FechaInclusion { get; set; }
        public TimeSpan HoraInclusion { get; set; }
    }

    public class PocCondicionDuplica
    {
        public int IdCondicion { get; set; }
        public string NombreNuevo { get; set; }
    }

    public class PocCondicion : IEquatable<PocCondicion>
    {
        public int IdCondicion { get; set; }
        public int IdMoneda { get; set; }
        public string NumTarjeta { get; set; }
        public string Descripcion { get; set; }
        public int IdEmpresa { get; set; }
        public int IdEstado { get; set; }
        public string DescripcionEstado { get; set; }
        public decimal? LimiteCredito { get; set; }
        /*****restricciones*****/
        public bool SinRestriccion { get; set; }
        public bool EsVip { get; set; }
        public DateTime? EsVipDesde { get; set; }
        public DateTime? EsVipHasta { get; set; }
        public bool ConsumeLocal { get; set; }
        public bool RestringeRangoConsumeLocal { get; set; }
        public DateTime? ConsumeLocalDesde { get; set; }
        public DateTime? ConsumeLocalHasta { get; set; }
        public bool ConsumeInternacional { get; set; }
        public bool RestringeRangoConsumeInternacional { get; set; }
        public DateTime? ConsumeInternacionalDesde { get; set; }
        public DateTime? ConsumeInternacionalHasta { get; set; }
        public bool PermiteAvanceEfectivo { get; set; }
        public bool PermiteSoloAvanceEfectivo { get; set; }
        public bool RestringeRangoPermiteAvanceEfectivo { get; set; }
        public DateTime? PermiteAvanceEfectivoDesde { get; set; }
        public DateTime? PermiteAvanceEfectivoHasta { get; set; }
        public bool ConsumeInternet { get; set; }
        public bool RestringeRangoConsumeInternet { get; set; }
        public DateTime? ConsumeInternetDesde { get; set; }
        public DateTime? ConsumeInternetHasta { get; set; }
        public bool RestringeMcc { get; set; }
        public List<PocMccCondicion> RestriccionesMcc { get; set; }
        public bool RestringeHorario { get; set; }
        public List<PocHorarioCondicion> RestriccionesHorario { get; set; }
        public bool RestringeRegion { get; set; }
        public List<PocRegionPaisCondicion> RestriccionesRegionPais { get; set; }
        public bool EsAutorizacion { get; set; }

        public string Observaciones { get; set; }

        /*PARA EFECTOS DEL APP*/
        public bool DesactivarRestriccionesPorLapso { get; set; }
        public DateTime? DesactivarRestriccionesDesde { get; set; }
        public DateTime? DesactivarRestriccionesHasta { get; set; }
        /*****restricciones*****/

        /**Datos Cambio Montos**/
        public int IdMonedaLocal { get; set; }
        public decimal TipoCambioCompra { get; set; }

        public bool EqualsLvl1(PocCondicion o)
        {
            return IsEquals(o, false);
        }

        public bool Equals(PocCondicion o)
        {
            return IsEquals(o, true);
        }

        private bool IsEquals(PocCondicion o, bool includeSub)
        {
            var resultado = (this.Descripcion ?? string.Empty) == (o.Descripcion ?? string.Empty) &&
                this.LimiteCredito == o.LimiteCredito &&
                this.SinRestriccion == o.SinRestriccion &&
                this.EsVip == o.EsVip &&
                this.IdMoneda == o.IdMoneda &&
                this.ConsumeLocal == o.ConsumeLocal &&
                this.ConsumeInternacional == o.ConsumeInternacional &&
                this.PermiteAvanceEfectivo == o.PermiteAvanceEfectivo &&
                this.PermiteSoloAvanceEfectivo == o.PermiteSoloAvanceEfectivo &&
                this.ConsumeInternet == o.ConsumeInternet &&
                this.RestringeRangoConsumeLocal == o.RestringeRangoConsumeLocal &&
                this.RestringeRangoConsumeInternacional == o.RestringeRangoConsumeInternacional &&
                this.RestringeRangoPermiteAvanceEfectivo == o.RestringeRangoPermiteAvanceEfectivo &&
                this.RestringeRangoConsumeInternet == o.RestringeRangoConsumeInternet &&
                this.RestringeMcc == o.RestringeMcc &&
                this.RestringeHorario == o.RestringeHorario &&
                this.RestringeRegion == o.RestringeRegion &&
                this.DesactivarRestriccionesPorLapso == o.DesactivarRestriccionesPorLapso;
            if (includeSub)
            {
                if (o == null) { return false; }
                this.RestriccionesMcc = this.RestriccionesMcc.OrderBy(m => m.IdCategoriaMcc).ThenBy(m => m.CodMcc).ToList();
                o.RestriccionesMcc = o.RestriccionesMcc.OrderBy(m => m.IdCategoriaMcc).ThenBy(m => m.CodMcc).ToList();
                this.RestriccionesHorario = this.RestriccionesHorario.OrderBy(m => m.IdDia).ToList();
                o.RestriccionesHorario = o.RestriccionesHorario.OrderBy(m => m.IdDia).ToList();
                this.RestriccionesRegionPais = this.RestriccionesRegionPais.OrderBy(m => m.IdRegion).ThenBy(m => m.IdPais).ToList();
                o.RestriccionesRegionPais = o.RestriccionesRegionPais.OrderBy(m => m.IdRegion).ThenBy(m => m.IdPais).ToList();
                resultado = resultado &&
                    this.RestriccionesMcc.IsEqualTo(o.RestriccionesMcc) &&
                    this.RestriccionesHorario.IsEqualTo(o.RestriccionesHorario) &&
                    this.RestriccionesRegionPais.IsEqualTo(o.RestriccionesRegionPais) &&
                    this.EsVipDesde == o.EsVipDesde &&
                    this.EsVipHasta == o.EsVipHasta &&
                    this.ConsumeLocalDesde == o.ConsumeLocalDesde &&
                    this.ConsumeLocalHasta == o.ConsumeLocalHasta &&
                    this.ConsumeInternacionalDesde == o.ConsumeInternacionalDesde &&
                    this.ConsumeInternacionalHasta == o.ConsumeInternacionalHasta &&
                    this.PermiteAvanceEfectivoDesde == o.PermiteAvanceEfectivoDesde &&
                    this.PermiteAvanceEfectivoHasta == o.PermiteAvanceEfectivoHasta &&
                    this.ConsumeInternetDesde == o.ConsumeInternetDesde &&
                    this.ConsumeInternetHasta == o.ConsumeInternetHasta &&
                    this.DesactivarRestriccionesDesde == o.DesactivarRestriccionesDesde &&
                    this.DesactivarRestriccionesHasta == o.DesactivarRestriccionesHasta;
            }
            return resultado;
        }
    }

    public class PocMccCondicion : IEquatable<PocMccCondicion>
    {
        public int IdCategoriaMcc { get; set; }
        public int IdMcc { get; set; }
        public int IdCondicion { get; set; }
        public string CodMcc { get; set; }
        public int IdEstado { get; set; }
        public string DescripcionMcc { get; set; }
        public string DescripcionCategoria { get; set; }
        public string DescripcionPeriodicidad { get; set; }
        public decimal? LimiteConsumo { get; set; }
        public decimal? LimiteConsumoInter { get; set; }
        public decimal? LimiteConsumoAdm { get; set; }
        public int? CantidadConsumo { get; set; }
        public int? CantidadConsumoAdm { get; set; }
        public decimal? SaldoLimiteConsumo { get; set; }
        public decimal? SaldoLimiteConsumoInter { get; set; }
        public int? SaldoCantidadConsumo { get; set; }
        public int IdPeriodicidad { get; set; }
        public int[] IdsSubCat { get; set; }
        public int IdSoloCategoria { get; set; }

        public bool Equals(PocMccCondicion o)
        {
            return this.IdCategoriaMcc == o.IdCategoriaMcc &&
                (this.CodMcc ?? string.Empty) == (o.CodMcc ?? string.Empty) &&
                this.LimiteConsumo == o.LimiteConsumo &&
                this.LimiteConsumoInter == o.LimiteConsumoInter &&
                this.SaldoLimiteConsumo == o.SaldoLimiteConsumo &&
                this.SaldoLimiteConsumoInter == o.SaldoLimiteConsumoInter &&
                this.CantidadConsumo == o.CantidadConsumo &&
                this.SaldoCantidadConsumo == o.SaldoCantidadConsumo &&
                this.IdPeriodicidad == o.IdPeriodicidad;
        }
    }

    public class PocHorarioCondicion : IEquatable<PocHorarioCondicion>
    {
        public int IdHorario { get; set; }
        public int IdCondicion { get; set; }
        public int IdDia { get; set; }
        public int IdEstado { get; set; }
        public int? IdRestriccion { get; set; }
        public DateTime HoraDesde { get; set; }
        public DateTime HoraHasta { get; set; }
        public string DescripcionDia { get; set; }

        public bool Equals(PocHorarioCondicion o)
        {
            return this.IdDia == o.IdDia &&
                this.IdRestriccion == o.IdRestriccion &&
                this.HoraDesde == o.HoraDesde &&
                this.HoraHasta == o.HoraHasta;
        }
    }

    public class PocRegionPaisCondicion : IEquatable<PocRegionPaisCondicion>
    {
        public int IdRegionPais { get; set; }
        public int IdCondicion { get; set; }
        public int? IdRegion { get; set; }
        public int? IdPais { get; set; }
        public int IdEstado { get; set; }
        public string DescripcionRegion { get; set; }
        public string DescripcionPais { get; set; }

        public bool Equals(PocRegionPaisCondicion o)
        {
            return this.IdRegion == o.IdRegion &&
                this.IdPais == o.IdPais;
        }
    }
}

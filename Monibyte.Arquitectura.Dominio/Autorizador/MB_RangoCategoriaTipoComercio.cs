using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Autorizador
{
    public partial class MB_RangoCategoriaTipoComercio : EntidadBaseCtrl
    {
        public int IdRango { get; set; }
        public int IdCategoriaMcc { get; set; }
        public string CodMccDesde { get; set; }
        public string CodMccHasta { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

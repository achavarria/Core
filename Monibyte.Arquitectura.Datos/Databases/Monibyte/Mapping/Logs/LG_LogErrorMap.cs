using Monibyte.Arquitectura.Dominio.Logs;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Logs
{
    public class LG_LogErrorMap : EntityTypeConfiguration<LG_LogError>
    {
        public LG_LogErrorMap()
        {
            // Primary Key
            this.HasKey(t => t.IdError);

            // Properties
            this.Property(t => t.Usuario)
                .HasMaxLength(100);

            this.Property(t => t.DescError)
                .HasMaxLength(2000);

            this.Property(t => t.Capa)
                .HasMaxLength(10);

            this.Property(t => t.NombreObjeto)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("LG_LogError", "Logs");
            this.Property(t => t.IdError).HasColumnName("IdError");
            this.Property(t => t.IdTipoError).HasColumnName("IdTipoError");
            this.Property(t => t.FecError).HasColumnName("FecError");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.DescError).HasColumnName("DescError");
            this.Property(t => t.Capa).HasColumnName("Capa");
            this.Property(t => t.NombreObjeto).HasColumnName("NombreObjeto");
            this.Property(t => t.IdSistema).HasColumnName("IdSistema");
            this.Property(t => t.HoraError).HasColumnName("HoraError");
        }
    }
}

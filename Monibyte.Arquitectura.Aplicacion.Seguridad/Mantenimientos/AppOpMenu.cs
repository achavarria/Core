﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad.Mantenimientos
{
    public class AppOpMenu : AplicacionBase, IAppOpMenu
    {
        private IRepositorioMnb<SC_Menu> _repMenu;

        public AppOpMenu(IRepositorioMnb<SC_Menu> repMenu)
        {
            _repMenu = repMenu;
        }

        public List<PocMenu> ListaConsulta()
        {
            var critMenu = new Criteria<SC_Menu>();
            var resultado = _repMenu.SelectBy(critMenu)
                .Join(_repMenu.Table,
                    me => me.IdMenu,
                    m => m.IdMenu,
                    (me, m) => new { me, m })
                .Select(tmp => new PocMenu
                {
                    IdMenu = tmp.me.IdMenu,
                    Descripcion = tmp.me.Descripcion
                });
            return resultado.ToList();
        }
    }
}

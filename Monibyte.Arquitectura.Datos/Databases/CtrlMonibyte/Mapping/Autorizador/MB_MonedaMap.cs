using Monibyte.Arquitectura.Dominio.Autorizador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador
{
    public class MB_MonedaMap : EntityTypeConfiguration<MB_Moneda>
    {
        public MB_MonedaMap()
        {
            // Primary Key
            this.HasKey(t => t.CodMonedaISO);

            // Properties
            this.Property(t => t.CodMonedaISO)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.NumInternacional)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("MB_Moneda", "Mnb");
            this.Property(t => t.CodMonedaISO).HasColumnName("CodMonedaISO");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.NumInternacional).HasColumnName("NumInternacional");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

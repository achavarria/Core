﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Poco.Notificacion;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public partial class AppNotificacion
    {
        //Notificaiones provenientes del CORE (Modificación de restricciones)
        public void Method0103(PocoQueue pocoQueue)
        {
            var dataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(pocoQueue.Data);
            var numTar = dataDictionary.ContainsKey("CardNumber") ? dataDictionary["CardNumber"] as string : null;
            if (numTar != null)
            {
                var tarjeta = _repTarjeta.Table.FirstOrDefault(x => x.NumTarjeta == numTar);
                dataDictionary.Add("CardholderName", tarjeta.NombreImpreso);
            }

            var numPer = dataDictionary.ContainsKey("IdPersona") ? int.Parse(dataDictionary["IdPersona"].ToString()) : 0;
            if (numPer != null)
            {
                var persona = _repPersona.Table.FirstOrDefault(x => x.IdPersona == numPer);
                dataDictionary.Add("NombreUsuarioModifica", persona.NombreCompleto);
            }            
            pocoQueue.Data = dataDictionary.ToJson();
            ProcesarNotificaciones(pocoQueue, false, false, pocoQueue.CompanyId);
        }
    }
}
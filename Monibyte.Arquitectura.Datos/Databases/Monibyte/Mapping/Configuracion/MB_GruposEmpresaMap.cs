using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_GruposEmpresaMap : EntityTypeConfiguration<MB_GruposEmpresa>
    {
        public MB_GruposEmpresaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdGrupo);

            // Properties
            this.Property(t => t.IdGrupo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("MB_GruposEmpresa", "Mnb");
            this.Property(t => t.IdGrupo).HasColumnName("IdGrupo");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.LimiteCredito).HasColumnName("LimiteCredito");
            this.Property(t => t.IdTipoGrupo).HasColumnName("IdTipoGrupo");

        }
    }
}

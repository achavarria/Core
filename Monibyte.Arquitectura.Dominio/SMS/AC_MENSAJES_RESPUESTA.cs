﻿using System;

namespace Monibyte.Arquitectura.Dominio.SMS
{
    public partial class AC_MENSAJES_RESPUESTA : EntidadBase
    {
        public long ID { get; set; }
        public string NUMERO_RESPUESTA { get; set; }
        public Nullable<System.DateTime> FECHA_HORA { get; set; }
        public string DETALLE_MENSAJE { get; set; }
        public string RESPUESTA_MENSAJE { get; set; }
        public Nullable<int> IND_ESTADO { get; set; }
    }
}

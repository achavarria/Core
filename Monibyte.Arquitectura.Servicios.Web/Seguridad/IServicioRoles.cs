﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.Collections.Generic;
using Monibyte.Arquitectura.Poco.Seguridad;

namespace Monibyte.Arquitectura.Servicios.Web.Seguridad
{
    [ServiceContract]
    public interface IServicioRoles : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ExisteRol")]
        Transporte ExisteRol(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerRolesPorUsuario")]
        Transporte ObtenerRolesPorUsuario(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListaRoles")]
        Transporte ListaRoles(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerRolesTipoUsuario")]
        Transporte ObtenerRolesTipoUsuario(Transporte transporte);

    }
}

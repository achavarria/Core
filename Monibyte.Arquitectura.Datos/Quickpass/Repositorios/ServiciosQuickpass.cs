﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Dominio.Quickpass.Repositorios;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace Monibyte.Arquitectura.Datos.Quickpass.Repositorios
{
    public class ServiciosQuickpass : IServiciosQuickpass
    {
        string[] formatoFecha = { "ddMMyyyy" };
        string[] formatoFechaCaducidad = { "MMyyyy" };
        string[] formatoFechaHora = { "ddMMyyyyHHmm" };
        string[] formatoTags = { "yyyyMMddHHmmss" };
        string[] formatoHora = { "HHmmss" };

        public List<QP_Quickpass> ImportarListaQuickpass(string documento, string entidad)
        {
            var res = new List<QP_Quickpass>();
            var error = "";

            var wsQuickpass = new PeajeElectronicoETC.Ws_Peaje_ElectronicoSoapClient();
            var xmlQuickpass = wsQuickpass.TagsPorNumeroDocumento(entidad, documento);
      
            if (xmlQuickpass == "")
                throw new CoreException("Respuesta XML sin datos");
            else
            {
                XDocument listaQuickpass = new XDocument();
                int cantEnc = 0;
                int cantRegControl = 0;

                try
                {
                    listaQuickpass = XDocument.Parse(xmlQuickpass.ToString());
                    var encabezado = listaQuickpass.Descendants("Encabezado");
                    var registroControl = listaQuickpass.Descendants("RegistroControl");
                    cantEnc = encabezado.Count();
                    cantRegControl = registroControl.Count();
                }
                catch
                {
                    throw new CoreException("Estructura no válida");
                }

                if ((cantEnc == 1) && (cantRegControl == 1))
                {
                    var cantEntidad = listaQuickpass.Root.Element("Encabezado").Descendants("Identificador_Entidad").Count();
                    var cantNumDoc = listaQuickpass.Root.Element("Encabezado").Descendants("Numero_Documento").Count();
                    var cantEmiDoc = listaQuickpass.Root.Element("Encabezado").Descendants("Fecha_Emision_Documento").Count();
                    
                    if ((cantEntidad > 1) || (cantNumDoc > 1) || (cantEmiDoc > 1))
                        throw new CoreException("La respuesta posee más de un encabezado");
                    
                    XmlReaderSettings vr = new XmlReaderSettings();
                    vr.Schemas.Add("", HttpContext.Current.Server.MapPath("xsdListaTags.xsd"));
                    vr.ValidationType = ValidationType.Schema;
                    vr.ValidationEventHandler += delegate(object sender, ValidationEventArgs e)
                        {
                            throw new CoreException("Estructura no válida");
                        };
                    XmlReader reader = XmlReader.Create(new XmlTextReader(new System.IO.StringReader(xmlQuickpass)), vr);
                    while (reader.Read()) ;

                    var parEntidad = listaQuickpass.Root.Element("Encabezado").Element("Identificador_Entidad").Value;
                    var parFecha = listaQuickpass.Root.Element("Encabezado").Element("Fecha_Emision_Documento").Value;
                    
                    DateTime fecha;
                    int cantQP = 0;

                    if (parEntidad != entidad)
                        error = "Código de Entidad Inválido";

                    if (!DateTime.TryParseExact(parFecha, formatoFecha, new CultureInfo("en-US"), DateTimeStyles.None, out fecha))
                        error = "Fecha encabezado no válida";
                                        
                    if (error != "")
                    {
                        throw new CoreException(error);
                    }
                    else
                    {
                        foreach (XElement xe in listaQuickpass.Descendants("Detalle"))
                        {
                            string numQuickpass = xe.Element("Medio_Pago").Value;
                            string fecCaducidad = xe.Element("Fecha_Caducidad").Value;

                            if (!DateTime.TryParseExact(fecCaducidad, formatoFechaCaducidad, new CultureInfo("en-US"), DateTimeStyles.None, out fecha))
                                throw new CoreException("Fecha de Caducidad no válida");

                            var nuevo = new QP_Quickpass()
                            {
                                NumQuickpass = numQuickpass.Trim(),
                                FecVencimiento = fecha
                            };

                            res.Add(nuevo);
                            cantQP++;
                        }

                        var numOperaciones = int.Parse(listaQuickpass.Root.Element("Encabezado").Element("RegistroControl").Element("Numero_Operaciones").Value);
                        if (cantQP != numOperaciones)
                            throw new CoreException("Cantidad indicada distinta a cantidad en respuesta");
                    }

                }
                else
                {
                    if (cantEnc > 1)
                        throw new CoreException("La respuesta posee más de un encabezado");
                    if (cantRegControl > 1)
                        throw new CoreException("La respuesta posee más de un registro de control");
                }
            }
            return res;
        }

        public string ImportarMovimientosQuickpass(string entidad, DateTime fecInicio, DateTime fecFinal, int montoMaximo)
        {
            var wsQuickpass = new PeajeElectronicoETC.Ws_Peaje_ElectronicoSoapClient();

            //fecInicio = DateTime.Today.AddDays(2);
            //fecFinal = DateTime.Today.AddDays(3);//  Para pruebas

            var xmlQuickpass = wsQuickpass.ConsultaDeTransacciones(entidad, fecInicio, fecFinal);
           
            var error = "";
            var mov = new List<string>();

            if (xmlQuickpass != "")
            {
                XDocument listaMovs = new XDocument();
                int cantEnc = 0;
                int cantRegControl = 0;

                try
                {
                    listaMovs = XDocument.Parse(xmlQuickpass.ToString());
                    var encabezado = listaMovs.Descendants("Encabezado");
                    var registroControl = listaMovs.Descendants("RegistroControl");
                    cantEnc = encabezado.Count();
                    cantRegControl = registroControl.Count();
                }
                catch
                {
                    throw new CoreException("Estructura no válida");
                }
                DateTime fecha;

                if ((cantEnc == 1) && (cantRegControl == 1))
                {
                    var cantEntidad = listaMovs.Root.Element("Encabezado").Descendants("Identificador_Entidad").Count();
                    var cantNumDoc = listaMovs.Root.Element("Encabezado").Descendants("Fecha").Count();
                    var cantEmiDoc = listaMovs.Root.Element("Encabezado").Descendants("Hora").Count();
                    var cantDetalle = listaMovs.Root.Element("Encabezado").Descendants("Detalle").Count();
                    if (cantDetalle == 0)
                    {
                        return "";
                    }

                    if ((cantEntidad > 1) || (cantNumDoc > 1) || (cantEmiDoc > 1))
                        throw new CoreException("La respuesta posee más de un encabezado");

                    XmlReaderSettings vr = new XmlReaderSettings();
                    vr.Schemas.Add("", HttpContext.Current.Server.MapPath("xsdMovimientos.xsd"));
                    vr.ValidationType = ValidationType.Schema;
                    vr.ValidationEventHandler += delegate(object sender, ValidationEventArgs e)
                    {
                        throw new CoreException("Estructura no válida");
                    };
                    XmlReader reader = XmlReader.Create(new XmlTextReader(new System.IO.StringReader(xmlQuickpass)), vr);
                    while (reader.Read()) ;

                    var parEntidad = listaMovs.Root.Element("Encabezado").Element("Identificador_Entidad").Value;
                    var fechaEnc = listaMovs.Root.Element("Encabezado").Element("Fecha").Value;
                    var horaEnc = listaMovs.Root.Element("Encabezado").Element("Hora").Value;
                    var tipoControl = listaMovs.Root.Element("Encabezado").Element("RegistroControl").Element("Tipo").Value;

                    if (parEntidad != entidad)
                        error = "Código de Entidad Invalido";

                    if (!DateTime.TryParseExact(fechaEnc, formatoFecha, new CultureInfo("en-US"), DateTimeStyles.None, out fecha))
                        throw new CoreException("Fecha de encabezado no válida");

                    if (!DateTime.TryParseExact(horaEnc, formatoHora, new CultureInfo("en-US"), DateTimeStyles.None, out fecha))
                        throw new CoreException("Hora de encabezado no válida");


                    if (error != "")
                    {
                        throw new CoreException(error);
                    }
                    else
                    {
                        int cantMovs = 0;
                        decimal totalMovs = 0;

                        foreach (XElement xe in listaMovs.Descendants("Detalle"))
                        {
                            string tipoRegistro = xe.Element("Tipo_Registro").Value;
                            string numQuickpass = xe.Element("Medio_Pago").Value;
                            string idLocalizador = xe.Element("Localizador").Value;
                            string fecCaducidad = xe.Element("Fecha_Caducidad").Value;
                            string montoImporte = xe.Element("Importe").Value;
                            string fechahora = xe.Element("Fecha_Transito").Value;
                            string codMoneda = xe.Element("Moneda").Value;
                            string iva = xe.Element("IVA").Value;

                            int codigo;
                            decimal montoValidar;

                            if (!((tipoRegistro == "10") || (tipoRegistro == "11")))
                                throw new CoreException("Tipo de registro no válido");

                            if (tipoControl != "90")
                                throw new CoreException("Tipo de control no válido");

                            if ((!decimal.TryParse(montoImporte, out montoValidar)) || !IsDigitsOnly(montoImporte))
                                throw new CoreException("Monto no válido");

                            if (montoValidar / 100 > montoMaximo)
                                throw new CoreException("Monto mayor a monto máximo");

                            if (!int.TryParse(codMoneda, out codigo))
                                throw new CoreException("Moneda no válida");

                            if (codigo != 99)
                                throw new CoreException("Moneda no válida");

                            if (!int.TryParse(iva, out codigo))
                                throw new CoreException("IVA no válido");

                            if (!DateTime.TryParseExact(fechahora, formatoFechaHora, new CultureInfo("en-US"), DateTimeStyles.None, out fecha))
                                throw new CoreException("Fecha transito no válida");

                            if (!DateTime.TryParseExact(fecCaducidad, formatoFechaCaducidad, new CultureInfo("en-US"), DateTimeStyles.None, out fecha))
                                throw new CoreException("Fecha de Caducidad no válida");

                            string detalle = tipoRegistro + "," + numQuickpass + "," + fechahora + "," + idLocalizador + "," + montoImporte;
 
                            if (mov.Any(s => detalle.Contains(s)))
                                throw new CoreException("Movimiento repetido");

                            decimal importe = decimal.Parse(xe.Element("Importe").Value) / 100;
                            mov.Add(detalle);
                            cantMovs++;
                            if (tipoRegistro=="11")
                                totalMovs = totalMovs - importe;
                            else
                                totalMovs = totalMovs + importe;
                        }

                        var numOperaciones = int.Parse(listaMovs.Root.Element("Encabezado").Element("RegistroControl").Element("Numero_Operaciones").Value);
                        var totalImporte = listaMovs.Root.Element("Encabezado").Element("RegistroControl").Element("Importe_Total").Value;
                        decimal montoTotalImporte = 0;

                        if (!decimal.TryParse(totalImporte, out montoTotalImporte))
                            throw new CoreException("Monto no válido");

                        montoTotalImporte = montoTotalImporte / 100;

                        if (cantMovs != numOperaciones)
                            throw new CoreException("Cantidad indicada distinta a cantidad en respuesta");

                        if (montoTotalImporte != totalMovs)
                            throw new CoreException("Importe total distinto a importe en respuesta");

                    }
                }
                else
                {
                    if (cantEnc > 1)
                        throw new CoreException("La respuesta posee más de un encabezado");
                    if (cantRegControl > 1)
                        throw new CoreException("La respuesta posee más de un registro de control");
                }

            }            
            var res = xmlQuickpass;
                
            return res;
        }

        bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        public bool EnviarListasQP(string emisor, int tipoLista, List<string> listaQP, DateTime fechaGeneracion, int secGeneracion)
        {
            string xmlEntrada = "";
            int cantRegistros = 0;

            xmlEntrada = "<Encabezado>";
            xmlEntrada += "\n\t<Identificador_Entidad>" + emisor + "</Identificador_Entidad>";
            xmlEntrada += "\n\t<Fecha>" + fechaGeneracion.ToString("yyyyMMddHHmmss") + "</Fecha>";
            xmlEntrada += "\n\t<Tipo_Lista>" + tipoLista.ToString("00000") + "</Tipo_Lista>";
            xmlEntrada += "\n\t<Secuencia_Generacion>" + secGeneracion.ToString("00000") + "</Secuencia_Generacion>";
            xmlEntrada += "\n\t<Detalles>";
            foreach (string quickpass in listaQP)
            {
                xmlEntrada += "\n\t<Detalle>";
                xmlEntrada += "\n\t\t<Medio_Pago>" + "    " + quickpass + "</Medio_Pago>";
                xmlEntrada += "\n\t</Detalle>";
                cantRegistros++;
            }
            xmlEntrada += "\n\t</Detalles>";
            xmlEntrada += "\n\t<RegistroControl>";
            xmlEntrada += "\n\t\t<Numero_Operaciones>" + cantRegistros.ToString("000000000000") + "</Numero_Operaciones>";
            xmlEntrada += "\n\t</RegistroControl>";
            xmlEntrada += "\n</Encabezado>";

            var wsQuickpass = new PeajeElectronicoETC.Ws_Peaje_ElectronicoSoapClient();
            var xmlQuickpass = wsQuickpass.ListasDeTags(xmlEntrada);

            return xmlQuickpass;
        }

        public string ResultadoListasDeTags(string entidad, DateTime fechaGeneracion, int tipoLista, int secuencia)
        {
            string resXML = "";
            var wsQuickpass = new PeajeElectronicoETC.Ws_Peaje_ElectronicoSoapClient();
            var xmlQuickpass = wsQuickpass.ResultadoListasDeTags(entidad, fechaGeneracion, tipoLista, secuencia);

            if (xmlQuickpass != "")
            {
                DateTime fecha = DateTime.Today;
                XDocument listaTags = new XDocument();
                var cantRes = 0;

                try
                {
                    listaTags = XDocument.Parse(xmlQuickpass.ToString());
                    cantRes = listaTags.Root.Descendants("Resultado_Procesamiento").Count();
                }
                catch
                {
                    throw new CoreException("Estructura no válida");
                }

                if (cantRes > 1)
                    throw new CoreException("Resultado duplicado");

                XmlReaderSettings vr = new XmlReaderSettings();
                vr.Schemas.Add("", HttpContext.Current.Server.MapPath("xsdResultadoListas.xsd"));
                vr.ValidationType = ValidationType.Schema;
                vr.ValidationEventHandler += delegate(object sender, ValidationEventArgs e)
                {
                    throw new CoreException("Estructura no válida");
                };
                XmlReader reader = XmlReader.Create(new XmlTextReader(new System.IO.StringReader(xmlQuickpass)), vr);
                while (reader.Read()) ;

                var resultadoBloque = listaTags.Descendants("Resultado");
                var parEntidad = listaTags.Root.Element("Identificador_Entidad").Value;
                var fechaResultado = listaTags.Root.Element("Fecha").Value;
                var resultadoProc = listaTags.Root.Element("Resultado_Procesamiento").Value;
                var resultadoDesc = listaTags.Root.Element("Resultado_Descripcion").Value;
                var secGeneracion = listaTags.Root.Element("Secuencia_Generacion").Value;
                int resultadoConsulta = 0;

                if (parEntidad != entidad)
                    throw new CoreException("Código de Entidad Inválido");

                if (resultadoBloque.Count() > 1)
                    throw new CoreException("Resultado duplicado");

                if (!int.TryParse(resultadoProc, out resultadoConsulta))
                    throw new CoreException("Resultado no válido");

                if ((resultadoConsulta < 0) || (resultadoConsulta > 2))
                    throw new CoreException("Resultado no válido");

                if (!DateTime.TryParseExact(fechaResultado, formatoTags, new CultureInfo("en-US"), DateTimeStyles.None, out fecha))
                    throw new CoreException("Fecha/Hora no válido");

                if (!int.TryParse(secGeneracion, out resultadoConsulta))
                    throw new CoreException("Secuencia no válida");

                resXML = xmlQuickpass;
            }
            else
                throw new CoreException("Respuesta vacia");

            return resXML;
        }
    }
}
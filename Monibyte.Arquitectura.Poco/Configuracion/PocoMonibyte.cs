﻿using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.Configuracion
{
    public class PocTarjetaEmpresa
    {
        public int IdEmpresa { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreImpreso { get; set; }
        public int? IdGrupoEmpresa { get; set; }
        public string DescripcionGrupo { get; set; }
        public bool EsAutorizacion { get; set; }
        public bool Agrupada { get; set; }
    }

    public class PocAgrupacionTarjetas
    {
        public int IdGrupoEmpresa { get; set; }
        public List<PocAgrupacionTarjetasItem> TarjetasIncluir { get; set; }
        public List<PocAgrupacionTarjetasItem> TarjetasExcluir { get; set; }
    }

    public class PocAgrupacionTarjetasItem
    {
        public int IdTarjeta { get; set; }
        public int IdGrupoEmpresa { get; set; }
    }

    public class PocConsultaGrupoEmpresa
    {
        public int IdUsuario { get; set; }
        public int IdCuenta { get; set; }
        public int IdEmpresa { get; set; }
        public int? IdGrupoEmpresa { get; set; }
    }

    public class PocGruposEmpresa
    {
        public int IdGrupo { get; set; }
        public int IdEstado { get; set; }
        public int IdEmpresa { get; set; }
        public int IdTipoGrupo { get; set; }
        public string DescripcionEstado { get; set; }
        public string Descripcion { get; set; }
        public string Observaciones { get; set; }
        public decimal? LimiteCredito { get; set; }
        public bool Autoriza { get; set; }
        public bool SoloConsulta { get; set; }
        public PocCondicion Configuracion { get; set; }
    }

    public class PocGrupoDuplica
    {
        public int IdGrupoEmpresa { get; set; }
        public string NombreNuevo { get; set; }
    }

    public class PocUsuarioEmpresa
    {
        public int IdEmpresa { get; set; }
        public int IdUsuario { get; set; }
        public int IdEstado { get; set; }
        public string CodUsuario { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool EsAutorizacion { get; set; }
        public bool EsAdministrador { get; set; }
        public bool Autoriza { get; set; }
        public bool SoloConsulta { get; set; }
    }

    public class PocGrupoEmpresaCatalogo
    {
        public int? IdGrupo { get; set; }
        public int? IdEstado { get; set; }
        public string Descripcion { get; set; }
        public decimal LimiteGrupo { get; set; }
        public decimal LimiteCuenta { get; set; }
    }

    public class PocTarjetaGrupoCatalogo
    {
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdTipoPlastico { get; set; }
        public string NombreImpreso { get; set; }
        public int IdRelacion { get; set; }
        public string NumTarjeta { get; set; }
        public int? IdEstadoTarjeta { get; set; }
        public decimal? LimiteCreditoLocal { get; set; }
        public decimal? LimiteCreditoInter { get; set; }
        public int? IdGrupoEmpresa { get; set; }
        public string Descripcion { get; set; }
        public bool Autoriza { get; set; }
        public bool SoloConsulta { get; set; }
        public bool AutorizacionPendiente { get; set; }
    }

    public class PocLicenciaPersona
    {
        public int IdPersona { get; set; }
        public int IdTipoLicencia { get; set; }
        public int IdEstado { get; set; }
        public string DescEstado { get; set; }
        public string NombrePersona { get; set; }
        public string DescLicencia { get; set; }
    }

    public class PocTipoLicencia
    {
        public int IdTipoLicencia { get; set; }
        public int IdTipoCargo { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public string DescTipoCargo { get; set; }
    }

    public class PocConsultaTarjetasAdmin
    {
        public int? IdCuenta { get; set; }
        public int? IdEmpresa { get; set; }
        public int? IdPersona { get; set; }
        public int? IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreCliente { get; set; }
        public string NombreEmpresa { get; set; }
        public string NumIdentificacionPersona { get; set; }
        public string NumIdentificacionEmpresa { get; set; }
    }

    public class PocTarjetasAdministrativo
    {
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int? IdPersonaTitular { get; set; }
        public string NombreImpreso { get; set; }
        public string NumTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public int? IdEstadoTarjeta { get; set; }
        public string DescEstadoTarjeta { get; set; }
        public int? IdEstadoCuenta { get; set; }
        public string DescEstadoCuenta { get; set; }
        public int? IdGrupoEmpresa { get; set; }
        public string DescGrupoEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
        public string Correo { get; set; }
        public string Texto { get; set; }

        public int IdTipoPlastico { get; set; }
        public int IdTipoTarjeta { get; set; }
        public int IdRelacion { get; set; }
        public decimal? LimiteCreditoLocal { get; set; }
        public decimal? LimiteCreditoInter { get; set; }

        public DateTime FecActivacion { get; set; }
        public DateTime FecVencimiento { get; set; }

        public int DiaCorte { get; set; }
    }
}
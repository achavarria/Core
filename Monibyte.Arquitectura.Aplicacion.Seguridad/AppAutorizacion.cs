﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Poco.Seguridad;
using System;
using System.Linq;
using System.Transactions;
using Monibyte.Arquitectura.Dominio.Generales;
using System.Collections.Generic;
using Monibyte.Arquitectura.Poco.Configuracion;
using Newtonsoft.Json.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public class AppAutorizacion : AplicacionBase, IAppAutorizacion
    {
        #region CONSTRUCTOR UNITY
        private IRepositorioEmail _repEmail;
        private IRepositorioUsuario _repUsuario;
        private IRepositorioCatalogo _repCatalogo;
        private IFuncionesGenerales _funcGenerales;
        private IDirectorioUsuarioApp _dirUsuarioApp;
        private IRepositorioMnb<SC_Autorizacion> _repAutorizacion;
        private IRepositorioPlantillaNotificacion _repPlantillaNot;
        private IRepositorioMnb<CL_EmpresasUsuario> _repEmpresasUsr;

        public AppAutorizacion(
            IRepositorioEmail repEmail,
            IRepositorioCatalogo repCatalogo,
            IFuncionesGenerales funcGenerales,
            IDirectorioUsuarioApp dirUsuarioApp,
            IRepositorioMnb<SC_Autorizacion> repAutorizacion,
            IRepositorioMnb<CL_EmpresasUsuario> repEmpresasUsr)
        {
            _repEmail = repEmail;
            _repCatalogo = repCatalogo;
            _dirUsuarioApp = dirUsuarioApp;
            _funcGenerales = funcGenerales;
            _repEmpresasUsr = repEmpresasUsr;
            _repAutorizacion = repAutorizacion;
            _repUsuario = GestorUnity.CargarUnidad<IRepositorioUsuario>();
            _repPlantillaNot = GestorUnity.CargarUnidad<IRepositorioPlantillaNotificacion>();
        }
        #endregion

        public int? PrepararAutorizacion(int tipoAutorizacion, string numReferencia, string detalleAutorizacion)
        {
            var autorizacion = new SC_Autorizacion();
            autorizacion.IdEmpresa = InfoSesion.Info.IdEmpresa;
            autorizacion.IdTipoAutorizacion = tipoAutorizacion;
            autorizacion.NumReferencia = numReferencia;
            autorizacion.DetalleAutorizacion = detalleAutorizacion;
            autorizacion.IdEstado = (int)EnumEstadoAutorizacion.Preparada;
            autorizacion.FecPrepara = DateTime.Now;
            autorizacion.IdUsuarioPrepara = InfoSesion.Info.IdUsuario;

            var infoAutorizacion = InfoSesion.ObtenerSesion<PocInfoAutorizacion>("INFOAUTORIZACION");
            autorizacion.Descripcion = infoAutorizacion != null ? infoAutorizacion.DescripcionProducto : null;
            autorizacion.Observaciones = infoAutorizacion.DetalleAutorizacion;
            ValidarAutorizacion(autorizacion);

            var idAutorizacion = ObtieneAutorizacionPendiente(tipoAutorizacion, numReferencia);

            if (idAutorizacion > 0)
            {
                if (tipoAutorizacion == (int)EnumTipoAutorizacion.RestriccionesGrupo ||
                    tipoAutorizacion == (int)EnumTipoAutorizacion.GrupoOperativo)
                {
                    var actualAut = _repAutorizacion.SelectById(idAutorizacion);
                    var data = actualAut.DetalleAutorizacion.FromJson<IDictionary<string, object>>();

                    if (actualAut.DetalleAutorizacion.Contains("EsNuevo"))
                    {
                        var dynamicData = detalleAutorizacion.MergeJson("{" + "EsNuevo" + ": true }") as IDictionary<string, object>;
                        autorizacion.DetalleAutorizacion = StringExt.ToJson(dynamicData);
                    }
                    else if (actualAut.DetalleAutorizacion.Contains("ViejoNombre"))
                    {
                        var stringData = new { ViejoNombre = data["ViejoNombre"].ToString() }.ToJson();
                        if (actualAut.DetalleAutorizacion.Contains("ViejoMonto"))
                        {
                            var limite = (double)data["ViejoMonto"];
                            var limteR = limite.ToString("R");
                            var enDecimal = decimal.Parse(limteR);

                            stringData = new
                            {
                                ViejoNombre = data["ViejoNombre"].ToString(),
                                ViejoMonto = enDecimal
                            }.ToJson();
                        }
                        var dynamicData = detalleAutorizacion.MergeJson(stringData) as IDictionary<string, object>;
                        autorizacion.DetalleAutorizacion = StringExt.ToJson(dynamicData);
                    }
                }

                autorizacion.IdAutorizacion = idAutorizacion.Value;
                _repAutorizacion.Update(autorizacion,
                    up => up.DetalleAutorizacion,
                    up => up.Observaciones,
                    up => up.FecPrepara,
                    up => up.IdUsuarioPrepara);
            }
            else
            {
                autorizacion.IdAutorizacion = _fGenerales.ObtieneSgteSecuencia("SEQ_IdAutorizacion");
                _repAutorizacion.Insert(autorizacion);
            }
            _repAutorizacion.UnidadTbjo.Save();
            NotificaAutorizacionPendiente(tipoAutorizacion, numReferencia, infoAutorizacion);
            return autorizacion.IdAutorizacion;
        }

        private void ValidarAutorizacion(SC_Autorizacion autorizacion)
        {
            var validation = new ValidationResponse();
            if (string.IsNullOrEmpty(autorizacion.DetalleAutorizacion))
            {
                validation.AddError("Err_0084");
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Error al validar la autorización", validation: validation);
            }
        }

        public string ObtieneDetalleAutorizacion(int tipoAutorizacion,
            string numReferencia, int? idTipoGrupo = null)
        {
            if (tipoAutorizacion != (int)EnumTipoAutorizacion.AgruparTarjetas)
            {
                var criteria = new Criteria<SC_Autorizacion>();
                criteria.And(m => m.NumReferencia == numReferencia &&
                                  m.IdTipoAutorizacion == tipoAutorizacion &&
                                  m.IdEstado == (int)EnumEstadoAutorizacion.Preparada);
                //_repAutorizacion.SelectBy(criteria);
                var autorizacion = _repAutorizacion.Table.AsQueryable()
                    .FirstOrDefault(criteria.Satisfecho());
                if (autorizacion != null)
                {
                    return autorizacion.DetalleAutorizacion;
                }
            }
            else
            {
                var listTC = new List<PocTarjetaEmpresa>();
                var criteria = new Criteria<SC_Autorizacion>();
                if (idTipoGrupo == (int)EnumTipoGrupo.Operativo)
                {
                    criteria.And(m => m.IdEmpresa == InfoSesion.Info.IdEmpresa &&
                                    m.NumReferencia == numReferencia &&
                                    m.IdTipoAutorizacion == tipoAutorizacion &&
                                    m.IdEstado == (int)EnumEstadoAutorizacion.Preparada);
                }
                else
                {
                    criteria.And(m => m.IdEmpresa == InfoSesion.Info.IdEmpresa &&
                                      m.IdTipoAutorizacion == tipoAutorizacion &&
                                      m.IdEstado == (int)EnumEstadoAutorizacion.Preparada);
                }

                var otrasAut = _repAutorizacion.Table.Where(criteria.Satisfecho());

                //Agregar la explucion de agrupacion y grupo empresa a las que no son del grupo de consulta
                foreach (var au in otrasAut)
                {
                    var tarjetas = au.DetalleAutorizacion.FromJson<List<PocTarjetaEmpresa>>();
                    tarjetas.Update(x => x.IdGrupoEmpresa = Int32.Parse(au.NumReferencia));
                    listTC.AddRange(tarjetas);
                }
                return listTC.Any() ? listTC.ToJson() : null;
            }
            return null;
        }

        public int? ObtieneAutorizacionPendiente(int? tipoAutorizacion, string numReferencia)
        {
            var idAutorizacion = _repAutorizacion.Table
                .Where(m => m.NumReferencia == numReferencia &&
                   (m.IdTipoAutorizacion == tipoAutorizacion || !tipoAutorizacion.HasValue) &&
                    m.IdEstado == (int)EnumEstadoAutorizacion.Preparada)
                .Select(s => s.IdAutorizacion).FirstOrDefault();
            return (idAutorizacion > 0) ? idAutorizacion as int? : null;
        }

        public int AprobarAutorizacion(int tipoAutorizacion, string numReferencia)
        {
            var aplicaAutorizacion = InfoSesion.ObtenerSesion<bool?>("APLICAAUTORIZACION");
            if (aplicaAutorizacion.HasValue && aplicaAutorizacion.Value)
            {
                var infoAutorizacion = InfoSesion.ObtenerSesion<PocInfoAutorizacion>("INFOAUTORIZACION");
                var datAutorizacion = _repAutorizacion.Table.Where(w =>
                    w.NumReferencia == numReferencia &&
                    w.IdTipoAutorizacion == tipoAutorizacion &&
                    w.IdEstado == (int)EnumEstadoAutorizacion.Preparada)
                    .FirstOrDefault();
                if (datAutorizacion != null)
                {
                    using (var ts = new TransactionScope())
                    {
                        _repAutorizacion.Update(new SC_Autorizacion
                        {
                            IdAutorizacion = datAutorizacion.IdAutorizacion,
                            IdEstado = (int)EnumEstadoAutorizacion.Autorizada,
                            Observaciones = infoAutorizacion != null ? infoAutorizacion.DetalleAutorizacion : null,
                            IdUsuarioAutoriza = InfoSesion.Info.IdUsuario,
                            FecAutoriza = DateTime.Now
                        },
                        up => up.IdEstado,
                        up => up.Observaciones,
                        up => up.IdUsuarioAutoriza,
                        up => up.FecAutoriza);
                        _repAutorizacion.UnidadTbjo.Save();
                        ts.Complete();
                    }
                    InfoSesion.IncluirSesion("APLICAAUTORIZACION", false);
                    //Enviar correo a quién preparó la autorización
                    NotificaAutorizacion(tipoAutorizacion, datAutorizacion,
                        (int)EnumContexto.APROBAR_AUTORIZACION, infoAutorizacion);
                }
            }
            return 0;
        }

        public int RechazarAutorizacion(int tipoAutorizacion, string numReferencia, PocInfoAutorizacion infoAutorizacion)
        {
            if (infoAutorizacion == null)
            {
                var infoEnSesion = InfoSesion.ObtenerSesion<PocInfoAutorizacion>("INFOAUTORIZACION");
                if (infoEnSesion != null)
                {
                    infoAutorizacion = infoEnSesion;
                }
            }

            var datAutorizacion = _repAutorizacion.Table.Where(w =>
                w.IdTipoAutorizacion == tipoAutorizacion &&
                w.NumReferencia == numReferencia &&
                (w.IdEstado == (int)EnumEstadoAutorizacion.Preparada
                || w.IdEstado == (int)EnumEstadoAutorizacion.Anulada))
                .OrderBy(x => x.IdEstado).FirstOrDefault();
            if (datAutorizacion != null)
            {
                if (datAutorizacion.IdEstado != (int)EnumEstadoAutorizacion.Anulada)
                {
                    using (var ts = new TransactionScope())
                    {
                        _repAutorizacion.Update(new SC_Autorizacion
                        {
                            IdAutorizacion = datAutorizacion.IdAutorizacion,
                            IdEstado = (int)EnumEstadoAutorizacion.Denegada,
                            Observaciones = infoAutorizacion != null ? infoAutorizacion.DetalleAutorizacion : null,
                            IdUsuarioAutoriza = InfoSesion.Info.IdUsuario,
                            FecAutoriza = DateTime.Now
                        },
                        up => up.IdEstado,
                        up => up.Observaciones,
                        up => up.IdUsuarioAutoriza,
                        up => up.FecAutoriza);
                        _repAutorizacion.UnidadTbjo.Save();
                        ts.Complete();
                    }
                }

                var tiposDependientes = _repCatalogo.Table.Where(x => x.Lista == "LTIPOAUTORIZACION"
                    && x.Referencia2.Contains(datAutorizacion.IdTipoAutorizacion.ToString()))
                    .Select(x => x.IdCatalogo).ToList();
                if (tiposDependientes.Any() && tiposDependientes.Count > 0)
                {

                    var dependecias = _repAutorizacion.Table.Where(w =>
                       tiposDependientes.Contains(w.IdTipoAutorizacion) &&
                       w.NumReferencia == numReferencia &&
                       w.IdEstado == (int)EnumEstadoAutorizacion.Preparada);

                    foreach (var item in dependecias)
                    {
                        RechazarAutorizacion(item.IdTipoAutorizacion, item.NumReferencia,
                            new PocInfoAutorizacion
                            {
                                DetalleAutorizacion = infoAutorizacion != null ?
                                    infoAutorizacion.DetalleAutorizacion : null
                            });
                    }
                }
            }

            //Enviar correo a quién preparó la autorización
            if (datAutorizacion != null)
            {
                NotificaAutorizacion(tipoAutorizacion, datAutorizacion, (int)EnumContexto.RECHAZAR_AUTORIZACION, infoAutorizacion);
            }
            return 1;
        }

        private void NotificaAutorizacion(int tipoAutorizacion, SC_Autorizacion datAutorizacion,
            int idContextoNotificacion, PocInfoAutorizacion infoAutorizacion)
        {
            var remitente = _fGenerales.ObtieneParametroCompania("REMITENTE_AUTORIZACIONES",
                InfoSesion.Info.IdCompania);

            SenderConfig sender = new SenderConfig(remitente, null);
            var notificacion = _repPlantillaNot.ObtenerPlantillaNot(
                idContextoNotificacion, tipoAutorizacion);
            var infoTipoAutorizacion = _repCatalogo.ListSp("LTIPOAUTORIZACION", tipoAutorizacion);
            var asunto = StringExt.ClearFormat(notificacion.Asunto, infoTipoAutorizacion[0].Descripcion);
            var mensaje = StringExt.ClearFormat(notificacion.Cuerpo, infoTipoAutorizacion[0].Descripcion,
                InfoSesion.Info.NombreCompleto, infoAutorizacion != null ? infoAutorizacion.DescripcionProducto : null
                , infoAutorizacion != null ? infoAutorizacion.DetalleAutorizacion : null);
            EmailConfig emailData = new EmailConfig(asunto, mensaje);
            var user = _repUsuario.Table.Where(w => w.IdUsuario == datAutorizacion.IdUsuarioPrepara).FirstOrDefault();
            var usuario = _dirUsuarioApp.ObtenerCuenta(user.CodUsuario);
            emailData.AddTo(usuario.Correo);
            _repEmail.EnviarEmail(sender, emailData);
        }

        private void NotificaAutorizacionPendiente(int tipoAutorizacion, string numReferencia, PocInfoAutorizacion detalleAutorizacion)
        {
            var remitente = _funcGenerales.ObtieneParametroCompania
                ("REMITENTE_AUTORIZACIONES", InfoSesion.Info.IdCompania);

            SenderConfig sender = new SenderConfig(remitente, null);
            var notificacion = _repPlantillaNot.ObtenerPlantillaNot(
                (int)EnumContexto.PREPARAR_AUTORIZACION, tipoAutorizacion);

            var criteriaCatalogo = new Criteria<GL_Catalogo>();
            criteriaCatalogo.And(c => c.Lista == "LTIPOAUTORIZACION");
            criteriaCatalogo.And(c => c.IdCatalogo == tipoAutorizacion);

            var infoTipoAutorizacion = _repCatalogo.List(criteriaCatalogo).ToList();

            var asunto = StringExt.ClearFormat(notificacion.Asunto, infoTipoAutorizacion[0].Descripcion);
            var mensaje = StringExt.ClearFormat(notificacion.Cuerpo, infoTipoAutorizacion[0].Descripcion,
                InfoSesion.Info.NombreCompleto, numReferencia, detalleAutorizacion.DescripcionProducto,
                detalleAutorizacion.DetalleAutorizacion);
            EmailConfig emailData = new EmailConfig(asunto, mensaje);

            var criteria = new Criteria<CL_EmpresasUsuario>();

            criteria.And(x => x.IdTipoUsuario == (int)EnumTipoUsuario.Administrador);
            criteria.And(x => (x.IdRoleOperativo == (int)EnumRoleOperativo.Autorizador || x.IdRoleOperativo == null));
            criteria.And(x => x.IdEmpresa == InfoSesion.Info.IdEmpresa);
            var admins = _repEmpresasUsr.SelectBy(criteria).ToList()
                            .Join(_repUsuario.Table,
                                eu => eu.IdUsuario,
                                u => u.IdUsuario, (eu, u) => new { eu, u })
                            .Select(x => new { x.u.Correo });
            if (admins != null && admins.Any())
            {
                var enviarCorreos = string.Join(";", admins.Select(p => p.Correo));
                emailData.AddTo(enviarCorreos); //Correo del administradores
                _repEmail.EnviarEmail(sender, emailData);
            }
        }

        public void AnularAutorizacionesGrupo(string numReferencia)
        {
            _repAutorizacion.UpdateOn(w =>
                w.NumReferencia == numReferencia &&
                w.IdEmpresa == InfoSesion.Info.IdEmpresa &&
                w.IdEstado == (int)EnumEstadoAutorizacion.Preparada,
                up => new SC_Autorizacion
                {
                    IdEstado = (int)EnumEstadoAutorizacion.Anulada,
                    Observaciones = "",
                    IdUsuarioAutoriza = InfoSesion.Info.IdUsuario,
                    FecAutoriza = DateTime.Now
                });
        }

        public int AnularAutorizacion(int tipoAutorizacion, string numReferencia, PocInfoAutorizacion infoAutorizacion)
        {
            return _repAutorizacion.UpdateOn(w =>
                w.IdTipoAutorizacion == tipoAutorizacion &&
                w.NumReferencia == numReferencia &&
                w.IdEstado == (int)EnumEstadoAutorizacion.Preparada,
                up => new SC_Autorizacion
                {
                    IdEstado = (int)EnumEstadoAutorizacion.Anulada,
                    Observaciones = infoAutorizacion != null ? infoAutorizacion.DetalleAutorizacion : null,
                    IdUsuarioAutoriza = InfoSesion.Info.IdUsuario,
                    FecAutoriza = DateTime.Now
                });
        }

        public int EliminarAutorizacion(int tipoAutorizacion, string numReferencia)
        {
            return _repAutorizacion.DeleteOn(w =>
                w.IdTipoAutorizacion == tipoAutorizacion &&
                w.NumReferencia == numReferencia);
        }

        public bool ExisteDependenciaPendiente(int idTipoAutorizacion, string numReferencia)
        {
            var catalogo = _repCatalogo.SelectById(idTipoAutorizacion);
            if (catalogo != null && !string.IsNullOrEmpty(catalogo.Referencia2))
            {
                var autorizacion = _repAutorizacion.Table
                    .Where(m => m.NumReferencia == numReferencia &&
                       catalogo.Referencia2.Contains(m.IdTipoAutorizacion.ToString()) &&
                        m.IdEstado == (int)EnumEstadoAutorizacion.Preparada)
                    .Select(s => new { Detalle = s.DetalleAutorizacion, Decripcion = s.Descripcion }).FirstOrDefault();
                if (autorizacion != null)
                {
                    if (autorizacion.Detalle.Contains("EsNuevo"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool EsNuevoGrupo(string numReferencia)
        {
            var grupo = _repAutorizacion.Table.FirstOrDefault(
                x => x.NumReferencia == numReferencia &&
                x.IdEstado == (int)EnumEstadoAutorizacion.Preparada &&
                (x.IdTipoAutorizacion == (int)EnumTipoAutorizacion.RestriccionesGrupo ||
                x.IdTipoAutorizacion == (int)EnumTipoAutorizacion.GrupoOperativo));

            if (grupo != null && grupo.DetalleAutorizacion.Contains("EsNuevo"))
            {
                return true;
            }
            return false;
        }

        public bool PermiteEditar(string numReferencia)
        {
            var grupo = _repAutorizacion.Table.FirstOrDefault(
                x => x.NumReferencia == numReferencia &&
                x.IdEstado == (int)EnumEstadoAutorizacion.Preparada);

            if (grupo != null)
            {
                return grupo.IdUsuarioPrepara == InfoSesion.Info.IdUsuario;
            }
            return true;
        }

        public void EliminarPendientes(string numReferencia, PocInfoAutorizacion infoAutorizacion)
        {
            using (var ts = new TransactionScope())
            {
                _repAutorizacion.UpdateOn(w =>
                    w.NumReferencia == numReferencia &&
                    w.IdEstado == (int)EnumEstadoAutorizacion.Preparada,
                    up => new SC_Autorizacion
                    {
                        IdEstado = (int)EnumEstadoAutorizacion.Anulada,
                        Observaciones = infoAutorizacion != null ? infoAutorizacion.DetalleAutorizacion : null,
                        IdUsuarioAutoriza = InfoSesion.Info.IdUsuario,
                        FecAutoriza = DateTime.Now
                    });
                _repAutorizacion.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public SC_Autorizacion ObtenerAutorizacion(int idTipoAutorizacion, string numReferencia)
        {
            var criteria = new Criteria<SC_Autorizacion>();
            criteria.And(m => m.NumReferencia == numReferencia &&
                              m.IdTipoAutorizacion == idTipoAutorizacion &&
                              m.IdEstado == (int)EnumEstadoAutorizacion.Preparada);
            var autorizacion = _repAutorizacion.Table.AsQueryable()
                .FirstOrDefault(criteria.Satisfecho());
            if (autorizacion != null)
            {
                return autorizacion;
            }
            return null;
        }

        public string ObtieneObervacion(int idTipoAutorizacion, string numReferencia)
        {
            var autorizacion = ObtenerAutorizacion(idTipoAutorizacion, numReferencia);
            return autorizacion != null ? autorizacion.Observaciones : string.Empty;
        }
    }
}

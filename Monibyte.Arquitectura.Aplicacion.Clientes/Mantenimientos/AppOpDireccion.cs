﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Clientes;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;


namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public class AppOpDireccion : AplicacionBase, IAppOpDireccion
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CL_Direccion> _repDireccion;
        private IRepositorioMnb<GL_Provincia> _repProvincia;
        private IRepositorioMnb<GL_Canton> _repCanton;
        private IRepositorioMnb<GL_Distrito> _repDistrito;

        public AppOpDireccion(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CL_Direccion> repDireccion,
            IRepositorioMnb<GL_Provincia> repProvincia,
            IRepositorioMnb<GL_Canton> repCanton,
            IRepositorioMnb<GL_Distrito> repDistrito)
        {
            _repCatalogo = repCatalogo;
            _repDireccion = repDireccion;
            _repProvincia = repProvincia;
            _repCanton = repCanton;
            _repDistrito = repDistrito;
        }

        public void Eliminar(PocDireccion registro)
        {
            using (var ts = new TransactionScope())
            {
                if (registro != null && registro.IdPersona > 0)
                {
                    _repDireccion.DeleteOn(x => x.IdDireccion == registro.IdDireccion);
                    _repDireccion.UnidadTbjo.Save();
                }
                ts.Complete();
            }
        }

        public void Insertar(PocDireccion registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CL_Direccion>();
                    entrada.IdDireccion = _fGenerales.ObtieneSgteSecuencia("SEQ_IdDireccion");
                    entrada.IdEstado = (int)EnumIdEstado.Activo;
                    if (entrada.IdOrigenProcesador == 0)
                        entrada.IdOrigenProcesador = (int)EnumIdSiNo.No;
                    _repDireccion.Insert(entrada);
                    _repDireccion.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocDireccion));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocDireccion registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CL_Direccion>();
                    _repDireccion.Update(entrada, m => m.IdPais, m => m.IdProvincia,
                        m => m.IdCanton, m => m.IdDistrito, m => m.Direccion,
                        m => m.IdEstado, m => m.IdTipo);
                    _repDireccion.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocDireccion));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        private ValidationResponse Validar(PocDireccion registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.IdPersona == 0)
            {
                validacion.AddError("Err_0027");
            }
            if (registro.IdTipo == 0 || registro.IdPais == 0 || registro.IdProvincia == 0 ||
                registro.IdCanton == 0 || registro.IdDistrito == 0 || string.IsNullOrEmpty(registro.Direccion))
            {
                validacion.AddError("Err_0035");
            }
            else if (!inserta)
            {
                if (registro.IdDireccion == 0)
                {
                    validacion.AddError("Err_0035");
                }
            }
            return validacion;
        }

        public List<PocDireccion> ObtenerPorPersona(int idPersona)
        {
            var criteria = new Criteria<CL_Direccion>();
            criteria.And(m => m.IdPersona == idPersona);
            return ObtenerDirecciones(criteria).ToList();
        }

        private IEnumerable<PocDireccion> ObtenerDirecciones(Criteria<CL_Direccion> criteria)
        {
            var query =
                from dir in _repDireccion.SelectBy(criteria)
                join pro in _repProvincia.Table on
                    dir.IdProvincia equals pro.IdProvincia into provs
                from pro in provs
                join can in _repCanton.Table on
                    dir.IdCanton equals can.IdCanton into cants
                from can in cants
                join dis in _repDistrito.Table on
                    dir.IdDistrito equals dis.IdDistrito into dists
                from dis in dists
                join cat in _repCatalogo.List("LUBICACION") on
                    dir.IdTipo equals cat.IdCatalogo
                select new PocDireccion
                {
                    IdDireccion = dir.IdDireccion,
                    DescripcionTipo = cat.Descripcion,
                    IdTipo = dir.IdTipo,
                    IdPais = dir.IdPais ?? 0,
                    IdProvincia = dir.IdProvincia ?? 0,
                    Provincia = pro != null ? pro.Descripcion : null,
                    IdCanton = dir.IdCanton ?? 0,
                    Canton = can != null ? can.Descripcion : null,
                    IdDistrito = dir.IdDistrito ?? 0,
                    Distrito = dis != null ? dis.Descripcion : null,
                    Direccion = dir.Direccion,
                    IdEstado = dir.IdEstado ?? 0,
                    IdPersona = dir.IdPersona ?? 0
                };
            return query.ToList();
        }

        public void EliminarPorPersona(int idPersona)
        {
            _repDireccion.DeleteOn(x => x.IdPersona == idPersona);
            _repDireccion.UnidadTbjo.Save();
        }
    }
}


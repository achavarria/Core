using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Procesador
{
    public partial class PT_AutorizacionesPBA : EntidadBase
    {
        public int Id { get; set; }
        public string MessageTypeId { get; set; }
        public string PrimaryBitMap { get; set; }
        public string SecondaryBitMap { get; set; }
        public string PrimaryNumberAccount { get; set; }
        public string ProcessingCode { get; set; }
        public Nullable<decimal> TransactionAmount { get; set; }
        public string TransmissionDateTime { get; set; }
        public Nullable<decimal> ConversionRateCb { get; set; }
        public string SystemTraceAuditNumber { get; set; }
        public string LocalTransactionTime { get; set; }
        public string LocalTransactionDate { get; set; }
        public string SettlementDate { get; set; }
        public string CaptureDate { get; set; }
        public string MerchantCategory { get; set; }
        public string AcquiringCountryCode { get; set; }
        public string PointOfServiceEntryMode { get; set; }
        public string PosConditionCode { get; set; }
        public string AcquiringId { get; set; }
        public string Track2Data { get; set; }
        public string RetrievalNumberReference { get; set; }
        public string CardAcceptorTerminalId { get; set; }
        public string CardAcceptorIdentificationCode { get; set; }
        public string CardAcceptorNameLocation { get; set; }
        public string AdditionalRetailerData { get; set; }
        public string TransactionCurrencyCode { get; set; }
        public string CurrencyCodeCb { get; set; }
        public string AtmTerminalData { get; set; }
        public string AtmCardIssuerAuthorizerData { get; set; }
        public string ReceivingIdCode { get; set; }
        public string AtmTerminalAddressBranchReg { get; set; }
        public string AtmAdditionalData { get; set; }
        public string ResponseCode { get; set; }
        public string AuthorizationIdResponse { get; set; }
        public string AccountIdentification1 { get; set; }
        public string AccountIdentification2 { get; set; }
        public Nullable<decimal> CardholderBillingAmount { get; set; }
        public string OriginalDataElements { get; set; }
        public string ReplacementAmounts { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string CardNumber { get; set; }
        public Nullable<int> DenegationId { get; set; }
        public string DenegationDetail { get; set; }
        public string InputParameter { get; set; }
    }
}

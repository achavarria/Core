﻿using Monibyte.Arquitectura.Aplicacion.Procesador;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;
using Monibyte.Arquitectura.Poco.Procesador;
using System.Collections.Generic;
namespace Monibyte.Arquitectura.Servicios.Web.Procesador
{
    public class ServiciosProcesador : ServicioUnity, IServiciosProcesador
    {
        private IAppAutorizaciones _autorizaciones;
        private IAppReportes _appAutorizaciones;

        public ServiciosProcesador(IAppAutorizaciones autorizaciones,
            IAppReportes appAutorizaciones)
        {
            _autorizaciones = autorizaciones;
            _appAutorizaciones = appAutorizaciones;
        }

        public Transporte ObetenerDenegaciones(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocFiltroAutorizaciones>();
            var resultado = _autorizaciones.ObetenerDenegaciones(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EnviarReporteEmpresa(Transporte transporte)
        {
            var entrada = transporte.GetDatos<List<PocDenegacionesDiarias>>();
            _appAutorizaciones.EnviarReporteEmpresa(entrada);
            return transporte;
        }

        public Transporte ObtieneReporteDenegacion(Transporte transporte)
        {
            var idEmpresa = transporte.GetValor<int?>("idEmpresa");
            var numTarjeta = transporte.GetValor<string>("numTarjeta");
            var numCuenta = transporte.GetValor<string>("numCuenta");
            var usuario = transporte.GetValor<string>("usuario");
            var fecDesde = transporte.GetValor<DateTime>("fecDesde");
            var fecHasta = transporte.GetValor<DateTime>("fecHasta");
            var resultado = _appAutorizaciones.ObtieneReporteDenegacion(numTarjeta, numCuenta, 
                                     usuario, fecDesde, fecHasta, idEmpresa);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public string ObtieneDetalleDenegacion(string denailDetail)
        {
            return _autorizaciones.ObtieneDetalleDenegacion(denailDetail);
        }
    }
}


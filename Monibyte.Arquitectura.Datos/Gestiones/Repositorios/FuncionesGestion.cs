﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Gestiones.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Gestiones.Repositorios
{
    public class FuncionesGestion : RepositorioBase, IFuncionesGestion
    {
        ISql _unidadTbjo;
        public FuncionesGestion(IUnidadTbjoMonibyte unidadTbjo)
            : base(unidadTbjo)
        {
            _unidadTbjo = unidadTbjo;
        }

        public IEnumerable<Cplx_GE_ModificacionesLimite> ObtieneModificacionesLimite(ParamReporteModificacionesLimite param)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", param.FechaDesde));
            listaParams.Add(new SqlParameter("p2", param.FechaHasta));
            listaParams.Add(new SqlParameter("p3", (object)param.NumCuenta ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p4", (object)param.IdEstado ?? DBNull.Value));
            return _unidadTbjo.ExecuteQuery<Cplx_GE_ModificacionesLimite>
                ("Gestion.P_ObtieneModificacionesLimite @p1, @p2, @p3, @p4", listaParams.ToArray()).ToList();
        }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using System.Linq;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;

namespace Monibyte.Arquitectura.Datos.Generales.Repositorios
{
    public class RepositorioUsuario : RepositorioMnb<GL_Usuario>, IRepositorioUsuario
    {
        public RepositorioUsuario(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public GL_Usuario ObtenerUsuarioPorCodigo(string aliasCod)
        {
            var criteria = new Criteria<GL_Usuario>();
            criteria.And(usuario => usuario.CodUsuario == aliasCod);
            criteria.Or(usuario => usuario.AliasUsuario == aliasCod);
            return this.SelectBy(criteria).FirstOrDefault();
        }

        public void ActualizaEstado(int idUsuario, int idEstado)
        {
            var usuario = new GL_Usuario
            {
                IdUsuario = idUsuario,
                IdEstado = idEstado
            };
            this.Update(usuario, columnas => columnas.IdEstado);
        }

        public bool ExisteUsuario(string codUsuario)
        {
            var _user = ObtenerUsuarioPorCodigo(codUsuario);
            return _user != null;
        }
    }
}


﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public interface IRolesApp : IAplicacionBase
    {
        IEnumerable<PocRol> ObtenerRolesTipoUsuario(int idTipoUsuario);
        bool ExisteRol(int codRol);
        IEnumerable<PocRol> ObtenerRolesPorGrupo(int codGrupo);
        IEnumerable<PocRol> ObtenerRolesPorUsuario(int AliasUsuario);
        IEnumerable<PocRol> ListaRoles();
    }
}
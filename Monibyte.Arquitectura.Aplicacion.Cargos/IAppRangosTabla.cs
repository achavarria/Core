﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Cargos;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public interface IAppRangosTabla : IAplicacionBase
    {
        void Insertar(PocRangosTabla registro);
        void Modificar(PocRangosTabla registro);
        void Eliminar(PocRangosTabla registro);
        IEnumerable<PocRangosTabla> ListadoRangosTabla();
        DataResult<PocRangosTabla> Listar(PocRangosTabla filtro, DataRequest request);
        DataResult<PocDetalleCargos> ListarDetalleCargos(PocDetalleCargos filtro, DataRequest request);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monibyte.Arquitectura.Poco.Clientes
{
   public class PocProfesion
    {
       public int IdProfesion { get; set; }
       public string Profesion { get; set; }
       public int IdEstado { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_EncabezadoEstCuentaMap : EntityTypeConfiguration<TC_EncabezadoEstCuenta>
    {
        public TC_EncabezadoEstCuentaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdEstCuenta);

            // Properties
            this.Property(t => t.IdEstCuenta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumCuenta)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.NumTarjeta)
                .HasMaxLength(20);

            this.Property(t => t.NombreCliente)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.CorreoElectronico)
                .HasMaxLength(100);

            this.Property(t => t.DireccionDeEnvio)
                .HasMaxLength(300);

            this.Property(t => t.ProgramaPuntos)
                .HasMaxLength(30);

            this.Property(t => t.Observaciones)
                .HasMaxLength(450);

            this.Property(t => t.NumeroCuentaMadre)
                .HasMaxLength(20);

            this.Property(t => t.DetalleError)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("TC_EncabezadoEstCuenta", "TCredito");
            this.Property(t => t.IdEstCuenta).HasColumnName("IdEstCuenta");
            this.Property(t => t.IdDiaCorte).HasColumnName("IdDiaCorte");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.NumCuenta).HasColumnName("NumCuenta");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.NombreCliente).HasColumnName("NombreCliente");
            this.Property(t => t.CorreoElectronico).HasColumnName("CorreoElectronico");
            this.Property(t => t.DireccionDeEnvio).HasColumnName("DireccionDeEnvio");
            this.Property(t => t.PlazoFinanc).HasColumnName("PlazoFinanc");
            this.Property(t => t.TasaInteresLocal).HasColumnName("TasaInteresLocal");
            this.Property(t => t.TasaInteresInter).HasColumnName("TasaInteresInter");
            this.Property(t => t.TasaMoraLocal).HasColumnName("TasaMoraLocal");
            this.Property(t => t.TasaMoraInter).HasColumnName("TasaMoraInter");
            this.Property(t => t.FecCorte).HasColumnName("FecCorte");
            this.Property(t => t.LimiteCreditoLocal).HasColumnName("LimiteCreditoLocal");
            this.Property(t => t.SaldoCortePrincLocal).HasColumnName("SaldoCortePrincLocal");
            this.Property(t => t.SaldoDisponibleLocal).HasColumnName("SaldoDisponibleLocal");
            this.Property(t => t.LimiteCreditoInter).HasColumnName("LimiteCreditoInter");
            this.Property(t => t.SaldoCortePrincInter).HasColumnName("SaldoCortePrincInter");
            this.Property(t => t.SaldoDisponibleInter).HasColumnName("SaldoDisponibleInter");
            this.Property(t => t.FecPagoMinimo).HasColumnName("FecPagoMinimo");
            this.Property(t => t.PagoMinPrincLocal).HasColumnName("PagoMinPrincLocal");
            this.Property(t => t.PagoMinPrincInter).HasColumnName("PagoMinPrincInter");
            this.Property(t => t.PagoMinInteresLocal).HasColumnName("PagoMinInteresLocal");
            this.Property(t => t.PagoMinInteresInter).HasColumnName("PagoMinInteresInter");
            this.Property(t => t.PagoMinLocal).HasColumnName("PagoMinLocal");
            this.Property(t => t.PagoMinInter).HasColumnName("PagoMinInter");
            this.Property(t => t.FecPagoContado).HasColumnName("FecPagoContado");
            this.Property(t => t.PagoContadoLocal).HasColumnName("PagoContadoLocal");
            this.Property(t => t.PagoContadoInter).HasColumnName("PagoContadoInter");
            this.Property(t => t.InterDiarioMoraLocal).HasColumnName("InterDiarioMoraLocal");
            this.Property(t => t.InterDiarioMoraInter).HasColumnName("InterDiarioMoraInter");
            this.Property(t => t.MontoMoraLocal).HasColumnName("MontoMoraLocal");
            this.Property(t => t.MontoMoraInter).HasColumnName("MontoMoraInter");
            this.Property(t => t.InteresMoraLocal).HasColumnName("InteresMoraLocal");
            this.Property(t => t.InteresMoraInter).HasColumnName("InteresMoraInter");
            this.Property(t => t.OtrosCargosLocal).HasColumnName("OtrosCargosLocal");
            this.Property(t => t.OtrosCargosInter).HasColumnName("OtrosCargosInter");
            this.Property(t => t.FecCorteAnterior).HasColumnName("FecCorteAnterior");
            this.Property(t => t.SaldoAntInteresLocal).HasColumnName("SaldoAntInteresLocal");
            this.Property(t => t.SaldoAntInteresInter).HasColumnName("SaldoAntInteresInter");
            this.Property(t => t.SaldoAnteriorLocal).HasColumnName("SaldoAnteriorLocal");
            this.Property(t => t.SaldoAnteriorInter).HasColumnName("SaldoAnteriorInter");
            this.Property(t => t.SaldoIniPremCorte).HasColumnName("SaldoIniPremCorte");
            this.Property(t => t.DebitosPremCorte).HasColumnName("DebitosPremCorte");
            this.Property(t => t.CreditosPremCorte).HasColumnName("CreditosPremCorte");
            this.Property(t => t.PuntosPorRedimir).HasColumnName("PuntosPorRedimir");
            this.Property(t => t.PuntosDelCorte).HasColumnName("PuntosDelCorte");
            this.Property(t => t.ProgramaPuntos).HasColumnName("ProgramaPuntos");
            this.Property(t => t.LimiteExtraf).HasColumnName("LimiteExtraf");
            this.Property(t => t.SobregiroLocal).HasColumnName("SobregiroLocal");
            this.Property(t => t.SobregiroInter).HasColumnName("SobregiroInter");
            this.Property(t => t.Observaciones).HasColumnName("Observaciones");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.InteresFinanciadoInter).HasColumnName("InteresFinanciadoInter");
            this.Property(t => t.InteresFinanciadoLocal).HasColumnName("InteresFinanciadoLocal");
            this.Property(t => t.TipoDeCambio).HasColumnName("TipoDeCambio");
            this.Property(t => t.NumeroCuentaMadre).HasColumnName("NumeroCuentaMadre");
            this.Property(t => t.DetalleError).HasColumnName("DetalleError");
            this.Property(t => t.LimiteAdelantoLocal).HasColumnName("LimiteAdelantoLocal");
            this.Property(t => t.LimiteAdelantoInter).HasColumnName("LimiteAdelantoInter");
            this.Property(t => t.CantImpVencidosLocal).HasColumnName("CantImpVencidosLocal");
            this.Property(t => t.CantImpVencidosInter).HasColumnName("CantImpVencidosInter");
            this.Property(t => t.MonImpVencidosLocal).HasColumnName("MonImpVencidosLocal");
            this.Property(t => t.MonImpVencidosInter).HasColumnName("MonImpVencidosInter");
            this.Property(t => t.CreditoLocal).HasColumnName("CreditoLocal");
            this.Property(t => t.CreditoInter).HasColumnName("CreditoInter");
            this.Property(t => t.DebitoLocal).HasColumnName("DebitoLocal");
            this.Property(t => t.DebitoInter).HasColumnName("DebitoInter");
        }
    }
}

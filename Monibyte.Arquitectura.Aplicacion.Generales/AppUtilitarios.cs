﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Generales;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Generales
{
    public class AppUtilitarios : AplicacionBase, IAppUtilitarios
    {
        private IRepositorioEmail _repEmail;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<GL_Pais> _repPais;
        private IRepositorioMnb<GL_Idioma> _repIdioma;
        private IRepositorioMnb<MB_PaisesRegion> _repPaisesRegion;
        private IRepositorioPlantillaNotificacion _repPlantillaNot;
        private IRepositorioMnb<GL_TipoIdentificacion> _repTipoIdentificacion;

        public AppUtilitarios(
            IRepositorioEmail repEmail,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<GL_Pais> repPais,
            IRepositorioMnb<GL_Idioma> repIdioma,
            IRepositorioMnb<MB_PaisesRegion> repPaisesRegion,
            IRepositorioPlantillaNotificacion repPlantillaNot,
            IRepositorioMnb<GL_TipoIdentificacion> repTipoIdentificacion)
        {
            _repEmail = repEmail;
            _repCatalogo = repCatalogo;
            _repPais = repPais;
            _repIdioma = repIdioma;
            _repPaisesRegion = repPaisesRegion;
            _repPlantillaNot = repPlantillaNot;
            _repTipoIdentificacion = repTipoIdentificacion;
        }

        public void EnviarEmail(SenderConfig sender, EmailConfig emailConfig)
        {
            _repEmail.EnviarEmail(sender, emailConfig);
        }

        public void EnviarInfoEmail(EmailConfig emailConfig)
        {
            _repEmail.EnviarInfoEmail(emailConfig);
        }

        public IEnumerable<PocIdioma> ListaIdiomas()
        {
            var idiomas = _repIdioma.Table.ToList();
            return idiomas.ProyectarComo<PocIdioma>();
        }

        public IEnumerable<PocPais> ListaPaises()
        {
            int? idPais = null;
            if (Context.User != null)
            {
                idPais = Context.User.IdPais;
            }
            return (from p in _repPais.Table
                    join pr in _repPaisesRegion.Table on
                       p.IdPais equals pr.IdPais
                    select new PocPais
                    {
                        IdPais = p.IdPais,
                        IdRegion = pr.IdRegion,
                        Descripcion = _fGenerales.Traducir(p.Descripcion, "GL_Pais", p.IdPais,
                            Context.Lang.Id, 0, Context.DefaultLang.Id),
                        Nacionalidad = p.Nacionalidad
                    })
                    .OrderBy(m => m.IdPais == idPais ? "aaaaa" : m.Descripcion);
        }

        public IEnumerable<PocTipoIdentificacion> ListaTipoIdentificacion()
        {
            var TiposIdent = _repTipoIdentificacion.Table.ToList()
                .Select(x => new PocTipoIdentificacion
                {
                    IdIdentificacion = x.IdIdentificacion,
                    IdFormato = x.IdFormato,
                    IdEstado = x.IdEstado,
                    Descripcion = _fGenerales.Traducir(x.Descripcion, "GL_TipoIdentificacion", x.IdIdentificacion,
                            Context.Lang.Id, 0, Context.DefaultLang.Id)
                });

            return TiposIdent;
        }

        public IEnumerable<PocCatalogo> ObtieneInfoLista(string lista, int?[] excluir, string ref1 = "",
            string ref2 = "", string ref3 = "", string ref4 = "", string ref5 = "")
        {
            var criteria = new Criteria<GL_Catalogo>();
            if (!string.IsNullOrEmpty(lista))
            {
                criteria.And(m => m.Lista == lista);
            }
            criteria.And(m => m.IdEstado != (int)EnumIdEstado.Inactivo);
            if (excluir != null && excluir.Any())
            {
                foreach (var value in excluir)
                {
                    criteria.And(m => m.IdCatalogo != value);
                }
            }
            if (!string.IsNullOrEmpty(ref1))
            {
                criteria.And(m => !string.IsNullOrEmpty(m.Referencia1) &&
                    m.Referencia1.Contains(ref1));
            }
            if (!string.IsNullOrEmpty(ref2))
            {
                criteria.And(m => !string.IsNullOrEmpty(m.Referencia2) &&
                    m.Referencia2.Contains(ref2));
            }
            if (!string.IsNullOrEmpty(ref3))
            {
                criteria.And(m => !string.IsNullOrEmpty(m.Referencia3) &&
                    m.Referencia3.Contains(ref3));
            }
            if (!string.IsNullOrEmpty(ref4))
            {
                criteria.And(m => !string.IsNullOrEmpty(m.Referencia4) &&
                    m.Referencia4.Contains(ref4));
            }
            if (!string.IsNullOrEmpty(ref5))
            {
                criteria.And(m => !string.IsNullOrEmpty(m.Referencia5) &&
                    m.Referencia5.Contains(ref5));
            }
            return _repCatalogo.List(criteria)
                .OrderBy(m => m.Orden)
                .ProyectarComo<PocCatalogo>();
        }

        public PocCatalogo ObtieneInfoValorLista(int idLista)
        {
            var repLista = _repCatalogo.SelectById(idLista);
            return repLista.ProyectarComo<PocCatalogo>();
        }

        public int RolPorTipoUsuario(int idTipoUsuario)
        {
            var role = _repCatalogo.SelectById(idTipoUsuario);
            return string.IsNullOrEmpty(role.Referencia2) ?
                0 : int.Parse(role.Referencia2);
        }

        public void EnviarNotificacion(string from, string to, string cc = null, string bcc = null,
            int? idPlantilla = null, string subject = null, string body = null,
            object[] subjectParams = null, object[] bodyParams = null)
        {
            var asunto = subject;
            var mensaje = body;
            if (idPlantilla.HasValue)
            {
                var notificacion = _repPlantillaNot.ObtenerPlantillaNot(idPlantilla.Value);
                asunto = StringExt.ClearFormat(notificacion.Asunto, subjectParams);
                mensaje = StringExt.ClearFormat(notificacion.Cuerpo, bodyParams);
            }
            if (string.IsNullOrEmpty(from) || string.IsNullOrEmpty(to) ||
                string.IsNullOrEmpty(asunto) || string.IsNullOrEmpty(mensaje))
            {
                throw new CoreException("", "Err_0087");
            }
            SenderConfig sender = new SenderConfig(from, null);
            EmailConfig emailData = new EmailConfig(asunto, mensaje);
            emailData.AddTo(to);
            if (!string.IsNullOrEmpty(cc))
            {
                emailData.AddCC(cc);
            }
            if (!string.IsNullOrEmpty(bcc))
            {
                emailData.AddBcc(bcc);
            }
            _repEmail.EnviarEmail(sender, emailData);
        }

        public int ObtenerSistemaPorTipoUsuario(int idTipoUsuario)
        {
            var catalogo = _repCatalogo.SelectById(idTipoUsuario);
            return catalogo != null ? Convert.ToInt32(catalogo.Referencia1) : 0;
        }

        public void ObtieneCamposContextoExcluirCriteria(ref IEnumerable<PocCampoContexto> listCamposContexto, 
            string ref1 = null, string ref2 = null, string ref3 = null, string ref4 = null, string ref5 = null)
        {
            var lista = new List<PocCampoContexto>();
            if (ref1 != null)
            {
                foreach (var item in listCamposContexto)
                {
                    if (ref1.IsEqualTo(item.Referencia1))
                    {
                        lista.Add(item);
                    }
                }
            }

            if (ref2 != null)
            {
                foreach (var item in listCamposContexto)
                {
                    if (ref2.IsEqualTo(item.Referencia2))
                    {
                        lista.Add(item);
                    }
                }
            }

            if (ref3 != null)
            {
                foreach (var item in listCamposContexto)
                {
                    if (ref3.IsEqualTo(item.Referencia3))
                    {
                        lista.Add(item);
                    }
                }
            }

            if (ref4 != null)
            {
                foreach (var item in listCamposContexto)
                {
                    if (ref4.IsEqualTo(item.Referencia4))
                    {
                        lista.Add(item);
                    }
                }
            }

            if (ref5 != null)
            {
                foreach (var item in listCamposContexto)
                {
                    if (ref5.IsEqualTo(item.Referencia5))
                    {
                        lista.Add(item);
                    }
                }
            }

            listCamposContexto = lista;

            //var repLista = _repCatalogo.SelectById(idContexto);
            //var catalogo = repLista.ProyectarComo<PocCatalogo>();
            //var listaPermitidos = catalogo.Referencia2.FromJson<List<int>>();

            //if (listaPermitidos != null && listaPermitidos.Count() > 0)
            //{
            //    var lista = new List<PocCampoContexto>();
            //    foreach (var item in listCamposContexto)
            //    {
            //        foreach (var item2 in listaPermitidos)
            //        {
            //            if (item2 == item.IdCampo)
            //            {
            //                lista.Add(item);
            //            }
            //        }
            //    }
            //    listCamposContexto = lista;
            //}
        }
    }
}
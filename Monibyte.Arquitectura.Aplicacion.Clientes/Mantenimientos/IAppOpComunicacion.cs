﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Clientes;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public interface IAppOpComunicacion : IAplicacionBase
    {
        void Insertar(PocComunicacion registro);
        void Eliminar(PocComunicacion registro);
        void Modificar(PocComunicacion registro);
        PocComunicacion Obtener(PocComunicacion filtro);
        DataResult<PocComunicacion> Listar(PocComunicacion filtro, DataRequest request);
        List<PocComunicacion> ObtenerPorPersona(int idPersona);
        PocComunicacion ObtenerTipoPorPersona(int idPersona, EnumTipoComunicacion idTipo);
        void EliminarPorPersona(int idPersona);
    }
}

using Monibyte.Arquitectura.Dominio.Cargos;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Cargos
{
    public class CA_CargosGeoMap : EntityTypeConfiguration<CA_CargosGeo>
    {
        public CA_CargosGeoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdRegistro);

            // Properties
            this.Property(t => t.Marca)
                .HasMaxLength(10);

            this.Property(t => t.Estilo)
                .HasMaxLength(10);

            this.Property(t => t.Placa)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("CA_CargosGeo", "Cargos");
            this.Property(t => t.IdRegistro).HasColumnName("IdRegistro");
            this.Property(t => t.IdCargo).HasColumnName("IdCargo");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdTipoVehiculo).HasColumnName("IdTipoVehiculo");
            this.Property(t => t.Marca).HasColumnName("Marca");
            this.Property(t => t.Estilo).HasColumnName("Estilo");
            this.Property(t => t.Placa).HasColumnName("Placa");
            this.Property(t => t.Modelo).HasColumnName("Modelo");
            this.Property(t => t.PaqueteBase).HasColumnName("PaqueteBase");
            this.Property(t => t.Sensor).HasColumnName("Sensor");
            this.Property(t => t.BotonPanico).HasColumnName("BotonPanico");
            this.Property(t => t.CargoOriginal).HasColumnName("CargoOriginal");
            this.Property(t => t.CargoActual).HasColumnName("CargoActual");
            this.Property(t => t.FecUltAplicacion).HasColumnName("FecUltAplicacion");
            this.Property(t => t.FecUltActividad).HasColumnName("FecUltActividad");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

﻿using System;

namespace Monibyte.Arquitectura.Dominio.SMS
{
    public partial class AC_MENSAJES_SMS : EntidadBase
    {
        public long ID_MENSAJE { get; set; }
        public Nullable<decimal> NUM_TELEFONO { get; set; }
        public string TEXTO { get; set; }
        public Nullable<System.DateTime> FECHA_HORA_REGISTRO { get; set; }
        public Nullable<System.DateTime> FECHA_HORA_PROCESADO { get; set; }
        public string DES_ERROR { get; set; }
        public Nullable<int> IND_ESTADO { get; set; }
        public Nullable<int> ID_TIPORIGEN { get; set; }
        public Nullable<int> ID_ORIGEN { get; set; }
    }
}

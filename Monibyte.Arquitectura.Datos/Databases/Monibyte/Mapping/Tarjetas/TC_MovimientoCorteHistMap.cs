using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_MovimientoCorteHistMap : EntityTypeConfiguration<TC_MovimientoCorteHist>
    {
        public TC_MovimientoCorteHistMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdMovimiento, t.IdCorte });

            // Properties
            this.Property(t => t.IdMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdCorte)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumAutorizacion)
                .HasMaxLength(20);

            this.Property(t => t.Comercio)
                .HasMaxLength(100);

            this.Property(t => t.DetalleMovimiento)
                .HasMaxLength(2000);

            this.Property(t => t.Documento2)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.NumReferencia)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("TC_MovimientoCorteHist", "TCredito");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdCorte).HasColumnName("IdCorte");
            this.Property(t => t.IdTipoMovimiento).HasColumnName("IdTipoMovimiento");
            this.Property(t => t.IdOrigenMovimiento).HasColumnName("IdOrigenMovimiento");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.FecTransaccion).HasColumnName("FecTransaccion");
            this.Property(t => t.HoraTransaccion).HasColumnName("HoraTransaccion");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
            this.Property(t => t.NumAutorizacion).HasColumnName("NumAutorizacion");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.MonMovimiento).HasColumnName("MonMovimiento");
            this.Property(t => t.TasaComision).HasColumnName("TasaComision");
            this.Property(t => t.Comercio).HasColumnName("Comercio");
            this.Property(t => t.IdMcc).HasColumnName("IdMcc");
            this.Property(t => t.DetalleMovimiento).HasColumnName("DetalleMovimiento");
            this.Property(t => t.IdDebCredito).HasColumnName("IdDebCredito");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.Documento1).HasColumnName("Documento1");
            this.Property(t => t.Documento2).HasColumnName("Documento2");
            this.Property(t => t.NumReferencia).HasColumnName("NumReferencia");
            this.Property(t => t.MonTipoCambio).HasColumnName("MonTipoCambio");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.CamposDinamicos).HasColumnName("CamposDinamicos");
            this.Property(t => t.IdEditado).HasColumnName("IdEditado");
            this.Property(t => t.HoraMovimiento).HasColumnName("HoraMovimiento");
            this.Property(t => t.FecCorte).HasColumnName("FecCorte");
            this.Property(t => t.GeoCamposDinamicos).HasColumnName("GeoCamposDinamicos");
            this.Property(t => t.IdLiquidado).HasColumnName("IdLiquidado");
        }
    }
}

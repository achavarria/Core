using Monibyte.Arquitectura.Dominio.Cargos;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Cargos 

{
    public class CA_CargosPorPersonaMap : EntityTypeConfiguration<CA_CargosPorPersona>
    {
        public CA_CargosPorPersonaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCargo);

            // Properties
            this.Property(t => t.IdCargo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CA_CargosPorPersona", "Cargos");
            this.Property(t => t.IdCargo).HasColumnName("IdCargo");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.IdTipoCargo).HasColumnName("IdTipoCargo");
            this.Property(t => t.FecInicio).HasColumnName("FecInicio");
            this.Property(t => t.FecFinal).HasColumnName("FecFinal");
            this.Property(t => t.IdCuentaCobro).HasColumnName("IdCuentaCobro");
            this.Property(t => t.IdTipoCobro).HasColumnName("IdTipoCobro");
            this.Property(t => t.MonCargo).HasColumnName("MonCargo");
            this.Property(t => t.IdTablaRangos).HasColumnName("IdTablaRangos");
            this.Property(t => t.IdPeriodicidad).HasColumnName("IdPeriodicidad");
            this.Property(t => t.ValPeriodicidad).HasColumnName("ValPeriodicidad");
            this.Property(t => t.FecUltCargo).HasColumnName("FecUltCargo");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdEstadoCobro).HasColumnName("IdEstadoCobro");

        }
    }
}

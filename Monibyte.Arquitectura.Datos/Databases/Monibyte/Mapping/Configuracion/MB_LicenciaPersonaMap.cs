using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_LicenciaPersonaMap : EntityTypeConfiguration<MB_LicenciaPersona>
    {
        public MB_LicenciaPersonaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdPersona, t.IdTipoLicencia });

            // Properties
            this.Property(t => t.IdPersona)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdTipoLicencia)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MB_LicenciaPersona", "Mnb");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.IdTipoLicencia).HasColumnName("IdTipoLicencia");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

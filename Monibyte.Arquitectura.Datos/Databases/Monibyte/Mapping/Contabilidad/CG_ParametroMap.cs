using Monibyte.Arquitectura.Dominio.Contabilidad;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Contabilidad
{
    public class CG_ParametroMap : EntityTypeConfiguration<CG_Parametro>
    {
        public CG_ParametroMap()
        {
            // Primary Key
            this.HasKey(t => t.IdParametro);

            // Properties
            this.Property(t => t.IdParametro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("CG_Parametro", "Contabilidad");
            this.Property(t => t.IdParametro).HasColumnName("IdParametro");
            this.Property(t => t.IdTipoParametro).HasColumnName("IdTipoParametro");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

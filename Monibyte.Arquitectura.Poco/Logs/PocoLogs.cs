﻿using System;

namespace Monibyte.Arquitectura.Poco.Logs
{
    public class PocLogs
    {
        public int IdEvento { get; set; }
        public int IdTipoEvento { get; set; }
        public DateTime FecEvento { get; set; }
        public string CodUsuario { get; set; }
        public int? IdSistema { get; set; }
        public DateTime? HoraEvento { get; set; }
        public DateTime FecInicio { get; set; }
        public DateTime? FecFinal { get; set; }
        public string DireccionIpv4 { get; set; }
        public string CodigoSesion { get; set; }
        public string Detalle { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Gestiones;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Clientes
{
    public partial class CL_Ejecutivo : EntidadBase
    {
        public int IdEjecutivo { get; set; }
        public int IdEstado { get; set; }
        public Nullable<int> IdPersona { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }        
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdEntidad { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
namespace Monibyte.Arquitectura.Datos.Tarjetas.Repositorios
{

    public class RepositorioTarjeta : RepositorioMnb<TC_Tarjeta>, IRepositorioTarjeta
    {
        public RepositorioTarjeta(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }
        public IEnumerable<TC_Tarjeta> TarjetasPorCuenta(int idCuenta, bool soloActivas = false)
        {
            var critTarjetas = new Criteria<TC_Tarjeta>();
            critTarjetas.And(tarjeta => tarjeta.IdCuenta == idCuenta);
            return this.SelectBy(critTarjetas);
        }
        public IEnumerable<TC_Tarjeta> TarjetasPorCliente(int idCliente)
        {
            var resultado2 = this.TableSet
                .Where(res =>
                    (res.IdEmpresa == idCliente && res.IdRelacion == 89) || //Titular
                    (res.IdPersona == idCliente && res.IdRelacion == 90) || //Adicional
                    (res.IdEmpresa == idCliente && res.IdRelacion == 90));  //Adicional
            return resultado2;
        }

        public virtual void MigrarTarjeta(
            string numTcOrigen, string numTcDestino)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", numTcOrigen));
            listaParams.Add(new SqlParameter("p2", numTcDestino));           

            var resultado = (this.UnidadTbjo as ISql).ExecuteQuery<int>
                (@"TCredito.P_MigrarTarjeta @p1, @p2", listaParams.ToArray()).ToJson();   

        }
    }

}

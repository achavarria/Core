using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_MovimientoCorte : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public int IdTipoMovimiento { get; set; }
        public int IdOrigenMovimiento { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public System.DateTime FecTransaccion { get; set; }
        public System.DateTime HoraTransaccion { get; set; }
        public Nullable<System.DateTime> FecMovimiento { get; set; }
        public string NumAutorizacion { get; set; }
        public int IdMoneda { get; set; }
        public decimal MonMovimiento { get; set; }
        public Nullable<decimal> TasaComision { get; set; }
        public string Comercio { get; set; }
        public Nullable<int> IdMcc { get; set; }
        public string DetalleMovimiento { get; set; }
        public int IdDebCredito { get; set; }
        public int IdPais { get; set; }
        public Nullable<long> Documento1 { get; set; }
        public string Documento2 { get; set; }
        public string NumReferencia { get; set; }
        public Nullable<decimal> MonTipoCambio { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string CamposDinamicos { get; set; }
        public int IdEditado { get; set; }
        public Nullable<System.DateTime> HoraMovimiento { get; set; }
        public Nullable<System.DateTime> FecCorte { get; set; }
        public string GeoCamposDinamicos { get; set; }
        public int IdLiquidado { get; set; }
    }
}

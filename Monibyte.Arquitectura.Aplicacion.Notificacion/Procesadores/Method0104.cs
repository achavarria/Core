﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Poco.Notificacion;
using Monibyte.Arquitectura.Poco.Tarjetas;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public partial class AppNotificacion
    {
        //Notificaicones provenientes del Servicio de Cierres (Notificacion días antes de fecha pago)
        public void Method0104(PocoQueue pocoQueue)
        {
            var cuenta = pocoQueue.Data.FromJson<PocoInfoCuentaNot>();
            pocoQueue.CompanyId = cuenta.IdEmpresa;
            ProcesarNotificaciones(pocoQueue, true, false, pocoQueue.CompanyId);
        }
    }
}
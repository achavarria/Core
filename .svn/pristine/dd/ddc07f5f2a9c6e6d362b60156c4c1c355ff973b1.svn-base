﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Dominio.Efectivo.Repositorios;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Gestiones.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco.Efectivo;
using Monibyte.Arquitectura.Poco.Gestiones;
using System;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Gestiones
{
    public class AppGestionesEfectivo : AplicacionBase, IAppGestionesEfectivo
    {
        private IRepositorioGestion _repGestion;
        private IRepositorioMnb<GE_HistorialGestion> _repHistGestion;
        private IAppGestiones _appGestiones;
        private IRepositorioMnb<GE_ParametrosValor> _repParamValor;
        private IRepositorioMovimientos _repMovimientos;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<EF_TarifaKilometraje> _repTarifaKm;
        private IRepositorioVwTarjetaUsuario _repVwTarjetaUsuario;
        private IAppGenerales _appGenerales;
        private IRepositorioUsuario _repUsuario;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;

        public AppGestionesEfectivo(
            IRepositorioGestion repGestion,
            IRepositorioMnb<GE_HistorialGestion> repHistGestion,
            IAppGestiones appGestiones,
            IRepositorioMnb<GE_ParametrosValor> repParamValor,
            IRepositorioMovimientos repMovimientos,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<EF_TarifaKilometraje> repTarifaKm,
            IRepositorioVwTarjetaUsuario repVwTarjetaUsuario,
            IAppGenerales appGenerales,
            IRepositorioUsuario repUsuario,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema)
        {
            _repGestion = repGestion;
            _repHistGestion = repHistGestion;
            _appGestiones = appGestiones;
            _repParamValor = repParamValor;
            _repMovimientos = repMovimientos;
            _repPersona = repPersona;
            _repTarifaKm = repTarifaKm;
            _repVwTarjetaUsuario = repVwTarjetaUsuario;
            _appGenerales = appGenerales;
            _repUsuario = repUsuario;
            _repCatalogo = repCatalogo;
            _repParamSistema = repParamSistema;
        }

        public DataResult<PocGestionKilometraje> ObtieneListaGestionKm(
            PocParametrosGestion filtro, DataRequest request)
        {
            var idEmpresa = InfoSesion.Info.IdEmpresa;
            var idCompania = InfoSesion.Info.IdCompania;
            var criteria = new Criteria<GE_Gestion>();
            criteria.And(m => m.IdEmpresa == idEmpresa);
            if (filtro.IdContexto > 0)
            {
                criteria.And(m => m.IdContexto == filtro.IdContexto);
            }
            if (filtro.IdTipoGestion > 0)
            {
                criteria.And(m => m.IdTipoGestion == filtro.IdTipoGestion);
            }
            if (filtro.FecDesde.HasValue && filtro.FecHasta.HasValue)
            {
                criteria.And(m => m.FecInclusion >= filtro.FecDesde);
                filtro.FecHasta = filtro.FecHasta.Value.AddDays(Time.DayFactor);
                criteria.And(m => m.FecInclusion <= filtro.FecHasta);
            }
            var personasVisibles = _repVwTarjetaUsuario
               .TarjetasPermitidas(incluirOperativas: true,
                   filtroEstado: new int[] { (int)EnumEstadoTarjeta.Todos });
            var resultado =
                _repGestion.SelectBy(criteria).ToList()
                .Join(personasVisibles,
                    ge => ge.IdPersona,
                    pe => pe.IdPersona,
                    (ge, pe) => new { ge, pe })
                .Join(_repParamValor.Table,
                    tmp => tmp.ge.IdGestion,
                    pv => pv.IdGestion,
                    (tmp, pv) => new { tmp.ge, tmp.pe, pv = pv.Valor.FromJson<PocParametrosGestionKM>(), pv2 = pv.Valor })
                .Join(_repCatalogo.List("LTIPOVEHICULOKM"),
                    tmp => new { tmp.pv.IdTipoVehiculo },
                    cat => new { IdTipoVehiculo = cat.IdCatalogo },
                    (tmp, tveh) => new { tmp.ge, tmp.pe, tmp.pv, tmp.pv2, tveh })
                .Join(_repPersona.Table,
                    tmp => tmp.ge.IdPersona,
                    per => per.IdPersona,
                    (tmp, per) => new { tmp.ge, tmp.pe, tmp.pv, tmp.pv2, tmp.tveh, per })
                .Join(_repCatalogo.List("LESTADOGESTION"),
                    tmp => new { tmp.ge.IdEstado },
                    cat => new { IdEstado = cat.IdCatalogo },
                    (tmp, est) => new { tmp.ge, tmp.pe, tmp.pv, tmp.pv2, tmp.tveh, tmp.per, est })
                .Join(_repUsuario.Table,
                    tmp => new { tmp.ge.IdUsuarioIncluye },
                    usr => new { IdUsuarioIncluye = usr.IdUsuario as int? },
                    (tmp, usr) => new { tmp.ge, tmp.pe, tmp.pv, tmp.pv2, tmp.tveh, tmp.per, tmp.est, usr })
                .Select(item => new PocGestionKilometraje
                {
                    IdEmpresa = item.ge.IdEmpresa,
                    IdPersona = item.ge.IdPersona,
                    IdGestion = item.ge.IdGestion,
                    Detalle = item.ge.Detalle,
                    FecInclusion = item.ge.FecInclusion.Date,
                    FecCierre = item.ge.FecCierre,
                    IdUsuarioCierra = item.ge.IdUsuarioCierra,
                    IdUsuarioIncluye = item.ge.IdUsuarioIncluye,
                    IdTipoVehiculo = item.tveh.IdCatalogo,
                    Modelo = item.pv.Modelo,
                    Placa = item.pv.Placa,
                    IdEstado = item.ge.IdEstado,
                    DescEstado = item.est.Descripcion,
                    IdCompania = idCompania,
                    NombrePersona = item.per.NombreCompleto,
                    CodUsuarioEmpresa = item.usr.CodUsuarioEmpresa,
                    NumIdentificacion = item.per.NumIdentificacion,
                    DescTipoVehiculo = item.tveh.Descripcion,
                    DynamicJson = item.pv2,
                    SoloConsulta = item.pe.IdSoloConsulta == (int)EnumIdSiNo.Si
                });
            return resultado.ToDataResult(request);
        }

        public int IncluirGestionKilometraje(PocGestionKilometraje registro)
        {
            using (var ts = new TransactionScope())
            {
                registro.IdGestion = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdGestion");
                _repGestion.Insert(new GE_Gestion
                {
                    IdGestion = registro.IdGestion,
                    Detalle = registro.Detalle,
                    IdPersona = registro.IdPersona,
                    FecInclusion = registro.FecInclusion,
                    FecCierre = DateTime.Now.Date,
                    IdUsuarioCierra = InfoSesion.Info.IdUsuario,
                    IdUsuarioIncluye = InfoSesion.Info.IdUsuario,
                    IdCompania = (int)InfoSesion.Info.IdCompania,
                    IdUnidad = (int)InfoSesion.Info.IdUnidad,
                    IdEmpresa = (int)InfoSesion.Info.IdEmpresa,
                    IdEstado = (int)EnumEstadoGestion.Registrada,
                    IdContexto = (int)EnumContexto.EF_KILOMETRAJE,
                    IdTipoProducto = (int)EnumTipoProducto.OperacionesEfectivo,
                    IdTipoGestion = (int)EnumTipoGestion.LiquidacionDeKilometraje,
                });
                if (registro.Detalle == null)
                {
                    registro.Detalle = _repGestion.Table.FirstOrDefault(x =>
                        x.IdGestion == registro.IdGestion).Detalle;
                }
                _repHistGestion.Insert(new GE_HistorialGestion
                {
                    IdGestion = registro.IdGestion,
                    FecEstado = DateTime.Now,
                    HoraEstado = DateTime.Now,
                    Detalle = registro.Detalle,
                    IdUsuario = InfoSesion.Info.IdUsuario,
                    IdEstado = (int)EnumEstadoGestion.Registrada
                });
                _appGestiones.IncluirValorParametro(registro.IdGestion, registro.DynamicJson);
                _repGestion.UnidadTbjo.Save();
                MantenimientoMovimientos(registro, false);
                ts.Complete();
            }
            return registro.IdGestion;
        }

        public void EditarGestionKilometraje(PocGestionKilometraje registro,
                PocGestionKilometraje registroPrevio)
        {
            var actualizarTodos = registro.Equals(registroPrevio);
            if (!actualizarTodos)
            {
                registro.ListaMovimientosKM.Update(x =>
                {
                    if (x.Accion != Acciones.Insert &&
                        x.Accion != Acciones.Delete)
                        x.Accion = Acciones.Update;
                });
            }
            using (var ts = new TransactionScope())
            {
                registro.IdEmpresa = (int)InfoSesion.Info.IdEmpresa;
                var entrada = registro.ProyectarComo<GE_Gestion>();
                _repGestion.UpdateExclude(entrada,
                    x => x.IdTipoGestion,
                    x => x.IdTipoProducto,
                    x => x.IdContexto,
                    x => x.IdEstado,
                    x => x.FecCierre,
                    x => x.IdUsuarioCierra,
                    x => x.IdCompania,
                    x => x.IdUnidad,
                    x => x.IdUsuarioIncluye,
                    x => x.IdUsuarioIncluyeAud,
                    x => x.FecInclusionAud);
                _repGestion.UnidadTbjo.Save();
                if (registro.ListaMovimientosKM != null)
                {
                    MantenimientoMovimientos(registro, true);
                }
                _appGestiones.ModificarValorParametro(entrada.IdGestion, registro.DynamicJson);
                ts.Complete();
            }
        }

        private void MantenimientoMovimientos(PocGestionKilometraje gestion, bool validaAccion)
        {
            if (gestion.ListaMovimientosKM != null)
            {
                var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
                var entidad = _appGenerales.ObtieneEntidad(idEntidad);
                foreach (var detalle in gestion.ListaMovimientosKM)
                {
                    var antiguedad = (gestion.FecInclusion.Year - gestion.Modelo);
                    if (!validaAccion || detalle.Accion == Acciones.Insert)
                    {
                        detalle.IdMovimiento = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdMovimientoEfectivo");
                        detalle.IdGestion = gestion.IdGestion;
                        detalle.IdPersona = gestion.IdPersona;
                        detalle.IdDebCredito = gestion.IdDebCredito;
                        detalle.IdDebCredito = (int)EnumDebitoCredito.Debito;
                        detalle.IdMoneda = entidad.MonedaLocal.IdMoneda;
                        detalle.CodMoneda = entidad.MonedaLocal.CodInternacional;
                        detalle.IdContexto = (int)EnumContexto.EF_KILOMETRAJE;
                        detalle.IdEmpresa = InfoSesion.Info.IdEmpresa;
                        var kilometros = (detalle.NumKmLlegada.Value - detalle.NumKmSalida.Value);
                        detalle.MonMovimiento = MontoKilometraje(gestion.IdTipoVehiculo,
                            antiguedad < 0 ? 0 : antiguedad, kilometros);
                        detalle.IdEstado = (int)EnumIdEstado.Activo;
                        var entradaMovs = detalle.ProyectarComo<EF_Movimiento>();
                        _repMovimientos.Insert(entradaMovs);
                    }
                    else if (validaAccion && detalle.Accion == Acciones.Update)
                    {
                        var kilometros = (detalle.NumKmLlegada.Value - detalle.NumKmSalida.Value);
                        _repMovimientos.UpdateOn(x => x.IdMovimiento == detalle.IdMovimiento,
                            z => new EF_Movimiento
                            {
                                MonMovimiento = MontoKilometraje(gestion.IdTipoVehiculo,
                                    antiguedad < 0 ? 0 : antiguedad, kilometros),
                                IdEstado = (int)EnumIdEstado.Activo,
                                NumKmLlegada = detalle.NumKmLlegada,
                                NumKmSalida = detalle.NumKmSalida,
                                NumFactura = detalle.NumFactura,
                                IdDebCredito = (int)EnumDebitoCredito.Debito,
                                DetalleMovimiento = detalle.DetalleMovimiento,
                                FecMovimiento = detalle.FecMovimiento.Value
                            });
                    }
                    else if (validaAccion && detalle.Accion == Acciones.Delete)
                    {
                        _repMovimientos.DeleteOn(mov => mov.IdMovimiento == detalle.IdMovimiento);
                    }
                }
                _repMovimientos.UnidadTbjo.Save();
            }
        }

        private decimal MontoKilometraje(int idTipoVehiculo, int antiguedad, int kilometros)
        {
            decimal monto = 0;
            int maximo = 0;
            int idEmpresa = InfoSesion.Info.IdEmpresa;

            var maxAntiguedad = _repTarifaKm.Table.OrderByDescending(x => x.Antiguedad)
                .FirstOrDefault(x => x.IdEmpresa == idEmpresa
                               && x.IdTipoVehiculo == idTipoVehiculo);
            if (maxAntiguedad != null)
            {
                maximo = maxAntiguedad.Antiguedad;
            }
            else
            {
                var entidadBase = _repParamSistema.Table.FirstOrDefault(
                        x => x.IdParametro == "ID_PERSONA_ENTIDAD_BASE");
                idEmpresa = Int32.Parse(entidadBase.ValParametro);
                maxAntiguedad = _repTarifaKm.Table.OrderByDescending(x => x.Antiguedad)
                .FirstOrDefault(x => x.IdEmpresa == idEmpresa
                               && x.IdTipoVehiculo == idTipoVehiculo);
                if (maxAntiguedad != null)
                {
                    maximo = maxAntiguedad.Antiguedad;
                }
            }
            if (antiguedad > maximo)
            {
                antiguedad = maximo;
            }
            var tarifa = _repTarifaKm.Table.FirstOrDefault(
                   x => x.IdTipoVehiculo == idTipoVehiculo &&
                   x.Antiguedad == antiguedad && x.IdEmpresa == idEmpresa);
            if (tarifa != null)
            {
                monto = (kilometros * tarifa.Monto);
            }
            return monto;
        }
    }
}

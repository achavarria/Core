﻿using System;
using Monibyte.Arquitectura.Comun.Nucleo.Trama;
namespace Monibyte.Arquitectura.Dominio.Tarjetas.MQATH
{
    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaConsultaCuentas
    {
        string _blancos = string.Empty.PadRight(38, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.CONSULTA_CTAS_CLIENTE; } set { value = TipoEntradaMsj.CONSULTA_CTAS_CLIENTE; } }
        [TramaColumnSpec(required: false, maxLength: 20, characterFill: Constantes.CARACTERBLANCO)]
        public string Nombre { get; set; }
        [TramaColumnSpec(required: false, maxLength: 15, characterFill: Constantes.CARACTERBLANCO)]
        public string Apellido1 { get; set; }
        [TramaColumnSpec(required: false, maxLength: 15, characterFill: Constantes.CARACTERBLANCO)]
        public string Apellido2 { get; set; }
        [TramaColumnSpec(required: false, maxLength: 19, characterFill: Constantes.CARACTERBLANCO, alignTo: "RIGHT")]
        public string Cedula { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 38)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaConsultaCuentas : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: false, maxLength: 20, characterFill: Constantes.CARACTERBLANCO)]
        public string Nombre { get; set; }
        [TramaColumnSpec(required: false, maxLength: 15, characterFill: Constantes.CARACTERBLANCO)]
        public string Apellido1 { get; set; }
        [TramaColumnSpec(required: false, maxLength: 15, characterFill: Constantes.CARACTERBLANCO)]
        public string Apellido2 { get; set; }
        [TramaColumnSpec(required: true, maxLength: 19, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string Cedula { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERBLANCO)]
        public string Cuenta { get; set; }
        //[TramaColumnSpec(required: false, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        //public string Usuario { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 902)]
        private string Blancos { get; set; }
    }
    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaConsultaTarjeta {
        string _entrada = " ";
        string _blancos = string.Empty.PadRight(71, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.CONSULTA; } set { value = TipoEntradaMsj.CONSULTA; } }
        [TramaColumnSpec(required: false, maxLength: 8, characterFill: Constantes.CARACTERBLANCO)]
        public string Emisor { get; set; }
        [TramaColumnSpec(required: false, maxLength: 5, characterFill: Constantes.CARACTERBLANCO)]
        public string NumCuenta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 8, format: "yyyyMMdd", 
            regex: @"^\d{4}-((0[1-9])|(1[012]))-((0[1-9]|[12]\d)|3[01])$", characterFill: Constantes.CARACTERBLANCO)]
        public DateTime? FechaCorte { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 1)]
        private string Entrada { get { return _entrada; } set { value = _entrada; } }
        [TramaColumnSpec(required: true, maxLength: 71)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }
    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaConsultaTarjeta : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4)]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: false, maxLength: 8)]
        public string Emisor { get; set; }
        [TramaColumnSpec(required: false, maxLength: 5)]
        public string NumCuenta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 16)]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy:100)]
        public decimal? SaldoLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? PagoMinLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? PagoContadoLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DisponibleLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DispAdelantoLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DisponibleLocalTarjeta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DispAdelantoLocalTarjeta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DebitosLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CreditosLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SubtSaldoCorteLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CargosBonifLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoTotalCorteLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldofPlazoLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CargosNoBonifLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? RecargoCuotaLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? TotalCalcPMLocal { get; set; }
        //------------------------------------------------------------------------------------
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? PagoMinInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? PagoContadoInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DisponibleInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DispAdelantoInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DisponibleInterTarjeta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DispAdelantoInterTarjeta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DebitosInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CreditosInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SubtSaldoCorteInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CargosBonifInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoTotalCorteInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldofPlazoInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CargosNoBonifInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? RecargoCuotaInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? TotalCalcPMInter { get; set; }
        //------------------------------------------------------------------------------------
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? LimiteCuentaLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? LimiteCuentaInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? LimiteTarjetaLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? LimiteTarjetaInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoTarjetaLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoTarjetaInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 8)]
        public DateTime? FecUltCorte { get; set; }
        [TramaColumnSpec(required: false, maxLength: 8)]
        public DateTime? FecVencimientoPago { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40)]
        public string Nombre { get; set; }
        //------------------------------------------------------------------------------------
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? LimiteEFLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoInicialEFLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DebitosEFLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CreditosEFLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoFinalEFLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DisponibleEFLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? LimiteEFInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoInicialEFInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DebitosEFInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CreditosEFInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoFinalEFInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DisponibleEFInter { get; set; }
        //------------------------------------------------------------------------------------
        [TramaColumnSpec(required: false, maxLength: 2)]
        public string EstadoCuenta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public string EstadoTarjeta { get; set; }
        //------------------------------------------------------------------------------------
        [TramaColumnSpec(required: false, maxLength: 2)]
        public int? ImportesVencLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? MonImportesVencLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public int? ImportesVencInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? MonImportesVencInter { get; set; }
        //------------------------------------------------------------------------------------
        [TramaColumnSpec(required: false, maxLength: 13)]
        public string NumExtrafinamiento { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DebitoTransitoLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CreditoTransitoLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? DebitoTransitoInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? CreditoTransitoInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 6, divideBy: 100)]
        public decimal? TasaInteresMensLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 6, divideBy: 100)]
        public decimal? TasaInteresMensInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 6, divideBy: 100)]
        public decimal? TasaMoraMensLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 6, divideBy: 100)]
        public decimal? TasaMoraMensInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoInicialCorteLocal { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100)]
        public decimal? SaldoInicialCorteInter { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
    }
}

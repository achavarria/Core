using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Autorizador
{
    public partial class MB_PaisesRegion : EntidadBaseCtrl
    {
        public int IdPaisRegion { get; set; }
        public int IdRegion { get; set; }
        public string CodPaisISO { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

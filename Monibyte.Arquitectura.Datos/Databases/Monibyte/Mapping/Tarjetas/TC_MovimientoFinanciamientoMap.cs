using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_MovimientoFinanciamientoMap : EntityTypeConfiguration<TC_MovimientoFinanciamiento>
    {
        public TC_MovimientoFinanciamientoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMovimiento);

            // Properties
            this.Property(t => t.IdMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DetalleAnulacion)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("TC_MovimientoFinanciamiento", "TCredito");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdFinanciamiento).HasColumnName("IdFinanciamiento");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
            this.Property(t => t.HoraMovimiento).HasColumnName("HoraMovimiento");
            this.Property(t => t.MonMoratorio).HasColumnName("MonMoratorio");
            this.Property(t => t.DiasMoratorio).HasColumnName("DiasMoratorio");
            this.Property(t => t.TasaMoratorio).HasColumnName("TasaMoratorio");
            this.Property(t => t.IdCuota).HasColumnName("IdCuota");
            this.Property(t => t.IdUsuarioMovimiento).HasColumnName("IdUsuarioMovimiento");
            this.Property(t => t.MonPrincipal).HasColumnName("MonPrincipal");
            this.Property(t => t.MonInteres).HasColumnName("MonInteres");
            this.Property(t => t.MonSeguro).HasColumnName("MonSeguro");
            this.Property(t => t.IdTipoMovimiento).HasColumnName("IdTipoMovimiento");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.MonPrincipalPagado).HasColumnName("MonPrincipalPagado");
            this.Property(t => t.MonInteresPagado).HasColumnName("MonInteresPagado");
            this.Property(t => t.MonSeguroPagado).HasColumnName("MonSeguroPagado");
            this.Property(t => t.MonMoratorioPagado).HasColumnName("MonMoratorioPagado");
            this.Property(t => t.FecAnulacion).HasColumnName("FecAnulacion");
            this.Property(t => t.IdUsuarioAnula).HasColumnName("IdUsuarioAnula");
            this.Property(t => t.HoraAnulacion).HasColumnName("HoraAnulacion");
            this.Property(t => t.IdMotivoAnulacion).HasColumnName("IdMotivoAnulacion");
            this.Property(t => t.DetalleAnulacion).HasColumnName("DetalleAnulacion");
            this.Property(t => t.FecCuota).HasColumnName("FecCuota");
            this.Property(t => t.IdMovimientoOrigen).HasColumnName("IdMovimientoOrigen");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

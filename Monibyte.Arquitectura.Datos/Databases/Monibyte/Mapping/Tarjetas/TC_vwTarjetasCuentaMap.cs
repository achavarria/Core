using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_vwTarjetasCuentaMap : EntityTypeConfiguration<TC_vwTarjetasCuenta>
    {
        public TC_vwTarjetasCuentaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.IdCuenta, t.IdTarjeta, t.IdTipoTarjeta, t.IdTipoPlastico, t.NumCuenta, t.NumTarjeta, t.IdEstadoCta, t.IdPersona, t.IdRelacion, t.IdTipoLimite, t.IdEsCuentaMadre, t.FecVencimiento, t.FechaVctoLineaCredito, t.SaldoLocal, t.SaldoInternacional, t.LimiteCreditoCtaLocal, t.LimiteCreditoCtaInter, t.IdCompania, t.IdUnidad, t.FecInclusionAud });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdCuenta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdTarjeta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdTipoTarjeta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdTipoPlastico)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumCuenta)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.NumTarjeta)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.IdEstadoCta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodEstadoCta)
                .HasMaxLength(20);

            this.Property(t => t.CodEstadoPago)
                .HasMaxLength(20);

            this.Property(t => t.CodEstadoTarjeta)
                .HasMaxLength(20);

            this.Property(t => t.IdPersona)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdRelacion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NombreImpreso)
                .HasMaxLength(60);

            this.Property(t => t.IdTipoLimite)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdEsCuentaMadre)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SaldoLocal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SaldoInternacional)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LimiteCreditoCtaLocal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LimiteCreditoCtaInter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdCompania)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdUnidad)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NombrePersona)
                .HasMaxLength(160);

            this.Property(t => t.NumIdentificacionPersona)
                .HasMaxLength(30);

            this.Property(t => t.NombreEmpresa)
                .HasMaxLength(160);

            this.Property(t => t.NumIdentificacionEmpresa)
                .HasMaxLength(30);

            this.Property(t => t.CorreoPersonal)
                .HasMaxLength(100);

            this.Property(t => t.CorreoLaboral)
                .HasMaxLength(100);

            this.Property(t => t.CorreoEstCuenta)
                .HasMaxLength(120);

            // Table & Column Mappings
            this.ToTable("TC_vwTarjetasCuenta", "TCredito");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.IdTipoTarjeta).HasColumnName("IdTipoTarjeta");
            this.Property(t => t.IdTipoPlastico).HasColumnName("IdTipoPlastico");
            this.Property(t => t.NumCuenta).HasColumnName("NumCuenta");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.IdEstadoCta).HasColumnName("IdEstadoCta");
            this.Property(t => t.CodEstadoCta).HasColumnName("CodEstadoCta");
            this.Property(t => t.CodEstadoPago).HasColumnName("CodEstadoPago");
            this.Property(t => t.IdEstadoTarjeta).HasColumnName("IdEstadoTarjeta");
            this.Property(t => t.CodEstadoTarjeta).HasColumnName("CodEstadoTarjeta");
            this.Property(t => t.IdGrupoEmpresa).HasColumnName("IdGrupoEmpresa");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.IdRelacion).HasColumnName("IdRelacion");
            this.Property(t => t.CamposDinamicos).HasColumnName("CamposDinamicos");
            this.Property(t => t.IdPersonaTitular).HasColumnName("IdPersonaTitular");
            this.Property(t => t.NombreImpreso).HasColumnName("NombreImpreso");
            this.Property(t => t.IdTipoLimite).HasColumnName("IdTipoLimite");
            this.Property(t => t.IdEsCuentaMadre).HasColumnName("IdEsCuentaMadre");
            this.Property(t => t.IdCuentaMadre).HasColumnName("IdCuentaMadre");
            this.Property(t => t.FecVencimiento).HasColumnName("FecVencimiento");
            this.Property(t => t.FechaVctoLineaCredito).HasColumnName("FechaVctoLineaCredito");
            this.Property(t => t.FecUltCorte).HasColumnName("FecUltCorte");
            this.Property(t => t.SaldoLocal).HasColumnName("SaldoLocal");
            this.Property(t => t.SaldoInternacional).HasColumnName("SaldoInternacional");
            this.Property(t => t.SaldoTarjetaLocal).HasColumnName("SaldoTarjetaLocal");
            this.Property(t => t.SaldoTarjetaInter).HasColumnName("SaldoTarjetaInter");
            this.Property(t => t.LimiteCreditoCtaGlobal).HasColumnName("LimiteCreditoCtaGlobal");
            this.Property(t => t.LimiteCreditoCtaLocal).HasColumnName("LimiteCreditoCtaLocal");
            this.Property(t => t.LimiteCreditoCtaInter).HasColumnName("LimiteCreditoCtaInter");
            this.Property(t => t.LimiteCreditoTarjetaLocal).HasColumnName("LimiteCreditoTarjetaLocal");
            this.Property(t => t.LimiteCreditoTarjetaInter).HasColumnName("LimiteCreditoTarjetaInter");
            this.Property(t => t.IdCompania).HasColumnName("IdCompania");
            this.Property(t => t.IdUnidad).HasColumnName("IdUnidad");
            this.Property(t => t.FecActivacion).HasColumnName("FecActivacion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.NombrePersona).HasColumnName("NombrePersona");
            this.Property(t => t.NumIdentificacionPersona).HasColumnName("NumIdentificacionPersona");
            this.Property(t => t.NombreEmpresa).HasColumnName("NombreEmpresa");
            this.Property(t => t.NumIdentificacionEmpresa).HasColumnName("NumIdentificacionEmpresa");
            this.Property(t => t.CorreoPersonal).HasColumnName("CorreoPersonal");
            this.Property(t => t.CorreoLaboral).HasColumnName("CorreoLaboral");
            this.Property(t => t.CorreoEstCuenta).HasColumnName("CorreoEstCuenta");
            this.Property(t => t.IdTipoGrupo).HasColumnName("IdTipoGrupo");
            this.Property(t => t.LimiteCreditoAjustado).HasColumnName("LimiteCreditoAjustado");
        }
    }
}

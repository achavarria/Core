using Monibyte.Arquitectura.Dominio.Seguridad;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Seguridad
{
    public class SC_AccionesRoleMap : EntityTypeConfiguration<SC_AccionesRole>
    {
        public SC_AccionesRoleMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdRole, t.IdAccion });

            // Properties
            this.Property(t => t.IdRole)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdAccion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("SC_AccionesRole", "Seguridad");
            this.Property(t => t.IdRole).HasColumnName("IdRole");
            this.Property(t => t.IdAccion).HasColumnName("IdAccion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

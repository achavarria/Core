﻿using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Monibyte.Arquitectura.Servicios.Web.Clientes
{
    [ServiceContract]
    public interface IServiciosClientes : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarPersonasBuscadas")]
        Transporte ListarPersonasBuscadas(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerInfoPersona")]
        Transporte ObtenerInfoPersona(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListaProfesiones")]
        Transporte ListaProfesiones(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "GuardarPersona")]
        Transporte GuardarPersona(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ActualizarPersona")]
        Transporte ActualizarPersona(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ComprobarIdentificacion")]
        Transporte ComprobarIdentificacion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminarPersona")]
        Transporte EliminarPersona(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtienePerfil")]
        Transporte ObtienePerfil(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ActualizarPerfilEmpresa")]
        Transporte ActualizarPerfilEmpresa(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerEjecutivos")]
        Transporte ObtenerEjecutivos(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerListaEjecutivos")]
        Transporte ObtenerListaEjecutivos(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminarEjecutivos")]
        Transporte EliminarEjecutivos(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "InsertarEjecutivos")]
        Transporte InsertarEjecutivos(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ActualizarEjecutivos")]
        Transporte ActualizarEjecutivos(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AsociarEjecutivo",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        void AsociarEjecutivo(int[] empresas, int? idEjecutivo);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ObtenerCorreoPersona",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        string ObtenerCorreoPersona(int idPersona, string codUsuario);
    }
}


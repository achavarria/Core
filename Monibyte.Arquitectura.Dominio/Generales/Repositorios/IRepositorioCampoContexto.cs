﻿using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using System.Linq;

namespace Monibyte.Arquitectura.Dominio.Generales.Repositorios
{
    public interface IRepositorioCampoContexto : IRepositorioMnb<GL_CamposContexto>
    {
        IQueryable<GL_CamposContexto> List(int idContexto);
        IQueryable<GL_CamposContexto> List(Criteria<GL_CamposContexto> criteria);
    }
}


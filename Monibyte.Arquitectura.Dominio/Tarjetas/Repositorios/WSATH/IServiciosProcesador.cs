﻿using Monibyte.Arquitectura.Dominio.Tarjetas.WSATH;

namespace Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios.WSATH
{
    public interface IServiciosProcesador
    {
        EntradaVip ActivarVip(EntradaVip paramsVip, string seccion = "");
        EntradaVip InactivarVip(EntradaVip paramsVip, string secccion = "");
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using System;

namespace Monibyte.Arquitectura.Dominio
{
    public class Context
    {
        public static PocInfoSesion User { get { return InfoSesion.Info; } }

        public static Language Lang { get { return InfoSesion.Lang; } }

        public static Language DefaultLang { get { return Config.LANG_DEFAULT; } }

        public static bool IsEditor
        {
            get
            {
                return (InfoSesion.Info.RequiereAutorizacion &&
                    InfoSesion.Info.IdRoleOperativo == (int)EnumRoleOperativo.Editor ||
                    InfoSesion.ObtenerSesion<bool?>("AUTORIZAPRODUCTO") == false);
            }
        }

        public static TipoUsuario UserType
        {
            get
            {
                if (InfoSesion.Info != null)
                {
                    var rep = GestorUnity.CargarUnidad<IRepositorioCatalogo>();
                    var cat = rep.SelectById(InfoSesion.Info.IdTipoUsuario);

                    return new TipoUsuario
                    {
                        MultiEntidad = cat.Referencia4.Contains("ME"),
                        PorUnidad = cat.Referencia4.Contains("UN"),
                        IdSistema = Int32.Parse(cat.Referencia1),
                        Descripcion = cat.Descripcion,
                        Codigo = cat.Codigo,
                        Lista = cat.Lista
                    };
                }
                return null;
            }
        }
    }
}

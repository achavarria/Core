using Monibyte.Arquitectura.Dominio.Generales;
using System;

namespace Monibyte.Arquitectura.Dominio.Configuracion
{
    public partial class MB_RegionPaisPlantilla : EntidadBase
    {
        public int IdRegionPais { get; set; }
        public int IdPlantilla { get; set; }
        public Nullable<int> IdPais { get; set; }
        public Nullable<int> IdRegion { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

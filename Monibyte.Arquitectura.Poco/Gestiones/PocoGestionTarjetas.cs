﻿using Monibyte.Arquitectura.Poco.Clientes;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.Gestiones
{
    public class PocGestionAdicionales
    {
        public int IdGestion { get; set; }
        public int IdEmpresa { get; set; }
        public int IdProducto { get; set; }
        public int? TipoFiltro { get; set; }
        public string NumProducto { get; set; }
        public List<PocTarjetaAdicional> Tarjetas { get; set; }
        public int IdSucursal { get; set; }
        public int? IdEjecutivo { get; set; }
        public string Sucursal { get; set; }
        public string Ejecutivo { get; set; }
        public bool EsAutorizacion { get; set; }
        public string Observaciones { get; set; }
    }

    public class PocTarjetaAdicional
    {
        public string Identificacion { get; set; }
        public int IdMoneda { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string TipoTarjeta { get; set; }
        public string Moneda { get; set; }
        public int IdIdentificacion { get; set; }
        public int IdCuenta { get; set; }
        public decimal Monto { get; set; }
        public string Justificacion { get; set; }
        public string Estado { get; set; }
        public int IdEstado { get; set; }
        public DateTime FecNacimiento { get; set; }
        public int IdGenero { get; set; }
        public int IdEstadoCivil { get; set; }
        public int IdPaisNacimiento { get; set; }
        public int IdProfesion { get; set; }
        public string Genero { get; set; }
        public string NumCuenta { get; set; }
        public string EstadoCivil { get; set; }
        public string Profesion { get; set; }
        public string TipoIdentificacion { get; set; }
        public DateTime FecVencimientoCedula { get; set; }

        public PocDireccion Direccion { get; set; }
        public PocComunicacion Comunicacion { get; set; }
    }

    public class PocCambioEstadoGestionTarjeta : PocCambioEstadoGestion
    {
        public int? IdDiaCorte { get; set; }
        public decimal? LimiteCuenta { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_TipoTarjetaMap : EntityTypeConfiguration<TC_TipoTarjeta>
    {
        public TC_TipoTarjetaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipoTarjeta);

            // Properties
            this.Property(t => t.IdTipoTarjeta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.ConfiguracionProcesador)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MensajeEstadoCuenta)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("TC_TipoTarjeta", "TCredito");
            this.Property(t => t.IdTipoTarjeta).HasColumnName("IdTipoTarjeta");
            this.Property(t => t.IdClase).HasColumnName("IdClase");
            this.Property(t => t.IdAlcance).HasColumnName("IdAlcance");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.Bin).HasColumnName("Bin");
            this.Property(t => t.IdEntidad).HasColumnName("IdEntidad");
            this.Property(t => t.IdMarca).HasColumnName("IdMarca");
            this.Property(t => t.IdMonedaLimite).HasColumnName("IdMonedaLimite");
            this.Property(t => t.MonLimiteMinimo).HasColumnName("MonLimiteMinimo");
            this.Property(t => t.MonLimiteMaximo).HasColumnName("MonLimiteMaximo");
            this.Property(t => t.MonIngresoMínimo).HasColumnName("MonIngresoMínimo");
            this.Property(t => t.PlazoVctoLineaCredito).HasColumnName("PlazoVctoLineaCredito");
            this.Property(t => t.TasaInteresLocal).HasColumnName("TasaInteresLocal");
            this.Property(t => t.TasaInteresInternacional).HasColumnName("TasaInteresInternacional");
            this.Property(t => t.TasaIntMoraLocal).HasColumnName("TasaIntMoraLocal");
            this.Property(t => t.TasaIntMoraInternacional).HasColumnName("TasaIntMoraInternacional");
            this.Property(t => t.TasaIntCJInternacional).HasColumnName("TasaIntCJInternacional");
            this.Property(t => t.TasaIntCJLocal).HasColumnName("TasaIntCJLocal");
            this.Property(t => t.TasaIntraFinLocal).HasColumnName("TasaIntraFinLocal");
            this.Property(t => t.TasaIntraFinInternacional).HasColumnName("TasaIntraFinInternacional");
            this.Property(t => t.DiasGraciaMora).HasColumnName("DiasGraciaMora");
            this.Property(t => t.MonCargoAdminAtraso).HasColumnName("MonCargoAdminAtraso");
            this.Property(t => t.IdExcCargoAdmin).HasColumnName("IdExcCargoAdmin");
            this.Property(t => t.MonMaxReversionCargo).HasColumnName("MonMaxReversionCargo");
            this.Property(t => t.PorcSobregiroAutorizado).HasColumnName("PorcSobregiroAutorizado");
            this.Property(t => t.MinDiasSobregiro).HasColumnName("MinDiasSobregiro");
            this.Property(t => t.PlazoIntraFinLocal).HasColumnName("PlazoIntraFinLocal");
            this.Property(t => t.PlazoIntraFinInternacional).HasColumnName("PlazoIntraFinInternacional");
            this.Property(t => t.ConfiguracionProcesador).HasColumnName("ConfiguracionProcesador");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.MensajeEstadoCuenta).HasColumnName("MensajeEstadoCuenta");
            this.Property(t => t.IdAplicaMillas).HasColumnName("IdAplicaMillas");

            this.Property(t => t.IdTipoSaldoAFavor).HasColumnName("IdTipoSaldoAFavor");
            this.Property(t => t.SaldoAFavor).HasColumnName("SaldoAFavor");
        }
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Clientes
{
    public partial class CL_Profesion : EntidadBase
    {
        public int IdProfesion { get; set; }
        public string Profesion { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

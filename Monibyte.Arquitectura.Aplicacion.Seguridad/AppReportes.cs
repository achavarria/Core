﻿using Monibyte.Arquitectura.Comun.Nucleo.Encryption;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public class AppReportes : AplicacionBase, IAppReportes
    {
        #region CONSTRUCTOR UNITY
        private IDirectorioUsuarioApp _directorioUsuario;

        public AppReportes(
            IDirectorioUsuarioApp directorioUsuario)
        {
            _directorioUsuario = directorioUsuario;
        }
        #endregion

        public void GenerarReporteSeguridad(string codUsuario, string nombreCompleto,
            int filtro, string nombreEmpresa, int? tipoPersona)
        {
            var contrasena = new PocCredencialesDirectorio();
            var token = new PocDobleFactorAut();
            using (var ts = new TransactionScope())
            {
                if (filtro != (int)EnumDatosComprobante.Token)
                {
                    contrasena = _directorioUsuario.ReestablecerContrasena(codUsuario);
                }
                if (filtro != (int)EnumDatosComprobante.Password)
                {
                    token = _directorioUsuario.GenerarToken(codUsuario);
                }
                //Get PDF -- Incluir cambio de estado
                _directorioUsuario.CambiarEstado(codUsuario, (int)EnumEstadoUsuario.Activo);

                var url = RestConfig.Get("ApiCore", "General", "InsertarColaImpresion");
                RestClient.CoreRequest(url, new
                {
                    CodUsuario = codUsuario,
                    Contenido = "NULO",
                    Formato = "PDF",
                    IdEstado = (int)EnumIdEstado.Activo,
                    NombreEmpresa = nombreEmpresa,
                    IdTipoPersona = tipoPersona,
                    Clave = contrasena.Password.Decrypt(),
                    PIN = token.ManualEntrySetupCode,
                    Semilla = "",
                    NombreCompleto = nombreCompleto,
                    IdCompania = InfoSesion.Info.IdCompania
                });
                ts.Complete();
            }
        }
    }
}

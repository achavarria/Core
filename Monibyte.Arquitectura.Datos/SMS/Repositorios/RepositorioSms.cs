﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Dominio.SMS;
using Monibyte.Arquitectura.Dominio.SMS.Repositorios;

namespace Monibyte.Arquitectura.Datos.SMS.Repositorios
{
    public class RepositorioMensajesRespuesta : RepositorioMnb<AC_MENSAJES_RESPUESTA>, IRepositorioMensajesRespuesta
    {
        public RepositorioMensajesRespuesta(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }
    }
    public class RepositorioMensajesSms : RepositorioMnb<AC_MENSAJES_SMS>, IRepositorioMensajesSms
    {
        public RepositorioMensajesSms(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }
    }
}

﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Efectivo;
using Monibyte.Arquitectura.Dominio.Efectivo;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<EF_ArchivoMovimiento> EF_ArchivoMovimiento { get; set; }
        public DbSet<EF_Movimiento> EF_Movimiento { get; set; }
        public DbSet<EF_TarifaKilometraje> EF_TarifaKilometraje { get; set; }
        public DbSet<EF_vwMovimientosEfectivo> EF_vwMovimientosEfectivo { get; set; }
        public DbSet<EF_ArchivoTarjeta> EF_ArchivoTarjeta { get; set; }

        void CreateContextModelEF(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EF_ArchivoMovimientoMap());
            modelBuilder.Configurations.Add(new EF_MovimientoMap());
            modelBuilder.Configurations.Add(new EF_TarifaKilometrajeMap());
            modelBuilder.Configurations.Add(new EF_vwMovimientosEfectivoMap());
            modelBuilder.Configurations.Add(new EF_ArchivoTarjetaMap());
        }
    }
}

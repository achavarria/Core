﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Generales
{
    public interface IParametroSistema : IAplicacionBase
    {
        IEnumerable<PocParametroSistema> ListaParametroSistema();
        PocParametroSistema ObtenerParametroSistema(PocParametroSistema pocEntrada);
        void InsertaParametroSistema(PocParametroSistema pocEntrada);
        void ActualizaParametroSistema(PocParametroSistema pocEntrada);
        void EliminaParametroSistema(PocParametroSistema pocEntrada);
    }
}

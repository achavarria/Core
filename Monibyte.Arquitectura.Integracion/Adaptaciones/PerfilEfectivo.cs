﻿using AutoMapper;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Poco.Efectivo;

namespace Monibyte.Arquitectura.Servicios.Web.AdaptadorEfectivo
{
    class PerfilEfectivo : Profile
    {
        protected override void Configure()
        {
            var mvsEfec = Mapper.CreateMap<PocMovimientosEfectivo, EF_Movimiento>();
            mvsEfec.ForMember(ent => ent.CamposDinamicos, poc => poc.MapFrom(mod => mod.DynamicJson));
        }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Poco.Quickpass;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public class AppListaTags : AplicacionBase, IAppListaTags
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<QP_Quickpass> _repQuickpass;
        private IRepositorioMnb<QP_ListaTags> _repListaTags;

        public AppListaTags(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<QP_Quickpass> repQuickpass,
            IRepositorioMnb<QP_ListaTags> repListaTags)
        {
            _repCatalogo = repCatalogo;
            _repQuickpass = repQuickpass;
            _repListaTags = repListaTags;
        }

        public DataResult<PocListaTagsTabla> Listar(PocListaTagsTabla filtro, DataRequest request)
        {
            var criteria = new Criteria<QP_ListaTags>();
            var resultado = _repListaTags.SelectBy(criteria)
                .Join(_repCatalogo.List("LTIPOLISTAQP"),
                    t => t.IdTipoLista,
                    c => c.IdCatalogo,
                    (t, c) => new { t, c })
                .Join(_repQuickpass.Table,
                    item => item.t.IdQuickpass,
                    qp => qp.IdQuickpass,
                    (item, qp) => new { item.c, item.t, qp })
                    .Select(res => new PocListaTagsTabla
                    {
                        numQuickpass = res.qp.NumQuickpass,
                        secGeneracion = res.t.IdSecuencia,
                        tipoLista = (int)res.t.IdTipoLista,
                        descTipoLista = res.c.Descripcion,
                        fechaGeneracion = res.t.FechaGeneracion.Value.Date,
                        fechahora = res.t.FechaGeneracion.Value.ToString("yyyyMMddHHmmss")
                    });
            return resultado.ToDataResult(request);
        }
    }
}

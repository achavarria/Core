using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_TipoLicenciaMap : EntityTypeConfiguration<MB_TipoLicencia>
    {
        public MB_TipoLicenciaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipoLicencia);

            // Properties
            this.Property(t => t.IdTipoLicencia)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("MB_TipoLicencia", "Mnb");
            this.Property(t => t.IdTipoLicencia).HasColumnName("IdTipoLicencia");
            this.Property(t => t.IdTipoCargo).HasColumnName("IdTipoCargo");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

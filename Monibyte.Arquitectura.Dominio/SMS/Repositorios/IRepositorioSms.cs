﻿
namespace Monibyte.Arquitectura.Dominio.SMS.Repositorios
{
    public interface IRepositorioMensajesRespuesta : IRepositorioMnb<AC_MENSAJES_RESPUESTA> { }
    public interface IRepositorioMensajesSms : IRepositorioMnb<AC_MENSAJES_SMS> { }
}

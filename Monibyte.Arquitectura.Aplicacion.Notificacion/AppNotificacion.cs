﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Procesador;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Notificacion;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Poco.Notificacion;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public partial class AppNotificacion : AppTraductorFiltro, IAppNotificacion
    {
        private IAppAutorizaciones _appAutorizaciones;
        private IRepositorioMnb<TC_Cuenta> _repCuenta;
        private IRepositorioMnb<GL_Usuario> _repUsuario;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<TC_Tarjeta> _repTarjeta;
        private IRepositorioNotificacion _repNotificacion;
        private IRepositorioMnb<CL_Comunicacion> _repComunicacion;
        private IRepositorioMnb<CL_EmpresasUsuario> _repEmpresaUsuario;
        private IRepositorioMnb<NT_IndicadorEnvio> _repNTIndicadorEnvio;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _repvwEmpresaUsuario;

        public AppNotificacion(
            IRepositorioCatalogo repCatalogo,
            IAppAutorizaciones appAutorizaciones,
            IRepositorioMnb<TC_Cuenta> repCuenta,
            IRepositorioMnb<GL_Usuario> repUsuario,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<TC_Tarjeta> repTarjeta,
            IRepositorioNotificacion repNotificacion,
            IRepositorioMnb<CL_Comunicacion> repComunicacion,
            IRepositorioMnb<CL_EmpresasUsuario> repEmpresaUsuario,
            IRepositorioMnb<NT_IndicadorEnvio> repNTIndicadorEnvio,
            IRepositorioMnb<CL_vwEmpresasUsuario> repvwEmpresaUsuario
        )
            : base()
        {
            _repCuenta = repCuenta;
            _repTarjeta = repTarjeta;
            _repPersona = repPersona;
            _repUsuario = repUsuario;
            _repCatalogo = repCatalogo;
            _repNotificacion = repNotificacion;
            _repComunicacion = repComunicacion;
            _repEmpresaUsuario = repEmpresaUsuario;
            _appAutorizaciones = appAutorizaciones;
            _repNTIndicadorEnvio = repNTIndicadorEnvio;
            _repvwEmpresaUsuario = repvwEmpresaUsuario;
        }

        public IEnumerable<PocConfigNotificacion> ObtenerNotificaciones(int? idContexto, int? idEmpresa, int? idEsActivo, int ExcluirAdminInt = 1)
        {
            return _repNotificacion.ObtenerNotificaciones(idContexto, idEmpresa, idEsActivo, ExcluirAdminInt).Select(item =>
            {
                var not = item.ProyectarComo<PocConfigNotificacion>();
                not.DetalleFiltro = Traducir(item.DetalleFiltro);
                not.Id = Guid.NewGuid().ToString("N");
                not.Group = (not.AplicaUsuario == true ? 1 : 0) + "-" +
                    (not.AplicaTarjetaHabiente == true ? 1 : 0);
                return not;
            }).ToList();
        }

        public IEnumerable<PocConfigNotificacion> EstablecerNotificacion(List<PocConfigNotificacion> notificaciones)
        {
            if (notificaciones == null)
            {
                throw new CoreException("Datos nulos al guardar la notificacion", "Err_0035");
            }
            //traducir los datos del detalle y establecerlos de acuerdo al tipo
            using (var ts = new TransactionScope())
            {
                notificaciones.ForEach(notificacion =>
                {
                    var validacion = Validar(notificacion);
                    if (validacion.IsValid)
                    {
                        var _record = notificacion.ProyectarComo<NT_Configuracion>();
                        _record.DetalleFiltro = Traducir(notificacion.DetalleFiltro);

                        if (!notificacion.IdConfiguracion.HasValue)
                        {
                            var _notifyId = _fGenerales.ObtieneSgteSecuencia("SEQ_IdNotificacion");
                            notificacion.IdConfiguracion = _record.IdConfiguracion = _notifyId;
                            notificacion.IdEmpresa = _record.IdEmpresa;
                            _repNotificacion.Insert(_record);
                            _repNotificacion.UnidadTbjo.Save();
                        }
                        else
                        {
                            _repNotificacion.UpdateOn(n => n.IdConfiguracion == notificacion.IdConfiguracion,
                                up => new NT_Configuracion
                                {
                                    IdEstado = notificacion.IdEstado,
                                    Destinatario = _record.Destinatario,
                                    DetalleFiltro = _record.DetalleFiltro,
                                    FecVigenciaHasta = _record.FecVigenciaHasta,
                                    IdNotificaAdministradoresSms = _record.IdNotificaAdministradoresSms,
                                    IdNotificaAdministradoresEmail = _record.IdNotificaAdministradoresEmail,
                                    IdNotificaTarjetaHabienteSms = _record.IdNotificaTarjetaHabienteSms,
                                    IdNotificaTarjetaHabienteEmail = _record.IdNotificaTarjetaHabienteEmail,
                                    IdNotificaUsuarioSms = _record.IdNotificaUsuarioSms,
                                    IdNotificaUsuarioEmail = _record.IdNotificaUsuarioEmail
                                });
                        }
                    }
                    else
                    {
                        var msj = string.Format("Error al Insertar {0}", typeof(PocConfigNotificacion));
                        throw new CoreException(msj, validation: validacion);
                    }
                });
                ts.Complete();
            }
            return notificaciones;
        }

        private ValidationResponse Validar(PocConfigNotificacion notificacion)
        {
            var validacion = new ValidationResponse();
            if (notificacion.IdEstado == (int)EnumIdEstado.Activo)
            {
                if (notificacion.FecVigenciaHasta < DateTime.Today)
                {
                    validacion.AddError("Err_0112");
                }
                if (!notificacion.Destinatarios.IsNullOrEmpty())
                {
                    notificacion.Destinatarios.Update(x =>
                    {
                        var r = x.Valores.ToList();
                        r.RemoveAll(v => string.IsNullOrEmpty(v));
                        x.Valores = r.ToArray();
                        if (x.Canal == CanalNotificacion.Email)
                        {
                            x.Valores.ToList().ForEach(email =>
                            {
                                try { new System.Net.Mail.MailAddress(email); }
                                catch { validacion.AddError("Err_0122", email); }
                            });
                        }
                    });
                    notificacion.Destinatarios.RemoveAll
                        (x => x.Valores.IsNullOrEmpty());
                }
            }
            return validacion;
        }

        public IEnumerable<PocDestinatarioDefault> ObtenerDestinatarios(int idEmpresa)
        {
            var result =
                from us in _repUsuario.Table
                join pe in _repPersona.Table on
                    us.IdPersona equals pe.IdPersona
                join eu in _repEmpresaUsuario.Table on
                    us.IdUsuario equals eu.IdUsuario
                join ct in _repCatalogo.Table on
                    eu.IdTipoUsuario equals ct.IdCatalogo
                let em = _repComunicacion.Table.Where(x =>
                    x.IdPersona == us.IdPersona &&
                    x.IdTipo == (int)EnumTipoComunicacion.Correo
                ).FirstOrDefault()
                let ce = _repComunicacion.Table.Where(x =>
                    x.IdPersona == us.IdPersona &&
                    x.IdTipo == (int)EnumTipoComunicacion.Celular
                ).FirstOrDefault()
                where eu.IdEmpresa == idEmpresa &&
                    (em != null || ce != null) &&
                    ct.Referencia3 == "Adm"
                select new PocDestinatarioDefault
                {
                    IdPersona = us.IdPersona,
                    Nombre = pe.NombreCompleto,
                    Correo = em != null ? em.ValComunicacion : null,
                    Celular = ce != null ? ce.ValComunicacion : null
                };
            return result.ToList();
        }

        public void ProcesarNotificaciones(PocoQueue pocoQueue)
        {
            if (pocoQueue != null && pocoQueue.EntityId != 0 && !string.IsNullOrEmpty(pocoQueue.MessageType))
            {
                typeof(AppNotificacion).GetMethod("Method" + pocoQueue.MessageType, BindingFlags.Instance | BindingFlags.Public).Invoke(this, new object[] { pocoQueue });
            }
        }

        private void ProcesarNotificaciones(PocoQueue pocoQueue, bool filter, bool validateSend, params int[] companyIds)
        {
            var companyNots = new List<PocConfigNotificacion>();
            var dataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(pocoQueue.Data);
            dataDictionary.Add("Companies", companyIds);
            foreach (var companyId in companyIds)
            {
                var nots = ObtenerNotificaciones(pocoQueue.OriginId, companyId, (int)EnumIdEstado.Activo, 0);
                nots.Update(x => x.IdEmpresa = companyId);
                companyNots.AddRange(nots);
            }
            var groupedNots = companyNots.GroupBy(x => new { x.IdContexto, x.SubContexto });
            foreach (var group in groupedNots)
            {
                var notificationList = group.ToArray();
                var addressList = pocoQueue.Recipients ?? new List<PocoQueueDestinatario> { 
                    new PocoQueueDestinatario { Canal = CanalNotificacion.Email, Valores = new PocoQueueDestinatarioValor[0] }, 
                    new PocoQueueDestinatario { Canal = CanalNotificacion.Sms, Valores = new PocoQueueDestinatarioValor[0] }
                };

                var _pocoQueueSend = pocoQueue.GetClone();
                foreach (var not in notificationList)
                {
                    var date = System.DateTime.Now.Date;
                    if (not.FecVigenciaHasta.HasValue && (date > ((DateTime)not.FecVigenciaHasta).Date))
                    {
                        break;
                    }
                    bool _validated = true;
                    if (filter)
                    {
                        var conditionSql = ToSqlStr(not.DetalleFiltro);
                        conditionSql = ConstruirWhere(conditionSql, dataDictionary, _pocoQueueSend);
                        _validated = _repNotificacion.ValidarFiltroNotificacion(conditionSql) == (int)EnumIdEstado.Activo;
                        #region ActualizaIndicadorEnvio
                        if (validateSend)
                        {
                            using (var ts = new TransactionScope())
                            {
                                NT_IndicadorEnvio ntIndicador = new NT_IndicadorEnvio()
                                {
                                    IdEntidad = Convert.ToInt16(dataDictionary["IdEntidad"].ToString()),
                                    IdEmpresa = Convert.ToInt16(dataDictionary["IdEmpresa"].ToString()),
                                    IdProducto = Convert.ToInt16(dataDictionary["IdCuenta"].ToString()),
                                    IdContexto = _pocoQueueSend.OriginId,
                                    IdValContexto = _pocoQueueSend.OriginIdVal,
                                    SubContexto = _pocoQueueSend.SubOrigin ?? null,
                                    Notificar = _validated ? (int)EnumIdSiNo.No : (int)EnumIdSiNo.Si
                                };
                                _repNTIndicadorEnvio.Update(ntIndicador, x => x.Notificar);
                                _repNTIndicadorEnvio.UnidadTbjo.Save();
                                ts.Complete();
                            }
                        }
                        #endregion
                    }
                    if (_validated)
                    {
                        _pocoQueueSend.SubOrigin = _pocoQueueSend.SubOrigin ?? not.SubContexto;
                         _pocoQueueSend.UserCode = _pocoQueueSend.UserCode ?? (Context.User != null ? Context.User.CodUsuario : null);

                        var idEmpresa = not.IdEmpresa.Value;
                        var notAdmSms = not.NotificaAdministradoresSms;
                        var notAdmEmail = not.NotificaAdministradoresEmail;
                        var notTarSms = not.NotificaTarjetaHabienteSms;
                        var notTarEmail = not.NotificaTarjetaHabienteEmail;
                        var notUsrSms = not.NotificaUsuarioSms;
                        var notUsrEmail = not.NotificaUsuarioEmail;
                        var codUsr = _pocoQueueSend.UserCode;
                        var numTar = dataDictionary.ContainsKey("CardNumber") ? dataDictionary["CardNumber"] as string : null;
                        var resultado = _repNotificacion.ObtenerDestinatarios(idEmpresa, codUsr, numTar,
                            notAdmSms, notAdmEmail, notTarSms, notTarEmail, notUsrSms, notUsrEmail);
                        if (resultado != null && resultado.Any())
                        {
                            addressList.Update(x =>
                            {
                                x.Valores = x.Valores.Concat(resultado.Select(y => new PocoQueueDestinatarioValor
                                {
                                    Nombre = y.Nombre,
                                    Valor = x.Canal == CanalNotificacion.Email ? y.Correo : y.Celular
                                })).ToArray();
                                x.Valores = x.Valores.Where(z => !string.IsNullOrEmpty(z.Valor))
                                    .DistinctBy(z => z.Valor).ToArray();
                            });
                        }
                        if (not.Destinatarios != null && not.Destinatarios.Any())
                        {
                            addressList.Update(x =>
                            {
                                var _nArry = not.Destinatarios.FirstOrDefault(y => x.Canal == y.Canal);
                                if (_nArry != null)
                                {
                                    var _mArray = x.Valores.Select(y => y.Valor);
                                    var _vArry = _nArry.Valores.Where(y => !_mArray.Contains
                                        (y, StringComparer.CurrentCultureIgnoreCase));
                                    x.Valores = x.Valores.Concat(_vArry.Select(y => new PocoQueueDestinatarioValor
                                    {
                                        Valor = y
                                    })).ToArray();
                                }
                            });
                        }
                    }
                }
                if (addressList.Any(x => x.Valores.Any()))
                {
                    _pocoQueueSend.Recipients = addressList;
                    _pocoQueueSend.Language = _pocoQueueSend.Language ?? ((InfoSesion.Lang ?? Config.LANG_DEFAULT).Code);
                    ThreadPool.QueueUserWorkItem(state => EnviarNotificacion(_pocoQueueSend));
                }
            }
        }

        private string ConstruirWhere(string conditionSql, Dictionary<string, object> data, PocoQueue pocoQueue)
        {
            foreach (KeyValuePair<string, object> entry in data)
            {
                string valueStr = null;
                var keyStr = string.Format("@@{0}", entry.Key);
                if (entry.Value is string)
                {
                    valueStr = "'" + entry.Value.ToString() + "'";
                }
                else if (entry.Value is DateTime)
                {
                    var value = entry.Value as DateTime?;
                    valueStr = "'" + value + "'";
                }
                else if (entry.Value is Double)
                {
                    var value = entry.Value as Double?;
                    valueStr = value.Value.ToString(CultureInfo.InstalledUICulture);
                }
                else if (entry.Value is Int64 || entry.Value is Int32 || entry.Value is Int16 || entry.Value is int)
                {
                    var value = entry.Value as Int64?;
                    valueStr = value.Value.ToString();
                }
                conditionSql = conditionSql.Replace(keyStr, valueStr);
            }
            conditionSql = conditionSql.Replace("@@OriginId", pocoQueue.OriginId.ToString());
            conditionSql = conditionSql.Replace("@@SubOrigin", string.IsNullOrEmpty(pocoQueue.SubOrigin) ? "NULL" : pocoQueue.SubOrigin);
            return conditionSql;
        }

        public void EnviarNotificacion(PocoQueue pocoQueue)
        {
            try
            {
                var dataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(pocoQueue.Data);
                if (!dataDictionary.ContainsKey("LocalTransactionDate"))
                {
                    dataDictionary["LocalTransactionDate"] = DateTime.Now.Date.ToString("yyyyMMdd");

                }
                if (!dataDictionary.ContainsKey("LocalTransactionTime"))
                {
                    dataDictionary["LocalTransactionTime"] = DateTime.Now.TimeOfDay.ToString("hhmmss");
                }
                if (dataDictionary.ContainsKey("CardNumber"))
                {
                    var numTarjeta = dataDictionary["CardNumber"].ToString();
                    dataDictionary["CardNumber"] = numTarjeta.Substring(numTarjeta.Length - 4);
                }
                pocoQueue.Data = dataDictionary.ToJson();
                pocoQueue.EntityId = pocoQueue.EntityId;
                RestClient.Post<object>("ApiNotification", "notifications", pocoQueue);
            }
            catch (Exception ex)
            {
                Logger.Log(string.Format("Error: {0} / PocoQueue: {1}", ex.FullStackTrace(), pocoQueue.ToJson()),
                    titulo: "EnviarNotificacion", idEvento: 200, severity: System.Diagnostics.TraceEventType.Error);
            }
        }
    }
}

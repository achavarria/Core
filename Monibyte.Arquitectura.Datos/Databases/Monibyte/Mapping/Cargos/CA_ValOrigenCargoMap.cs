using Monibyte.Arquitectura.Dominio.Cargos;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Cargos
{
    public class CA_ValOrigenCargoMap : EntityTypeConfiguration<CA_ValOrigenCargo>
    {
        public CA_ValOrigenCargoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdTipoCargo, t.IdValOrigen });

            // Properties
            this.Property(t => t.IdTipoCargo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdValOrigen)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CA_ValOrigenCargo", "Cargos");
            this.Property(t => t.IdTipoCargo).HasColumnName("IdTipoCargo");
            this.Property(t => t.IdValOrigen).HasColumnName("IdValOrigen");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");

        }
    }
}

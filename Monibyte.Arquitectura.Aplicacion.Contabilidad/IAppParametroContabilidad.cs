﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Contabilidad;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Contabilidad
{
    public interface IAppParametroContabilidad : IAplicacionBase
    {
        void Insertar(PocParametro registro);
        void Eliminar(PocParametro registro);
        void Modificar(PocParametro registro);
        DataResult<PocParametro> Listar(PocParametro filtro, DataRequest request);

        IEnumerable<PocParametro> ListaCustomer();
        IEnumerable<PocParametro> ListaAccount();
        IEnumerable<PocParametro> ListaItem();
    }
}

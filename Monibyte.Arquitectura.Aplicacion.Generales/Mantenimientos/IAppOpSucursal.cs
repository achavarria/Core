﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;


namespace Monibyte.Arquitectura.Aplicacion.Generales.Mantenimientos
{
    public interface IAppOpSucursal : IAplicacionBase
    {
        void Insertar(PocSucursal registro);
        void Eliminar(PocSucursal registro);
        void Modificar(PocSucursal registro);
        DataResult<PocSucursal> Listar(PocSucursal filtro, DataRequest request);
        PocSucursal ObtenerSucursal(int idSucursal);
        IEnumerable<PocSucursal> ObtenerSucursales();
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_ColaImpresion : EntidadBase
    {
        public int IdCola { get; set; }
        public string Formato { get; set; }
        public string Contenido { get; set; }
        public string NombreCompleto { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string CodUsuario { get; set; }
        public string Clave { get; set; }
        public string PIN { get; set; }
        public string Semilla { get; set; }
        public string NombreEmpresa { get; set; }
        public Nullable<int> IdTipoPersona { get; set; }
    }
}

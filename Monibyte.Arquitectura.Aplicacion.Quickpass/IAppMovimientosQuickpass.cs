﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Quickpass;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public interface IAppMovimientosQuickpass : IAplicacionBase
    {
        void Insertar(PocQPMovimientos registro);
        void Eliminar(PocQPMovimientos registro);
        void Modificar(PocQPMovimientos registro);
        DataResult<PocQPMovimientos> Listar(PocQPMovimientos filtro, DataRequest request);
        DataResult<PocQPMovimientos> ListarMovimientosQP(PocQPMovimientos filtro, DataRequest request);
        List<PocQuickpassMovimientos> ConsultarMontosQP(PocListaQPMontos filtro);
        void AplicarMontosQP(PocListaQPMontos filtro);
    }
}

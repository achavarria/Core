using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_EntidadMap : EntityTypeConfiguration<GL_Entidad>
    {
        public GL_EntidadMap()
        {
            // Primary Key
            this.HasKey(t => t.IdEntidad);

            // Properties
            this.Property(t => t.IdEntidad)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(120);

            this.Property(t => t.CodigoBIC)
                .HasMaxLength(10);

            this.Property(t => t.CodSuperFranquicia)
                .HasMaxLength(5);

            this.Property(t => t.CodEmisor)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("GL_Entidad", "General");
            this.Property(t => t.IdEntidad).HasColumnName("IdEntidad");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdEsEmisor).HasColumnName("IdEsEmisor");
            this.Property(t => t.CodigoBIC).HasColumnName("CodigoBIC");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.IdTipoEntidad).HasColumnName("IdTipoEntidad");
            this.Property(t => t.CodSuperFranquicia).HasColumnName("CodSuperFranquicia");
            this.Property(t => t.CodEmisor).HasColumnName("CodEmisor");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdMonedaLocal).HasColumnName("IdMonedaLocal");
            this.Property(t => t.IdMonedaInternacional).HasColumnName("IdMonedaInternacional");
        }
    }
}

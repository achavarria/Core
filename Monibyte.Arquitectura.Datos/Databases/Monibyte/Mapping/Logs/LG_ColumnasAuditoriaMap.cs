using Monibyte.Arquitectura.Dominio.Logs;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Logs
{
    public class LG_ColumnasAuditoriaMap : EntityTypeConfiguration<LG_ColumnasAuditoria>
    {
        public LG_ColumnasAuditoriaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdColumna);

            // Properties
            this.Property(t => t.IdColumna)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Columna)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.TipoDato)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.Descripcion)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("LG_ColumnasAuditoria", "Logs");
            this.Property(t => t.IdColumna).HasColumnName("IdColumna");
            this.Property(t => t.IdTabla).HasColumnName("IdTabla");
            this.Property(t => t.Columna).HasColumnName("Columna");
            this.Property(t => t.TipoDato).HasColumnName("TipoDato");
            this.Property(t => t.IdEsLlave).HasColumnName("IdEsLlave");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

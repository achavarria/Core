﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Cargos;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public interface IAppValOrigenCargo : IAplicacionBase
    {
        void Insertar(PocValOrigenCargo registro);
        void Modificar(PocValOrigenCargo registro);
        void Eliminar(PocValOrigenCargo registro);
        IEnumerable<PocCatalogo> ObtieneInfoLista(int idLista, int?[] excluir);
        DataResult<PocValOrigenCargo> Listar(PocValOrigenCargo filtro, DataRequest request);
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Monibyte.Arquitectura.Servicios.Web.Contabilidad
{
    [ServiceContract]
    public interface IServicioContabilidad : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "IncluirParametroContabilidad")]
        Transporte IncluirParametroContabilidad(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminaParametroContabilidad")]
        Transporte EliminaParametroContabilidad(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarParametroContabilidad")]
        Transporte ListarParametroContabilidad(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificaParametroContabilidad")]
        Transporte ModificaParametroContabilidad(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "IncluirConfAsientoContabilidad")]
        Transporte IncluirConfAsientoContabilidad(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminaConfAsientoContabilidad")]
        Transporte EliminaConfAsientoContabilidad(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarConfAsientoContabilidad")]
        Transporte ListarConfAsientoContabilidad(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificaConfAsientoContabilidad")]
        Transporte ModificaConfAsientoContabilidad(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListaCustomer")]
        Transporte ListaCustomer(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListaAccount")]
        Transporte ListaAccount(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListaItem")]
        Transporte ListaItem(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "GenerarArchivo")]
        Transporte GenerarArchivo(Transporte transporte);
    }
}

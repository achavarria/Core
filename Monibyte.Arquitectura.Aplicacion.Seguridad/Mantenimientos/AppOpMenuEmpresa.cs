﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad.Mantenimientos
{
    public class AppOpMenuEmpresa : AplicacionBase, IAppOpMenuEmpresa
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<SC_Menu> _repMenu;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<SC_MenuEmpresa> _repMenuEmpresa;

        public AppOpMenuEmpresa(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<SC_Menu> repMenu,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<SC_MenuEmpresa> repMenuEmpresa)
        {
            _repCatalogo = repCatalogo;
            _repMenu = repMenu;
            _repPersona = repPersona;
            _repMenuEmpresa = repMenuEmpresa;
        }

        public void Eliminar(PocMenuEmpresa registro)
        {
            using (var ts = new TransactionScope())
            {
                _repMenuEmpresa.DeleteOn(p =>
                    p.IdEmpresa == registro.IdEmpresa &&
                    p.IdMenu == registro.IdMenu);
                _repMenuEmpresa.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocMenuEmpresa registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<SC_MenuEmpresa>();
                    entrada.IdEmpresa = registro.IdEmpresa;
                    entrada.IdMenu = registro.IdMenu;
                    entrada.IdEstado = registro.IdEstado;

                    _repMenuEmpresa.Insert(entrada);
                    _repMenuEmpresa.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocMenuEmpresa));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocMenuEmpresa registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<SC_MenuEmpresa>();
                    _repMenuEmpresa.UpdateExclude(entrada,
                        ex => ex.IdEmpresa,
                        ex => ex.IdMenu,
                        ex => ex.FecInclusionAud,
                        ex => ex.IdUsuarioIncluyeAud);
                    _repMenuEmpresa.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocMenuEmpresa));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocMenuEmpresa> Listar(PocMenuEmpresa filtro, DataRequest request)
        {
            var critMenuEmpresa = new Criteria<SC_MenuEmpresa>();
            if (filtro != null)
            {
                if (filtro.IdEmpresa > 0)
                {
                    critMenuEmpresa.And(m => m.IdEmpresa == filtro.IdEmpresa);
                }
                if (filtro.IdMenu > 0)
                {
                    critMenuEmpresa.And(x => x.IdMenu == filtro.IdMenu);
                }
                if (filtro.IdEstado > 0)
                {
                    critMenuEmpresa.And(x => x.IdEstado == filtro.IdEstado);
                }
            }
            var resultado = _repMenuEmpresa.SelectBy(critMenuEmpresa)
                .Join(_repMenu.Table,
                    me => me.IdMenu,
                    m => m.IdMenu,
                    (me, m) => new { me, m })
                .Join(_repCatalogo.List("LACTINACTIVA"),
                    tmp => tmp.me.IdEstado,
                    cat => cat.IdCatalogo,
                    (tmp, cat) => new { tmp.me, tmp.m, cat })
                .Join(_repPersona.Table,
                    tmp => tmp.me.IdEmpresa,
                    ent => ent.IdPersona,
                    (tmp, ent) => new { tmp.me, tmp.m, tmp.cat, ent })
                .Select(tmp => new PocMenuEmpresa
                {
                    IdEmpresa = tmp.me.IdEmpresa,
                    IdEstado = tmp.me.IdEstado,
                    IdMenu = tmp.me.IdMenu,
                    DescEmpresa = tmp.ent.NombreCompleto,
                    DescMenu = tmp.m.Descripcion,
                    DescEstado = tmp.cat.Descripcion
                });
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocMenuEmpresa registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (inserta)
            {
                if (registro.IdEmpresa <= 0) { validacion.AddError("Err_0024"); }
                if (registro.IdMenu <= 0) { validacion.AddError("Err_0096"); }
            }
            else
            {
                if (registro.IdEmpresa <= 0) { validacion.AddError("Err_0024"); }
                if (registro.IdMenu <= 0) { validacion.AddError("Err_0096"); }
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }
    }
}

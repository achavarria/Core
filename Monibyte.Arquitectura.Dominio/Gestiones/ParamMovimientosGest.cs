﻿using System;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public class ParamMovGestion
    {
        public int IdGestion { get; set; }
        public int IdLiquidado { get; set; }
    }
}

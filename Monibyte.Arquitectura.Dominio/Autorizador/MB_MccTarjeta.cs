using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Autorizador
{
    public partial class MB_MccTarjeta : EntidadBaseCtrl
    {
        public int IdMcc { get; set; }
        public int IdCondicionTarjeta { get; set; }
        public int IdCategoriaMcc { get; set; }
        public string CodMccDesde { get; set; }
        public string CodMccHasta { get; set; }
        public Nullable<decimal> LimiteConsumo { get; set; }
        public Nullable<int> CantidadConsumo { get; set; }
        public Nullable<decimal> SaldoLimiteConsumo { get; set; }
        public Nullable<int> SaldoCantidadConsumo { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdSoloCategoria { get; set; }
        public Nullable<System.DateTime> FecUltMiscelaneo { get; set; }
        public int IdPeriodicidad { get; set; }
        public decimal LimiteConsumoInter { get; set; }
        public decimal SaldoLimiteConsumoInter { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Cargos;
using Monibyte.Arquitectura.Poco.Configuracion;
using System;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public class AppConfiguracionLicPer : AplicacionBase, IAppConfiguracionLicPer
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<MB_TipoLicencia> _repTipoLicencia;
        private IRepositorioMnb<MB_LicenciaPersona> _repLicenciaPersona;
        private IRepositorioMnb<CA_CargosPorPersona> _repCargosPorPersona;


        public AppConfiguracionLicPer(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<MB_TipoLicencia> repTipoLicencia,
            IRepositorioMnb<MB_LicenciaPersona> repLicenciaPersona,
            IRepositorioMnb<CA_CargosPorPersona> repCargosPorPersona)
        {
            _repCatalogo = repCatalogo;
            _repPersona = repPersona;
            _repTipoLicencia = repTipoLicencia;
            _repLicenciaPersona = repLicenciaPersona;
            _repCargosPorPersona = repCargosPorPersona;
        }

        public void Eliminar(PocLicenciaPersona registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<MB_LicenciaPersona>();
                _repLicenciaPersona.Delete(entrada);
                _repLicenciaPersona.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocLicenciaPersona registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<MB_LicenciaPersona>();
                    _repLicenciaPersona.Insert(entrada);
                    _repLicenciaPersona.UnidadTbjo.Save();

                    var tipoLic = _repTipoLicencia.Table.FirstOrDefault(x => x.IdTipoLicencia == entrada.IdTipoLicencia);
                    PocCargosPorPersona entradaCargosPersona = new PocCargosPorPersona();
                    var entrada2 = entradaCargosPersona.ProyectarComo<CA_CargosPorPersona>();
                    entrada2.IdCargo = _fGenerales.ObtieneSgteSecuencia("SEQ_IdCargo");
                    entrada2.IdPersona = entrada.IdPersona;
                    entrada2.IdTipoCargo = tipoLic.IdTipoCargo;
                    entrada2.FecInicio = DateTime.Today;
                    entrada2.IdEstado = entrada.IdEstado;
                    _repCargosPorPersona.Insert(entrada2);
                    _repCargosPorPersona.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocLicenciaPersona));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocLicenciaPersona registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<MB_LicenciaPersona>();
                    _repLicenciaPersona.UpdateExclude(entrada, p => p.FecInclusionAud, p => p.IdUsuarioIncluyeAud);
                    _repLicenciaPersona.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocLicenciaPersona));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocLicenciaPersona> Listar(PocLicenciaPersona filtro, DataRequest request)
        {
            var criteria = new Criteria<MB_LicenciaPersona>();
            var resultado = _repLicenciaPersona.SelectBy(criteria)
                .Join(_repPersona.Table,
                    licPer => licPer.IdPersona,
                    per => per.IdPersona,
                    (licencia, persona) => new { licencia, persona })
                .Join(_repTipoLicencia.Table,
                    item => item.licencia.IdTipoLicencia,
                    tipLic => tipLic.IdTipoLicencia,
                    (item, tipLic) => new { item.licencia, item.persona, tipLic })
                .Join(_repCatalogo.List("LACTINACTIVA"),
                    item => item.licencia.IdEstado,
                    estLic => estLic.IdCatalogo,
                    (item, estLic) => new { item.licencia, item.persona, item.tipLic, estLic })
                .Select(item => new PocLicenciaPersona
                {
                    IdPersona = item.licencia.IdPersona,
                    IdTipoLicencia = item.licencia.IdTipoLicencia,
                    IdEstado = item.licencia.IdEstado,
                    DescEstado = item.estLic.Descripcion,
                    NombrePersona = item.persona.NombreCompleto,
                    DescLicencia = item.tipLic.Descripcion
                });
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocLicenciaPersona registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.IdPersona < 0) { validacion.AddError("Err_0035"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }
    }
}

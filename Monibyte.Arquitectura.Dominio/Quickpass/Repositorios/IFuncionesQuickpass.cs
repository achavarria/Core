﻿using Monibyte.Arquitectura.Dominio.Nucleo;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Quickpass.Repositorios
{
    public interface IFuncionesQuickpass : IRepositorioBase 
    {
        IEnumerable<QP_CorteQuickpas> GenerarcargosCorteQuickpass();
        IEnumerable<QP_MovimientosQuickpass> GenerarCargosMovimientosQuickpass();
    }
}

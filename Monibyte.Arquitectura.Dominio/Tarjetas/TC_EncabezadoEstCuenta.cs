using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_EncabezadoEstCuenta : EntidadBase
    { 
        public int IdEstCuenta { get; set; }
        public int IdDiaCorte { get; set; }
        public int IdCuenta { get; set; }
        public Nullable<int> IdTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreCliente { get; set; }
        public string CorreoElectronico { get; set; }
        public string DireccionDeEnvio { get; set; }
        public Nullable<int> PlazoFinanc { get; set; }
        public Nullable<decimal> TasaInteresLocal { get; set; }
        public Nullable<decimal> TasaInteresInter { get; set; }
        public Nullable<decimal> TasaMoraLocal { get; set; }
        public Nullable<decimal> TasaMoraInter { get; set; }
        public Nullable<System.DateTime> FecCorte { get; set; }
        public Nullable<decimal> LimiteCreditoLocal { get; set; }
        public Nullable<decimal> SaldoCortePrincLocal { get; set; }
        public Nullable<decimal> SaldoDisponibleLocal { get; set; }
        public Nullable<decimal> LimiteCreditoInter { get; set; }
        public Nullable<decimal> SaldoCortePrincInter { get; set; }
        public Nullable<decimal> SaldoDisponibleInter { get; set; }
        public Nullable<System.DateTime> FecPagoMinimo { get; set; }
        public Nullable<decimal> PagoMinPrincLocal { get; set; }
        public Nullable<decimal> PagoMinPrincInter { get; set; }
        public Nullable<decimal> PagoMinInteresLocal { get; set; }
        public Nullable<decimal> PagoMinInteresInter { get; set; }
        public Nullable<decimal> PagoMinLocal { get; set; }
        public Nullable<decimal> PagoMinInter { get; set; }
        public Nullable<System.DateTime> FecPagoContado { get; set; }
        public Nullable<decimal> PagoContadoLocal { get; set; }
        public Nullable<decimal> PagoContadoInter { get; set; }
        public Nullable<decimal> InterDiarioMoraLocal { get; set; }
        public Nullable<decimal> InterDiarioMoraInter { get; set; }
        public Nullable<decimal> MontoMoraLocal { get; set; }
        public Nullable<decimal> MontoMoraInter { get; set; }
        public Nullable<decimal> InteresMoraLocal { get; set; }
        public Nullable<decimal> InteresMoraInter { get; set; }
        public Nullable<decimal> OtrosCargosLocal { get; set; }
        public Nullable<decimal> OtrosCargosInter { get; set; }
        public Nullable<System.DateTime> FecCorteAnterior { get; set; }
        public Nullable<decimal> SaldoAntInteresLocal { get; set; }
        public Nullable<decimal> SaldoAntInteresInter { get; set; }
        public Nullable<decimal> SaldoAnteriorLocal { get; set; }
        public Nullable<decimal> SaldoAnteriorInter { get; set; }
        public Nullable<decimal> SaldoIniPremCorte { get; set; }
        public Nullable<decimal> DebitosPremCorte { get; set; }
        public Nullable<decimal> CreditosPremCorte { get; set; }
        public Nullable<decimal> PuntosPorRedimir { get; set; }
        public Nullable<decimal> PuntosDelCorte { get; set; }
        public string ProgramaPuntos { get; set; }
        public Nullable<decimal> LimiteExtraf { get; set; }
        public Nullable<decimal> SobregiroLocal { get; set; }
        public Nullable<decimal> SobregiroInter { get; set; }
        public string Observaciones { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdEstado { get; set; }
        public decimal InteresFinanciadoInter { get; set; }
        public decimal InteresFinanciadoLocal { get; set; }
        public Nullable<decimal> TipoDeCambio { get; set; }
        public string NumeroCuentaMadre { get; set; }
        public string DetalleError { get; set; }
        public Nullable<decimal> LimiteAdelantoLocal { get; set; }
        public Nullable<decimal> LimiteAdelantoInter { get; set; }
        public Nullable<int> CantImpVencidosLocal { get; set; }
        public Nullable<int> CantImpVencidosInter { get; set; }
        public Nullable<decimal> MonImpVencidosLocal { get; set; }
        public Nullable<decimal> MonImpVencidosInter { get; set; }
        public Nullable<decimal> CreditoLocal { get; set; }
        public Nullable<decimal> CreditoInter { get; set; }
        public Nullable<decimal> DebitoLocal { get; set; }
        public Nullable<decimal> DebitoInter { get; set; }
    }
}

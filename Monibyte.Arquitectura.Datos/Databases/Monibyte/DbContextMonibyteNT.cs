﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Notificacion;
using Monibyte.Arquitectura.Dominio.Notificacion;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<NT_PlantillaNotificacion> NT_PlantillaNotificacion { get; set; }

        void CreateContextModelNT(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new NT_ConfiguracionMap());
            modelBuilder.Configurations.Add(new NT_IndicadorEnvioMap());
            modelBuilder.Configurations.Add(new NT_PlantillaNotificacionMap());
        }
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_vwMovimientosTarjeta : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public int IdMoneda { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdPais { get; set; }
        public int IdDebCredito { get; set; }
        public int IdEditado { get; set; }
        public int IdEstado { get; set; }
        public Nullable<int> IdMcc { get; set; }
        public int IdOrigenMovimiento { get; set; }
        public Nullable<int> IdDiaCorte { get; set; }
        public int IdEmpresa { get; set; }
        public string DetalleMovimiento { get; set; }
        public string Descripcion { get; set; }
        public string CamposDinamicos { get; set; }
        public Nullable<System.DateTime> FecCorte { get; set; }
        public System.DateTime FecTransaccion { get; set; }
        public Nullable<System.DateTime> FecMovimiento { get; set; }
        public decimal MonMovimiento { get; set; }
        public decimal MonMovimientoLocal { get; set; }
        public decimal MonMovimientoInter { get; set; }
        public int IdTipoMovimiento { get; set; }
        public string NumAutorizacion { get; set; }
        public Nullable<long> Documento1 { get; set; }
        public Nullable<System.DateTime> HoraMovimiento { get; set; }
        public System.DateTime HoraTransaccion { get; set; }
        public string GeoCamposDinamicos { get; set; }
        public string DesMoneda { get; set; }
        public string CodAlpha2 { get; set; }
        public string CodMoneda { get; set; }
        public string CodMonedaLocal { get; set; }
        public string CodMonedaInter { get; set; }
        public string CodMonedaNormativa { get; set; }
        public string Simbolo { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreImpreso { get; set; }
        public int IdPersona { get; set; }
        public string DebitoCredito { get; set; }
        public Nullable<decimal> SignoMonMovimiento { get; set; }
        public Nullable<decimal> SignoMonMovimientoLocal { get; set; }
        public Nullable<decimal> SignoMonMovimientoInter { get; set; }
        public decimal MonCredLocal { get; set; }
        public decimal MonDebLocal { get; set; }
        public decimal MonCredInter { get; set; }
        public decimal MonDebInter { get; set; }
        public string NumReferencia { get; set; }
        public string CodEditado { get; set; }
        public Nullable<int> MesMovimiento { get; set; }
        public Nullable<int> AnoMovimiento { get; set; }
        public int IdLiquidado { get; set; }
        public int EsHistorico { get; set; }
        public string DescripcionPais { get; set; }
        public string CategoriaMcc { get; set; }
        public int? CodCategoriaMcc { get; set; }
        public Nullable<int> IdGestion { get; set; }
    }
}

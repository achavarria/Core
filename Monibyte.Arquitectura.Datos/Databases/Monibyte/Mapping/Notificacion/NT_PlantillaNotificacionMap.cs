using Monibyte.Arquitectura.Dominio.Notificacion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Notificacion
{
    public class NT_PlantillaNotificacionMap : EntityTypeConfiguration<NT_PlantillaNotificacion>
    {
        public NT_PlantillaNotificacionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPlantilla);

            // Properties
            this.Property(t => t.IdPlantilla)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.SubContexto)
                .HasMaxLength(20);

            this.Property(t => t.Asunto)
                .HasMaxLength(200);

            this.Property(t => t.NomAdjunto)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("NT_PlantillaNotificacion", "Notificacion");
            this.Property(t => t.IdPlantilla).HasColumnName("IdPlantilla");
            this.Property(t => t.IdIdioma).HasColumnName("IdIdioma");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.IdValorContexto).HasColumnName("IdValorContexto");
            this.Property(t => t.SubContexto).HasColumnName("SubContexto");
            this.Property(t => t.Asunto).HasColumnName("Asunto");
            this.Property(t => t.Cuerpo).HasColumnName("Cuerpo");
            this.Property(t => t.NomAdjunto).HasColumnName("NomAdjunto");
            this.Property(t => t.Canal).HasColumnName("Canal");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

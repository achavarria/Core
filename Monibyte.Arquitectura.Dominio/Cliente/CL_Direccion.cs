using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Clientes
{
    public partial class CL_Direccion : EntidadBase
    {
        public int IdDireccion { get; set; }
        public int IdTipo { get; set; }
        public Nullable<int> IdPais { get; set; }
        public Nullable<int> IdProvincia { get; set; }
        public Nullable<int> IdCanton { get; set; }
        public Nullable<int> IdDistrito { get; set; }
        public string Direccion { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<int> IdPersona { get; set; }
        public Nullable<System.DateTime> FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdOrigenProcesador { get; set; }
    }
}

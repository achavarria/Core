﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Tarjetas.Repositorios
{
    public class RepositorioCuenta : RepositorioMnb<TC_Cuenta>, IRepositorioCuenta
    {
        public RepositorioCuenta(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }
        public IEnumerable<TC_Cuenta> CuentasPorCliente(int idCliente)
        {
            var resultado2 = this.TableSet
                .Where(res =>
                    res.IdPersonaTitular == idCliente);

            return resultado2;
        }

        public EncabezadoEstCuenta ObtenerEncabezadoEstCuenta(int idCuenta, DateTime fecCorte,
            int? idCuentaMadre, string tablaEnc, int idIdioma, int idIdiomaBase)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", idCuenta));
            listaParams.Add(new SqlParameter("p2", fecCorte.ToString("MM-dd-yyyy")));
            listaParams.Add(new SqlParameter("p3", (object)idCuentaMadre ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p4", tablaEnc));
            listaParams.Add(new SqlParameter("p5", idIdioma));
            listaParams.Add(new SqlParameter("p6", idIdiomaBase));

            var datos = (UnidadTbjo as ISql).ExecuteQuery<EncabezadoEstCuenta>
                ("General.P_ObtieneEncabezadoEstCuenta @p1, @p2, @p3 ,@p4, @p5, @p6",
                listaParams.ToArray()).FirstOrDefault();

            return datos;
        }
    }
}

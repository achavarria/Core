using Monibyte.Arquitectura.Dominio.Logs;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Logs
{
    public class LG_TriggersAuditoriaMap : EntityTypeConfiguration<LG_TriggersAuditoria>
    {
        public LG_TriggersAuditoriaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.BaseDatos, t.Esquema, t.Tabla });

            // Properties
            this.Property(t => t.BaseDatos)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Esquema)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Tabla)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("LG_TriggersAuditoria", "Logs");
            this.Property(t => t.BaseDatos).HasColumnName("BaseDatos");
            this.Property(t => t.Esquema).HasColumnName("Esquema");
            this.Property(t => t.Tabla).HasColumnName("Tabla");
            this.Property(t => t.TriggerStatement).HasColumnName("TriggerStatement");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

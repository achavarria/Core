using Monibyte.Arquitectura.Dominio.Gestiones;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Gestiones
{
    public class GE_ParametrosValorMap : EntityTypeConfiguration<GE_ParametrosValor>
    {
        public GE_ParametrosValorMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Valor)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("GE_ParametrosValor", "Gestion");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdGestion).HasColumnName("IdGestion");
            this.Property(t => t.Valor).HasColumnName("Valor");
            this.Property(t => t.IdAccion).HasColumnName("IdAccion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Documentacion;
using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Seguridad
{
    public partial class SC_Accion : EntidadBase
    {
        public int IdAccion { get; set; }
        public string Descripcion { get; set; }
        public string Controlador { get; set; }
        public string Area { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdConsulta { get; set; }
    }
}

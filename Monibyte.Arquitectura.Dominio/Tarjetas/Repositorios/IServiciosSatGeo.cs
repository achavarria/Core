﻿using Monibyte.Arquitectura.Dominio.Tarjetas.SatGeo;

namespace Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios
{
    public interface IServiciosSatGeo
    {
        SatGeoOut ObtieneInfoMovimiento(SatgeoIn datos);
    }
}

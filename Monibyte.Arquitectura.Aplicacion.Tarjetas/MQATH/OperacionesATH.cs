﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.MQATH;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios.MQATH;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH
{
    public class OperacionesATH : AplBase, IOperacionesATH
    {
        private IRepositorioATH _repTarjetaATH;
        private IRepositorioCuenta _repCuenta;
        private IRepositorioTarjeta _repTarjeta;
        private IRepositorioTipoTarjeta _repTipoTarjeta;
        private IRepositorioVwTarjetaUsuario _repVwTarjetaUsuario;
        private IRepositorioMovTT _repMovTT;
        private IRepositorioMoneda _repMoneda;
        private IFuncionesGenerales _repFuncionesGenerales;
        private IRepositorioDetalleMovTT _repDetalleMovTmp;

        public OperacionesATH(IRepositorioATH repTarjetaATH,
            IRepositorioCuenta repCuenta,
            IRepositorioTarjeta repTarjeta,
            IRepositorioTipoTarjeta repTipoTarjeta,
            IRepositorioVwTarjetaUsuario repVwTarjetaUsuario,
            IRepositorioMovTT repMovTT,
            IRepositorioMoneda repMoneda,
            IFuncionesGenerales repFuncionesGenerales,
            IRepositorioDetalleMovTT repDetalleMovTmp)
        {
            _repTarjetaATH = repTarjetaATH;
            _repCuenta = repCuenta;
            _repTarjeta = repTarjeta;
            _repTipoTarjeta = repTipoTarjeta;
            _repVwTarjetaUsuario = repVwTarjetaUsuario;
            _repMovTT = repMovTT;
            _repMoneda = repMoneda;
            _repFuncionesGenerales = repFuncionesGenerales;
            _repDetalleMovTmp = repDetalleMovTmp;
        }

        public PocSalidaConsultaTarjeta ConsultaTarjeta(PocEntradaConsultaTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaConsultaTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarConsultaTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaConsultaTarjeta>();
        }
        public PocSalidaPagoTarjeta PagoTarjeta(PocEntradaPagoTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaPagoTarjeta>();
            entrada.Establecer(mod =>
            {
                mod.TipoMensaje = TipoEntradaMsj.PAGO;
                mod.NumFinanEspecial = Constantes.CARACTERBLANCO.ToString();
                mod.NumReferencia = Constantes.CARACTERBLANCO.ToString();
            });
            var tramaSalida = _repTarjetaATH.ProcesarPagoTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaPagoTarjeta>();
        }
        public PocSalidaPagoTarjeta ReversaPagoTarjeta(PocEntradaPagoTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaPagoTarjeta>();
            entrada.Establecer(mod =>
            {
                mod.TipoMensaje = TipoEntradaMsj.REVERSION_PAGO;
                mod.NumFinanEspecial = Constantes.CARACTERBLANCO.ToString();
            });
            var tramaSalida = _repTarjetaATH.ProcesarPagoTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaPagoTarjeta>();
        }
        public PocSalidaPagoTarjeta PagoEspecialTarjeta(PocEntradaPagoTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaPagoTarjeta>();
            entrada.Establecer(mod =>
            {
                mod.TipoMensaje = TipoEntradaMsj.PAGO_ESPECIAL;
                mod.NumFinanEspecial = Constantes.CARACTERBLANCO.ToString();
            });
            var tramaSalida = _repTarjetaATH.ProcesarPagoTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaPagoTarjeta>();
        }
        public PocSalidaPagoTarjeta ReversaPagoEspecialTarjeta(PocEntradaPagoTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaPagoTarjeta>();
            entrada.Establecer(mod =>
            {
                mod.TipoMensaje = TipoEntradaMsj.REVERSION_PAGO_ESPECIAL;
            });
            var tramaSalida = _repTarjetaATH.ProcesarPagoTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaPagoTarjeta>();
        }
        public PocSalidaActualizaTarjeta ActualizaEstadoTarjeta(PocEntradaActualizaTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaActualizaTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarEstadoTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaActualizaTarjeta>();
        }
        public PocSalidaActualizaLimiteTarjeta ActualizaLimiteCuenta(PocEntradaActualizaLimiteTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaActualizaLimiteCuenta>();
            var tramaSalida = _repTarjetaATH.ProcesarLimiteCuenta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaActualizaLimiteTarjeta>();
        }
        public PocSalidaActualizaLimiteTarjeta ActualizaLimiteTarjeta(PocEntradaActualizaLimiteTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaActualizaLimiteTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarLimiteTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaActualizaLimiteTarjeta>();
        }
        public PocSalidaActivaTarjeta ActivaTarjeta(PocEntradaActivaTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaActivaTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarActivaTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaActivaTarjeta>();
        }
        public PocSalidaReposicionPinTarjeta ReposicionPinTarjeta(PocEntradaReposicionPinTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaReposicionPinTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarReposicionPinTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaReposicionPinTarjeta>();
        }
        public IEnumerable<PocSalidaMovimientosTarjeta> MovimientosTarjeta(PocEntradaMovimientosTarjeta pocEntrada)
        {
            return ObtenerMovimientosMq(pocEntrada, false);
        }

        public IEnumerable<PocSalidaMovimientosTarjeta> MovimientosTransitoTarjeta(PocEntradaMovimientosTarjeta pocEntrada)
        {
            return ObtenerMovimientosMq(pocEntrada, true);
        }

        #region METODOS PRIVADOS MOVIMIENTOS
        private IEnumerable<PocSalidaMovimientosTarjeta> ObtenerMovimientosMq(PocEntradaMovimientosTarjeta pocEntrada, bool transito)
        {
            var resultado = new List<PocSalidaMovimientosTarjeta>();
            if (pocEntrada.Mq)
            {
                var entrada = pocEntrada.ProyectarComo<EntradaMovimientosTarjeta>();
                if (!string.IsNullOrEmpty(entrada.NumTarjeta))
                {
                    var criteria = new Criteria<TC_vwTarjetaUsuario>();
                    criteria.And(m => m.IdTarjeta == pocEntrada.IdTarjeta || pocEntrada.IdTarjeta == null);
                    var tarjetasPermitidas = _repVwTarjetaUsuario.TarjetasPermitidas(
                        (int)pocEntrada.IdCuenta, filtroEstado: new int[] { (int)EnumEstadoTarjeta.Todos },
                        criterias: criteria).ToList();

                    var movsPrinc = ObtenerMovimientosTarjetaMq(entrada, transito);
                    movsPrinc = movsPrinc.Join(tarjetasPermitidas,
                            mov => mov.NumTarjeta, tar => tar.NumTarjeta,
                            (mov, tar) => new { mov, tar })
                        .Select(r =>
                        {
                            r.mov.IdTarjeta = r.tar.IdTarjeta;
                            r.mov.NombreImpreso = r.tar.NombreImpreso;
                            r.mov.DynamicJsonTarjeta = r.tar.CamposDinamicos;
                            r.mov.SoloConsulta = r.tar.IdSoloConsulta == (int)EnumIdSiNo.Si;
                            return r.mov;
                        });
                    resultado.AddRange(movsPrinc.ToList());

                    if (pocEntrada.IdTarjeta == null && pocEntrada.IdEsCuentaMadre == (int)EnumIdSiNo.Si)
                    {
                        tarjetasPermitidas.RemoveAll(m => m.NumTarjeta == entrada.NumTarjeta);
                        tarjetasPermitidas = tarjetasPermitidas.DistinctBy(m => m.IdCuenta).ToList();
                        foreach (var tar in tarjetasPermitidas)
                        {
                            entrada.NumTarjeta = tar.NumTarjeta;
                            var movsHija = ObtenerMovimientosTarjetaMq(entrada, transito);
                            movsHija.Update(m =>
                            {
                                m.IdTarjeta = tar.IdTarjeta;
                                m.NombreImpreso = tar.NombreImpreso;
                                m.DynamicJsonTarjeta = tar.CamposDinamicos;
                                m.SoloConsulta = tar.IdSoloConsulta == (int)EnumIdSiNo.Si;
                            });
                            resultado.AddRange(movsHija);
                        }
                    }
                }
                var catalogo = _repFuncionesGenerales.ObtieneListaCatalogo("LDEBITOCREDITO", (int)InfoSesion.InfoRegion.IdIdioma);
                resultado.Update(p =>
                {
                    var moneda = _repMoneda.ObtenerPorCodigoAth(p.Moneda);
                    p.IdMoneda = moneda.IdMoneda;
                    p.Moneda = moneda.CodInternacional;
                    p.DescripcionMoneda = moneda.Descripcion;
                    var itemCatalogo = catalogo.FirstOrDefault(m => m.Codigo == p.CodMovimiento);
                    p.IdDebCredito = itemCatalogo == null ? (int)EnumDebitoCredito.Debito :
                         itemCatalogo.IdCatalogo;
                });
                RenovarMovimientosMqTemp(pocEntrada.IdCuenta, resultado);
            }
            else
            {
                var movimientos = ObtenerMovimientosMqTemp(pocEntrada.IdCuenta, pocEntrada.IdTarjeta);
                resultado.AddRange(movimientos);
            }
            return resultado;
        }


        private IEnumerable<PocSalidaMovimientosTarjeta> ObtenerMovimientosTarjetaMq(EntradaMovimientosTarjeta entrada,
            bool movimientosTransito)
        {
            var seccion = ObtenerSeccionConfiguracion(entrada.NumTarjeta);
            if (movimientosTransito)
            {
                entrada.Establecer(mod =>
                {
                    mod.TipoMensaje = TipoEntradaMsj.MOVIMIENTOS_TRANSITO;
                });
                var tramaSalida = _repTarjetaATH.ProcesarMovTransitoTarjeta(entrada, seccion);
                tramaSalida = tramaSalida.DistinctBy(m => m.NumReferencia);
                return tramaSalida.ProyectarComoColeccion<PocSalidaMovimientosTarjeta>();
            }
            else
            {
                entrada.Establecer(mod =>
                {
                    mod.TipoMensaje = TipoEntradaMsj.MOVIMIENTOS_TARJETA;
                });
                var tramaSalida = _repTarjetaATH.ProcesarMovimientosTarjeta(entrada, seccion);
                return tramaSalida.ProyectarComoColeccion<PocSalidaMovimientosTarjeta>();
            }
        }

        private void RenovarMovimientosMqTemp(int? idCuenta, IEnumerable<PocSalidaMovimientosTarjeta> movimientosTemp)
        {
            _repMovTT.DeleteOn(p => p.IdUsuario == InfoSesion.Info.IdUsuario);
            foreach (var movimiento in movimientosTemp)
            {
                _repMovTT.Insert(new TC_Movimiento_TT
                {
                    IdUsuario = InfoSesion.Info.IdUsuario,
                    IdCuenta = idCuenta,
                    IdTarjeta = movimiento.IdTarjeta,
                    NumTarjeta = movimiento.NumTarjeta,
                    IdMoneda = movimiento.IdMoneda,
                    MonTransaccion = movimiento.Monto,
                    IdDebCredito = movimiento.IdDebCredito,
                    Descripcion = movimiento.Descripcion,
                    FecTransaccion = movimiento.FecConsumo,
                    FecMovimiento = movimiento.FecMovimiento,
                    NumReferencia = movimiento.NumAutorizacion ?? movimiento.NumReferenciaSiscard
                });
            }
            _repMovTT.UnidadTbjo.Save();
        }

        private IEnumerable<PocSalidaMovimientosTarjeta> ObtenerMovimientosMqTemp(int? idCuenta, int? idTarjeta)
        {
            var tarjetas = _repVwTarjetaUsuario.TarjetasPermitidas(idCuenta);
            var movimientos = _repMovTT.Table
                .Join(tarjetas,
                    temp => temp.NumTarjeta,
                    cuenta => cuenta.NumTarjeta,
                    (mov, tar) => new { mov, tar })
                .Join(_repMoneda.Table,
                    temp => temp.mov.IdMoneda,
                    moneda => moneda.IdMoneda,
                    (mov, moneda) => new
                    {
                        mov.mov,
                        mov.tar,
                        IdMoneda = moneda.IdMoneda,
                        CodMoneda = moneda.CodInternacional,
                        DesMoneda = moneda.Descripcion,
                    })
                .Where(p => p.mov.IdUsuario == InfoSesion.Info.IdUsuario &&
                    (p.mov.IdTarjeta == idTarjeta || !idTarjeta.HasValue) &&
                    (p.mov.IdCuenta == idCuenta || !idCuenta.HasValue))
                .Select(res => new PocSalidaMovimientosTarjeta
                {
                    IdTarjeta = (int)res.mov.IdTarjeta,
                    NumTarjeta = res.mov.NumTarjeta,
                    NombreImpreso = res.tar.NombreImpreso,
                    Descripcion = res.mov.Descripcion,
                    NumCuenta = res.tar.NumCuenta,
                    IdDebCredito = res.mov.IdDebCredito,
                    FecConsumo = (DateTime)res.mov.FecTransaccion,
                    FecMovimiento = (DateTime)res.mov.FecMovimiento,
                    Moneda = res.CodMoneda,
                    IdMoneda = res.IdMoneda,
                    DescripcionMoneda = res.DesMoneda,
                    Monto = (decimal)res.mov.MonTransaccion,
                    NumAutorizacion = res.mov.NumReferencia,
                    DynamicJsonTarjeta = res.tar != null ? res.tar.CamposDinamicos : string.Empty,
                    SoloConsulta = res.tar.IdSoloConsulta == (int)EnumIdSiNo.Si
                });
            return movimientos;
        }
        #endregion

        public IEnumerable<PocSalidaLealtadTarjeta> ConsultaProgLealtadTarjeta(PocEntradaLealtadTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaLealtadTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarProgLealtadTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComoColeccion<PocSalidaLealtadTarjeta>();
        }
        public PocSalidaAplicaMovLealtadTarjeta AplicaMovProgLealtadTarjeta(PocEntradaAplicMovLealtadTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaAplicMovLealtadTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarAplicaMovLealtadTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaAplicaMovLealtadTarjeta>();
        }
        public IEnumerable<PocSalidaConsultaCuentas> ConsultaCuentasDeCliente(PocEntradaConsultaCuentas pocEntrada)
        {
            var entrada = pocEntrada.ProyectarComo<EntradaConsultaCuentas>();
            var tramaSalida = _repTarjetaATH.ProcesarConsultaCuentas(entrada, pocEntrada.SeccionMq);
            return tramaSalida.ProyectarComoColeccion<PocSalidaConsultaCuentas>();
        }
        public PocSalidaMiscelaneosTarjeta AplicaMiscelaneosTarjeta(PocEntradaMiscelaneosTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaMiscelaneosTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarMiscelaneosTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaMiscelaneosTarjeta>();
        }
        public PocSalidaAdelantoEfectivoTarjeta AdelantoEfectivoTarjeta(PocEntradaAdelantoEfectivoTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaAdelantoEfectivoTarjeta>();
            entrada.Establecer(mod =>
            {
                mod.TipoMensaje = TipoEntradaMsj.ADELANTO_EFECTIVO;
                mod.NumReferencia = Constantes.CARACTERBLANCO.ToString();
            });
            var tramaSalida = _repTarjetaATH.ProcesarAdelantoEfectivoTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaAdelantoEfectivoTarjeta>();
        }
        public PocSalidaAdelantoEfectivoTarjeta ReversaAdelantoEfectivoTarjeta(PocEntradaAdelantoEfectivoTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaAdelantoEfectivoTarjeta>();
            entrada.Establecer(mod =>
            {
                mod.TipoMensaje = TipoEntradaMsj.REVERSION_ADELANTO_EFECTIVO;
            });
            var tramaSalida = _repTarjetaATH.ProcesarAdelantoEfectivoTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaAdelantoEfectivoTarjeta>();
        }
        public PocSalidaInclusionSeguroTarjeta InclusionSeguroTarjeta(PocEntradaInclusionSeguroTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaInclusionSeguroTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarInclusionSeguroTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaInclusionSeguroTarjeta>();
        }
        public PocSalidaCargoAutomaticoTarjeta GeneraCargoAutomaticoTarjeta(PocEntradaCargoAutomaticoTarjeta pocEntrada)
        {
            var seccion = ObtenerSeccionConfiguracion(pocEntrada.NumTarjeta);
            var entrada = pocEntrada.ProyectarComo<EntradaCargoAutomaticoTarjeta>();
            var tramaSalida = _repTarjetaATH.ProcesarCargoAutomaticoTarjeta(entrada, seccion);
            return tramaSalida.ProyectarComo<PocSalidaCargoAutomaticoTarjeta>();
        }

        private string ObtenerSeccionConfiguracion(string numTarjeta)
        {
            var validation = new ValidationResponse();
            if (!string.IsNullOrEmpty(numTarjeta))
            {
                var bin = numTarjeta.Substring(0, 6);
                var criteria = new Criteria<TC_TipoTarjeta>();
                var tipoTarjeta = _repTipoTarjeta.Table
                    .FirstOrDefault(t => t.Bin == Int32.Parse(bin));
                if (tipoTarjeta != null)
                {
                    return tipoTarjeta.ConfiguracionProcesador;
                }
            }
            validation.AddError("Err_0063", numTarjeta);
            throw new CoreException("No se puede obtener la configuracion requerida para el bin", validation: validation);
        }
    }
}

﻿using AutoMapper;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.AdaptadorPerfiles
{
    class PerfilGenerales : Profile
    {
        protected override void Configure()
        {
            var mapperPel = Mapper.CreateMap<GL_ParametroEmpresaLista, PocParametroEmpresaLista>();
            mapperPel.ForMember(m => m.Texto, m => m.MapFrom(x => string.Format("{0}-{1}", x.Codigo, x.Descripcion)));
            mapperPel.ForMember(m => m.CodigoDependencia, m => m.MapFrom(x =>
                StringExt.FromJson<string[]>(x.CodigoDependencia)));
            mapperPel.ForMember(m => m.ConfiguracionDinamica, m => m.MapFrom(x => StringExt.FromJson<IEnumerable
                    <PocConfiguracionDinamica>>(x.ConfiguracionDinamica)));
            var mapperGPel = Mapper.CreateMap<PocParametroEmpresaLista, GL_ParametroEmpresaLista>();
            mapperGPel.ForMember(m => m.CodigoDependencia, m =>
                m.MapFrom(x => StringExt.ToJson(x.CodigoDependencia)));
            mapperGPel.ForMember(m => m.ConfiguracionDinamica, m =>
                m.MapFrom(x => StringExt.ToJson(x.ConfiguracionDinamica)));

            var mapperListPM = Mapper.CreateMap<GL_ParametroEmpresaLista, PocParametroEmpresaLista>();
            mapperListPM.ForMember(m => m.Texto, m => m.MapFrom(x => string.Format("{0}-{1}", x.Codigo, x.Descripcion)));

            var _pcglr = Mapper.CreateMap<PocReporteBase, GL_Reportes>();
            _pcglr.ForMember(m => m.IdIncluyeEncabTxt, m => m.MapFrom(x =>
                x.IdIncluyeEncabTxt ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            _pcglr.ForMember(m => m.IdIncluyeContrapartida, m => m.MapFrom(x =>
                x.IdIncluyeContrapartida ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            var _glrpc = Mapper.CreateMap<GL_Reportes, PocReporteBase>();
            _glrpc.ForMember(m => m.IdIncluyeEncabTxt, m => m.MapFrom(x =>
                x.IdIncluyeEncabTxt == (int)EnumIdSiNo.Si));
            _glrpc.ForMember(m => m.IdIncluyeContrapartida, m => m.MapFrom(x =>
                x.IdIncluyeContrapartida == (int)EnumIdSiNo.Si));

            var _CrRc = Mapper.CreateMap<Cplx_GL_CamposReporte, ReportColumn>();
            _CrRc.ForMember(m => m.CanGrow, m => m.MapFrom(x =>
                x.IdPuedeCrecer == (int)EnumIdSiNo.Si));
            _CrRc.ForMember(m => m.Group, m => m.MapFrom(x =>
                x.IdAgrupable == (int)EnumIdSiNo.Si));
            _CrRc.ForMember(m => m.Sum, m => m.MapFrom(x =>
                x.IdSumariza == (int)EnumIdSiNo.Si));
            _CrRc.ForMember(m => m.Width, m => m.MapFrom(x => x.Ancho));
            _CrRc.ForMember(m => m.FieldType, m => m.MapFrom(x => x.Tipo));
            _CrRc.ForMember(m => m.HeaderName, m => m.MapFrom(x => x.Encabezado));
            _CrRc.ForMember(m => m.Name, m => m.MapFrom(x => x.Nombre));
            _CrRc.ForMember(m => m.Order, m => m.MapFrom(x => x.Orden));
        }
    }
}
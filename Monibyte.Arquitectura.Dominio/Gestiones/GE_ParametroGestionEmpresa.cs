using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_ParametroGestionEmpresa : EntidadBase
    {
        public int IdParametroEmpresa { get; set; }
        public int IdTipoGestion { get; set; }
        public int IdEmpresa { get; set; }
        public string ValorDefecto { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

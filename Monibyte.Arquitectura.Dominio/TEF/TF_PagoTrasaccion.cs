﻿using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;

namespace Monibyte.Arquitectura.Dominio.TEF
{
    public partial class TF_PagoTrasaccion : EntidadBase
    {
        public int IdPagoTrasaccion { get; set; }
        public int IdTransaccion { get; set; }
        public int Producto { get; set; }
        public Nullable<int> SubProducto { get; set; }
        public int IdMoneda { get; set; }
        public decimal MontoPago { get; set; }
        public Nullable<int> IdModoAplicado { get; set; }
        public string CodRefSiscard { get; set; }
        public string NumComprobanteInterno { get; set; }
        public string Justificacion { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string CodMovimientoMQ { get; set; }
        public int IdTipoProducto { get; set; }
        public virtual TS_Moneda TS_Moneda { get; set; }
        public virtual TF_TransaccionSinpe TF_TransaccionSinpe { get; set; }
    }
}

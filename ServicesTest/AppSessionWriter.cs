﻿using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monibyte.Arquitectura.Poco.Seguridad;

namespace ServicesTest
{
    public class AppSessionWriter : SessionWriter
    {
        public override void AddHeaders(System.Net.WebHeaderCollection headers)
        {
            //headers.Add("LANG", LanguageFilter.Current.ToJson().CompressStr());
            headers.Add("IDSISTEMA", 1.ToJson().CompressStr());
            headers.Add("IDPROCESADOR", 3.ToJson().CompressStr());
            headers.Add("SEGURIDADACTIVA", 0.ToJson().CompressStr());
            headers.Add("FACTORCONVERSION", 50.ToJson().CompressStr());
            headers.Add("FECHAMAQUINA", new DateTime().ToJson().CompressStr());
            headers.Add("INFO", new PocInfoSesion
            {
                OtpActivo = false,
                EsInterno = false,
                Locale = "",
                IdIdioma = 1,
                IdPais = 1,
                IdPolitica = 1,
                NumIdentificacion = "114200489",
                NombreCompleto = "Anthony Chavarri C",
                NombreCorto = "Anthony Chavarria",
                AliasUsuario = "tonyman",
                CodUsuario = "8236860",
                Correo = "achavarria@impesa.net",
                IdTipoUsuario = 415,
                IdRoleOperativo = null,
                IdRole = 1,
                IdEmpresa = 119,
                IdPersona = 119,
                IdUsuario = 101,
                IdCompania = 1,
                IdUnidad = 1,
                IdEstado = 94,
                RequiereOtp = false,
                RequiereAutorizacion = false
            }.ToJson().CompressStr());

            if (InfoSesion.ObtenerSesion<bool?>("BUSCARAUTORIZACION").HasValue)
            {
                var value = InfoSesion.ObtenerSesion<bool>("BUSCARAUTORIZACION");
                headers.Add("BUSCARAUTORIZACION", value.ToJson().CompressStr());
                InfoSesion.IncluirSesion("BUSCARAUTORIZACION", null);
            }
            if (InfoSesion.ObtenerSesion<bool?>("APLICAAUTORIZACION").HasValue)
            {
                var value = InfoSesion.ObtenerSesion<bool>("APLICAAUTORIZACION");
                headers.Add("APLICAAUTORIZACION", value.ToJson().CompressStr());
                InfoSesion.IncluirSesion("APLICAAUTORIZACION", null);
            }
            if (InfoSesion.ObtenerSesion<PocInfoAutorizacion>("INFOAUTORIZACION") != null)
            {
                var value = InfoSesion.ObtenerSesion<PocInfoAutorizacion>("INFOAUTORIZACION");
                headers.Add("INFOAUTORIZACION", value.ToJson().CompressStr());
                InfoSesion.IncluirSesion("INFOAUTORIZACION", null);
            }
            if (InfoSesion.ObtenerSesion<bool?>("AP") != null)
            {
                var value = InfoSesion.ObtenerSesion<bool?>("AP");
                headers.Add("AUTORIZAPRODUCTO", value.ToJson().CompressStr());
                InfoSesion.IncluirSesion("AUTORIZAPRODUCTO", null);
            }
        }
    }
}

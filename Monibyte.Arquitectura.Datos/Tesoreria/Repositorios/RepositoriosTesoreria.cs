﻿using System.Linq;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;

namespace Monibyte.Arquitectura.Datos.Tesoreria.Repositorios
{
    public class RepositorioMoneda : RepositorioMnb<TS_Moneda>, IRepositorioMoneda
    {
        public RepositorioMoneda(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public TS_Moneda ObtenerPorCodigoAth(string codigo)
        {
            var criteria = new Criteria<TS_Moneda>();
            criteria.And(p => p.CodAlpha2 == codigo);
            return this.SelectBy(criteria).FirstOrDefault();
        }
    }

    public class RepositorioTipoCambio : RepositorioMnb<TS_TipoDeCambio>, IRepositorioTipoCambio
    {
        public RepositorioTipoCambio(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public TS_TipoDeCambio ObtenerUltTCVentaXMoneda(int idEntidadFinanciera, int idMoneda, int idMonedaConversion)
        {
            var criteria = new Criteria<TS_TipoDeCambio>();
            criteria.And(p => p.IdEntidadFinanciera == idEntidadFinanciera && p.IdTipoCambio == 345 &&
                    p.IdMoneda == idMoneda && p.IdMonedaConversion == idMonedaConversion);
            return this.SelectBy(criteria).OrderByDescending(x => x.HoraCambio).First();
        }
    }    
}

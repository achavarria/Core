using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Logs
{
    public partial class LG_TriggersAuditoria : EntidadBase
    {
        public string BaseDatos { get; set; }
        public string Esquema { get; set; }
        public string Tabla { get; set; }
        public string TriggerStatement { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

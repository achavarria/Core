using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ParametroCompaniaMap : EntityTypeConfiguration<GL_ParametroCompania>
    {
        public GL_ParametroCompaniaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdParametro, t.IdCompania });

            // Properties
            this.Property(t => t.IdParametro)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.IdCompania)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ValParametro)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.Descripcion)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("GL_ParametroCompania", "General");
            this.Property(t => t.IdParametro).HasColumnName("IdParametro");
            this.Property(t => t.IdCompania).HasColumnName("IdCompania");
            this.Property(t => t.ValParametro).HasColumnName("ValParametro");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

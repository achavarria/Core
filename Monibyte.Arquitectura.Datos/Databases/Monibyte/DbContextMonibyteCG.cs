﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Contabilidad;
using Monibyte.Arquitectura.Dominio.Contabilidad;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<CG_ConfiguracionAsiento> CG_ConfiguracionAsiento { get; set; }
        public DbSet<CG_Parametro> CG_Parametro { get; set; }

        void CreateContextModelCG(DbModelBuilder modelBuilder) 
        {
            modelBuilder.Configurations.Add(new CG_ConfiguracionAsientoMap());
            modelBuilder.Configurations.Add(new CG_ParametroMap());
        }
    }
}

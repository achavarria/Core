﻿using System;

namespace Monibyte.Arquitectura.Poco.Tesoreria
{
    public class PocMoneda
    {
        public int IdMoneda { get; set; }
        public string Descripcion { get; set; }
        public int IdEsBase { get; set; }
        public int IdBaseConversion { get; set; }
        public string Simbolo { get; set; }
        public string CodInternacional { get; set; }
        public int IdEstado { get; set; }
        public string OperadorConversion { get; set; }
        public int? CodigoSUGEF { get; set; }
        public string NumInternacional { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }
    }

    public class PocMontoConvertir
    {
        public int IdMonedaOrigen { get; set; }
        public decimal Monto { get; set; }
        public int IdMonedaDestino { get; set; }
        public int? Factor { get; set; }
    }

    public class PocTipoCambio
    {
        public int IdCambio { get; set; }
        public int? IdEntidadFinanciera { get; set; }
        public int IdMoneda { get; set; }
        public int IdTipoCambio { get; set; }
        public DateTime? FecCambio { get; set; }
        public decimal? MonTipoCambio { get; set; }
        public int? IdEstado { get; set; }
        public int? IdMonedaConversion { get; set; }
        public DateTime HoraCambio { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }

        public string DescEntidadFinanciera { get; set; }
        public string DescMoneda { get; set; }
        public string DescTipoCambio { get; set; }

        public decimal? MonTipoCambioCompra { get; set; }
        public decimal? MonTipoCambioVenta { get; set; }
        public bool EsEdicion { get; set; }
        public string Usuario { get; set; }
    }

    public class PocConfiguracionFinanciera
    {
        public int IdEntidadFinanciera { get; set; }
        public int MonedaConversion { get; set; }
        public string FormatoLocal { get; set; }
        public string FormatoInter { get; set; }
        public int NumDecimalLocal { get; set; }
        public int NumDecimalInter { get; set; }
        public string SimboloLocal { get; set; }
        public string SimboloInter { get; set; }
        public int IdMonedaLocal { get; set; }
        public int IdMonedaInter { get; set; }
        public int IdMonedaDefecto { get; set; }
        public string OperadorConversionLocal { get; set; }
        public string OperadorConversionInter { get; set; }
    }
}

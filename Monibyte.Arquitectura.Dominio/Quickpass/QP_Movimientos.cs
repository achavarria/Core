using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Quickpass
{
    public partial class QP_Movimientos : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public Nullable<int> IdTipoMovimiento { get; set; }
        public int CodMovimiento { get; set; }
        public int IdQuickpass { get; set; }
        public System.DateTime FecConsumo { get; set; }
        public decimal Monto { get; set; }
        public int IdMoneda { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<System.DateTime> FecProcesamiento { get; set; }
        public Nullable<System.DateTime> FecAplicacion { get; set; }
        public Nullable<int> IdLote { get; set; }
        public string Detalle { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string NumReferenciaPago { get; set; }
        public Nullable<int> IdTipoRegistro { get; set; }
        public string CodLocalizador { get; set; }
        public string NumRefProcesador { get; set; }
        public string DetAplicacionPago { get; set; }
    }
}

﻿using System;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public class ParamReporteModificacionesLimite
    {
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public string NumCuenta { get; set; }
        public int? IdEstado { get; set; }
    }
}

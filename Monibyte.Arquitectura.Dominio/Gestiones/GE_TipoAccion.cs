using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_TipoAccion : EntidadBase
    {
        public int IdTipoAccion { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

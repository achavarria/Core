using Monibyte.Arquitectura.Dominio.Clientes;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_EncabezadoEstCtaExtFinHist : EntidadBase
    {
        public int IdEncabezado { get; set; }
        public int IdEstCuenta { get; set; }
        public int IdPersona { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public string NumeroExtra { get; set; }
        public Nullable<int> IdCuenta { get; set; }
        public Nullable<int> IdTarjeta { get; set; }
        public Nullable<decimal> MonApertura { get; set; }
        public Nullable<int> IdMoneda { get; set; }
        public string Comercio { get; set; }
        public Nullable<System.DateTime> FecApertura { get; set; }
        public Nullable<int> Plazo { get; set; }
        public Nullable<decimal> TasaInteres { get; set; }
        public Nullable<System.DateTime> FechaVencimiento { get; set; }
        public Nullable<System.DateTime> FechaCorte { get; set; }
        public Nullable<decimal> MonCuota { get; set; }
        public Nullable<decimal> MonIntereses { get; set; }
        public Nullable<decimal> MonPrincipal { get; set; }
        public string Concepto { get; set; }
        public Nullable<int> IdTipo { get; set; }
        public Nullable<int> CuotasPagadas { get; set; }
        public Nullable<decimal> Filler1 { get; set; }
        public Nullable<decimal> Filler2 { get; set; }
        public Nullable<decimal> MonSaldo { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

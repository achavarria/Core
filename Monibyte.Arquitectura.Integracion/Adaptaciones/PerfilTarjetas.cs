﻿using AutoMapper;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.MQATH;
using Monibyte.Arquitectura.Poco.Tarjetas;

namespace Monibyte.Arquitectura.Servicios.Web.AdaptadorPerfiles
{
    class PerfilTarjetas : Profile
    {
        protected override void Configure()
        {
            var movTran = Mapper.CreateMap<SalidaMovTransitoTarjeta, PocSalidaMovimientosTarjeta>();
            movTran.ForMember(p => p.FecConsumo, p => p.MapFrom(m => m.FecAutorizacion));
            movTran.ForMember(p => p.FecMovimiento, p => p.MapFrom(m => m.FecAutorizacion));
            movTran.ForMember(p => p.NumReferenciaSiscard, p => p.MapFrom(m => m.NumReferencia));
            movTran.ForMember(p => p.Monto, p => p.MapFrom(m => m.MontoAutorizacion));
            movTran.ForMember(p => p.Moneda, p => p.MapFrom(m => m.MonedaAutorizacion));
            movTran.ForMember(p => p.Descripcion, p => p.MapFrom(m => m.NombreComercio));

            var mapperMovimientos = Mapper.CreateMap<PocSalidaMovimientosTarjeta, PocMovimientosTarjeta>();
            mapperMovimientos.ForMember(p => p.FecTransaccion, p => p.MapFrom(m => m.FecConsumo));
            mapperMovimientos.ForMember(p => p.NumReferencia, p => p.MapFrom(m => m.NumAutorizacion ?? m.NumReferenciaSiscard));

            var mapperConsultaTarjeta = Mapper.CreateMap<PocSalidaConsultaTarjeta, PocDetalleCtaTarjeta>();
            mapperConsultaTarjeta.ForMember(p => p.FecVencePagoMinimo, p => p.MapFrom(x => x.FecVencimientoPago));
            mapperConsultaTarjeta.ForMember(p => p.FecVencePagoContado, p => p.MapFrom(x => x.FecVencimientoPago));
            mapperConsultaTarjeta.ForMember(p => p.TarjetaHabiente, p => p.MapFrom(x => x.Nombre));
            mapperConsultaTarjeta.ForMember(p => p.DispAdelantoEfectivoInter, p => p.MapFrom(x => x.DispAdelantoInter));
            mapperConsultaTarjeta.ForMember(p => p.DispAdelantoEfectivoLocal, p => p.MapFrom(x => x.DispAdelantoLocal));

            var mapperTarjetaUsuario = Mapper.CreateMap<TC_vwTarjetaUsuario, PocTarjetaCuentaUsuario>();
            mapperTarjetaUsuario.ForMember(p => p.DynamicJson, p => p.MapFrom(x => x.CamposDinamicos));
            mapperTarjetaUsuario.ForMember(p => p.Autoriza, p => p.MapFrom(x =>
                x.IdAutoriza != null ? x.IdAutoriza == (int)EnumIdSiNo.Si : !Context.IsEditor));
            mapperTarjetaUsuario.ForMember(p => p.SoloConsulta, p => p.MapFrom(x => x.IdSoloConsulta == (int)EnumIdSiNo.Si));
            mapperTarjetaUsuario.ForMember(p => p.FueAgregada, p => p.MapFrom(x => x.FueAgregada == (int)EnumIdSiNo.Si));
            mapperTarjetaUsuario.ForMember(p => p.Texto, p => p.MapFrom(x => TextoImpreso(x) ));
            mapperTarjetaUsuario.ForMember(p => p.TextoTitular, p => p.MapFrom(x => string.Format("{0} {1} - {2}", x.NumTarjetaTitular, x.Descripcion, x.NombreImpresoTitular)));
        }

        private string TextoImpreso(TC_vwTarjetaUsuario x)
        {
            if(x.IdRelacion == (int)EnumRelacionTarjeta.Adicional) 
                return string.Format("{0} {1}", x.NumTarjeta, x.NombreImpreso);
            else
                return string.Format("{0} {1} - {2}", x.NumTarjeta, x.Descripcion, x.NombreImpreso);
        }

    }
}
using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_TipoComercioMap : EntityTypeConfiguration<MB_TipoComercio>
    {
        public MB_TipoComercioMap()
        {
            // Primary Key
            this.HasKey(t => t.CodMcc);

            // Properties
            this.Property(t => t.CodMcc)
                .IsRequired()
                .HasMaxLength(6);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("MB_TipoComercio", "Mnb");
            this.Property(t => t.CodMcc).HasColumnName("CodMcc");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdCategoriaMcc).HasColumnName("IdCategoriaMcc");
            this.Property(t => t.IdMcc).HasColumnName("IdMcc");
            this.Property(t => t.TasaComision).HasColumnName("TasaComision");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

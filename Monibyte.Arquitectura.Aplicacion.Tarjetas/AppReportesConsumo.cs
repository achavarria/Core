﻿using Monibyte.Arquitectura.Aplicacion.Nucleo;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Linq;
using System.Collections.Generic;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Datos.ReportServer;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public class AppReportesConsumo : AplicacionBase, IAppReportesConsumo
    {
        private IRepositorioReportServer _repReportServer;
        private IRepositorioMnb<TC_CierreCorte> _repCierreCorte;

        public AppReportesConsumo(IRepositorioReportServer repReportServer,
            IRepositorioMnb<TC_CierreCorte> repCierreCorte)
        {
            _repReportServer = repReportServer;
            _repCierreCorte = repCierreCorte;
        }

        public byte[] ObtenerReporteConsumo(PocDetalleConsumo datosEntrada)
        {
            var parameters = new List<ReportParam>();
            Reporte resultado = null;
            parameters.Add(new ReportParam { Name = "pIdCompania", Value = datosEntrada.IdEntidad.ToString() });
            parameters.Add(new ReportParam { Name = "pFecHasta", Value = datosEntrada.FecHasta.ToString("MM-dd-yyyy") });
            if (datosEntrada.TipoConsumo == (int)EnumTipoConsumo.Compras)
            {
                parameters.Add(new ReportParam { Name = "pFecDesde", Value = datosEntrada.FecDesde.ToString("MM-dd-yyyy") });
                resultado = _repReportServer.ExportCsv("/Reports/AdmCartera/DetalleCompras", parameters.ToArray());
            }
            else if (datosEntrada.TipoConsumo == (int)EnumTipoConsumo.Financiacion)
            {
                var date = new DateTime(datosEntrada.FecHasta.Year, datosEntrada.FecHasta.Month, 1);
                var corte = date.AddMonths(1).AddDays(-1);
                var datosCorte = _repCierreCorte.Table.FirstOrDefault(x => x.FecCorte == corte);

                parameters.Add(new ReportParam { Name = "pIdCorte", Value = (datosCorte != null ? datosCorte.IdCorte : 0).ToString() });
                resultado = _repReportServer.ExportCsv("/Reports/AdmCartera/InteresesFinanciados", parameters.ToArray());
            }
            return resultado.Output;
        }

        public byte[] ObtenerReporteEjecutivos(PocDetalleConsumo datosEntrada)
        {
            var parameters = new List<ReportParam>();
            Reporte resultado = null;
            parameters.Add(new ReportParam { Name = "idEntidad", Value = datosEntrada.IdEntidad.ToString() });
            parameters.Add(new ReportParam { Name = "fecDesde", Value = datosEntrada.FecHasta.ToString("MM-dd-yyyy") });
            parameters.Add(new ReportParam { Name = "fecHasta", Value = datosEntrada.FecDesde.ToString("MM-dd-yyyy") });
            resultado = _repReportServer.ExportCsv("/Reports/AdmCartera/r-FactEjectutivo", parameters.ToArray());

            return resultado.Output;
        }
    }
}

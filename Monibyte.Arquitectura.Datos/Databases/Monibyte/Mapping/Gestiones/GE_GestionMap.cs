using Monibyte.Arquitectura.Dominio.Gestiones;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Gestiones
{
    public class GE_GestionMap : EntityTypeConfiguration<GE_Gestion>
    {
        public GE_GestionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdGestion);

            // Properties
            this.Property(t => t.IdGestion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Detalle)
                .IsRequired()
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("GE_Gestion", "Gestion");
            this.Property(t => t.IdGestion).HasColumnName("IdGestion");
            this.Property(t => t.IdTipoGestion).HasColumnName("IdTipoGestion");
            this.Property(t => t.FecInclusion).HasColumnName("FecInclusion");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.Detalle).HasColumnName("Detalle");
            this.Property(t => t.IdTipoProducto).HasColumnName("IdTipoProducto");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.IdProducto).HasColumnName("IdProducto");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecCierre).HasColumnName("FecCierre");
            this.Property(t => t.IdUsuarioCierra).HasColumnName("IdUsuarioCierra");
            this.Property(t => t.IdUsuarioIncluye).HasColumnName("IdUsuarioIncluye");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdCompania).HasColumnName("IdCompania");
            this.Property(t => t.IdUnidad).HasColumnName("IdUnidad");
            this.Property(t => t.IdSucursal).HasColumnName("IdSucursal");
            this.Property(t => t.IdEjecutivo).HasColumnName("IdEjecutivo");
            this.Property(t => t.IdExportado).HasColumnName("IdExportado");
            this.Property(t => t.IdContextoMovimientos).HasColumnName("IdContextoMovimientos");
        }
    }
}

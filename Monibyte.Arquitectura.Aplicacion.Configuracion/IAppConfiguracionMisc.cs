﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Configuracion;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Seguridad;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public interface IAppConfiguracionMisc : IAplicacionBase
    {
        List<PocRegion> ObtenerRegiones();
        List<PocPaisRegion> ObtenerPaisesRegion(int idRegion);
        List<PocCategoriaComercio> ObtenerListaCategoriaComercio();
        List<PocComercio> ObtenerListaComercio(int idCategoria);
        DataResult<PocUsuario> ObtenerUsuariosPorGrupo(PocUsuariosGrupos filtro, DataRequest request);
        DataResult<PocBitacoraPanelCtrl> ObtenerBitacoraPanelCrtl(PocFiltroBitacoraPanelCtrl filtro,
            DataRequest request);
        int ObtenerAutorizacionesCount();
        IEnumerable<PocAutorizacion> ObtenerAutorizaciones();
        string ObtieneDetalleBitacoraPanelCtrl(string datosBitacora);
    }
}

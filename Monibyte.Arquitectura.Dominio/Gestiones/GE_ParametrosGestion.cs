using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_ParametrosGestion : EntidadBase
    {
        public int IdParamGestion { get; set; }
        public int IdParametro { get; set; }
        public int IdTipoGestion { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<int> IdRequerido { get; set; }
        public string ValorDefecto { get; set; }
        public Nullable<int> IdTipo { get; set; }
        public Nullable<int> IdFormato { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

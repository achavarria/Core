using Monibyte.Arquitectura.Dominio.Documentacion;
using Monibyte.Arquitectura.Dominio.Notificacion;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Idioma : EntidadBase
    {
        public int IdIdioma { get; set; }
        public string Idioma { get; set; }
        public int IdEstado { get; set; }
        public string Abreviatura { get; set; }
        public string CodCultura { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

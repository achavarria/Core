﻿using System;
using Monibyte.Arquitectura.Comun.Nucleo.Trama;
namespace Monibyte.Arquitectura.Dominio.Tarjetas.MQATH
{
    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaMovimientosTarjeta
    {
        private string _blancos = string.Empty.PadRight(75, ' ');
        private string _tipoMensaje = TipoEntradaMsj.MOVIMIENTOS_TARJETA;
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string TipoMensaje { get { return _tipoMensaje; } set { _tipoMensaje = value; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 8, format: "yyyyMMdd", regex: @"^\d{4}((0[1-9])|(1[012]))((0[1-9]|[12]\d)|3[01])$", characterFill: Constantes.CARACTERBLANCO)]
        public DateTime FechaDesde { get; set; }
        [TramaColumnSpec(required: true, maxLength: 8, format: "yyyyMMdd", regex: @"^\d{4}((0[1-9])|(1[012]))((0[1-9]|[12]\d)|3[01])$", characterFill: Constantes.CARACTERBLANCO)]
        public DateTime FechaHasta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 75)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }
    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaMovimientosTarjeta : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4)]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16)]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string codMovimiento { get; set; }
        [TramaColumnSpec(required: true, maxLength: 45)]
        public string Descripcion { get; set; }
        [TramaColumnSpec(required: true, maxLength: 1)]
        public string estado { get; set; }
        [TramaColumnSpec(required: true, maxLength: 8)]
        public string emisor { get; set; }
        [TramaColumnSpec(required: true, maxLength: 5)]
        public string NumCuenta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 8)]
        public DateTime FecConsumo { get; set; }
        [TramaColumnSpec(required: true, maxLength: 8)]
        public DateTime FecMovimiento { get; set; }
        [TramaColumnSpec(required: true, maxLength: 6)]
        public string HoraMovimiento { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100)]
        public decimal Monto { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string Moneda { get; set; }
        [TramaColumnSpec(required: true, maxLength: 7)]
        public string NumAutorizacion { get; set; }
        [TramaColumnSpec(required: false, maxLength: 13)]
        public string NumReferenciaSiscard { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 821)]
        public string Blancos { get; set; }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaMovTransitoTarjeta : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4)]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16)]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 8)]
        public DateTime FecAutorizacion { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12)]
        public string NumReferencia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 40)]
        public string NombreComercio { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100)]
        public decimal MontoAutorizacion { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string MonedaAutorizacion { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 866)]
        public string Blancos { get; set; }
    }

}

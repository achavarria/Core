using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Logs
{
    public partial class LG_TablasAuditoria : EntidadBase
    {
        public int IdTabla { get; set; }
        public string BaseDatos { get; set; }
        public string Esquema { get; set; }
        public string Tabla { get; set; }
        public string Descripcion { get; set; }
        public int IdAudInserta { get; set; }
        public int IdAudActualiza { get; set; }
        public int IdAudBorra { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Cargos;
using Monibyte.Arquitectura.Dominio.Cargos;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<CA_CargosPorPersona> CA_CargosPorPersona { get; set; }
        public DbSet<CA_DetRangosCargo> CA_DetRangosCargo { get; set; }
        public DbSet<CA_DetRangosCargosPer> CA_DetRangosCargosPer { get; set; }
        public DbSet<CA_Movimientos> CA_Movimientos { get; set; }
        public DbSet<CA_RangosTabla> CA_RangosTabla { get; set; }
        public DbSet<CA_TipoCargo> CA_TipoCargo { get; set; }
        public DbSet<CA_ValOrigenCargo> CA_ValOrigenCargo { get; set; }
        public DbSet<CA_ValOrigenCargoPer> CA_ValOrigenCargoPer { get; set; }
        public DbSet<CA_CargosGeo> CA_CargosGeo { get; set; }

        void CreateContextModelCA(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CA_CargosPorPersonaMap());
            modelBuilder.Configurations.Add(new CA_DetRangosCargoMap());
            modelBuilder.Configurations.Add(new CA_DetRangosCargosPerMap());
            modelBuilder.Configurations.Add(new CA_MovimientosMap());
            modelBuilder.Configurations.Add(new CA_RangosTablaMap());
            modelBuilder.Configurations.Add(new CA_TipoCargoMap());
            modelBuilder.Configurations.Add(new CA_ValOrigenCargoMap());
            modelBuilder.Configurations.Add(new CA_ValOrigenCargoPerMap());
            modelBuilder.Configurations.Add(new CA_CargosGeoMap());
        }
    }
}

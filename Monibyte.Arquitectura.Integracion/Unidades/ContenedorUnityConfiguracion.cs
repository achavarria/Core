﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Configuracion;
    using Monibyte.Arquitectura.Datos.Configuracion.Repositorios;
    using Monibyte.Arquitectura.Dominio.Configuracion.Repositorios;

    public class ContenedorUnityConfiguracion
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            _unityContainer.RegisterType<IAppConfiguracionApl, AppConfiguracionApl>();
            _unityContainer.RegisterType<IAppConfiguracionCtrl, AppConfiguracionCtrl>();
            _unityContainer.RegisterType<IAppConfiguracionGrps, AppConfiguracionGrps>();
            _unityContainer.RegisterType<IAppConfiguracionMisc, AppConfiguracionMisc>();
            _unityContainer.RegisterType<IAppReportes, AppReportes>();
            _unityContainer.RegisterType<IAppConfiguracionLicPer, AppConfiguracionLicPer>();
            _unityContainer.RegisterType<IAppConfiguracionTipoLic, AppConfiguracionTipoLic>();
            _unityContainer.RegisterType<IAppPrivilegios, AppPrivilegios>();
            #endregion

            #region Repositorios            
            _unityContainer.RegisterType<IRepositorioPlantilla, RepositorioPlantilla>();
            _unityContainer.RegisterType<IRepositorioTipoComercio, RepositorioTipoComercio>();
            _unityContainer.RegisterType<IFuncionesConfiguracion, FuncionesConfiguracion>();
            #endregion
        }
    }
}

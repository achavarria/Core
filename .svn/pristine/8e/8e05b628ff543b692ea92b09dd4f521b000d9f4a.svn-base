﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Gestiones;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Gestiones
{
    public interface IAppGestionesTarjeta : IAplicacionBase
    {
        void ModificarEstado(PocCambioEstadoGestion poco);
        DataResult<PocDetalleGestion> ObtenerGestionesPermitidasPorTipo(
            DataRequest request, PocParametrosGestion filtro);

        PocGestionAdicionales ObtenerGestionAdicionales(int idGestion);
        PocGestionAdicionales ObtenerGestionAdicionalesEnProceso(int idEmpresa, int idCuenta);
        int? ProcesarAdicionales(PocGestionAdicionales gestion);
        void GuardarAdicionales(List<PocTarjetaAdicional> datosEntrada, int idGestion);

        void ModificaLimiteCuenta(PocModificaLimite pocEntrada);
        int ModificaLimiteTarjeta(PocModificaLimite pocEntrada);
        PocModificaLimite CargarCambioLimite(string numTarjeta);

        int ReactivarTarjeta(PocEstadoTarjeta pocEntrada);
        void ModificaFechaCorteTarjeta(PocModificaFecCorteTarjeta pocEntrada);
        int BloqueoTarjeta(PocBloqueoTarjeta pocEntrada);
        int ReposicionTarjeta(PocReposicionTarjeta pocEntrada);
        int CorreoSalidaDelPais(PocNotificacionSalida notEntrada);
        void ActivarTarjeta(PocEstadoTarjeta pocEntrada);
        PocModificaVip CargarCambioVip(PocModificaVip pocEntrada);
        int ModificaVipTarjeta(PocModificaVip pocEntrada);
        List<PocMigracionTarjeta> MigrarConfiguracionTarjeta(List<PocMigracionTarjeta> tarjetas,
            int idAccion, int idEstado);
    }
}

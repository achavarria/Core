﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Poco.Gestiones;
using System.IO;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using System.Collections.Generic;
using Monibyte.Arquitectura.Poco.Generales;
using System.Linq;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Monibyte.Arquitectura.Adaptador.Nucleo;

namespace ServicesTest
{
    [TestClass]
    public class GestionesTest
    {
        [TestInitialize]
        public void EscribirSession()
        {
            RestClient.SetSessionWriter(new AppSessionWriter());
        }

        [TestMethod, TestCategory("Gestiones")]
        public void ObtieneGestion()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetDatos(1);
            var resultado = TServidorRest.Response<PocDetalleGestion>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtieneGestion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("Gestiones")]
        public void ObtieneGestionesEmpresaCuenta()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetDatos(new PocParametrosGestion
            {
                IdContexto = (int)EnumContexto.TC_CUENTA,
                IdProducto = 18,
                IdPersona = 119,
                FecDesde = new DateTime(2017, 01, 01),
                FecHasta = new DateTime(2017, 03, 01)
            });
            var request = new DataRequest();
            transporte.SetDataRequest(request);
            var resultado = TServidorRest.Response<DataResult<PocDetalleGestion>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtieneListaGestion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("Gestiones")]
        public void ObtieneGestionesEmpresaTarjeta()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetDatos(new PocParametrosGestion
            {
                IdContexto = (int)EnumContexto.TC_TARJETA,
                IdProducto = 52,
                IdPersona = 119,
                FecDesde = new DateTime(2017, 01, 01),
                FecHasta = new DateTime(2017, 03, 01)
            });
            var request = new DataRequest();
            transporte.SetDataRequest(request);
            var resultado = TServidorRest.Response<DataResult<PocDetalleGestion>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtieneListaGestion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }
        [TestMethod, TestCategory("Gestiones")]
        public void ObtieneHistorialGestion()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetValor("idGestion", 1);
            var request = new DataRequest();
            transporte.SetDataRequest(request);
            var resultado = TServidorRest.Response<DataResult<PocDetalleGestion>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtieneHistorialGestion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }
        [TestMethod, TestCategory("Gestiones")]
        public void ListaTipoGestion()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            var resultado = TServidorRest.Response<List<PocTipoGestion>>(transporte,
            "Gestiones", "ServiciosGestiones", "ListaTipoGestion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("Gestiones")]
        public void ObtieneGestionesRealizadas()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetDatos(new PocParametrosGestion
            {
                IdPersona = 919,
                FecDesde = new DateTime(2017, 01, 01),
                FecHasta = new DateTime(2017, 01, 10)
            });
            var request = new DataRequest();
            transporte.SetDataRequest(request);
            var resultado = TServidorRest.Response<DataResult<PocDetalleGestion>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtieneListaGestion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }
        [TestMethod, TestCategory("Gestiones")]
        public void EliminarGestionRegistrada()
        {
            //try
            //{
            //    var transporte = FabricaTransporte.CrearTransporte();
            //    transporte.SetValor("idGestion", 1);
            //    TServidorRest.Request(transporte,
            //    "Gestiones", "ServiciosGestiones", "EliminarGestionRegistrada");
            //    Console.WriteLine("Gestion Eliminada");
            //    Assert.IsTrue(true);
            //}
            //catch
            //{
            //    Console.WriteLine("Error Eliminando Gestión");
            //    Assert.IsTrue(false);
            //}
            Assert.IsTrue(true);
        }

        [TestMethod, TestCategory("Liquidacion Gastos")]
        public void ObtenerParametrosLiquidacionGastos()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetValor("contexto", EnumContexto.LiquidacionGastos);
            transporte.SetValor("tipoGestion", EnumTipoGestion.LiquidacionDeGastos);
            var resultado = TServidorRest.Response<IEnumerable<PocParametroEmpresa>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtenerParametrosEmpresaGestion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("ParametrosGestiones")]
        public void ObtenerParametrosLiquidacionKilometraje()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetValor("contexto", EnumContexto.EF_KILOMETRAJE);
            transporte.SetValor("tipoGestion", EnumTipoGestion.LiquidacionDeKilometraje);
            var resultado = TServidorRest.Response<IEnumerable<PocParametroEmpresa>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtenerParametrosEmpresaGestion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("ParametrosGestiones")]
        public void ObtenerParametrosLiquidacionEfectivo()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetValor("contexto", EnumContexto.EF_MOVIMIENTO);
            transporte.SetValor("tipoGestion", EnumTipoGestion.LiquidacionDeEfectivo);
            var resultado = TServidorRest.Response<IEnumerable<PocParametroEmpresa>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtenerParametrosEmpresaGestion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("Liquidacion Gastos")]
        public void ObtenerMovimientosLiquidacion()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            PocTarjetaCuentaUsuario[] subjects = new PocTarjetaCuentaUsuario[1];
            subjects[0] = new PocTarjetaCuentaUsuario
            {
                IdPersona = 119,
                IdCuenta = 18,
                IdTarjeta = 52
            };

            transporte.SetDatos(new PocParametrosLiquidacion
            {
                Personas = subjects,
                IdCuenta = 18,
                IdTarjeta = 52,
                FechaDesde = new DateTime(2017, 01, 01),
                FechaHasta = new DateTime(2017, 01, 10)
            });
            var resultado = TServidorRest.Response<IEnumerable<PocMovimientosLiquidacion>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtenerMovimientosLiquidacion");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("Liquidacion Gastos")]
        public void GuardarLiquidacionGastos()
        {
            var transporte = FabricaTransporte.CrearTransporte();
            PocTarjetaCuentaUsuario[] subjects = new PocTarjetaCuentaUsuario[1];
            subjects[0] = new PocTarjetaCuentaUsuario
            {
                IdPersona = 119,
                IdCuenta = 18,
                IdTarjeta = 52
            };

            transporte.SetDatos(new PocParametrosLiquidacion
            {
                Personas = subjects,
                IdCuenta = 18,
                IdTarjeta = 52,
                FechaDesde = new DateTime(2017, 01, 01),
                FechaHasta = new DateTime(2017, 01, 03)
            });
            var movimientos = TServidorRest.Response<IEnumerable<PocMovimientosLiquidacion>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtenerMovimientosLiquidacion");

            var liquidacion = new PocLiquidacionGastos
            {
                DynamicJson = "{\"f269\":\"F02\",\"FecSalida\":\"2017-01-02T00:00:00\",\"FecRegreso\":\"2017-01-03T00:00:00\",\"IdCuenta\":18,\"IdTarjeta\":52,\"NumTarjeta\":\"5235850035670003\",\"Observaciones\":\"Prueba de Liquidación gastos\",\"NombreCompleto\":\"ANTONY CHAVARRIA C.\",\"NumIdentificacion\":\"114200489\"}",
                IdsMovs = movimientos.ProyectarComo<PocMovimientosDeLiquidacion>(),
                IdPersona = 119,
                IdTarjeta = 52,
                NumTarjeta = "5235850035670003",
                Observaciones = "Uni Test Prueba Liquidación de Gastos",
                NumIdentificacion = "114200489",
                IdEstado = 314,
                IdContextoMovimientos = 609,
                FecRegreso = new DateTime(2017, 01, 01),
                FecSalida = new DateTime(2017, 01, 03)
            };


            var resultado = TServidorRest.Response<IEnumerable<PocMovimientosLiquidacion>>(transporte,
            "Gestiones", "ServiciosGestiones", "GuardarLiquidacionGastos");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("Liquidacion Gastos")]
        public void EliminarMovimientoLiquidacion()
        {
            try
            {
                PocMovimientosDeLiquidacion[] subjects = new PocMovimientosDeLiquidacion[1];
                subjects[0] = new PocMovimientosDeLiquidacion
                {
                    IdContexto = 609,
                    IdMovimiento = 31693
                };
                var transporte = FabricaTransporte.CrearTransporte();
                var gestion = TServidorRest.Response<PocDetalleGestion>(transporte,
                "Gestiones", "ServiciosGestiones", "ObtieneGestion");
                transporte.SetValor("idGestion", gestion.IdGestion);
                transporte.SetDatos(subjects);
                TServidorRest.Request(transporte,
                "Gestiones", "ServiciosGestiones", "EliminarMovimientoLiquidacion");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.IsTrue(false);
            }
        }

        [TestMethod, TestCategory("ParametrosGestiones")]
        public void ObtenerParamGestionEmpresa()
        {
            /*Pantalla de parametros par aliquidación*/
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetValor("reqTodo", false);
            var resultado = TServidorRest.Response<IEnumerable<PocParametrosGestionEmpresa>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtenerParamGestionEmpresa");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("ParametrosGestiones")]
        public void ObtenerParamGestionEmpresa2()
        {
            /*Pantalla de parametros par aliquidación grid*/
            var transporte = FabricaTransporte.CrearTransporte();
            transporte.SetValor("reqTodo", true);
            var resultado = TServidorRest.Response<IEnumerable<PocParametrosGestionEmpresa>>(transporte,
            "Gestiones", "ServiciosGestiones", "ObtenerParamGestionEmpresa");
            Console.WriteLine(resultado.ToJson());
            Assert.IsTrue(resultado != null);
        }

        [TestMethod, TestCategory("ParametrosGestiones")]
        public void GuardarParametrosEmpresa()
        {
            try
            {
                var transporte = FabricaTransporte.CrearTransporte();
                transporte.SetValor("reqTodo", false);
                var resultado = TServidorRest.Response<IEnumerable<PocParametrosGestionEmpresa>>(transporte,
                "Gestiones", "ServiciosGestiones", "ObtenerParamGestionEmpresa");

                transporte.SetValor("datosSalida", resultado);
                transporte.SetValor("datosPrevios", resultado);

                TServidorRest.Request(transporte,
                "Gestiones", "ServiciosGestiones", "GuardarParametrosEmpresa");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.IsTrue(false);
            }
        }

        [TestMethod, TestCategory("Liquidacion Gastos")]
        public void ObtieneReporteLiq()
        {
            try
            {
                var transporte = FabricaTransporte.CrearTransporte();
                var gestion = TServidorRest.Response<PocDetalleGestion>(transporte,
                "Gestiones", "ServiciosGestiones", "ObtieneGestion");
                transporte.SetValor("idGestion", gestion.IdGestion);
                var reporte = TServidorRest.Response<byte[]>(transporte,
                "Gestiones", "ServiciosGestiones", "ObtieneReporteLiq");
                Assert.IsTrue(reporte.Any());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.IsTrue(false);
            }
        }

        [TestMethod, TestCategory("Liquidacion Gastos")]
        public void ModificarEstadoLiquidacion()
        {
            try
            {
                var transporte = FabricaTransporte.CrearTransporte();
                var gestion = TServidorRest.Response<PocDetalleGestion>(transporte,
                "Gestiones", "ServiciosGestiones", "ObtieneGestion");
                transporte.SetDatos(new PocCambioEstadoGestionTarjeta
                {
                    IdGestion = gestion.IdGestion,
                    Justificacion = "Prueba cambio de estado",
                    IdEstado = 1004,
                    IdTipoGestion = 13
                });
                TServidorRest.Request(transporte,
                "Gestiones", "ServiciosGestiones", "ModificarEstadoLiquidacion");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.IsTrue(false);
            }
        }

        [TestMethod, TestCategory("Liquidacion Gastos")]
        public void CerrarLiqGastos()
        {
            try
            {
                var transporte = FabricaTransporte.CrearTransporte();
                var gestion = TServidorRest.Response<PocDetalleGestion>(transporte,
                "Gestiones", "ServiciosGestiones", "ObtieneGestion");
                transporte.SetValor("idGestion", gestion.IdGestion);
                TServidorRest.Request(transporte,
                "Gestiones", "ServiciosGestiones", "CerrarLiqGastos");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.IsTrue(false);
            }
        }

        [TestMethod, TestCategory("Liquidacion Gastos")]
        public void EliminarLiquidacion()
        {
            try
            {
                var transporte = FabricaTransporte.CrearTransporte();
                var gestion = TServidorRest.Response<PocDetalleGestion>(transporte,
                "Gestiones", "ServiciosGestiones", "ObtieneGestion");
                transporte.SetValor("idGestion", gestion.IdGestion);
                TServidorRest.Request(transporte,
                "Gestiones", "ServiciosGestiones", "EliminarLiquidacion");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.IsTrue(false);
            }
        }

        [TestMethod, TestCategory("Gestiones")]
        public void ModificaLimiteCuenta()
        {
            try
            {
                var transporte = FabricaTransporte.CrearTransporte();
                transporte.SetValor("NumTarjeta", "5235850035670003");
                var infoTarjeta = TServidorRest.Response<PocLimitesTarjeta>(transporte,
                "Tarjetas", "ServiciosInfoTarjeta", "ObtieneGestion");

                transporte.SetDatos(new PocModificaLimite
                {
                    IdCuenta = 18,
                    IdTarjeta = 52,
                    NumCuenta = "5235850035670",
                    NumTarjeta = "5235850035670003",
                    MonLimiteNuevo = infoTarjeta.LimiteCuenta - 1,
                    MonLimiteActual = infoTarjeta.LimiteCuenta,
                    IdEstado = (int)EnumEstadoGestion.Cerrada
                });
                TServidorRest.Request(transporte,
                "Gestiones", "ServiciosGestiones", "ModificaLimiteCuenta");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.IsTrue(false);
            }
        }
        [TestMethod, TestCategory("Gestiones")]
        public void ObtenerModificacionesLimite()
        {
            try
            {
                var transporte = FabricaTransporte.CrearTransporte();
                transporte.SetDatos(new PocReporteModificacionesLimite
                {
                    FechaDesde = DateTime.Now.AddMonths(-2),
                    FechaHasta = DateTime.Now,
                    NumCuenta = "5235850035670",
                    MonLimiteActual = 700,
                    IdEstado = (int)EnumEstadoGestion.Cerrada
                });
                var resultado = TServidorRest.Response<List<PocReporteModificacionesLimite>>(transporte,
                "Gestiones", "ServiciosGestiones", "ObtenerModificacionesLimite");
                Console.WriteLine(resultado.ToJson());
                Assert.IsTrue(resultado != null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.IsTrue(false);
            }
        }
    }
}

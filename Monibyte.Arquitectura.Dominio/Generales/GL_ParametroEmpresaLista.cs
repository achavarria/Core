using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_ParametroEmpresaLista : EntidadBase
    {
        public int IdParametroEmpresa { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Orden { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string CodigoContrapartida { get; set; }
        public string CodigoDependencia { get; set; }
        public string ConfiguracionDinamica { get; set; }
    }
}

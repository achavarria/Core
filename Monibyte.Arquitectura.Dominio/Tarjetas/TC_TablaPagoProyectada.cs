using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_TablaPagoProyectada : EntidadBase
    {
        public int IdCuota { get; set; }
        public int IdFinanciamiento { get; set; }
        public Nullable<System.DateTime> FecCuota { get; set; }
        public Nullable<decimal> MonPrincipal { get; set; }
        public Nullable<decimal> MonInteres { get; set; }
        public Nullable<decimal> TasaInteres { get; set; }
        public Nullable<decimal> MonSaldo { get; set; }
        public Nullable<int> IdCancelada { get; set; }
        public Nullable<System.DateTime> FecCancelada { get; set; }
        public Nullable<decimal> MonSeguro { get; set; }
        public Nullable<decimal> MonPrincipalPagado { get; set; }
        public Nullable<decimal> MonInteresPagado { get; set; }
        public Nullable<decimal> MonSeguroPagado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

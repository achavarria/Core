﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Poco.Seguridad;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public interface IAppAutorizacion : IAplicacionBase
    {
        int? PrepararAutorizacion(int tipoAutorizacion, string numReferencia, string detalleAutorizacion);
        string ObtieneDetalleAutorizacion(int tipoAutorizacion, 
            string numReferencia, int? idTipoGrupo = null);
        int? ObtieneAutorizacionPendiente(int? tipoAutorizacion, string numReferencia);
        int AprobarAutorizacion(int tipoAutorizacion, string numReferencia);
        int RechazarAutorizacion(int tipoAutorizacion, string numReferencia, PocInfoAutorizacion infoAutorizacion);
        void AnularAutorizacionesGrupo(string numReferencia);
        int AnularAutorizacion(int tipoAutorizacion, string numReferencia, PocInfoAutorizacion infoAutorizacion);
        int EliminarAutorizacion(int tipoAutorizacion, string numReferencia);
        bool ExisteDependenciaPendiente(int idTipoAutorizacion, string numReferencia);
        bool EsNuevoGrupo(string numReferencia);
        bool PermiteEditar(string numReferencia);
        void EliminarPendientes(string numReferencia, PocInfoAutorizacion infoAutorizacion);
        SC_Autorizacion ObtenerAutorizacion(int idTipoAutorizacion, string numReferencia);
        string ObtieneObervacion(int idTipoAutorizacion, string numReferencia);
    }
}
﻿using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.TEF
{
    public class PocCedulaRegistrada
    {
        public int IdCedulaRegistrada { get; set; }
        public int IdCuenta { get; set; }
        public int IdUsuario { get; set; }
        public string NumCuenta { get; set; }
        public string NumIdentCuenta { get; set; }
        public string NombreCompleto { get; set; }
        public string NumIdentificacion { get; set; }
        public int IdEstado { get; set; }
    }

    public class PocPagoPendiente
    {
        public int IdTransaccion { get; set; }
        public int IdOrigenInterno { get; set; }
        public int? CodServicio { get; set; }

        public string CodEntidadOrigen { get; set; }
        public string IdentOrigen { get; set; }
        public string CCOrigen { get; set; }
        public int? IdMonedaOrigen { get; set; }

        public string CodEntidadDestino { get; set; }
        public string IdentDestino { get; set; }
        public string CCDestino { get; set; }
        public int IdMonedaDestino { get; set; }
        public string MonedaDestino { get; set; }

        public decimal Comision { get; set; }
        public decimal MontoPago { get; set; }
        public decimal MontoTransaccion { get; set; }
        public string Detalle { get; set; }

        public string CodRefSinpe { get; set; }
        public int? CodRefCoreBancario { get; set; }

        public int IdEstadoTrans { get; set; }
        public string EstadoTrans { get; set; }
        public int EstadoSINPE { get; set; }
        public string DetalleError { get; set; }
        public int? CodMotivoRechazo { get; set; }
        public string DescripcionRechazo { get; set; }
        public int IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }

        public System.DateTime? FecValor { get; set; }
        public System.DateTime? FecLiquidacion { get; set; }

        public int IdPagoTrasaccion { get; set; }
        public int IdPersona { get; set; }
        public string NombrePersona { get; set; }
        public int IdMoneda { get; set; }
        public int IdEstado { get; set; }
        public decimal Monto { get; set; }
        public string NumTarjeta { get; set; }
        public string Justificacion { get; set; }
        public string ModoAplicado { get; set; }
        public string CodRefSiscard { get; set; }
        public string CodMovimiento { get; set; }
    }

    public class PocPagoPrincipal
    {
        public int IdTransaccion { get; set; }
        public int IdMoneda { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdExtraFinanciamiento { get; set; }

        public decimal MontoXTipoCambio { get; set; }
        public decimal MontoConvMoneda { get; set; }
        public decimal MontoTarjeta { get; set; }
        public decimal MontoExtraFin { get; set; }

        public decimal TCCompra { get; set; }
        public decimal TCVenta { get; set; }

        public int idCuentaGlobal { get; set; }
        public int idTarjetaGlobal { get; set; }
        public int IdExtraFin { get; set; }

        public decimal MontoNoDistribuido { get; set; }
        public decimal MontoDistribuido { get; set; }
        public string NumTarjeta { get; set; }
        public string NumTarjetaOtroCuenta { get; set; }
        public int NumExtraFin { get; set; }
        public string NumReferenciaTC { get; set; }
        public string NumReferenciaOC { get; set; }
        public string NumReferenciaEX { get; set; }
    }

    public class PocPagoDistribuido
    {
        public int IdTransaccion { get; set; }

        public int IdCuentaBase { get; set; }
        public int IdTarjetaBase { get; set; }
        public int IdMonedaBase { get; set; }

        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdMoneda { get; set; }

        public int IdExtraFinanciamiento { get; set; }
        public List<PocDirigirPago> Pagos { get; set; }

        public decimal TCCompra { get; set; }
        public decimal TCVenta { get; set; }

        public decimal MontoNoDistribuido { get; set; }
        public decimal MontoDistribuido { get; set; }
        public string NumTarjetaBase { get; set; }
    }

    public class PocDirigirPago
    {
        public int IdTransaccion { get; set; }
        public int IdAccionPago { get; set; }
        public int IdModoPago { get; set; }

        public int IdCuentaBase { get; set; }
        public int IdTarjetaBase { get; set; }
        public int IdMonedaBase { get; set; }
        public int IdExtraFinanciamiento { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdMoneda { get; set; }

        public decimal MontoPagoCRC { get; set; }
        public decimal MontoPagoUSD { get; set; }
        public decimal MontoPago { get; set; }
        public decimal MontoPagoTC { get; set; }

        public string NumTarjetaBase { get; set; }
        public int NumExtraFinanciamiento { get; set; }
        public string NumTarjeta { get; set; }
        public string AccionPago { get; set; }
        public string ModoPago { get; set; }

        public int CodError { get; set; }
        public string DesError { get; set; }
        public string CodReferenciaSiscard { get; set; }
        public string CodAutorización { get; set; }
        public string TarjetaHabiente { get; set; }
    }

    public class PocPagoAplicado
    {
        public int IdTransaccion { get; set; }
        public int IdMoneda { get; set; }
        public decimal MontoPago { get; set; }
        public decimal PagoMinimo { get; set; }
        public decimal PrcDistribuible { get; set; }
        public int IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }
        public int IdTipoProducto { get; set; }
        public string CodMovimientoMQ { get; set; }
        public string CodMoneda { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaValor { get; set; }
        public string NumTarjeta { get; set; }
    }
    public class PocEncPagoDirigido
    {
        public List<PocPagoAplicado> TotalPagos { get; set; }
        public List<PocPagoAplicado> DetallePagos { get; set; }
    }

    public class PocSinpeDistribuido
    {
        public int IdTransaccion { get; set; }
        public int IdMoneda { get; set; }
        public decimal Monto { get; set; }
        public decimal PagoMinimo { get; set; }
        public int IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public string CodMoneda { get; set; }
        public DateTime Fecha { get; set; }
    }

    public class PocPagoMQ
    {
        //public string IdTransaccion { get; set; }
        public string CodMovimientoMQ { get; set; }
        public string CodRefSiscard { get; set; }
        public string NumReferencia { get; set; }

        public string TipoPago { get; set; }
        public string NumTarjeta { get; set; }
        public string NumProducto { get; set; }

        public int IdMoneda { get; set; }
        public decimal Monto { get; set; }

        public DateTime FecConsumo { get; set; }
    }

    public class PocRespuestaMQ
    {
        public int EstadoTrans { get; set; }
        public DateTime FecLiquidacion { get; set; }
        public string NumReferencia { get; set; }
        public string DetalleError { get; set; }
    }
    public class PocRespuestaServimas
    {
        public string NumAutorizacion { get; set; }
        public string CodRefSiscard { get; set; }
        public string CodError { get; set; }
        public string DetalleError { get; set; }
    }
}

﻿namespace Monibyte.Arquitectura.Dominio.Generales.Repositorios
{
    public interface IRepositorioPerfilUsuario : IRepositorioMnb<GL_Usuario>
    {
        GL_Usuario ObtenerInfoUsuario(int? IdUsuario);   
        
    }
}
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_MovimientoGestion : EntidadBase
    {
        public int IdGestion { get; set; }
        public int IdMovimiento { get; set; }
        public int IdContexto { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Aplicacion.Gestiones;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Poco.Gestiones;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Gestiones
{
    public class ServicioGestionesTarjeta : ServicioUnity, IServicioGestionesTarjeta
    {
        private IAppGestiones _gestion;
        private IAppGestionesTarjeta _gestionTarjeta;
        private IAppGestionesTarjeta _repGestionesTarjeta;
        public ServicioGestionesTarjeta(IAppGestiones paramGestion,
            IAppGestionesTarjeta gestionTarjeta,
            IAppGestionesTarjeta repGestionesTarjeta)
        {
            _gestion = paramGestion;
            _gestionTarjeta = gestionTarjeta;
            _repGestionesTarjeta = repGestionesTarjeta;
        }

        public Transporte ObtenerGestionesPermitidasPorTipo(Transporte transporte)
        {
            var parametros = transporte.GetDatos<PocParametrosGestion>();
            var dataRequest = transporte.GetDataRequest();
            var resultado = _repGestionesTarjeta.
                ObtenerGestionesPermitidasPorTipo(dataRequest, parametros);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ConsultaSolicitudAdicionales(Transporte transporte)
        {
            var idGestion = transporte.GetValor<int>("idGestion");
            var resultado = _repGestionesTarjeta.ObtenerGestionAdicionales(idGestion);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerGestionAdicionalesEnProceso(Transporte transporte)
        {
            var idCuenta = transporte.GetValor<int>("idCuenta");
            var idEmpresa = transporte.GetValor<int>("idEmpresa");
            var resultado = _repGestionesTarjeta.
                ObtenerGestionAdicionalesEnProceso(idEmpresa, idCuenta);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte GuardarAdicionales(Transporte transporte)
        {
            var idGestion = transporte.GetValor<int>("idGestion");
            var adicionales = transporte.GetDatos<List<PocTarjetaAdicional>>();
            _repGestionesTarjeta.GuardarAdicionales(adicionales, idGestion);
            return transporte;
        }

        public Transporte ProcesarAdicionales(Transporte transporte)
        {
            var gestionAdicionales = transporte.GetDatos<PocGestionAdicionales>();
            var idGestion = _repGestionesTarjeta.ProcesarAdicionales(gestionAdicionales);
            transporte.SetDatos(idGestion);
            return transporte;
        }

        public Transporte ModificaLimiteTarjeta(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocModificaLimite>();
            var resultado = _repGestionesTarjeta.ModificaLimiteTarjeta(entradaServicio);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte CargarCambioLimite(Transporte transporte)
        {
            var numTarjeta = transporte.GetValor<string>("NumTarjeta");
            var buscarAut = transporte.GetValor<bool>("BUSCARAUTORIZACION");
            var resultado = _repGestionesTarjeta.CargarCambioLimite(numTarjeta, buscarAut);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ModificaFechaCorteTarjeta(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocModificaFecCorteTarjeta>();
            _repGestionesTarjeta.ModificaFechaCorteTarjeta(entradaServicio);
            transporte.SetDatos(null);
            return transporte;
        }

        public Transporte BloqueoTarjeta(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocBloqueoTarjeta>();
            var resultado = _repGestionesTarjeta.BloqueoTarjeta(entradaServicio);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ReactivarTarjeta(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocEstadoTarjeta>();
            var resultado = _repGestionesTarjeta.ReactivarTarjeta(entradaServicio);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ActivarTarjeta(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocEstadoTarjeta>();
            _repGestionesTarjeta.ActivarTarjeta(entradaServicio);
            return transporte;

        }

        public Transporte ReposicionTarjeta(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocReposicionTarjeta>();
            var resultado = _repGestionesTarjeta.ReposicionTarjeta(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ModificarEstado(Transporte transporte)
        {
            var poco = transporte.GetDatos<PocCambioEstadoGestionTarjeta>();
            _gestionTarjeta.ModificarEstado(poco);
            return transporte;
        }

        public Transporte CorreoSalidaDelPais(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocNotificacionSalida>();
            var resultado = _gestionTarjeta.CorreoSalidaDelPais(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte CargarCambioVip(Transporte transporte)
        {
            var datosVip = transporte.GetDatos<PocModificaVip>();
            var resultado = _repGestionesTarjeta.CargarCambioVip(datosVip);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ModificaVipTarjeta(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocModificaVip>();
            var resultado = _repGestionesTarjeta.ModificaVipTarjeta(entradaServicio);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public List<PocMigracionTarjeta> MigrarConfiguracionTarjeta(List<PocMigracionTarjeta> tarjetas,
            int idAccion, int idEstado)
        {
            return _repGestionesTarjeta.MigrarConfiguracionTarjeta(tarjetas, idAccion, idEstado);
        }
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Procesador
{
    public partial class PT_InformacionCola : EntidadBase
    {
        public int IdCola { get; set; }
        public string TipoReferencia { get; set; }
        public string CodReferencia { get; set; }
        public System.DateTime FechaSolicitud { get; set; }
        public string TipoMensajeSolicitud { get; set; }
        public string TramaSolicitud { get; set; }
        public byte[] IdMensajeSolicitud { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FechaRespuesta { get; set; }
        public string TramaRespuesta { get; set; }
        public byte[] IdMensajeRespuesta { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdTipoUsuario { get; set; }
    }
}

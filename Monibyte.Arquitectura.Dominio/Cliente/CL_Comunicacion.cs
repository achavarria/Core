using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Clientes
{
    public partial class CL_Comunicacion : EntidadBase
    {
        public int IdComunicacion { get; set; }
        public string ValComunicacion { get; set; }
        public int IdEstado { get; set; }
        public int IdTipo { get; set; }
        public Nullable<int> IdUbicacion { get; set; }
        public Nullable<int> IdFormato { get; set; }
        public int IdPersona { get; set; }
        public Nullable<System.DateTime> FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdOrigenProcesador { get; set; }
    }
}

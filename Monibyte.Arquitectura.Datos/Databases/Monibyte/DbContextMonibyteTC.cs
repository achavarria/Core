﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<TC_AdministradoresCuenta> TC_AdministradoresCuenta { get; set; }
        public DbSet<TC_BitacoraCuentaTarjeta> TC_BitacoraCuentaTarjeta { get; set; }
        public DbSet<TC_CierreCorte> TC_CierreCorte { get; set; }
        public DbSet<TC_Cuenta> TC_Cuenta { get; set; }
        public DbSet<TC_CuentaHist> TC_CuentaHist { get; set; }
        public DbSet<TC_DetalleEstCuenta> TC_DetalleEstCuenta { get; set; }
        public DbSet<TC_DetalleEstCuentaExtFin> TC_DetalleEstCuentaExtFin { get; set; }
        public DbSet<TC_DetalleEstCuentaExtFinHist> TC_DetalleEstCuentaExtFinHist { get; set; }
        public DbSet<TC_DetalleEstCuentaHist> TC_DetalleEstCuentaHist { get; set; }
        public DbSet<TC_DetalleMovimiento> TC_DetalleMovimiento { get; set; }
        public DbSet<TC_DetalleMovimiento_TT> TC_DetalleMovimiento_TT { get; set; }
        public DbSet<TC_DetalleMovimientoHist> TC_DetalleMovimientoHist { get; set; }
        public DbSet<TC_EncabezadoEstCtaExtFin> TC_EncabezadoEstCtaExtFin { get; set; }
        public DbSet<TC_EncabezadoEstCtaExtFinHist> TC_EncabezadoEstCtaExtFinHist { get; set; }
        public DbSet<TC_EncabezadoEstCuenta> TC_EncabezadoEstCuenta { get; set; }
        public DbSet<TC_EncabezadoEstCuentaHist> TC_EncabezadoEstCuentaHist { get; set; }
        public DbSet<TC_EstCuentaIndividual> TC_EstCuentaIndividual { get; set; }
        public DbSet<TC_Financiamiento> TC_Financiamiento { get; set; }
        public DbSet<TC_GeoLocalizacion> TC_GeoLocalizacion { get; set; }
        public DbSet<TC_Movimiento_TT> TC_Movimiento_TT { get; set; }
        public DbSet<TC_MovimientoCorte> TC_MovimientoCorte { get; set; }
        public DbSet<TC_MovimientoCorteHist> TC_MovimientoCorteHist { get; set; }
        public DbSet<TC_MovimientoFinanciamiento> TC_MovimientoFinanciamiento { get; set; }
        public DbSet<TC_MovimientoInterno> TC_MovimientoInterno { get; set; }
        public DbSet<TC_MovimientoMillas> TC_MovimientoMillas { get; set; }
        public DbSet<TC_MovimientoMillasHist> TC_MovimientoMillasHist { get; set; }
        public DbSet<TC_MovimientoTransito> TC_MovimientoTransito { get; set; }
        public DbSet<TC_MovimientoTransitoHist> TC_MovimientoTransitoHist { get; set; }
        public DbSet<TC_Programa> TC_Programa { get; set; }
        public DbSet<TC_ProspectacionMasiva> TC_ProspectacionMasiva { get; set; }
        public DbSet<TC_Sicveca> TC_Sicveca { get; set; }
        public DbSet<TC_SicvecaConsolidado> TC_SicvecaConsolidado { get; set; }
        public DbSet<TC_TablaPago> TC_TablaPago { get; set; }
        public DbSet<TC_TablaPagoProyectada> TC_TablaPagoProyectada { get; set; }
        public DbSet<TC_Tarjeta> TC_Tarjeta { get; set; }
        public DbSet<TC_TarjetaGrupo> TC_TarjetaGrupo { get; set; }
        public DbSet<TC_TarjetaHist> TC_TarjetaHist { get; set; }
        public DbSet<TC_TarjetaUsuario> TC_TarjetaUsuario { get; set; }
        public DbSet<TC_TipoTarjeta> TC_TipoTarjeta { get; set; }
        public DbSet<TC_MovimientoCorteTT> TC_MovimientoCorteTT { get; set; }
        public DbSet<TC_vwMovimientosTarjeta> TC_vwMovimientosTarjeta { get; set; }
        public DbSet<TC_vwTarjetasCuenta> TC_vwTarjetasCuenta { get; set; }
        public DbSet<TC_vwTarjetaUsuario> TC_vwTarjetaUsuario { get; set; }
        public DbSet<TC_ExtraFinanciamiento> TC_ExtraFinanciamiento { get; set; }

        void CreateContextModelTC(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TC_AdministradoresCuentaMap());
            modelBuilder.Configurations.Add(new TC_BitacoraCuentaTarjetaMap());
            modelBuilder.Configurations.Add(new TC_CierreCorteMap());
            modelBuilder.Configurations.Add(new TC_CuentaMap());
            modelBuilder.Configurations.Add(new TC_CuentaHistMap());
            modelBuilder.Configurations.Add(new TC_DetalleEstCuentaMap());
            modelBuilder.Configurations.Add(new TC_DetalleEstCuentaExtFinMap());
            modelBuilder.Configurations.Add(new TC_DetalleEstCuentaExtFinHistMap());
            modelBuilder.Configurations.Add(new TC_DetalleEstCuentaHistMap());
            modelBuilder.Configurations.Add(new TC_DetalleMovimientoMap());
            modelBuilder.Configurations.Add(new TC_DetalleMovimiento_TTMap());
            modelBuilder.Configurations.Add(new TC_DetalleMovimientoHistMap());
            modelBuilder.Configurations.Add(new TC_EncabezadoEstCtaExtFinMap());
            modelBuilder.Configurations.Add(new TC_EncabezadoEstCtaExtFinHistMap());
            modelBuilder.Configurations.Add(new TC_EncabezadoEstCuentaMap());
            modelBuilder.Configurations.Add(new TC_EncabezadoEstCuentaHistMap());
            modelBuilder.Configurations.Add(new TC_EstCuentaIndividualMap());
            modelBuilder.Configurations.Add(new TC_FinanciamientoMap());
            modelBuilder.Configurations.Add(new TC_GeoLocalizacionMap());
            modelBuilder.Configurations.Add(new TC_Movimiento_TTMap());
            modelBuilder.Configurations.Add(new TC_MovimientoCorteMap());
            modelBuilder.Configurations.Add(new TC_MovimientoCorteHistMap());
            modelBuilder.Configurations.Add(new TC_MovimientoFinanciamientoMap());
            modelBuilder.Configurations.Add(new TC_MovimientoInternoMap());
            modelBuilder.Configurations.Add(new TC_MovimientoMillasMap());
            modelBuilder.Configurations.Add(new TC_MovimientoMillasHistMap());
            modelBuilder.Configurations.Add(new TC_MovimientoTransitoMap());
            modelBuilder.Configurations.Add(new TC_MovimientoTransitoHistMap());
            modelBuilder.Configurations.Add(new TC_ProgramaMap());
            modelBuilder.Configurations.Add(new TC_ProspectacionMasivaMap());
            modelBuilder.Configurations.Add(new TC_SicvecaMap());
            modelBuilder.Configurations.Add(new TC_SicvecaConsolidadoMap());
            modelBuilder.Configurations.Add(new TC_TablaPagoMap());
            modelBuilder.Configurations.Add(new TC_TablaPagoProyectadaMap());
            modelBuilder.Configurations.Add(new TC_TarjetaMap());
            modelBuilder.Configurations.Add(new TC_TarjetaGrupoMap());
            modelBuilder.Configurations.Add(new TC_TarjetaHistMap());
            modelBuilder.Configurations.Add(new TC_TarjetaUsuarioMap());
            modelBuilder.Configurations.Add(new TC_TipoTarjetaMap());
            modelBuilder.Configurations.Add(new TC_MovimientoCorteTTMap());
            modelBuilder.Configurations.Add(new TC_vwMovimientosTarjetaMap());
            modelBuilder.Configurations.Add(new TC_vwTarjetasCuentaMap());
            modelBuilder.Configurations.Add(new TC_vwTarjetaUsuarioMap());
            modelBuilder.Configurations.Add(new TC_ExtraFinanciamientoMap());
        }
    }
}

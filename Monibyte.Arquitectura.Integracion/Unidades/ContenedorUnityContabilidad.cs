﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Contabilidad;

    public class ContenedorUnityContabilidad
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            _unityContainer.RegisterType<IAppParametroContabilidad, AppParametroContabilidad>();
            _unityContainer.RegisterType<IAppConfAsientoContabilidad, AppConfAsientoContabilidad>();
            _unityContainer.RegisterType<IAppMovimientoCorte, AppMovimientoCorte>();
            #endregion            
        }
    }
}

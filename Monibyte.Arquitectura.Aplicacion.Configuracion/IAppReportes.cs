﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Generales;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public interface IAppReportes : IAplicacionBase
    {
        PocReporte GenerarReporteGrupos(int idEmpresa, int? idGrupo, int idEntidad, string nombreCompleto);
    }
}
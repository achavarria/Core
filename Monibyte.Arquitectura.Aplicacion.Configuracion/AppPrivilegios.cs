﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public class AppPrivilegios : AplicacionBase, IAppPrivilegios
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<TC_AdministradoresCuenta> _repAdminCuenta;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _repVwEmpresasUsuario;
        private IRepositorioMnb<CL_EmpresasUsuario> _repEmpresaUsuario;
        private IRepositorioMnb<TC_TipoTarjeta> _repTipoTarjeta;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioVwTarjetaUsuario _repVwTarjetaUsuario;
        private IRepositorioTarjeta _repTarjeta;

        public AppPrivilegios(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<TC_AdministradoresCuenta> repAdminCuenta,
            IRepositorioMnb<CL_vwEmpresasUsuario> repVwEmpresasUsuario,
            IRepositorioMnb<CL_EmpresasUsuario> repEmpresaUsuario,
            IRepositorioMnb<TC_TipoTarjeta> repTipoTarjeta,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioVwTarjetaUsuario repVwTarjetaUsuario,
            IRepositorioTarjeta repTarjeta)
        {
            _repCatalogo = repCatalogo;
            _repAdminCuenta = repAdminCuenta;
            _repVwEmpresasUsuario = repVwEmpresasUsuario;
            _repEmpresaUsuario = repEmpresaUsuario;
            _repTipoTarjeta = repTipoTarjeta;
            _repPersona = repPersona;
            _repVwTarjetaUsuario = repVwTarjetaUsuario;
            _repTarjeta = repTarjeta;
        }

        public IEnumerable<PocPrivilegiosCuenta> ObtenerPrivilegiosCuenta()
        {
            var critEmpUsr = new Criteria<CL_vwEmpresasUsuario>();
            critEmpUsr.And(x => x.IdEmpresa == InfoSesion.Info.IdEmpresa);
            critEmpUsr.And(x => x.IdUsuario != InfoSesion.Info.IdUsuario);
            var critTarUsr = new Criteria<TC_vwTarjetaUsuario>();
            critTarUsr.And(x => x.IdRelacion == (int)EnumRelacionTarjeta.Titular);


            var result = _repVwTarjetaUsuario.TarjetasPermitidas(criterias: critTarUsr,
                filtroEstado: new int[1] { (int)EnumEstadoTarjeta.Todos })
                .Join(_repTipoTarjeta.Table,
                    tar => tar.IdTipoTarjeta,
                    tt => tt.IdTipoTarjeta,
                    (tar, tt) => new { tar, tt })
                .Join(_repCatalogo.List("LESTADOCUENTA"),
                    tmp => tmp.tar.IdEstadoCta,
                    est => est.IdCatalogo,
                    (tmp, est) => new { tmp.tar, tmp.tt, est })
                .GroupJoin(_repAdminCuenta.Table
                    .Join(_repVwEmpresasUsuario.SelectBy(critEmpUsr),
                        ac => ac.IdUsuario,
                        eu => eu.IdUsuario,
                        (ac, eu) => new { ac, eu })
                    .Join(_repPersona.Table,
                        tmp => tmp.eu.IdPersona,
                        per => per.IdPersona,
                        (tmp, per) => new { tmp.ac, tmp.eu, per }),
                    tmp => tmp.tar.IdCuenta,
                    grj => grj.ac.IdCuenta,
                    (tmp, grj) => new { tmp.tar, tmp.tt, tmp.est, grj })
                .SelectMany(right => right.grj.DefaultIfEmpty(),
                (left, right) => new PocPrivilegiosCuenta
                {
                    IdCuenta = left.tar.IdCuenta,
                    NumCuenta = left.tar.NumCuenta,
                    TipoTarjeta = left.tt.Descripcion,
                    DescripcionEstado = left.est.Descripcion,
                    LimiteCuenta = left.tar.LimiteCreditoCtaInter,

                    IdUsuario = right != null ? right.eu.IdUsuario as int? : null,
                    CodUsuario = right != null ? right.eu.CodUsuario : null,
                    NombreCompleto = right != null ? right.per.NombreCompleto : null,
                    //CodUsuarioEmpresa = right != null ? right.eu.CodUsuarioEmpresa : null
                });
            return result;
        }

        public void GuardarAgrupacionesCuenta(List<PocAdmAgrupacion> adms, int[] admPrevios, int idCuenta)
        {
            Func<int, int> _lamb = (a) =>
            {
                var cat = _repCatalogo.SelectById(a);
                return Convert.ToInt32(cat.Referencia2);
            };

            using (var ts = new TransactionScope())
            {
                foreach (var adm in adms)
                {
                    var exist = admPrevios.Contains(adm.IdUsuario);
                    if (exist && !adm.Agrupada)
                    {
                        _repAdminCuenta.DeleteOn(x => x.IdUsuario == adm.IdUsuario
                            && x.IdCuenta == idCuenta);

                        var existAnyOther = _repAdminCuenta.Table.Any(x => x.IdUsuario == adm.IdUsuario
                            && x.IdCuenta != idCuenta);
                        if (!existAnyOther)
                        {
                            _repEmpresaUsuario.UpdateOn(x => x.IdUsuario == adm.IdUsuario &&
                                x.IdEmpresa == InfoSesion.Info.IdEmpresa, z => new CL_EmpresasUsuario
                                {
                                    IdRole = _lamb((int)EnumTipoUsuario.Administrador),
                                    IdTipoUsuario = (int)EnumTipoUsuario.Administrador,
                                    IdRoleOperativo = null
                                });
                        }
                    }
                    if (!exist && adm.Agrupada)
                    {
                        _repAdminCuenta.Insert(new TC_AdministradoresCuenta
                        {
                            IdUsuario = adm.IdUsuario,
                            IdCuenta = idCuenta
                        });
                        _repEmpresaUsuario.UpdateOn(x => x.IdUsuario == adm.IdUsuario &&
                            x.IdEmpresa == InfoSesion.Info.IdEmpresa, z => new CL_EmpresasUsuario
                            {
                                IdRole = _lamb((int)EnumTipoUsuario.AdminCuentas),
                                IdTipoUsuario = (int)EnumTipoUsuario.AdminCuentas,
                                IdRoleOperativo = null
                            });
                    }
                }
                _repEmpresaUsuario.UnidadTbjo.Save();
                _repAdminCuenta.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void EliminarPrivilegio(PocPrivilegiosCuenta datosEntrada)
        {
            Func<int, int> _lamb = (a) =>
            {
                var cat = _repCatalogo.SelectById(a);
                return Convert.ToInt32(cat.Referencia2);
            };

            using (var ts = new TransactionScope())
            {
                _repAdminCuenta.DeleteOn(x => x.IdCuenta == datosEntrada.IdCuenta &&
                    x.IdUsuario == datosEntrada.IdUsuario);

                var existAnyOther = _repAdminCuenta.Table.Any(x => x.IdUsuario == datosEntrada.IdUsuario
                            && x.IdCuenta != datosEntrada.IdCuenta);

                if (!existAnyOther)
                {
                    _repEmpresaUsuario.UpdateOn(x => x.IdUsuario == datosEntrada.IdUsuario &&
                        x.IdEmpresa == InfoSesion.Info.IdEmpresa, z => new CL_EmpresasUsuario
                        {
                            IdRole = _lamb((int)EnumTipoUsuario.Administrador),
                            IdTipoUsuario = (int)EnumTipoUsuario.Administrador,
                            IdRoleOperativo = null
                        });
                }
                _repEmpresaUsuario.UnidadTbjo.Save();
                _repAdminCuenta.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void ValidarCuentasGrupo(int idGrupo)
        {
            var cuentasGrp = _repTarjeta.Table
                .Where(x => x.IdGrupoEmpresa == idGrupo &&
                        x.IdEmpresa == InfoSesion.Info.IdEmpresa)
                        .Select(x => new { IdCuenta = x.IdCuenta });

            if (cuentasGrp != null && cuentasGrp.Any())
            {

                var cuentasAdm = _repAdminCuenta.Table.Where(x =>
                                    x.IdUsuario == InfoSesion.Info.IdUsuario)
                                    .Select(x => new { IdCuenta = x.IdCuenta });
                foreach (var cu in cuentasGrp)
                {
                    if (!cuentasAdm.Any(x => x.IdCuenta == cu.IdCuenta))
                    {
                        var validacion = new ValidationResponse();
                        validacion.AddError("Err_0102");
                        throw new CoreException("Error del usuario Administrador de cuentas",
                            validation: validacion);
                    }
                }
            }
        }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Cargos;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public interface IAppCargosPorPersona : IAplicacionBase
    {
        void Insertar(PocCargosPorPersona registro);
        void Eliminar(PocCargosPorPersona registro);
        void Modificar(PocCargosPorPersona registro);
        DataResult<PocCargosPorPersona> Listar(PocCargosPorPersona filtro, DataRequest request);
    }
}

﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Documentacion;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Documentacion
{
    public interface IAppDocumentacion : IAplicacionBase
    {
        List<PocDocumentacion> ObtenerAyuda(int idAccion, bool novedades = false);
    }
}

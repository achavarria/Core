using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;

namespace Monibyte.Arquitectura.Dominio.Contabilidad
{
    public partial class CG_ConfiguracionAsiento : EntidadBase
    {
        public int IdConfAsiento { get; set; }
        public int IdTipoMovimiento { get; set; }
        public int IdOrigenMovimiento { get; set; }
        public int IdMoneda { get; set; }
        public Nullable<int> Orden { get; set; }
        public int IdCustomer { get; set; }
        public int IdAccount { get; set; }
        public int IdItem { get; set; }
        public string DescMemo { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

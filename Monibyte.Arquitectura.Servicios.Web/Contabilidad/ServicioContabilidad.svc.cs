﻿using Monibyte.Arquitectura.Aplicacion.Contabilidad;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Contabilidad;
using Monibyte.Arquitectura.Servicios.Nucleo;

namespace Monibyte.Arquitectura.Servicios.Web.Contabilidad
{
    public class ServicioContabilidad : ServicioUnity, IServicioContabilidad
    {
        private IAppParametroContabilidad _appParametroContabilidad;
        private IAppConfAsientoContabilidad _appConfAsientoContabilidad;
        private IAppMovimientoCorte _appMovimientoCorte;

        public ServicioContabilidad(
            IAppParametroContabilidad appParametroContabilidad,
            IAppConfAsientoContabilidad appConfAsientoContabilidad,
            IAppMovimientoCorte appMovimientoCorte)
        {
            _appParametroContabilidad = appParametroContabilidad;
            _appConfAsientoContabilidad = appConfAsientoContabilidad;
            _appMovimientoCorte = appMovimientoCorte;
        }

        public Transporte IncluirParametroContabilidad(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocParametro>();
            _appParametroContabilidad.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarParametroContabilidad(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocParametro>();
            var resultado = _appParametroContabilidad.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaParametroContabilidad(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocParametro>();
            _appParametroContabilidad.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaParametroContabilidad(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocParametro>();
            _appParametroContabilidad.Modificar(entrada);
            return transporte;
        }

        public Transporte IncluirConfAsientoContabilidad(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocConfiguracionAsiento>();
            _appConfAsientoContabilidad.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarConfAsientoContabilidad(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocConfiguracionAsiento>();
            var resultado = _appConfAsientoContabilidad.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaConfAsientoContabilidad(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocConfiguracionAsiento>();
            _appConfAsientoContabilidad.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaConfAsientoContabilidad(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocConfiguracionAsiento>();
            _appConfAsientoContabilidad.Modificar(entrada);
            return transporte;
        }

        public Transporte ListaCustomer(Transporte transporte)
        {
            var resultado = _appParametroContabilidad.ListaCustomer();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaAccount(Transporte transporte)
        {
            var resultado = _appParametroContabilidad.ListaAccount();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaItem(Transporte transporte)
        {
            var resultado = _appParametroContabilidad.ListaItem();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte GenerarArchivo(Transporte transporte)
        {
            var fecha = transporte.GetDatos<PocGenerarArchivo>();
            var resultado = _appMovimientoCorte.GenerarArchivoInvoice(fecha.FecMovimientoDesde, fecha.FecMovimientoHasta);
            transporte.SetDatos(resultado);
            return transporte;
        }
    }
}

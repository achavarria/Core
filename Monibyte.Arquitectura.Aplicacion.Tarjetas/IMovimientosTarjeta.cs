﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public interface IMovimientosTarjeta : IAplicacionBase
    {
        IQueryable<PocMovimientosTarjeta> QueryMovimientosTarjeta(
            ParamMovimientosTc parametros,
            PocTarjetaCuentaUsuario[] tarjetas,
            Criteria<TC_vwMovimientosTarjeta> criteria = null);

        IEnumerable<PocMovimientosTarjeta> ObtenerMovimientosTarjeta(PocParametrosTarjeta parametros);
        void GuardarDetalleMovimiento(List<PocMovimientosTarjeta> datosEntrada,
            string dynamicJson, bool noEditStr, List<PocArchivoMovimientoTc> archivo);
        IEnumerable<PocMovimientosTarjeta> ObtenerMovTransitoTarjeta(
            PocParametrosTarjeta parametros, bool metodoSimple = false);
        IEnumerable<PocMovimientosTarjeta> ObtieneResumenMovimientos(PocParametrosTarjeta parametros);
        IEnumerable<PocMovimientosTarjeta> ObtieneMovTipoComercio(PocParametrosTarjeta parametros, string categoria);
        IEnumerable<PocMovimientosGeo> ObtieneMovCombustible(PocParametrosTarjeta parametros);
        byte[] ObtenerReporteMovimientos(int idReporte,
            PocParametrosTarjeta datosEntrada);
        void NuevoMovimiento(PocMovimientoTarjeta registro);
    }
}

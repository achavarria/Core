using Monibyte.Arquitectura.Dominio.Generales;
using System;

namespace Monibyte.Arquitectura.Dominio.Logs
{
    public partial class LG_ColumnasAuditoria : EntidadBase
    {
        public int IdColumna { get; set; }
        public int IdTabla { get; set; }
        public string Columna { get; set; }
        public string TipoDato { get; set; }
        public int IdEsLlave { get; set; }
        public int IdEstado { get; set; }
        public string Descripcion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Tesoreria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using System.Data.Entity;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Comun.Nucleo;

namespace Monibyte.Arquitectura.Aplicacion.Tesoreria
{
    public class AppOpTipoCambio : AplicacionBase, IAppOpTipoCambio
    {
        private IRepositorioMoneda _repMoneda;
        private IRepositorioCatalogo _repCatalogo;
        private IParametroSistema _repParametroSistema;
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IRepositorioMnb<TS_TipoDeCambio> _repTipoCambio;
        private IRepositorioMnb<TS_TipoDeCambioH> _repTipoCambioHist;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _repEmpresaUsuario;

        public AppOpTipoCambio(
            IRepositorioMoneda repMoneda,
            IRepositorioCatalogo repCatalogo,
            IParametroSistema repParametroSistema,
            IRepositorioMnb<GL_Entidad> repEntidad,
            IRepositorioMnb<TS_TipoDeCambio> repTipoCambio,
            IRepositorioMnb<TS_TipoDeCambioH> repTipoCambioHist,
            IRepositorioMnb<CL_vwEmpresasUsuario> repEmpresaUsuario)
        {
            _repMoneda = repMoneda;
            _repCatalogo = repCatalogo;
            _repParametroSistema = repParametroSistema;
            _repEntidad = repEntidad;
            _repTipoCambio = repTipoCambio;
            _repTipoCambioHist = repTipoCambioHist;
            _repEmpresaUsuario = repEmpresaUsuario;
        }

        #region metodos de mantenimiento

        public void Insertar(PocTipoCambio registro)
        {
            Func<TS_TipoDeCambio, TS_TipoDeCambio> _obtener = (filtro) =>
            {
                var salida = _repTipoCambio.Table.Where(x =>
                    x.IdEntidadFinanciera == filtro.IdEntidadFinanciera &&
                    x.IdTipoCambio == filtro.IdTipoCambio &&
                    x.IdMoneda == filtro.IdMoneda).FirstOrDefault();
                return salida;
            };
            Action<TS_TipoDeCambio, int> _insert = (entrada, idCambio) =>
            {
                _repTipoCambio.DeleteOn(z => z.IdCambio == idCambio);
                _repTipoCambio.Insert(entrada);
            };

            Action<TS_TipoDeCambioH> _insertH = (entrada) =>
            {
                entrada.IdUsuarioTraslado = entrada.IdUsuarioIncluyeAud;
                entrada.FecTrasladoHist = entrada.HoraCambio;
                _repTipoCambioHist.Insert(entrada);
            };


            if (registro.FecCambio.HasValue &&
               registro.FecCambio.Value.Date == DateTime.Now.Date
               && !registro.EsEdicion)
            {
                registro.HoraCambio = DateTime.Now.TrimMilliseconds();
                using (var ts = new TransactionScope())
                {
                    var exist = _repTipoCambio.Table.Any(x =>
                    x.FecCambio == registro.FecCambio);
                    var entrada = registro.ProyectarComo<TS_TipoDeCambio>();

                    var pocParametroSistema = new PocParametroSistema() { IdParametro = "MONEDA_BASE_CONVERSION" };
                    var resultado = _repParametroSistema.ObtenerParametroSistema(pocParametroSistema);

                    entrada.IdMonedaConversion = Int32.Parse(resultado.ValParametro);
                    entrada.IdEstado = (int)EnumIdEstado.Activo;
                    entrada.IdUsuarioActualizaAud = null;
                    entrada.FecActualizaAud = null;

                    if (exist)
                    {
                        entrada.IdTipoCambio = (int)EnumIdTipoDeCambio.Compra;
                        entrada.MonTipoCambio = registro.MonTipoCambioCompra;
                        entrada.IdCambio = _fGenerales.ObtieneSgteSecuencia("SEQ_IdCambio");
                        var _cambioActualCompra = _obtener(entrada);
                        _insertH(_cambioActualCompra.ProyectarComo<TS_TipoDeCambioH>());
                        _repTipoCambioHist.UnidadTbjo.Save();
                        _insert(entrada, _cambioActualCompra.IdCambio);
                        _repTipoCambio.UnidadTbjo.Save();

                        var clone = entrada.GetClone();
                        clone.IdTipoCambio = (int)EnumIdTipoDeCambio.Venta;
                        clone.MonTipoCambio = registro.MonTipoCambioVenta;
                        clone.IdCambio = _fGenerales.ObtieneSgteSecuencia("SEQ_IdCambio");
                        var _cambioActualVenta = _obtener(clone);
                        _insertH(_cambioActualVenta.ProyectarComo<TS_TipoDeCambioH>());
                        _repTipoCambioHist.UnidadTbjo.Save();
                        _insert(clone, _cambioActualVenta.IdCambio);
                        _repTipoCambio.UnidadTbjo.Save();
                    }
                    else
                    {
                        //Tipo de Cambio - COMPRA
                        entrada.IdCambio = _fGenerales.ObtieneSgteSecuencia("SEQ_IdCambio");
                        entrada.IdTipoCambio = (int)EnumIdTipoDeCambio.Compra;
                        entrada.MonTipoCambio = registro.MonTipoCambioCompra;
                        _repTipoCambio.Insert(entrada);

                        //Tipo de Cambio - VENTA
                        var clone = entrada.GetClone();
                        clone.IdCambio = _fGenerales.ObtieneSgteSecuencia("SEQ_IdCambio");
                        clone.IdTipoCambio = (int)EnumIdTipoDeCambio.Venta;
                        clone.MonTipoCambio = registro.MonTipoCambioVenta;
                        _repTipoCambio.Insert(clone);

                    }
                    _repTipoCambio.UnidadTbjo.Save();
                    ts.Complete();
                }
            }
            else
            {
                MantenimientoTipoCambioH(registro);
            }
        }

        private void MantenimientoTipoCambioH(PocTipoCambio registro)
         {
            using (var ts = new TransactionScope())
            {
                var pocParametroSistema = new PocParametroSistema() { IdParametro = "MONEDA_BASE_CONVERSION" };
                var resultado = _repParametroSistema.ObtenerParametroSistema(pocParametroSistema);

                registro.IdMonedaConversion = Int32.Parse(resultado.ValParametro);
                registro.IdEstado = (int)EnumIdEstado.Activo;
                registro.IdUsuarioActualizaAud = null;
                registro.FecActualizaAud = null;

                var exist = _repTipoCambioHist.Table.Any(x =>
                    x.FecCambio == registro.FecCambio);

                var existActual = _repTipoCambio.Table.Any(x =>
                    x.FecCambio == registro.FecCambio);
                if (!existActual || registro.EsEdicion)
                {
                    var esAtual = false;
                    if (registro.EsEdicion && existActual)
                    {
                        esAtual = _repTipoCambio.Table.Any(x =>
                      x.FecCambio == registro.FecCambio &&
                      x.HoraCambio == registro.HoraCambio);
                    }
                    if (esAtual)
                    {
                        _repTipoCambio.UpdateOn(m => m.FecCambio == registro.FecCambio
                                 && m.IdTipoCambio == (int)EnumIdTipoDeCambio.Compra &&
                                 m.HoraCambio == registro.HoraCambio,
                                    up => new TS_TipoDeCambio
                                    {
                                        MonTipoCambio = registro.MonTipoCambioCompra
                                    });

                        _repTipoCambio.UpdateOn(m => m.FecCambio == registro.FecCambio
                            && m.IdTipoCambio == (int)EnumIdTipoDeCambio.Venta &&
                             m.HoraCambio == registro.HoraCambio,
                        up => new TS_TipoDeCambio
                        {
                            MonTipoCambio = registro.MonTipoCambioVenta
                        });
                    }
                    else if (exist && registro.EsEdicion)
                    {
                        exist = _repTipoCambioHist.Table.Any(x =>
                            x.FecCambio == registro.FecCambio &&
                            x.HoraCambio == registro.HoraCambio);
                        if (exist)
                        {
                            _repTipoCambioHist.UpdateOn(m => m.FecCambio == registro.FecCambio
                                && m.IdTipoCambio == (int)EnumIdTipoDeCambio.Compra &&
                                m.HoraCambio == registro.HoraCambio,
                            up => new TS_TipoDeCambioH
                            {
                                MonTipoCambio = registro.MonTipoCambioCompra
                            });

                            _repTipoCambioHist.UpdateOn(m => m.FecCambio == registro.FecCambio
                                && m.IdTipoCambio == (int)EnumIdTipoDeCambio.Venta &&
                                 m.HoraCambio == registro.HoraCambio,
                            up => new TS_TipoDeCambioH
                            {
                                MonTipoCambio = registro.MonTipoCambioVenta
                            });
                        }
                        else
                        {
                            throw new CoreException("Error al Incluir Tipo de Cambio", "Msj_TipoCambioExistente");
                        }
                    }
                    else if (!exist && !existActual)
                    {
                        registro.HoraCambio = registro.FecCambio.Value;
                        var entrada = registro.ProyectarComo<TS_TipoDeCambioH>();
                        entrada.IdCambio = _fGenerales.ObtieneSgteSecuencia("SEQ_IdCambio");
                        entrada.IdTipoCambio = (int)EnumIdTipoDeCambio.Compra;
                        entrada.MonTipoCambio = registro.MonTipoCambioCompra;
                        entrada.FecTrasladoHist = DateTime.Now;
                        _repTipoCambioHist.Insert(entrada);

                        var clone = entrada.GetClone();
                        clone.IdCambio = _fGenerales.ObtieneSgteSecuencia("SEQ_IdCambio");
                        clone.IdTipoCambio = (int)EnumIdTipoDeCambio.Venta;
                        clone.MonTipoCambio = registro.MonTipoCambioVenta;
                        _repTipoCambioHist.Insert(clone);
                    }
                    else if (exist || (existActual && !registro.EsEdicion) || (!exist && existActual))
                    {
                        throw new CoreException("Error al Incluir Tipo de Cambio", "Msj_TipoCambioExistente");
                    }
                }
                else if (exist || (existActual && !registro.EsEdicion))
                {
                    throw new CoreException("Error al Incluir Tipo de Cambio", "Msj_TipoCambioExistente");
                }
                _repTipoCambio.UnidadTbjo.Save();
                _repTipoCambioHist.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public DataResult<PocTipoCambio> Listar(PocTipoCambio filtro, DataRequest request)
        {
            var resultado = _repTipoCambioHist.Table
                .Join(_repCatalogo.List("LTIPOCAMBIO"),
                    tc => tc.IdTipoCambio,
                    cat => cat.IdCatalogo,
                    (tc, cat) => new { tc, cat })
                .Join(_repEntidad.Table,
                    item => item.tc.IdEntidadFinanciera,
                    ent => ent.IdEntidad,
                    (item, ent) => new { item.tc, item.cat, ent })
                .Join(_repMoneda.Table,
                    item => item.tc.IdMoneda,
                    mod => mod.IdMoneda,
                    (item, mod) => new { item.tc, item.cat, item.ent, mod })
                .Join(_repEmpresaUsuario.Table,
                    item => item.tc.IdUsuarioIncluyeAud,
                    eu => eu.IdUsuario,
                    (item, eu) => new { item.tc, item.cat, item.ent, item.mod, eu })
                .Select(sel => new PocTipoCambio
                {
                    IdCambio = sel.tc.IdCambio,
                    IdEntidadFinanciera = sel.tc.IdEntidadFinanciera,
                    IdMoneda = sel.tc.IdMoneda,
                    IdTipoCambio = sel.tc.IdTipoCambio,
                    FecCambio = sel.tc.FecCambio,
                    MonTipoCambio = sel.tc.MonTipoCambio,
                    IdEstado = sel.tc.IdEstado,
                    IdMonedaConversion = sel.tc.IdMonedaConversion,
                    HoraCambio = sel.tc.HoraCambio,
                    FecInclusionAud = sel.tc.FecInclusionAud,
                    IdUsuarioIncluyeAud = sel.tc.IdUsuarioIncluyeAud,
                    FecActualizaAud = sel.tc.FecActualizaAud,
                    IdUsuarioActualizaAud = sel.tc.IdUsuarioActualizaAud,
                    DescTipoCambio = sel.cat.Codigo,
                    DescEntidadFinanciera = sel.ent.Descripcion,
                    DescMoneda = sel.mod.Descripcion,
                    Usuario = sel.eu.AliasUsuario
                });



            var resultado2 = _repTipoCambio.Table
                .Join(_repCatalogo.List("LTIPOCAMBIO"),
                    tc => tc.IdTipoCambio,
                    cat => cat.IdCatalogo,
                    (tc, cat) => new { tc, cat })
                .Join(_repEntidad.Table,
                    item => item.tc.IdEntidadFinanciera,
                    ent => ent.IdEntidad,
                    (item, ent) => new { item.tc, item.cat, ent })
                .Join(_repMoneda.Table,
                    item => item.tc.IdMoneda,
                    mod => mod.IdMoneda,
                    (item, mod) => new { item.tc, item.cat, item.ent, mod })
                .Join(_repEmpresaUsuario.Table,
                    item => item.tc.IdUsuarioIncluyeAud,
                    eu => eu.IdUsuario,
                    (item, eu) => new { item.tc, item.cat, item.ent, item.mod, eu })
                .Select(sel => new PocTipoCambio
                {
                    IdCambio = sel.tc.IdCambio,
                    IdEntidadFinanciera = sel.tc.IdEntidadFinanciera,
                    IdMoneda = sel.tc.IdMoneda,
                    IdTipoCambio = sel.tc.IdTipoCambio,
                    FecCambio = sel.tc.FecCambio,
                    MonTipoCambio = sel.tc.MonTipoCambio,
                    IdEstado = sel.tc.IdEstado,
                    IdMonedaConversion = sel.tc.IdMonedaConversion,
                    HoraCambio = sel.tc.HoraCambio,
                    FecInclusionAud = sel.tc.FecInclusionAud,
                    IdUsuarioIncluyeAud = sel.tc.IdUsuarioIncluyeAud,
                    FecActualizaAud = sel.tc.FecActualizaAud,
                    IdUsuarioActualizaAud = sel.tc.IdUsuarioActualizaAud,
                    DescTipoCambio = sel.cat.Codigo,
                    DescEntidadFinanciera = sel.ent.Descripcion,
                    DescMoneda = sel.mod.Descripcion,
                    Usuario = sel.eu.AliasUsuario
                });

            resultado = resultado.Concat(resultado2);
            var result = resultado.OrderByDescending(p => p.FecCambio)
                .ThenByDescending(x => x.IdCambio).ToList();
            foreach (PocTipoCambio item in result)
            {
                foreach (PocTipoCambio x in result)
                {
                    if (item.HoraCambio == x.HoraCambio && item.FecCambio == x.FecCambio)
                    {
                        if (x.IdTipoCambio == (int)EnumIdTipoDeCambio.Compra)
                            item.MonTipoCambioCompra = x.MonTipoCambio;
                        else
                            item.MonTipoCambioVenta = x.MonTipoCambio;

                        if (item.IdCambio != x.IdCambio)
                            break;
                    }
                }
            }

            for (int i = 0; i < result.Count; i++)
                if (result[i].MonTipoCambioCompra == null || result[i].MonTipoCambioVenta == null)
                    result.Remove(result[i]);

            return result.ToDataResult(request);
        }

        public List<PocTipoCambio> Obtener(PocTipoCambio filtro)
        {
            var criteria = new Criteria<TS_TipoDeCambio>();
            if (filtro.IdEntidadFinanciera.HasValue)
            {
                criteria.And(x => x.IdEntidadFinanciera == filtro.IdEntidadFinanciera);
            }
            if (filtro.IdTipoCambio > 0)
            {
                criteria.And(x => x.IdTipoCambio == filtro.IdTipoCambio);
            }
            if (filtro.IdMoneda > 0)
            {
                criteria.And(x => x.IdMoneda == filtro.IdMoneda);
            }
            if (filtro.IdMonedaConversion.HasValue)
            {
                criteria.And(x => x.IdMonedaConversion == filtro.IdMonedaConversion);
            }

            var _cambioActual = _repTipoCambio.SelectBy(criteria);
            return _cambioActual.ProyectarComo<PocTipoCambio>();
        }

        #endregion

        #region IAppOpTipoCambio

        public PocTipoCambio ObtenerTipoDeCambio(PocTipoCambio filtro)
        {
            var criteria = new Criteria<TS_TipoDeCambio>();
            criteria.And(m => m.IdEntidadFinanciera == filtro.IdEntidadFinanciera);
            criteria.And(m => m.IdMoneda == filtro.IdMoneda);
            if (filtro.FecCambio != DateTime.MinValue) criteria.And(m => m.FecCambio == filtro.FecCambio);

            var resultado = _repTipoCambio
                .SelectBy(criteria)
                .Join(_repCatalogo.List("LTIPOCAMBIO"),
                    tc => tc.IdTipoCambio,
                    cat => cat.IdCatalogo,
                    (tc, cat) => new { tc, cat })
                .Join(_repEntidad.Table,
                    item => item.tc.IdEntidadFinanciera,
                    ent => ent.IdEntidad,
                    (item, ent) => new { item.tc, item.cat, ent })
                .Join(_repMoneda.Table,
                    item => item.tc.IdMoneda,
                    mod => mod.IdMoneda,
                    (item, mod) => new { item.tc, item.cat, item.ent, mod })
                .Select(sel => new PocTipoCambio
                {
                    IdCambio = sel.tc.IdCambio,
                    IdEntidadFinanciera = sel.tc.IdEntidadFinanciera,
                    IdMoneda = sel.tc.IdMoneda,
                    IdTipoCambio = sel.tc.IdTipoCambio,
                    FecCambio = sel.tc.FecCambio,
                    MonTipoCambio = sel.tc.MonTipoCambio,
                    IdEstado = sel.tc.IdEstado,
                    IdMonedaConversion = sel.tc.IdMonedaConversion,
                    HoraCambio = sel.tc.HoraCambio,
                    FecInclusionAud = sel.tc.FecInclusionAud,
                    IdUsuarioIncluyeAud = sel.tc.IdUsuarioIncluyeAud,
                    FecActualizaAud = sel.tc.FecActualizaAud,
                    IdUsuarioActualizaAud = sel.tc.IdUsuarioActualizaAud,
                    DescTipoCambio = sel.cat.Codigo,
                    DescEntidadFinanciera = sel.ent.Descripcion,
                    DescMoneda = sel.mod.Descripcion
                }).ToList();

            PocTipoCambio pocResultado = null;
            foreach (PocTipoCambio item in resultado)
            {
                if (pocResultado == null)
                    pocResultado = item;

                if (item.IdTipoCambio == (int)EnumIdTipoDeCambio.Compra)
                    pocResultado.MonTipoCambioCompra = item.MonTipoCambio;
                else
                    pocResultado.MonTipoCambioVenta = item.MonTipoCambio;
            }

            if (pocResultado == null)
                pocResultado = new PocTipoCambio() { MonTipoCambioCompra = 0, MonTipoCambioVenta = 0 };

            return pocResultado;
        }

        #endregion
    }
}
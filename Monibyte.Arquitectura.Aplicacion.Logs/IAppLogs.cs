﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Logs;

namespace Monibyte.Arquitectura.Aplicacion.Logs
{
    public interface IAppLogs : IAplicacionBase
    {
        void GuardarEventoAsync(PocLogs poco);
        void ModificarEventoAsync(string sessionCode, string codUsuario, int tipoEvento);
    }
}
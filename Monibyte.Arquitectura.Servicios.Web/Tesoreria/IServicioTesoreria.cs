﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using Monibyte.Arquitectura.Poco.Tesoreria;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Tesoreria
{
    [ServiceContract]
    public interface IServicioTesoreria : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ConvierteMonto")]
        Transporte ConvierteMonto(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListaMonedas")]
        Transporte ListaMonedas(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtieneTipoCambioBCCR")]
        Transporte ObtieneTipoCambioBCCR(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "InsertarTipoCambio")]
        Transporte InsertarTipoCambio(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarTipoCambio")]
        Transporte ListarTipoCambio(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerTipoDeCambio")]
        Transporte ObtenerTipoDeCambio(Transporte transporte);
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ObtenerTipoCambioActual",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        List<PocTipoCambio> ObtenerTipoCambioActual(PocTipoCambio filtro);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ObtienerUltimoTipoCambio")]
        decimal ObtienerUltimoTipoCambio(int IdTipoCambio, System.DateTime FecCambio, int IdMoneda, int? IdEntidad = null);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ConvierteMontoHistorico")]
        decimal ConvierteMontoHistorico(int idMonedaOrigen, decimal monto, int idMonedaDestino,
             System.DateTime fecReferencia, int idTipoCambio, int? idEntidadFinanciera = null);
    }
}

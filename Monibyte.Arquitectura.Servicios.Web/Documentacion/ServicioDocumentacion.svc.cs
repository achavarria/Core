﻿using System;
using Monibyte.Arquitectura.Aplicacion.Documentacion;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Documentacion;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Documentacion
{
    public class ServicioDocumentacion : ServicioUnity, IServicioDocumentacion
    {
        private IAppDocumentacion _appDocumentacion;

        public ServicioDocumentacion(IAppDocumentacion appDocumentacion)
        {
            _appDocumentacion = appDocumentacion;
        }

        public Transporte ObtenerAyuda(Transporte transporte)
        {
            var idAccion = transporte.GetValor<int>("idAccion");
            var novedades = transporte.GetValor<bool>("novedades");
            var resultado = _appDocumentacion.ObtenerAyuda(idAccion, novedades);
            transporte.SetDatos(resultado);
            return transporte;
        }
    }
}


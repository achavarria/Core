﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Datos.SMS.Repositorios;
    using Monibyte.Arquitectura.Dominio.SMS.Repositorios;
    public class ContenedorUnitySms
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios

            #endregion

            #region Repositorios
            _unityContainer.RegisterType<IRepositorioMensajesRespuesta, RepositorioMensajesRespuesta>();
            _unityContainer.RegisterType<IRepositorioMensajesSms, RepositorioMensajesSms>();
            #endregion
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.Cargos
{
    public class PocTipoCargo
    {
        public int IdTipoCargo { get; set; }
        public string Descripcion { get; set; }
        public int IdMoneda { get; set; }
        public int? IdOrigenCargo { get; set; }
        public int IdTipoCobro { get; set; }
        public decimal? MonCargo { get; set; }
        public int? IdTablaRangos { get; set; }
        public int IdPeriodicidad { get; set; }
        public int ValPeriodicidad { get; set; }
        public int IdMovimientoTC { get; set; }
        public int IdMovReversaTC { get; set; }
        public int IdEstado { get; set; }
        public string DescMoneda { get; set; }
        public string DescOrigenCargo { get; set; }
        public string DescTipoCobro { get; set; }
        public string DescTablaRangos { get; set; }
        public string DescPeriodicidad { get; set; }
        public string DescEstado { get; set; }
        public string DescMovimientoTC { get; set; }
        public string DescMovReversaTC { get; set; }
        public List<PocValOrigenCargo> ListaOrigenCargos { get; set; }
    }

    public class PocRangosTabla
    {
        public int IdTablaRangos  { get; set; }
        public string Descripcion { get; set; }
        public int IdMoneda { get; set; }
        public string DescMoneda { get; set; }
    }

    public class PocDetRangosCargo
    {
        public int IdTablaRangos { get; set; }
        public decimal RangoMin { get; set; }
        public decimal? RangoMax { get; set; }
        public decimal? MonCargo { get; set; }
        public string DescTablaRangos { get; set; }
    }

    public class PocDetRangosCargosPer
    {
        public int IdTablaRangos { get; set; }
        public int IdCargo { get; set; }
        public decimal RangoMin { get; set; }
        public decimal? RangoMax { get; set; }
        public decimal? MonCargo { get; set; }
        public string DescTablaRangos { get; set; }
    }

    public class PocCargosPorPersona
    {
        public int IdCargo { get; set; }
        public int IdPersona { get; set; }
        public int IdTipoCargo { get; set; }
        public DateTime FecInicio { get; set; }
        public DateTime? FecFinal { get; set; }
        public int? IdCuentaCobro { get; set; }
        public int? IdTipoCobro { get; set; }
        public decimal? MonCargo { get; set; }
        public int? IdTablaRangos { get; set; }
        public int? IdPeriodicidad { get; set; }
        public int? ValPeriodicidad { get; set; }
        public DateTime? FecUltCargo { get; set; }
        public int? IdEstado { get; set; }
        public string NombrePersona { get; set; }
        public string NumIdentificacion { get; set; }
        public string DescTipoCargo { get; set; }
        public string DescCuentaCobro { get; set; }
        public string DescTipoCobro { get; set; }
        public string DescTablaRangos { get; set; }
        public string DescPeriodicidad { get; set; }
        public string DescEstado { get; set; }
        public List<PocDetalleCargos> ListaDetalleCargos { get; set; }
        public List<PocValOrigenCargoPer> ListaOrigenCargos { get; set; }
    }


    public class PocDetalleCargos
    {
        public int IdTablaRangos { get; set; }
        public int? IdCargo { get; set; }
        public decimal RangoMin { get; set; }
        public decimal? RangoMax { get; set; }
        public decimal? MonCargo { get; set; }
        public string DescTablaRangos { get; set; }
    }

    public class PocValOrigenCargo
    {
        public int IdTipoCargo { get; set; }
        public int IdValOrigen { get; set; }
        public string Descripcion { get; set; }
    }

    public class PocValOrigenCargoPer
    {
        public int IdCargo { get; set; }
        public int? IdTipoCargo { get; set; }
        public int IdValOrigen { get; set; }
        public string Descripcion { get; set; }
    }
    
    public class PocRegistrarPlacas
    {
        public int IdEmpresa { get; set; }
        public int IdEjecutivo { get; set; }
        public int IdEstado { get; set; }
        public string Cliente { get; set; }
        public string Ejecutivo { get; set; }
        public string DescripcionEstado { get; set; }
        public DateTime FecInclusion { get; set; }
        public string Detalle { get; set; }
        public List<PocCargosPlaca> Registros { get; set; }
    }

    public class PocCargosPlaca
    {
        public int IdCargo { get; set; }
        public int IdRegistro { get; set; }
        public int IdCuenta { get; set; }
        public int IdTipoVehiculo { get; set; }
        public string Cuenta { get; set; }
        public string NombreImpresa { get; set; }
        public string TipoVehiculo { get; set; }
        public string Marca { get; set; }
        public string Estilo { get; set; }
        public int Modelo { get; set; }
        public string Placa { get; set; }
        public decimal PaqueteBase { get; set; }
        public decimal SensorCombustible { get; set; }
        public decimal BotonPanico { get; set; }
        public decimal CargoOriginal { get; set; }
        public decimal CargoActual { get; set; }
    }
}        
﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Cargos;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public interface IAppDetRangosCargo : IAplicacionBase
    {
        void Insertar(PocDetRangosCargo registro);
        void Modificar(PocDetRangosCargo registro);
        void Eliminar(PocDetRangosCargo registro);
        DataResult<PocDetRangosCargo> Listar(PocDetRangosCargo filtro, DataRequest request);        
    }
}

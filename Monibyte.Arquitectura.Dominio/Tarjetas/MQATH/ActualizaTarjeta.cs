﻿using System;
using Monibyte.Arquitectura.Comun.Nucleo.Trama;
namespace Monibyte.Arquitectura.Dominio.Tarjetas.MQATH
{
    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaActualizaTarjeta
    {
        private string _blancos = string.Empty.PadRight(87, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.ACTUALIZA_TARJETA; } set { value = TipoEntradaMsj.ACTUALIZA_TARJETA; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERCERO)]
        public string EstadoDeseado { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string MotivoCancelacion { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 87)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaActualizaTarjeta : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERCERO)]
        public string EstadoDeseado { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2, characterFill: Constantes.CARACTERCERO)]
        public string MotivoCancelacion { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 922)]
        public string Blancos { get; set; }
    }

    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaActualizaLimiteCuenta
    {
        string _blancos = string.Empty.PadRight(75, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.ACTUALIZA_LIMITE_CUENTA; } set { value = TipoEntradaMsj.ACTUALIZA_LIMITE_CUENTA; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadTramite { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadRetiro { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal LimiteCredito { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 75)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaActualizaLimiteTarjeta
    {
        string _blancos = string.Empty.PadRight(46, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.ACTUALIZA_LIMITE_TARJETA; } set { value = TipoEntradaMsj.ACTUALIZA_LIMITE_TARJETA; } }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumCuenta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadTramite { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadRetiro { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal LimiteCredito { get; set; }
        [TramaColumnSpec(required: true, maxLength: 1, defaultValue: "M")]
        public string FormaCompras { get; set; }
        [TramaColumnSpec(required: true, maxLength: 1, characterFill: Constantes.CARACTERBLANCO)]
        public string PeriodoCompras { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal LimiteAdelantos { get; set; }
        [TramaColumnSpec(required: true, maxLength: 1, characterFill: Constantes.CARACTERBLANCO)]
        public string FormaAdelantos { get; set; }
        [TramaColumnSpec(required: true, maxLength: 1, characterFill: Constantes.CARACTERBLANCO)]
        public string PeriodoAdelantos { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 46)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaActualizaLimiteTarjeta : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumCuenta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadTramite { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadRetiro { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal LimiteCredito { get; set; }
        [TramaColumnSpec(required: true, maxLength: 1, characterFill: Constantes.CARACTERBLANCO)]
        public string FormaCompras { get; set; }
        [TramaColumnSpec(required: true, maxLength: 1, characterFill: Constantes.CARACTERBLANCO)]
        public string PeriodoCompras { get; set; }
        [TramaColumnSpec(required: false, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal LimiteAdelantos { get; set; }
        [TramaColumnSpec(required: false, maxLength: 1, characterFill: Constantes.CARACTERBLANCO)]
        public string FormaAdelantos { get; set; }
        [TramaColumnSpec(required: false, maxLength: 1, characterFill: Constantes.CARACTERBLANCO)]
        public string PeriodoAdelantos { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        //[TramaColumnSpec(required: false, maxLength: 906)]
        //public string Blancos { get; set; }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaActualizaLimiteCuenta : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal LimiteCredito { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 914)]
        public string Blancos { get; set; }
    }
    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaActivaTarjeta
    {
        string _blancos = string.Empty.PadRight(87, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.ACTIVA_TARJETA; } set { value = TipoEntradaMsj.ACTIVA_TARJETA; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadCustodia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadDestino { get; set; }
        [TramaColumnSpec(required: true, maxLength: 87)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaActivaTarjeta : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadCustodia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadDestino { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 922)]
        public string Blancos { get; set; }
    }

    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaReposicionPinTarjeta
    {
        string _blancos = string.Empty.PadRight(87, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.REPOSION_PIN_TARJETA; } set { value = TipoEntradaMsj.REPOSION_PIN_TARJETA; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadTramite { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string LocalidadRetiro { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 87)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaReposicionPinTarjeta : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 926)]
        public string Blancos { get; set; }

    }

    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaInclusionSeguroTarjeta
    {
        string _blancos = string.Empty.PadRight(32, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.INCLUSION_SEGURO; } set { value = TipoEntradaMsj.INCLUSION_SEGURO; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 3, characterFill: Constantes.CARACTERBLANCO)]
        public string TipoPoliza { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERBLANCO)]
        public string NumPoliza { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40, characterFill: Constantes.CARACTERBLANCO)]
        public string Comentario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 32)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaInclusionSeguroTarjeta : RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 3, characterFill: Constantes.CARACTERBLANCO)]
        public string TipoPoliza { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERBLANCO)]
        public string NumPoliza { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40, characterFill: Constantes.CARACTERBLANCO)]
        public string Comentario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 907)]
        public string Blancos { get; set; }
    }

}

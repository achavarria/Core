﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Tarjetas;
using Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.SMS;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.TEF;
using Monibyte.Arquitectura.Dominio.TEF.Repositorios;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Monibyte.Arquitectura.Poco.TEF;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.TEF.SINPE
{
    public class OperacionesSINPE : AplicacionBase, IOperacionesSINPE
    {
        private IRepositorioMnb<TF_CedulasRegistrada> _repCedulaRegistrada;
        private IRepositorioMnb<CL_Comunicacion> _repComunicacion;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioCuenta _repCuenta;
        private IRepositorioTarjeta _repTarjeta;
        private IOperacionesATH _appOperacionesATH;
        private IFuncionesTesoreria _repFuncionTesoreria;
        private IRepositorioEmail _repEmail;
        private IRepositorioMnb<TC_ExtraFinanciamiento> _repExtraFinanc;
        private IParametrosSINPE _paramSinpe;
        private IRepositorioMnb<TF_PagoTrasaccion> _repPagosTransaccion;
        private IRepositorioPlantillaNotificacion _repPlantillaNot;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMoneda _repMoneda;
        private IRepositorioMnb<AC_MENSAJES_SMS> _repMensajeSMS;
        private IRepositorioTipoCambio _repTipoCambio;
        private IRepositorioMnb<TC_TipoTarjeta> _repTipoTarjeta;
        private IRepositorioMnb<TF_TransaccionSinpe> _repTransacionSinpe;
        private IRepositorioMnb<TF_TramaSinpe> _repTramaSinpe;
        private IRepositorioMnb<TC_vwTarjetasCuenta> _repVwTarjetasCuenta;
        private IPagosTarjeta _pagosTarjeta;
        private IRepositorioMnb<TC_EncabezadoEstCuenta> _repEncabezadoEC;
        private IRepositorioMnb<TC_EncabezadoEstCuentaHist> _repEncabezadoECHist;

        public OperacionesSINPE(
            IRepositorioMnb<TF_CedulasRegistrada> repCedulaRegistrada,
            IRepositorioMnb<CL_Comunicacion> repComunicacion,
            IRepositorioCatalogo repCatalogo,
            IRepositorioCuenta repCuenta,
            IRepositorioTarjeta repTarjeta,
            IOperacionesATH appOperacionesATH,
            IFuncionesTesoreria repFuncionTesoreria,
            IRepositorioEmail repEmail,
            IRepositorioMnb<TC_ExtraFinanciamiento> repExtraFinanc,
            IParametrosSINPE paramSinpe,
            IRepositorioMnb<TF_PagoTrasaccion> repPagosTransaccion,
            IRepositorioPlantillaNotificacion repPlantillaNot,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMoneda repMoneda,
            IRepositorioMnb<AC_MENSAJES_SMS> repMensajeSMS,
            IRepositorioTipoCambio repTipoCambio,
            IRepositorioMnb<TC_TipoTarjeta> repTipoTarjeta,
            IRepositorioMnb<TF_TransaccionSinpe> repTransacionSinpe,
            IRepositorioMnb<TF_TramaSinpe> repTramaSinpe,
            IRepositorioMnb<TC_vwTarjetasCuenta> repVwTarjetasCuenta,
            IPagosTarjeta pagosTarjeta,
            IRepositorioMnb<TC_EncabezadoEstCuenta> repEncabezadoEC,
            IRepositorioMnb<TC_EncabezadoEstCuentaHist> repEncabezadoECHist)
        {
            _repCedulaRegistrada = repCedulaRegistrada;
            _repComunicacion = repComunicacion;
            _repCatalogo = repCatalogo;
            _repCuenta = repCuenta;
            _repTarjeta = repTarjeta;
            _appOperacionesATH = appOperacionesATH;
            _repFuncionTesoreria = repFuncionTesoreria;
            _repEmail = repEmail;
            _repExtraFinanc = repExtraFinanc;
            _paramSinpe = paramSinpe;
            _repPagosTransaccion = repPagosTransaccion;
            _repPlantillaNot = repPlantillaNot;
            _repParamSistema = repParamSistema;
            _repPersona = repPersona;
            _repMoneda = repMoneda;
            _repMensajeSMS = repMensajeSMS;
            _repTipoCambio = repTipoCambio;
            _repTipoTarjeta = repTipoTarjeta;
            _repTransacionSinpe = repTransacionSinpe;
            _repTramaSinpe = repTramaSinpe;
            _repVwTarjetasCuenta = repVwTarjetasCuenta;
            _pagosTarjeta = pagosTarjeta;
            _repEncabezadoEC = repEncabezadoEC;
            _repEncabezadoECHist = repEncabezadoECHist;
        }

        #region Servicios Entrantes de otros Sistemas
        public void RegistraResultadoPagoEntrante(List<PocTransEntranteSINPE> listaTrasacciones, string xmlData)
        {
            try
            {
                TF_TramaSinpe data = new TF_TramaSinpe
                {
                    IdTramaSinpe = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_Indice"),
                    Metodo = "RegistraResultadoPagoEntrante",
                    TramaXML = xmlData
                };
                _repTramaSinpe.Insert(data);
                _repTramaSinpe.UnidadTbjo.Save();

                foreach (PocTransEntranteSINPE pocTrasaccion in listaTrasacciones)
                {
                    var estado = (int)EnumEstadosTransaccion.Recibida;
                    #region Estandarizar Estado
                    if (pocTrasaccion.CodServicio == (int)ServiciosPEL.DBD || pocTrasaccion.CodServicio == (int)ServiciosPEL.CRD)
                    {
                        switch (Convert.ToInt32(pocTrasaccion.EstadoSinpe))
                        {
                            case (int)EstadosCreDebDirectoPEL.Aprobado:
                                estado = (int)EnumEstadosTransaccion.Rechazado;
                                break;
                            case (int)EstadosCreDebDirectoPEL.Liquidado:
                                estado = (int)EnumEstadosTransaccion.Recibida;
                                break;
                            case (int)EstadosCreDebDirectoPEL.Devuelto:
                                estado = (int)EnumEstadosTransaccion.Rechazado;
                                break;
                            case (int)EstadosCreDebDirectoPEL.Excluido:
                                estado = (int)EnumEstadosTransaccion.Excluido;
                                break;
                            case (int)EstadosCreDebDirectoPEL.EnEspera:
                                estado = (int)EnumEstadosTransaccion.Ingresada;
                                break;
                        }
                    }
                    if (pocTrasaccion.CodServicio == (int)ServiciosPEL.TFO)
                    {
                        switch (Convert.ToInt32(pocTrasaccion.EstadoSinpe))
                        {
                            case (int)EstadosTFOPEL.Confirmada:
                                estado = (int)EnumEstadosTransaccion.Recibida;
                                break;
                            case (int)EstadosTFOPEL.Rechazado:
                                estado = (int)EnumEstadosTransaccion.Rechazado;
                                break;
                            case (int)EstadosTFOPEL.PorAcreditar:
                                estado = (int)EnumEstadosTransaccion.Rechazado;
                                break;
                        }
                    }
                    #endregion

                    //Validación para evitar duplicados
                    var cant_Dublicado = _repTransacionSinpe.Table.Where(w => w.CodReferenciaSinpe == pocTrasaccion.CodRefSinpe &&
                                            w.CodRefCoreBancario == pocTrasaccion.CodRefCoreBanca && w.MontoTrasaccion == pocTrasaccion.Monto).Count();
                    if (cant_Dublicado == 0)
                    {
                        #region Agrego la Trasferencia recibida
                        PocTransaccionSinpe transSinpe = new PocTransaccionSinpe
                        {
                            CodServicio = pocTrasaccion.CodServicio,
                            CodRefSinpe = pocTrasaccion.CodRefSinpe,
                            CodRefCoreBancario = pocTrasaccion.CodRefCoreBanca,
                            IdOrigenInterno = (int)EnumOrigenInterno.OtroBanco,

                            IdentOrigen = pocTrasaccion.IdentOrigen, /*?*/
                            CCOrigen = pocTrasaccion.CCOrigen,  /*?*/
                            IdentDestino = pocTrasaccion.IdentDestino,
                            CCDestino = pocTrasaccion.CCDestino,
                            IdMonedaDestino = pocTrasaccion.IdMoneda,
                            MontoPago = pocTrasaccion.Monto,
                            MontoTransaccion = pocTrasaccion.Monto,

                            FecValor = pocTrasaccion.FecValor,
                            FecLiquidacion = pocTrasaccion.FecLiquidacion,

                            Detalle = pocTrasaccion.Detalle, /*?*/
                            CodMotivoRechazo = pocTrasaccion.CodRechazo,
                            DescripcionRechazo = pocTrasaccion.DescripcionRechazo,

                            IdEstadoTrans = estado,
                            EstadoSINPE = (int)pocTrasaccion.EstadoSinpe
                        };
                        transSinpe = InsertaTransaccion(transSinpe);
                        #endregion

                        if (estado == (int)EnumEstadosTransaccion.Recibida)
                        {
                            //Identificación de la persona que le aplica el pago
                            string _numIdentificacion = string.Empty;
                            int _aplico = -110;

                            #region Buscar identificación en detalle de la transacción [Prioridad 1]
                            string ID = ObtenerIDParametroDetalle(pocTrasaccion.Detalle.ToString());
                            if (!string.IsNullOrEmpty(ID))
                            {
                                _numIdentificacion = ID;

                                var _persona = _repPersona.Table.Where(w => w.NumIdentificacion == _numIdentificacion).FirstOrDefault();
                                if (_persona != null)
                                {
                                    transSinpe.IdPersona = _persona.IdPersona;
                                    transSinpe.NombrePersona = _persona.NombreCompleto;
                                    _aplico = AplicarPagoSINPE(transSinpe, true);

                                }
                            }
                            #endregion

                            #region Verificamos que exista la persona [Prioridad 2]
                            _numIdentificacion = pocTrasaccion.IdentOrigen.ToString().Replace("-", "").TrimStart('0');
                            if (_aplico <= -110)
                            {
                                var _persona = _repPersona.Table.Where(w => w.NumIdentificacion == _numIdentificacion).FirstOrDefault();
                                if (_persona != null)
                                {
                                    transSinpe.IdPersona = _persona.IdPersona;
                                    transSinpe.NombrePersona = _persona.NombreCompleto;
                                    _aplico = AplicarPagoSINPE(transSinpe, true);
                                }
                            }
                            #endregion

                            #region Validamos si la cedula de la trasacción esta registrada para una cuenta [Prioridad 3]
                            if (_aplico <= -110)
                            {
                                var _cedulaRegistrada = _repCedulaRegistrada.Table.Where(w => w.NumIdentificacion == _numIdentificacion);
                                if (_cedulaRegistrada != null && _cedulaRegistrada.Count() == 1)
                                {
                                    transSinpe.IdCuenta = _cedulaRegistrada.FirstOrDefault().IdCuenta;
                                    _numIdentificacion = _cedulaRegistrada.FirstOrDefault().NumIdentCuenta;

                                    var _persona = _repPersona.Table.Where(w => w.NumIdentificacion == _numIdentificacion).FirstOrDefault();

                                    if (_persona != null)
                                    {
                                        transSinpe.IdPersona = _persona.IdPersona;
                                        transSinpe.NombrePersona = _persona.NombreCompleto;
                                        _aplico = AplicarPagoSINPE(transSinpe, true);
                                    }
                                }

                            }
                            #endregion

                            if (_aplico != 0)
                            {
                                if (_aplico == -101)
                                    transSinpe.DetalleError = "Extra-Financiamiento unico por moneda no encontrado";
                                else if (_aplico == -102)
                                    transSinpe.DetalleError = "Cuenta no válida o tiene más de 2 cuentas validas";
                                else if (_aplico == -103)
                                    transSinpe.DetalleError = "Problemas con el envio a MQ";
                                else if (_aplico == -104)
                                    transSinpe.DetalleError = "Monto transacción inferior para PM2";
                                else if (_aplico == -105)
                                    transSinpe.DetalleError = "Monto transacción inferior para PM2 y FINX";
                                else
                                    transSinpe.DetalleError = "Identificación no encontrada";

                                EstableceTrasaccionPendiente(transSinpe);
                            }
                        }
                        else
                        {
                            //Defino la trasacción como pendiente de pago
                            _repTransacionSinpe.UpdateOn(x => x.IdTransaccion == transSinpe.IdTransaccion,
                                z => new TF_TransaccionSinpe
                                {
                                    IdEstadoTrans = (int)EnumEstadosTransaccion.Rechazado,
                                    DetalleError = "Estado de SINPE no valido",
                                    IdUsuarioActualizaAud = Convert.ToInt32(_repParamSistema.SelectById("ID_USUARIO_BASE").ValParametro)
                                });
                        }

                    }
                    else
                    {
                        //Reporte de transacciones duplicadas
                        if (Convert.ToInt32(_repParamSistema.SelectById("ENVIAR_CORREO_SINPE_DUPLICADO").ValParametro) == (int)EnumIdEstado.Activo)
                        {
                            int numPlantilla = Convert.ToInt32(_repParamSistema.SelectById("PLANTILLA_SINPE_DUPLICADO").ValParametro);
                            var notificacion = _repPlantillaNot.ObtenerPlantillaNot(numPlantilla);

                            var asunto = notificacion.Asunto;
                            var mensaje = notificacion.Cuerpo;

                            string codMoneda = _repMoneda.SelectById(pocTrasaccion.IdMoneda).CodInternacional.ToString();

                            mensaje = mensaje.Replace("@@CEDULAORIGEN", pocTrasaccion.IdentOrigen);
                            mensaje = mensaje.Replace("@@CODREFSINPE", pocTrasaccion.CodRefSinpe);
                            mensaje = mensaje.Replace("@@MONTO", pocTrasaccion.Monto.ToString("0,0.00", CultureInfo.InvariantCulture));
                            mensaje = mensaje.Replace("@@MONEDA", codMoneda);
                            mensaje = mensaje.Replace("@@CCDESTINO", pocTrasaccion.CCDestino);
                            mensaje = mensaje.Replace("@@DETALLE", pocTrasaccion.Detalle);

                            var emailFROM = _fGenerales.ObtieneParametroCompania("REMITENTE_SINPE", int.Parse(_repParamSistema.SelectById("ID_ENTIDAD_BASE").ValParametro));
                            var emailTO = _fGenerales.ObtieneParametroCompania("CORREO_PAGO_PENDIENTE", int.Parse(_repParamSistema.SelectById("ID_ENTIDAD_BASE").ValParametro));

                            SenderConfig sender = new SenderConfig(emailFROM, null);
                            EmailConfig emailData = new EmailConfig(asunto, mensaje);

                            //Asignamos el e-mail a enviar
                            emailData.AddTo(emailTO);

                            //Enviar e-mail
                            _repEmail.EnviarEmail(sender, emailData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.FullStackTrace(), ex.Message);
                string mensaje = "Core RegistraResultadoPagoEntrante: " + ex.Message + " - " + ex.FullStackTrace();
                EnviarNotificacionError(mensaje);
            }
        }
        #endregion

        #region Pagos Pendientes
        public DataResult<PocPagoPendiente> ListarPagosPendientes(PocPagoPendiente filtro, DataRequest request)
        {
            var criteria = new Criteria<TF_TransaccionSinpe>();
            if (filtro.IdEstadoTrans > 0)
            {
                criteria.And(m => m.IdEstadoTrans == filtro.IdEstadoTrans);
            }
            else
            {
                criteria.And(m => m.IdEstadoTrans == (int)EnumEstadosTransaccion.PendientePago || m.IdEstadoTrans == (int)EnumEstadosTransaccion.PorAplicarMQ ||
                    m.IdEstadoTrans == (int)EnumEstadosTransaccion.Recibida);
            }

            var resultado =
                (from ts in _repTransacionSinpe.SelectBy(criteria)
                 join mo in _repMoneda.Table on ts.IdMonedaDestino equals mo.IdMoneda
                 join ca in _repCatalogo.Table on ts.IdEstadoTrans equals ca.IdCatalogo into cas
                 from ca in cas.DefaultIfEmpty()
                 orderby ts.FecInclusionAud descending
                 select new { ts, mo, ca }).ToList()
                .Select(tmp => new PocPagoPendiente
                {
                    IdTransaccion = tmp.ts.IdTransaccion,
                    IdentOrigen = tmp.ts.IdentificacionOrigen,
                    CCOrigen = tmp.ts.CuentaClienteOrigen,
                    IdentDestino = tmp.ts.IdentificacionDestino,
                    CCDestino = tmp.ts.CuentaClienteDestino,
                    IdMonedaDestino = tmp.ts.IdMonedaDestino,
                    MonedaDestino = tmp.mo.Descripcion,
                    MontoPago = tmp.ts.MontoPago,
                    MontoTransaccion = tmp.ts.MontoTrasaccion,
                    FecValor = tmp.ts.FecValor,
                    FecLiquidacion = tmp.ts.FecLiquidacion,
                    CodServicio = tmp.ts.CodServicio,
                    CodRefSinpe = tmp.ts.CodReferenciaSinpe,
                    CodRefCoreBancario = tmp.ts.CodRefCoreBancario,
                    IdOrigenInterno = tmp.ts.IdOrigenInterno,
                    Detalle = tmp.ts.Detalle,
                    DetalleError = tmp.ts.DetalleError,
                    CodMotivoRechazo = tmp.ts.CodMotivoRechazo,
                    DescripcionRechazo = tmp.ts.DescripcionRechazo,
                    IdEstadoTrans = tmp.ts.IdEstadoTrans,
                    EstadoTrans = tmp.ca.Descripcion,
                    EstadoSINPE = tmp.ts.EstadoSINPE,
                    IdMoneda = tmp.ts.IdMonedaDestino,
                    Monto = tmp.ts.MontoPago
                });

            return resultado.ToDataResult(request);
        }
        public List<PocPagoPendiente> ListarPagos(int IdTrasaccion)
        {
            var resultado =
                (from pg in _repPagosTransaccion.Table
                 join vtc in _repVwTarjetasCuenta.Table on new { pg.Producto, pg.SubProducto }
                      equals new { Producto = vtc.IdCuenta, SubProducto = (int?)vtc.IdTarjeta } into vtcs
                 from vtc in vtcs.DefaultIfEmpty()
                 join exf in _repExtraFinanc.Table on new { pg.Producto, pg.SubProducto }
                      equals new { Producto = exf.IdCuenta, SubProducto = (int?)exf.IdExtraFinanciamiento } into exfs
                 from exf in exfs.DefaultIfEmpty()
                 join c in _repCuenta.Table on pg.Producto equals c.IdCuenta
                 join p in _repPersona.Table on c.IdPersonaTitular equals p.IdPersona
                 join mo in _repMoneda.Table on pg.IdMoneda equals mo.IdMoneda
                 join ca in _repCatalogo.Table on pg.IdEstado equals ca.IdCatalogo into cas
                 from ca in cas.DefaultIfEmpty()
                 where pg.IdTransaccion == IdTrasaccion
                 orderby pg.FecInclusionAud descending
                 select new { pg, vtc, exf, c, p, mo, ca }).ToList()
                .Select(tmp => new PocPagoPendiente
                {
                    IdPagoTrasaccion = tmp.pg.IdPagoTrasaccion,
                    IdTransaccion = tmp.pg.IdTransaccion,
                    IdTarjeta = tmp.pg.SubProducto,
                    NumTarjeta = tmp.pg.IdTipoProducto == (int)EnumTipoProducto.TarjetaCredito ? (tmp.vtc == null ? "" : tmp.vtc.NumTarjeta) : (tmp.exf.NumExtraFinanciamiento.ToString()),
                    NombrePersona = tmp.vtc == null ? tmp.p.NombreCompleto : tmp.vtc.NombrePersona,
                    IdMoneda = tmp.pg.IdMoneda,
                    MonedaDestino = tmp.mo.Descripcion,
                    Monto = tmp.pg.MontoPago,
                    IdEstado = tmp.pg.IdEstado,
                    EstadoTrans = tmp.ca.Descripcion,
                    CodRefSiscard = tmp.pg.CodRefSiscard,
                    Justificacion = tmp.pg.Justificacion,
                    CodMovimiento = tmp.pg.CodMovimientoMQ,
                    FecLiquidacion = tmp.pg.FecInclusionAud
                }).ToList();
            return resultado;
        }
        public void AplicarPagoPendiente(PocPagoPendiente pagoSinpe)
        {
            var _cuenta = _repVwTarjetasCuenta.Table.ToList().Where(w => w.IdCuenta == pagoSinpe.IdCuenta && w.IdRelacion == (int)EnumRelacionTarjeta.Titular).FirstOrDefault();
            var _transaccion = _repTransacionSinpe.SelectById(pagoSinpe.IdTransaccion);

            var _pago = new PocPagoTransaccion
            {
                //IdPagoTrasaccion = _idPagoTrasaccion,
                IdTransaccion = pagoSinpe.IdTransaccion,
                IdTipoProducto = (int)EnumTipoProducto.TarjetaCredito,
                Producto = _cuenta.IdCuenta,
                SubProducto = _cuenta.IdTarjeta,
                IdModoAplicado = (int)EnumModoAplicado.Manual,
                NumTarjeta = _cuenta.NumTarjeta.ToString(),
                IdEstado = (int)EnumEstadoPago.PorEnviar,
                Justificacion = pagoSinpe.Justificacion,
                MontoPago = pagoSinpe.MontoPago,
                IdMoneda = pagoSinpe.IdMonedaDestino,
                FecConsumo = _transaccion.FecInclusionAud
            };
            _pago = InsertaPagoTransaccion(_pago);


            pagoSinpe.NumTarjeta = _cuenta.NumTarjeta.ToString();

            //Realizo el pago
            var _pagoMQ = _pagosTarjeta.PagarTarjetaTEF(_pago, false);

            _repTransacionSinpe.UpdateOn(w => w.IdTransaccion == pagoSinpe.IdTransaccion,
                up => new TF_TransaccionSinpe
                {
                    IdEstadoTrans = _pagoMQ.IdEstado == (int)EnumEstadoPago.Aplicada ? (int)EnumEstadosTransaccion.Aplicada : (int)EnumEstadosTransaccion.PorAplicarMQ,
                    DetalleError = _pagoMQ.DetalleError
                });

            //Mandar correo al cliente o Operaciones
            if (_pagoMQ.IdEstado == (int)EnumEstadoPago.Aplicada)
            {
                var transSinpe = pagoSinpe.ProyectarComo<PocTransaccionSinpe>();
                EnviarNotificacionExitoso(transSinpe);
            }

        }
        public void EliminarPagosPendientes(PocPagoPendiente pagoSinpe)
        {
            //Defino la trasacción como pendiente de pago
            _repTransacionSinpe.UpdateOn(x => x.IdTransaccion == pagoSinpe.IdTransaccion,
                z => new TF_TransaccionSinpe
                {
                    IdEstadoTrans = (int)EnumEstadosTransaccion.Borrado
                });
        }
        #endregion

        #region Pagos Distribuidos
        public PocEncPagoDirigido ListarPagosAplicados(int IdCuenta)
        {
            var pagosSinpe = ObtenerSINPEPorDistribuir(IdCuenta);

            var total = from p in pagosSinpe.AsEnumerable()
                        group p by new { p.IdMoneda, p.IdCuenta, p.CodMoneda, p.PagoMinimo } into g
                        select new PocPagoAplicado
                        {
                            IdCuenta = g.Key.IdCuenta,
                            IdMoneda = g.Key.IdMoneda,
                            MontoPago = g.Sum(i => i.Monto),
                            PagoMinimo = g.Key.PagoMinimo,
                            PrcDistribuible = g.Sum(i => i.Monto) - g.Key.PagoMinimo,
                            CodMoneda = g.Key.CodMoneda,
                            Descripcion = g.Sum(i => i.Monto).ToString("0,0.00", CultureInfo.CurrentCulture) + " " + g.Key.CodMoneda
                        };

            var detalle = from p in pagosSinpe.AsEnumerable()
                          select new PocPagoAplicado
                          {
                              IdCuenta = p.IdCuenta,
                              IdTarjeta = p.IdTarjeta,
                              IdMoneda = p.IdMoneda,
                              MontoPago = p.Monto,
                              CodMoneda = p.CodMoneda,
                              FechaValor = p.Fecha,
                              NumTarjeta = p.NumTarjeta
                          };

            PocEncPagoDirigido result = new PocEncPagoDirigido();
            result.TotalPagos = total.ToList();
            result.DetallePagos = detalle.ToList();

            return result;
        }

        public List<PocDirigirPago> AplicarPagoDistribuido(List<PocDirigirPago> modelo)
        {
            #region Declaracion de Variables
            string _codMQDebito = string.Empty;
            string _codMQCredito = string.Empty;
            bool _isSameCurrency = true;
            decimal _totalDirigir = 0;
            int _indiceControl = 0;
            int _idExtraFinanciamiento = 0;
            List<PocPagoTransaccion> _pagosDistribuidos = new List<PocPagoTransaccion>();

            Dictionary<string, string> codMovMQ = new Dictionary<string, string>();
            codMovMQ.Add("CTS", _repParamSistema.SelectById("COD_MOV_CRE_TRANSLADOSALDO").ValParametro);
            codMovMQ.Add("DTS", _repParamSistema.SelectById("COD_MOV_DEB_TRANSLADOSALDO").ValParametro);
            codMovMQ.Add("CCS", _repParamSistema.SelectById("COD_MOV_CRE_CONVERCIONSALDO").ValParametro);
            codMovMQ.Add("DCS", _repParamSistema.SelectById("COD_MOV_DEB_CONVERCIONSALDO").ValParametro);
            codMovMQ.Add("EXF", _repParamSistema.SelectById("COD_MOV_CRE_EXTRAFINAN").ValParametro);
            codMovMQ.Add("DOC", _repParamSistema.SelectById("COD_MOV_DEB_OTRACARTERA").ValParametro);

            IEnumerable<PocSinpeDistribuido> _transaccionesSinpe;
            IEnumerable<PocPagoAplicado> _sumatoria;
            #endregion

            try
            {
                //Obtenemos las transacción inicial y validamos el monto distribuido
                var dataSINPE = ObtenerSINPEPorDistribuir(modelo[0].IdCuentaBase);
                _transaccionesSinpe = from p in dataSINPE.AsEnumerable()
                                      where p.IdMoneda == modelo[0].IdMonedaBase
                                      select new PocSinpeDistribuido
                                      {
                                          IdCuenta = p.IdCuenta,
                                          IdTarjeta = p.IdTarjeta,
                                          IdMoneda = p.IdMoneda,
                                          Monto = p.Monto,
                                          PagoMinimo = p.PagoMinimo,
                                          IdTransaccion = p.IdTransaccion,
                                          Fecha = p.Fecha,
                                          NumTarjeta = p.NumTarjeta,
                                          CodMoneda = p.CodMoneda
                                      };

                var _cuenta = _repCuenta.SelectById(modelo[0].IdCuentaBase);

                #region Validaciones generales
                if (_transaccionesSinpe.Count() <= 0)
                {
                    throw new CoreException("Transferencias SINPE validas no encontradas", "Err_0123");
                }

                _sumatoria = from p in _transaccionesSinpe.AsEnumerable()
                             where p.IdMoneda == modelo[0].IdMonedaBase
                             group p by new { p.IdMoneda, p.IdCuenta, p.CodMoneda, p.PagoMinimo } into g
                             select new PocPagoAplicado
                             {
                                 IdCuenta = g.Key.IdCuenta,
                                 IdMoneda = g.Key.IdMoneda,
                                 MontoPago = g.Sum(i => i.Monto),
                                 PagoMinimo = g.Key.PagoMinimo,
                                 PrcDistribuible = g.Sum(i => i.Monto) - g.Key.PagoMinimo
                             };

                //sumatoria de los pagos menos el pago minimo
                if (_sumatoria.First().PrcDistribuible <= 0)
                {
                    throw new CoreException("Se ha distribuido la total de los pagos", "Err_0124");
                }
                #endregion

                var _tarjetaBase = _repVwTarjetasCuenta.Table.ToList().Where(w => w.IdCuenta == modelo[0].IdCuentaBase && w.IdRelacion == (int)EnumRelacionTarjeta.Titular).FirstOrDefault();

                foreach (PocDirigirPago pagoDirigido in modelo)
                {
                    if (pagoDirigido.MontoPago <= 0)
                    {
                        throw new CoreException("El monto a distribuir debe ser superior a cero", "Err_0125");
                        //pagoDirigido.CodError = 12;
                        //pagoDirigido.DesError = "El monto a distribuir debe ser superior a cero";
                        //break;
                    }
                    _idExtraFinanciamiento = 0;
                    pagoDirigido.NumTarjetaBase = _tarjetaBase.NumTarjeta;
                    pagoDirigido.TarjetaHabiente = _tarjetaBase.NombreImpreso;

                    #region Ajuste de monto y tipo de cambio
                    if (pagoDirigido.IdMoneda == pagoDirigido.IdMonedaBase)
                    {
                        _isSameCurrency = true;
                        _totalDirigir += pagoDirigido.MontoPago;
                    }
                    else
                    {
                        _isSameCurrency = false;
                        _totalDirigir += pagoDirigido.MontoPagoTC;
                    }
                    #endregion

                    #region Definimos los movimientos para cada pago dirigido
                    _codMQDebito = string.Empty;
                    _codMQCredito = string.Empty;
                    switch (pagoDirigido.IdAccionPago)
                    {
                        case (int)EnumAccionPago.Traslado:
                            _codMQDebito = codMovMQ["DTS"];
                            _codMQCredito = codMovMQ["CTS"];
                            break;
                        case (int)EnumAccionPago.TrasladoAExtraIntra:
                            _codMQDebito = codMovMQ["DOC"];
                            _codMQCredito = codMovMQ["EXF"];
                            break;
                        case (int)EnumAccionPago.ConvertirMontoMismaTarjeta:
                            _codMQDebito = codMovMQ["DCS"];
                            _codMQCredito = codMovMQ["CCS"];
                            break;
                        case (int)EnumAccionPago.ConvertirMontoOtraTarjeta:
                            _codMQDebito = codMovMQ["DCS"];
                            _codMQCredito = codMovMQ["CCS"];
                            break;
                        case (int)EnumAccionPago.ConvertirMontoExtraIntra:
                            _codMQDebito = codMovMQ["DOC"];
                            _codMQCredito = codMovMQ["EXF"];
                            break;
                        default:
                            pagoDirigido.CodError = 16;
                            pagoDirigido.DesError = "Acción Invalida";
                            break;
                    }
                    if (pagoDirigido.CodError == 16)
                    {
                        //Continuar con el siguiente pago, saltar el ciclo for.
                        break;
                    }
                    #endregion

                    if (_codMQCredito == codMovMQ["EXF"])
                    {
                        var extra = _repExtraFinanc.SelectById(pagoDirigido.IdExtraFinanciamiento);
                        pagoDirigido.NumExtraFinanciamiento = extra.NumExtraFinanciamiento;
                        _idExtraFinanciamiento = extra.IdExtraFinanciamiento;
                    }
                    else
                    {
                        pagoDirigido.IdExtraFinanciamiento = 0;
                    }
                    if (_codMQCredito != codMovMQ["EXF"] && pagoDirigido.IdTarjeta <= 0)
                    {
                        var _titular = _repVwTarjetasCuenta.Table.Where(w => w.IdCuenta == pagoDirigido.IdCuenta && w.IdRelacion == (int)EnumRelacionTarjeta.Titular).FirstOrDefault();
                        pagoDirigido.IdTarjeta = _titular.IdTarjeta;
                        pagoDirigido.NumTarjeta = _titular.NumTarjeta;
                    }

                    #region Genero los objetos del movimiento
                    _indiceControl++;
                    var _movDebito = new PocPagoTransaccion
                    {
                        //IdTransaccion = _transaccionSinpe.IdTransaccion,
                        Producto = pagoDirigido.IdCuentaBase,
                        SubProducto = pagoDirigido.IdTarjetaBase,
                        IdMoneda = pagoDirigido.IdMonedaBase,
                        MontoPago = _isSameCurrency ? pagoDirigido.MontoPago : pagoDirigido.MontoPagoTC,
                        NumTarjeta = pagoDirigido.NumTarjetaBase,

                        FecConsumo = DateTime.Now,
                        IdModoAplicado = (int)EnumModoAplicado.Manual,
                        IdEstado = (int)EnumEstadoPago.PorEnviar,

                        IdTipoProducto = _codMQDebito == codMovMQ["EXF"] ? (int)EnumTipoProducto.Extra : (int)EnumTipoProducto.TarjetaCredito,
                        CodMovimientoMQ = _codMQDebito
                    };
                    _pagosDistribuidos.Add(_movDebito);
                    var _movCredito = new PocPagoTransaccion
                    {
                        IndiceControl = _indiceControl,
                        //IdTransaccion = _transaccionSinpe.IdTransaccion,
                        Producto = pagoDirigido.IdCuenta,
                        SubProducto = _codMQCredito == codMovMQ["EXF"] ? _idExtraFinanciamiento : pagoDirigido.IdTarjeta,
                        IdMoneda = pagoDirigido.IdMoneda,
                        MontoPago = pagoDirigido.MontoPago,
                        NumTarjeta = pagoDirigido.NumTarjeta,
                        NumExtraFinanciamiento = pagoDirigido.NumExtraFinanciamiento.ToString(),

                        FecConsumo = DateTime.Now,
                        IdModoAplicado = (int)EnumModoAplicado.Manual,
                        IdEstado = (int)EnumEstadoPago.PorEnviar,

                        IdTipoProducto = _codMQCredito == codMovMQ["EXF"] ? (int)EnumTipoProducto.Extra : (int)EnumTipoProducto.TarjetaCredito,
                        CodMovimientoMQ = _codMQCredito
                    };
                    _pagosDistribuidos.Add(_movCredito);
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Logger.Log(ex.FullStackTrace(), ex.Message);
                throw new CoreException("En este momento no podemos procesar los pagos dirigidos", "Err_0126");
            }

            //Validamos los montos de todos los pagos
            if (_sumatoria.First().PrcDistribuible < _totalDirigir)
            {
                throw new CoreException("Los pagos son superiores al disponible", "Err_0127");
            }

            //Obtengo las transacciones 
            List<int> arrTransaccion = new List<int>();
            foreach (PocSinpeDistribuido item in _transaccionesSinpe)
            {
                arrTransaccion.Add(item.IdTransaccion);
            }

            //Insertamos todo el grupo de movimientos que vamos a realizar
            using (var ts = new TransactionScope())
            {
                //Por posicion el IdPagoTransaccion de referencia es del movimiento credito
                var _idTransaccion = -1;
                foreach (PocPagoTransaccion pago in _pagosDistribuidos)
                {
                    //Por orden en que se agrega, el primer registro sera un debito
                    if (pago.CodMovimientoMQ == codMovMQ["DTS"] || pago.CodMovimientoMQ == codMovMQ["DCS"]
                            || pago.CodMovimientoMQ == codMovMQ["DOC"])
                    {
                        _idTransaccion = CargarPagoATransaccion(arrTransaccion, pago.MontoPago);
                        pago.SubProducto = _repPagosTransaccion.Table.Where(w => w.IdTransaccion == _idTransaccion && w.CodMovimientoMQ == null && w.IdTipoProducto == (int)EnumTipoProducto.TarjetaCredito).OrderByDescending(o => o.FecInclusionAud).FirstOrDefault().SubProducto;
                    }

                    pago.IdTransaccion = _idTransaccion;
                    var resultado = InsertaPagoTransaccion(pago, false);
                    pago.IdPagoTrasaccion = resultado.IdPagoTrasaccion;
                    pago.NumComprobanteInterno = resultado.NumComprobanteInterno;
                }
                _indiceControl = 0;
                foreach (PocDirigirPago pagoDirigido in modelo)
                {
                    _indiceControl++;
                    pagoDirigido.CodAutorización = _pagosDistribuidos.Where(w => w.Producto == pagoDirigido.IdCuenta && w.IndiceControl == _indiceControl
                            && w.NumTarjeta == pagoDirigido.NumTarjeta
                            && w.MontoPago == pagoDirigido.MontoPago).FirstOrDefault().NumComprobanteInterno;
                }
                ts.Complete();
            }

            var respuesta = AplicarMovimientoDirigido(_pagosDistribuidos, codMovMQ);

            foreach (PocDirigirPago pagoDirigido in modelo)
            {
                var pagoMQ = respuesta.Where(w => w.NumComprobanteInterno == pagoDirigido.CodAutorización).FirstOrDefault();
                pagoDirigido.CodReferenciaSiscard = pagoMQ.CodRefSiscard;
                pagoDirigido.DesError = pagoMQ.DetalleError;
                pagoDirigido.CodError = pagoMQ.DetalleError != null ? 20 : 0;
            }
            return modelo;
        }
        #endregion

        #region Metodos Privados Utilitarios
        private PocTransaccionSinpe InsertaTransaccion(PocTransaccionSinpe pocEntrada)
        {
            pocEntrada.FecInclusionAud = DateTime.Now;
            pocEntrada.UsuarioRegistra = _repParamSistema.SelectById("ID_USUARIO_BASE").ValParametro;
            pocEntrada.CodEntidadDestino = _paramSinpe.ObtenerParamSinpe("CodEntidadBase", "Pagos");
            pocEntrada.IdTransaccion = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdTransaccion");

            var trans = new TF_TransaccionSinpe
            {
                FecInclusionAud = pocEntrada.FecInclusionAud,
                IdUsuarioIncluyeAud = Convert.ToInt32(pocEntrada.UsuarioRegistra),
                CodEntidadDestino = pocEntrada.CodEntidadDestino,
                IdTransaccion = pocEntrada.IdTransaccion,
                UsuarioRegistra = pocEntrada.UsuarioRegistra,

                CodServicio = pocEntrada.CodServicio,
                CodReferenciaSinpe = pocEntrada.CodRefSinpe,
                CodRefCoreBancario = pocEntrada.CodRefCoreBancario,
                IdOrigenInterno = pocEntrada.IdOrigenInterno,

                IdentificacionOrigen = pocEntrada.IdentOrigen,
                CuentaClienteOrigen = pocEntrada.CCOrigen,
                IdentificacionDestino = pocEntrada.IdentDestino,
                CuentaClienteDestino = pocEntrada.CCDestino,
                IdMonedaDestino = pocEntrada.IdMonedaDestino,
                MontoPago = pocEntrada.MontoPago,
                MontoTrasaccion = pocEntrada.MontoTransaccion,

                FecValor = pocEntrada.FecValor ?? DateTime.Now,
                FecLiquidacion = pocEntrada.FecLiquidacion,

                Detalle = pocEntrada.Detalle,
                CodMotivoRechazo = pocEntrada.CodMotivoRechazo,
                DescripcionRechazo = pocEntrada.DescripcionRechazo,

                IdEstadoTrans = pocEntrada.IdEstadoTrans,
                EstadoSINPE = pocEntrada.EstadoSINPE,
                IdEsDistribuible = (int)EnumIdSiNo.Si
            };

            _repTransacionSinpe.Insert(trans);
            _repTransacionSinpe.UnidadTbjo.Save();

            return pocEntrada;
        }
        private PocPagoTransaccion InsertaPagoTransaccion(PocPagoTransaccion pocEntrada, bool isSINPE = true)
        {
            pocEntrada.IdPagoTrasaccion = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdPagoTrasaccion");
            pocEntrada.NumComprobanteInterno = isSINPE == true ? "SINPE-" + pocEntrada.IdPagoTrasaccion.ToString("0000000") : pocEntrada.IdPagoTrasaccion.ToString("0000000000000");
            //_repFuncionesGenerales.ObtieneSgteSecuencia("SEQ_IdRefProcesador").ToString();
            var _pago = pocEntrada.ProyectarComo<TF_PagoTrasaccion>();

            _repPagosTransaccion.Insert(_pago);
            _repPagosTransaccion.UnidadTbjo.Save();

            return pocEntrada;
        }
        private int AplicarPagoSINPE(PocTransaccionSinpe transSinpe, bool EsPagoDirecto)
        {
            bool _banderaRetorno = false;
            bool _esPagoBase = true;
            int _esDistribuible = (int)EnumIdSiNo.Si;
            try
            {
                //Convertimos el monto a dolares
                decimal factorConversionDolares = Convert.ToDecimal(_repTipoCambio.ObtenerUltTCVentaXMoneda(4, (int)EnumMoneda.Colones, (int)EnumMoneda.Dolares).MonTipoCambio ?? 1);
                decimal montoDolarizado = transSinpe.IdMonedaDestino == (int)EnumMoneda.Colones ?
                    _repFuncionTesoreria.ConvierteMontoHistorico(new ParamsConvierteMontoHistoricoTS
                    {
                        idMonedaOrigen = (int)EnumMoneda.Colones,
                        monto = transSinpe.MontoPago,
                        idMonedaDestino = (int)EnumMoneda.Dolares,
                        fecReferencia = (DateTime.Now).Date,
                        idTipoCambio = (int)EnumIdTipoDeCambio.Venta
                    }) :
                    transSinpe.MontoPago;

                #region Buscamos la Cuenta
                //Obtenemos las cuentas con la validaciones de la regla de negocio
                var criteriaCuentas = new Criteria<TC_vwTarjetasCuenta>(); ;
                criteriaCuentas.And(m => m.CodEstadoPago == "P");

                if (transSinpe.IdCuenta > 0)
                {
                    //Para el caso de las cedulas registradas que tiene definida una cuenta
                    criteriaCuentas.And(m => m.IdCuenta == transSinpe.IdCuenta);
                }

                //Código en el detalle - TJ 
                int _idTajeta = ObtenerTJParametroDetalle(transSinpe.Detalle, transSinpe.IdPersona);
                if (_idTajeta > 0)
                {
                    criteriaCuentas.And(m => m.IdTarjeta == _idTajeta);
                    criteriaCuentas.And(m => (m.IdPersona == transSinpe.IdPersona || m.IdPersonaTitular == transSinpe.IdPersona));
                }
                else
                {
                    criteriaCuentas.And(m => m.IdRelacion == (int)EnumRelacionTarjeta.Titular);
                    criteriaCuentas.And(m => m.IdPersonaTitular == transSinpe.IdPersona);
                }

                //Obtenemos las tarjetas validas para la persona
                //Validamos que los saldos sean mayores a cero y que el monto no supere el saldo de una moneda + el porcentaje de saldo a favor.
                var _cuentas = (from tc in _repVwTarjetasCuenta.SelectBy(criteriaCuentas)
                                join c in _repCuenta.Table on tc.IdCuenta equals c.IdCuenta
                                join tt in _repTipoTarjeta.Table on tc.IdTipoTarjeta equals tt.IdTipoTarjeta
                                select new { tc, c, tt }).ToList()
                    .Where(w => (w.tc.SaldoLocal > 0 || w.tc.SaldoInternacional > 0 || (w.c.SaldoExtraFinLocal ?? 0) > 0 || (w.c.SaldoExtraFinInter ?? 0) > 0) &&
                        (
                          (montoDolarizado - (((w.tc.SaldoLocal + (w.c.SaldoExtraFinLocal ?? 0)) / factorConversionDolares) + (w.tc.SaldoInternacional + (w.c.SaldoExtraFinInter ?? 0))))
                          <= (w.tt.IdTipoSaldoAFavor == (int)EnumTipoSaldoFavor.Porcentaje ? (w.tc.LimiteCreditoCtaInter * w.tt.SaldoAFavor) : w.tt.SaldoAFavor)
                        )
                    ).ToList();
                #endregion

                //Solo aplicamos el Pago a MQ si encontramos una cuenta
                if (_cuentas.Count() == 1)
                {
                    var _cuenta = _cuentas.FirstOrDefault();
                    transSinpe.NumTarjeta = _cuenta.tc.NumTarjeta.ToString();

                    int _subProducto = _cuenta.tc.IdTarjeta;
                    int _idTipoProducto = (int)EnumTipoProducto.TarjetaCredito;
                    var _pago = new PocPagoTransaccion();
                    Dictionary<string, decimal> listaPago = new Dictionary<string, decimal>();

                    if (transSinpe.Detalle.ToUpper().Contains("PC2") || transSinpe.Detalle.ToUpper().Contains("PM2"))
                    {
                        _esPagoBase = false;
                        _esDistribuible = (int)EnumIdSiNo.No;
                        #region Pagos de contado y pagos minimos
                        bool _pagoInferiorSaldos = false;
                        decimal _cuotaExtra = 0;

                        decimal pagoCRC = transSinpe.Detalle.ToUpper().Contains("PM2") ? _cuenta.c.PagoMinLocal ?? 0 : _cuenta.c.PagoContadoLocal ?? 0;
                        decimal pagoUSD = transSinpe.Detalle.ToUpper().Contains("PM2") ? _cuenta.c.PagoMinInter ?? 0 : _cuenta.c.PagoContadoInter ?? 0;

                        #region Colonizo o dolarizo para validaciones de monto cubre pagos
                        //Las converciones se realizan con idTipoDeCompra contrarias a las reales, ya que son son validadicones con base a montos en su moneda, no a la moneda de la transferencia
                        decimal pUSDColonizado = pagoUSD > 0 && transSinpe.IdMonedaDestino == (int)EnumMoneda.Colones ? _repFuncionTesoreria.ConvierteMontoHistorico(new ParamsConvierteMontoHistoricoTS
                        {
                            idMonedaOrigen = (int)EnumMoneda.Dolares,
                            monto = pagoUSD,
                            idMonedaDestino = (int)EnumMoneda.Colones,
                            fecReferencia = DateTime.Now,
                            idTipoCambio = (int)EnumIdTipoDeCambio.Venta
                        })
                            : 0;
                        decimal pCRCDolarizado = pagoCRC > 0 && transSinpe.IdMonedaDestino == (int)EnumMoneda.Dolares ? _repFuncionTesoreria.ConvierteMontoHistorico(new ParamsConvierteMontoHistoricoTS
                        {
                            idMonedaOrigen = (int)EnumMoneda.Colones,
                            monto = pagoCRC,
                            idMonedaDestino = (int)EnumMoneda.Dolares,
                            fecReferencia = DateTime.Now,
                            idTipoCambio = (int)EnumIdTipoDeCambio.Compra
                        })
                             : 0;
                        #endregion

                        #region Validaciones
                        if ((transSinpe.IdMonedaDestino == (int)EnumMoneda.Colones ? pagoCRC + pUSDColonizado : pagoUSD + pCRCDolarizado) > transSinpe.MontoPago)
                        {
                            //Monto transacción inferior a los pagos minimos
                            //return -104;
                            if ((transSinpe.IdMonedaDestino == (int)EnumMoneda.Colones ? pagoCRC : pagoUSD) >= transSinpe.MontoPago)
                            {
                                _esPagoBase = true;
                            }

                            _pagoInferiorSaldos = true;
                        }

                        if (!_esPagoBase && transSinpe.Detalle.ToUpper().Contains("FINX"))
                        {
                            int idExtrafinanciamiento = ObtenerFINParametroDetalle(transSinpe.Detalle, _cuenta.c.IdCuenta, transSinpe.IdMonedaDestino);
                            if (idExtrafinanciamiento == 0 || idExtrafinanciamiento == -101)
                            {
                                //Retorno porque no se encontro un unico Extra-Financiamiento
                                return -101;
                            }
                            //Cuota seria en la moneda destino
                            _cuotaExtra = _repExtraFinanc.SelectById(idExtrafinanciamiento).CuotaMensual;

                            if ((transSinpe.IdMonedaDestino == (int)EnumMoneda.Colones ? pagoCRC + pUSDColonizado + _cuotaExtra : pagoUSD + pCRCDolarizado + _cuotaExtra) > transSinpe.MontoPago)
                            {
                                //Monto transacción inferior a los pagos minimos y FINX
                                //return -105;
                                if (_pagoInferiorSaldos || _cuotaExtra >= transSinpe.MontoPago)
                                {
                                    _esPagoBase = true;
                                }
                                _pagoInferiorSaldos = true;
                            }

                            if (idExtrafinanciamiento > 0)
                            {
                                _subProducto = idExtrafinanciamiento;
                                _idTipoProducto = (int)EnumTipoProducto.Extra;
                            }
                        }
                        #endregion

                        if (_esPagoBase == false)
                        {
                            if (transSinpe.IdMonedaDestino == (int)EnumMoneda.Colones)
                            {
                                if (pagoUSD > 0)
                                {
                                    listaPago.Add("PUSD", _pagoInferiorSaldos ?
                                        _repFuncionTesoreria.ConvierteMontoHistorico(new ParamsConvierteMontoHistoricoTS
                                        {
                                            idMonedaOrigen = (int)EnumMoneda.Colones,
                                            monto = transSinpe.MontoPago - (pagoCRC + _cuotaExtra),
                                            idMonedaDestino = (int)EnumMoneda.Dolares,
                                            fecReferencia = DateTime.Now,
                                            idTipoCambio = (int)EnumIdTipoDeCambio.Venta
                                        })
                                        : pagoUSD);
                                }
                                if (pagoCRC > 0)
                                {
                                    listaPago.Add("PCRC", _pagoInferiorSaldos ? (pagoUSD > 0 ? pagoCRC : transSinpe.MontoPago - _cuotaExtra) : transSinpe.MontoPago - (pUSDColonizado + _cuotaExtra));
                                }
                                if (_idTipoProducto == (int)EnumTipoProducto.Extra)
                                {
                                    listaPago.Add("PEX", _cuotaExtra);
                                }
                            }
                            else if (transSinpe.IdMonedaDestino == (int)EnumMoneda.Dolares)
                            {
                                if (pagoCRC > 0)
                                {
                                    listaPago.Add("PCRC", _pagoInferiorSaldos ?
                                        _repFuncionTesoreria.ConvierteMontoHistorico(new ParamsConvierteMontoHistoricoTS
                                        {
                                            idMonedaOrigen = (int)EnumMoneda.Dolares,
                                            monto = transSinpe.MontoPago - (pagoUSD + _cuotaExtra),
                                            idMonedaDestino = (int)EnumMoneda.Colones,
                                            fecReferencia = DateTime.Now,
                                            idTipoCambio = (int)EnumIdTipoDeCambio.Compra
                                        }
                                        )
                                        : pagoCRC);
                                }
                                if (pagoUSD > 0)
                                {
                                    listaPago.Add("PUSD", _pagoInferiorSaldos ? (pagoCRC > 0 ? pagoUSD : transSinpe.MontoPago - _cuotaExtra) : transSinpe.MontoPago - (pCRCDolarizado + _cuotaExtra));
                                }
                                if (_idTipoProducto == (int)EnumTipoProducto.Extra)
                                {
                                    listaPago.Add("PEX", _cuotaExtra);
                                }
                            }

                            foreach (var pago in listaPago)
                            {
                                //Si se encuentran pagos negativos, por inconsistencias se realiza pago normal.
                                if (pago.Value < 0)
                                {
                                    _esPagoBase = true;
                                }
                            }

                            if (_esPagoBase == false)
                            {
                                _banderaRetorno = true;
                                #region Pagos MQ
                                foreach (var pago in listaPago)
                                {
                                    if (pago.Value > 0)
                                    {
                                        int _idmoneda = pago.Key == "PUSD" ? (int)EnumMoneda.Dolares : (int)EnumMoneda.Colones;

                                        _idTipoProducto = (int)EnumTipoProducto.TarjetaCredito;
                                        if (pago.Key == "PEX")
                                        {
                                            _idmoneda = transSinpe.IdMonedaDestino;
                                            _idTipoProducto = (int)EnumTipoProducto.Extra;
                                            _pago.NumExtraFinanciamiento = _repExtraFinanc.SelectById(_subProducto).NumExtraFinanciamiento.ToString();
                                        }

                                        _pago.IdTransaccion = transSinpe.IdTransaccion;
                                        _pago.IdTipoProducto = _idTipoProducto;
                                        _pago.Producto = _cuenta.tc.IdCuenta;
                                        _pago.SubProducto = _idTipoProducto == (int)EnumTipoProducto.Extra ? _subProducto : _cuenta.tc.IdTarjeta;
                                        _pago.IdModoAplicado = (int)EnumModoAplicado.Automatico;
                                        _pago.NumTarjeta = _cuenta.tc.NumTarjeta.ToString();
                                        _pago.IdEstado = (int)EnumEstadoPago.PorEnviar;
                                        _pago.Justificacion = transSinpe.Justificacion;
                                        _pago.IdMoneda = _idmoneda;
                                        _pago.MontoPago = pago.Value;
                                        _pago = InsertaPagoTransaccion(_pago);

                                        #region Realizo el pago a MQ
                                        if (_idTipoProducto == (int)EnumTipoProducto.Extra)
                                        {
                                            _pago = _pagosTarjeta.PagarExtraIntra(_pago);
                                        }
                                        else
                                        {
                                            _pago = _pagosTarjeta.PagarTarjetaTEF(_pago, EsPagoDirecto);
                                        }

                                        if (_pago.IdEstado != (int)EnumEstadoPago.Aplicada)
                                        {
                                            break;
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                                _banderaRetorno = false;
                            }
                        }
                        #endregion
                    }

                    if (_esPagoBase)
                    {
                        #region Pagos Normales/ID/TJ/FIN123456/FINX
                        //Código en el detalle - FINX / FIN123456
                        int idExtrafinanciamiento = ObtenerFINParametroDetalle(transSinpe.Detalle, _cuenta.c.IdCuenta, transSinpe.IdMonedaDestino);
                        if (idExtrafinanciamiento > 0)
                        {
                            _subProducto = idExtrafinanciamiento;
                            _idTipoProducto = (int)EnumTipoProducto.Extra;
                            _pago.NumExtraFinanciamiento = _repExtraFinanc.SelectById(idExtrafinanciamiento).NumExtraFinanciamiento.ToString();
                            _esDistribuible = (int)EnumIdSiNo.No;
                        }
                        if (idExtrafinanciamiento == -101)
                        {
                            //Retorno porque no se encontro un unico Extra-Financiamiento
                            return -101;
                        }

                        #region Insert de Pago Transacción
                        _pago.IdTransaccion = transSinpe.IdTransaccion;
                        _pago.IdTipoProducto = _idTipoProducto;
                        _pago.Producto = _cuenta.tc.IdCuenta;
                        _pago.SubProducto = _subProducto;
                        _pago.IdModoAplicado = (int)EnumModoAplicado.Automatico;
                        _pago.NumTarjeta = _cuenta.tc.NumTarjeta.ToString();
                        _pago.IdEstado = (int)EnumEstadoPago.PorEnviar;
                        _pago.Justificacion = transSinpe.Justificacion;
                        _pago.IdMoneda = transSinpe.IdMonedaDestino;
                        _pago.MontoPago = transSinpe.MontoPago;

                        _pago = InsertaPagoTransaccion(_pago);
                        #endregion

                        _banderaRetorno = true;
                        #region Realizo el pago a MQ
                        if (_idTipoProducto == (int)EnumTipoProducto.Extra)
                        {
                            _pago = _pagosTarjeta.PagarExtraIntra(_pago);
                        }
                        else
                        {
                            _pago = _pagosTarjeta.PagarTarjetaTEF(_pago, EsPagoDirecto);
                        }
                        _banderaRetorno = false;
                        #endregion
                        #endregion
                    }

                    _repTransacionSinpe.UpdateOn(w => w.IdTransaccion == transSinpe.IdTransaccion,
                    up => new TF_TransaccionSinpe
                    {
                        IdEstadoTrans = _pago.IdEstado == (int)EnumEstadoPago.Aplicada ? (int)EnumEstadosTransaccion.Aplicada : (int)EnumEstadosTransaccion.PorAplicarMQ,
                        DetalleError = _pago.DetalleError,
                        IdEsDistribuible = _esDistribuible,
                        IdUsuarioActualizaAud = Convert.ToInt32(_repParamSistema.SelectById("ID_USUARIO_BASE").ValParametro)
                    });

                    //Mandar correo al cliente o Operaciones
                    if (_pago.IdEstado == (int)EnumEstadoPago.Aplicada)
                    {
                        EnviarNotificacionExitoso(transSinpe, listaPago);
                    }
                    else
                    {
                        EnviarCorreoPagoPendiente(transSinpe);
                    }
                    return 0;
                }

                return -102;
            }
            catch (Exception ex)
            {
                Logger.Log(ex.FullStackTrace(), ex.Message);
                string mensaje = "Core AplicarPagoSINPE: " + ex.Message + " - " + ex.FullStackTrace();
                EnviarNotificacionError(mensaje);

                if (_banderaRetorno)
                    return -103;
                else
                    return 0;
            }
        }

        private List<PocPagoTransaccion> AplicarMovimientoDirigido(List<PocPagoTransaccion> pagos, Dictionary<string, string> codMovMQ)
        {
            foreach (PocPagoTransaccion pago in pagos)
            {
                try
                {
                    //Aplico Miselaneo MQ
                    if (pago.IdTipoProducto == (int)EnumTipoProducto.Extra)
                    {
                        var resultado = _pagosTarjeta.PagarExtraIntraDirigido(pago);
                        pago.CodRefSiscard = resultado.NumReferencia;
                    }
                    else
                    {
                        var entradaServicio = new PocEntradaMiscelaneosTarjeta();
                        entradaServicio.CodMovimiento = pago.CodMovimientoMQ;
                        entradaServicio.MontoTransaccion = pago.MontoPago;
                        entradaServicio.NumTarjeta = pago.NumTarjeta;
                        entradaServicio.NumReferencia = pago.NumComprobanteInterno;
                        entradaServicio.MonedaTransaccion = _repMoneda.SelectById(pago.IdMoneda).CodAlpha2;
                        entradaServicio.FechaConsumo = DateTime.Now;

                        var resultado = _appOperacionesATH.AplicaMiscelaneosTarjeta(entradaServicio);
                        pago.CodRefSiscard = resultado.NumReferenciaSiscard;
                    }
                    pago.IdEstado = pago.CodRefSiscard == null ? (int)EnumEstadoPago.MQNoAplico : (int)EnumEstadoPago.Aplicada;
                    _repPagosTransaccion.UpdateOn(w => w.IdPagoTrasaccion == pago.IdPagoTrasaccion,
                        up => new TF_PagoTrasaccion
                        {
                            IdEstado = pago.IdEstado,
                            CodRefSiscard = pago.CodRefSiscard
                        });

                    //Actualizo saldo disponible
                    //if (pago.IdEstado == (int)EnumEstadoPago.Aplicada && (pago.CodMovimientoMQ == codMovMQ["DTS"] || pago.CodMovimientoMQ == codMovMQ["DCS"]
                    //        || pago.CodMovimientoMQ == codMovMQ["DOC"]))
                    //{
                    //    var _transaccionSinpe = _repTransacionSinpe.SelectById(pago.IdTransaccion);
                    //    _repTransacionSinpe.UpdateOn(w => w.IdTransaccion == pago.IdTransaccion,
                    //        up => new TF_TransaccionSinpe
                    //        {
                    //            MontoNoDistribuido = (_transaccionSinpe.MontoNoDistribuido ?? _transaccionSinpe.MontoPago) - pago.MontoPago
                    //        });
                    //    _repTransacionSinpe.UnidadTbjo.Save();
                    //    _repTransacionSinpe.UnidadTbjo.Refresh();
                    //}
                }
                catch (Exception ex)
                {
                    Logger.Log(ex.FullStackTrace(), ex.Message);

                    pago.DetalleError = "El pago se procesara en el transcurso del día";
                    pago.IdEstado = (int)EnumEstadoPago.MQNoAplico;
                    _repPagosTransaccion.UpdateOn(w => w.IdPagoTrasaccion == pago.IdPagoTrasaccion,
                        up => new TF_PagoTrasaccion
                        {
                            IdEstado = pago.IdEstado,
                            CodRefSiscard = pago.CodRefSiscard
                        });

                    EnviarCorreoErrorPagoDistribuido(pagos);

                    break;
                }
            }
            return pagos;
        }

        private decimal ActualizarMontoDistribuido(int idTransaccion, decimal montoNoDistribuido, decimal montoPago)
        {
            var totalNoDistribuido = montoNoDistribuido - montoPago;

            if (totalNoDistribuido < 0)
            {
                throw new CoreException("El monto a distribuir excede el monto no distribuido", "Err_0116");
            }

            _repTransacionSinpe.UpdateOn(x =>
                x.IdTransaccion == idTransaccion, z => new TF_TransaccionSinpe
                {
                    MontoNoDistribuido = totalNoDistribuido
                });

            return totalNoDistribuido;
        }
        private void EstableceTrasaccionPendiente(PocTransaccionSinpe transSinpe)
        {
            //Defino la trasacción como pendiente de pago
            _repTransacionSinpe.UpdateOn(x => x.IdTransaccion == transSinpe.IdTransaccion,
                z => new TF_TransaccionSinpe
                {
                    IdEstadoTrans = (int)EnumEstadosTransaccion.PendientePago,
                    DetalleError = transSinpe.DetalleError,
                    IdUsuarioActualizaAud = Convert.ToInt32(_repParamSistema.SelectById("ID_USUARIO_BASE").ValParametro)
                });

            EnviarCorreoPagoPendiente(transSinpe);

        }
        private void EnviarCorreoPagoPendiente(PocTransaccionSinpe entrada)
        {
            if (Convert.ToInt32(_repParamSistema.SelectById("ENVIAR_CORREO_A_OPERACIONES").ValParametro) == (int)EnumIdEstado.Activo)
            {
                int numPlantilla = Convert.ToInt32(_repParamSistema.SelectById("PLANTILLA_PAGO_PENDIENTE").ValParametro);
                var notificacion = _repPlantillaNot.ObtenerPlantillaNot(numPlantilla);

                var asunto = notificacion.Asunto;
                var mensaje = notificacion.Cuerpo;

                string codMoneda = _repMoneda.SelectById(entrada.IdMonedaDestino).CodInternacional.ToString();

                mensaje = mensaje.Replace("@@CEDULAORIGEN", entrada.IdentOrigen);
                mensaje = mensaje.Replace("@@MONTO", entrada.MontoPago.ToString("0,0.00", CultureInfo.CurrentCulture));
                mensaje = mensaje.Replace("@@MONEDA", codMoneda);
                mensaje = mensaje.Replace("@@CCDESTINO", entrada.CCDestino);
                mensaje = mensaje.Replace("@@DETALLE", entrada.Detalle);

                var emailFROM = _fGenerales.ObtieneParametroCompania("REMITENTE_SINPE", int.Parse(_repParamSistema.SelectById("ID_ENTIDAD_BASE").ValParametro));
                var emailTO = _fGenerales.ObtieneParametroCompania("CORREO_PAGO_PENDIENTE", int.Parse(_repParamSistema.SelectById("ID_ENTIDAD_BASE").ValParametro));

                SenderConfig sender = new SenderConfig(emailFROM, null);
                EmailConfig emailData = new EmailConfig(asunto, mensaje);

                //Asignamos el e-mail a enviar
                emailData.AddTo(emailTO);

                //Enviar e-mail
                _repEmail.EnviarEmail(sender, emailData);
            }
        }
        private void EnviarNotificacionExitoso(PocTransaccionSinpe entrada, Dictionary<string, decimal> ListaPagos = null)
        {
            string codMoneda = _repMoneda.SelectById(entrada.IdMonedaDestino).CodInternacional.ToString();
            if (Convert.ToInt32(_repParamSistema.SelectById("ENVIAR_CORREO_PAGO_CLIENTE").ValParametro) == (int)EnumIdEstado.Activo)
            {
                #region e-mail
                var comunicacion = _repComunicacion.Table.ToList().Where(w => w.IdPersona == entrada.IdPersona &&
                                    w.IdTipo == (int)EnumTipoComunicacion.Correo &&
                                    w.IdEstado == (int)EnumIdEstado.Activo).FirstOrDefault();

                if (comunicacion != null && !string.IsNullOrEmpty(comunicacion.ValComunicacion))
                {
                    int numPlantilla = Convert.ToInt32(_repParamSistema.SelectById("PLANTILLA_CORREO_SINPE_PAGO").ValParametro);
                    var notificacion = _repPlantillaNot.ObtenerPlantillaNot(numPlantilla);

                    var asunto = notificacion.Asunto;
                    var mensaje = notificacion.Cuerpo;

                    string emailTO = comunicacion.ValComunicacion;
                    var emailFROM = _fGenerales.ObtieneParametroCompania("REMITENTE_SINPE", int.Parse(_repParamSistema.SelectById("ID_ENTIDAD_BASE").ValParametro));

                    mensaje = mensaje.Replace("@@NOMBRECOMPLETO", entrada.NombrePersona);
                    mensaje = mensaje.Replace("@@TARJETA", MascaraNumeroTarjeta(entrada.NumTarjeta));
                    mensaje = mensaje.Replace("@@MONTO", entrada.MontoPago.ToString("0,0.00", CultureInfo.CurrentCulture));
                    mensaje = mensaje.Replace("@@MONEDA", codMoneda);

                    if (ListaPagos != null && ListaPagos.Count > 0)
                    {
                        string table = string.Empty;
                        table = "<tbody>";
                        foreach (var pago in ListaPagos)
                        {
                            string label = string.Empty;
                            int idCurrenty = pago.Key == "PEX" ? entrada.IdMonedaDestino : pago.Key == "PUSD" ? (int)EnumMoneda.Dolares : (int)EnumMoneda.Colones;
                            string codCurenty = entrada.IdMonedaDestino == idCurrenty ? codMoneda : _repMoneda.SelectById(idCurrenty).CodInternacional.ToString();
                            if (entrada.Detalle.ToUpper().Contains("PC2"))
                            {
                                label = pago.Key == "PEX" ? "Pago Extra/Intra" : pago.Key == "PUSD" ? "Pago contado dólares" : "Pago contado colones";
                            }
                            if (entrada.Detalle.ToUpper().Contains("PM2"))
                            {
                                label = pago.Key == "PEX" ? "Pago Extra/Intra" : pago.Key == "PUSD" ? "Pago míninmo dólares" : "Pago mínimo colones";
                            }

                            string col1 = "<td>" + label + "</td>";
                            string col2 = "<td>" + pago.Value.ToString("0,0.00", CultureInfo.CurrentCulture) + "</td>";
                            string col3 = "<td>" + codCurenty + "</td>";
                            table += "<tr>" + col1 + col2 + col3 + "</tr>";
                        }
                        table += "</tbody>";

                        mensaje = mensaje.Replace("@@TABLAPAGOS", table);
                    }
                    else
                    {
                        mensaje = mensaje.Replace("<br/><h3>Pago se ha distribuido de la siguiente manera</h3>", "");
                        mensaje = mensaje.Replace("@@TABLAPAGOS", "");
                    }

                    SenderConfig sender = new SenderConfig(emailFROM, null);
                    EmailConfig emailData = new EmailConfig(asunto, mensaje);

                    //Asignamos el e-mail a enviar
                    emailData.AddTo(emailTO);

                    //Enviar e-mail
                    _repEmail.EnviarEmail(sender, emailData);
                }
                #endregion
            }
            if (Convert.ToInt32(_repParamSistema.SelectById("ENVIAR_SMS_PAGO_CLIENTE").ValParametro) == (int)EnumIdEstado.Activo)
            {
                #region SMS
                var comunicacion = _repComunicacion.Table.ToList().Where(w => w.IdPersona == entrada.IdPersona &&
                                        w.IdTipo == (int)EnumTipoComunicacion.Celular &&
                                        w.IdEstado == (int)EnumIdEstado.Activo).FirstOrDefault();

                if (comunicacion != null && !string.IsNullOrEmpty(comunicacion.ValComunicacion))
                {
                    int numPlantilla = Convert.ToInt32(_repParamSistema.SelectById("PLANTILLA_SMS_SINPE_PAGO").ValParametro);
                    var notificacion = _repPlantillaNot.ObtenerPlantillaNot(numPlantilla);

                    var mensaje = notificacion.Cuerpo;

                    mensaje = mensaje.Replace("@@TARJETA", MascaraNumeroTarjeta(entrada.NumTarjeta));

                    if (ListaPagos != null && ListaPagos.Count > 0)
                    {
                        string pagodistribuido = string.Empty;
                        if (entrada.Detalle.ToUpper().Contains("PC2"))
                        {
                            pagodistribuido = "PC";
                        }
                        if (entrada.Detalle.ToUpper().Contains("PM2"))
                        {
                            pagodistribuido = "PM";
                        }

                        foreach (var pago in ListaPagos)
                        {
                            int idCurrenty = pago.Key == "PEX" ? entrada.IdMonedaDestino : pago.Key == "PUSD" ? (int)EnumMoneda.Dolares : (int)EnumMoneda.Colones;
                            string codCurenty = entrada.IdMonedaDestino == idCurrenty ? codMoneda : _repMoneda.SelectById(idCurrenty).CodInternacional.ToString();
                            if (pago.Key == "PEX")
                            {
                                pagodistribuido += " FIN";
                            }
                            pagodistribuido += " " + string.Format("{0:0,0.00}", pago.Value) + " " + codCurenty;
                        }
                        mensaje = mensaje.Replace("@@INFOADICIONAL", pagodistribuido);
                    }
                    else
                    {
                        mensaje = mensaje.Replace("@@INFOADICIONAL", "por un monto de " + entrada.MontoPago.ToString("0,0.00", CultureInfo.InvariantCulture) + " " + codMoneda + " ha sido recibido");
                    }

                    var SMS = new AC_MENSAJES_SMS
                    {
                        NUM_TELEFONO = Convert.ToInt32(comunicacion.ValComunicacion.Replace("-", "")),
                        TEXTO = mensaje.Length > 160 ? mensaje.Substring(0, 160) : mensaje,
                        FECHA_HORA_REGISTRO = DateTime.Now,
                        IND_ESTADO = (int)EnumIdEstado.Activo
                    };

                    _repMensajeSMS.Insert(SMS);
                    _repMensajeSMS.UnidadTbjo.Save();
                }
                #endregion
            }
        }

        private void EnviarNotificacionError(string mensaje)
        {
            var emailTO = _repParamSistema.SelectById("NOTIFICACIERRESA").ValParametro;
            var emailFROM = _fGenerales.ObtieneParametroCompania("REMITENTE_SINPE", int.Parse(_repParamSistema.SelectById("ID_ENTIDAD_BASE").ValParametro));

            SenderConfig sender = new SenderConfig(emailFROM, null);
            EmailConfig emailData = new EmailConfig("SINPE - Transacción Fallida", mensaje);

            //Asignamos el e-mail a enviar
            emailData.AddTo(emailTO);

            //Enviar e-mail
            _repEmail.EnviarEmail(sender, emailData);
        }
        private void EnviarCorreoErrorPagoDistribuido(List<PocPagoTransaccion> pagos)
        {
            //Enviar Correo
            int numPlantilla = Convert.ToInt32(_repParamSistema.SelectById("PLANTILLA_ERROR_PAGO_DIRIGIDO").ValParametro);
            var notificacion = _repPlantillaNot.ObtenerPlantillaNot(numPlantilla);

            var asunto = notificacion.Asunto;
            var mensaje = notificacion.Cuerpo;

            var _transaccionSinpe = _repTransacionSinpe.SelectById(pagos[0].IdTransaccion);
            var montoND = _transaccionSinpe.MontoNoDistribuido ?? _transaccionSinpe.MontoPago;
            string codMoneda = _repMoneda.SelectById(_transaccionSinpe.IdMonedaDestino).CodInternacional.ToString();
            var f1Sinpe = string.Format("<td>Id</td><td>Monto no distribuido</td><td>Moneda</td><td>IdCuenta</td><td>#Tarjeta</td><td>Fecha</td>");
            var f2Sinpe = string.Format("<td>{0}</td><td align='right'>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td>",
                _transaccionSinpe.IdTransaccion, montoND.ToString("0,0.00", CultureInfo.CurrentCulture), codMoneda, pagos[0].Producto,
                pagos[0].NumTarjeta, _transaccionSinpe.FecInclusionAud);

            var f1Mov = string.Format("<tr style='border: #000 1px solid;'><td>Id</td><td>Código MQ</td><td>Monto</td><td>Moneda</td><td>IdCuenta</td><td>#Tarjeta/#Extra</td><td>Estado</td></tr>");
            var f2Mov = string.Empty;
            foreach (PocPagoTransaccion pago in pagos)
            {
                codMoneda = _repMoneda.SelectById(pago.IdMoneda).CodInternacional.ToString();
                var estadoDes = _repCatalogo.SelectById(pago.IdEstado).Descripcion;
                f2Mov += string.Format("<tr style='border: #000 1px solid;'><td>{0}</td><td>{1}</td><td align='right'>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>",
                    pago.IdPagoTrasaccion, pago.CodMovimientoMQ, pago.MontoPago, codMoneda, pago.Producto, pago.NumTarjeta, estadoDes);
            }
            string tablaSinpe = String.Format("<Table style='width: 700px; border: #000 1px solid; border-collapse: collapse;' ><tr style='border: #000 1px solid;'>{0}</tr><tr style='border: #000 1px solid;'>{1}</tr></Table>", f1Sinpe, f2Sinpe);
            string tablaMov = String.Format("<Table style='width: 700px; border: #000 1px solid; border-collapse: collapse;'>{0}{1}</Table>", f1Mov, f2Mov);

            mensaje = mensaje.Replace("@@TABLASINPE", tablaSinpe);
            mensaje = mensaje.Replace("@@TABLEMOVIMIENTOS", tablaMov);

            var emailFROM = _fGenerales.ObtieneParametroCompania("REMITENTE_SINPE", int.Parse(_repParamSistema.SelectById("ID_ENTIDAD_BASE").ValParametro));
            var emailTO = _fGenerales.ObtieneParametroCompania("CORREO_PAGO_PENDIENTE", int.Parse(_repParamSistema.SelectById("ID_ENTIDAD_BASE").ValParametro));

            SenderConfig sender = new SenderConfig(emailFROM, null);
            EmailConfig emailData = new EmailConfig(asunto, mensaje);

            //Asignamos el e-mail a enviar
            emailData.AddTo(emailTO);

            //Enviar e-mail
            _repEmail.EnviarEmail(sender, emailData);
        }

        private string MascaraNumeroTarjeta(string numTarjeta)
        {
            return Regex.Replace(numTarjeta, @"(\d{6})(\d{7})(\d{3})", "$1XXXXXXX$3");
        }
        //Aplica para cedulas de tipo
        // -Persona Fisica Nacional [0P-TTTT-AAAA]
        // -Gobierno Central [2-PPP-CCCCCC]
        // -Persona Juridica [3-TTT-CCCCCC]
        // -Institución Autónoma [4-000-CCCCCC]
        private string ObtenerIDParametroDetalle(string detalle)
        {
            detalle = detalle.ToUpper();
            if (detalle.Contains("ID"))
            {
                var hilera = detalle.Substring(detalle.IndexOf("ID")).Trim();
                string ID = string.Empty;

                while (!string.IsNullOrEmpty(hilera))
                {
                    hilera = hilera.Replace("ID ", "ID");
                    var arrPalabras = hilera.Split(' ');
                    ID = arrPalabras[0].Replace("ID", "").Replace("-", "").Replace(":", "").Replace("=", "").Replace(",", "");
                    if (ID.Length > 9)
                    {
                        ID = ID.Substring(0, 9);
                    }

                    long number1 = 0;
                    bool canConvert = long.TryParse(ID, out number1);
                    if (canConvert && ID.Length == 9)
                    {
                        return ID;
                    }

                    detalle = hilera.Replace(arrPalabras[0], "");
                    if (detalle.Contains("ID"))
                    {
                        hilera = detalle.Substring(detalle.IndexOf("ID"));
                    }
                    else
                    {
                        hilera = string.Empty;
                    }
                }
            }
            return null;
        }

        private int ObtenerTJParametroDetalle(string detalle, int idPersona)
        {
            detalle = detalle.ToUpper();
            if (detalle.Contains("TJ"))
            {
                var hilera = detalle.Substring(detalle.IndexOf("TJ")).Trim();
                string TJ = string.Empty;
                long number1 = 0;

                while (!string.IsNullOrEmpty(hilera))
                {
                    hilera = hilera.Replace("TJ ", "TJ");
                    var arrPalabras = hilera.Split(' ');
                    TJ = arrPalabras[0].Replace("TJ", "");
                    if (TJ.Length > 4)
                    {
                        TJ = TJ.Substring(0, 4);
                    }

                    bool canConvert = long.TryParse(TJ, out number1);
                    if (canConvert && TJ.Length == 4)
                    {
                        var tarjetas = _repVwTarjetasCuenta.Table.Where(w => (w.IdPersona == idPersona || w.IdPersonaTitular == idPersona) && w.NumTarjeta.Substring(w.NumTarjeta.Length - 4) == TJ);
                        if (tarjetas.Count() == 1)
                        {
                            return tarjetas.FirstOrDefault().IdTarjeta;
                        }
                    }

                    detalle = hilera.Replace(arrPalabras[0], "");
                    if (detalle.Contains("TJ"))
                    {
                        hilera = detalle.Substring(detalle.IndexOf("TJ"));
                    }
                    else
                    {
                        hilera = string.Empty;
                    }
                }
            }
            return 0;
        }
        private int ObtenerFINParametroDetalle(string detalle, int idCuenta, int idMoneda)
        {
            detalle = detalle.ToUpper();
            if (detalle.Contains("FIN"))
            {
                var hilera = detalle.Substring(detalle.IndexOf("FIN")).Trim();
                string FIN = string.Empty;
                long number1 = 0;
                bool canConvert = false;

                while (!string.IsNullOrEmpty(hilera))
                {
                    hilera = hilera.Replace("FIN ", "FIN");
                    var arrPalabras = hilera.Split(' ');
                    FIN = arrPalabras[0].Replace("FIN", "");

                    if (FIN.Substring(0, 1) == "X")
                    {
                        var extras = _repExtraFinanc.Table.ToList().Where(w => w.IdCuenta == idCuenta && w.IdMoneda == idMoneda);
                        if (extras.Count() == 1)
                        {
                            return extras.FirstOrDefault().IdExtraFinanciamiento;
                        }
                        return -101; //No existe un unico Extra-financiamiento en la moneda de la transacción
                    }
                    else if (FIN.Length > 9)
                    {
                        FIN = FIN.Substring(0, 9);
                    }

                    canConvert = long.TryParse(FIN, out number1);
                    if (canConvert && FIN.Length <= 9)
                    {
                        var extras = _repExtraFinanc.Table.ToList().Where(w => w.IdCuenta == idCuenta && w.IdMoneda == idMoneda && w.NumExtraFinanciamiento == number1);
                        if (extras.Count() == 1)
                        {
                            return extras.FirstOrDefault().IdExtraFinanciamiento;
                        }
                    }

                    detalle = hilera.Replace(arrPalabras[0], "");
                    if (detalle.Contains("FIN"))
                    {
                        hilera = detalle.Substring(detalle.IndexOf("FIN"));
                    }
                    else
                    {
                        hilera = string.Empty;
                    }
                }
            }
            return 0;
        }

        private IQueryable<PocSinpeDistribuido> ObtenerSINPEPorDistribuir(int IdCuenta)
        {
            //var pagoMinimos = ((from ec in _repEncabezadoEC.Table
            //                    where ec.IdCuenta == IdCuenta
            //                    select new { ec.IdCuenta, ec.PagoMinLocal, ec.PagoMinInter, ec.FecCorte })
            //                    .Union(from ech in _repEncabezadoECHist.Table
            //                           where ech.IdCuenta == IdCuenta
            //                           select new { ech.IdCuenta, ech.PagoMinLocal, ech.PagoMinInter, ech.FecCorte })
            //                    ).OrderByDescending(o => o.FecCorte);

            var pagosSinpe =
               from pt in _repPagosTransaccion.Table
               join ts in _repTransacionSinpe.Table
                   on new
                   {
                       pt.IdTransaccion,
                       IdEstadoTrans = (int)EnumEstadosTransaccion.Aplicada,
                       IdEsDistribuible = (int?)EnumIdSiNo.Si,
                       pt.IdTipoProducto,
                       pt.IdEstado,
                       pt.Producto
                   }
                   equals new
                   {
                       ts.IdTransaccion,
                       ts.IdEstadoTrans,
                       ts.IdEsDistribuible,
                       IdTipoProducto = (int)EnumTipoProducto.TarjetaCredito,
                       IdEstado = (int)EnumEstadoPago.Aplicada,
                       Producto = IdCuenta
                   }
               join c in _repCuenta.Table on pt.Producto equals c.IdCuenta
               join m in _repMoneda.Table on pt.IdMoneda equals m.IdMoneda
               //join ec in pagoMinimos.Take(1) on pt.Producto equals ec.IdCuenta into etc
               //from ec in etc.DefaultIfEmpty()
               join t in _repTarjeta.Table on new { pt.Producto, pt.SubProducto } equals new { Producto = t.IdCuenta, SubProducto = (int?)t.IdTarjeta } into tar
               from t in tar.DefaultIfEmpty()
               where (pt.CodMovimientoMQ == null || pt.CodMovimientoMQ == "") &&
                      (pt.FecInclusionAud >= (c.FecUltCorte ?? c.FecInclusionAud)) &&
                      (ts.MontoNoDistribuido != 0) &&
                   //ts.FecInclusionAud < DbFunctions.AddMonths((c.FecUltCorte ?? c.FecInclusionAud), 1)) &&
                      (DateTime.Now >= (c.FecUltCorte ?? c.FecInclusionAud)) //&& lastDatemonth < (c.FecUltCorte ?? c.FecInclusionAud)
               orderby pt.FecInclusionAud
               select new PocSinpeDistribuido
               {
                   IdCuenta = pt.Producto,
                   IdTarjeta = pt.SubProducto,
                   IdMoneda = pt.IdMoneda,
                   Monto = ts.MontoNoDistribuido ?? pt.MontoPago,
                   //PagoMinimo = (pt.IdMoneda == (int)EnumMoneda.Colones ? (ec.PagoMinLocal ?? c.PagoMinLocal) : (ec.PagoMinInter ?? c.PagoMinInter)) ?? 0,
                   PagoMinimo = (pt.IdMoneda == (int)EnumMoneda.Colones ? c.PagoMinLocal : c.PagoMinInter) ?? 0,
                   IdTransaccion = ts.IdTransaccion,
                   Fecha = ts.FecValor,
                   NumTarjeta = t.NumTarjeta,
                   CodMoneda = m.CodInternacional
               };

            return pagosSinpe;
        }
        private int CargarPagoATransaccion(List<int> arrTransaccion, decimal montoPago)
        {
            var pago = montoPago;

            //Actualizo saldo disponible
            foreach (int idTrans in arrTransaccion)
            {
                var _transaccionSinpe = _repTransacionSinpe.SelectById(idTrans);
                var _disponible = _transaccionSinpe.MontoNoDistribuido ?? _transaccionSinpe.MontoPago;

                if (_disponible > 0)
                {
                    if (_disponible >= pago)
                    {
                        _repTransacionSinpe.UpdateOn(w => w.IdTransaccion == idTrans,
                            up => new TF_TransaccionSinpe
                            {
                                MontoNoDistribuido = _disponible - pago
                            });
                        _repTransacionSinpe.UnidadTbjo.Save();
                        _repTransacionSinpe.UnidadTbjo.Refresh();

                        return idTrans;
                    }
                    else
                    {
                        pago = pago - _disponible;
                        _repTransacionSinpe.UpdateOn(w => w.IdTransaccion == idTrans,
                            up => new TF_TransaccionSinpe
                            {
                                MontoNoDistribuido = 0
                            });
                        _repTransacionSinpe.UnidadTbjo.Save();
                        _repTransacionSinpe.UnidadTbjo.Refresh();
                    }
                }
            }
            return -1;
        }
        #endregion
    }
}

﻿using Monibyte.Arquitectura.Dominio.Nucleo;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Seguridad.Repositorios
{
    public interface IFuncionesSeguridad : IRepositorioBase
    {
        IEnumerable<Cplx_SC_MenusRole> ObtenerMenuUsuario();
        IEnumerable<Cplx_SC_Accion> ObtenerPermisosUsuario();
    }
}

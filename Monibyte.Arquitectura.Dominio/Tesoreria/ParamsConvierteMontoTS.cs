﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Dominio.Tesoreria
{
    public class ParamsConvierteMontoTS : EntidadBase
    {
        public int? idMonedaOrigen { get; set; }
        public decimal? monto { get; set; }
        public int? idMonedaDestino { get; set; }
        public decimal? cambioOrigen { get; set; }
        public decimal? cambioDestino { get; set; }
        public int? idEntidadFinanciera { get; set; }
    }

    public class ParamsConvierteMontoHistoricoTS : ParamsConvierteMontoTS
    {
        public DateTime fecReferencia { get; set; }
        private int tipo = (int)EnumIdTipoDeCambio.Compra;
        public int idTipoCambio
        {
            get
            {
                return tipo;
            }
            set
            {
                tipo = value;
            }
        }
        //Enviar  (int)EnumIdTipoDeCambio.Compra
    }
}

﻿using Monibyte.Arquitectura.Datos.Nucleo;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public interface IUnidadTbjoMonibyte : IUnidadTbjoOperaciones { }

    public class UnidadTbjoMonibyte : UnidadTbjoBase, IUnidadTbjoMonibyte
    {
        public UnidadTbjoMonibyte(DbContextMonibyte ctx) : base(ctx) { }
    }
}

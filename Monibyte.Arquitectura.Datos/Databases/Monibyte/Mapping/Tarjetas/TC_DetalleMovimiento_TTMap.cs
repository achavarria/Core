using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_DetalleMovimiento_TTMap : EntityTypeConfiguration<TC_DetalleMovimiento_TT>
    {
        public TC_DetalleMovimiento_TTMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdReferencia, t.IdTarjeta });

            // Properties
            this.Property(t => t.IdReferencia)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.IdTarjeta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TC_DetalleMovimiento_TT", "TCredito");
            this.Property(t => t.IdReferencia).HasColumnName("IdReferencia");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.CamposDinamicos).HasColumnName("CamposDinamicos");
        }
    }
}

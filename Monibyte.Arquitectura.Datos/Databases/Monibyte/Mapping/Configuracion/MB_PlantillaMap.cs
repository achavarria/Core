using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_PlantillaMap : EntityTypeConfiguration<MB_Plantilla>
    {
        public MB_PlantillaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPlantilla);

            // Properties
            this.Property(t => t.IdPlantilla)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("MB_Plantilla", "Mnb");
            this.Property(t => t.IdPlantilla).HasColumnName("IdPlantilla");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.SinRestriccion).HasColumnName("SinRestriccion");
            this.Property(t => t.EsVip).HasColumnName("EsVip");
            this.Property(t => t.MonLimite).HasColumnName("MonLimite");
            this.Property(t => t.ConsumeColones).HasColumnName("ConsumeColones");
            this.Property(t => t.ConsumeDolares).HasColumnName("ConsumeDolares");
            this.Property(t => t.ConsumeLocal).HasColumnName("ConsumeLocal");
            this.Property(t => t.ConsumeInternacional).HasColumnName("ConsumeInternacional");
            this.Property(t => t.ConsumeInternet).HasColumnName("ConsumeInternet");
            this.Property(t => t.PermiteAvanceEfectivo).HasColumnName("PermiteAvanceEfectivo");
            this.Property(t => t.RestringeMcc).HasColumnName("RestringeMcc");
            this.Property(t => t.RestringeHorario).HasColumnName("RestringeHorario");
            this.Property(t => t.RestringeRegion).HasColumnName("RestringeRegion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
        }
    }
}

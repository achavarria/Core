﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Configuracion;
using Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Configuracion;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public class OperacionesTarjeta : AplicacionBase, IOperacionesTarjeta
    {
        private IOperacionesATH _appOperacionesATH;
        private IRepositorioCuenta _repCuenta;
        private IRepositorioTarjeta _repTarjeta;
        private IRepositorioCatalogo _repCatalogo;
        private IConsultasTarjeta _consultasTarjeta;
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IFuncionesTesoreria _repFuncionesTesoreria;
        private IRepositorioMnb<CL_Direccion> _repDireccion;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;
        private IRepositorioMnb<TC_vwTarjetasCuenta> _repVwTarjetasCuenta;
        private IRepositorioMnb<GL_ParametroCompania> _repParamCompania;
        private IAppConfiguracionCtrl _appConfiguracionCtrl;

        public OperacionesTarjeta(
            IOperacionesATH appOperacionesATH,
            IRepositorioCuenta repCuenta,
            IRepositorioTarjeta repTarjeta,
            IRepositorioCatalogo repCatalogo,
            IConsultasTarjeta consultasTarjeta,
            IRepositorioMnb<GL_Entidad> repEntidad,
            IFuncionesTesoreria repFuncionesTesoreria,
            IRepositorioMnb<CL_Direccion> repDireccion,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema,
            IRepositorioMnb<TC_vwTarjetasCuenta> repVwTarjetasCuenta,
            IRepositorioMnb<GL_ParametroCompania> repParamCompania,
            IAppConfiguracionCtrl appConfiguracionCtrl)
        {
            _appOperacionesATH = appOperacionesATH;
            _repCuenta = repCuenta;
            _repTarjeta = repTarjeta;
            _repCatalogo = repCatalogo;
            _consultasTarjeta = consultasTarjeta;
            _repEntidad = repEntidad;
            _repFuncionesTesoreria = repFuncionesTesoreria;
            _repDireccion = repDireccion;
            _repParamSistema = repParamSistema;
            _repVwTarjetasCuenta = repVwTarjetasCuenta;
            _repParamCompania = repParamCompania;
            _appConfiguracionCtrl = appConfiguracionCtrl;
        }

        public void ActualizarDetalleTarjeta(PocEntradaConsultaTarjeta pocEntrada)
        {
            var resultadoConsulta = _appOperacionesATH.ConsultaTarjeta(pocEntrada);
            var entrada = resultadoConsulta.ProyectarComo<TC_Cuenta>();
            entrada.IdCuenta = pocEntrada.IdCuenta;
            _repCuenta.Update(entrada);
        }

        public void ActivarTarjeta(PocEntradaActivaTarjeta pocEntrada)
        {
            var resultadoOper = _appOperacionesATH.ActivaTarjeta(pocEntrada);
            var entrada = resultadoOper.ProyectarComo<TC_Cuenta>();
            entrada.IdCuenta = pocEntrada.IdCuenta;
            _repCuenta.Update(entrada);
        }

        public void ActualizarEstadoTarjeta(PocEntradaActualizaTarjeta pocEntrada)
        {
            var resultadoOper = _appOperacionesATH.ActualizaEstadoTarjeta(pocEntrada);
            var entrada = resultadoOper.ProyectarComo<TC_Cuenta>();
            entrada.IdCuenta = pocEntrada.IdCuenta;
            _repCuenta.Update(entrada);
        }

        public void ReponerPinTarjeta(PocEntradaReposicionPinTarjeta pocEntrada)
        {
            var resultadoOper = _appOperacionesATH.ReposicionPinTarjeta(pocEntrada);
            var entrada = resultadoOper.ProyectarComo<TC_Cuenta>();
            entrada.IdCuenta = pocEntrada.IdCuenta;
            _repCuenta.Update(entrada);
        }

        public PocModificaFecCorteTarjeta ObtieneFechaCorte(int idCuenta)
        {
            var cuentaActual = _repCuenta.SelectById(idCuenta);
            var diaCorte = ObtenerDiaCorte(cuentaActual.IdDiaCorte.Value);
            var diaProxCorte = (cuentaActual.IdDiaCorte == (int)EnumIdDiaCorte.Corte30 &&
                DateTime.Now.Month == 2) ? DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) : diaCorte;
            var fechaProxCorte = new DateTime(DateTime.Today.Year,
                DateTime.Today.Month, diaProxCorte);
            fechaProxCorte = diaCorte > DateTime.Now.Day ? fechaProxCorte :
                fechaProxCorte.AddMonths(1);
            var horasFechaCorteStr = _repParamSistema
                .SelectById("HORAS_CAMBIO_FECHA_CORTE").ValParametro;
            var horasFechaCorte = Int32.Parse(horasFechaCorteStr);
            var fecPagoMinimoEval = cuentaActual.FecPagoMinimo.HasValue ?
                cuentaActual.FecPagoMinimo.Value : DateTime.Now;
            var resultado = new PocModificaFecCorteTarjeta
            {
                IdCuenta = cuentaActual.IdCuenta,
                NumCuenta = cuentaActual.NumCuenta,
                DiaCorte = diaCorte,
                IdDiaCorte = (int)cuentaActual.IdDiaCorte,
                FecCorte = fechaProxCorte,
                FecPagoMinimo = cuentaActual.FecPagoMinimo,
                HorasFechaCorte = horasFechaCorte,
                HabilitadoPagoMinimo = true,
                HabilitadoPeriodo = (DateTime.Now.Date > fecPagoMinimoEval.Date || cuentaActual.FecUltCorte == null) &&
                    DateTime.Now < fechaProxCorte.AddHours(-horasFechaCorte)
            };
            if (resultado.HabilitadoPeriodo)
            {
                if (cuentaActual.PagoMinLocal > 0 || cuentaActual.PagoMinInter > 0)
                {
                    var detalleCuenta = _consultasTarjeta.
                        ConsultarDetalleTarjetaMQ(cuentaActual.IdCuenta);
                    if (detalleCuenta.PagoMinLocal > 0 || detalleCuenta.PagoMinInter > 0)
                    {
                        resultado.HabilitadoPagoMinimo = false;
                    }
                }
            }
            return resultado;
        }

        public void AplicarFechaCorte(int idCuenta, int idDiaCorte)
        {
            /****************APLICAR EN SISCARD*************************/
            _repCuenta.UpdateOn(x => x.IdCuenta == idCuenta, z => new TC_Cuenta
            {
                IdDiaCorte = idDiaCorte
            });
            /****************APLICAR EN SISCARD*************************/
        }

        public void AplicarCambioLimiteCuenta(int idCuenta, decimal montoInter)
        {
            var validation = new ValidationResponse();
            var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
            var factor = InfoSesion.ObtenerSesion<int>("FACTORCONVERSION");
            var entidad = _repEntidad.SelectById(idEntidad);
            var montoLocal = _repFuncionesTesoreria.ConvierteMonto(
                entidad.IdMonedaInternacional,
                montoInter, entidad.IdMonedaLocal);
            var cuenta = _repCuenta.SelectById(idCuenta);
            var update = new TC_Cuenta { IdCuenta = idCuenta };
            if (cuenta.IdEsCuentaMadre == (int)EnumIdSiNo.Si)
            {
                var permitido = LimiteGlobalPermitido(idCuenta, montoInter);
                if (permitido)
                {
                    var diferencia = montoInter - cuenta.LimiteGlobal.Value;
                    var resultado = diferencia + cuenta.LimiteCreditoInter;
                    update.LimiteCreditoInter = resultado >= 0 ? resultado :
                        cuenta.LimiteCreditoInter;
                }
                else
                {
                    validation.AddError("Err_0003");
                }
            }
            else
            {
                update.LimiteCreditoInter =
                    cuenta.LimiteCreditoInter == cuenta.LimiteGlobal ||
                    cuenta.LimiteCreditoInter > montoInter ?
                    montoInter : cuenta.LimiteCreditoInter;
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Error de validacion cuenta", validation: validation);
            }
            update.LimiteCreditoLocal = montoLocal;
            update.LimiteGlobal = montoInter;
            using (var ts = new TransactionScope())
            {
                _repCuenta.Update(update,
                    up => up.LimiteCreditoInter,
                    up => up.LimiteCreditoLocal,
                    up => up.LimiteGlobal);
                _repCuenta.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void AplicarCambioLimiteTarjeta(string numTarjeta, decimal nuevoLimite, int idTipoGestion, DateTime? VigenteHasta = null)
        {
            using (var ts = new TransactionScope())
            {
                var tc = _repVwTarjetasCuenta.Table.FirstOrDefault(tar => tar.NumTarjeta == numTarjeta);
                decimal? limiteGlobal = null;
                if (tc == null)
                {
                    throw new CoreException("Error al cambiar el limite de la tarjeta", "Err_0002");
                }

                if (idTipoGestion == (int)EnumTipoGestion.ModificaLimiteCuenta)
                {
                    limiteGlobal = nuevoLimite;
                    if (nuevoLimite < tc.LimiteCreditoCtaGlobal)
                    {
                        ValidarCambioLimiteTarjeta(tc.NumTarjeta, nuevoLimite, idTipoGestion, VigenteHasta);
                    }
                }
                else if (idTipoGestion == (int)EnumTipoGestion.ModificaLimiteEfectivoTarjeta)
                {
                    limiteGlobal = tc.LimiteCreditoCtaGlobal;
                    ValidarCambioLimiteTarjeta(tc.NumTarjeta, nuevoLimite, idTipoGestion, VigenteHasta);
                }
                var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
                var factor = InfoSesion.ObtenerSesion<int>("FACTORCONVERSION");
                var entidad = _repEntidad.SelectById(idEntidad);
                var idMonedaLocal = entidad.IdMonedaLocal.Value;
                var idMonedaInter = entidad.IdMonedaInternacional.Value;
                var montoLocal = _repFuncionesTesoreria.ConvierteMonto(
                    idMonedaInter, nuevoLimite, idMonedaLocal);
                var cambioLimite = new PocEntradaActualizaLimiteTarjeta
                {
                    LimiteCredito = nuevoLimite,
                    IdCuenta = tc.IdCuenta,
                    NumCuenta = tc.NumCuenta,
                    NumTarjeta = tc.NumTarjeta
                };
                if (tc.IdRelacion == (int)EnumRelacionTarjeta.Titular)
                {
                    _repCuenta.UpdateOn(m => m.IdCuenta == tc.IdCuenta,
                        up => new TC_Cuenta
                        {
                            LimiteCreditoLocal = montoLocal,
                            LimiteCreditoInter = nuevoLimite,
                            LimiteGlobal = limiteGlobal
                        });
                    //Actualiza la tarjeta titular por MQ
                    _appOperacionesATH.ActualizaLimiteCuenta(cambioLimite);

                    //Actualizando la tabla de Tarjeta
                    _repTarjeta.UpdateOn(m => m.IdCuenta == tc.IdCuenta
                                           && m.LimiteCreditoInter == tc.LimiteCreditoTarjetaInter,
                        up => new TC_Tarjeta
                        {
                            LimiteCreditoLocal = montoLocal,
                            LimiteCreditoInter = nuevoLimite,
                            LimiteCreditoAjustado = nuevoLimite
                        });
                }
                else
                {
                    //Actualiza la tarjeta adicional por MQ
                    _appOperacionesATH.ActualizaLimiteTarjeta(cambioLimite);

                    //Actualizando la tabla de Tarjeta
                    _repTarjeta.UpdateOn(m => m.IdTarjeta == tc.IdTarjeta,
                        up => new TC_Tarjeta
                        {
                            LimiteCreditoLocal = montoLocal,
                            LimiteCreditoInter = nuevoLimite,
                            LimiteCreditoAjustado = nuevoLimite
                        });
                }
                _repTarjeta.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void ValidarCambioLimiteTarjeta(string numTarjeta, decimal limiteNuevo, int idTipoGestion, DateTime? VigenteHasta = null)
        {
            var validation = new ValidationResponse();

            var _limites = _consultasTarjeta.ObtenerLimitesTarjeta(numTarjeta);

            if (limiteNuevo > _limites.LimiteCuenta && idTipoGestion == (int)EnumTipoGestion.ModificaLimiteEfectivoTarjeta)
            {
                validation.AddError("Err_0004");
            }
            if (limiteNuevo < _limites.LimiteMinimo)
            {
                validation.AddError("Err_0003");
            }
            if (limiteNuevo < _limites.LimiteMinimoTarjeta)
            {
                validation.AddError("Err_0088");
            }

            if (validation.IsValid)
            {
                if (_limites.LimiteGrupo.HasValue && limiteNuevo > _limites.LimiteGrupo)
                {
                    validation.AddError("Err_0059");
                }
                if (_limites.LimiteTarjeta != _limites.LimiteCuenta && limiteNuevo < _limites.SaldoTotalTc)
                {
                    validation.AddError("Err_0005");
                }
            }

            if (VigenteHasta.HasValue && VigenteHasta < DateTime.Today)
            {
                validation.AddError("Err_0112");
            }

            if (_limites.LimiteTarjeta == limiteNuevo)
            {
                validation.AddError("Err_0113");
            }

            if (!validation.IsValid)
            {
                throw new CoreException("Error validando el límite de crédito", validation: validation);
            }
        }

        public void AplicarBloqueo(int idTarjeta, int idTipoBloqueo, int idCuenta)
        {
            TC_Tarjeta entrada = new TC_Tarjeta();
            DateTime? fechaInactivacion = null;
            if (idTipoBloqueo != (int)EnumEstadoTarjeta.BloqueoTemporal)
                fechaInactivacion = System.DateTime.Now;
            _repTarjeta.UpdateOn(m => m.IdTarjeta == idTarjeta, p => new TC_Tarjeta
            {
                IdEstadoTarjeta = idTipoBloqueo,
                FecInactivacion = fechaInactivacion
            });
            GL_Catalogo lista = new GL_Catalogo();
            PocEntradaActualizaTarjeta entradabloqueo = new PocEntradaActualizaTarjeta();
            entradabloqueo.EstadoDeseado = _repCatalogo.SelectById(idTipoBloqueo).Referencia1;
            entradabloqueo.NumTarjeta = _repTarjeta.Table.FirstOrDefault(x => x.IdTarjeta == idTarjeta).NumTarjeta;
            entradabloqueo.IdCuenta = idCuenta;
            entradabloqueo.MotivoCancelacion = "";
            /***** ATH******/
            _appOperacionesATH.ActualizaEstadoTarjeta(entradabloqueo);
            /***** ATH******/
            _repTarjeta.UnidadTbjo.Save();
        }

        public void AplicarReactivarTarjeta(int idTarjeta, int idEstado, string motivoCancelacion)
        {
            var entradaEstado = new PocEntradaActualizaTarjeta();
            _repTarjeta.UpdateOn(m => m.IdTarjeta == idTarjeta, p => new TC_Tarjeta
            {
                IdEstadoTarjeta = idEstado
            });
            entradaEstado.MotivoCancelacion = motivoCancelacion != null ? motivoCancelacion : string.Empty;
            entradaEstado.NumTarjeta = _repTarjeta.Table.FirstOrDefault(x => x.IdTarjeta == idTarjeta).NumTarjeta;
            entradaEstado.EstadoDeseado = _repCatalogo.SelectById(idEstado).Referencia1;
            /***** ATH******/
            _appOperacionesATH.ActualizaEstadoTarjeta(entradaEstado);
            /***** ATH******/
            _repTarjeta.UnidadTbjo.Save();
        }

        public void AplicarActivarTarjeta(int idTarjeta, int idEstado)
        {
            _repTarjeta.UpdateOn(m => m.IdTarjeta == idTarjeta, p => new TC_Tarjeta
            {
                IdEstadoTarjeta = idEstado
            });
        }

        public string ObtenerDireccion(int idTarjeta)
        {
            var resultado = _repTarjeta.Table.Join(_repDireccion.Table,
                    item => item.IdPersona,
                    dir => dir.IdPersona,
                    (item, dir) => new { item, dir })
                .FirstOrDefault(x => x.dir.IdTipo == (int)EnumUbicacion.Casa &&
                    x.item.IdTarjeta == idTarjeta);
            return resultado != null ? resultado.dir.Direccion : string.Empty;
        }

        public int ObtenerDiaCorte(int diaCorte)
        {
            var criteria = new Criteria<GL_Catalogo>();
            criteria.And(c => c.Lista == "LDIASCORTE");
            criteria.And(c => c.IdCatalogo == diaCorte);
            var resultado = _repCatalogo.List(criteria).FirstOrDefault();
            return resultado != null ? Convert.ToInt32(resultado.Referencia1) : 0;
        }

        public TC_Cuenta ObtieneInfoCuenta(int idCuenta)
        {
            return _repCuenta.Table.FirstOrDefault(x => x.IdCuenta == idCuenta);
        }

        public void GuardarDetalleTarjeta(PocTarjetaCuentaUsuario tarjeta)
        {
            using (var ts = new TransactionScope())
            {
                _repTarjeta.Update(new TC_Tarjeta
                {
                    IdCuenta = tarjeta.IdCuenta,
                    IdTarjeta = tarjeta.IdTarjeta,
                    CamposDinamicos = tarjeta.DynamicJson
                }, up => up.CamposDinamicos);
                _repTarjeta.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void ActualizarCorreosTarjeta(List<PocTarjetaCuentaUsuario> datosEntrada,
            List<PocTarjetaCuentaUsuario> modeloPrevio)
        {
            modeloPrevio = modeloPrevio ?? new List<PocTarjetaCuentaUsuario>();
            using (var ts = new TransactionScope())
            {
                foreach (var item in datosEntrada)
                {
                    //busca un item similar en la configuracion previa
                    var itemPrevio = modeloPrevio.FirstOrDefault(m => m.IdTarjeta == item.IdTarjeta &&
                                    m.CorreoEstCuenta == item.CorreoEstCuenta &&
                                    m.CopiaCorreoEstCuenta == item.CopiaCorreoEstCuenta);
                    if (itemPrevio == null)
                    {
                        _repTarjeta.UpdateOn(m => m.IdTarjeta == item.IdTarjeta,
                            z => new TC_Tarjeta
                            {
                                CorreoEstCuenta = item.CorreoEstCuenta,
                                CopiaCorreoEstCuenta = item.CopiaCorreoEstCuenta
                            });
                    }
                }
                _repTarjeta.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void GuardarCopiasDeCorreo(int[] tarjetas, string texto)
        {
            using (var ts = new TransactionScope())
            {
                foreach (var item in tarjetas)
                {
                    _repTarjeta.UpdateOn(m => m.IdTarjeta == item,
                        z => new TC_Tarjeta
                        {
                            CopiaCorreoEstCuenta = texto
                        });
                }
                _repTarjeta.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public bool LimiteGlobalPermitido(int idCuentaM, decimal monInter)
        {
            var montoTotal = _repVwTarjetasCuenta.Table
                        .Where(x => x.CodEstadoCta == "V" &&
                               x.IdCuentaMadre == idCuentaM)
                    .Sum(x => x.LimiteCreditoTarjetaInter).Value;
            return (monInter >= montoTotal);
        }

        public void ValidarCambioVipTarjeta(PocModificaVip datosVip)
        {
            var validation = new ValidationResponse();

            if (datosVip == null)
            {
                validation.AddError("Err_0035");
            }
            var diasVigencia = _repParamCompania.Table.FirstOrDefault(
                x => x.IdParametro == "PLAZO_MAXIMO_VIP"
                    && x.IdCompania == InfoSesion.Info.IdCompania).ValParametro;
            if (datosVip.IndicadorVipNuevo)
            {
                if ((datosVip.VigenteHasta.Value - datosVip.VigenteDesde.Value).Days >
                    Int32.Parse(string.IsNullOrEmpty(diasVigencia) ? "0" : diasVigencia))
                {
                    validation.AddError("Err_0089");
                }
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Error validando el límite de crédito", validation: validation);
            }
        }
        public void AplicarCambioVipTarjeta(PocModificaVip datosVip)
        {
            using (var ts = new TransactionScope())
            {
                var tc = _repVwTarjetasCuenta.Table.FirstOrDefault(tar => tar.NumTarjeta == datosVip.NumTarjeta);
                if (tc == null)
                {
                    throw new CoreException("Error al cambiar el indicador VIP de la tarjeta", "Err_0002");
                }
                else
                {
                    //incluye la configuración de VIP en el panel de control
                    var condicionTarjeta = _appConfiguracionCtrl.
                        ObtenerCondicion(datosVip.NumTarjeta) ??
                        new PocCondicion
                        {
                            IdEmpresa = InfoSesion.Info.IdEmpresa,
                            NumTarjeta = datosVip.NumTarjeta
                        };
                    var condicionPrevia = condicionTarjeta.GetClone();
                    InfoSesion.IncluirSesion("CONDICIONPREVIA", condicionPrevia);
                    var cambioFechas = datosVip.VigenteDesde != condicionPrevia.EsVipDesde ||
                        datosVip.VigenteHasta != condicionPrevia.EsVipHasta;

                    if (datosVip.IndicadorVipNuevo == false)
                    {
                        condicionTarjeta.EsVip = datosVip.IndicadorVipNuevo;

                        condicionTarjeta.EsVipDesde = datosVip.VigenteDesde;
                        condicionTarjeta.EsVipHasta = datosVip.VigenteHasta;

                        var config = _appConfiguracionCtrl.RestriccionesTarjeta(datosVip.NumTarjeta);
                        if (config == null)
                        {
                            condicionTarjeta.SinRestriccion = true;
                        }

                        _appConfiguracionCtrl.EstablecerCondicion(condicionTarjeta, notificaCambioConfig: false);
                        _appOperacionesATH.InactivarVip(new PocParametrosVip
                        {
                            NumCuenta = tc.NumCuenta,
                            NumTarjeta = datosVip.NumTarjeta,
                            ValidaVigencia = true,
                            FechaHasta = datosVip.VigenteHasta
                        });
                    }
                    else if (condicionPrevia.EsVip != datosVip.IndicadorVipNuevo || cambioFechas)
                    {
                        var programacionPrevia = condicionPrevia.EsVip &&
                            condicionPrevia.EsVipDesde > DateTime.Now.Date &&
                            condicionPrevia.EsVipHasta > DateTime.Now.Date;

                        condicionTarjeta.EsVip = datosVip.IndicadorVipNuevo;

                        condicionTarjeta.EsVipDesde = datosVip.VigenteDesde;
                        condicionTarjeta.EsVipHasta = datosVip.VigenteHasta;

                        var config = _appConfiguracionCtrl.RestriccionesTarjeta(datosVip.NumTarjeta);
                        if (config == null)
                        {
                            condicionTarjeta.SinRestriccion = true;
                        }

                        _appConfiguracionCtrl.EstablecerCondicion(condicionTarjeta, notificaCambioConfig: false);
                        if (datosVip.VigenteDesde <= DateTime.Now.Date)
                        {
                            _appOperacionesATH.ActivarVip(new PocParametrosVip
                            {
                                NumCuenta = tc.NumCuenta,
                                NumTarjeta = datosVip.NumTarjeta,
                                ValidaVigencia = true,
                                FechaHasta = datosVip.VigenteHasta
                            });
                        }
                    }
                    _repTarjeta.UnidadTbjo.Save();
                }
                ts.Complete();
            }
        }

        public PocRespuestaServicio ActualizarPlaca(string nombreImpreso, string placa)
        {
            Logger.Log("Actualizando placa:" + placa + " de " + nombreImpreso, tipoLog: new string[] { "ArchivoGeo" });
            string resultCode = null;
            try
            {
                var nombre = nombreImpreso == nombreImpreso.TrimEnd('.') ?
                    nombreImpreso.ToLower() : nombreImpreso.ToLower().TrimEnd('.');

                nombreImpreso = nombre == nombreImpreso.ToLower() ?
                    nombreImpreso.ToLower() + '.' : nombreImpreso.ToLower();
                using (var ts = new TransactionScope())
                {
                    if (_repTarjeta.Table.Any(x =>
                        x.NombreImpreso.ToLower() == nombreImpreso ||
                        x.NombreImpreso.ToLower() == nombre))
                    {
                        _repTarjeta.UpdateOn(x =>
                            x.NombreImpreso.ToLower() == nombreImpreso ||
                            x.NombreImpreso.ToLower() == nombre,
                            z => new TC_Tarjeta
                            {
                                Placa = placa
                            });
                        _repTarjeta.UnidadTbjo.Save();
                        ts.Complete();
                        resultCode = "00";
                    }
                    else
                    {
                        resultCode = "01";
                    }
                }
            }
            catch { resultCode = "02"; }

            return new PocRespuestaServicio { Codigo = resultCode };
        }
    }
}

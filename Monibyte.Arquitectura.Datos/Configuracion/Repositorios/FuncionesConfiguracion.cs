﻿using Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Configuracion.Repositorios;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Configuracion.Repositorios
{
    public class FuncionesConfiguracion : RepositorioBase, IFuncionesConfiguracion
    {
        ISql _unidadTbjo;
        public FuncionesConfiguracion(IUnidadTbjoCtrlMonibyte unidadTbjo)
            : base(unidadTbjo)
        {
            _unidadTbjo = unidadTbjo;
        }

        public bool RestriccionVigente(ParamRestriccionVigente parameters)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", (object)parameters.IdCondicionTarjeta));
            listaParams.Add(new SqlParameter("p2", (object)parameters.IdRestriccion));
            listaParams.Add(new SqlParameter("p3", (object)parameters.FecTransacion));

            var resultado = _unidadTbjo.ExecuteQuery<bool>
               (@"SELECT Mnb.F_RestriccionVigente(@p1, @p2, @p3)", listaParams.ToArray()).FirstOrDefault();

            return resultado;
        }


    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_DetalleEstCuenta : EntidadBase
    {
        public int IdDetEstCuenta { get; set; }
        public int IdEstCuenta { get; set; }
        public Nullable<int> IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public string NomTarjetahabiente { get; set; }
        public Nullable<System.DateTime> FecTransaccion { get; set; }
        public string DetMovimiento { get; set; }
        public int IdMoneda { get; set; }
        public Nullable<decimal> MonTransaccion { get; set; }
        public string Ciudad { get; set; }
        public string Pais { get; set; }
        public Nullable<decimal> MonPrincipalLocal { get; set; }
        public Nullable<decimal> MonPrincipalInter { get; set; }
        public Nullable<decimal> InteresesLocal { get; set; }
        public Nullable<decimal> InteresesInter { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string IndDebCredito { get; set; }
        public string CodigoMovimiento { get; set; }
        public Nullable<System.DateTime> FecMovimiento { get; set; }
        public Nullable<decimal> MonCapital { get; set; }
        public Nullable<decimal> MonIntereses { get; set; }
        public Nullable<decimal> MonOtrosCargos { get; set; }
        public Nullable<decimal> MonMoratorios { get; set; }
        public string NumAutorizacion { get; set; }
    }
}

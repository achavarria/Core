﻿using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Nucleo;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Tarjetas.Repositorios
{

    public class RepositorioVwTarjetaUsuario : RepositorioMnb<TC_vwTarjetaUsuario>, IRepositorioVwTarjetaUsuario
    {
        public RepositorioVwTarjetaUsuario(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public TC_vwTarjetaUsuario ObtenerTarjeta(int idTarjeta)
        {
            var query = this as IRepositorioConsulta<TC_vwTarjetaUsuario>;
            return query.Table.FirstOrDefault(x => x.IdTarjeta == idTarjeta);
        }

        public TC_vwTarjetaUsuario ObtenerTarjeta(string numTarjeta, int idUsuario)
        {
            var query = this as IRepositorioConsulta<TC_vwTarjetaUsuario>;
            return query.Table.FirstOrDefault(x => x.NumTarjeta == numTarjeta && x.IdUsuario == idUsuario);
        }

        public IEnumerable<TC_vwTarjetaUsuario> TarjetasPermitidas(int? idCuenta = null,
            bool incluirHijas = true, int[] filtroEstado = null, int[] filtroEstadoExc = null,
            Criteria<TC_vwTarjetaUsuario> criterias = null, bool incluirOperativas = false,
            bool ignorarPanelCtrl = false, bool ignorarIdGrupo = true, bool ignorarSoloConsulta = false)
        {
            string pTarjActivas = "S";
            var criteria = new Criteria<TC_vwTarjetaUsuario>();
            if (!incluirHijas)
            {
                criteria.And(m => m.IdCuentaMadre == null);
            }
            if (filtroEstado != null && filtroEstado.Any())
            {
                if (filtroEstado.Contains(-1))
                {
                    pTarjActivas = "N";
                }
                else
                {
                    pTarjActivas = "T";
                }
                filtroEstado = filtroEstado.Where(m => m != -1 && m != -2).ToArray();
                if (filtroEstado.Any())
                {
                    criteria.And(m => EvalStatus(filtroEstado, m.IdEstadoTarjeta, false));
                }
            }
            if (filtroEstadoExc != null && filtroEstadoExc.Any())
            {
                criteria.And(m => EvalStatus(filtroEstadoExc, m.IdEstadoTarjeta, true));
            }
            if (criterias != null)
            {
                criteria.And(criterias.Satisfecho());
            }
            var listaParams = new List<SqlParameter>(6);
            listaParams.Add(new SqlParameter("p1", InfoSesion.Info.IdUsuario));
            listaParams.Add(new SqlParameter("p2", InfoSesion.Info.IdEmpresa));
            listaParams.Add(new SqlParameter("p3", (object)idCuenta ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p4", pTarjActivas));
            listaParams.Add(new SqlParameter("p5", incluirOperativas ? "S" : "N"));
            listaParams.Add(new SqlParameter("p6", ignorarPanelCtrl ? "S" : "N"));
            listaParams.Add(new SqlParameter("p7", ignorarIdGrupo ? "S" : "N"));
            listaParams.Add(new SqlParameter("p8", ignorarSoloConsulta ? "S" : "N"));

            var resultado = (this.UnidadTbjo as ISql).ExecuteQuery<TC_vwTarjetaUsuario>
                (@"TCredito.P_TarjetasUsuario @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8",
                listaParams.ToArray()).ToList();

            var queriable = resultado.AsQueryable<TC_vwTarjetaUsuario>()
                as IQueryable<TC_vwTarjetaUsuario>;
            return queriable.Where(criteria.Satisfecho());
        }

        public IEnumerable<TC_vwTarjetaUsuario> TarjetasPermitidasGrupo(int? idCuenta = null,
            int? idGrupoEmpresa = null, int[] filtroEstado = null)
        {
            var criterias = new Criteria<TC_vwTarjetaUsuario>();
            criterias.And(m => idGrupoEmpresa.HasValue ? m.IdGrupoEmpresa
                == idGrupoEmpresa : !m.IdGrupoEmpresa.HasValue);
            return TarjetasPermitidas(idCuenta, true, filtroEstado,
                criterias: criterias, ignorarIdGrupo: false);
        }

        private bool EvalStatus(int[] filtro, int? status, bool exclude)
        {
            if (exclude)
            {
                return !filtro.Contains((int)status);
            }
            return filtro.Contains(status ?? -1);
        }
    }
}

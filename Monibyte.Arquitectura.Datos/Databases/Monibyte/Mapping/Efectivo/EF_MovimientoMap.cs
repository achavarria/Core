using Monibyte.Arquitectura.Dominio.Efectivo;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Efectivo
{
    public class EF_MovimientoMap : EntityTypeConfiguration<EF_Movimiento>
    {
        public EF_MovimientoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMovimiento);

            // Properties
            this.Property(t => t.IdMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumFactura)
                .HasMaxLength(20);

            this.Property(t => t.DetalleMovimiento)
                .HasMaxLength(2000);

            this.Property(t => t.CentroCosto)
                .HasMaxLength(20);

            this.Property(t => t.CuentaContable)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("EF_Movimiento", "Efectivo");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
            this.Property(t => t.NumFactura).HasColumnName("NumFactura");
            this.Property(t => t.DetalleMovimiento).HasColumnName("DetalleMovimiento");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.MonMovimiento).HasColumnName("MonMovimiento");
            this.Property(t => t.CentroCosto).HasColumnName("CentroCosto");
            this.Property(t => t.CuentaContable).HasColumnName("CuentaContable");
            this.Property(t => t.NumKmSalida).HasColumnName("NumKmSalida");
            this.Property(t => t.NumKmLlegada).HasColumnName("NumKmLlegada");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.IdDebCredito).HasColumnName("IdDebCredito");
            this.Property(t => t.CamposDinamicos).HasColumnName("CamposDinamicos");
            this.Property(t => t.IdTipoVehiculo).HasColumnName("IdTipoVehiculo");
            this.Property(t => t.Modelo).HasColumnName("Modelo");
            this.Property(t => t.Placa).HasColumnName("Placa");
            this.Property(t => t.IdLiquidado).HasColumnName("IdLiquidado");
        }
    }
}

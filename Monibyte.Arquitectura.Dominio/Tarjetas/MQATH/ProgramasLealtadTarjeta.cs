﻿using System;
using Monibyte.Arquitectura.Comun.Nucleo.Trama;
namespace Monibyte.Arquitectura.Dominio.Tarjetas.MQATH
{
    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaLealtadTarjeta {
        string _blancos = string.Empty.PadRight(91, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.LEALTAD_TARJETA; } set { value = TipoEntradaMsj.LEALTAD_TARJETA; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 91)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }
    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaLealtadTarjeta:RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4)]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16)]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13)]
        public string NumCuenta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string CodPlan { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12)]
        public decimal SaldoPremiacion { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 911)]
        public string Blancos { get; set; }
    }

    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaAplicMovLealtadTarjeta
    {
        string _blancos = string.Empty.PadRight(75, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.APLICA_MOV_LEALTAD; } set { value = TipoEntradaMsj.APLICA_MOV_LEALTAD; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal Cantidad { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string CodPlan { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string CodMovimiento { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 75)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }
    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaAplicMovLealtadTarjeta :RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100)]
        public decimal Cantidad { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string CodPlan { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string CodMovimiento { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 901)]
        public string Blancos { get; set; }
    }

}

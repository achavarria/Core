using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Quickpass
{
    public partial class QP_Bitacora : EntidadBase
    {
        public int IdBitacora { get; set; }
        public int IdQuickpass { get; set; }
        public int IdEstado { get; set; }
        public Nullable<int> IdEstadoOperativo { get; set; }
        public string Detalle { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

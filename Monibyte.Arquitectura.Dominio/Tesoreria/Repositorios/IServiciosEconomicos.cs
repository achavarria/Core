﻿
using System;
namespace Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios
{
   public interface IServiciosEconomicos
    {
       object ObtenerTipoCambioCompra(DateTime? fecTipoCambio = null);
       object ObtenerTipoCambioVenta(DateTime? fecTipoCambio = null);
    }
}

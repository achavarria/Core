﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Notificacion;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Autorizador;
using Monibyte.Arquitectura.Dominio.Autorizador.Repositorios;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Configuracion.Repositorios;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Logs;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Configuracion;
using Monibyte.Arquitectura.Poco.Notificacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using ConfigDomain = Monibyte.Arquitectura.Dominio.Configuracion;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public class AppConfiguracionCtrl : AdmCondicion, IAppConfiguracionCtrl
    {
        protected override int TipoAutorizacion
        {
            get { return (int)EnumTipoAutorizacion.RestriccionesTarjeta; }
        }

        private IRepositorioCuenta _repCuenta;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<GL_Pais> _repPais;
        private IRepositorioVwTarjetaUsuario _repVwTarjetaUsuario;
        private IRepositorioMnb<LG_BitacoraPanelControl> _repBitacoraPanelControl;
        private IRepositorioTipoComercio _repTipoComercioConf;
        private IRepositorioMnb<ConfigDomain.MB_Region> _repRegionConf;
        private IRepositorioMnb<ConfigDomain.MB_CategoriaTipoComercio> _repCatTipoComercioConf;
        private IRepositorioMnb<ConfigDomain.MB_GruposEmpresa> _repGruposEmpresaConf;
        private IRepositorioCondicionTarjeta _repCondicionTarjeta;
        private IRepositorioCtrl<MB_MccTarjeta> _repMccTarjeta;
        private IRepositorioCtrl<MB_HorarioTarjeta> _repHorarioTarjeta;
        private IRepositorioCtrl<MB_RegionPaisTarjeta> _repRegionPaisTarjeta;
        private IRepositorioCtrl<MB_CategoriasEmpresa> _repCategoriaPorEmpresa;
        private IRepositorioMnb<SC_Autorizacion> _repAutorizacion;
        private IFuncionesConfiguracion _funcConf;
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IAppConfiguracionMisc _appConfigMisc;
        private IAppNotificacion _appNotificacion;
        private IFuncionesTesoreria _repFuncionTesoreria;

        public AppConfiguracionCtrl(
            IRepositorioCuenta repCuenta,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<GL_Pais> repPais,
            IRepositorioVwTarjetaUsuario repVwTarjetaUsuario,
            IRepositorioMnb<LG_BitacoraPanelControl> repBitacoraPanelControl,
            IRepositorioTipoComercio repTipoComercioConf,
            IRepositorioMnb<ConfigDomain.MB_Region> repRegionConf,
            IRepositorioMnb<ConfigDomain.MB_CategoriaTipoComercio> repCatTipoComercioConf,
            IRepositorioMnb<ConfigDomain.MB_GruposEmpresa> repGruposEmpresaConf,
            IRepositorioCondicionTarjeta repCondicionTarjeta,
            IRepositorioCtrl<MB_MccTarjeta> repMccTarjeta,
            IRepositorioCtrl<MB_HorarioTarjeta> repHorarioTarjeta,
            IRepositorioCtrl<MB_RegionPaisTarjeta> repRegionPaisTarjeta,
            IRepositorioCtrl<MB_CategoriasEmpresa> repCategoriaPorEmpresa,
            IRepositorioMnb<SC_Autorizacion> repAutorizacion,
            IAppNotificacion appNotificacion,
            IFuncionesConfiguracion funcConf,
            IRepositorioMnb<GL_Entidad> repEntidad,
            IAppConfiguracionMisc appConfigMisc,
            IFuncionesTesoreria repFuncionTesoreria)
        {
            _repCuenta = repCuenta;
            _repCatalogo = repCatalogo;
            _repPais = repPais;
            _repVwTarjetaUsuario = repVwTarjetaUsuario;
            _repBitacoraPanelControl = repBitacoraPanelControl;

            _repTipoComercioConf = repTipoComercioConf;
            _repRegionConf = repRegionConf;
            _repCatTipoComercioConf = repCatTipoComercioConf;
            _repGruposEmpresaConf = repGruposEmpresaConf;

            _repCondicionTarjeta = repCondicionTarjeta;
            _repMccTarjeta = repMccTarjeta;
            _repHorarioTarjeta = repHorarioTarjeta;
            _repRegionPaisTarjeta = repRegionPaisTarjeta;
            _repCategoriaPorEmpresa = repCategoriaPorEmpresa;

            _repAutorizacion = repAutorizacion;
            _funcConf = funcConf;
            _repEntidad = repEntidad;

            _appNotificacion = appNotificacion;
            _appConfigMisc = appConfigMisc;
            _repFuncionTesoreria = repFuncionTesoreria;
        }

        #region Catalogos del control monibyte: grupos y tarjetas
        public DataResult<PocGrupoEmpresaCatalogo> ObtenerGruposEmpresaCatalogo(PocConsultaGrupoEmpresa consulta, DataRequest request)
        {
            var cuenta = _repCuenta.SelectById(consulta.IdCuenta);
            var resultado = _repGruposEmpresaConf.Table
                .Where(p => p.IdEmpresa == consulta.IdEmpresa &&
                    p.IdEstado == (int)EnumIdEstado.Activo &&
                    p.IdTipoGrupo == (int)EnumTipoGrupo.PanelControl)
                .ToList()
                .Where(p =>
                    _repVwTarjetaUsuario.TarjetasPermitidasGrupo
                    (consulta.IdCuenta, p.IdGrupo).Count() > 0)
                .Select(p => new PocGrupoEmpresaCatalogo
                {
                    IdEstado = p.IdEstado,
                    Descripcion = p.Descripcion,
                    IdGrupo = p.IdGrupo,
                    LimiteCuenta = cuenta.LimiteCreditoInter,
                    LimiteGrupo = 0
                }).ToList();
            if (_repVwTarjetaUsuario.TarjetasPermitidasGrupo(
                consulta.IdCuenta).Count() > 0)
            {
                resultado.Insert(0, new PocGrupoEmpresaCatalogo
                {
                    Descripcion = ResxHelper.Load("RecMensajes", "Msj_SinAsignar"),
                    LimiteCuenta = cuenta.LimiteCreditoInter,
                    LimiteGrupo = 0
                });
            }
            return resultado.ToDataResult(request);
        }

        public DataResult<PocTarjetaGrupoCatalogo> ObtenerTarjetasGrupoCatalogo(PocConsultaGrupoEmpresa consulta, DataRequest request)
        {
            var resultado = _repVwTarjetaUsuario.TarjetasPermitidas(consulta.IdCuenta, ignorarIdGrupo: false)
                .GroupJoin(_repGruposEmpresaConf.Table,
                    usr => usr.IdGrupoEmpresa,
                    emp => emp.IdGrupo,
                    (usr, emp) => new { usr, emp = emp.FirstOrDefault() })
                 .GroupJoin(_repAutorizacion.Table,
                    item => new
                    {
                        item.usr.NumTarjeta,
                        IdEstado = (int)EnumEstadoAutorizacion.Preparada,
                        IdTipoAutorizacion = (int)EnumTipoAutorizacion.RestriccionesTarjeta
                    },
                    aut => new
                    {
                        NumTarjeta = aut.NumReferencia,
                        IdEstado = aut.IdEstado,
                        aut.IdTipoAutorizacion
                    },
                    (item, aut) => new { item.usr, item.emp, aut = aut.FirstOrDefault() })
                .Select(sel => new PocTarjetaGrupoCatalogo
                {
                    IdCuenta = sel.usr.IdCuenta,
                    IdTarjeta = sel.usr.IdTarjeta,
                    IdTipoPlastico = sel.usr.IdTipoPlastico,
                    NombreImpreso = sel.usr.NombreImpreso,
                    IdRelacion = sel.usr.IdRelacion,
                    NumTarjeta = sel.usr.NumTarjeta,
                    IdEstadoTarjeta = sel.usr.IdEstadoTarjeta,
                    LimiteCreditoLocal = sel.usr.LimiteCreditoTarjetaLocal,
                    LimiteCreditoInter = sel.usr.LimiteCreditoTarjetaInter,
                    IdGrupoEmpresa = sel.usr.IdGrupoEmpresa,
                    Descripcion = sel.emp != null ? sel.emp.Descripcion : ResxHelper.Load("RecMensajes", "Msj_SinAsignar"),
                    Autoriza = sel.usr.IdAutoriza != null ? sel.usr.IdAutoriza == (int)EnumIdSiNo.Si : !Context.IsEditor,
                    SoloConsulta = sel.usr.IdSoloConsulta == (int)EnumIdSiNo.Si,
                    AutorizacionPendiente = sel.aut != null
                });
            return resultado.ToDataResult(request);
        }

        #endregion

        #region metodos nuevos de configuracion
        public void EstablecerCondicionPorDefecto(int idEmpresa, string numTarjeta)
        {
            var condicionPrevia = ObtenerCondicion(numTarjeta);
            if (condicionPrevia != null)
            {
                //Se establece el valor actual como condicion previa
                InfoSesion.IncluirSesion("CONDICIONPREVIA", condicionPrevia);
                var entidad = _repEntidad.SelectById(InfoSesion.ObtenerSesion<int>("IDPROCESADOR"));
                var condicion = new PocCondicion
                {
                    //Se incluye el parémetro SinRestriccion
                    SinRestriccion = true,
                    NumTarjeta = numTarjeta,
                    IdEmpresa = idEmpresa,
                    IdCondicion = condicionPrevia.IdCondicion,
                    IdMoneda = entidad.IdMonedaLocal.Value
                };
                EstablecerCondicion(condicion);
            }
        }

        public void ResetCategorias(List<PocMccCondicion> condiciones, int idMoneda)
        {
            foreach (var item in condiciones)
            {
                ModificarMcc(item, idMoneda);
            }
        }

        public void PermitirConsumoInternet(int idCondicion, bool consumeInternet)
        {
            using (var ts = new TransactionScope())
            {
                _repCondicionTarjeta.UpdateOn(x => x.IdCondicionTarjeta == idCondicion,
                    z => new MB_CondicionTarjeta
                    {
                        ConsumeInternet = consumeInternet ?
                            (int)EnumEstadoRestriccion.Activo : (int)EnumEstadoRestriccion.Inactivo
                    });
                _repCondicionTarjeta.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        #endregion

        #region metodos heredados y sobreescritos
        //registro en bd y validaciones
        protected override void SalvarCambios()
        {
            //Salvar la configuracion de la tarjeta
            _repCondicionTarjeta.UnidadTbjo.Save();
        }

        protected override void SalvarBitacora(PocCondicion condicion, MultilanguageTopic tracker, bool notificaCambioConfig)
        {
            try
            {
                var _topic = InfoSesion.ObtenerSesion<MultilanguageTopic>("TOPICTRACK");
                if (_topic.Items.Count() > 0 || _topic.Details.Count() > 0)
                {
                    var _bitacora = new LG_BitacoraPanelControl
                    {
                        IdCondicion = condicion.IdCondicion,
                        NumTarjeta = condicion.NumTarjeta,
                        DatosBitacora = tracker.ToJson().CompressStr()
                    };
                    _repBitacoraPanelControl.Insert(_bitacora);
                    _repBitacoraPanelControl.UnidadTbjo.Save();

                    #region EnvioNotificaciones
                    if (notificaCambioConfig)
                    {
                        _appNotificacion.ProcesarNotificaciones(new PocoQueue
                        {
                            MessageType = "0103",
                            EntityId = InfoSesion.Info.IdCompania,
                            CompanyId = InfoSesion.Info.IdEmpresa,
                            OriginId = (int)EnumIdContextoNotificacion.PanelControl,
                            Data = new
                            {
                                IdEmpresa = InfoSesion.Info.IdEmpresa,
                                IdPersona = InfoSesion.Info.IdPersona,
                                CardNumber = condicion.NumTarjeta,
                                FecGestion = DateTime.Now,
                                Detalle = _appConfigMisc.ObtieneDetalleBitacoraPanelCtrl(tracker.ToJson())
                            }.ToJson()
                        });
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Logger.Log(string.Format("Error: {0} / PocCondicion: {1}", ex.Message, condicion.ToJson()), titulo:
                    "SalvarBitacora", idEvento: 200, severity: System.Diagnostics.TraceEventType.Error);
            }
        }

        protected override void ValidarCondicion(PocCondicion condicion, bool update = false)
        {
            var validation = new ValidationResponse();
            if (string.IsNullOrEmpty(condicion.NumTarjeta))
            {
                validation.AddError("Err_0002");
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Error al establecer condicion de tarjeta", validation: validation);
            }
        }

        //consultas
        protected override PocCondicion ObtenerCondicionPrincipal(object criterioBusqueda)
        {
            var condicionTarjeta = _repCondicionTarjeta.ObtenerPorNumTarjeta(criterioBusqueda as string);
            if (condicionTarjeta != null)
            {
                return condicionTarjeta.ProyectarComo<PocCondicion>();
            }
            return null;
        }

        protected override List<PocMccCondicion> ObtenerMccs(int idCondicion,
                                        int? idMoneda, bool esActualizar = false)
        {
            var entidad = _repEntidad.SelectById(InfoSesion.ObtenerSesion<int>("IDPROCESADOR"));
            var esLocal = entidad.IdMonedaLocal.Value == idMoneda;

            var criteria = new Criteria<MB_MccTarjeta>();
            criteria.And(m => m.IdCondicionTarjeta == idCondicion);
            var resulMcc =
                from mcc in _repMccTarjeta.SelectBy(criteria).ToList()
                join cat in _repCatTipoComercioConf.Table
                    on mcc.IdCategoriaMcc equals cat.IdCategoria
                join per in _repCatalogo.List("LPERIODICIDAD")
                    on mcc.IdPeriodicidad equals per.IdCatalogo
                join tip in _repTipoComercioConf.Table
                    on new
                    {
                        CodMcc = mcc.CodMccDesde,
                        mcc.IdSoloCategoria
                    } equals new
                    {
                        tip.CodMcc,
                        IdSoloCategoria = (int)EnumIdSiNo.No
                    } into tipCom
                from tip in tipCom.DefaultIfEmpty()
                join cpe in _repCategoriaPorEmpresa.Table
                    on cat.IdCategoria equals cpe.IdCategoriaPadre into cpeCom
                //from cpe in cpeCom.DefaultIfEmpty()
                where (mcc.IdEstado == (int)EnumIdEstado.Activo &&
                    cat.IdEstado == (int)EnumIdEstado.Activo &&
                    (mcc.IdSoloCategoria == (int)EnumIdSiNo.No ||
                        (mcc.IdSoloCategoria == (int)EnumIdSiNo.Si &&
                            mcc.CodMccDesde == _repMccTarjeta.SelectBy(criteria)
                            .Where(com => com.IdCategoriaMcc == mcc.IdCategoriaMcc)
                            .Min(m => m.CodMccDesde))))
                select new PocMccCondicion
                {
                    IdCondicion = mcc.IdCondicionTarjeta,
                    IdCategoriaMcc = cat.IdCategoria,
                    IdMcc = mcc.IdMcc,
                    IdEstado = mcc.IdEstado,
                    LimiteConsumo = esLocal ? mcc.LimiteConsumo : mcc.LimiteConsumoInter,
                    LimiteConsumoInter = mcc.LimiteConsumoInter,
                    SaldoLimiteConsumo = esLocal ? mcc.SaldoLimiteConsumo : mcc.SaldoLimiteConsumoInter,
                    SaldoLimiteConsumoInter = mcc.LimiteConsumoInter,
                    CantidadConsumo = mcc.CantidadConsumo,
                    SaldoCantidadConsumo = mcc.SaldoCantidadConsumo,
                    CodMcc = tip != null ? tip.CodMcc : null,
                    DescripcionMcc = string.Format("{0} - {1}",
                        tip != null ? tip.CodMcc : null,
                        tip != null ? tip.Descripcion : null),
                    DescripcionCategoria = _fGenerales.Traducir(cat.Descripcion,
                        "MB_CategoriaTipoComercio", cat.IdCategoria,
                        Context.Lang.Id, 0, Context.DefaultLang.Id),
                    DescripcionPeriodicidad = per.Descripcion,
                    IdPeriodicidad = mcc.IdPeriodicidad,
                    IdsSubCat = cpeCom.Select(x => x.IdCategoria).ToArray(),
                    IdSoloCategoria = mcc.IdSoloCategoria
                };
            return resulMcc.ToList();
        }

        protected override List<PocHorarioCondicion> ObtenerHorarios(int idCondicion)
        {
            var criteria = new Criteria<MB_HorarioTarjeta>();
            criteria.And(m => m.IdCondicionTarjeta == idCondicion);
            var restHorario =
                from hor in _repHorarioTarjeta.SelectBy(criteria).ToList()
                join cat in _repCatalogo.List("LDIASSEMANA") on
                    hor.IdDia.ToString() equals cat.Codigo into dias
                from dia in dias.DefaultIfEmpty()
                select new PocHorarioCondicion
                {
                    IdHorario = hor.IdHorario,
                    IdCondicion = hor.IdCondicionTarjeta,
                    IdDia = hor.IdDia,
                    HoraDesde = hor.HoraDesde,
                    HoraHasta = hor.HoraHasta,
                    IdRestriccion = hor.IdRestriccion,
                    IdEstado = hor.IdEstado,
                    DescripcionDia = hor.IdDia != 0 ? dia.Descripcion : null
                };
            return restHorario.ToList();
        }

        protected override List<PocRegionPaisCondicion> ObtenerRegionesPaises(int idCondicion)
        {
            var criteria = new Criteria<MB_RegionPaisTarjeta>();
            criteria.And(m => m.IdCondicionTarjeta == idCondicion);
            var restRegionPais = _repRegionPaisTarjeta.SelectBy(criteria).ToList()
                .GroupJoin(_repRegionConf.Table,
                    cond => cond.IdRegion,
                    region => region.IdRegion,
                    (condicion, regiones) => new { condicion, region = regiones.FirstOrDefault() })
                .GroupJoin(_repPais.Table,
                    rep => rep.condicion.CodPaisISO,
                    pais => pais.CodISO,
                    (condicion, paises) => new
                    {
                        condicion = condicion.condicion,
                        region = condicion.region,
                        pais = paises.FirstOrDefault()
                    })
                .Select(res => new PocRegionPaisCondicion
                {
                    IdRegionPais = res.condicion.IdRegionPais,
                    IdCondicion = res.condicion.IdCondicionTarjeta,
                    IdEstado = res.condicion.IdEstado,
                    IdPais = res.pais != null ? (int?)res.pais.IdPais : null,
                    IdRegion = res.region != null ? (int?)res.region.IdRegion : null,
                    DescripcionPais = res.pais != null ? res.pais.Descripcion : string.Empty,
                    DescripcionRegion = res.region != null ? res.region.Descripcion : string.Empty
                });
            return restRegionPais.ToList();
        }

        //registros nuevos
        protected override int? InsertarCondicionPrincipal(PocCondicion condicion)
        {
            var entradaCondicion = condicion.ProyectarComo<MB_CondicionTarjeta>();
            entradaCondicion.IdCondicionTarjeta = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdCondicionTarjeta");
            entradaCondicion.IdEstado = (int)EnumIdEstado.Activo;
            _repCondicionTarjeta.Insert(entradaCondicion);
            return entradaCondicion.IdCondicionTarjeta;
        }

        protected override void InsertarMcc(PocMccCondicion item)
        {
            var mcc = item.ProyectarComo<MB_MccTarjeta>();
            mcc.IdEstado = (int)EnumIdEstado.Activo;
            mcc.IdMcc = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdMccTarjeta");
            mcc.IdSoloCategoria = item.CodMcc == this.DefaultMcc ?
                (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
            mcc.SaldoLimiteConsumo = item.LimiteConsumo;
            mcc.SaldoCantidadConsumo = item.CantidadConsumo;
            mcc.SaldoLimiteConsumoInter = item.LimiteConsumoInter.Value;
            _repMccTarjeta.Insert(mcc);
        }

        protected override void InsertarHorario(PocHorarioCondicion item)
        {
            var hrs = item.ProyectarComo<MB_HorarioTarjeta>();
            hrs.IdHorario = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdHorarioTarjeta");
            hrs.IdEstado = (int)EnumIdEstado.Activo;
            hrs.IdRestriccion = null;
            _repHorarioTarjeta.Insert(hrs);
        }

        protected override void InsertaVigencia(int idCondicion,
            EnumTipoRestriccion tipoRestriccion, DateTime horaDesde, DateTime horaHasta)
        {
            var horarioTarjeta = new MB_HorarioTarjeta();
            horarioTarjeta.IdHorario = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdHorarioTarjeta");
            horarioTarjeta.IdDia = 0;
            horarioTarjeta.IdEstado = (int)EnumIdEstado.Activo;
            horarioTarjeta.IdCondicionTarjeta = idCondicion;
            horarioTarjeta.IdRestriccion = (int)tipoRestriccion;
            horarioTarjeta.HoraDesde = horaDesde;
            horarioTarjeta.HoraHasta = horaHasta;
            _repHorarioTarjeta.Insert(horarioTarjeta);
        }

        protected override void InsertarRegionPais(PocRegionPaisCondicion item)
        {
            var regionPais = item.ProyectarComo<MB_RegionPaisTarjeta>();
            if (item.IdPais.HasValue)
            {
                var glPais = _repPais.Table.Where(p => p.IdPais == item.IdPais);
                regionPais.CodPaisISO = glPais.First().CodISO;
            }
            regionPais.IdRegionPais = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdRegionPaisTarjeta");
            regionPais.IdEstado = (int)EnumIdEstado.Activo;
            _repRegionPaisTarjeta.Insert(regionPais);
        }

        //modificar registros
        protected override int? ModificarCondicionPrincipal(PocCondicion condicion)
        {
            var entradaCondicion = condicion.ProyectarComo<MB_CondicionTarjeta>();
            entradaCondicion.IdEstado = (int)EnumIdEstado.Activo;
            _repCondicionTarjeta.UpdateExclude(entradaCondicion, p => p.FecInclusionAud, p => p.IdUsuarioIncluyeAud);
            return entradaCondicion.IdCondicionTarjeta;
        }

        protected override void ModificarMcc(PocMccCondicion item)
        {
            _repMccTarjeta.UpdateOn(m => m.IdCategoriaMcc == item.IdCategoriaMcc &&
                m.IdCondicionTarjeta == item.IdCondicion,
                up => new MB_MccTarjeta
                {
                    LimiteConsumo = item.LimiteConsumo,
                    LimiteConsumoInter = item.LimiteConsumoInter.Value,
                    CantidadConsumo = item.CantidadConsumo,
                    SaldoLimiteConsumo = item.LimiteConsumoAdm > 0 ? item.LimiteConsumoAdm : item.LimiteConsumo,
                    SaldoLimiteConsumoInter = item.LimiteConsumoInter.Value,
                    SaldoCantidadConsumo = item.CantidadConsumoAdm > 0 ? item.CantidadConsumoAdm : item.CantidadConsumo,
                    IdPeriodicidad = item.IdPeriodicidad
                });
        }

        private void ModificarMcc(PocMccCondicion item, int idMoneda)
        {
            using (var ts = new TransactionScope())
            {
                var entidad = _repEntidad.SelectById(InfoSesion.ObtenerSesion<int>("IDPROCESADOR"));
                var esLocal = entidad.IdMonedaLocal.Value == idMoneda;
                if (esLocal)
                {
                    _repMccTarjeta.UpdateOn(m => m.IdCategoriaMcc == item.IdCategoriaMcc &&
                        m.IdCondicionTarjeta == item.IdCondicion,
                        up => new MB_MccTarjeta
                        {
                            SaldoLimiteConsumo = item.LimiteConsumoAdm > 0 ? item.LimiteConsumoAdm : item.LimiteConsumo,
                            SaldoCantidadConsumo = item.CantidadConsumoAdm > 0 ? item.CantidadConsumoAdm : item.CantidadConsumo
                        });
                }
                else
                {
                    var condicionActual = _repMccTarjeta.Table.FirstOrDefault(m => m.IdCategoriaMcc == item.IdCategoriaMcc &&
                        m.IdCondicionTarjeta == item.IdCondicion);

                    var saldoLimiteLocal = _repFuncionTesoreria.ConvierteMonto(entidad.IdMonedaInternacional,
                        item.LimiteConsumoAdm > 0 ? item.LimiteConsumoAdm.Value : item.LimiteConsumo.Value,
                        entidad.IdMonedaLocal);

                    _repMccTarjeta.UpdateOn(m => m.IdCategoriaMcc == item.IdCategoriaMcc &&
                        m.IdCondicionTarjeta == item.IdCondicion,
                        up => new MB_MccTarjeta
                        {
                            SaldoLimiteConsumo = saldoLimiteLocal,
                            SaldoLimiteConsumoInter = item.LimiteConsumoAdm > 0 ?
                                                      item.LimiteConsumoAdm.Value : item.LimiteConsumo.Value,
                            SaldoCantidadConsumo = item.CantidadConsumoAdm > 0 ? item.CantidadConsumoAdm : item.CantidadConsumo
                        });
                }
                ts.Complete();
            }
        }

        protected override void ModificarHorario(PocHorarioCondicion item)
        {
            var horario = item.ProyectarComo<MB_HorarioTarjeta>();
            _repHorarioTarjeta.Update(horario, m => m.HoraDesde, m => m.HoraHasta);
        }

        protected override void ModificarVigencia(PocHorarioCondicion item)
        {
            var horario = item.ProyectarComo<MB_HorarioTarjeta>();
            _repHorarioTarjeta.UpdateOn(m => m.IdCondicionTarjeta == item.IdCondicion && m.IdDia == 0 &&
                m.IdRestriccion == item.IdRestriccion,
                up => new MB_HorarioTarjeta
                {
                    HoraDesde = item.HoraDesde,
                    HoraHasta = item.HoraHasta
                });
        }

        protected override void ModificarRegionPais(PocRegionPaisCondicion item)
        {
            var regionPais = item.ProyectarComo<MB_RegionPaisTarjeta>();
            _repRegionPaisTarjeta.Update(regionPais, m => m.IdRegion, m => m.CodPaisISO);
        }

        //eliminar registros no utilizados
        protected override void EliminarMcc(PocMccCondicion item)
        {
            _repMccTarjeta.DeleteOn(m => m.IdCategoriaMcc == item.IdCategoriaMcc &&
                m.IdCondicionTarjeta == item.IdCondicion);
        }

        protected override void EliminarHorario(PocHorarioCondicion item)
        {
            _repHorarioTarjeta.DeleteOn(m => m.IdDia == item.IdDia && m.IdDia != 0 &&
                m.IdCondicionTarjeta == item.IdCondicion);
        }

        protected override void EliminarVigencia(PocHorarioCondicion item)
        {
            _repHorarioTarjeta.DeleteOn(m => m.IdCondicionTarjeta == item.IdCondicion && m.IdDia == 0 &&
                m.IdRestriccion == item.IdRestriccion);
        }

        protected override void EliminarRegionPais(PocRegionPaisCondicion item)
        {
            _repRegionPaisTarjeta.DeleteOn(m => m.IdRegionPais == item.IdRegionPais &&
                m.IdCondicionTarjeta == item.IdCondicion);
        }

        public MB_CondicionTarjeta RestriccionesTarjeta(string numTarjeta)
        {
            return _repCondicionTarjeta.Table
                .FirstOrDefault(x => x.NumTarjeta == numTarjeta);
        }

        #endregion

        #region APPMONIBYTE

        public bool VerificaEsVip(string numeroTarjeta)
        {
            bool EsVip = false;
            var datos = _repCondicionTarjeta.Table.Where(x => x.NumTarjeta == numeroTarjeta).FirstOrDefault();
            if (datos != null)
            {
                EsVip = datos.EsVip == (int)EnumEstadoRestriccion.Activo || datos.EsVip == (int)EnumEstadoRestriccion.ActivoRango;
            }
            return EsVip;
        }

        public PocHorarioCondicion VerificaDetalleVip(string numeroTarjeta)
        {
            var detallevip =
                (from cond in _repCondicionTarjeta.Table.Where(x => x.NumTarjeta == numeroTarjeta).ToList()
                 join horario in _repHorarioTarjeta.Table on cond.IdCondicionTarjeta equals horario.IdCondicionTarjeta
                 where horario.IdRestriccion == (int)EnumTipoRestriccion.VIP
                 select new PocHorarioCondicion
                 {
                     IdHorario = horario.IdHorario,
                     IdCondicion = horario.IdCondicionTarjeta,
                     IdDia = horario.IdDia,
                     HoraDesde = horario.HoraDesde,
                     HoraHasta = horario.HoraHasta,
                     IdRestriccion = horario.IdRestriccion,
                     IdEstado = horario.IdEstado
                 }).FirstOrDefault();

            return detallevip;
        }

        public void RestriccionesDesactivadas(string numTarjeta, ref bool restriccionesDesactivadas, ref int vigencia, ref int tiempoRemanente)
        {
            var datos = ObtenerCondicion(numTarjeta);
            if (datos != null)
            {
                restriccionesDesactivadas = datos.DesactivarRestriccionesPorLapso; //consultar como va a funcionar
                if (restriccionesDesactivadas)
                {
                    if (RestriccionVigente(new ParamRestriccionVigente
                    {
                        IdCondicionTarjeta = datos.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.SinRestriccion,
                        FecTransacion = DateTime.Now
                    }))
                    {
                        var FecDesde = datos.DesactivarRestriccionesDesde.Value;
                        var fecHasta = datos.DesactivarRestriccionesHasta.Value;
                        if (FecDesde != null && fecHasta != null)
                        {
                            vigencia = (fecHasta.Subtract(FecDesde).Hours) * 60 >= 60 ? (fecHasta.Subtract(FecDesde).Hours) * 60 : fecHasta.Subtract(FecDesde).Minutes;
                            tiempoRemanente = fecHasta.Subtract(DateTime.Now).Minutes;
                        }
                    }
                }
            }
        }

        protected override bool RestriccionVigente(ParamRestriccionVigente parameters)
        {
            return _funcConf.RestriccionVigente(parameters);
        }
        #endregion
    }
}

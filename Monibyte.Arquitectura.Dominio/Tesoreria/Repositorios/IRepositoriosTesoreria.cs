﻿using Monibyte.Arquitectura.Dominio.Nucleo;

namespace Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios
{
    public interface IRepositorioMoneda: IRepositorioMnb<TS_Moneda>
    {
        TS_Moneda ObtenerPorCodigoAth(string codigo);
    }
    public interface IRepositorioTipoCambio : IRepositorioMnb<TS_TipoDeCambio>
    {
        TS_TipoDeCambio ObtenerUltTCVentaXMoneda(int idEntidadFinanciera, int idMoneda, int idMonedaConversion);
    }
}
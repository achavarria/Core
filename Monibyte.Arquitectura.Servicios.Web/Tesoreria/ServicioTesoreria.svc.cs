﻿using Monibyte.Arquitectura.Aplicacion.Tesoreria;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Tesoreria;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Tesoreria
{
    public class ServicioTesoreria : ServicioUnity, IServicioTesoreria
    {
        private IAppTesoreria _appTesoreria;
        private IAppOpTipoCambio _appOpTipoCambio;

        public ServicioTesoreria(IAppTesoreria tesoreria, IAppOpTipoCambio tipoCambio)
        {
            _appTesoreria = tesoreria;
            _appOpTipoCambio = tipoCambio;
        }

        public Transporte ConvierteMonto(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocMontoConvertir>();
            var resultado = _appTesoreria.ConvierteMonto(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaMonedas(Transporte transporte)
        {
            var resultado = _appTesoreria.ListaMonedas();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtieneTipoCambioBCCR(Transporte transporte)
        {
            bool esCompra = transporte.GetValor<bool>("ESCOMPRA");
            DateTime fecCambio = transporte.GetValor<DateTime>("FECCAMBIO");
            var resultado = _appTesoreria.ObtieneCambioBCCR(fecCambio, esCompra);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte InsertarTipoCambio(Transporte transporte)
        {
            var pocTipoCambio = transporte.GetDatos<PocTipoCambio>();
            _appOpTipoCambio.Insertar(pocTipoCambio);

            return transporte;
        }

        public Transporte ListarTipoCambio(Transporte transporte)
        {
            var filtro = transporte.GetDatos<PocTipoCambio>();
            var request = transporte.GetDataRequest();
            transporte.SetDatos(_appOpTipoCambio.Listar(filtro, request));
            return transporte;
        }

        public Transporte ObtenerTipoDeCambio(Transporte transporte)
        {
            var filtro = transporte.GetDatos<PocTipoCambio>();
            transporte.SetDatos(_appOpTipoCambio.ObtenerTipoDeCambio(filtro));
            return transporte;
        }
        public List<PocTipoCambio> ObtenerTipoCambioActual(PocTipoCambio filtro)
        {
            var app = GestorUnity.CargarUnidad<IAppOpTipoCambio>();
            return app.Obtener(filtro);
        }

        public decimal ObtienerUltimoTipoCambio(int IdTipoCambio, System.DateTime FecCambio, int IdMoneda, int? IdEntidad = null)
        {
            var app = GestorUnity.CargarUnidad<IAppTesoreria>();
            return app.ObtieneUltimoTipoCambio(IdTipoCambio, FecCambio, IdMoneda, IdEntidad);
        }

        public decimal ConvierteMontoHistorico(int idMonedaOrigen, decimal monto, int idMonedaDestino,
             System.DateTime fecReferencia, int idTipoCambio, int? idEntidadFinanciera = null)
        {
            if (idTipoCambio == 0)
                idTipoCambio = (int)EnumIdTipoDeCambio.Compra;

            var app = GestorUnity.CargarUnidad<IAppTesoreria>();
            return app.ConvertirMontoHistorico(idMonedaOrigen, monto, idMonedaDestino, fecReferencia, idEntidadFinanciera, idTipoCambio);
        }
    }
}

using System;

namespace Monibyte.Arquitectura.Dominio.Configuracion
{
    public partial class MB_RangoCategoriaTipoComercio : EntidadBase
    {
        public int IdRango { get; set; }
        public int IdCategoriaMcc { get; set; }
        public string CodMccDesde { get; set; }
        public string CodMccHasta { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

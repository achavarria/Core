using Monibyte.Arquitectura.Dominio.Cargos;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Cargos
{
    public class CA_TipoCargoMap : EntityTypeConfiguration<CA_TipoCargo>
    {
        public CA_TipoCargoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipoCargo);

            // Properties
            this.Property(t => t.IdTipoCargo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("CA_TipoCargo", "Cargos");
            this.Property(t => t.IdTipoCargo).HasColumnName("IdTipoCargo");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.IdOrigenCargo).HasColumnName("IdOrigenCargo");
            this.Property(t => t.IdTipoCobro).HasColumnName("IdTipoCobro");
            this.Property(t => t.MonCargo).HasColumnName("MonCargo");
            this.Property(t => t.IdTablaRangos).HasColumnName("IdTablaRangos");
            this.Property(t => t.IdPeriodicidad).HasColumnName("IdPeriodicidad");
            this.Property(t => t.ValPeriodicidad).HasColumnName("ValPeriodicidad");
            this.Property(t => t.IdMovimientoTC).HasColumnName("IdMovimientoTC");
            this.Property(t => t.IdMovReversaTC).HasColumnName("IdMovReversaTC");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdMonedaCobro).HasColumnName("IdMonedaCobro");

        }
    }
}

using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_BitacoraCuentaTarjetaMap : EntityTypeConfiguration<TC_BitacoraCuentaTarjeta>
    {
        public TC_BitacoraCuentaTarjetaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdBitacora);

            // Properties
            this.Property(t => t.NumCuenta)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.CuentaClienteLocal)
                .HasMaxLength(20);

            this.Property(t => t.CuentaClienteInternacional)
                .HasMaxLength(20);

            this.Property(t => t.IdBitacora)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NombreImpreso)
                .HasMaxLength(60);

            this.Property(t => t.NumTarjeta)
                .HasMaxLength(20);

            this.Property(t => t.NumPoliza)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("TC_BitacoraCuentaTarjeta", "TCredito");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.NumCuenta).HasColumnName("NumCuenta");
            this.Property(t => t.IdTipoTarjeta).HasColumnName("IdTipoTarjeta");
            this.Property(t => t.FechaVencimiento).HasColumnName("FechaVencimiento");
            this.Property(t => t.FecApertura).HasColumnName("FecApertura");
            this.Property(t => t.FecActivacion).HasColumnName("FecActivacion");
            this.Property(t => t.FecBloqueo).HasColumnName("FecBloqueo");
            this.Property(t => t.IdUsuarioBloqueo).HasColumnName("IdUsuarioBloqueo");
            this.Property(t => t.IdUsuarioActivacion).HasColumnName("IdUsuarioActivacion");
            this.Property(t => t.IdUsuarioApertura).HasColumnName("IdUsuarioApertura");
            this.Property(t => t.SaldoLocal).HasColumnName("SaldoLocal");
            this.Property(t => t.IdEstadoCta).HasColumnName("IdEstadoCta");
            this.Property(t => t.IdEnvioEstCuenta).HasColumnName("IdEnvioEstCuenta");
            this.Property(t => t.CuentaClienteLocal).HasColumnName("CuentaClienteLocal");
            this.Property(t => t.PorcAvanceEfectivo).HasColumnName("PorcAvanceEfectivo");
            this.Property(t => t.SaldoInternacional).HasColumnName("SaldoInternacional");
            this.Property(t => t.DispAvanceEfectivoInter).HasColumnName("DispAvanceEfectivoInter");
            this.Property(t => t.MillasAcumuladas).HasColumnName("MillasAcumuladas");
            this.Property(t => t.DiasAtrasoLocal).HasColumnName("DiasAtrasoLocal");
            this.Property(t => t.IdDiaCorte).HasColumnName("IdDiaCorte");
            this.Property(t => t.DiasAtrasoInternacional).HasColumnName("DiasAtrasoInternacional");
            this.Property(t => t.PagoMinLocal).HasColumnName("PagoMinLocal");
            this.Property(t => t.PagoMinInternacional).HasColumnName("PagoMinInternacional");
            this.Property(t => t.PagoContadoLocal).HasColumnName("PagoContadoLocal");
            this.Property(t => t.PagoContadoInternacional).HasColumnName("PagoContadoInternacional");
            this.Property(t => t.FecUltPagoLocal).HasColumnName("FecUltPagoLocal");
            this.Property(t => t.FecUltPagoInternacional).HasColumnName("FecUltPagoInternacional");
            this.Property(t => t.FecPagoContado).HasColumnName("FecPagoContado");
            this.Property(t => t.FecPagoMinimo).HasColumnName("FecPagoMinimo");
            this.Property(t => t.CuentaClienteInternacional).HasColumnName("CuentaClienteInternacional");
            this.Property(t => t.TasaInteresLocal).HasColumnName("TasaInteresLocal");
            this.Property(t => t.TasaInteresInternacional).HasColumnName("TasaInteresInternacional");
            this.Property(t => t.TasaIntMoraLocal).HasColumnName("TasaIntMoraLocal");
            this.Property(t => t.TasaIntMoraInternacional).HasColumnName("TasaIntMoraInternacional");
            this.Property(t => t.InteresMoratorioLocal).HasColumnName("InteresMoratorioLocal");
            this.Property(t => t.InteresMoratorioInternacional).HasColumnName("InteresMoratorioInternacional");
            this.Property(t => t.TasaIntCJLocal).HasColumnName("TasaIntCJLocal");
            this.Property(t => t.TasaIntCJInternacional).HasColumnName("TasaIntCJInternacional");
            this.Property(t => t.IdCalificacionRiesgo).HasColumnName("IdCalificacionRiesgo");
            this.Property(t => t.PlazoMeses).HasColumnName("PlazoMeses");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.IdBitacora).HasColumnName("IdBitacora");
            this.Property(t => t.IdTipoPlastico).HasColumnName("IdTipoPlastico");
            this.Property(t => t.NombreImpreso).HasColumnName("NombreImpreso");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.IdRelacion).HasColumnName("IdRelacion");
            this.Property(t => t.LimiteCreditoInter).HasColumnName("LimiteCreditoInter");
            this.Property(t => t.IdEstadoTarjeta).HasColumnName("IdEstadoTarjeta");
            this.Property(t => t.FecEntrega).HasColumnName("FecEntrega");
            this.Property(t => t.IdUsuarioEntrega).HasColumnName("IdUsuarioEntrega");
            this.Property(t => t.FecRenovacion).HasColumnName("FecRenovacion");
            this.Property(t => t.IdUsuarioRenueva).HasColumnName("IdUsuarioRenueva");
            this.Property(t => t.FecReposicion).HasColumnName("FecReposicion");
            this.Property(t => t.IdUsuarioRepone).HasColumnName("IdUsuarioRepone");
            this.Property(t => t.IdTarjetaAnterior).HasColumnName("IdTarjetaAnterior");
            this.Property(t => t.IdTipoReposicion).HasColumnName("IdTipoReposicion");
            this.Property(t => t.MonDisponibleLocal).HasColumnName("MonDisponibleLocal");
            this.Property(t => t.MonDisponibleInter).HasColumnName("MonDisponibleInter");
            this.Property(t => t.MonCargosBonifLocal).HasColumnName("MonCargosBonifLocal");
            this.Property(t => t.MonCargosBonifInter).HasColumnName("MonCargosBonifInter");
            this.Property(t => t.MonCreditosLocal).HasColumnName("MonCreditosLocal");
            this.Property(t => t.MonCreditosInter).HasColumnName("MonCreditosInter");
            this.Property(t => t.SaldoTotalCorteLocal).HasColumnName("SaldoTotalCorteLocal");
            this.Property(t => t.SaldoTotalCorteInter).HasColumnName("SaldoTotalCorteInter");
            this.Property(t => t.DisponibleLocalTarjeta).HasColumnName("DisponibleLocalTarjeta");
            this.Property(t => t.DisponibleInterTarjeta).HasColumnName("DisponibleInterTarjeta");
            this.Property(t => t.DispAdelantoLocalTarjeta).HasColumnName("DispAdelantoLocalTarjeta");
            this.Property(t => t.DispAdelantoInterTarjeta).HasColumnName("DispAdelantoInterTarjeta");
            this.Property(t => t.CantImpVencidosLocal).HasColumnName("CantImpVencidosLocal");
            this.Property(t => t.CantImpVencidosInter).HasColumnName("CantImpVencidosInter");
            this.Property(t => t.MonImpVencidosLocal).HasColumnName("MonImpVencidosLocal");
            this.Property(t => t.MonImpVencidosInter).HasColumnName("MonImpVencidosInter");
            this.Property(t => t.DebitosTransitoLocal).HasColumnName("DebitosTransitoLocal");
            this.Property(t => t.DebitosTransitoInter).HasColumnName("DebitosTransitoInter");
            this.Property(t => t.CreditosTransitoLocal).HasColumnName("CreditosTransitoLocal");
            this.Property(t => t.CreditosTransitoInter).HasColumnName("CreditosTransitoInter");
            this.Property(t => t.LimiteExtraFinLocal).HasColumnName("LimiteExtraFinLocal");
            this.Property(t => t.LimiteExtraFinInter).HasColumnName("LimiteExtraFinInter");
            this.Property(t => t.SaldoExtraFinLocal).HasColumnName("SaldoExtraFinLocal");
            this.Property(t => t.SaldoExtraFinInter).HasColumnName("SaldoExtraFinInter");
            this.Property(t => t.MonDispExtraFinLocal).HasColumnName("MonDispExtraFinLocal");
            this.Property(t => t.MonDispExtraFinInter).HasColumnName("MonDispExtraFinInter");
            this.Property(t => t.FecUltCorte).HasColumnName("FecUltCorte");
            this.Property(t => t.IdTipoPoliza).HasColumnName("IdTipoPoliza");
            this.Property(t => t.NumPoliza).HasColumnName("NumPoliza");
            this.Property(t => t.LimiteCreditoLocal).HasColumnName("LimiteCreditoLocal");
            this.Property(t => t.DispAvanceEfectivoLocal).HasColumnName("DispAvanceEfectivoLocal");
            this.Property(t => t.IdGrupoEmpresa).HasColumnName("IdGrupoEmpresa");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
        }
    }
}

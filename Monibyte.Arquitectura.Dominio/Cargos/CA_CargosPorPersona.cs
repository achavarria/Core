using System;

namespace Monibyte.Arquitectura.Dominio.Cargos
{
    public partial class CA_CargosPorPersona : EntidadBase
    {
        public int IdCargo { get; set; }
        public int IdPersona { get; set; }
        public int IdTipoCargo { get; set; }
        public System.DateTime FecInicio { get; set; }
        public Nullable<System.DateTime> FecFinal { get; set; }
        public Nullable<int> IdCuentaCobro { get; set; }
        public Nullable<int> IdTipoCobro { get; set; }
        public Nullable<decimal> MonCargo { get; set; }
        public Nullable<int> IdTablaRangos { get; set; }
        public Nullable<int> IdPeriodicidad { get; set; }
        public Nullable<int> ValPeriodicidad { get; set; }
        public Nullable<System.DateTime> FecUltCargo { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public Nullable<int> IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdEstadoCobro { get; set; }
    }
}

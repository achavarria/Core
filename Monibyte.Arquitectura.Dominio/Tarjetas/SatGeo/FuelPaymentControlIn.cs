﻿
namespace Monibyte.Arquitectura.Dominio.Tarjetas.SatGeo
{
    public partial class SatgeoIn
    {
        public int TransactionID { get; set; }
        public string CardNumber { get; set; }
        public string CurrencyType { get; set; }
        public string Date { get; set; }
        public string Detail { get; set; }
        public string NameCardHolder { get; set; }
        public string Plate { get; set; }
        public string Value { get; set; }
    }
}


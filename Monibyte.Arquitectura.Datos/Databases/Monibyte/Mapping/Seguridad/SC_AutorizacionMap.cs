using Monibyte.Arquitectura.Dominio.Seguridad;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Seguridad
{
    public class SC_AutorizacionMap : EntityTypeConfiguration<SC_Autorizacion>
    {
        public SC_AutorizacionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdAutorizacion);

            // Properties
            this.Property(t => t.IdAutorizacion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumReferencia)
                .HasMaxLength(30);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Observaciones)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("SC_Autorizacion", "Seguridad");
            this.Property(t => t.IdAutorizacion).HasColumnName("IdAutorizacion");
            this.Property(t => t.IdTipoAutorizacion).HasColumnName("IdTipoAutorizacion");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.NumReferencia).HasColumnName("NumReferencia");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdUsuarioPrepara).HasColumnName("IdUsuarioPrepara");
            this.Property(t => t.FecPrepara).HasColumnName("FecPrepara");
            this.Property(t => t.IdUsuarioAutoriza).HasColumnName("IdUsuarioAutoriza");
            this.Property(t => t.FecAutoriza).HasColumnName("FecAutoriza");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.DetalleAutorizacion).HasColumnName("DetalleAutorizacion");
            this.Property(t => t.Observaciones).HasColumnName("Observaciones");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

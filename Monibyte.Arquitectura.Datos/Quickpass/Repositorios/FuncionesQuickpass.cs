﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Dominio.Quickpass.Repositorios;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Datos.Quickpass.Repositorios
{
    public class FuncionesQuickpass : RepositorioBase, IFuncionesQuickpass
    {
        ISql _unidadTbjo;
        public FuncionesQuickpass(IUnidadTbjoMonibyte unidadTbjo)
            : base(unidadTbjo)
        {
            _unidadTbjo = unidadTbjo;
        }

        public IEnumerable<QP_CorteQuickpas> GenerarcargosCorteQuickpass()
        {
            return _unidadTbjo.ExecuteQuery<QP_CorteQuickpas>
                (@"Quickpass.P_GenerarcargosCorteQuickpass");
        }
        public IEnumerable<QP_MovimientosQuickpass> GenerarCargosMovimientosQuickpass()
        {
            return _unidadTbjo.ExecuteQuery<QP_MovimientosQuickpass>
                (@"Quickpass.P_GenerarCargosMovimientosQuickpass");
        }
    }
}

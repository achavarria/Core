using Monibyte.Arquitectura.Dominio.Seguridad;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Seguridad
{
    public class SC_AccionMap : EntityTypeConfiguration<SC_Accion>
    {
        public SC_AccionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdAccion);

            // Properties
            this.Property(t => t.IdAccion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Controlador)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Area)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("SC_Accion", "Seguridad");
            this.Property(t => t.IdAccion).HasColumnName("IdAccion");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.Controlador).HasColumnName("Controlador");
            this.Property(t => t.Area).HasColumnName("Area");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdConsulta).HasColumnName("IdConsulta");
        }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Clientes;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Seguridad;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Monibyte.Arquitectura.Servicios.Web.Generales
{
    [ServiceContract]
    public interface IMembresia : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ActualizarPerfil",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        void ActualizarPerfil(PocInfoUsuario infoUsuario);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ObtenerInfoPerfil",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        PocInfoUsuario ObtenerInfoPerfil(string codUsuario);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RegistrarPerfil",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        void RegistrarPerfil(PocRegistroUsuario infoUsuario);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ActualizarContrasena",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        void ActualizarContrasena(PocCambioContrasena datos);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ObtenerEmpresasAsociadas",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        IEnumerable<PocEmpresasUsuario> ObtenerEmpresasAsociadas(string codUsuario);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "FinalizarSesion",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        void FinalizarSesion(string ticket);


        #region Servicios de mantenimiento de usuarios

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "IncluirUsuario",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte IncluirUsuario(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ListarUsuarios",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte ListarUsuarios(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "EliminaUsuario",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte EliminaUsuario(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ModificaUsuario",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte ModificaUsuario(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ModificaCodUsuarioEmpresa",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte ModificaCodUsuarioEmpresa(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AsociarEmpresasUsuario",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte AsociarEmpresasUsuario(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ListarUsuariosConectados",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte ListarUsuariosConectados(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CambiosSeguridad",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte CambiosSeguridad(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CambiarEstadoUsuario",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte CambiarEstadoUsuario(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "InsertaNotificaUsuario",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte InsertaNotificaUsuario(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CambiarRolesOperativos",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Transporte CambiarRolesOperativos(Transporte transporte);

        #endregion

    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Seguridad;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Encryption;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Clientes;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Generales.Mantenimientos
{
    public class AppOpUsuario : AplicacionBase, IAppOpUsuario
    {
        #region CONSTRUCTOR UNITY
        private IRepositorioUsuario _repUsuario;
        private IRepositorioCatalogo _repCatalogo;
        private IDirectorioUsuarioApp _directorioUsr;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<CL_PerfilEmpresa> _repPerfilEmpresa;
        private IRepositorioMnb<CL_EmpresasUsuario> _repEmpresasUsuario;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _vwEmpresasUsuario;
        private IRepositorioMnb<GL_Entidad> _repEntidad;

        public AppOpUsuario(
            IRepositorioUsuario repUsuario,
            IRepositorioCatalogo repCatalogo,
            IDirectorioUsuarioApp directorioUsr,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<CL_PerfilEmpresa> repPerfilEmpresa,
            IRepositorioMnb<CL_EmpresasUsuario> repEmpresasUsuario,
            IRepositorioMnb<CL_vwEmpresasUsuario> vwEmpresasUsuario,
            IRepositorioMnb<GL_Entidad> repEntidad)
        {
            _repUsuario = repUsuario;
            _repCatalogo = repCatalogo;
            _directorioUsr = directorioUsr;
            _repPersona = repPersona;
            _repPerfilEmpresa = repPerfilEmpresa;
            _repEmpresasUsuario = repEmpresasUsuario;
            _vwEmpresasUsuario = vwEmpresasUsuario;
            _repEntidad = repEntidad;
        }
        #endregion

        public void Insertar(PocUsuario registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    if (registro.IdEmpresa <= 0)
                    {
                        registro.IdEmpresa = registro.IdPersona;
                    }
                    var idRole = registro.IdRole;

                    if (registro.EsInterno)
                    {
                        var entidad = _repEntidad.Table.FirstOrDefault(x => x.IdEntidad == registro.IdCompania);
                        if (entidad != null)
                        {
                            registro.IdEmpresa = entidad.IdPersona.Value;
                        }
                    }

                    registro.CodUsuario = _directorioUsr.IncluirCuenta(registro);
                    var nuevoReg = registro.ProyectarComo<GL_Usuario>();
                    nuevoReg.AliasUsuario = registro.CodUsuario;
                    nuevoReg.IdEstado = (int)EnumEstadoUsuario.Inactivo;
                    nuevoReg.IdUsuario = _fGenerales.ObtieneSgteSecuencia("SEQ_IdUsuario");
                    using (var ts1 = new TransactionScope())
                    {
                        _repUsuario.Insert(nuevoReg);
                        _repUsuario.UnidadTbjo.Save();

                        var datosEmpresa = new CL_EmpresasUsuario
                        {
                            IdUsuario = nuevoReg.IdUsuario,
                            IdEmpresa = registro.IdEmpresa,
                            IdTipoUsuario = registro.IdTipoUsuario,
                            IdRole = registro.IdRole,
                            IdDefault = (int)EnumIdSiNo.Si
                        };
                        _repEmpresasUsuario.Insert(datosEmpresa);
                        _repEmpresasUsuario.UnidadTbjo.Save();
                        ts1.Complete();
                    }
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocUsuario));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void InsertaNotificaUsuario(PocUsuario registro)
        {
            Insertar(registro);
            //Cambiar estado del Usuario Inactivo a Registrado
            _directorioUsr.CambiarEstado(registro.CodUsuario,
                (int)EnumEstadoUsuario.Registrado);
            //Reestablece la contrase;a y notifica al usuario por e-mail
            //_repDirectorioUsuarios.ReestablecerContrasena(codUsuario, true);
        }

        public void Eliminar(PocUsuario registro)
        {
            using (var ts = new TransactionScope())
            {
                var validation = new ValidationResponse();
                var codUsuario = registro.CodUsuario;
                var usuario = _repUsuario.ObtenerUsuarioPorCodigo(codUsuario);
                if (usuario != null)
                {
                    var usrGlobal = _directorioUsr.ObtenerCuenta(registro.CodUsuario);
                    usuario.IdEstado = usrGlobal != null ? usrGlobal.IdEstado : usuario.IdEstado;
                }
                if (usuario == null)
                {
                    validation.AddError("Err_0006");
                }
                else if (usuario.IdEstado == (int)EnumEstadoUsuario.Activo)
                {
                    validation.AddError("Err_0071");
                }
                if (!validation.IsValid)
                {
                    string detalle = string.Format("Errores al eliminar el usuario {0}", codUsuario);
                    throw new CoreException(detalle, validation: validation);
                }
                try
                {
                    _repUsuario.DeleteOn(p => p.IdUsuario == usuario.IdUsuario);
                }
                catch (Exception ex)
                {
                    throw new CoreException("Imposible Eliminar Usuario", "Err_0072", inner: ex);
                }
                _repUsuario.UnidadTbjo.Save();
                _directorioUsr.EliminarCuenta(codUsuario);
                ts.Complete();
            }
        }

        public DataResult<PocUsuario> Listar
            (PocUsuario filtro, DataRequest request)
        {
            var resultado = ObtenerUsuarios(filtro);
            return resultado.ToDataResult(request);
        }

        public IEnumerable<PocUsuario> Listar(PocUsuario filtro)
        {
            return ObtenerUsuarios(filtro);
        }

        private IEnumerable<PocUsuario> ObtenerUsuarios(PocUsuario filtro)
        {
            var critTipoUsr = new Criteria<GL_Catalogo>();
            critTipoUsr.And(u => u.Lista == "LTIPOUSUARIO");
            var critUsuario = new Criteria<CL_vwEmpresasUsuario>();
            var tipoU = Context.UserType;

            if (!tipoU.MultiEntidad)
            {
                var compania = InfoSesion.Info.IdCompania;
                filtro.IdCompania = compania;
                critUsuario.And(m => m.IdCompania == compania);
                if (tipoU.PorUnidad)
                {
                    var sucursal = InfoSesion.Info.IdUnidad;
                    filtro.IdUnidad = sucursal;
                    critUsuario.And(m => m.IdUnidad == sucursal);
                }
            }
            if (filtro != null)
            {
                if (filtro.IdEmpresa > 0)
                {
                    critUsuario.And(m => m.IdEmpresa == filtro.IdEmpresa);
                }
                if (filtro.IdRoleOperativo.HasValue)
                {
                    critUsuario.And(m => m.IdRoleOperativo == filtro.IdRoleOperativo);
                }
                if (filtro.IdTipoUsuario > 0)
                {
                    critUsuario.And(m => m.IdTipoUsuario == filtro.IdTipoUsuario);
                }
                if (!string.IsNullOrEmpty(filtro.Alias))
                {
                    critUsuario.And(x => x.AliasUsuario.Contains(filtro.Alias));
                    critUsuario.Or(x => x.CodUsuario.Contains(filtro.Alias));
                }
                if (!string.IsNullOrEmpty(filtro.Ref4TipoUsuario))
                {
                    critTipoUsr.And(x => !string.IsNullOrEmpty(x.Referencia4) &&
                        x.Referencia4.Contains(filtro.Ref4TipoUsuario));
                }
                if (InfoSesion.Info.EsInterno)
                {
                    critUsuario.And(x => x.IdDefault == (int)EnumIdSiNo.Si);
                }
            }
            var usrGb = _directorioUsr.ObtenerUsuarios(filtro.Alias);
            var query =
                from usr in _vwEmpresasUsuario.SelectBy(critUsuario)
                join per in _repPersona.Table on
                  usr.IdPersona equals per.IdPersona
                join emp in _repPersona.Table on
                  usr.IdEmpresa equals emp.IdPersona
                join tip in _repCatalogo.List(critTipoUsr) on
                    usr.IdTipoUsuario equals tip.IdCatalogo
                where (filtro.IdPersona <= 0 || per.IdPersona == filtro.IdPersona ||
                        (filtro.MostrarAdicionales == (int)EnumIdSiNo.Si &&
                        emp.IdPersona == filtro.IdPersona)) &&
                    (string.IsNullOrEmpty(filtro.NombreCompleto) ||
                        per.NombreCompleto.ToLower().Contains(filtro.NombreCompleto.ToLower()) ||
                        (filtro.MostrarAdicionales == (int)EnumIdSiNo.Si &&
                        emp.NombreCompleto.ToLower().Contains(filtro.NombreCompleto.ToLower()))) &&
                    (string.IsNullOrEmpty(filtro.NumIdentificacion) ||
                        per.NumIdentificacion.ToLower().Contains(filtro.NumIdentificacion.ToLower()) ||
                        (filtro.MostrarAdicionales == (int)EnumIdSiNo.Si &&
                        emp.NumIdentificacion.ToLower().Contains(filtro.NumIdentificacion.ToLower())))
                select new { usr, per, emp, tip };
            var resultado = query.ToList()
                .Join(usrGb, tmp => tmp.usr.CodUsuario, gb => gb.CodUsuario,
                    (tmp, gb) => new { tmp.usr, tmp.per, tmp.emp, tmp.tip, gb })
                    .Join(_repCatalogo.Table,
                    tmp => tmp.gb.IdEstado,
                    est => est.IdCatalogo, (tmp, est) =>
                    new { tmp.usr, tmp.per, tmp.emp, tmp.tip, tmp.gb, est })
                .Select(tmp => new PocUsuario
                {
                    IdUsuario = tmp.usr.IdUsuario,
                    IdEjecutivo = tmp.per.IdEjecutivo,
                    CodUsuario = tmp.usr.CodUsuario,
                    Alias = tmp.usr.AliasUsuario,
                    IdPersona = tmp.per.IdPersona,
                    Correo = tmp.gb.Correo,
                    NumIntentosFallidos = tmp.gb.NumIntentosFallidos.HasValue ?
                        tmp.gb.NumIntentosFallidos.Value : 0,
                    IdPaisPreferencia = tmp.gb.IdPais.Value,
                    IdIdiomaPreferencia = tmp.gb.IdIdioma.Value,
                    NombreCompleto = tmp.per.NombreCompleto,
                    NumIdentificacion = tmp.per.NumIdentificacion,
                    TipoUsuario = tmp.tip.Descripcion,
                    IdTipoUsuario = (int)tmp.usr.IdTipoUsuario,
                    IdCompania = tmp.usr.IdCompania,
                    IdUnidad = tmp.usr.IdUnidad,
                    Estado = tmp.est.Descripcion,
                    IdEmpresa = tmp.usr.IdEmpresa,
                    IdEstado = tmp.gb.IdEstado,
                    TipoPersona = tmp.per.IdTipoPersona,
                    NombreEmpresa = tmp.emp.NombreCompleto,
                    NumIdentificacionEmpresa = tmp.emp.NumIdentificacion,
                    IdRequiereOTP = tmp.gb.RequiereOtp ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No,
                    IdRoleOperativo = tmp.usr.IdRoleOperativo,
                    IdRole = tmp.usr.IdRole,
                    EsInterno = tmp.gb.EsInterno
                });
            return resultado;
        }

        public void Modificar(PocUsuario registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var idRole = registro.IdRole;
                    if (registro.EsInterno)
                    {
                        var entidad = _repEntidad.Table.FirstOrDefault(x => x.IdPersona == registro.IdCompania);
                        if (entidad != null)
                        {
                            registro.IdEmpresa = entidad.IdPersona.Value;
                        }
                    }

                    _repEmpresasUsuario.UpdateOn(x => x.IdEmpresa != registro.IdEmpresa &&
                        x.IdUsuario == registro.IdUsuario, z => new CL_EmpresasUsuario
                        {
                            IdDefault = (int)EnumIdSiNo.No
                        });

                    var existente = _repEmpresasUsuario.SelectById(registro.IdEmpresa, registro.IdUsuario);
                    if (existente != null)
                    {
                        existente.IdDefault = (int)EnumIdSiNo.Si;
                        existente.IdRole = registro.IdRole;
                        existente.IdTipoUsuario = registro.IdTipoUsuario;
                        _repEmpresasUsuario.Update(existente);
                    }
                    else
                    {
                        var datosEmpresa = new CL_EmpresasUsuario
                        {
                            IdUsuario = registro.IdUsuario,
                            IdEmpresa = registro.IdEmpresa,
                            IdRole = registro.IdRole,
                            IdTipoUsuario = registro.IdTipoUsuario,
                            IdDefault = (int)EnumIdSiNo.Si
                        };
                        _repEmpresasUsuario.Insert(datosEmpresa);
                    }
                    _repUsuario.UpdateOn(x => x.IdUsuario == registro.IdUsuario,
                        p => new GL_Usuario
                        {
                            IdCompania = registro.IdCompania,
                            IdUnidad = registro.IdUnidad,
                            Correo = registro.Correo,
                            IdIdiomaPreferencia = registro.IdIdiomaPreferencia
                        });
                    if (registro.IdEjecutivo.HasValue)
                    {
                        _repPersona.UpdateOn(x => x.IdPersona == registro.IdPersona,
                            z => new CL_Persona { IdEjecutivo = registro.IdEjecutivo });
                        _repPersona.UnidadTbjo.Save();
                    }
                    _repEmpresasUsuario.UnidadTbjo.Save();
                    _directorioUsr.ModificarCuenta(registro);
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocUsuario));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void CambiosSeguridad(PocSeguridadUsuario condiciones)
        {
            using (var ts = new TransactionScope())
            {
                var usuario = _directorioUsr.ObtenerUsuarios(condiciones.CodUsuario).FirstOrDefault();
                if (!string.IsNullOrEmpty(condiciones.Contrasena))
                {
                    _directorioUsr.ActualizarContrasena(new PocCambioContrasena
                    {
                        CodUsuario = condiciones.CodUsuario,
                        Contrasena = condiciones.Contrasena.Encrypt(),
                        DiasVigencia = condiciones.RenovarContraseña ? 0 : 30
                    });
                }
                if (condiciones.InicializarContadores)
                {
                    _directorioUsr.ReiniciarIntentos(condiciones.CodUsuario);
                }
                if (condiciones.RenovarContraseña && usuario.IdEstado
                    != (int)EnumEstadoUsuario.Registrado)
                {
                    _directorioUsr.RenovarContrasena(condiciones.CodUsuario);
                }
                if (condiciones.BloquearUsuario && usuario.IdEstado
                    != (int)EnumEstadoUsuario.TemporalmenteBloqueado)
                {
                    _directorioUsr.CambiarEstado(usuario.CodUsuario,
                        (int)EnumEstadoUsuario.TemporalmenteBloqueado);
                }
                else if (usuario.IdEstado == (int)EnumEstadoUsuario.TemporalmenteBloqueado)
                {
                    string urlHref = RestConfig.Get("ApiCore", "Membresia", "ObtenerCuenta");
                    var info = RestClient.CoreRequest<PocInfoSesion>(urlHref, usuario.CodUsuario);

                    var estado = (int)EnumEstadoUsuario.Activo;
                    if (!info.EsInterno)
                    {
                        var url = RestConfig.Get("ApiCore", "Membresia", "ObtenerPreguntasUsuario");
                        var preguntas = RestClient.CoreRequest<List<PocPreguntaUsuario>>(url, usuario.CodUsuario);
                        var registro = preguntas != null && preguntas.Any();
                        estado = registro ? (int)EnumEstadoUsuario.Activo : (int)EnumEstadoUsuario.Inactivo;
                    }
                    _directorioUsr.CambiarEstado(usuario.CodUsuario, estado);
                    _repUsuario.UpdateOn(x => x.CodUsuario == condiciones.CodUsuario,
                        z => new GL_Usuario
                        {
                            IdEstado = estado
                        });
                }
                _repUsuario.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        private ValidationResponse Validar(PocUsuario registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (inserta)
            {
                if (registro.Correo.Length <= 0) { validacion.AddError("Err_0008"); }
                if (registro.IdIdiomaPreferencia <= 0) { validacion.AddError("Err_0009"); }
                if (registro.IdPaisPreferencia <= 0) { validacion.AddError("Err_0010"); }
                if (registro.IdPersona <= 0) { validacion.AddError("Err_0021"); }
                if (registro.IdTipoUsuario <= 0) { validacion.AddError("Err_0040"); }
                if (registro.IdRole <= 0) { validacion.AddError("Err_0042"); }
                if (registro.IdEmpresa == 0)
                {
                    registro.IdEmpresa = registro.IdPersona;
                }
                var userExist = _vwEmpresasUsuario.Table
                   .Where(x => x.IdPersona == registro.IdPersona &&
                                        x.IdEmpresa == registro.IdEmpresa).ToList();
                if (userExist != null && userExist.Any())
                {
                    foreach (var us in userExist)
                    {
                        var userGB = _directorioUsr.ObtenerCuenta(us.CodUsuario);
                        if (userGB.EsInterno == registro.EsInterno)
                        {
                            validacion.AddError("Err_0043");
                        }
                    }
                }
            }
            else
            {
                if (registro.IdUsuario <= 0) { validacion.AddError("Err_0006"); }
            }
            if (string.IsNullOrEmpty(registro.NumIdentificacion)) { validacion.AddError("Err_0039"); }
            return validacion;
        }

        public void ModificaCodUsuarioEmpresa(IEnumerable<PocUsuario> usuarios)
        {
            using (var ts = new TransactionScope())
            {
                foreach (PocUsuario usuario in usuarios)
                {
                    var entrada = usuario.ProyectarComo<GL_Usuario>();
                    //INCLUIR EL MODIFICAR CUENTA GLOBAL
                    _repUsuario.Update(entrada, x => x.CodUsuarioEmpresa);
                }
                _repUsuario.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void AsociarEmpresasUsuario(string codUsuario, List<PocEmpresasUsuario> modelo,
            List<PocEmpresasUsuario> modeloPrevio)
        {
            modeloPrevio = modeloPrevio ?? new List<PocEmpresasUsuario>();
            using (var ts = new TransactionScope())
            {
                var usr = _repUsuario.SelectById(modelo.First().IdUsuario);

                foreach (var item in modelo)
                {
                    item.IdDefault = item.IdDefault > 0 ? item.IdDefault : (int)EnumIdSiNo.No;
                    //busca un item similar en la configuracion previa
                    var itemPrevio = modeloPrevio.FirstOrDefault(m => m.IdEmpresa == item.IdEmpresa &&
                        m.IdUsuario == item.IdUsuario);
                    if (itemPrevio == null)
                    {
                        if (_vwEmpresasUsuario.Table.Any(x =>
                            x.IdPersona == usr.IdPersona &&
                            x.IdEmpresa == item.IdEmpresa))
                        {
                            throw new CoreException("msj", "Err_0043");
                        }
                        _repEmpresasUsuario.Insert(item.ProyectarComo<CL_EmpresasUsuario>());
                    }
                    else if (itemPrevio != null && !item.Equals(itemPrevio))
                    {
                        _repEmpresasUsuario.Update(item.ProyectarComo<CL_EmpresasUsuario>(),
                            m => m.IdRole, m => m.IdTipoUsuario, m => m.IdDefault);
                    }
                }
                if (modeloPrevio != null)
                {
                    foreach (var itemPrevio in modeloPrevio)
                    {
                        var item = modelo.FirstOrDefault(m => m.IdEmpresa == itemPrevio.IdEmpresa &&
                        m.IdUsuario == itemPrevio.IdUsuario);
                        if (item == null)
                        {
                            _repEmpresasUsuario.Delete(itemPrevio.ProyectarComo<CL_EmpresasUsuario>());
                        }
                    }
                }
                _repEmpresasUsuario.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public List<PocEmpresasUsuario> ObtenerEmpresasAsociadas(string codUsuario)
        {
            var empresas =
                _vwEmpresasUsuario.Table
                .Join(_repUsuario.Table,
                    eu => eu.IdUsuario,
                    u => u.IdUsuario,
                    (eu, u) => new { eu, u })
                .Join(_repPersona.Table,
                    item => item.eu.IdEmpresa,
                    p => p.IdPersona,
                    (item, p) => new { item.eu, item.u, p })
                .GroupJoin(_repPerfilEmpresa.Table,
                    item => item.eu.IdEmpresa,
                    pf => pf.IdEmpresa,
                    (item, pf) => new { item.eu, item.u, item.p, pf = pf.FirstOrDefault() })
                .Where(x => x.eu.CodUsuario == codUsuario)
                .Select(x => new PocEmpresasUsuario
                {
                    IdUsuario = x.u.IdUsuario,
                    IdTipoUsuario = x.eu.IdTipoUsuario,
                    IdRole = x.eu.IdRole,
                    IdDefault = x.eu.IdDefault,
                    IdEmpresa = x.p.IdPersona,
                    IdRoleOperativo = x.eu.IdRoleOperativo,
                    IdRequiereAutorizacion = x.pf != null ? x.pf.IdRequiereAutorizacion :
                        x.p.IdTipoPersona == (int)EnumTipoPersona.Juridica ? (int)EnumIdSiNo.Si :
                        (int)EnumIdSiNo.No,
                    NombreEmpresa = x.p.NombreCompleto
                });
            return empresas.ToList();
        }

        public DataResult<PocUsuario> ListarUsuariosConectados(DataRequest request)
        {
            var conectados = _directorioUsr.ObtenerSesionesActivas();
            var resultado = conectados
                 .Join(_repUsuario.Table,
                    con => con.CodUsuario,
                    usr => usr.CodUsuario, (con, usr) => new { con, usr })
                .Join(_repPersona.Table,
                    tmp => tmp.usr.IdPersona,
                    per => per.IdPersona,
                    (tmp, persona) => new { tmp.usr, tmp.con, persona })
                .Join(_repCatalogo.List("LESTADOUSUARIO"),
                    item => item.usr.IdEstado,
                    list => list.IdCatalogo,
                    (tmp, estadoUsr) => new { tmp.usr, tmp.con, tmp.persona, estadoUsr })
                .Join(_repEmpresasUsuario.Table,
                    item => new { item.usr.IdUsuario, IdDefault = (int)EnumIdSiNo.Si },
                    eu => new { eu.IdUsuario, eu.IdDefault },
                    (item, eu) => new { item.usr, item.con, item.persona, item.estadoUsr, eu })
                .Join(_repPersona.Table,
                    item => item.eu.IdEmpresa,
                    emp => emp.IdPersona,
                    (item, emp) => new { item.usr, item.con, item.persona, item.estadoUsr, item.eu, emp })
                .Select(tmp => new PocUsuario
                {
                    IdUsuario = tmp.usr.IdUsuario,
                    CodUsuario = tmp.usr.CodUsuario,
                    Alias = tmp.usr.AliasUsuario,
                    IdPersona = tmp.persona.IdPersona,
                    NombreCompleto = tmp.persona.NombreCompleto,
                    NombreEmpresa = tmp.emp.NombreCompleto,
                    NumIdentificacion = tmp.persona.NumIdentificacion,
                    Estado = tmp.estadoUsr.Descripcion,
                    IdEstado = tmp.usr.IdEstado,
                    FecInicio = tmp.con.FecInicio,
                    TipoPersona = tmp.persona.IdTipoPersona,
                    TiempoTranscurrido = String.Format("{0}:{1}:{2}",
                        (int)(DateTime.Now - tmp.con.FecInicio).TotalHours,
                        DateTime.Now.Subtract(tmp.con.FecInicio).Minutes,
                        DateTime.Now.Subtract(tmp.con.FecInicio).Seconds)
                });
            return resultado.OrderBy(x => x.FecActualizaAud).ToDataResult(request);
        }

        public void CambiarRolesOperativos(List<PocUsuarioRoleOperativo> datosEntrada)
        {
            var idEmpresa = InfoSesion.Info.IdEmpresa;
            using (var ts = new TransactionScope())
            {
                foreach (var item in datosEntrada)
                {
                    _repEmpresasUsuario
                    .UpdateOn(x =>
                        x.IdEmpresa == idEmpresa &&
                        x.IdUsuario == item.IdUsuario,
                        z => new CL_EmpresasUsuario
                        {
                            IdRoleOperativo = item.IdRoleOperativo
                        });
                }
                _repEmpresasUsuario.UnidadTbjo.Save();
                ts.Complete();
            }
        }
    }
}


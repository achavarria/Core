﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Cargos;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public class AppRangosTabla : AplicacionBase, IAppRangosTabla
    {
        private IRepositorioMoneda _repMoneda;
        private IRepositorioMnb<CA_RangosTabla> _repRangosTabla;
        private IRepositorioMnb<CA_DetRangosCargo> _repDetRangosCargo;
        private IRepositorioMnb<CA_DetRangosCargosPer> _repDetRangosCargosPer;

        public AppRangosTabla(
            IRepositorioMoneda repMoneda,
            IRepositorioMnb<CA_RangosTabla> repRangosTabla,
            IRepositorioMnb<CA_DetRangosCargo> repDetRangosCargo,
            IRepositorioMnb<CA_DetRangosCargosPer> repDetRangosCargosPer)
        {
            _repMoneda = repMoneda;
            _repRangosTabla = repRangosTabla;
            _repDetRangosCargo = repDetRangosCargo;
            _repDetRangosCargosPer = repDetRangosCargosPer;
        }

        public void Eliminar(PocRangosTabla registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CA_RangosTabla>();
                _repDetRangosCargo.DeleteOn(det => det.IdTablaRangos == entrada.IdTablaRangos);
                _repRangosTabla.Delete(entrada);
                _repRangosTabla.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocRangosTabla registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_RangosTabla>();
                    entrada.IdTablaRangos = _fGenerales.ObtieneSgteSecuencia("SEQ_IdTablaRangos");
                    _repRangosTabla.Insert(entrada);
                    _repRangosTabla.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocRangosTabla));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocRangosTabla registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_RangosTabla>();
                    _repRangosTabla.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repRangosTabla.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocRangosTabla));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocRangosTabla> Listar(PocRangosTabla filtro, DataRequest request)
        {
            var criteria = new Criteria<CA_RangosTabla>();
            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.Descripcion))
                {
                    criteria.And(m => m.Descripcion.Contains(filtro.Descripcion));
                }
            }
            var resultado = _repRangosTabla
                .SelectBy(criteria)
                .Join(_repMoneda.Table,
                    r => r.IdMoneda,
                    m => m.IdMoneda,
                    (r, m) => new { r, m })
                .Select(item => new PocRangosTabla
                {
                    IdTablaRangos = item.r.IdTablaRangos,
                    Descripcion = item.r.Descripcion,
                    IdMoneda = item.r.IdMoneda,
                    DescMoneda = item.m.Descripcion
                });


            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocRangosTabla registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.Descripcion == "")
            {
                validacion.AddError("Err_0035");
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }

        public IEnumerable<PocRangosTabla> ListadoRangosTabla()
        {
            var parametros = _repRangosTabla.Table.ToList();
            return parametros.ProyectarComo<PocRangosTabla>();
        }

        //Carga de detalle de tabla de rangos dependiendo de si tiene valores en CA_DetRangosCargo o CA_DetRangosCargosPer
        public DataResult<PocDetalleCargos> ListarDetalleCargos(PocDetalleCargos filtro, DataRequest request)
        {
            var criteria = new Criteria<CA_DetRangosCargo>();
            var criteriaPer = new Criteria<CA_DetRangosCargosPer>();
            if (filtro != null)
            {
                if (filtro.IdTablaRangos >= 0)
                {
                    criteria.And(m => m.IdTablaRangos == filtro.IdTablaRangos);
                    criteriaPer.And(m => m.IdTablaRangos == filtro.IdTablaRangos);
                }
                criteriaPer.And(m => m.IdCargo == filtro.IdCargo);
            }

            var resultadoCargos = _repDetRangosCargo
                .SelectBy(criteria)
                .Select(item => new PocDetalleCargos
                {
                    IdTablaRangos = item.IdTablaRangos,
                    MonCargo = item.MonCargo,
                    RangoMax = item.RangoMax,
                    RangoMin = item.RangoMin

                });

            var resultadoCargosPer = _repDetRangosCargosPer
                .SelectBy(criteriaPer)
                .Select(item => new PocDetalleCargos
                {
                    IdCargo = item.IdCargo,
                    IdTablaRangos = item.IdTablaRangos,
                    MonCargo = item.MonCargo,
                    RangoMax = item.RangoMax,
                    RangoMin = item.RangoMin
                });

            if (resultadoCargosPer.ToList().Count > 0)
                return resultadoCargosPer.ToDataResult(request);
            else
                return resultadoCargos.ToDataResult(request);
        }
    }
}

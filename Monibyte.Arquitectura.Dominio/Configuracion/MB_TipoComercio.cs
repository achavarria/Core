using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Configuracion
{
    public partial class MB_TipoComercio : EntidadBase
    {
        public string CodMcc { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> IdCategoriaMcc { get; set; }
        public int IdMcc { get; set; }
        public decimal TasaComision { get; set; }
        public int IdEstado { get; set; }
        public Nullable<System.DateTime> FecInclusionAud { get; set; }
        public Nullable<int> IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

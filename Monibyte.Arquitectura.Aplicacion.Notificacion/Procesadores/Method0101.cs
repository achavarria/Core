﻿using Monibyte.Arquitectura.Poco.Notificacion;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public partial class AppNotificacion
    {
        //Notificaciones provenientes del ApiCore (Ingreso exitoso)
        public void Method0101(PocoQueue pocoQueue)
        {
            var result =
                (from usr in _repUsuario.Table
                 join per in _repvwEmpresaUsuario.Table
                     on usr.IdUsuario equals per.IdUsuario
                 where usr.CodUsuario == pocoQueue.UserCode
                 select new
                 {
                     Usuario = usr.IdUsuario,
                     CodUsuario = usr.CodUsuario,
                     AliasUsuario = usr.AliasUsuario,
                     IdEntidad = usr.IdCompania,
                     IdPersona = per.IdPersona,
                     IdEmpresa = per.IdEmpresa,
                     NombreEmpresa = per.NombreEmpresa
                 }).ToList();
            var companies = result.Select(x => x.IdEmpresa).ToArray();
            ProcesarNotificaciones(pocoQueue, false, false, companies);
        }
    }
}
using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_PaisesRegionMap : EntityTypeConfiguration<MB_PaisesRegion>
    {
        public MB_PaisesRegionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPaisRegion);

            // Properties
            this.Property(t => t.IdPaisRegion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MB_PaisesRegion", "Mnb");
            this.Property(t => t.IdPaisRegion).HasColumnName("IdPaisRegion");
            this.Property(t => t.IdRegion).HasColumnName("IdRegion");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

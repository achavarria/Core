﻿using Monibyte.Arquitectura.Dominio.Tarjetas.MQATH;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios.MQATH
{
    public interface IRepositorioATH
    {
        SalidaConsultaTarjeta ProcesarConsultaTarjeta(EntradaConsultaTarjeta entrada, string seccion = "");
        SalidaPagoTarjeta ProcesarPagoTarjeta(EntradaPagoTarjeta entrada, string seccion = "");
        SalidaActualizaTarjeta ProcesarEstadoTarjeta(EntradaActualizaTarjeta entrada, string seccion = "");
        SalidaActualizaLimiteCuenta ProcesarLimiteCuenta(EntradaActualizaLimiteCuenta entrada, string seccion = "");
        SalidaActualizaLimiteTarjeta ProcesarLimiteTarjeta(EntradaActualizaLimiteTarjeta entrada, string seccion = "");
        SalidaActivaTarjeta ProcesarActivaTarjeta(EntradaActivaTarjeta entrada, string seccion = "");
        SalidaReposicionPinTarjeta ProcesarReposicionPinTarjeta(EntradaReposicionPinTarjeta entrada, string seccion = "");
        IEnumerable<SalidaMovimientosTarjeta> ProcesarMovimientosTarjeta(EntradaMovimientosTarjeta entrada, string seccion = "");
        IEnumerable<SalidaMovTransitoTarjeta> ProcesarMovTransitoTarjeta(EntradaMovimientosTarjeta entrada, string seccion = "");
        IEnumerable<SalidaLealtadTarjeta> ProcesarProgLealtadTarjeta(EntradaLealtadTarjeta entrada, string seccion = "");
        SalidaAplicMovLealtadTarjeta ProcesarAplicaMovLealtadTarjeta(EntradaAplicMovLealtadTarjeta entrada, string seccion = "");
        IEnumerable<SalidaConsultaCuentas> ProcesarConsultaCuentas(EntradaConsultaCuentas entrada, string seccion = "");
        SalidaMiscelaneosTarjeta ProcesarMiscelaneosTarjeta(EntradaMiscelaneosTarjeta entrada, string seccion = "");
        SalidaAdelantoEfectivoTarjeta ProcesarAdelantoEfectivoTarjeta(EntradaAdelantoEfectivoTarjeta entrada, string seccion = "");
        SalidaInclusionSeguroTarjeta ProcesarInclusionSeguroTarjeta(EntradaInclusionSeguroTarjeta entrada, string seccion = "");
        SalidaCargoAutomaticoTarjeta ProcesarCargoAutomaticoTarjeta(EntradaCargoAutomaticoTarjeta entrada, string seccion = "");
        SalidaPagoExtraFinanciamiento ProcesarPagoExtraFinanciamientoa(EntradaPagoExtraFinanciamiento entrada, string seccion = "");
    }
}

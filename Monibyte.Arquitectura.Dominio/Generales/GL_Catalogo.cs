using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Contabilidad;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Logs;
using Monibyte.Arquitectura.Dominio.Procesador;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Catalogo : EntidadBase
    {
        public int IdCatalogo { get; set; }
        public string Lista { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public Nullable<int> Orden { get; set; }
        public string Referencia1 { get; set; }
        public string Referencia2 { get; set; }
        public string Referencia3 { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string Referencia4 { get; set; }
        public string Referencia5 { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Monibyte.Arquitectura.Dominio.TEF;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.TEF
{
    public class TF_PagoTrasaccionMap : EntityTypeConfiguration<TF_PagoTrasaccion>
    {
        public TF_PagoTrasaccionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPagoTrasaccion);

            // Properties
            this.Property(t => t.IdPagoTrasaccion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodRefSiscard)
                .HasMaxLength(25);

            this.Property(t => t.NumComprobanteInterno)
                .HasMaxLength(25);

            this.Property(t => t.Justificacion)
                .HasMaxLength(350);

            this.Property(t => t.CodMovimientoMQ)
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("TF_PagoTrasaccion", "TEF");
            this.Property(t => t.IdPagoTrasaccion).HasColumnName("IdPagoTrasaccion");
            this.Property(t => t.IdTransaccion).HasColumnName("IdTransaccion");
            this.Property(t => t.Producto).HasColumnName("Producto");
            this.Property(t => t.SubProducto).HasColumnName("SubProducto");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.MontoPago).HasColumnName("MontoPago");
            this.Property(t => t.IdModoAplicado).HasColumnName("IdModoAplicado");
            this.Property(t => t.CodRefSiscard).HasColumnName("CodRefSiscard");
            this.Property(t => t.NumComprobanteInterno).HasColumnName("NumComprobanteInterno");
            this.Property(t => t.Justificacion).HasColumnName("Justificacion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.CodMovimientoMQ).HasColumnName("CodMovimientoMQ");
            this.Property(t => t.IdTipoProducto).HasColumnName("IdTipoProducto");
        }
    }
}

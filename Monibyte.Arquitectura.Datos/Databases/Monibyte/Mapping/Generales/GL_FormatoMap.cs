using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_FormatoMap : EntityTypeConfiguration<GL_Formato>
    {
        public GL_FormatoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdFormato);

            // Properties
            this.Property(t => t.IdFormato)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripción)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Marcara)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.ExpresionRegular)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GL_Formato", "General");
            this.Property(t => t.IdFormato).HasColumnName("IdFormato");
            this.Property(t => t.Descripción).HasColumnName("Descripción");
            this.Property(t => t.Marcara).HasColumnName("Marcara");
            this.Property(t => t.ExpresionRegular).HasColumnName("ExpresionRegular");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

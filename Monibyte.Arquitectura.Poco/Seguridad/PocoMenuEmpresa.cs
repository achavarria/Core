﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Poco.Seguridad
{
    public class PocMenuEmpresa
    {
        public int IdEmpresa { get; set; }       
        public int IdMenu { get; set; }
        public int IdEstado { get; set; }
        public string DescEmpresa { get; set; }
        public string DescMenu { get; set; }
        public string DescEstado { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocEntradaLealtadTarjeta
    {
        public string NumTarjeta { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
    }
    public class PocSalidaLealtadTarjeta
    {
        public string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public string CodPlan { get; set; }
        public decimal SaldoPremiacion { get; set; }
    }

    public class PocEntradaAplicMovLealtadTarjeta
    {
        public string NumTarjeta { get; set; }
        public decimal Cantidad { get; set; }
        public string CodPlan { get; set; }
        public string CodMovimiento { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
    }
    public class PocSalidaAplicaMovLealtadTarjeta
    {
        public string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public decimal Cantidad { get; set; }
        public string CodPlan { get; set; }
        public string CodMovimiento { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }
}

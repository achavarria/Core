using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_Movimiento_TTMap : EntityTypeConfiguration<TC_Movimiento_TT>
    {
        public TC_Movimiento_TTMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMovimiento);

            // Properties
            this.Property(t => t.NumTarjeta)
                .HasMaxLength(20);

            this.Property(t => t.NumReferencia)
                .HasMaxLength(18);

            this.Property(t => t.Descripcion)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TC_Movimiento_TT", "TCredito");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.FecTransaccion).HasColumnName("FecTransaccion");
            this.Property(t => t.HoraTransaccion).HasColumnName("HoraTransaccion");
            this.Property(t => t.NumReferencia).HasColumnName("NumReferencia");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.MonTransaccion).HasColumnName("MonTransaccion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdDebCredito).HasColumnName("IdDebCredito");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
        }
    }
}

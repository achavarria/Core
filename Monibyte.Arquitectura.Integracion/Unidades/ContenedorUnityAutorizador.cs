﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Datos.Autorizador.Repositorios;
    using Monibyte.Arquitectura.Dominio.Autorizador.Repositorios;

    public class ContenedorUnityAutorizador
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            #endregion

            #region Repositorios
            _unityContainer.RegisterType<IRepositorioCondicionTarjeta, RepositorioCondicionTarjeta>();
            #endregion
        }
    }
}

﻿using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbContextMonibyte()
            : base("name=MonibyteConn")
        {
            Database.SetInitializer<DbContextMonibyte>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            CreateContextModelCL(modelBuilder);
            CreateContextModelSC(modelBuilder);
            CreateContextModelCA(modelBuilder);
            CreateContextModelCG(modelBuilder);
            CreateContextModelMB(modelBuilder);
            CreateContextModelDC(modelBuilder);
            CreateContextModelEF(modelBuilder);
            CreateContextModelGL(modelBuilder);
            CreateContextModelGE(modelBuilder);
            CreateContextModelLG(modelBuilder);
            CreateContextModelNT(modelBuilder);
            CreateContextModelPT(modelBuilder);
            CreateContextModelQP(modelBuilder);
            CreateContextModelTC(modelBuilder);
            CreateContextModelTS(modelBuilder);
            CreateContextModelTF(modelBuilder);
            CreateContextModelSms(modelBuilder);
        }
    }
}
using Monibyte.Arquitectura.Dominio.Efectivo;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Efectivo
{
    public class EF_ArchivoMovimientoMap : EntityTypeConfiguration<EF_ArchivoMovimiento>
    {
        public EF_ArchivoMovimientoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdArchivo);

            // Properties
            this.Property(t => t.TipoArchivo)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.NombreArchivo)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("EF_ArchivoMovimiento", "Efectivo");
            this.Property(t => t.IdArchivo).HasColumnName("IdArchivo");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.TipoArchivo).HasColumnName("TipoArchivo");
            this.Property(t => t.NombreArchivo).HasColumnName("NombreArchivo");
            this.Property(t => t.DatosArchivo).HasColumnName("DatosArchivo");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

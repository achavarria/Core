﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Quickpass;
using Monibyte.Arquitectura.Dominio.Quickpass;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<QP_Bitacora> QP_Bitacora { get; set; }
        public DbSet<QP_ListaTags> QP_ListaTags { get; set; }
        public DbSet<QP_Movimientos> QP_Movimientos { get; set; }
        public DbSet<QP_Quickpass> QP_Quickpass { get; set; }
        public DbSet<QP_TipoQuickpass> QP_TipoQuickpass { get; set; }

        void CreateContextModelQP(DbModelBuilder modelBuilder) 
        {
            modelBuilder.Configurations.Add(new QP_BitacoraMap());
            modelBuilder.Configurations.Add(new QP_ListaTagsMap());
            modelBuilder.Configurations.Add(new QP_MovimientosMap());
            modelBuilder.Configurations.Add(new QP_QuickpassMap());
            modelBuilder.Configurations.Add(new QP_TipoQuickpassMap());
        }
    }
}

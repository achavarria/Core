using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Cargos
{
    public partial class CA_DetRangosCargosPer : EntidadBase
    {
        public int IdTablaRangos { get; set; }
        public int IdCargo { get; set; }
        public decimal RangoMin { get; set; }
        public Nullable<decimal> RangoMax { get; set; }
        public Nullable<decimal> MonCargo { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public Nullable<int> IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

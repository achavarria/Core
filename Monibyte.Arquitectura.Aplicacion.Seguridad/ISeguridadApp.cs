﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public interface ISeguridadApp : IAplicacionBase
    {
        IEnumerable<PocAccion> ObtenerPermisosPorUsuario();
        IEnumerable<PocAccionMenu> ObtenerMenuUsuario();
    }
}
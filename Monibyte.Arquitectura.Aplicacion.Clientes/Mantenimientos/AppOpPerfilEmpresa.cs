﻿using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Poco.Clientes;
using System.Globalization;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public class AppOpPerfilEmpresa : AplicacionBase, IAppOpPerfilEmpresa
    {
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<CL_PerfilEmpresa> _repPerfilEmpresa;
        private IRepositorioReportServer _repReportServer;

        public AppOpPerfilEmpresa(
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<CL_PerfilEmpresa> repPerfilEmpresa,
            IRepositorioReportServer repReportServer)
        {
            _repPersona = repPersona;
            _repPerfilEmpresa = repPerfilEmpresa;
            _repReportServer = repReportServer;
        }

        public PocPerfilEmpresa ObtienePerfil()
        {
            var perfilEmpresa = _repPersona.Table
                .GroupJoin(_repPerfilEmpresa.Table,
                    persona => persona.IdPersona,
                    perfil => perfil.IdEmpresa,
                    (persona, perfil) => new { persona, perfil = perfil.FirstOrDefault() })
                .Where(x => x.persona.IdPersona == InfoSesion.Info.IdEmpresa)
                .Select(item => new PocPerfilEmpresa
                {
                    IdEmpresa = item.perfil != null ? item.perfil.IdEmpresa : 0,
                    NombreEmpresa = item.persona.NombreCompleto,
                    IdDesmarcableMovLiq = item.perfil != null ? item.perfil.IdDesmarcableMovLiq : (int)EnumIdSiNo.No,
                    IdValidaEdicionMov = item.perfil != null ? item.perfil.IdValidaEdicionMov : (int)EnumIdSiNo.No,
                    IdInfDenegaciones = item.perfil != null ? item.perfil.IdInfDenegaciones : (int)EnumIdSiNo.No,
                    NumDecimales = item.perfil != null ? item.perfil.NumDecimales : 2,
                    SepDecimal = item.perfil != null ? item.perfil.SepDecimal : CultureInfo.
                        CurrentCulture.NumberFormat.CurrencyDecimalSeparator,
                    SepMiles = item.perfil != null ? item.perfil.SepMiles : CultureInfo.
                        CurrentCulture.NumberFormat.CurrencyGroupSeparator,
                    FormatoFecha = item.perfil != null ? item.perfil.FormatoFecha : CultureInfo.
                        CurrentCulture.DateTimeFormat.ShortDatePattern,
                    IdRequiereAutorizacion = item.perfil != null ? item.perfil.IdRequiereAutorizacion : (int)EnumIdSiNo.No,
                    IdRepMovTC = item.perfil != null ? (item.perfil.IdRepMovTC.HasValue ? item.perfil.IdRepMovTC.Value : 0) : 0,
                    LogoEmpresa = item.perfil != null ? item.perfil.LogoEmpresa : null,
                    IdEnviaEstCtaIndividual = item.perfil != null ? item.perfil.IdEnviaEstCtaIndividual : (int)EnumIdSiNo.No,
                    IdIncluyeDebCred = item.perfil != null ? item.perfil.IdIncluyeDebCred : (int)EnumIdSiNo.No,
                    IdPermiteDetalleTarjeta = item.perfil != null ? item.perfil.IdPermiteDetalleTarjeta : (int)EnumIdSiNo.No
                }).FirstOrDefault();
            return perfilEmpresa;
        }

        public void ActualizarPerfilEmpresa(PocPerfilEmpresa datosEntrada)
        {
            using (var ts = new TransactionScope())
            {
                if (datosEntrada.IdInfDenegaciones == 0)
                {
                    datosEntrada.IdInfDenegaciones = (int)EnumIdSiNo.No;
                }
                var existePerfil = _repPerfilEmpresa.Table.Any(x =>
                    x.IdEmpresa == InfoSesion.Info.IdEmpresa);
                if (datosEntrada.IdEmpresa > 0 && existePerfil)
                {
                    _repPerfilEmpresa.UpdateOn(x => x.IdEmpresa == InfoSesion.Info.IdEmpresa,
                        z => new CL_PerfilEmpresa
                    {
                        IdDesmarcableMovLiq = datosEntrada.IdDesmarcableMovLiq,
                        IdValidaEdicionMov = datosEntrada.IdValidaEdicionMov,
                        IdInfDenegaciones = datosEntrada.IdInfDenegaciones,
                        NumDecimales = datosEntrada.NumDecimales,
                        SepDecimal = datosEntrada.SepDecimal,
                        SepMiles = datosEntrada.SepMiles,
                        FormatoFecha = datosEntrada.FormatoFecha,
                        LogoEmpresa = datosEntrada.LogoEmpresa,
                        IdRequiereAutorizacion = datosEntrada.IdRequiereAutorizacion,
                        IdEnviaEstCtaIndividual = datosEntrada.IdEnviaEstCtaIndividual,
                        IdIncluyeDebCred = datosEntrada.IdIncluyeDebCred,
                        IdPermiteDetalleTarjeta = datosEntrada.IdPermiteDetalleTarjeta
                    });
                }
                else
                {
                    var nuevoPerfil = new CL_PerfilEmpresa();
                    nuevoPerfil.FormatoFecha = datosEntrada.FormatoFecha;
                    nuevoPerfil.IdDesmarcableMovLiq = datosEntrada.IdDesmarcableMovLiq;
                    nuevoPerfil.IdValidaEdicionMov = datosEntrada.IdValidaEdicionMov;
                    nuevoPerfil.IdEmpresa = InfoSesion.Info.IdEmpresa;
                    nuevoPerfil.IdEstado = (int)EnumIdEstado.Activo;
                    nuevoPerfil.IdRepMovTC = datosEntrada.IdRepMovTC;
                    nuevoPerfil.LogoEmpresa = datosEntrada.LogoEmpresa;
                    nuevoPerfil.NumDecimales = datosEntrada.NumDecimales;
                    nuevoPerfil.SepDecimal = datosEntrada.SepDecimal;
                    nuevoPerfil.SepMiles = datosEntrada.SepMiles;
                    nuevoPerfil.IdInfDenegaciones = datosEntrada.IdInfDenegaciones;
                    nuevoPerfil.IdRequiereAutorizacion = datosEntrada.IdRequiereAutorizacion;
                    nuevoPerfil.IdEnviaEstCtaIndividual = datosEntrada.IdEnviaEstCtaIndividual;
                    nuevoPerfil.IdIncluyeDebCred = datosEntrada.IdIncluyeDebCred;
                    nuevoPerfil.IdPermiteDetalleTarjeta = datosEntrada.IdPermiteDetalleTarjeta;
                    _repPerfilEmpresa.Insert(nuevoPerfil);
                }
                if (datosEntrada.Imagen != null)
                {
                    var path = @"/Resources/LogosEmpresariales";
                    var fileName = string.Format("Logo-{0}.gif", InfoSesion.Info.IdEmpresa);
                    _repReportServer.UploadFile(path, fileName, datosEntrada.Imagen.Data,
                        datosEntrada.Imagen.Extension, datosEntrada.Imagen.MimeType);
                }
                _repPerfilEmpresa.UnidadTbjo.Save();
                ts.Complete();
            }
        }
    }
}


using System;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_TipoGestionRol : EntidadBase
    {
        public int IdTipoGestionRol { get; set; }
        public int IdTipoGestion { get; set; }
        public int IdRol { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

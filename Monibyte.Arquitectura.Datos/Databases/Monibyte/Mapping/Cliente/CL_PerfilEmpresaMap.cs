using Monibyte.Arquitectura.Dominio.Clientes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Clientes
{
    public class CL_PerfilEmpresaMap : EntityTypeConfiguration<CL_PerfilEmpresa>
    {
        public CL_PerfilEmpresaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdEmpresa);

            // Properties
            this.Property(t => t.IdEmpresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FormatoFecha)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.SepDecimal)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.SepMiles)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("CL_PerfilEmpresa", "Cliente");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.FormatoFecha).HasColumnName("FormatoFecha");
            this.Property(t => t.SepDecimal).HasColumnName("SepDecimal");
            this.Property(t => t.SepMiles).HasColumnName("SepMiles");
            this.Property(t => t.NumDecimales).HasColumnName("NumDecimales");
            this.Property(t => t.IdValidaEdicionMov).HasColumnName("IdValidaEdicionMov");
            this.Property(t => t.IdDesmarcableMovLiq).HasColumnName("IdDesmarcableMovLiq");
            this.Property(t => t.IdRepMovTC).HasColumnName("IdRepMovTC");
            this.Property(t => t.LogoEmpresa).HasColumnName("LogoEmpresa");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdInfDenegaciones).HasColumnName("IdInfDenegaciones");
            this.Property(t => t.IdRequiereAutorizacion).HasColumnName("IdRequiereAutorizacion");
            this.Property(t => t.IdEnviaEstCtaIndividual).HasColumnName("IdEnviaEstCtaIndividual");
            this.Property(t => t.IdIncluyeDebCred).HasColumnName("IdIncluyeDebCred");
            this.Property(t => t.IdPermiteDetalleTarjeta).HasColumnName("IdPermiteDetalleTarjeta");
            this.Property(t => t.IdVerSubCatMcc).HasColumnName("IdVerSubCatMcc");
        }
    }
}

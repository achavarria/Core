﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Dominio.Notificacion
{
    public partial class NT_IndicadorEnvio :EntidadBase
    {
        public int IdEntidad { get; set; }
        public int IdEmpresa { get; set; }
        public int IdProducto { get; set; }
        public int IdContexto { get; set; }
        public int IdValContexto { get; set; }
        public string SubContexto { get; set; }
        public int Notificar { get; set; }
    }
}
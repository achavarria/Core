﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Tesoreria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Generales
{
    public class AppGenerales : AplicacionBase, IAppGenerales
    {
        private IRepositorioMoneda _repMoneda;
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IRepositorioMnb<GL_UnidadNegocio> _repUnidadNeg;
        private IRepositorioMnb<GL_Pais> _repPais;
        private IRepositorioMnb<GL_Provincia> _repProvincia;
        private IRepositorioMnb<GL_Canton> _repCanton;
        private IRepositorioMnb<GL_Distrito> _repDistrito;
        private IRepositorioMnb<GL_ParametroCompania> _repParamCompania;
        private IRepositorioMnb<GL_Parametro> _repParametro;
        private IRepositorioMnb<GL_Reportes> _repReporte;
        private IRepositorioMnb<GL_CamposReporte> _repCamposReporte;
        private IRepositorioMnb<CL_PerfilEmpresa> _repPerfilEmpresa;
        private IRepositorioMnb<CL_EmpresasUsuario> _repEmpresasUsuario;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;
        private IRepositorioCampoContexto _repCamposContexto;

        public AppGenerales(
            IRepositorioMoneda repMoneda,
            IRepositorioMnb<GL_Entidad> repEntidad,
            IRepositorioMnb<GL_UnidadNegocio> repUnidadNeg,
            IRepositorioMnb<GL_Pais> repPais,
            IRepositorioMnb<GL_Provincia> repProvincia,
            IRepositorioMnb<GL_Canton> repCanton,
            IRepositorioMnb<GL_Distrito> repDistrito,
            IRepositorioMnb<GL_ParametroCompania> repParamCompania,
            IRepositorioMnb<GL_Parametro> repParametro,
            IRepositorioMnb<GL_Reportes> repReporte,
            IRepositorioMnb<GL_CamposReporte> repCamposReporte,
            IRepositorioMnb<CL_PerfilEmpresa> repPerfilEmpresa,
            IRepositorioMnb<CL_EmpresasUsuario> repEmpresasUsuario,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema,
            IRepositorioCampoContexto repCamposContexto)
        {
            _repEntidad = repEntidad;
            _repUnidadNeg = repUnidadNeg;
            _repMoneda = repMoneda;
            _repPais = repPais;
            _repProvincia = repProvincia;
            _repCanton = repCanton;
            _repDistrito = repDistrito;
            _repParamCompania = repParamCompania;
            _repParametro = repParametro;
            _repReporte = repReporte;
            _repCamposReporte = repCamposReporte;
            _repPerfilEmpresa = repPerfilEmpresa;
            _repCamposContexto = repCamposContexto;
            _repEmpresasUsuario = repEmpresasUsuario;
            _repParamSistema = repParamSistema;
        }

        public PocEntidad ObtieneEntidad(int idEntidad)
        {
            var validacion = new ValidationResponse();
            if (idEntidad <= 0) { validacion.AddError("Err_0026"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes al obtener entidad", validation: validacion);
            }
            else
            {
                var resultado =
                    (from ent in _repEntidad.Table
                     join mLoc in _repMoneda.Table on
                        ent.IdMonedaLocal equals mLoc.IdMoneda
                     join mInt in _repMoneda.Table on
                         ent.IdMonedaInternacional equals mInt.IdMoneda
                     where ent.IdEntidad == idEntidad
                     select new PocEntidad
                     {
                         IdEntidad = ent.IdEntidad,
                         IdPais = ent.IdPais,
                         Descripcion = ent.Descripcion,
                         MonedaLocal = new PocMoneda
                         {
                             IdMoneda = mLoc.IdMoneda,
                             IdEstado = mLoc.IdEstado,
                             Descripcion = _fGenerales.Traducir(mLoc.Descripcion, "TS_MONEDA",
                                mLoc.IdMoneda, Context.Lang.Id, 0, Context.DefaultLang.Id),
                             Simbolo = mLoc.Simbolo,
                             CodInternacional = mLoc.CodInternacional,
                             OperadorConversion = mLoc.OperadorConversion,
                             NumInternacional = mLoc.NumInternacional
                         },
                         MonedaInternacional = new PocMoneda
                         {
                             IdMoneda = mInt.IdMoneda,
                             IdEstado = mInt.IdEstado,
                             Descripcion = _fGenerales.Traducir(mInt.Descripcion, "TS_MONEDA",
                                mInt.IdMoneda, Context.Lang.Id, 0, Context.DefaultLang.Id),
                             Simbolo = mInt.Simbolo,
                             CodInternacional = mInt.CodInternacional,
                             OperadorConversion = mInt.OperadorConversion,
                             NumInternacional = mInt.NumInternacional
                         }
                     })
                     .FirstOrDefault();
                return resultado;
            }
        }

        public PocEntidad ObtenerEntidadFinanciera()
        {
            var entidad = _repParamSistema.Table
                .FirstOrDefault(x => x.IdParametro == "ID_ENTIDAD_CONVERSION");
            return ObtieneEntidad(Int32.Parse(entidad.ValParametro));
        }

        public PocConfiguracionFinanciera ObtenerInformacionFinanciera()
        {
            var entidad = _repParamSistema.Table
              .FirstOrDefault(x => x.IdParametro == "CONFIGURACION_FINANCIERA");
            return entidad != null ? entidad.ValParametro
                .FromJson<PocConfiguracionFinanciera>() : null;
        }

        public List<PocEntidad> ListaEntidades(int? idTipoEntidad = null)
        {
            var criteria = new Criteria<GL_Entidad>();
            var tipoU = Context.UserType;

            if (idTipoEntidad != null && idTipoEntidad > 0)
            {
                criteria.And(m => m.IdTipoEntidad == idTipoEntidad);
            }
            if (InfoSesion.Info.EsInterno && !tipoU.MultiEntidad)
            {
                var entesAsoc = _repEntidad.SelectBy(criteria)
                    .Join(_repEmpresasUsuario.Table,
                        e => e.IdPersona,
                        eu => eu.IdEmpresa, (e, eu) => new { e, eu })
                        .Where(x => x.eu.IdUsuario == (InfoSesion.Info.IdUsuario))
                        .Select(item => item.e);
                var result = entesAsoc.ProyectarComo<PocEntidad>();
                return result;
            }
            var entidades = _repEntidad.SelectBy(criteria);
            var resultado = entidades.ProyectarComo<PocEntidad>();
            return resultado;
        }

        public List<PocUnidadNegocio> ListaUnidadesPorEntidad(int idEntidad)
        {
            var entidad = _repEntidad.Table
                .FirstOrDefault(x => x.IdPersona == idEntidad);
            var id = entidad != null ? entidad.IdEntidad : idEntidad;
            var criteria = new Criteria<GL_UnidadNegocio>();
            criteria.And(m => m.IdEntidad == id);
            var unidades = _repUnidadNeg.SelectBy(criteria);
            var resultado = unidades.ProyectarComo<PocUnidadNegocio>();
            return resultado;
        }

        public List<PocProvincia> ListaProvincias()
        {
            return _repProvincia.Table.ToList().ProyectarComo<PocProvincia>();
        }

        public List<PocCanton> ListaCantones(int? idProvincia)
        {
            if (idProvincia > 0)
            {
                return _repCanton.Table.Where(x => x.IdProvincia == idProvincia)
                    .ToList().ProyectarComo<PocCanton>();
            }
            else
            {
                return _repCanton.Table.ToList().ProyectarComo<PocCanton>();
            }
        }

        public List<PocDistrito> ListaDistritos(int? idCanton)
        {
            if (idCanton > 0)
            {
                return _repDistrito.Table.Where(x => x.IdCanton == idCanton)
                    .ToList().ProyectarComo<PocDistrito>();
            }
            else
            {
                return _repDistrito.Table.ToList().ProyectarComo<PocDistrito>();
            }
        }

        public PocPais ObtienePaisProcesador()
        {
            var entidad = _repEntidad.SelectById(InfoSesion.ObtenerSesion<int>("IDPROCESADOR"));
            if (entidad != null)
            {
                var pais = _repPais.SelectById(entidad.IdPais);
                return pais.ProyectarComo<PocPais>();
            }
            return null;
        }

        public string ObtieneParametroCompania(string idParametro, int idCompania)
        {
            var param = _repParamCompania.Table.
                FirstOrDefault(x => x.IdParametro == idParametro &&
                                x.IdCompania == idCompania);
            return param != null ? param.ValParametro : string.Empty;
        }

        public void GuardarParametroCompania(string idParametro, int idCompania, string valorParametro)
        {
            _repParamCompania.UpdateOn(x => x.IdParametro == idParametro &&
                x.IdCompania == idCompania, z => new GL_ParametroCompania
                {
                    ValParametro = valorParametro
                });
        }

        public List<PocParametros> ListasPredefinidas()
        {
            return _repParametro.Table
                .Where(x => x.IdTipoOrigen == (int)EnumOrigenParametro.ListaSQL)
                .Select(item => new PocParametros
                {
                    IdParametro = item.IdParametro,
                    Descripcion = item.Descripcion
                }).ToList();
        }

        public void EliminarReporte(int idReporte)
        {
            var perfil = _repPerfilEmpresa.Table
                            .FirstOrDefault(x => x.IdEmpresa == InfoSesion.Info.IdEmpresa &&
                                            x.IdRepMovTC == idReporte);
            if (perfil == null)
            {
                using (var ts = new TransactionScope())
                {
                    _repCamposReporte.DeleteOn(x => x.IdReporte == idReporte);
                    _repReporte.DeleteOn(p => p.IdReporte == idReporte);
                    _repReporte.UnidadTbjo.Save();
                    ts.Complete();
                }
            }
            else
            {
                throw new CoreException("Error al eliminar reporte", "Err_0086");
            }
        }

        public IEnumerable<PocCampoContexto> ObtenerCamposContexto(int idContexto)
        {
            var criteria = new Criteria<GL_CamposContexto>();
            criteria.And(c => c.IdContexto == idContexto);
            Func<string, List<PocListaSql>> _func = delegate(string sql)
            {
                if (!string.IsNullOrEmpty(sql))
                {
                    int idTipo = Convert.ToInt32(sql.Split('~')[0]);
                    if (idTipo == (int)EnumTipoParamEmpresa.Lista
                        || idTipo == (int)EnumTipoParamEmpresa.ListaPredefinida)
                    {
                        sql = sql.Split('~')[1];
                    var x = _fGenerales.ObtenerListaSql(sql);
                    return x.ProyectarComo<PocListaSql>();
                }
                return null;
                }
                return null;
            };
            return _repCamposContexto.List(criteria).ToList()
                .Select(c => new PocCampoContexto
                {
                    IdCampo = c.IdCampo,
                    IdContexto = c.IdContexto,
                    NombreCampo = c.Campo,
                    IdTipo = c.IdTipo,
                    Descripcion = c.Descripcion,
                    Lista = !string.IsNullOrEmpty(c.SQLOrigen) ? _func(string.
                        Concat(c.IdTipo, "~", c.SQLOrigen)) : _func(c.SQLOrigen),
                    Referencia1 = c.Referencia1,
                    Referencia2 = c.Referencia2,
                    Referencia3 = c.Referencia3,
                    Referencia4 = c.Referencia4,
                    Referencia5 = c.Referencia5
                });
        }
    }
}

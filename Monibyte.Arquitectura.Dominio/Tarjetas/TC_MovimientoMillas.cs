using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_MovimientoMillas : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public Nullable<int> IdTarjeta { get; set; }
        public System.DateTime FecTransaccion { get; set; }
        public Nullable<System.DateTime> HoraTransaccion { get; set; }
        public decimal CantidadMillas { get; set; }
        public int IdDebCredito { get; set; }
        public Nullable<System.DateTime> FecProceso { get; set; }
        public string DetalleMovimiento { get; set; }
        public Nullable<int> IdUsuario { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<int> IdPrograma { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

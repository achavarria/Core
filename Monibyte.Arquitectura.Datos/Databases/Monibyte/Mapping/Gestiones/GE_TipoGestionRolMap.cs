using Monibyte.Arquitectura.Dominio.Gestiones;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Gestiones
{
    public class GE_TipoGestionRolMap : EntityTypeConfiguration<GE_TipoGestionRol>
    {
        public GE_TipoGestionRolMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipoGestionRol);

            // Properties
            this.Property(t => t.IdTipoGestionRol)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("GE_TipoGestionRol", "Gestion");
            this.Property(t => t.IdTipoGestionRol).HasColumnName("IdTipoGestionRol");
            this.Property(t => t.IdTipoGestion).HasColumnName("IdTipoGestion");
            this.Property(t => t.IdRol).HasColumnName("IdRol");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

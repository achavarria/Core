﻿using Monibyte.Arquitectura.Dominio.Nucleo;
using System.Collections.Generic;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Dominio.Generales.Repositorios
{
    public interface IFuncionesGenerales : IRepositorioBase
    {
        [DbFunction("General", "f_ObtieneTraduccion")]
        string Traducir(string pTexto, string pContexto, int pIdContexto,
            int pIdIdioma, int pAbreviatura, int pIdIdiomaBase);
        int ObtieneSgteSecuencia(string pSecuencia);
        IEnumerable<Cplx_SqlLista> ObtenerListaSql(string sql);
        IEnumerable<Cplx_SqlLista> ObtenerListaPredefinida(int idParametro);
        string ObtieneParametroCompania(string idParametro, int idCompania);
    }
}

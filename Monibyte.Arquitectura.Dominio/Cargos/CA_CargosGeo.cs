using System;

namespace Monibyte.Arquitectura.Dominio.Cargos
{
    public partial class CA_CargosGeo: EntidadBase
    {
        public int IdRegistro { get; set; }
        public int IdCargo { get; set; }
        public int IdCuenta { get; set; }
        public int IdTipoVehiculo { get; set; }
        public string Marca { get; set; }
        public string Estilo { get; set; }
        public string Placa { get; set; }
        public int Modelo { get; set; }
        public decimal PaqueteBase { get; set; }
        public decimal Sensor { get; set; }
        public decimal BotonPanico { get; set; }
        public decimal CargoOriginal { get; set; }
        public decimal CargoActual { get; set; }
        public System.DateTime FecUltAplicacion { get; set; }
        public System.DateTime FecUltActividad { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }  
    }
}

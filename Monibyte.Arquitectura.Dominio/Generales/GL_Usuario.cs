using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Usuario : EntidadBase
    {
       public int IdUsuario { get; set; }
        public string CodUsuario { get; set; }
        public string AliasUsuario { get; set; }
        public int IdEstado { get; set; }
        public int IdPersona { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdCompania { get; set; }
        public int IdUnidad { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public string Correo { get; set; }
        public int? IdIdiomaPreferencia { get; set; }
    }
}

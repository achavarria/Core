﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Configuracion;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public interface IAppConfiguracionLicPer : IAplicacionBase
    {
        void Insertar(PocLicenciaPersona registro);
        void Eliminar(PocLicenciaPersona registro);
        void Modificar(PocLicenciaPersona registro);
        DataResult<PocLicenciaPersona> Listar(PocLicenciaPersona filtro, DataRequest request);
    }
}

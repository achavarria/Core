﻿using Microsoft.Practices.Unity.InterceptionExtension;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Monibyte.Arquitectura.Integracion.Interceptor
{
    public class AuditInterceptor : ICallHandler
    {
        public int Order { get; set; }

        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            var idUsuarioAud = InfoSesion.Info != null ? InfoSesion.Info.IdUsuario as int? : null;
            string methodName = input.MethodBase.Name;
            if (methodName == "Insert")
            {
                object type = input.Arguments[0];
                var idUnidadAud = InfoSesion.Info != null ? InfoSesion.Info.IdUnidad as int? : null;
                var idCompaniaAud = InfoSesion.Info != null ? InfoSesion.Info.IdCompania as int? : null;
                var fecInclusion = type.GetType().GetProperty("FecInclusionAud");
                if (fecInclusion != null)
                {
                    type.GetType().GetProperty("FecInclusionAud").SetValue(type, DateTime.Now, null);
                }
                var usuarioIncluye = type.GetType().GetProperty("IdUsuarioIncluyeAud");
                if (usuarioIncluye != null && idUsuarioAud != null)
                {
                    type.GetType().GetProperty("IdUsuarioIncluyeAud").SetValue(type, idUsuarioAud, null);
                }
                //Establece los valores para compania y unidad en los registros que corresponde
                var idUnidad = type.GetType().GetProperty("IdUnidad");
                if (idUnidad != null && idUnidadAud != null)
                {
                    var starvalue = idUnidad.GetValue(type, null) as int?;
                    var value = starvalue.HasValue && starvalue != 0 ? starvalue : idUnidadAud;
                    type.GetType().GetProperty("IdUnidad").SetValue(type, value, null);
                }
                var idCompania = type.GetType().GetProperty("IdCompania");
                if (idCompania != null && idCompaniaAud != null)
                {
                    var starvalue = idCompania.GetValue(type, null) as int?;
                    var value = starvalue.HasValue && starvalue != 0 ? starvalue : idCompaniaAud;
                    type.GetType().GetProperty("IdCompania").SetValue(type, value, null);
                }
            }
            else if (methodName == "Update")
            {
                object type = input.Arguments[0];
                var fecActualiza = type.GetType().GetProperty("FecActualizaAud");
                var usuarioActualiza = type.GetType().GetProperty("IdUsuarioActualizaAud");
                if (fecActualiza != null)
                {
                    type.GetType().GetProperty("FecActualizaAud").SetValue(type, DateTime.Now, null);
                }
                if (usuarioActualiza != null && idUsuarioAud != null)
                {
                    type.GetType().GetProperty("IdUsuarioActualizaAud").SetValue(type, idUsuarioAud, null);
                }
                if (input.Arguments.Count == 3 && fecActualiza != null && usuarioActualiza != null)
                {
                    MethodInfo method = GetType().GetMethod("UpdateAuditExpressions",
                        BindingFlags.Instance | BindingFlags.NonPublic);
                    method = method.MakeGenericMethod(new Type[] { ObjectContext.GetObjectType(type.GetType()) });
                    input.Arguments[2] = method.Invoke(this, new object[] { type, input.Arguments[2] });
                }
            }
            else if (methodName == "UpdateOn")
            {
                var type = input.MethodBase.GetGenericArguments()[0];
                var fecActualiza = type.GetProperty("FecActualizaAud");
                var usuarioActualiza = type.GetProperty("IdUsuarioActualizaAud");
                if (fecActualiza != null && usuarioActualiza != null)
                {
                    MethodInfo method = GetType().GetMethod("UpdateOnAuditExpressions",
                        BindingFlags.Instance | BindingFlags.NonPublic);
                    method = method.MakeGenericMethod(type);
                    input.Arguments[1] = method.Invoke(this, new object[] { input.Arguments[1], idUsuarioAud });
                }
            }
            IMethodReturn msg = getNext()(input, getNext);
            return msg;
        }

        private Expression<Func<T, object>>[] UpdateAuditExpressions<T>(ref T type, object item)
        {
            var expressions = item as Expression<Func<T, object>>[];
            var expressionType = Expression.Parameter(ObjectContext.GetObjectType(type.GetType()), "creditCard");
            var fecExpression = Expression.Convert(Expression.PropertyOrField(expressionType, "FecActualizaAud"), typeof(object));
            var usrExpression = Expression.Convert(Expression.PropertyOrField(expressionType, "IdUsuarioActualizaAud"), typeof(object));
            var usrExpressionLmd = Expression.Lambda<Func<T, object>>(usrExpression, expressionType);
            var fecExpressionLmd = Expression.Lambda<Func<T, object>>(fecExpression, expressionType);
            Array.Resize(ref expressions, expressions.Length + 2);
            expressions[expressions.Length - 2] = usrExpressionLmd;
            expressions[expressions.Length - 1] = fecExpressionLmd;
            return expressions;
        }

        private Expression<Func<T, T>> UpdateOnAuditExpressions<T>(object item, int? idUsuarioAud)
        {
            var expresion = item as Expression<Func<T, T>>;
            var meminitexp = expresion.Body as MemberInitExpression;
            var bindings = meminitexp.Bindings.ToList();
            var fecMember = typeof(T).GetMember("FecActualizaAud")[0];
            bindings.Add(Expression.Bind(fecMember, Expression.Constant(DateTime.Now)));
            if (idUsuarioAud != null)
            {
                var usrMember = typeof(T).GetMember("IdUsuarioActualizaAud")[0];
                bindings.Add(Expression.Bind(usrMember, Expression.Constant(idUsuarioAud)));
            }
            var newtype = Expression.New(typeof(T));
            var expressionType = Expression.Parameter(typeof(T), "creditCard");
            var memberInitExpression = Expression.MemberInit(newtype, bindings.ToArray());
            var result = Expression.Lambda<Func<T, T>>(memberInitExpression, expressionType);
            return result;
        }
    }
}
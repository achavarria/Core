﻿using Monibyte.Arquitectura.Aplicacion.Efectivo;
using Monibyte.Arquitectura.Aplicacion.Gestiones;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Efectivo;
using Monibyte.Arquitectura.Poco.Gestiones;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Efectivo
{
    public class ServicioEfectivo : ServicioUnity, IServicioEfectivo
    {
        private IAppMovimientos _appMovimientos;
        private IAppGestionesEfectivo _appGestionesEfectivo;

        public ServicioEfectivo(IAppMovimientos appMovimientos, IAppGestionesEfectivo appGestionesEfectivo)
        {
            _appMovimientos = appMovimientos;
            _appGestionesEfectivo = appGestionesEfectivo;
        }

        public Transporte IncluirMovimientosEfectivo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocMovimientosEfectivo>();
            _appMovimientos.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarMovimientosEfectivo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocMovimientosEfectivo>();
            DateTime? fecDesde = transporte.GetValor<DateTime?>("fecDesde");
            DateTime? fecHasta = transporte.GetValor<DateTime?>("fecHasta");
            var resultado = _appMovimientos.ListarMovEfectivo(entrada, transporte.GetDataRequest(), fecDesde, fecHasta);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListarMovimientosKilometraje(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocMovimientosEfectivo>();
            DateTime? fecDesde = transporte.GetValor<DateTime?>("fecDesde");
            DateTime? fecHasta = transporte.GetValor<DateTime?>("fecHasta");
            var resultado = _appMovimientos.ListarMovKilometraje(entrada, transporte.GetDataRequest(), fecDesde, fecHasta);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaMovimientosEfectivo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocMovimientosEfectivo>();
            _appMovimientos.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaMovimientosEfectivo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocMovimientosEfectivo>();
            _appMovimientos.Modificar(entrada);
            return transporte;
        }

        public Transporte ObtieneListaGestionKm(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocParametrosGestion>();
            var resultado = _appGestionesEfectivo.ObtieneListaGestionKm(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte IncluirGestionKilometraje(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocGestionKilometraje>();
            var resultado = _appGestionesEfectivo.IncluirGestionKilometraje(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }
        
        public Transporte ListarArchivoMovimiento(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocArchivoMovimiento>();
            var resultado = _appMovimientos.ListarArchivoMovimiento(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaArchivoMovimiento(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocArchivoMovimiento>();
            _appMovimientos.EliminaArchivoMovimiento(entrada);
            return transporte;
        }

        public Transporte AbrirArchivoMovimiento(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocArchivoMovimiento>();
            var resultado = _appMovimientos.AbrirArchivoMovimiento(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ExisteArchivoMovimiento(Transporte transporte)
        {
            var idMovimiento = transporte.GetValor<int>("idMovimiento");
            var nombreArchivo = transporte.GetValor<string>("nombreArchivo");
            var resultado = _appMovimientos.ExisteArchivoMovimiento(idMovimiento, nombreArchivo);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EditarGestionKilometraje(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocGestionKilometraje>();
            var gestionPrevia = transporte.GetValor<PocGestionKilometraje>("gestionPrevia");
            _appGestionesEfectivo.EditarGestionKilometraje(entrada, gestionPrevia);
            return transporte;
        }

        public Transporte ObtenerTarifas(Transporte transporte)
        {
            var idEmpresa = transporte.GetValor<int>("idEmpresa");
            var resultado = _appMovimientos.ObtenerTarifas(transporte.GetDataRequest(), idEmpresa);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ModificarTarifa(Transporte transporte)
        {
            var entrada = transporte.GetDatos<List<PocTarifaKilometraje>>();
            _appMovimientos.ModificarTarifa(entrada);
            return transporte;
        }

        public Transporte EliminarTarifa(Transporte transporte)
        {
            var entrada = transporte.GetDatos<List<PocTarifaKilometraje>>();
            _appMovimientos.EliminarTarifa(entrada);
            return transporte;
        }

        public Transporte CrearTarifa(Transporte transporte)
        {
            var entrada = transporte.GetDatos<List<PocTarifaKilometraje>>();
            var idEmpresa = transporte.GetValor<int>("idEmpresa");
            _appMovimientos.CrearTarifa(entrada, idEmpresa);
            return transporte;
        }

        public Transporte ObtenerInfoMovEfectivo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocMovimientosEfectivo>();
            var resultado = _appMovimientos.Obtener(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ExisteArchivoTarjeta(Transporte transporte)
        {
            var idMovimiento = transporte.GetValor<int>("idMovimiento");
            var nombreArchivo = transporte.GetValor<string>("nombreArchivo");
            var resultado = _appMovimientos.ExisteArchivoTarjeta(idMovimiento, nombreArchivo);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListarArchivoMovimientoTc(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocArchivoTarjeta>();
            var resultado = _appMovimientos.ListarArchivoMovimientoTc(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaArchivoMovimientoTc(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocArchivoTarjeta>();
            _appMovimientos.EliminaArchivoMovimientoTc(entrada);
            return transporte;
        }

        public Transporte AbrirArchivoMovimientoTc(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocArchivoTarjeta>();
            var resultado = _appMovimientos.AbrirArchivoMovimientoTc(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }
    }
}


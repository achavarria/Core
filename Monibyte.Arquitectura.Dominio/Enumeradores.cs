﻿
namespace Monibyte.Arquitectura.Dominio
{
    public struct Acciones
    {
        public const string Delete = "D";
        public const string Insert = "I";
        public const string Update = "U";
    }

    public struct Time
    {
        public const double DayFactor = 0.99999;
    }

    public struct SubContexto
    {
        public const string VipActivo = "VIPACTIVO";
        public const string VipInactivo = "VIPINACT";
        public const string VipProgramado = "VIPREPROG";
        public const string VipInactivoPramado = "VIPINACTPROG";
        public const string VipActivoOp = "VIPACTIVOOP";
        public const string VipInactivoOp = "VIPINACTIVOOP";
    }

    public struct AccionVIP
    {
        public const string AnularProgramacion = "AP";
        public const string Inactivacion = "I";
        public const string AutorizaInactivacion = "CI";
        public const string Programada = "P";
        public const string AutorizaProgramacion = "CP";
        public const string Activacion = "A";
        public const string AutorizaActivacion = "CA";
    }

    public struct DebitoCredito
    {
        public const string Debito = "D";
        public const string Credito = "C";
    }

    public struct TipoMovimientosTC
    {
        public const string Gasto = "G";
    }
	
	public struct VigenciaTarjeta
    {
        public const string Vigente = "V";
        public const string SinEntregar = "NA";
    }

    public struct CanalNotificacion
    {
        public const string Sms = "Sms";
        public const string Push = "Push";
        public const string Email = "Email";
    }

    public enum EnumIdEstado
    {
        Activo = 1,
        Inactivo = 0,
    }

    public enum EnumTipoIdentificacion
    {
        CedulaIdentidad = 1,
        CedulaJuridica = 2
    }

    public enum EnumIdSiNo
    {
        Si = 5,
        No = 6,
    }

    public enum EnumEstadoUsuario
    {
        Activo = 94,
        Inactivo = 95,
        Registrado = 96,
        Bloqueado = 97,
        TemporalmenteBloqueado = 98
    }

    public enum EnumTipoUsuario
    {
        Adicional = 413,
        SubAdministrador = 414,
        Administrador = 415,
        //Internos
        SuperUsuario = 416,
        JefeDeOficina = 417,
        UsuarioAdministrativo = 418,
        SoloConsulta = 419,
        AdminCuentas = 420
    }

    public enum EnumRelacionTarjeta
    {
        Titular = 89,
        Adicional = 90
    }

    public enum EnumEstadoCuentaTarjeta
    {
        Activa = 194,
        Bloqueada = 195,
        CerradaASolicitud = 197,
        Cerrada = 198,
        Sobregirada = 203
    }

    public enum EnumEstadoTarjeta
    {
        Todos = -2,
        Vigentes = -1,
        Activa = 224,
        Perdida = 225,
        Robada = 226,
        BloqueoTemporal = 227,
        SinEntregar = 229,
        CuentaCerrada = 232,
        Vencida = 233,
        BloqueadaIv = 235,
        SinImprimir = 236,
        CerradaPorFraude = 242,
        Sobregirada = 237,
        Despido = 238,
        Renuncia = 379,
        Otro = 380
    }

    public enum EnumTipoReposicion
    {
        PorDeterioro = 244,
        PorPerdida = 245,
        PorRobo = 246,
        PorFraude = 247
    }

    public enum EnumDebitoCredito
    {
        Debito = 134,
        Credito = 135
    }

    public enum EnumTipoGestion
    {
        BloqueoTemporalTarjeta = 2,
        BloqueoTarjeta = 3,
        ModificaLimiteCuentaTarjeta = 4,
        ModificaLimiteEfectivoTarjeta = 5,
        ModificaFechaCorteTarjeta = 6,
        RenovacionTarjeta = 7,
        ReposiciónTarjeta = 8,
        ActivarTarjeta = 9,
        CambioEstadoTarjeta = 10,
        CancelaTarjeta = 11,
        SolicitaTarjetaAdicional = 12,
        LiquidacionDeGastos = 13,
        LiquidacionDeKilometraje = 14,
        NotificacionDeSalida = 15,
        LiquidacionDeEfectivo = 16,
        VipTarjetaCredito = 17,
        ReActivarTarjeta = 18,
        ModificaLimiteCuenta = 19
    }

    public enum EnumEstadoGestion
    {
        Todos = 0,
        Condicionada = 313,
        Registrada = 314,
        Cerrada = 315,
        Anulada = 316,
        Aprobada = 317,
        EnEdicion = 318,
        Rechazada = 319,
        EnAnalisis = 320,
        RevisionPendiente = 321,
        Autorizada = 322,
        AprobacionTransitoria = 323,
        PorAplicar = 324,
        PendienteAprobacion = 1003,
        PendienteCorrecion = 1004,
        Finalizada = 1005
    }

    public enum EnumTipoProducto
    {
        TarjetaCredito = 299,
        Credito = 300,
        OperacionesEfectivo = 301,
        Extra = 302,
        Intra = 303,
        SolicitudProducto = 304,
        Promocion = 305
    }

    public enum EnumTipoRestriccion
    {
        VIP = 391,
        SinRestriccion = 392,
        VigenciaVip = 393,
        ConsumeLocal = 394,
        ConsumeInternacional = 395,
        ConsumeInternet = 396,
        AvanceEfectivo = 397,
        SoloAvanceEfectivo = 398
    }

    public enum EnumEstadoRestriccion
    {
        Inactivo = 0,
        Activo = 1,
        ActivoRango = 2,
        SoloAvance = 3,
        DesactivacionTemporal = 4
    }

    public enum EnumIdTipoDeCambio
    {
        Compra = 344,
        Venta = 345
    }

    public enum EnumIdDiaCorte
    {
        Corte03 = 56,
        Corte17 = 57,
        Corte20 = 58,
        Corte24 = 59,
        Corte27 = 60,
        Corte30 = 61
    }

    public enum EnumTipoComunicacion
    {
        Celular = 27,
        Correo = 28,
        Fax = 29,
        Telefono = 30,
    }

    public enum EnumUbicacion
    {
        Casa = 39,
        Trabajo = 40
    }

    public enum EnumTipoPersona
    {
        Fisica = 17,
        Juridica = 18,
    }

    public enum EnumTipoFiltro
    {
        Guardar = 1,
        Solicitar = 2,
    }

    public enum EnumEstadoQuickpass
    {
        Activo = 435,
        Asignado = 436,
        Registrado = 437,
        Perdido = 438,
        Devuelto = 439,
        Defectuoso = 440,
        Vencido = 1056
    }

    public enum EnumEstadOperativoQP
    {
        ListaBlanca = 1070,
        ListaGris = 1071,
        ListaNegra = 1072
    }

    public enum EnumTipoMovQuickpass
    {
        CompraQuickpass = 441,
        AdmQuickpass = 442,
        PeajeQuickpass = 443
    }

    public enum EnumEstadoMovQuickpass
    {
        Registrado = 451,
        Aplicado = 452,
        Anulado = 453,
        Reversado = 454,
        Error = 455
    }

    public enum EnumTipoMovimientoTC
    {
        PeajeQuickpass = 467,
        CargoMensualQuickpass = 468,
        AdquisicionQuickpass = 469
    }

    public enum EnumPeriodicidad
    {
        Diario = 821,
        Semanal = 822,
        Quincenal = 823,
        Mensual = 104,
        BiMensual = 105,
        Trimestral = 106,
        Cuatrimestral = 107,
        Semestral = 108,
        Anual = 109
    }

    public enum EnumPeriodicidadServicio
    {
        Segundo = 560,
        Minuto = 561,
        Hora = 562,
        Diario = 563,
        Semanal = 564,
        Mensual = 565,
        Anual = 566
    }

    public enum EnumEstadoCivil
    {
        Casado = 10,
        Soltero = 11,
        UnionLibre = 12,
        Viudo = 13,
        Divorciado = 14,
        NoRegistrado = 15
    }

    public enum EnumRelacionNegocio
    {
        Persona = 19,
        Cliente = 20,
        Empleado = 21,
        Inversionista = 22,
        Proveedor = 23
    }

    public enum EnumTipoParametroContabilidad
    {
        CustomerJob = 570,
        Account = 571,
        Item = 572
    }

    public enum EnumEstadoMovimientoTC
    {
        Aplicado = 550,
        Bloqueado = 551,
        Contabilidad = 552,
        DistribucionPendiente = 553,
        Anulado = 554,
        PorAplicar = 555,
        StandBy = 556
    }

    public enum EnumEventos
    {
        Login = 1,
        IngresoFallido = 2,
        TockenInvalido = 3
    }

    public enum EnumDatosComprobante
    {
        Password = 1,
        Token = 2,
        PasswordToken = 3
    }

    public enum EnumTipoListaQP
    {
        Blanca = 580,
        Gris = 581,
        Negra = 582
    }

    public enum EnumContexto
    {
        LTipoUsuario = 150,
        GE_TIPOGESTION = 152,
        TC_CUENTA = 159,
        TC_TARJETA = 160,
        LAlcanceTarjeta = 162,
        LCalificacionTarjeta = 163,
        LClaseTarjeta = 165,
        LCodPlan = 166,
        LDebitoCredito = 168,
        LDiasCorte = 169,
        LDiasSemana = 170,
        LEstadoCivil = 171,
        LEstadoCuenta = 172,
        LEstadoGestion = 173,
        LEstadoMovimientoTc = 174,
        LEstadomovQuickpass = 175,
        LEstadoQuickpass = 176,
        LEstadoTarjeta = 177,
        LEstadoUsuario = 178,
        LGenero = 179,
        LMarcaTc = 180,
        LMeses = 181,
        LOrigenMovimientoTc = 183,
        LPeriodicidad = 185,
        LRelacionNegocio = 187,
        LRelacionTarjeta = 188,
        LTipoComunicacion = 193,
        LTipoDireccion = 593,
        LTipoEntidad = 594,
        LTipoMovimientoTc = 597,
        LTipomovQuickpass = 598,
        LTipoPersona = 601,
        LTipoPlastico = 602,
        LTipoPoliza = 603,
        LTipoProducto = 604,
        LTipoPrograma = 605,
        LTipoReposicion = 606,
        LUbicacion = 607,
        TC_TIPOTARJETA = 608,
        TC_MOVIMIENTO = 609,
        EF_KILOMETRAJE = 610,
        EF_MOVIMIENTO = 611,
        NOT_DENEGACIONES = 613,
        RECHAZAR_AUTORIZACION = 614,
        APROBAR_AUTORIZACION = 615,
        TARJETA_ADICIONAL = 617,
        REPORTE_LIQGASTO = 618,
        MB_CATEGORIATIPOCOMERCIO = 622,
        LiquidacionGastos = 625,
        TC_Geolocalizacion = 633,
        PREPARAR_AUTORIZACION = 635
    }

    public enum EnumTipoCobroCargo
    {
        Fijo = 741,
        Cantidad = 742
    }

    public enum EnumSistema
    {
        SistTransaccional = 1,
        SistAdministrativo = 2,
        ModTarjetas = 100,
        ModGeneral = 101,
        ModClientes = 102,
        ModTesoreria = 103,
        ModContabilidad = 104,
        ModQuickpass = 105,
        ModGestiones = 106
    }

    public enum EnumMoneda
    {
        Colones = 1,
        Dolares = 2
    }

    public enum MonedasInternacional
    {
        Colones = 188,
        Dolares = 840
    }

    public enum EnumEstadoPago
    {
        PorEnviar = 1424,
        Aplicada = 1425,
        MQNoAplico = 1426,
        Eliminada = 1427,
        Anulado = 1428
    }

    public enum EnumEstadoEnvio
    {
        NoEnviado = 0,
        Enviado = 1,
        CompletadoConError = 2
    }

    public enum EnumTipoTarjeta
    {
        MONIBYTEBlack = 1,
        MONIBYTEBlackCorporativa = 2,
        WorldMasterCardBusiness = 3,
        MasterCardBusiness = 4,
        MasterCardPlatinum = 5,
        TarjetaSiscardPruebas = 6
    }

    public enum EnumTipoEntidad
    {
        Asociada = 43,
        Proveedor = 44
    }

    public enum EnumTipoParamEmpresa
    {
        Texto = 751,
        Cantidad = 752,
        Monto = 753,
        Fecha = 754,
        Lista = 755,
        ListaPredefinida = 756
    }

    public enum EnumTipoAgrupacionEstCuenta
    {
        SinAgrupar = 0,
        TarjetaPorComercios = 1,
        ComercioPorTarjetas = 2,
        PoTarjeta = 3,
        ResumenPorComercio = 4
    }

    //Apunta a la tabla de GL_Parametros
    public enum EnumParametrosMONIBYTE
    {
        CentroCosto = 14,
        CuentaContable = 15,
    }

    public enum EnumTipoOrdenLista
    {
        Codigo = 781,
        Descripcion = 782
    }

    public enum EnumLOrigen
    {
        Personalizado = 791,
        Fisico = 792
    }

    public enum EnumTipoLimite
    {
        Compartido = 816,
        Separado = 817
    }

    public enum EnumCategoriaMcc
    {
        Todos = 0,
        Gas = 1,
        AvanceEfectivo = -1,
        ComprasInternet = -2
    }

    public enum EnumRoleOperativo
    {
        Editor = 884,
        Autorizador = 885
    }
    public enum EnumTipoAutorizacion
    {
        RestriccionesTarjeta = 844,
        RestriccionesGrupo = 845,
        AgruparTarjetas = 846,
        AdministradoresGrupo = 848,
        EliminarGrupo = 849,
        LimiteCreditoTarjeta = 850,
        VipTarjeta = 851,
        TarjetasAdicionales = 852,
        GrupoOperativo = 853
    }
    public enum EnumEstadoAutorizacion
    {
        Preparada = 874,
        Autorizada = 875,
        Denegada = 876,
        Anulada = 877
    }

    public enum EnumOrigenParametro
    {
        Lista = 361,
        Valor = 362,
        SQL = 363,
        ListaSQL = 364
    }

    public enum EnumTipoReporte
    {
        Archivo = 949,
        Reporte = 950
    }

    public enum EnumEstadoEdicion
    {
        Editado = 951,
        Parcial = 952,
        Completa = 953,
        Pendiente = 954,
        Liquidado = 956,
        PendienteAprobacion = 1003,
        PendienteCorrecion = 1004,
        Finalizada = 1005
    }

    public enum EnumSubReporte
    {
        SubReporte1 = 1,
        SubReporte2 = 2
    }

    public enum EnumTipoGrupo
    {
        PanelControl = 961,
        Operativo = 962
    }


    public enum EnumUnidadMedida
    {
        Litros = 959,
        Galones = 960
    }

    public enum EnumRoleSistema
    {
        Administrador = 1,
        SubAdministrador = 2,
        Adicional = 3,
        SoloConsulta = 4,
        AdminCuentas = 7
    }

    public enum EnumTipoConsumo
    {
        Compras = 1,
        Financiacion = 2
    }

    public enum EnumTipoCargo
    {
        LicenciaAdmMONIBYTE = 1,
        LicenciaSubAdmMONIBYTE = 2,
        LicenciaLAFISEMONIBYTE = 3,
        CargoPorSeguro = 4,
        CargosAdministradorCuentas = 5,
        CargoGeo = 6
    }
    public enum EnumIdAccionMigracion
    {
        SoloMigracion = 1,
        SoloLimites = 2,
        Ambos = 3,
        Inactivacion = 4,
        Activacion = 5
    }
    public enum EnumEstadosTransaccion
    {
        Ingresada = 1409,
        Enviada = 1410,
        Recibida = 1411,
        PendientePago = 1412,
        Autorizado = 1413,
        Rechazado = 1414,
        PorAplicarMQ = 1415,
        Aplicada = 1416,
        Reversado = 1417,
        Excluido = 1418,
        Borrado = 1419
    }
    public enum EnumModoAplicado
    {
        Manual = 1405,
        Automatico = 1406
    }
    public enum EnumModoPago
    {
        OtraTarjeta = 1466,
        Extra_Intra = 1467
    }
    public enum EnumAccionPago
    {
        Traslado = 1461,
        TrasladoAExtraIntra = 1462,
        ConvertirMontoMismaTarjeta = 1463,
        ConvertirMontoOtraTarjeta = 1464,
        ConvertirMontoExtraIntra = 1465
    }
    public enum EnumOrigenInterno
    {
        OtroBanco = 1075,
        EnLinea = 1076,
        CargoAutomatico = 1077,
        SitioAdmin = 1078
    }
    public enum EstadosTFOPEL
    {
        PorProcesar = 1,
        Autorizada = 2,
        Confirmada = 4,
        Rechazado = 8,
        Reversado = 16,
        EnEdicion = 32,
        Verificado = 64,
        Aprobado = 128,
        PorAcreditar = 256
    }
    public enum ServiciosPEL
    {
        NBS = 0,    //Núcleo Básico del Sistema
        DBD = 1,    //Débitos Directos
        CRD = 2,    //Créditos Directos    => T + 0
        ADB = 3,    //Autorizaciones de Débito
        DBR = 4,    //Débitos en Tiempo Real
        TFO = 5,    //Transferencias en Tiempo Real
        ARM = 7,    //Administrador de Reportes de MONEX
        CCS = 8,    //Centro de Seguridad
        CDI = 9,    //Consulta de Datos de Identificación
        SGD = 10,   //Servicios de Gobierno Digital
        CTS = 11,   //Cobros y Tarifas SINPE
        COV = 12,   //Cámara de Otros Valores
        CHQ = 13,   //Cheques
        SFD = 14,   //Visor de Firma Digital
        PMB = 15    //Pagos Monedero Bancario
    }
    public enum EstadosCreDebDirectoPEL
    {
        EnEdicion = 1,
        Verificado = 2,
        Aprobado = 4,
        Enviado = 8,
        Liquidado = 16,
        Devuelto = 32,
        Aplazado = 64,
        Excluido = 128,
        Cancelado = 256,
        Aceptado = 512,
        EnEspera = 2048,        //Por Acreditar (En Espera).
        Remitido = 4096
    }
    public enum EnumTipoSaldoFavor
    {
        Porcentaje = 1407,
        Monto = 1408
    }

    public enum EnumTipoErrorGeo
    {
        FormatoFechaNoValido = 1001,
        CampoPlateRequerido = 1002,
        CampoValueRequerido = 1003,
        CampoCurrencyTypeRequerido = 1004,
        CampoCardNumberRequerido = 1005,
        CampoNameCardHolderRequerido = 1006,
        DispositivoNoCoincide = 1007,
        NoHayDatosAnteriores = 0
    }
    
    public enum EnumIdContextoNotificacion
    {
        Autorizacion = 636,
        Denegacion = 638,
        BloqueosTarjeta = 640,       
        PanelControl = 641,
        Gestion = 645
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_TraductorMap : EntityTypeConfiguration<GL_Traductor>
    {
        public GL_TraductorMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTraductor);

            // Properties
            this.Property(t => t.IdTraductor)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Contexto)
                .IsRequired()
                .HasMaxLength(60);

            this.Property(t => t.Traduccion)
                .IsRequired()
                .HasMaxLength(2000);

            this.Property(t => t.Abreviatura)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GL_Traductor", "General");
            this.Property(t => t.IdTraductor).HasColumnName("IdTraductor");
            this.Property(t => t.IdIdioma).HasColumnName("IdIdioma");
            this.Property(t => t.IdCategoria).HasColumnName("IdCategoria");
            this.Property(t => t.Contexto).HasColumnName("Contexto");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.Traduccion).HasColumnName("Traduccion");
            this.Property(t => t.Abreviatura).HasColumnName("Abreviatura");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

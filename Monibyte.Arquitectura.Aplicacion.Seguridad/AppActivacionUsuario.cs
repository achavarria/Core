﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Encryption;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Poco.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Monibyte.Arquitectura.Dominio.Tarjetas;


namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public class AppActivacionUsuario : AplicacionBase, IAppActivacionUsuario
    {
        private IRepositorioUsuario _repUsuario;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;
        private IRepositorioMnb<SC_ActivacionUsuario> _repActivacionUsuario;
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IRepositorioMnb<TC_Tarjeta> _repTarjeta;

        public AppActivacionUsuario(
            IRepositorioUsuario repUsuario,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema,
            IRepositorioMnb<SC_ActivacionUsuario> repActivacionUsuario,
            IRepositorioMnb<GL_Entidad> repEntidad,
            IRepositorioMnb<TC_Tarjeta> repTarjeta)
        {
            _repUsuario = repUsuario;
            _repParamSistema = repParamSistema;
            _repActivacionUsuario = repActivacionUsuario;
            _repEntidad = repEntidad;
            _repTarjeta = repTarjeta;                 
        }

        public void Insertar(PocActivacionUsuario registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<SC_ActivacionUsuario>();
                var existente = _repActivacionUsuario.SelectById(registro.IdUsuario);
                if (existente == null)
                {
                    entrada.IdUsuario = registro.IdUsuario;
                    entrada.CodActivacion = registro.CodActivacion;
                    entrada.IdEstado = registro.IdEstado;
                    _repActivacionUsuario.Insert(entrada);
                }
                else
                {
                    _repActivacionUsuario.UpdateOn(x => x.IdUsuario == registro.IdUsuario,
                        p => new SC_ActivacionUsuario
                        {
                            CodActivacion = registro.CodActivacion,
                            IdEstado = (int)EnumIdEstado.Activo,
                            FecInclusionAud = DateTime.Now
                        });
                }
                _repActivacionUsuario.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public bool ValidaCodigoActivacion(string codActivacion)
        {
            var existente = _repActivacionUsuario.Table.Any(x =>
                x.CodActivacion == codActivacion &&
                x.IdEstado == (int)EnumIdEstado.Activo);
            if (existente)
            {
                return true;
            }
            string detalle = "Errores al activar el usuario";
            throw new CoreException(detalle, "Err_0094");
        }

        public bool ValidaAccesoUsuario(PocActivacionUsuario registro)
        {
            bool usuarioValido = false;

            //Valido que exista el usuario
            var usuario = _repUsuario.ObtenerUsuarioPorCodigo(registro.CodUsuario);
            if (usuario != null)
            {
                //Valido que exista Usuario con respectiva codigo
                var existente = _repActivacionUsuario.Table
                    .FirstOrDefault(x =>
                        x.IdUsuario == usuario.IdUsuario &&
                        x.CodActivacion == registro.CodActivacion &&
                        x.IdEstado == (int)EnumIdEstado.Activo);
                //Continua validacion solo si existe registro
                if (existente != null)
                {
                    DateTime fechaInicio = existente.FecActualizaAud == null ?
                        (DateTime)existente.FecInclusionAud :
                        (DateTime)existente.FecActualizaAud;
                    TimeSpan ts = DateTime.Now - fechaInicio;
                    int diferenciaHoras = ts.Hours;
                    int maxPeriodoVigencia = Convert.ToInt32(_repParamSistema
                        .SelectById("PERIODO_VIGENCIA_ACTIVACION").ValParametro);
                    //Continua validacion solo si el registro tiene menos del periodo de vigencia
                    if (diferenciaHoras < maxPeriodoVigencia)
                    {
                        usuarioValido = true;
                        registro.IdUsuario = usuario.IdUsuario;
                        //Inactivo codigo de activación
                        registro.IdUsuario = usuario.IdUsuario;
                        registro.IdEstado = (int)EnumIdEstado.Inactivo;
                        var entrada = registro.ProyectarComo<SC_ActivacionUsuario>();
                        _repActivacionUsuario.UpdateExclude(entrada,
                            ex => ex.IdUsuario,
                            ex => ex.CodActivacion,
                            ex => ex.FecInclusionAud);
                        _repActivacionUsuario.UnidadTbjo.Save();
                    }
                    else
                    {
                        string detalle = "Errores al activar el usuario";
                        throw new CoreException(detalle, "Err_0095");
                    }
                }
                else
                {
                    string detalle = "Errores al activar el usuario";
                    throw new CoreException(detalle, "Err_0094");
                }

            }
            else
            {
                string detalle = "Errores al activar el usuario";
                throw new CoreException(detalle, "Err_0023");
            }

            return usuarioValido;
        }           

    }
}

﻿
namespace Monibyte.Arquitectura.Dominio.Generales.Repositorios
{
    public interface IRepositorioUsuario : IRepositorioMnb<GL_Usuario>
    {
        GL_Usuario ObtenerUsuarioPorCodigo(string aliasUsuario);
        void ActualizaEstado(int idUsuario, int idEstado);
        bool ExisteUsuario(string codUsuario);
    }
}
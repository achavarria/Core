﻿using System;

namespace Monibyte.Arquitectura.Dominio.Efectivo
{
    public class ParamMovimientosEf
    {
        public int IdEmpresa { get; set; }
        public int? IdPersona { get; set; }
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
        public ParamMovimientosEfGe FiltroGestiones { get; set; }
    }
    public class ParamMovimientosEfGe
    {
        public int[] IdMovimientos { get; set; }
        public int[] Gestiones { get; set; }
        public bool? MatchExists { get; set; }
    }
}

﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Cargos;

    public class ContenedorUnityCargos
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            _unityContainer.RegisterType<IAppTipoCargo, AppTipoCargo>();
            _unityContainer.RegisterType<IAppRangosTabla, AppRangosTabla>();
            _unityContainer.RegisterType<IAppDetRangosCargo, AppDetRangosCargo>();
            _unityContainer.RegisterType<IAppCargosPorPersona, AppCargosPorPersona>();
            _unityContainer.RegisterType<IAppDetRangosCargosPer, AppDetRangosCargosPer>();
            _unityContainer.RegisterType<IAppValOrigenCargo, AppValOrigenCargo>();
            _unityContainer.RegisterType<IAppValOrigenCargoPer, AppValOrigenCargoPer>();
            _unityContainer.RegisterType<IAppCargosGeolocalizacion, AppCargosGeolocalizacion>();
            #endregion
        }
    }
}

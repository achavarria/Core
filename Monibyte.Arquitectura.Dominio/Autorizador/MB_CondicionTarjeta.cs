using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Autorizador
{
    public partial class MB_CondicionTarjeta : EntidadBaseCtrl
    { 
        public int IdCondicionTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public int IdEmpresa { get; set; }
        public int IdEstado { get; set; }
        public Nullable<int> SinRestriccion { get; set; }
        public int EsVip { get; set; }
        public Nullable<decimal> MonLimite { get; set; }
        public int ConsumeColones { get; set; }
        public int ConsumeDolares { get; set; }
        public int ConsumeLocal { get; set; }
        public int ConsumeInternacional { get; set; }
        public int ConsumeInternet { get; set; }
        public int PermiteAvanceEfectivo { get; set; }
        public int RestringeMcc { get; set; }
        public int RestringeHorario { get; set; }
        public int RestringeRegion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> SaldoCantTranDiaria { get; set; }
        public Nullable<decimal> SaldoMonTranDiaria { get; set; }
        public Nullable<int> SaldoCantTranSemanal { get; set; }
        public Nullable<decimal> SaldoMonTranSemanal { get; set; }
        public Nullable<int> SaldoCantTranMensual { get; set; }
        public Nullable<decimal> SaldoMonTranMensual { get; set; }
        public Nullable<int> SaldoCantAvanceEfecDia { get; set; }
        public Nullable<decimal> SaldoMonAvanceEfecDia { get; set; }
        public Nullable<int> SaldoCantTranCuasiDia { get; set; }
        public Nullable<decimal> SaldoMonTranCuasiDia { get; set; }
        public Nullable<int> IdFactorInterno { get; set; }
        public int IdMoneda { get; set; }
    }
}

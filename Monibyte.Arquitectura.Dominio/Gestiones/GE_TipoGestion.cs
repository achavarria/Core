using System;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_TipoGestion : EntidadBase
    {
        public int IdTipoGestion { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int IdSistema { get; set; }
        public Nullable<int> AplicaA { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int ReqAdministracion { get; set; }
        public Nullable<int> IdEntidad { get; set; }
        public string PlantillaDetalle { get; set; }
        public Nullable<int> IdVisualizaAdmin { get; set; }
    }
}

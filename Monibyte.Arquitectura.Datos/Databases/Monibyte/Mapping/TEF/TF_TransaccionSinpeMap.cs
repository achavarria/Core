﻿using System.ComponentModel.DataAnnotations.Schema;
using Monibyte.Arquitectura.Dominio.TEF;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.TEF
{
    public class TF_TransaccionSinpeMap : EntityTypeConfiguration<TF_TransaccionSinpe>
    {
        public TF_TransaccionSinpeMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTransaccion);

            // Properties
            this.Property(t => t.IdTransaccion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodEntidadOrigen)
                .HasMaxLength(5);

            this.Property(t => t.IdentificacionOrigen)
                .HasMaxLength(25);

            this.Property(t => t.CuentaClienteOrigen)
                .HasMaxLength(25);

            this.Property(t => t.CodEntidadDestino)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.IdentificacionDestino)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.CuentaClienteDestino)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.Detalle)
                .HasMaxLength(300);

            this.Property(t => t.CodReferenciaSinpe)
                .HasMaxLength(25);

            this.Property(t => t.UsuarioRegistra)
                .HasMaxLength(30);

            this.Property(t => t.DescripcionRechazo)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("TF_TransaccionSinpe", "TEF");
            this.Property(t => t.IdTransaccion).HasColumnName("IdTransaccion");
            this.Property(t => t.IdOrigenInterno).HasColumnName("IdOrigenInterno");
            this.Property(t => t.CodServicio).HasColumnName("CodServicio");
            this.Property(t => t.CodEntidadOrigen).HasColumnName("CodEntidadOrigen");
            this.Property(t => t.IdentificacionOrigen).HasColumnName("IdentificacionOrigen");
            this.Property(t => t.CuentaClienteOrigen).HasColumnName("CuentaClienteOrigen");
            this.Property(t => t.IdMonedaOrigen).HasColumnName("IdMonedaOrigen");
            this.Property(t => t.CodEntidadDestino).HasColumnName("CodEntidadDestino");
            this.Property(t => t.IdentificacionDestino).HasColumnName("IdentificacionDestino");
            this.Property(t => t.CuentaClienteDestino).HasColumnName("CuentaClienteDestino");
            this.Property(t => t.IdMonedaDestino).HasColumnName("IdMonedaDestino");
            this.Property(t => t.Comision).HasColumnName("Comision");
            this.Property(t => t.MontoPago).HasColumnName("MontoPago");
            this.Property(t => t.MontoTrasaccion).HasColumnName("MontoTrasaccion");
            this.Property(t => t.Detalle).HasColumnName("Detalle");
            this.Property(t => t.CodReferenciaSinpe).HasColumnName("CodReferenciaSinpe");
            this.Property(t => t.CodRefCoreBancario).HasColumnName("CodRefCoreBancario");
            this.Property(t => t.UsuarioRegistra).HasColumnName("UsuarioRegistra");
            this.Property(t => t.FecValor).HasColumnName("FecValor");
            this.Property(t => t.FecLiquidacion).HasColumnName("FecLiquidacion");
            this.Property(t => t.IdEstadoTrans).HasColumnName("IdEstadoTrans");
            this.Property(t => t.EstadoSINPE).HasColumnName("EstadoSINPE");
            this.Property(t => t.DetalleError).HasColumnName("DetalleError");
            this.Property(t => t.CodMotivoRechazo).HasColumnName("CodMotivoRechazo");
            this.Property(t => t.DescripcionRechazo).HasColumnName("DescripcionRechazo");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.MontoNoDistribuido).HasColumnName("MontoNoDistribuido");
        }
    }
}

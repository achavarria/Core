﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Aplicacion.Tarjetas;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Dominio.Efectivo.Repositorios;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Gestiones.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Gestiones
{
    public class AppGestionesGenerales : AplicacionBase, IAppGestionesGenerales
    {
        private IRepositorioMoneda _repMoneda;
        private IMovimientosTarjeta _appMovsTarjeta;
        private IRepositorioMovimientos _repMovEfectivo;
        private IRepositorioMovimientosGestion _repMovsGestion;
        private IAppGenerales _appGenerales;
        private IAppGestiones _appGestiones;
        private IRepositorioMnb<GL_Reportes> _repReporte;
        private IRepositorioGestion _repGestion;
        private IRepositorioMnb<GE_TipoGestion> _repTipoGestion;
        private IRepositorioMnb<GL_Parametro> _repParametro;
        private IRepositorioMnb<GL_ParametroEmpresa> _repParamEmpresa;
        private IRepositorioMnb<GE_ParametrosGestion> _repParamGestion;
        private IRepositorioMnb<GL_ParametroEmpresaLista> _repParamEmpresaList;
        private IRepositorioMnb<GE_ParametroGestionEmpresa> _repParamGestionEmpresa;
        private IParametroEmpresa _repParametroEmpresa;
        private IRepositorioMnb<CL_PerfilEmpresa> _repPerfilEmpresa;
        private IRepositorioMnb<GL_ParametroEmpresaContexto> _repParametroEmpresaContexto;
        private IRepositorioMnb<TC_MovimientoCorte> _movCorte;
        private IRepositorioMnb<TC_MovimientoCorteHist> _movCorteHist;

        public AppGestionesGenerales(
            IRepositorioMoneda repMoneda,
            IMovimientosTarjeta appMovsTarjeta,
            IRepositorioMovimientos repMovEfectivo,
            IRepositorioMovimientosGestion repMovsGestion,
            IAppGenerales appGenerales,
            IAppGestiones appGestiones,
            IRepositorioMnb<GL_Reportes> repReporte,
            IRepositorioGestion repGestion,
            IRepositorioMnb<GE_TipoGestion> repTipoGestion,
            IRepositorioMnb<GL_Parametro> repParametro,
            IRepositorioMnb<GL_ParametroEmpresa> repParamEmpresa,
            IRepositorioMnb<GE_ParametrosGestion> repParamGestion,
            IRepositorioMnb<GL_ParametroEmpresaLista> repParamEmpresaList,
            IRepositorioMnb<GE_ParametroGestionEmpresa> repParamGestionEmpresa,
            IParametroEmpresa repParametroEmpresa,
            IRepositorioMnb<CL_PerfilEmpresa> repPerfilEmpresa,
            IRepositorioMnb<GL_ParametroEmpresaContexto> repParametroEmpresaContexto,
            IRepositorioMnb<TC_MovimientoCorte> movCorte,
            IRepositorioMnb<TC_MovimientoCorteHist> movCorteHist)
        {
            _repMoneda = repMoneda;
            _appMovsTarjeta = appMovsTarjeta;
            _repMovEfectivo = repMovEfectivo;
            _repMovsGestion = repMovsGestion;
            _appGenerales = appGenerales;
            _appGestiones = appGestiones;
            _repReporte = repReporte;
            _repGestion = repGestion;
            _repTipoGestion = repTipoGestion;
            _repParametro = repParametro;
            _repParamEmpresa = repParamEmpresa;
            _repParamGestion = repParamGestion;
            _repParamEmpresaList = repParamEmpresaList;
            _repParamGestionEmpresa = repParamGestionEmpresa;
            _repPerfilEmpresa = repPerfilEmpresa;
            _repParametroEmpresa = repParametroEmpresa;
            _repParametroEmpresaContexto = repParametroEmpresaContexto;
            _movCorte = movCorte;
            _movCorteHist = movCorteHist;
        }

        public IEnumerable<PocMovimientosLiquidacion> ObtenerMovimientosLiquidacion(PocParametrosLiquidacion parametros)
        {
            var resultado = new List<PocMovimientosLiquidacion>();
            if (parametros != null)
            {
                int? contexto = null;
                if (parametros.IdContexto != (int)EnumContexto.LiquidacionGastos)
                {
                    contexto = parametros.IdContexto;
                }
                if (parametros.IdGestiones != null && parametros.MarcarExportados == true)
                {
                    _appGestiones.MarcarExportadas(parametros.IdGestiones);
                }

                if (!contexto.HasValue || contexto == (int)EnumContexto.TC_MOVIMIENTO)
                {
                    var criteria = new Criteria<TC_vwMovimientosTarjeta>();
                    int[] _movsTar = null;
                    if (parametros.IdsMovs != null)
                    {
                        _movsTar = parametros.IdsMovs.Where(m =>
                            m.IdContexto == (int)EnumContexto.TC_MOVIMIENTO)
                        .Select(m => m.IdMovimiento).ToArray();
                        _movsTar = _movsTar.Any() ? _movsTar : null;
                    }

                    var perfilEmpresa = _repPerfilEmpresa.SelectById(InfoSesion.Info.IdEmpresa);
                    if (perfilEmpresa != null && perfilEmpresa.IdRepMovTC.HasValue)
                    {
                        var reporte = _repReporte.Table.FirstOrDefault(x =>
                            x.IdReporte == perfilEmpresa.IdRepMovTC);
                        if (reporte != null && !string.IsNullOrEmpty(reporte.TipoMovimiento))
                        {
                            var tipos = reporte.TipoMovimiento.FromJson<int[]>();
                            criteria.And(m => tipos.Contains(m.IdTipoMovimiento));
                        }
                    }
                    resultado.AddRange(_appMovsTarjeta.QueryMovimientosTarjeta(new ParamMovimientosTc
                    {
                        IdUsuario = InfoSesion.Info.IdUsuario,
                        IdEmpresa = InfoSesion.Info.IdEmpresa,
                        IdCuenta = parametros.IdCuenta,
                        IdTarjeta = parametros.IdTarjeta,
                        FecDesde = parametros.FechaDesde,
                        FecHasta = parametros.FechaHasta,
                        FiltroGestiones = new ParamMovimientosGe
                        {
                            Gestiones = parametros.IdGestiones,
                            IdMovimientos = _movsTar,
                            MatchExists = parametros.BuscarMovimientos
                        }
                    }, parametros.Personas, criteria).ToList()
                    .Select(movs =>
                    {
                        var res = movs.ProyectarComo<PocMovimientosLiquidacion>();
                        res.IdContexto = (int)EnumContexto.TC_MOVIMIENTO;
                        res.TipoMovimiento = "Tarjeta";
                        res.NumFactura = movs.NumReferencia;
                        res.NombreCompleto = movs.NombreImpreso;
                        res.DetalleMovimiento = movs.Descripcion;
                        res.FecTransaccion = movs.FecTransaccion.Date;
                        res.FecMovimiento = movs.FecMovimiento.Value.Date;
                        res.MonLocal = movs.MonMovimientoLocal ?? 0;
                        res.MonInter = movs.MonMovimientoInter ?? 0;
                        return res;
                    }));
                    if (contexto == (int)EnumContexto.TC_MOVIMIENTO && perfilEmpresa != null &&
                        perfilEmpresa.IdValidaEdicionMov == (int)EnumIdSiNo.Si &&
                    resultado.Any(x => x.CodEditado != "V"))
                    {

                        var infParam = _repParamEmpresa.Table
                            .Where(w => w.IdEmpresa == InfoSesion.Info.IdEmpresa)
                            .Join(_repParametroEmpresaContexto.Table,
                             pe => new
                             {
                                 pe.IdParametro,
                                 IdContexto = contexto.Value,
                                 IdEstado = (int)EnumIdEstado.Activo,
                                 IdEsRequerido = (int)EnumIdSiNo.Si
                             },
                             pec => new
                             {
                                 pec.IdParametro,
                                 pec.IdContexto,
                                 pec.IdEstado,
                                 pec.IdEsRequerido
                             },
                        (pe, pec) => new { pe, pec })
                        .Any(w => w.pe.IdParametro > 0);

                        if (infParam)
                        {
                            throw new CoreException("Uno o más movimientos sin personalizar", "Err_0077");
                        }
                    }
                }
                if (!contexto.HasValue || (contexto == (int)EnumContexto.EF_MOVIMIENTO ||
                    contexto == (int)EnumContexto.EF_KILOMETRAJE))
                {
                    var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
                    var entidad = _appGenerales.ObtieneEntidad(idEntidad);
                    var criteriaEfect = new Criteria<EF_vwMovimiento>();
                    criteriaEfect.And(m =>
                        m.IdContexto == (int)EnumContexto.EF_MOVIMIENTO ||
                        m.IdContexto == (int)EnumContexto.EF_KILOMETRAJE);

                    int[] _movsEfec = null;
                    if (parametros.IdsMovs != null)
                    {
                        _movsEfec = parametros.IdsMovs.Where(m =>
                            m.IdContexto == (int)EnumContexto.EF_MOVIMIENTO ||
                            m.IdContexto == (int)EnumContexto.EF_KILOMETRAJE)
                        .Select(m => m.IdMovimiento).ToArray();
                        _movsEfec = _movsEfec.Any() ? _movsEfec : null;
                    }
                    if (parametros.BuscarMovimientos == false)
                    {
                        criteriaEfect.And(m => m.IdPersona == parametros.IdPersonaLiq);
                    }

                    var query =
                        from mov in _repMovEfectivo.ObtenerMovimientos(new ParamMovimientosEf
                        {
                            IdEmpresa = InfoSesion.Info.IdEmpresa,
                            FecDesde = parametros.FechaDesde,
                            FecHasta = parametros.FechaHasta,
                            FiltroGestiones = new ParamMovimientosEfGe
                            {
                                IdMovimientos = _movsEfec,
                                Gestiones = parametros.IdGestiones,
                                MatchExists = parametros.BuscarMovimientos
                            }
                        }).Where(criteriaEfect.Satisfecho().Compile())
                        join tar in parametros.Personas on mov.IdPersona equals tar.IdPersona
                        join mon in _repMoneda.Table on mov.IdMoneda equals mon.IdMoneda
                        select new PocMovimientosLiquidacion
                        {
                            IdMovimiento = mov.IdMovimiento,
                            IdMoneda = mov.IdMoneda.Value,
                            IdDebCredito = mov.IdDebCredito,
                            TipoMovimiento = "Efectivo",
                            IdEditado = (int)EnumIdSiNo.Si,
                            IdContexto = mov.IdContexto,
                            NumReferencia = mov.NumFactura,
                            DescripcionMoneda = mon.Descripcion,
                            SimboloMonedaLocal = entidad.MonedaLocal.Simbolo,
                            SimboloMonedaInter = entidad.MonedaInternacional.Simbolo,
                            FecMovimiento = mov.FecMovimiento,
                            CodMoneda = mon.CodInternacional,
                            NombreCompleto = mov.NombreCompleto,
                            FecTransaccion = mov.FecMovimiento,
                            HoraTransaccion = mov.FecMovimiento,
                            DetalleMovimiento = mov.DetalleMovimiento,
                            Descripcion = mov.DetalleMovimiento,
                            MonLocal = entidad.MonedaInternacional.IdMoneda == mov.IdMoneda
                                ? 0 : (decimal)mov.MonMovimiento,
                            MonInter = entidad.MonedaInternacional.IdMoneda == mov.IdMoneda
                                ? (decimal)mov.MonMovimiento : 0,
                            MonMovimiento = mov.MonMovimiento,
                            NumFactura = mov.NumFactura,
                            CodUsuarioEmpresa = mov.CodUsuarioEmpresa,
                            DebitoCredito = mov.IdDebCredito == (int)EnumDebitoCredito.Debito ?
                                DebitoCredito.Debito : DebitoCredito.Credito,
                            NombreImpreso = tar.NombreImpreso,
                            NumTarjeta = tar.NumTarjeta,
                            MonCredInter = mov.IdDebCredito == (int)EnumDebitoCredito.Credito &&
                                    entidad.MonedaInternacional.IdMoneda == mov.IdMoneda ? (decimal)mov.MonMovimiento : 0,
                            MonDebInter = mov.IdDebCredito == (int)EnumDebitoCredito.Debito &&
                                    entidad.MonedaInternacional.IdMoneda == mov.IdMoneda ? (decimal)mov.MonMovimiento : 0,
                            MonDebLocal = mov.IdDebCredito == (int)EnumDebitoCredito.Debito &&
                                    entidad.MonedaLocal.IdMoneda == mov.IdMoneda ? (decimal)mov.MonMovimiento : 0,
                            MonCredLocal = mov.IdDebCredito == (int)EnumDebitoCredito.Credito &&
                                    entidad.MonedaLocal.IdMoneda == mov.IdMoneda ? (decimal)mov.MonMovimiento : 0,
                            DynamicJson = mov.CamposDinamicos,
                            MesMovimiento = mov.FecMovimiento.ToString("MM"),
                            AnoMovimiento = mov.FecMovimiento.ToString("yyyy"),
                            ArchivoAdjunto = mov.ArchivoAdjunto.HasValue,
                            NumKmSalida = mov.NumKmSalida,
                            NumKmLlegada = mov.NumKmLlegada,
                            IdPersona = mov.IdPersona,
                            IdEstado = mov.IdEstado,
                            IdEmpresa = mov.IdEmpresa,
                            SoloConsulta = tar.SoloConsulta,
                            IdTipoVehiculo = mov.IdTipoVehiculo,
                            Modelo = mov.Modelo,
                            Placa = mov.Placa,
                            IdLiquidado = mov.IdLiquidado,
                            IdGestion = mov.IdGestion
                        };
                    resultado.AddRange(query);
                }
                return resultado.OrderByDescending(p => p.FecMovimiento);
            }
            return null;
        }

        public PocRepuestaLiquidacion GuardarLiquidacionGastos(PocLiquidacionGastos entrada, int? idReporte)
        {
            var datosSalida = new PocRepuestaLiquidacion();
            using (var ts = new TransactionScope())
            {
                var movs = entrada.IdsMovs.ProyectarComo<PocMovimientosDeLiquidacion>();
                if (entrada.IdGestion != null)
                {
                    _appGestiones.ModificarValorParametro(entrada.IdGestion.Value, entrada.DynamicJson);
                    _repGestion.UpdateOn(x => x.IdGestion == entrada.IdGestion,
                        z => new GE_Gestion
                        {
                            FecInclusion = entrada.FecRegreso.Value,
                            IdEstado = entrada.IdEstado.Value
                        });
                }
                else
                {
                    var detalle = _appGestiones.ObtenerDetalleTipoGestion(EnumTipoGestion.LiquidacionDeGastos);
                    entrada.IdGestion = _appGestiones.InsertaGestion(EnumTipoGestion.LiquidacionDeGastos,
                        (EnumEstadoGestion)entrada.IdEstado, EnumContexto.TC_TARJETA, entrada.IdTarjeta.Value, detalle, entrada.FecRegreso,
                        idContextoMovimientos: entrada.IdContextoMovimientos);
                    _appGestiones.IncluirValorParametro(entrada.IdGestion.Value, entrada.DynamicJson);
                }
                if (movs != null && movs.Any())
                {
                    foreach (var item in movs)
                    {
                        _repMovsGestion.Insert(new GE_MovimientoGestion
                        {
                            IdGestion = entrada.IdGestion.Value,
                            IdMovimiento = item.IdMovimiento,
                            IdContexto = item.IdContexto
                        });
                    }
                }
                datosSalida.IdGestion = entrada.IdGestion.Value;
                _repMovsGestion.UnidadTbjo.Save();
                ts.Complete();
            }
            return datosSalida;
        }

        public void EliminarMovimientoLiquidacion(int idGestion, PocMovimientosDeLiquidacion[] movimientos)
        {
            using (var ts = new TransactionScope())
            {
                foreach (var movimiento in movimientos)
                {
                    _repMovsGestion.DeleteOn(x =>
                        x.IdGestion == idGestion &&
                        x.IdContexto == movimiento.IdContexto &&
                        x.IdMovimiento == movimiento.IdMovimiento);

                    if (movimiento.IdContexto == (int)EnumContexto.TC_MOVIMIENTO)
                    {
                        var esActual = _movCorte.Table.FirstOrDefault(x => x.IdMovimiento == movimiento.IdMovimiento);
                        if (esActual != null)
                        {
                            _movCorte.UpdateOn(x => x.IdMovimiento == movimiento.IdMovimiento,
                                z => new TC_MovimientoCorte
                                {
                                    IdEditado = string.IsNullOrEmpty(esActual.CamposDinamicos) ?
                                            (int)EnumEstadoEdicion.Pendiente : (int)EnumEstadoEdicion.Editado
                                });
                            _movCorte.UnidadTbjo.Save();
                        }
                        else
                        {
                            var movtH = _movCorteHist.Table.FirstOrDefault(x => x.IdMovimiento == movimiento.IdMovimiento);
                            _movCorteHist.UpdateOn(x => x.IdMovimiento == movimiento.IdMovimiento,
                                z => new TC_MovimientoCorteHist
                                {
                                    IdEditado = string.IsNullOrEmpty(movtH.CamposDinamicos) ?
                                            (int)EnumEstadoEdicion.Pendiente : (int)EnumEstadoEdicion.Editado
                                });
                            _movCorteHist.UnidadTbjo.Save();
                        }
                    }

                }
                _repMovsGestion.UnidadTbjo.Save();
                ts.Complete();

            }
        }

        public void CerrarLiqGastos(int idGestion)
        {
            using (var ts = new TransactionScope())
            {
                var cambioEstado = new PocCambioEstadoGestion
                {
                    Justificacion = "Cerrada",
                    IdEstado = (int)EnumEstadoGestion.Cerrada,
                    IdGestion = idGestion
                };

                var movs = _repMovsGestion.Table.Where(x => x.IdGestion == idGestion);
                if (movs != null && movs.Any())
                {
                    foreach (var item in movs)
                    {
                        if (item.IdContexto == (int)EnumContexto.TC_MOVIMIENTO)
                        {
                            var esActual = _movCorte.Table.Any(x => x.IdMovimiento == item.IdMovimiento);
                            if (esActual)
                            {
                                _movCorte.UpdateOn(x => x.IdMovimiento == item.IdMovimiento,
                                    z => new TC_MovimientoCorte
                                    {
                                        IdEditado = (int)EnumEstadoEdicion.Liquidado
                                    });
                                _movCorte.UnidadTbjo.Save();
                            }
                            else
                            {
                                _movCorteHist.UpdateOn(x => x.IdMovimiento == item.IdMovimiento,
                                    z => new TC_MovimientoCorteHist
                                    {
                                        IdEditado = (int)EnumEstadoEdicion.Liquidado
                                    });
                                _movCorteHist.UnidadTbjo.Save();
                            }
                        }
                    }
                }
                _appGestiones.ModificarEstado(cambioEstado);
                ts.Complete();
            }
        }

        public IEnumerable<PocParametrosGestionEmpresa> ObtenerParamGestionEmpresa(
            int? idTipoGestion, bool reqTodo = false)
        {
            var idEmpresa = InfoSesion.Info.IdEmpresa;
            var resultado = _repParamGestion.Table
                .Join(_repParametro.Table,
                    pg => pg.IdParametro,
                    pa => pa.IdParametro,
                    (pg, pa) => new { pg, pa })
                .Join(_repTipoGestion.Table,
                    tmp => tmp.pg.IdTipoGestion,
                    tg => tg.IdTipoGestion,
                    (tmp, tg) => new { tmp.pg, tmp.pa, tg })
                .GroupJoin(_repParamEmpresa.Table,
                    tmp => new { IdEmpresa = idEmpresa, IdParametro = tmp.pg.IdParametro },
                    pe => new { pe.IdEmpresa, IdParametro = pe.IdParametroInterno ?? 0 },
                    (tmp, pe) => new
                    {
                        tmp.pg,
                        tmp.pa,
                        tmp.tg,
                        pe = pe.Join(_repParamGestionEmpresa.Table,
                                a => new { tmp.pg.IdTipoGestion, a.IdEmpresa, a.IdParametro },
                                b => new { b.IdTipoGestion, b.IdEmpresa, IdParametro = b.IdParametroEmpresa },
                                (a, b) => new
                                {
                                    a.Nombre,
                                    a.IdParametro,
                                    b.ValorDefecto,
                                    b.IdTipoGestion
                                }).FirstOrDefault()
                    })
                .Where(x =>
                    x.tg.ReqAdministracion == (int)EnumIdSiNo.No &&
                    (!idTipoGestion.HasValue || x.tg.IdTipoGestion == idTipoGestion))
                .Select(tmp => new PocParametrosGestionEmpresa
                {
                    IdParametro = tmp.pg.IdParametro,
                    IdParametroEmpresa = tmp.pe != null ? tmp.pe.IdParametro : 0,
                    DescripcionGestion = _fGenerales.Traducir(tmp.pa.Descripcion,
                        "GL_Parametro", tmp.pa.IdParametro,
                        Context.Lang.Id, 0, Context.DefaultLang.Id),
                    ValorDefecto = tmp.pe != null ? tmp.pe.ValorDefecto : string.Empty,
                    DescripcionEmpresa = tmp.pe != null ? tmp.pe.Nombre : string.Empty,
                    DescripcionTipoGestion = tmp.tg != null ? _fGenerales.Traducir(
                            tmp.tg.Descripcion, "GE_TipoGestion", tmp.tg.IdTipoGestion,
                            Context.Lang.Id, 0, Context.DefaultLang.Id) : string.Empty,
                    IdTipoGestion = tmp.pe != null ? tmp.pe.IdTipoGestion : tmp.pg != null ? tmp.pg.IdTipoGestion : 0
                })
                .ToList() as IEnumerable<PocParametrosGestionEmpresa>;
            if (!reqTodo)
            {
                resultado = resultado.DistinctBy(m => m.IdParametro);
            }
            return resultado;
        }

        public IEnumerable<PocParametroEmpresa> ObtenerParametrosEmpresaGestion(
            EnumTipoGestion tipoGestion, EnumContexto contexto)
        {
            var idEmpresa = InfoSesion.Info.IdEmpresa;
            var resultado = _repParamGestion.Table
                .Join(_repParametro.Table,
                    pg => pg.IdParametro,
                    pa => pa.IdParametro,
                    (pg, pa) => new { pg, pa })
                .Join(_repTipoGestion.Table,
                    tmp => tmp.pg.IdTipoGestion,
                    tg => tg.IdTipoGestion,
                    (tmp, tg) => new { tmp.pg, tmp.pa, tg })
                .GroupJoin(_repParamEmpresa.Table,
                    tmp => new { IdEmpresa = idEmpresa, IdParametro = tmp.pg.IdParametro },
                    pe => new { pe.IdEmpresa, IdParametro = pe.IdParametroInterno ?? 0 },
                    (tmp, pe) => new
                    {
                        tmp.pg,
                        tmp.pa,
                        tmp.tg,
                        pe = pe.Join(_repParamGestionEmpresa.Table,
                            a => new { tmp.pg.IdTipoGestion, a.IdEmpresa, a.IdParametro },
                            b => new { b.IdTipoGestion, b.IdEmpresa, IdParametro = b.IdParametroEmpresa },
                            (a, b) => new { a, b })
                            .GroupJoin(_repParametroEmpresaContexto.Table,
                                stmp => new { IdContexto = (int)contexto, stmp.a.IdParametro },
                                cont => new { cont.IdContexto, cont.IdParametro },
                                (stmp, cont) => new { stmp.a, stmp.b, cont = cont.FirstOrDefault() })
                            .GroupJoin(_repParamEmpresaList.Table,
                                stmp => stmp.a.IdParametro,
                                pel => pel.IdParametroEmpresa,
                                (stmp, pel) => new { stmp.a, stmp.b, stmp.cont, pel })
                            .FirstOrDefault()
                    })
                .Where(x => x.tg.IdTipoGestion == (int)tipoGestion)
                .ToList()
                .Select(item => new PocParametroEmpresa
                {
                    IdParametro = item.pe != null ? item.pe.a.IdParametro : 0,
                    IdParametroInterno = item.pe != null ? item.pe.a.IdParametroInterno : null,
                    IdTipo = item.pe != null ? item.pe.a.IdTipo : 0,
                    Lista = item.pe != null ? _repParametroEmpresa.OrdenarParamLista(item.pe.a, item.pe.pel) : null,
                    IdDependeDe = item.pe != null ? item.pe.a.IdDependeDe : null,
                    ListaPadres = item.pe != null ? item.pe.a.IdDependeDe.HasValue ?
                        _repParametroEmpresa.ObtenerListasPadre(item.pe.a.IdDependeDe).ToList() : null : null,
                    Descripcion = item.pe != null ? item.pe.a.Descripcion : string.Empty,
                    Nombre = item.pe != null ? item.pe.a.Nombre : string.Empty,
                    IdOrden = item.pe != null && item.pe.cont != null ? item.pe.cont.IdOrden : null,
                    ValorDefecto = item.pe != null ? item.pe.cont != null ?
                        item.pe.cont.ValorDefecto ?? item.pe.b.ValorDefecto : item.pe.b.ValorDefecto : null,
                    IdOrdenadoPor = item.pe != null ? item.pe.a.IdOrdenadoPor : 0,
                    Ascendente = item.pe != null ? item.pe.a.Ascendente : 0,
                    DescParametro = item.pa.Descripcion
                });
            return resultado;
        }

        public void ModificarValorDefecto(IEnumerable<PocParametrosGestionEmpresa> datosEntrada)
        {
            var idEmpresa = InfoSesion.Info.IdEmpresa;
            using (var ts = new TransactionScope())
            {
                foreach (PocParametrosGestionEmpresa item in datosEntrada)
                {
                    var entrada = item.ProyectarComo<GE_ParametroGestionEmpresa>();
                    var exist = _repParamGestionEmpresa.Table.Any(m =>
                        m.IdTipoGestion == item.IdTipoGestion &&
                        m.IdParametroEmpresa == item.IdParametroEmpresa);
                    if (exist)
                    {
                        _repParamGestionEmpresa.Update(entrada, x => x.ValorDefecto);
                    }
                    else
                    {
                        entrada.IdEmpresa = idEmpresa;
                        if (!string.IsNullOrEmpty(entrada.ValorDefecto))
                        {
                            _repParamGestionEmpresa.Insert(entrada);
                        }
                    }
                }
                _repParamGestionEmpresa.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void GuardarParametrosEmpresa(List<PocParametrosGestionEmpresa> datosEntrada,
                                             List<PocParametrosGestionEmpresa> datosActuales)
        {
            datosActuales = datosActuales ?? new List<PocParametrosGestionEmpresa>();
            using (var ts = new TransactionScope())
            {
                if (!datosActuales.IsEqualTo(datosEntrada))
                {
                    if (datosActuales != null)
                    {
                        foreach (var itemPrevio in datosActuales)
                        {
                            _repParamEmpresa.UpdateOn(x => x.IdParametro == itemPrevio.IdParametroEmpresa,
                            p => new GL_ParametroEmpresa
                            {
                                IdParametroInterno = null
                            });

                        }
                    }
                    foreach (var item in datosEntrada)
                    {
                        _repParamEmpresa.UpdateOn(x => x.IdParametro == item.IdParametroEmpresa,
                            p => new GL_ParametroEmpresa
                            {
                                IdParametroInterno = item.IdParametro
                            });
                        ValidarParametroEmpresa(item);
                    }
                    _repParamEmpresa.UnidadTbjo.Save();
                    ts.Complete();
                }
            }
        }

        private void ValidarParametroEmpresa(PocParametrosGestionEmpresa item)
        {
            var idEmpresa = InfoSesion.Info.IdEmpresa;
            var paramG = _repParamGestionEmpresa.Table
                .FirstOrDefault(x => x.IdParametroEmpresa == item.IdParametroEmpresa &&
                x.IdEmpresa == idEmpresa);
            if (paramG == null)
            {
                var tiposG = _repParamGestion.Table.Where(x => x.IdParametro == item.IdParametro);
                foreach (var tipo in tiposG)
                {
                    var NuevoParametro = new GE_ParametroGestionEmpresa
                    {
                        IdParametroEmpresa = item.IdParametroEmpresa,
                        IdTipoGestion = tipo.IdTipoGestion,
                        IdEmpresa = idEmpresa,
                        ValorDefecto = null
                    };
                    _repParamGestionEmpresa.Insert(NuevoParametro);
                }
                _repParamGestionEmpresa.UnidadTbjo.Save();
            }
        }

        public string ObtieneDescripcionGestion(int idTipoGestion)
        {
            var datos = _repTipoGestion.Table.FirstOrDefault(
                x => x.IdTipoGestion == idTipoGestion);
            return datos != null ? datos.Descripcion : string.Empty;
        }

        public DateTime? ObtenerUltimoRangoLiquidacion(int idPersona)
        {
            var maxDate = _repGestion.Table
                .Where(x => x.IdPersona == idPersona &&
                    x.IdTipoGestion == (int)EnumTipoGestion.LiquidacionDeGastos)
                .Max(x => x.FecInclusion as DateTime?);
            return maxDate;
        }

        public void EliminarLiquidacion(int idGestion)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                _repMovsGestion.ActualizarIdLiquidadoMovimientosGestion(new ParamMovGestion
                {
                    IdGestion = idGestion,
                    IdLiquidado = (int)EnumIdSiNo.No
                });
                _repMovsGestion.DeleteOn(p => p.IdGestion == idGestion);
                _appGestiones.EliminarGestionRegistrada(idGestion);
                _repMovsGestion.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void ModificarEstadoLiquidacion(PocCambioEstadoGestion cambioEstado)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                var estadoActual = _repGestion.SelectById(cambioEstado.IdGestion).IdEstado;

                if (cambioEstado.IdEstado == (int)EnumEstadoGestion.PendienteAprobacion)
                {
                    var perfil = _repPerfilEmpresa.SelectById(InfoSesion.Info.IdEmpresa);
                    if (perfil != null && perfil.IdValidaEdicionMov == (int)EnumIdSiNo.Si)
                    {
                        if (_repParamEmpresa.Table.Any(x =>
                            x.IdEmpresa == InfoSesion.Info.IdEmpresa))
                        {
                            var i = _repMovsGestion.MovimientosSinEditar(cambioEstado.IdGestion);
                            if (i > 0)
                            {
                                throw new CoreException("Uno o más movimientos sin personalizar", "Err_0077");
                            }
                        }
                    }
                }
                if (cambioEstado.IdEstado == (int)EnumEstadoGestion.Anulada)
                {
                    _repMovsGestion.ActualizarIdLiquidadoMovimientosGestion(new ParamMovGestion
                    {
                        IdGestion = cambioEstado.IdGestion,
                        IdLiquidado = (int)EnumIdSiNo.No
                    });
                    _repMovsGestion.DeleteOn(x => x.IdGestion == cambioEstado.IdGestion);
                }
                if (cambioEstado.IdEstado == (int)EnumEstadoGestion.Aprobada)
                {
                    _repMovsGestion.ActualizarIdLiquidadoMovimientosGestion(new ParamMovGestion
                    {
                        IdGestion = cambioEstado.IdGestion,
                        IdLiquidado = (int)EnumIdSiNo.Si
                    });
                }
                if (estadoActual == (int)EnumEstadoGestion.Aprobada &&
                    cambioEstado.IdEstado == (int)EnumEstadoGestion.PendienteCorrecion)
                {
                    _repMovsGestion.ActualizarIdLiquidadoMovimientosGestion(new ParamMovGestion
                    {
                        IdGestion = cambioEstado.IdGestion,
                        IdLiquidado = (int)EnumIdSiNo.No
                    });
                }
                if (string.IsNullOrEmpty(cambioEstado.Justificacion))
                {
                    cambioEstado.Justificacion = ObtieneDescripcionGestion(cambioEstado.IdTipoGestion);
                }
                _appGestiones.ModificarEstado(cambioEstado);
                _repMovsGestion.UnidadTbjo.Save();
                ts.Complete();
            }
        }
    }
}
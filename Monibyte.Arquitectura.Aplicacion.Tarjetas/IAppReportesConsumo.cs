﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Tarjetas;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public interface IAppReportesConsumo : IAplicacionBase
    {
        byte[] ObtenerReporteConsumo(PocDetalleConsumo datosEntrada);
        byte[] ObtenerReporteEjecutivos(PocDetalleConsumo datosEntrada);
    }
}

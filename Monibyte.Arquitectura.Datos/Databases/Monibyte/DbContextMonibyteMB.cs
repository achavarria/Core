﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion;
using Monibyte.Arquitectura.Dominio.Configuracion;
using System.Data.Entity;
namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<MB_AdministradoresGrupo> MB_AdministradoresGrupo { get; set; }
        public DbSet<MB_CategoriaTipoComercio> MB_CategoriaTipoComercio { get; set; }
        public DbSet<MB_GruposEmpresa> MB_GruposEmpresa { get; set; }
        public DbSet<MB_HorarioPlantilla> MB_HorarioPlantilla { get; set; }
        public DbSet<MB_LicenciaPersona> MB_LicenciaPersona { get; set; }
        public DbSet<MB_MccPlantilla> MB_MccPlantilla { get; set; }
        public DbSet<MB_PaisesRegion> MB_PaisesRegion { get; set; }
        public DbSet<MB_Plantilla> MB_Plantilla { get; set; }
        public DbSet<MB_RangoCategoriaTipoComercio> MB_RangoCategoriaTipoComercio { get; set; }
        public DbSet<MB_Region> MB_Region { get; set; }
        public DbSet<MB_RegionPaisPlantilla> MB_RegionPaisPlantilla { get; set; }
        public DbSet<MB_TipoComercio> MB_TipoComercio { get; set; }
        public DbSet<MB_TipoLicencia> MB_TipoLicencia { get; set; }

        void CreateContextModelMB(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MB_AdministradoresGrupoMap());
            modelBuilder.Configurations.Add(new MB_CategoriaTipoComercioMap());
            modelBuilder.Configurations.Add(new MB_GruposEmpresaMap());
            modelBuilder.Configurations.Add(new MB_HorarioPlantillaMap());
            modelBuilder.Configurations.Add(new MB_LicenciaPersonaMap());
            modelBuilder.Configurations.Add(new MB_MccPlantillaMap());
            modelBuilder.Configurations.Add(new MB_PaisesRegionMap());
            modelBuilder.Configurations.Add(new MB_PlantillaMap());
            modelBuilder.Configurations.Add(new MB_RangoCategoriaTipoComercioMap());
            modelBuilder.Configurations.Add(new MB_RegionMap());
            modelBuilder.Configurations.Add(new MB_RegionPaisPlantillaMap());
            modelBuilder.Configurations.Add(new MB_TipoComercioMap());
            modelBuilder.Configurations.Add(new MB_TipoLicenciaMap());
        }
    }
}

using Monibyte.Arquitectura.Dominio.Gestiones;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Parametro : EntidadBase
    {
        public int IdParametro { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int IdTipoDato { get; set; }
        public Nullable<int> Longitud { get; set; }
        public int IdTipoOrigen { get; set; }
        public string IdOrigen { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

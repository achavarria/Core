﻿using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using System.Collections.Generic;
namespace Monibyte.Arquitectura.Dominio.Gestiones.Repositorios
{
    public interface IRepositorioGestion : IRepositorioMnb<GE_Gestion>
    {
        IEnumerable<GE_Gestion> ConsultaGestiones(
                        int? idTipoGestion = null,
                        int? idGestion = null,
                        int? idEmpresa = null,
                        int? idPersona = null,
                        int? idTipoProducto = null,
                        int? idProducto = null,
                        int[] filtroEstado = null,
                        int[] filtroEstadoExc = null,
                        Criteria<GE_Gestion> criteria = null);
    }

    public interface IRepositorioMovimientosGestion : IRepositorioMnb<GE_MovimientoGestion>
    {
        void ActualizarIdLiquidadoMovimientosGestion(ParamMovGestion parameters);
        int MovimientosSinEditar(int idGestion);
    }   
}
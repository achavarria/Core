using Monibyte.Arquitectura.Dominio.Clientes;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Notificacion
{
    public partial class NT_Configuracion : EntidadBase
    {
        public int IdConfiguracion { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> IdEmpresa { get; set; }
        public int IdEstado { get; set; }
        public int IdContexto { get; set; }
        public string SubContexto { get; set; }
        public int IdTipoNotificacion { get; set; }
        public Nullable<System.DateTime> FecVigenciaHasta { get; set; }        
        public string DetalleFiltro { get; set; }
        public string Destinatario { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int Orden { get; set; }
        public int IdNotificacion { get; set; }
        public int IdAdministracionInterna { get; set; }
        public int IdNotificaAdministradoresSms { get; set; }
        public int IdNotificaAdministradoresEmail { get; set; }
        public int IdAplicaTarjetaHabiente { get; set; }
        public int IdNotificaTarjetaHabienteSms { get; set; }
        public int IdNotificaTarjetaHabienteEmail { get; set; }
        public int IdAplicaUsuario { get; set; }
        public int IdNotificaUsuarioSms { get; set; }
        public int IdNotificaUsuarioEmail { get; set; }
    }
}

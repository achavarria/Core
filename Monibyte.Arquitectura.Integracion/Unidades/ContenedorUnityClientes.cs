﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos;

    public class ContenedorUnityClientes
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            _unityContainer.RegisterType<IAppOpPersona, AppOpPersona>();
            _unityContainer.RegisterType<IAppOpProfesion, AppOpProfesion>();
            _unityContainer.RegisterType<IAppOpComunicacion, AppOpComunicacion>();
            _unityContainer.RegisterType<IAppOpDireccion, AppOpDireccion>();
            _unityContainer.RegisterType<IAppOpPerfilEmpresa, AppOpPerfilEmpresa>();
            _unityContainer.RegisterType<IAppOpEjecutivo, AppOpEjecutivo>();
            #endregion

            #region Repositorios
            //_unityContainer.RegisterType<IRepositorioPersona, RepositorioPersona>();
            //_unityContainer.RegisterType<IRepositorioProfesion, RepositorioProfesion>();
            //_unityContainer.RegisterType<IRepositorioComunicacion, RepositorioComunicacion>();
            //_unityContainer.RegisterType<IRepositorioDireccion, RepositorioDireccion>();
            //_unityContainer.RegisterType<IRepositorioPerfilEmpresa, RepositorioPerfilEmpresa>();
            //_unityContainer.RegisterType<IRepositorioEjecutivo, RepositorioEjecutivo>();
            //_unityContainer.RegisterType<IRepositorioEmpresasUsuario, RepositorioEmpresasUsuario>();
            //_unityContainer.RegisterType<IRepositorioVwEmpresasUsuario, RepositorioVwEmpresasUsuario>();
            #endregion
        }
    }
}

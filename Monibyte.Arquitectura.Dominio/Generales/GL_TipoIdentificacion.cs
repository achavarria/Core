using Monibyte.Arquitectura.Dominio.Clientes;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_TipoIdentificacion : EntidadBase
    {
        public int IdIdentificacion { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<int> IdFormato { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

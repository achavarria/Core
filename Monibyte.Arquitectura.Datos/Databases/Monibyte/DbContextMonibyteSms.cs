﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.SMS;
using Monibyte.Arquitectura.Dominio.SMS;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<AC_MENSAJES_RESPUESTA> AC_MENSAJES_RESPUESTA { get; set; }
        public DbSet<AC_MENSAJES_SMS> AC_MENSAJES_SMS { get; set; }

        void CreateContextModelSms(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AC_MENSAJES_RESPUESTAMap());
            modelBuilder.Configurations.Add(new AC_MENSAJES_SMSMap());
        }
    }
}

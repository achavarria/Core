using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Seguridad
{
    public partial class SC_Menu : EntidadBase
    {
        public int IdMenu { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int Nivel { get; set; }
        public int Orden { get; set; }
        public Nullable<int> IdMenuPadre { get; set; }
        public int IdSistema { get; set; }
        public Nullable<int> IdAccion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdRestringeEmpresa { get; set; }
        public Nullable<int> IdCompania { get; set; }
    }
}

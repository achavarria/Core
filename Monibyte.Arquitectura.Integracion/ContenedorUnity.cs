﻿namespace Monibyte.Arquitectura.Integracion
{
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.InterceptionExtension;
    using Monibyte.Arquitectura.Adaptador.Nucleo.Fabrica;
    using Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte;
    using Monibyte.Arquitectura.Datos.Databases.Monibyte;
    using Monibyte.Arquitectura.Dominio;
    using Monibyte.Arquitectura.Integracion.Interceptor;
    using Monibyte.Arquitectura.Integracion.Unidades;
    using Monibyte.Arquitectura.Servicios.Nucleo;
    using System.ServiceModel.Dispatcher;

    public class ContenedorUnity
    {
        private static readonly IUnityContainer _unityContainer;
        static ContenedorUnity()
        {
            _unityContainer = new UnityContainer();
            //Interceptor para auditar las tablas
            _unityContainer.AddNewExtension<Interception>();
            _unityContainer.Configure<Interception>().AddPolicy("AuditPolicy")
              .AddMatchingRule<MemberNameMatchingRule>(new InjectionConstructor(
                  new[] { "Insert*", "Update*" }, true))
              .AddCallHandler<AuditInterceptor>(new ContainerControlledLifetimeManager(),
                  new InjectionProperty("Order", 1));
            //Controlador de errores del sistema
            _unityContainer.RegisterType<IErrorHandler, ManejadorError>();
            //Adaptador de tipos
            _unityContainer.RegisterType<IMapperAdapterFactory, MapperAdapterFactory>
                (new ContainerControlledLifetimeManager());
            var adapterFactory = _unityContainer.Resolve<IMapperAdapterFactory>();
            MapperFactory.SetFactory(adapterFactory);
            //Base de datos monibyte
            _unityContainer.RegisterType<IUnidadTbjoMonibyte, UnidadTbjoMonibyte>
                (new PerResolveLifetimeManager()).Configure<Interception>()
                .SetInterceptorFor(typeof(IUnidadTbjoMonibyte),
                new TransparentProxyInterceptor());
            _unityContainer.RegisterType(typeof(IRepositorioMnb<>), typeof(RepositorioMnb<>),
                new InjectionConstructor(typeof(IUnidadTbjoMonibyte)));
            //Base de datos ctrlmonibyte
            _unityContainer.RegisterType<IUnidadTbjoCtrlMonibyte, UnidadTbjoCtrlMonibyte>
                (new PerResolveLifetimeManager()).Configure<Interception>()
                .SetInterceptorFor(typeof(IUnidadTbjoCtrlMonibyte),
                new TransparentProxyInterceptor());
            _unityContainer.RegisterType(typeof(IRepositorioCtrl<>), typeof(RepositorioCtrl<>),
                new InjectionConstructor(typeof(IUnidadTbjoCtrlMonibyte)));
            //Servicios y repositorios especializados
            ContenedorUnitySeguridad.Configurar(_unityContainer);
            ContenedorUnityLogs.Configurar(_unityContainer);
            ContenedorUnityGenerales.Configurar(_unityContainer);
            ContenedorUnityClientes.Configurar(_unityContainer);
            ContenedorUnityTarjetas.Configurar(_unityContainer);
            ContenedorUnityGestiones.Configurar(_unityContainer);
            ContenedorUnityTesoreria.Configurar(_unityContainer);
            ContenedorUnityConfiguracion.Configurar(_unityContainer);
            ContenedorUnityAutorizador.Configurar(_unityContainer);
            ContenedorUnityQuickpass.Configurar(_unityContainer);
            ContenedorUnityContabilidad.Configurar(_unityContainer);
            ContenedorUnityCargos.Configurar(_unityContainer);
            ContenedorUnityEfectivo.Configurar(_unityContainer);
            ContenedorUnityProcesador.Configurar(_unityContainer);
            ContenedorUnityNotificacion.Configurar(_unityContainer);
            ContenedorUnityDocumentacion.Configurar(_unityContainer);
            ContenedorUnityTEF.Configurar(_unityContainer);
            ContenedorUnitySms.Configurar(_unityContainer);
        }

        public static IUnityContainer Contenedor
        {
            get { return _unityContainer; }
        }
    }
}
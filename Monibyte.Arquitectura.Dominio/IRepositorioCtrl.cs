﻿using Monibyte.Arquitectura.Dominio.Nucleo;

namespace Monibyte.Arquitectura.Dominio
{
    public interface IRepositorioCtrl<TEntity> : IRepositorio<TEntity> 
        where TEntity : EntidadBaseCtrl
    {
        //new IQueryable<TEntity> Table { get; }
    }
}

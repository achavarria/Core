﻿using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.Gestiones
{
    public class PocDetalleGestion
    {
        public int IdGestion { get; set; }
        public int IdPersona { get; set; }
        public int IdEmpresa { get; set; }
        public string NombrePersona { get; set; }
        public string Empresa { get; set; }
        public DateTime FecInclusion { get; set; }
        public TimeSpan HoraInclusion { get; set; }
        public string Detalle { get; set; }
        public int IdEstado { get; set; }
        public string DescripcionEstado { get; set; }
        public string DescripcionGestion { get; set; }
        public string ValorParametro { get; set; }
        public DateTime FecCierre { get; set; }
        public int IdUsuarioCierra { get; set; }
        public string NumProducto { get; set; }
        public int IdTipoGestion { get; set; }
        public string DescripcionEstadoProducto { get; set; }
        public string TipoProducto { get; set; }
        public int IdTipoProducto { get; set; }
        public int IdContexto { get; set; }
        public int? IdProducto { get; set; }
        public int IdUsuarioIncluye { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public int? IdSucursal { get; set; }
        public int? IdEjecutivo { get; set; }
        public string Sucursal { get; set; }
        public string Ejecutivo { get; set; }
        public int? IdExportado { get; set; }
        public bool Autoriza { get; set; }
        public bool SoloConsulta { get; set; }
        public int? IdContextoMovimientos { get; set; }
        public int? IdRolAsociado { get; set; }
    }

    public class PocParametrosGestion
    {
        public int? IdGestion { get; set; }
        public int? IdEmpresa { get; set; }
        public int? IdPersona { get; set; }
        public int? IdEstado { get; set; }
        public int? IdTipoGestion { get; set; }
        public int? IdContexto { get; set; }
        public int? IdProducto { get; set; }
        public string Contexto { get; set; }
        public int? IdExportado { get; set; }
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
        public DateTime? FecIncluyeDesde { get; set; }
        public DateTime? FecIncluyeHasta { get; set; }
        public string NombreEmpresa { get; set; }
        public string NombreCompleto { get; set; }
        public string NumIdentificacion { get; set; }
    }

    public class PocDetalleHistoriaGestion
    {
        public int Id { get; set; }
        public int IdGestion { get; set; }
        public DateTime FecEstado { get; set; }
        public TimeSpan HoraEstado { get; set; }
        public string Detalle { get; set; }
        public int IdEstado { get; set; }
        public string DescripcionEstado { get; set; }
    }

    public class PocProductoGestion
    {
        public string NumProducto { get; set; }
        public string DescripcionEstado { get; set; }
        public int? IdPersona { get; set; }
        public int IdTipoProducto { get; set; }
    }

    public class PocTipoGestion
    {
        public int IdTipoGestion { get; set; }
        public int IdEstado { get; set; }
        public string Descripcion { get; set; }
    }

    public class PocCambioEstadoGestion
    {
        public int IdGestion { get; set; }
        public int IdEstado { get; set; }
        public int IdTipoGestion { get; set; }
        public string Justificacion { get; set; }
        public int IdUsuarioIncluye { get; set; }
    }

    public class PocModificaGestion
    {
        public int IdGestion { get; set; }
        public int IdEstado { get; set; }
        public int IdTipoGestion { get; set; }
        public string Justificacion { get; set; }
        public decimal LimiteCuenta { get; set; }
        public int IdUsuarioIncluye { get; set; }
    }
    public class PocMovimientosLiquidacion
    {
        public int IdMovimiento { get; set; }
        public DateTime HoraTransaccion { get; set; }
        public DateTime FecTransaccion { get; set; }
        public DateTime FecMovimiento { get; set; }
        public string NumFactura { get; set; }
        public string DetalleMovimiento { get; set; }
        public int IdMoneda { get; set; }
        public int IdContexto { get; set; }
        public string DescripcionMoneda { get; set; }
        public string SimboloMonedaLocal { get; set; }
        public string SimboloMonedaInter { get; set; }
        public decimal MonLocal { get; set; }
        public decimal MonInter { get; set; }
        public int IdDebCredito { get; set; }
        public int IdTipoMovimiento { get; set; }
        public string Establecimiento { get; set; }
        public string Categoria { get; set; }
        public string NombreCompleto { get; set; }
        public string TipoMovimiento { get; set; }
        public int IdEditado { get; set; }
        public string CodMoneda { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public bool ArchivoAdjunto { get; set; }
        public int? NumKmSalida { get; set; }
        public int? NumKmLlegada { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdCuenta { get; set; }
        public int IdPersona { get; set; }
        public int IdEmpresa { get; set; }
        public int? IdEstado { get; set; }
        public bool SoloConsulta { get; set; }
        public int? IdTipoVehiculo { get; set; }
        public int? Modelo { get; set; }
        public string Placa { get; set; }
        public int IdLiquidado { get; set; }
        public bool EsHistorico { get; set; }
        public int? IdGestion { get; set; }
        /****************CAMPOS REQUERIDOS SAP MOV DE TARJETA*****************/
        public string NumReferencia { get; set; }
        public string NumTarjeta { get; set; }
        public string Descripcion { get; set; }
        public string DynamicJson { get; set; }
        public string DynamicJsonTarjeta { get; set; }
        public DateTime FecCorte { get; set; }
        public string CodMonedaLocal { get; set; }
        public string CodMonedaInter { get; set; }
        public string CodMonedaNormativa { get; set; }
        public decimal MonCredLocal { get; set; }
        public decimal MonCredInter { get; set; }
        public decimal MonDebLocal { get; set; }
        public decimal MonDebInter { get; set; }
        public decimal? MonMovimiento { get; set; }
        public decimal SignoMonMovimiento { get; set; }
        public decimal? SignoMonMovimientoLocal { get; set; }
        public decimal? SignoMonMovimientoInter { get; set; }
        public decimal? MonMovimientoLocal { get; set; }
        public decimal? MonMovimientoInter { get; set; }
        public int? CodCategoriaMcc { get; set; }
        public string CategoriaMcc { get; set; }
        public string NombreImpreso { get; set; }
        public string DebitoCredito { get; set; }
        public string CodEditado { get; set; }
        public string MesMovimiento { get; set; }
        public string AnoMovimiento { get; set; }
        /****************CAMPOS REQUERIDOS SAP MOV DE TARJETA*****************/
    }

    public class PocLiquidacionGastos
    {
        public DateTime? FecSalida { get; set; }
        public DateTime? FecRegreso { get; set; }
        public int IdPersona { get; set; }
        public int? IdGestion { get; set; }
        public int? IdEstado { get; set; }
        public string NombreCompleto { get; set; }
        public string NumIdentificacion { get; set; }
        public string CentroCosto { get; set; }
        public string IdParametroEmpresa { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdTipoFiltro { get; set; }
        public string Observaciones { get; set; }
        public string NumTarjeta { get; set; }
        public List<PocMovimientosDeLiquidacion> IdsMovs { get; set; }
        public string DynamicJson { get; set; }
        public int IdUsrIncluye { get; set; }
        public int? IdContextoMovimientos { get; set; }
    }

    public class PocLiquidacionEncabezado
    {
        public DateTime? FecSalida { get; set; }
        public DateTime? FecRegreso { get; set; }
        public int IdPersona { get; set; }
        public string NombreCompleto { get; set; }
        public string NumIdentificacion { get; set; }
        public string CentroCosto { get; set; }
        public string IdParametroEmpresa { get; set; }
        public string Observaciones { get; set; }
        public string NumTarjeta { get; set; }
    }

    public class PocMovimientosDeLiquidacion
    {
        public int IdMovimiento { get; set; }
        public int IdContexto { get; set; }
    }

    public class PocParametrosLiquidacion
    {
        public int? IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdContexto { get; set; }
        public int? IdPersonaLiq { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public int[] IdGestiones { get; set; }
        public bool? BuscarMovimientos { get; set; }
        public bool? MarcarExportados { get; set; }
        public PocTarjetaCuentaUsuario[] Personas { get; set; }
        public List<PocMovimientosDeLiquidacion> IdsMovs { get; set; }
    }

    public class PocParametrosGestionEmpresa
    {
        public int IdTipoGestion { get; set; }
        public int IdParametro { get; set; }
        public int IdParametroGestion { get; set; }
        public int IdParametroEmpresa { get; set; }
        public string DescripcionGestion { get; set; }
        public string DescripcionEmpresa { get; set; }
        public string DescripcionTipoGestion { get; set; }
        public string ValorDefecto { get; set; }
        public bool Equal(PocParametrosGestionEmpresa o)
        {
            var ue = o as PocParametrosGestionEmpresa;
            if (ue == null)
            {
                return false;
            }
            return this.IdParametro == ue.IdParametro &&
                this.IdParametroEmpresa == ue.IdParametroEmpresa;
        }
    }

    public class PocRepuestaLiquidacion
    {
        public int IdGestion { get; set; }
        public byte[] reporte { get; set; }
    }

    public class PocNotificacionSalida
    {
        public string NombreCompleto { get; set; }
        public string NumTarjeta { get; set; }
        public string Detalle { get; set; }
        public int IdTipoTarjeta { get; set; }
        public int IdTarjeta { get; set; }
        public DateTime FecSalida { get; set; }
        public DateTime FecRegreso { get; set; }
        public decimal ConsumoTotalEstimado { get; set; }
        public string ListaPaises { get; set; }
        public string NombrePaises { get; set; }
        public int IdEmpresa { get; set; }
    }
}

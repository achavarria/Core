﻿using System.Linq;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;

namespace Monibyte.Arquitectura.Datos.Generales.Repositorios
{
    public class RepositorioPerfilUsuario : RepositorioMnb<GL_Usuario>, IRepositorioPerfilUsuario
    {
        public RepositorioPerfilUsuario(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public GL_Usuario ObtenerInfoUsuario(int? IdUsuario)
        {
            var criteria = new Criteria<GL_Usuario>();
            criteria.And(usuario => usuario.IdUsuario == IdUsuario);
            return this.SelectBy(criteria).First();
        }

    }
}



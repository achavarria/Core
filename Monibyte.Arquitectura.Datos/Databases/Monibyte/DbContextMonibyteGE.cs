﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Gestiones;
using Monibyte.Arquitectura.Dominio.Gestiones;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<GE_Gestion> GE_Gestion { get; set; }
        public DbSet<GE_HistorialGestion> GE_HistorialGestion { get; set; }
        public DbSet<GE_MovimientoGestion> GE_MovimientoGestion { get; set; }
        public DbSet<GE_ParametroGestionEmpresa> GE_ParametroGestionEmpresa { get; set; }
        public DbSet<GE_ParametrosGestion> GE_ParametrosGestion { get; set; }
        public DbSet<GE_ParametrosValor> GE_ParametrosValor { get; set; }
        public DbSet<GE_TipoAccion> GE_TipoAccion { get; set; }
        public DbSet<GE_TipoGestion> GE_TipoGestion { get; set; }
        public DbSet<GE_TipoRespuesta> GE_TipoRespuesta { get; set; }
        public DbSet<GE_TipoGestionRol> GE_TipoGestionRol { get; set; }

        void CreateContextModelGE(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new GE_AccionMap());
            modelBuilder.Configurations.Add(new GE_GestionMap());
            modelBuilder.Configurations.Add(new GE_HistorialGestionMap());
            modelBuilder.Configurations.Add(new GE_MovimientoGestionMap());
            modelBuilder.Configurations.Add(new GE_ParametroGestionEmpresaMap());
            modelBuilder.Configurations.Add(new GE_ParametrosGestionMap());
            modelBuilder.Configurations.Add(new GE_ParametrosValorMap());
            modelBuilder.Configurations.Add(new GE_TipoAccionMap());
            modelBuilder.Configurations.Add(new GE_TipoGestionMap());
            modelBuilder.Configurations.Add(new GE_TipoRespuestaMap());
            modelBuilder.Configurations.Add(new GE_TipoGestionRolMap());

        }
    }
}

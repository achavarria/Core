﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Logs;
using Monibyte.Arquitectura.Poco.Logs;
using System;

namespace Monibyte.Arquitectura.Aplicacion.Logs
{
    public class AppLogs : AplicacionBase, IAppLogs
    {
        private IRepositorioMnb<LG_LogEvento> _repEventLogs;

        public AppLogs(IRepositorioMnb<LG_LogEvento> repEventLogs)
        {
            _repEventLogs = repEventLogs;
        }

        public void GuardarEventoAsync(PocLogs poco)
        {
            LG_LogEvento logs = new LG_LogEvento();
            logs = poco.ProyectarComo<LG_LogEvento>();
            logs.IdEvento = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdEvento");
            logs.FecEvento = DateTime.Now;
            logs.FecInicio = DateTime.Now;
            logs.HoraEvento = DateTime.Now;
            _repEventLogs.Insert(logs);
            _repEventLogs.UnidadTbjo.Save();
        }

        public void ModificarEventoAsync(string sessionCode, string codUsuario, int tipoEvento)
        {
            _repEventLogs.UpdateOn(x => x.CodigoSesion == sessionCode && x.CodUsuario == codUsuario
                && x.IdTipoEvento == tipoEvento, r => new LG_LogEvento
                {
                    FecFinal = DateTime.Now
                });
        }
    }
}

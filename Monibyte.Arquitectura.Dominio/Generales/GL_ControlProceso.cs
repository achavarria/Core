using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_ControlProceso : EntidadBase
    {
        public int IdEjecucion { get; set; }
        public int IdProceso { get; set; }
        public string Referencia { get; set; }
        public int IdEstado { get; set; }
        public string DetEjecucion { get; set; }
        public string DetError { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Contabilidad;

namespace Monibyte.Arquitectura.Aplicacion.Contabilidad
{
    public interface IAppConfAsientoContabilidad : IAplicacionBase
    {
        void Insertar(PocConfiguracionAsiento registro);
        void Eliminar(PocConfiguracionAsiento registro);
        void Modificar(PocConfiguracionAsiento registro);
        DataResult<PocConfiguracionAsiento> Listar(PocConfiguracionAsiento filtro, DataRequest request);
    }
}

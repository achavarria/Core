﻿using System.Collections.Generic;
using Monibyte.Arquitectura.Poco.Configuracion;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public interface IAppConfiguracionApl : IAdmCondicion
    {
        void EliminarPlantilla(int? idPlantilla);
        int? DuplicarPlantilla(PocCondicionDuplica poco, int idGrupo);
        void CambioEstadoPlantilla(int idPlantilla, int idEstado);
        List<PocCondicion> ObtenerPlantillasEmpresa(int idEmpresa);
        List<PocCondicion> ObtenerPlantillasDisponibles(int idEmpresa);
    }
}

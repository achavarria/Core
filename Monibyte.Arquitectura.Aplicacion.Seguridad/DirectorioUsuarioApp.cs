﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;


namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public class DirectorioUsuarioApp : AplicacionBase, IDirectorioUsuarioApp
    {
        #region CONSTRUCTOR UNITY
        private IRepositorioUsuario _repUsuario;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<CL_PerfilEmpresa> _repPerfilEmpresa;
        private IRepositorioMnb<CL_EmpresasUsuario> _repEmpresasUsuario;
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IRepositorioMnb<GL_Pais> _repPais;
        private IRepositorioMnb<GL_UnidadNegocio> _repUnidadNegocio;
        private IRepositorioMnb<CL_Comunicacion> _repComunicacion;
        private IRepositorioMnb<GL_ParametroCompania> _repParamCompania;

        public DirectorioUsuarioApp(IRepositorioUsuario repUsuario,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<CL_PerfilEmpresa> repPerfilEmpresa,
            IRepositorioMnb<CL_EmpresasUsuario> repEmpresasUsuario,
            IRepositorioMnb<GL_Entidad> repEntidad,
            IRepositorioMnb<GL_Pais> repPais,
            IRepositorioMnb<GL_UnidadNegocio> repUnidadNegocio,
            IRepositorioMnb<CL_Comunicacion> repComunicacion,
            IRepositorioMnb<GL_ParametroCompania> repParamCompania)
        {
            _repUsuario = repUsuario;
            _repPersona = repPersona;
            _repPerfilEmpresa = repPerfilEmpresa;
            _repEmpresasUsuario = repEmpresasUsuario;
            _repEntidad = repEntidad;
            _repPais = repPais;
            _repUnidadNegocio = repUnidadNegocio;
            _repComunicacion = repComunicacion;
            _repParamCompania = repParamCompania;
        }
        #endregion

        public PocInfoSesion ObtenerCuentaPorTicket(string ticket)
        {
            string url = RestConfig.Get("ApiCore", "Auth", "ObtenerUsuarioTicket");
            var info = RestClient.CoreRequest<PocInfoSesion>(url, ticket);
            if (info == null)
            {
                throw new CoreException("Usuario incorrecto", "Err_0006");
            }
            using (var ts = new TransactionScope())
            {
                _repUsuario.UpdateOn(x => x.CodUsuario == info.CodUsuario,
                    z => new GL_Usuario
                    {
                        Correo = info.Correo,
                        IdIdiomaPreferencia = info.IdIdioma,
                        IdEstado = info.IdEstado,
                        AliasUsuario = info.AliasUsuario
                    });
                _repUsuario.UnidadTbjo.Save();
                ts.Complete();
            }
            return ObtenerUsuario(info);
        }

        public PocInfoSesion ObtenerCuenta(string codUsuario)
        {
            string url = RestConfig.Get("ApiCore", "Membresia", "ObtenerCuenta");
            var info = RestClient.CoreRequest<PocInfoSesion>(url, codUsuario);
            if (info == null)
            {
                throw new CoreException("Usuario incorrecto", "Err_0006");
            }
            return ObtenerUsuario(info);
        }

        private PocInfoSesion ObtenerUsuario(PocInfoSesion info)
        {
            var criteria = new Criteria<GL_Usuario>();
            criteria.And(usuario => usuario.CodUsuario == info.CodUsuario);
            criteria.Or(usuario => usuario.AliasUsuario == info.CodUsuario);
            var res = _repUsuario.SelectBy(criteria)
            .Join(_repPersona.Table,
                usr => usr.IdPersona, per => per.IdPersona,
                (usr, per) => new { usr, per })
            .Join(_repEmpresasUsuario.Table, item => item.usr.IdUsuario, eu => eu.IdUsuario,
                (item, eu) => new { item.per, item.usr, eu })
            .GroupJoin(_repPerfilEmpresa.Table,
                tmp => tmp.eu.IdEmpresa,
                pf => pf.IdEmpresa,
                (tmp, pf) => new { tmp.usr, tmp.per, tmp.eu, pf = pf.FirstOrDefault() })
            .Where(temp => !string.IsNullOrEmpty(info.CodUsuario) &&
                temp.eu.IdDefault == (int)EnumIdSiNo.Si)
            .First();
            info.IdRole = res.eu.IdRole;
            info.IdUsuario = res.usr.IdUsuario;
            info.IdPersona = res.per.IdPersona;
            info.IdEmpresa = res.eu.IdEmpresa;
            info.IdTipoUsuario = res.eu.IdTipoUsuario;
            info.NombreCorto = string.Format("{0} {1}",
                res.per.PrimerNombre, res.per.PrimerApellido);
            info.NombreCompleto = res.per.NombreCompleto;
            info.NumIdentificacion = res.per.NumIdentificacion;
            info.IdRoleOperativo = res.eu.IdRoleOperativo;
            info.RequiereAutorizacion = res.pf != null ?
                res.pf.IdRequiereAutorizacion == (int)EnumIdSiNo.Si :
                res.per.IdTipoPersona == (int)EnumTipoPersona.Juridica ? true : false;
            return info;
        }

        public string IncluirCuenta(PocUsuario cuenta)
        {
            try
            {
                var url = RestConfig.Get("ApiCore", "Membresia", "IncluirCuenta");
                cuenta.CodUsuario = RestClient.CoreRequest<string>(url, new
                {
                    Correo = cuenta.Correo,
                    IdCompania = cuenta.IdCompania,
                    IdUnidad = cuenta.IdUnidad,
                    IdPaisPreferencia = cuenta.IdPaisPreferencia,
                    IdIdiomaPreferencia = cuenta.IdIdiomaPreferencia,
                    IdRequiereOTP = cuenta.IdRequiereOTP,
                    IdEsInterno = cuenta.EsInterno ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No,
                    IdTipoUsuario = cuenta.IdTipoUsuario
                });
                var validacion = ValidaDatosBasicos(cuenta.CodUsuario, cuenta.Correo);
                if (!validacion.IsValid)
                {
                    var dir = RestConfig.Get("ApiCore", "Membresia", "EliminarCuenta");
                    RestClient.CoreRequest(dir, cuenta.CodUsuario);
                    string detalle = string.Format("Errores al incluir usuario {0}", cuenta.CodUsuario);
                    throw new CoreException(detalle, validation: validacion);
                }
                return cuenta.CodUsuario;
            }
            catch
            {
                if (!string.IsNullOrEmpty(cuenta.CodUsuario))
                {
                    var url = RestConfig.Get("ApiCore", "Membresia", "EliminarCuenta");
                    RestClient.CoreRequest(url, cuenta.CodUsuario);
                }
                throw new CoreException("Error en la Inclusión de un Usuario", "Err_0101");
            }
        }

        public void ModificarCuenta(PocUsuario cuenta)
        {
            var validacion = ValidaDatosBasicos(cuenta.CodUsuario, cuenta.Correo);
            if (!validacion.IsValid)
            {
                string detalle = string.Format("Errores al modificar el usuario 0}", cuenta.CodUsuario);
                throw new CoreException(detalle, validation: validacion);
            }
            var url = RestConfig.Get("ApiCore", "Membresia", "ModificarCuenta");

            var jsonData = new
            {
                CodUsuario = cuenta.CodUsuario,
                Correo = cuenta.Correo,
                IdCompania = cuenta.IdCompania,
                IdUnidad = cuenta.IdUnidad,
                IdIdiomaPreferencia = cuenta.IdIdiomaPreferencia,
                IdPaisPreferencia = cuenta.IdPaisPreferencia,
                IdTipoUsuario = cuenta.IdTipoUsuario
            };

            RestClient.CoreRequest(url, jsonData);
        }

        public void EliminarCuenta(string codUsuario)
        {
            var url = RestConfig.Get("ApiCore", "Membresia", "EliminarCuenta");
            RestClient.CoreRequest(url, codUsuario);
        }

        public PocCredencialesDirectorio ReestablecerContrasena(string codUsuario)
        {
            var url = RestConfig.Get("ApiCore", "Membresia", "ReestablecerContrasena");
            return RestClient.CoreRequest<PocCredencialesDirectorio>(url, codUsuario);
        }

        public void ActualizarContrasena(PocCambioContrasena datos)
        {
            var url = RestConfig.Get("ApiCore", "Membresia", "ActualizarContrasena");
            RestClient.CoreRequest(url, datos);
        }

        public void RenovarContrasena(string codUsuario)
        {
            var url = RestConfig.Get("ApiCore", "Membresia", "RenovarContrasena");
            RestClient.CoreRequest(url, codUsuario);
        }

        public void ReiniciarIntentos(string codUsuario)
        {
            var url = RestConfig.Get("ApiCore", "Membresia", "ReiniciarIntentos");
            RestClient.CoreRequest(url, codUsuario);
        }

        public void CambiarEstado(string codUsuario, int nuevoEstado)
        {
            using (var ts = new TransactionScope())
            {
                if (nuevoEstado == (int)EnumEstadoUsuario.Activo)
                {
                    var tipoUsuario = _repUsuario.Table
                    .Join(_repEmpresasUsuario.Table,
                        usr => usr.IdUsuario,
                        emp => emp.IdUsuario,
                        (usr, emp) => new { usr, emp })
                    .FirstOrDefault(x => x.emp.IdDefault == (int)EnumIdSiNo.Si &&
                            x.usr.CodUsuario == codUsuario).emp.IdTipoUsuario;

                    string url = RestConfig.Get("ApiCore", "Membresia", "ObtenerCuenta");
                    var info = RestClient.CoreRequest<PocInfoSesion>(url, codUsuario);
                    var url0 = RestConfig.Get("ApiCore",
                        "Membresia", "ObtenerPreguntasUsuario");
                    var preguntas = RestClient.CoreRequest<List
                        <PocPreguntaUsuario>>(url0, codUsuario);
                    var registro = preguntas != null && preguntas.Any();
                    nuevoEstado = (!registro && !info.EsInterno) ?
                        (int)EnumEstadoUsuario.Registrado : nuevoEstado;
                }
                var url1 = RestConfig.Get("ApiCore", "Membresia", "CambiarEstado");
                RestClient.CoreRequest(url1, new { CodUsuario = codUsuario, IdEstado = nuevoEstado });
                _repUsuario.UpdateOn(u => u.CodUsuario == codUsuario,
                    up => new GL_Usuario { IdEstado = nuevoEstado });
                _repUsuario.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void RegistrarPerfil(PocRegistroUsuario infoUsuario)
        {
            using (var ts = new TransactionScope())
            {
                _repUsuario.UpdateOn(x => x.CodUsuario == infoUsuario.CodUsuario,
                    z => new GL_Usuario
                    {
                        IdEstado = (int)EnumEstadoUsuario.Activo
                    });
                _repUsuario.UnidadTbjo.Save();
                var url = RestConfig.Get("ApiCore", "Membresia", "RegistrarPerfil");
                var usuario = RestClient.CoreRequest<PocInfoUsuario>(url, infoUsuario);
                ts.Complete();
            }
        }

        public PocInfoUsuario ObtenerInfoPerfil(string codUsuario)
        {
            codUsuario = codUsuario ?? InfoSesion.Info.CodUsuario;
            var url = RestConfig.Get("ApiCore", "Membresia", "ObtenerInfoPerfil");
            var usuario = RestClient.CoreRequest<PocInfoUsuario>(url, codUsuario);
            usuario.IdUsuario = _repUsuario.Table.FirstOrDefault(x => x.CodUsuario ==
                    usuario.CodUsuario).IdUsuario;
            return usuario;
        }

        public void ActualizarPerfil(PocInfoUsuario infoUsuario)
        {
            using (var ts = new TransactionScope())
            {
                var url = RestConfig.Get("ApiCore", "Membresia", "ActualizarPerfil");
                RestClient.CoreRequest(url, infoUsuario);
                _repUsuario.UpdateOn(x => x.CodUsuario == infoUsuario.CodUsuario,
                    z => new GL_Usuario
                    {
                        AliasUsuario = infoUsuario.AliasUsuario
                    });
                _repUsuario.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public List<PocInfoSesion> ObtenerUsuarios(string alias)
        {
            var url = RestConfig.Get("ApiCore", "Membresia", "ListarUsuario");
            return RestClient.CoreRequest<List<PocInfoSesion>>(url, new
            {
                IdCompania = InfoSesion.Info.IdCompania,
                AliasUsuario = alias
            });
        }

        public List<PocSesionActiva> ObtenerSesionesActivas()
        {
            var url = RestConfig.Get("ApiCore", "Membresia", "UsuariosConectados");
            return RestClient.CoreRequest<List<PocSesionActiva>>
                (url, InfoSesion.Info.IdCompania);
        }

        private ValidationResponse ValidaDatosBasicos(string codUsuario, string email)
        {
            var validation = new ValidationResponse();
            if (string.IsNullOrEmpty(codUsuario))
            {
                validation.AddError("Err_0006");
            }
            if (string.IsNullOrEmpty(email))
            {
                validation.AddError("Err_0058");
            }
            return validation;
        }

        public void FinalizarSesion(string ticket)
        {
            var url = RestConfig.Get("ApiCore", "Auth", "FinalizarSesion");
            RestClient.CoreRequest(url, ticket);
        }

        public PocDobleFactorAut GenerarToken(string codUsuario)
        {
            var url = RestConfig.Get("ApiCore", "Auth", "GenerarCodigo");
            return RestClient.CoreRequest<PocDobleFactorAut>(url, codUsuario);
        }

        public PocInfoSesion ObtenerUsuarioApi(int clientId)
        {

            var user = (from en in _repEntidad.Table
                        join us in _repUsuario.Table
                            on en.IdPersona equals us.IdPersona
                        where en.IdEntidad == clientId
                        select us).FirstOrDefault();
            var _info = ObtenerCuenta(user.CodUsuario);
            if (string.IsNullOrEmpty(_info.Correo))
            {
                var comunica = _repComunicacion.Table
                    .Where(x => x.IdPersona == _info.IdPersona &&
                        x.IdTipo == (int)EnumTipoComunicacion.Correo)
                    .FirstOrDefault();
                //Si no existe correo de la persona dueña de la tarjeta, envía el correo a servicio al cliente
                if (comunica == null)
                {
                    var comunica1 = _repParamCompania.Table
                    .Where(w => w.IdParametro == "REMITENTE_CORREO_GESTION" &&
                        w.IdCompania == _info.IdCompania).FirstOrDefault();
                    _info.Correo = comunica1.ValParametro;
                }
                else
                {
                    _info.Correo = comunica.ValComunicacion;
                }
            }
            return _info;
        }       

       
    }
}
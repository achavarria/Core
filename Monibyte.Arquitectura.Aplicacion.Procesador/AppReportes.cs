﻿using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using Monibyte.Arquitectura.Poco.Procesador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;

namespace Monibyte.Arquitectura.Aplicacion.Procesador
{
    public class AppReportes : AplicacionBase, IAppReportes
    {
        private IRepositorioEmail _repEmail;
        private IRepositorioReportServer _repReportServer;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;
        private IRepositorioMnb<GL_ParametroCompania> _repParamCompania;
        private IRepositorioPlantillaNotificacion _repPlantillaNot;

        public AppReportes(
            IRepositorioEmail repEmail,
            IRepositorioReportServer repReportServer,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema,
            IRepositorioMnb<GL_ParametroCompania> repParamCompania,
            IRepositorioPlantillaNotificacion repPlantillaNot)
        {
            _repEmail = repEmail;
            _repReportServer = repReportServer;
            _repParamSistema = repParamSistema;
            _repPlantillaNot = repPlantillaNot;
            _repParamCompania = repParamCompania;
        }

        public byte[] ObtieneReporteDenegacion(string numTarjeta, string numCuenta, string usuario,
            DateTime FecDesde, DateTime FecHasta, int? idEmpresa)
        {
            Reporte resultado;
            var parameters = new List<ReportParam>();

            if (!string.IsNullOrEmpty(numTarjeta))
            {
                parameters.Add(new ReportParam { Name = "NumTarjeta", Value = numTarjeta.ToString() });
            }
            if (!string.IsNullOrEmpty(numCuenta))
            {
                parameters.Add(new ReportParam { Name = "Cuenta", Value = numCuenta.ToString() });
            }
            if (!string.IsNullOrEmpty(usuario))
            {
                parameters.Add(new ReportParam { Name = "Usuario", Value = usuario.ToString() });
            }
            parameters.Add(new ReportParam { Name = "FecDesde", Value = FecDesde.ToString("MM-dd-yyyy") });
            parameters.Add(new ReportParam { Name = "FecHasta", Value = FecHasta.ToString("MM-dd-yyyy") });
            if (idEmpresa.HasValue)
            {
                parameters.Add(new ReportParam { Name = "IdEmpresa", Value = idEmpresa.ToString() });
            }

            resultado = _repReportServer.ExportPdf("/Reports/Notificaciones/r-Denegaciones", parameters.ToArray());
            return resultado.Output;
        }

        public void EnviarReporteEmpresa(List<PocDenegacionesDiarias> datosEntrada)
        {
            var plantilla = _repPlantillaNot.ObtenerPlantillaNot((int)EnumContexto.NOT_DENEGACIONES, 0);
            foreach (var item in datosEntrada)
            {
                var FecDesde = DateTime.Today.AddDays(-1);
                var FecHasta = DateTime.Today;
                var emailExterno = new EmailConfig(plantilla.Asunto,
                   string.Format(plantilla.Cuerpo, FecDesde.ToShortDateString(), FecHasta.ToShortDateString()));
                var reporte = ObtieneReporteDenegacion(string.Empty, string.Empty, string.Empty, FecDesde, FecHasta, item.IdEmpresa);

                emailExterno.AddAttachment(reporte, plantilla.NomAdjunto, MediaTypeNames.Application.Pdf);
                emailExterno.AddTo(item.Correo);

                var remitente = _repParamCompania.Table.FirstOrDefault(
                    x => x.IdParametro == "REMITENTE_CORREO_GESTION" &&
                        x.IdCompania == item.IdCompania);
                SenderConfig sender = null;
                if (remitente != null)
                {
                    sender = new SenderConfig(remitente.ValParametro);
                }
                _repEmail.EnviarEmail(sender, emailExterno);
            }
        }
    }
}

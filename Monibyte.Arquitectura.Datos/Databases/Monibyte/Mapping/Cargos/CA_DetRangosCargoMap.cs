using Monibyte.Arquitectura.Dominio.Cargos;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Cargos
{
    public class CA_DetRangosCargoMap : EntityTypeConfiguration<CA_DetRangosCargo>
    {
        public CA_DetRangosCargoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdTablaRangos, t.RangoMin });

            // Properties
            this.Property(t => t.IdTablaRangos)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RangoMin)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CA_DetRangosCargo", "Cargos");
            this.Property(t => t.IdTablaRangos).HasColumnName("IdTablaRangos");
            this.Property(t => t.RangoMin).HasColumnName("RangoMin");
            this.Property(t => t.RangoMax).HasColumnName("RangoMax");
            this.Property(t => t.MonCargo).HasColumnName("MonCargo");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");

        }
    }
}

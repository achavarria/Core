﻿using System;
namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocEntradaCargoAutomaticoTarjeta
    {
        public string NumTarjeta { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string CodComercio { get; set; }
        public string CodSucursal { get; set; }
        public string NumDocumento { get; set; }
        public string NombreDuenoCargo { get; set; }
    }

    public class PocSalidaCargoAutomaticoTarjeta
    {
        public string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string CodComercio { get; set; }
        public string CodSucursal { get; set; }
        public string NumDocumento { get; set; }
        public string NombreDuenoCargo { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }

}

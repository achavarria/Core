using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Logs
{
    public partial class LG_Bitacora : EntidadBase
    {
        public int IdBitacora { get; set; }
        public string BaseDatos { get; set; }
        public string Esquema { get; set; }
        public string Tabla { get; set; }
        public string Accion { get; set; }
        public string DetBitacora { get; set; }
        public System.DateTime FecBitacora { get; set; }
        public string UsuarioBD { get; set; }
        public string UsuarioApl { get; set; }
        public string UsuarioSO { get; set; }
        public string Maquina { get; set; }
        public string DireccionIP { get; set; }
    }
}

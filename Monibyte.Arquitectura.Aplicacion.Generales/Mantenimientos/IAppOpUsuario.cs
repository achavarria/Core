﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Clientes;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Generales.Mantenimientos
{
    public interface IAppOpUsuario : IAplicacionBase
    {
        void Insertar(PocUsuario registro);
        void Eliminar(PocUsuario registro);
        void Modificar(PocUsuario registro);
        DataResult<PocUsuario> Listar(PocUsuario filtro, DataRequest request);
        //Mantenimientos Usuario
        IEnumerable<PocUsuario> Listar(PocUsuario filtro);
        void ModificaCodUsuarioEmpresa(IEnumerable<PocUsuario> usuarios);
        void AsociarEmpresasUsuario(string codUsuario, List<PocEmpresasUsuario> modelo,
            List<PocEmpresasUsuario> modeloPrevio);
        List<PocEmpresasUsuario> ObtenerEmpresasAsociadas(string codUsuario);
        DataResult<PocUsuario> ListarUsuariosConectados(DataRequest request);
        void CambiosSeguridad(PocSeguridadUsuario condiciones);
        void CambiarRolesOperativos(List<PocUsuarioRoleOperativo> datosEntrada);
        void InsertaNotificaUsuario(PocUsuario registro);
    }
}

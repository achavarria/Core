﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Logs;
using Monibyte.Arquitectura.Dominio.Logs;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<LG_Bitacora> LG_Bitacora { get; set; }
        public DbSet<LG_BitacoraPanelControl> LG_BitacoraPanelControl { get; set; }
        public DbSet<LG_ColumnasAuditoria> LG_ColumnasAuditoria { get; set; }
        public DbSet<LG_LogError> LG_LogError { get; set; }
        public DbSet<LG_LogEvento> LG_LogEvento { get; set; }
        public DbSet<LG_TablasAuditoria> LG_TablasAuditoria { get; set; }
        public DbSet<LG_TipoError> LG_TipoError { get; set; }
        public DbSet<LG_TipoEvento> LG_TipoEvento { get; set; }
        public DbSet<LG_TriggersAuditoria> LG_TriggersAuditoria { get; set; }

        void CreateContextModelLG(DbModelBuilder modelBuilder) 
        {
            modelBuilder.Configurations.Add(new LG_BitacoraMap());
            modelBuilder.Configurations.Add(new LG_BitacoraPanelControlMap());
            modelBuilder.Configurations.Add(new LG_ColumnasAuditoriaMap());
            modelBuilder.Configurations.Add(new LG_LogErrorMap());
            modelBuilder.Configurations.Add(new LG_LogEventoMap());
            modelBuilder.Configurations.Add(new LG_TablasAuditoriaMap());
            modelBuilder.Configurations.Add(new LG_TipoErrorMap());
            modelBuilder.Configurations.Add(new LG_TipoEventoMap());
            modelBuilder.Configurations.Add(new LG_TriggersAuditoriaMap());
        }
    }
}

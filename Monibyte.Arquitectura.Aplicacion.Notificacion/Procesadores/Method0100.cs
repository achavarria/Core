﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Notificacion;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public partial class AppNotificacion
    {
        //Notificaciones provenientes del Autorizador
        public void Method0100(PocoQueue pocoQueue)
        {
            var dataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(pocoQueue.Data);
            if (pocoQueue.OriginId == (int)EnumIdContextoNotificacion.Denegacion)
            {
                var htmlstring = _appAutorizaciones.ObtieneDetalleDenegacion(dataDictionary["DenegationDetail"].ToString());
                htmlstring = string.Format("<root>{0}</root>", htmlstring).Replace("\\", "");
                var onDemandLink = XElement.Parse(htmlstring);
                var message = onDemandLink.Element("p").Value.ToLower();
                message = char.ToUpper(message[0]) + message.Substring(1);
                if (message[message.Length - 1] == '.')
                {
                    message = message.Remove(message.Length - 1);
                }
                dataDictionary["DenegationDetail"] = message;
            }
            var numero = dataDictionary["CardNumber"].ToString();
            var tarjeta = _repTarjeta.Table.FirstOrDefault(x => x.NumTarjeta == numero);
            dataDictionary.Add("CardholderName", tarjeta.NombreImpreso);
            pocoQueue.CompanyId = tarjeta.IdEmpresa.Value;
            pocoQueue.Data = dataDictionary.ToJson();
            ProcesarNotificaciones(pocoQueue, true, false, pocoQueue.CompanyId);
        }
    }
}
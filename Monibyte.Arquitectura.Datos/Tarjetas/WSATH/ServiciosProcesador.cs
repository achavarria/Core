﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios.WSATH;
using Monibyte.Arquitectura.Dominio.Tarjetas.WSATH;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Datos.Tarjetas.WSATH
{
    public class ServiciosProcesador : IServiciosProcesador
    {
        private const int EXITOSO = 0;
        private const int RESPUESTAVIPACTIVO = 7;
        private const int RESPUESTAVIPINACTIVO = 25;
        private const string ColaPorDefecto = "ATH-MONIBYTE";
        protected PropiedadesServiciosExt _propiedadesServ;

        public string ConvierteValorPropiedad(string valor, string propiedad, string seccion)
        {
            seccion = string.IsNullOrEmpty(seccion) ? ColaPorDefecto : seccion;
            _propiedadesServ = new PropiedadesServiciosExt(null, seccion);
            return (string.IsNullOrEmpty(valor) ? _propiedadesServ.ObtieneValPropiedad<string>(propiedad, seccion) : valor);
        }
        private static EntradaVip ValidaSalidaServicio(WsOperacionesVip.mensaje[] respServicio, EntradaVip paramsVip, List<int> respExitosa)
        {
            foreach (var mensaje in respServicio)
            {
                var codRespuesta = mensaje.codigo.ToString().PadLeft(3);
                if (!respExitosa.Contains(mensaje.codigo))
                {
                    var descripcionRespuesta =
                        CodRespuestaServATH.ResourceManager.GetString(string.Concat("VIP_", codRespuesta));

                    if (string.IsNullOrEmpty(descripcionRespuesta))
                    {
                        descripcionRespuesta = mensaje.descripcion;
                    }
                    paramsVip.CodigoRespuesta = codRespuesta;
                    paramsVip.DescripcionCodRespuesta = descripcionRespuesta;
                    throw new CoreException("Error en Monibyte.Arquitectura.Datos.Tarjetas.WSATH.ServicioProcesador", "", descripcionRespuesta, reasonCode: codRespuesta);
                }
                break;
            }
            return paramsVip;
        }

        public EntradaVip ActivarVip(EntradaVip paramsVip, string seccion = "")
        {
            try
            {
                seccion = string.Format("{0}-VIP", seccion);
                paramsVip.LocalidadTramite = ConvierteValorPropiedad(paramsVip.LocalidadTramite, "Localidad", seccion);

                var usuario = ConvierteValorPropiedad(null, "UsuarioVIP", seccion);
                if (usuario == "Usuariovip")
                {
                    return null;
                }
                var password = ConvierteValorPropiedad(null, "PasswordVIP", seccion);
                var usuarioOper = ConvierteValorPropiedad(null, "UsuarioOperacionesVIP", seccion);

                var wsVip = new WsOperacionesVip.WebServiceActivarVipClient();
                var expect100 = System.Net.ServicePointManager.Expect100Continue;
                System.Net.ServicePointManager.Expect100Continue = false;

                //wsIndicadores.Endpoint.
                wsVip.ClientCredentials.UserName.UserName = usuario;
                wsVip.ClientCredentials.UserName.Password = password;

                var validaVigencia = paramsVip.ValidaVigencia ? "S" : "N";
                var strVigenciaHasta = paramsVip.ValidaVigencia && paramsVip.FechaHasta.HasValue
                    ? paramsVip.FechaHasta.ToDateStr("ddMMyyyy")
                    : string.Empty;

                var resultado = wsVip.incluirTarjetaVIP(paramsVip.NumCuenta, paramsVip.NumTarjeta
                    , paramsVip.LocalidadTramite, validaVigencia, strVigenciaHasta, "S", usuarioOper);

                System.Net.ServicePointManager.Expect100Continue = expect100;

                paramsVip = ValidaSalidaServicio(resultado.mensajes, paramsVip,
                    new List<int> { EXITOSO, RESPUESTAVIPACTIVO });

                return paramsVip;
            }
            catch (Exception ex)
            {
                Logger.Log(ex.StackTrace, ex.Message);
                throw ex;
            }
        }

        public EntradaVip InactivarVip(EntradaVip paramsVip, string seccion = "")
        {
            try
            {
                seccion = string.Format("{0}-VIP", seccion);
                paramsVip.LocalidadTramite = ConvierteValorPropiedad(paramsVip.LocalidadTramite, "Localidad", seccion);
                var usuario = ConvierteValorPropiedad(null, "UsuarioVIP", seccion);
                var password = ConvierteValorPropiedad(null, "PasswordVIP", seccion);
                var usuarioOper = ConvierteValorPropiedad(null, "UsuarioOperacionesVIP", seccion);

                //Create cliente with the WCF
                var wsVip = new WsOperacionesVip.WebServiceActivarVipClient();
                var expect100 = System.Net.ServicePointManager.Expect100Continue;
                System.Net.ServicePointManager.Expect100Continue = false;

                //wsIndicadores.Endpoint.
                wsVip.ClientCredentials.UserName.UserName = usuario;
                wsVip.ClientCredentials.UserName.Password = password;

                var resultado = wsVip.excluirTarjetaVip(paramsVip.NumCuenta, paramsVip.NumTarjeta,
                    paramsVip.LocalidadTramite, usuarioOper);

                System.Net.ServicePointManager.Expect100Continue = expect100;
                paramsVip = ValidaSalidaServicio(resultado.mensajes, paramsVip,
                    new List<int> { EXITOSO, RESPUESTAVIPINACTIVO });
                return paramsVip;

            }
            catch (Exception ex)
            {
                Logger.Log(ex.StackTrace, ex.Message);
                throw ex;
            }
        }

    }
}

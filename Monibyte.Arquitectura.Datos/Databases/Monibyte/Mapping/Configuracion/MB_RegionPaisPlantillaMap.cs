using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_RegionPaisPlantillaMap : EntityTypeConfiguration<MB_RegionPaisPlantilla>
    {
        public MB_RegionPaisPlantillaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdRegionPais);

            // Properties
            this.Property(t => t.IdRegionPais)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MB_RegionPaisPlantilla", "Mnb");
            this.Property(t => t.IdRegionPais).HasColumnName("IdRegionPais");
            this.Property(t => t.IdPlantilla).HasColumnName("IdPlantilla");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.IdRegion).HasColumnName("IdRegion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

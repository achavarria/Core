﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Configuracion.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Configuracion.Repositorios
{
    public class RepositorioTipoComercio : RepositorioMnb<MB_TipoComercio>, IRepositorioTipoComercio
    {
        public RepositorioTipoComercio(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public MB_TipoComercio ObtenerPorId(int idMcc)
        {
            var criteria = new Criteria<MB_TipoComercio>();
            criteria.And(dt => dt.IdMcc == idMcc);
            return this.SelectBy(criteria).FirstOrDefault();
        }
    }

    public class RepositorioPlantilla : RepositorioMnb<MB_Plantilla>, IRepositorioPlantilla
    {
        public RepositorioPlantilla(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public bool ExistePlantilla(string nombre, int idEmpresa)
        {
            var criteria = new Criteria<MB_Plantilla>();
            criteria.And(dt => dt.IdEmpresa == idEmpresa);
            criteria.And(dt => dt.Descripcion.ToLower().Trim() == nombre.ToLower().Trim());
            var resultado = this.SelectBy(criteria).FirstOrDefault();
            return resultado != null;
        }
    }
}

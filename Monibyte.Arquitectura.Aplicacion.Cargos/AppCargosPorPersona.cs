﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco.Cargos;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public class AppCargosPorPersona : AplicacionBase, IAppCargosPorPersona
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CA_CargosPorPersona> _repCargosPorPersona;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<CA_TipoCargo> _repTipoCargo;
        private IRepositorioCuenta _repCuenta;
        private IRepositorioMnb<CA_RangosTabla> _repRangosTabla;
        private IRepositorioMnb<CA_DetRangosCargosPer> _repDetRangosCargosPer;
        private IRepositorioMnb<CA_ValOrigenCargoPer> _repValOrigenCargoPer;

        public AppCargosPorPersona(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CA_CargosPorPersona> repCargosPorPersona,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<CA_TipoCargo> repTipoCargo,
            IRepositorioCuenta repCuenta,
            IRepositorioMnb<CA_RangosTabla> repRangosTabla,
            IRepositorioMnb<CA_DetRangosCargosPer> repDetRangosCargosPer,
            IRepositorioMnb<CA_ValOrigenCargoPer> repValOrigenCargoPer)
        {
            _repCatalogo = repCatalogo;
            _repCargosPorPersona = repCargosPorPersona;
            _repPersona = repPersona;
            _repTipoCargo = repTipoCargo;
            _repCuenta = repCuenta;
            _repRangosTabla = repRangosTabla;
            _repDetRangosCargosPer = repDetRangosCargosPer;
            _repValOrigenCargoPer = repValOrigenCargoPer;
        }

        public void Eliminar(PocCargosPorPersona registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CA_CargosPorPersona>();
                _repValOrigenCargoPer.DeleteOn(det => det.IdCargo == entrada.IdCargo);
                _repDetRangosCargosPer.DeleteOn(det => det.IdCargo == entrada.IdCargo);
                _repCargosPorPersona.Delete(entrada);
                _repCargosPorPersona.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocCargosPorPersona registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_CargosPorPersona>();
                    var idCargo = _fGenerales.ObtieneSgteSecuencia("SEQ_IdCargo");

                    entrada.IdCargo = idCargo;
                    entrada.IdEstado = (int)EnumIdEstado.Activo;
                    _repCargosPorPersona.Insert(entrada);
                    _repCargosPorPersona.UnidadTbjo.Save();

                    if (registro.ListaDetalleCargos != null)
                    {
                        foreach (PocDetalleCargos detalle in registro.ListaDetalleCargos)
                        {
                            var entradaDetalleCargos = new PocDetRangosCargosPer
                            {
                                IdCargo = idCargo,
                                IdTablaRangos = detalle.IdTablaRangos,
                                MonCargo = detalle.MonCargo,
                                RangoMin = detalle.RangoMin,
                                RangoMax = detalle.RangoMax
                            };
                            var entradaEnviar = entradaDetalleCargos.ProyectarComo<CA_DetRangosCargosPer>();
                            _repDetRangosCargosPer.Insert(entradaEnviar);
                        }
                        _repDetRangosCargosPer.UnidadTbjo.Save();
                    }

                    if (registro.ListaOrigenCargos != null)
                    {
                        foreach (PocValOrigenCargoPer detalle in registro.ListaOrigenCargos)
                        {
                            detalle.IdCargo = entrada.IdCargo;

                            var entradaOrigCargo = detalle.ProyectarComo<CA_ValOrigenCargoPer>();
                            _repValOrigenCargoPer.Insert(entradaOrigCargo);
                        }
                        _repValOrigenCargoPer.UnidadTbjo.Save();
                    }
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocCargosPorPersona));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocCargosPorPersona registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_CargosPorPersona>();

                    _repCargosPorPersona.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repCargosPorPersona.UnidadTbjo.Save();

                    if (registro.ListaDetalleCargos != null)
                    {
                        _repDetRangosCargosPer.DeleteOn(det => det.IdCargo == registro.IdCargo);

                        foreach (PocDetalleCargos detalle in registro.ListaDetalleCargos)
                        {
                            var entradaDetalleCargos = new PocDetRangosCargosPer
                            {
                                IdCargo = registro.IdCargo,
                                IdTablaRangos = detalle.IdTablaRangos,
                                MonCargo = detalle.MonCargo,
                                RangoMin = detalle.RangoMin,
                                RangoMax = detalle.RangoMax
                            };
                            var entradaEnviar = entradaDetalleCargos.ProyectarComo<CA_DetRangosCargosPer>();
                            _repDetRangosCargosPer.Insert(entradaEnviar);
                        }
                        _repDetRangosCargosPer.UnidadTbjo.Save();
                    }

                    if (registro.ListaOrigenCargos != null)
                    {
                        _repValOrigenCargoPer.DeleteOn(det => det.IdCargo == entrada.IdCargo);
                        foreach (PocValOrigenCargoPer detalle in registro.ListaOrigenCargos)
                        {
                            detalle.IdCargo = entrada.IdCargo;

                            var entradaOrigCargo = detalle.ProyectarComo<CA_ValOrigenCargoPer>();
                            _repValOrigenCargoPer.Insert(entradaOrigCargo);
                        }
                        _repValOrigenCargoPer.UnidadTbjo.Save();
                    }
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocCargosPorPersona));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocCargosPorPersona> Listar(PocCargosPorPersona filtro, DataRequest request)
        {
            var criteria = new Criteria<CA_CargosPorPersona>();
            var resultado = _repCargosPorPersona.SelectBy(criteria)
                .Join(_repPersona.Table,
                    car => car.IdPersona, per => per.IdPersona,
                    (car, per) => new
                    {
                        car,
                        per
                    })
                .Join(_repTipoCargo.Table,
                    tmp => tmp.car.IdTipoCargo, tcar => tcar.IdTipoCargo,
                    (tmp, tcar) => new
                    {
                        tmp.car,
                        tmp.per,
                        tcar
                    })
                .Join(_repCatalogo.List("LTIPOCOBROCARGO"),
                    tmp => tmp.car.IdTipoCobro ?? tmp.tcar.IdTipoCobro, tcob => tcob.IdCatalogo,
                    (tmp, tcob) => new
                    {
                        tmp.car,
                        tmp.per,
                        tmp.tcar,
                        tcob
                    })
                .Join(_repRangosTabla.Table,
                    tmp => tmp.car.IdTablaRangos ?? tmp.tcar.IdTablaRangos, tran => tran.IdTablaRangos,
                    (tmp, tran) => new
                    {
                        tmp.car,
                        tmp.per,
                        tmp.tcar,
                        tmp.tcob,
                        tran
                    })
                .Join(_repCatalogo.List("LPERIODICIDAD"),
                    tmp => tmp.car.IdPeriodicidad ?? tmp.tcar.IdPeriodicidad, fre => fre.IdCatalogo,
                    (tmp, fre) => new
                    {
                        tmp.car,
                        tmp.per,
                        tmp.tcar,
                        tmp.tcob,
                        tmp.tran,
                        fre
                    })
                .Join(_repCatalogo.List("LACTINACTIVA"),
                    tmp => tmp.car.IdEstado, fre => fre.IdCatalogo,
                    (tmp, est) => new
                    {
                        tmp.car,
                        tmp.per,
                        tmp.tcar,
                        tmp.tcob,
                        tmp.tran,
                        tmp.fre,
                        est
                    })
                .GroupJoin(_repCuenta.Table,
                    tmp => tmp.car.IdCuentaCobro, cta => cta.IdCuenta,
                    (tmp, cta) => new
                    {
                        tmp.car,
                        tmp.per,
                        tmp.tcar,
                        tmp.tcob,
                        tmp.tran,
                        tmp.fre,
                        tmp.est,
                        cta = cta.FirstOrDefault()
                    })
                .Where(tmp =>
                    (string.IsNullOrEmpty(filtro.NombrePersona) || tmp.per.NombreCompleto.ToLower().Contains(filtro.NombrePersona.ToLower())) &&
                    (string.IsNullOrEmpty(filtro.NumIdentificacion) || tmp.per.NumIdentificacion.ToLower().Contains(filtro.NumIdentificacion.ToLower())))
                .Select(item => new PocCargosPorPersona
                    {
                        IdCargo = item.car.IdCargo,
                        IdPersona = item.car.IdPersona,
                        IdTipoCargo = item.car.IdTipoCargo,
                        FecInicio = item.car.FecInicio,
                        FecFinal = item.car.FecFinal,
                        IdCuentaCobro = item.car.IdCuentaCobro,
                        IdTipoCobro = item.tcob.IdCatalogo,
                        MonCargo = item.car.IdTipoCobro != null ? item.car.MonCargo : item.tcar.MonCargo,
                        IdTablaRangos = item.tran.IdTablaRangos,
                        IdPeriodicidad = item.fre.IdCatalogo,
                        ValPeriodicidad = item.car.IdPeriodicidad == null ?
                                          item.tcar.ValPeriodicidad : item.car.ValPeriodicidad,
                        FecUltCargo = item.car.FecUltCargo,
                        IdEstado = item.car.IdEstado,
                        NombrePersona = item.per.NombreCompleto,
                        DescTipoCargo = item.tcar.Descripcion,
                        DescCuentaCobro = item.cta != null ? item.cta.NumCuenta : "",
                        DescTipoCobro = item.tcob.Descripcion,
                        DescTablaRangos = item.tran.Descripcion,
                        DescPeriodicidad = item.fre.Descripcion,
                        DescEstado = item.est.Descripcion
                    });
            return resultado.ToDataResult(request);
        }

        public ValidationResponse Validar(PocCargosPorPersona registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.IdPersona < 0)
            {
                validacion.AddError("Err_0035");
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }
    }
}

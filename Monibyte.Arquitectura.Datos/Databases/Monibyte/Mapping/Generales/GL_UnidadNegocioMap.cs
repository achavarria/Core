using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_UnidadNegocioMap : EntityTypeConfiguration<GL_UnidadNegocio>
    {
        public GL_UnidadNegocioMap()
        {
            // Primary Key
            this.HasKey(t => t.IdUnidad);

            // Properties
            this.Property(t => t.IdUnidad)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(120);

            // Table & Column Mappings
            this.ToTable("GL_UnidadNegocio", "General");
            this.Property(t => t.IdUnidad).HasColumnName("IdUnidad");
            this.Property(t => t.IdEntidad).HasColumnName("IdEntidad");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.IdEsPrincipal).HasColumnName("IdEsPrincipal");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

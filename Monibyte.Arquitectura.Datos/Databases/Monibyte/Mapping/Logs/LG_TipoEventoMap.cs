using Monibyte.Arquitectura.Dominio.Logs;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Logs
{
    public class LG_TipoEventoMap : EntityTypeConfiguration<LG_TipoEvento>
    {
        public LG_TipoEventoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipoEvento);

            // Properties
            this.Property(t => t.IdTipoEvento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("LG_TipoEvento", "Logs");
            this.Property(t => t.IdTipoEvento).HasColumnName("IdTipoEvento");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

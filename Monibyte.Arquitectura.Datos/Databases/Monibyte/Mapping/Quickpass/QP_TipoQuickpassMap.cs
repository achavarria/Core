using Monibyte.Arquitectura.Dominio.Quickpass;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Quickpass
{
    public class QP_TipoQuickpassMap : EntityTypeConfiguration<QP_TipoQuickpass>
    {
        public QP_TipoQuickpassMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipoQuickpass);

            // Properties
            this.Property(t => t.IdTipoQuickpass)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("QP_TipoQuickpass", "Quickpass");
            this.Property(t => t.IdTipoQuickpass).HasColumnName("IdTipoQuickpass");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdPrepago).HasColumnName("IdPrepago");
            this.Property(t => t.MonCargaInicial).HasColumnName("MonCargaInicial");
            this.Property(t => t.MonMinRecargar).HasColumnName("MonMinRecargar");
            this.Property(t => t.MonMinListaNegra).HasColumnName("MonMinListaNegra");
            this.Property(t => t.IdMonedaPeaje).HasColumnName("IdMonedaPeaje");
            this.Property(t => t.IdCargoDiario).HasColumnName("IdCargoDiario");
            this.Property(t => t.MonCostoAdm).HasColumnName("MonCostoAdm");
            this.Property(t => t.MonCostoUnidad).HasColumnName("MonCostoUnidad");
            this.Property(t => t.MesesVigencia).HasColumnName("MesesVigencia");
            this.Property(t => t.IdMonedaAdm).HasColumnName("IdMonedaAdm");
            this.Property(t => t.MesesFinanciamiento).HasColumnName("MesesFinanciamiento");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
        }
    }
}

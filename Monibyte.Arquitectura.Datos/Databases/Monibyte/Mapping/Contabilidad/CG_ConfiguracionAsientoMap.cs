using Monibyte.Arquitectura.Dominio.Contabilidad;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Contabilidad
{
    public class CG_ConfiguracionAsientoMap : EntityTypeConfiguration<CG_ConfiguracionAsiento>
    {
        public CG_ConfiguracionAsientoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdConfAsiento);

            // Properties
            this.Property(t => t.DescMemo)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("CG_ConfiguracionAsiento", "Contabilidad");
            this.Property(t => t.IdConfAsiento).HasColumnName("IdConfAsiento");
            this.Property(t => t.IdTipoMovimiento).HasColumnName("IdTipoMovimiento");
            this.Property(t => t.IdOrigenMovimiento).HasColumnName("IdOrigenMovimiento");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.Orden).HasColumnName("Orden");
            this.Property(t => t.IdCustomer).HasColumnName("IdCustomer");
            this.Property(t => t.IdAccount).HasColumnName("IdAccount");
            this.Property(t => t.IdItem).HasColumnName("IdItem");
            this.Property(t => t.DescMemo).HasColumnName("DescMemo");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_RangoCategoriaTipoComercioMap : EntityTypeConfiguration<MB_RangoCategoriaTipoComercio>
    {
        public MB_RangoCategoriaTipoComercioMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdRango, t.IdCategoriaMcc });

            // Properties
            this.Property(t => t.IdRango)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdCategoriaMcc)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodMccDesde)
                .IsRequired()
                .HasMaxLength(6);

            this.Property(t => t.CodMccHasta)
                .IsRequired()
                .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("MB_RangoCategoriaTipoComercio", "Mnb");
            this.Property(t => t.IdRango).HasColumnName("IdRango");
            this.Property(t => t.IdCategoriaMcc).HasColumnName("IdCategoriaMcc");
            this.Property(t => t.CodMccDesde).HasColumnName("CodMccDesde");
            this.Property(t => t.CodMccHasta).HasColumnName("CodMccHasta");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");

        }
    }
}

﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Notificacion;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public interface IAppNotificacion : IAplicacionBase
    {
        IEnumerable<PocConfigNotificacion> ObtenerNotificaciones(int? idContexto, int? idEmpresa, int? idEsActivo, int ExcluirAdminInt = 1);
        IEnumerable<PocConfigNotificacion> EstablecerNotificacion(List<PocConfigNotificacion> notificaciones);
        IEnumerable<PocDestinatarioDefault> ObtenerDestinatarios(int idEmpresa);
        void ProcesarNotificaciones(PocoQueue pocoQueue);
        void EnviarNotificacion(PocoQueue pocoQueue);
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Configuracion;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public class AppConfiguracionTipoLic : AplicacionBase, IAppConfiguracionTipoLic
    {
        private IRepositorioMnb<CA_TipoCargo> _repTipoCargo;
        private IRepositorioMnb<MB_TipoLicencia> _repTipoLicencia;

        public AppConfiguracionTipoLic(
            IRepositorioMnb<CA_TipoCargo> repTipoCargo,
            IRepositorioMnb<MB_TipoLicencia> repTipoLicencia)
        {
            _repTipoCargo = repTipoCargo;
            _repTipoLicencia = repTipoLicencia;
        }

        public void Eliminar(PocTipoLicencia registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<MB_TipoLicencia>();
                _repTipoLicencia.Delete(entrada);
                _repTipoLicencia.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocTipoLicencia registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<MB_TipoLicencia>();
                    var idTipoLicencia = _fGenerales.ObtieneSgteSecuencia("SEQ_IdTipoLicencia");
                    entrada.IdTipoLicencia = idTipoLicencia;
                    _repTipoLicencia.Insert(entrada);
                    _repTipoLicencia.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocTipoLicencia));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocTipoLicencia registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<MB_TipoLicencia>();
                    _repTipoLicencia.UpdateExclude(entrada, p => p.FecInclusionAud, p => p.IdUsuarioIncluyeAud);
                    _repTipoLicencia.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocTipoLicencia));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocTipoLicencia> Listar(PocTipoLicencia filtro, DataRequest request)
        {
            var criteria = new Criteria<MB_TipoLicencia>();

            var resultado = _repTipoLicencia
               .SelectBy(criteria)
               .Join(_repTipoCargo.Table,
                   c => c.IdTipoCargo,
                   t => t.IdTipoCargo,
                   (c, t) => new { c, t })
               .Select(item => new PocTipoLicencia
               {
                   IdTipoLicencia = item.c.IdTipoLicencia,
                   IdTipoCargo = item.c.IdTipoCargo,
                   IdEstado = item.c.IdEstado,
                   Descripcion = item.c.Descripcion,
                   DescTipoCargo = item.t.Descripcion
               });
            return resultado.ToDataResult(request);
        }

        public IEnumerable<PocTipoLicencia> ListadoTipoLicencia()
        {
            var parametros = _repTipoLicencia.Table.ToList();
            return parametros.ProyectarComo<PocTipoLicencia>();
        }

        private ValidationResponse Validar(PocTipoLicencia registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.IdTipoLicencia < 0) { validacion.AddError("Err_0035"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }
    }
}

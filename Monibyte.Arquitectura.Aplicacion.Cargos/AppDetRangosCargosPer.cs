﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Cargos;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public class AppDetRangosCargosPer : AplicacionBase, IAppDetRangosCargosPer
    {
        private IRepositorioMnb<CA_DetRangosCargosPer> _repDetRangosCargosPer;

        public AppDetRangosCargosPer(IRepositorioMnb<CA_DetRangosCargosPer> repDetRangosCargosPer)
        {
            _repDetRangosCargosPer = repDetRangosCargosPer;
        }

        public void Eliminar(PocDetRangosCargosPer registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CA_DetRangosCargosPer>();
                _repDetRangosCargosPer.Delete(entrada);
                _repDetRangosCargosPer.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocDetRangosCargosPer registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_DetRangosCargosPer>();
                    _repDetRangosCargosPer.Insert(entrada);
                    _repDetRangosCargosPer.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocDetRangosCargosPer));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocDetRangosCargosPer registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_DetRangosCargosPer>();
                    _repDetRangosCargosPer.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repDetRangosCargosPer.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocDetRangosCargosPer));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocDetRangosCargosPer> Listar(PocDetRangosCargosPer filtro, DataRequest request)
        {
            var criteria = new Criteria<CA_DetRangosCargosPer>();
            if (filtro != null)
            {
                if (filtro.IdTablaRangos >= 0)
                {
                    criteria.And(m => m.IdTablaRangos == filtro.IdTablaRangos);
                }
            }
            var resultado = _repDetRangosCargosPer.SelectBy(criteria)
                .Select(item => new PocDetRangosCargosPer
                {
                    IdTablaRangos = item.IdTablaRangos,
                    MonCargo = item.MonCargo,
                    RangoMax = item.RangoMax,
                    RangoMin = item.RangoMin,
                    IdCargo = item.IdCargo

                });
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocDetRangosCargosPer registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.IdTablaRangos < 0)
            {
                validacion.AddError("Err_0035");
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }
    }
}

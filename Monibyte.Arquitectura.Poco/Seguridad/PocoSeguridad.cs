﻿using System;
namespace Monibyte.Arquitectura.Poco.Seguridad
{
    public class PocAccion
    {
        public int IdAccion { get; set; }
        public string Descripcion { get; set; }
        public string Controlador { get; set; }
        public string Area { get; set; }
        public int IdEstado { get; set; }
    }

    public class PocAccionMenu
    {
        public int? IdAccion { get; set; }
        public int? IdMenu { get; set; }
        public int? IdMenuPadre { get; set; }
        public string Area { get; set; }
        public string Controlador { get; set; }
        public string Descripcion { get; set; }
        public string DescMenu { get; set; }
        public int? AyudaVigente { get; set; }
    }

    public class PocPreguntaUsuario
    {
        public int IdPregunta { get; set; }
        public string Respuesta { get; set; }
        public string Descripcion { get; set; }
        public string CodUsuario { get; set; }
    }

    public class PocCambioContrasena
    {
        public int DiasVigencia { get; set; }
        public string CodUsuario { get; set; }
        public string Contrasena { get; set; }
        public string ContrasenaActual { get; set; }
    }

    public class PocCredencialesDirectorio
    {
        public string CodUsuario { get; set; }
        public string Password { get; set; }
        public string TokenPin { get; set; }
        public string TokenSeed { get; set; }
    }

    public class PocDobleFactorAut
    {
        public string ManualEntrySetupCode { get; set; }
        public string QRCodeImageUrl { get; set; }
    }

    public class PocAutorizacion
    {
        public int IdAutorizacion { get; set; }
        public int IdTipoAutorizacion { get; set; }
        public int IdEmpresa { get; set; }
        public string NumReferencia { get; set; }
        public string TipoAutorizacion { get; set; }
        public string EstadoAutorizacion { get; set; }
        public int IdUsuarioPrepara { get; set; }
        public string UsuarioEditor { get; set; }
        public DateTime FecPrepara { get; set; }
        public int? IdUsuarioAutoriza { get; set; }
        public DateTime? FecAutoriza { get; set; }
        public int IdEstado { get; set; }
        public string DetalleAutorizacion { get; set; }
        public string Descripcion { get; set; }
    }

    public class PocInfoAutorizacion
    {
        public string DescripcionProducto { get; set; }
        public string DetalleAutorizacion { get; set; }
    }

    public class PocMenu
    {
        public int IdMenu { get; set; }
        public string Descripcion { get; set; }
        public bool AyudaVigente { get; set; }
    }

    public class PocActivacionUsuario
    {
        public int IdUsuario { get; set; }
        public string CodActivacion { get; set; }
        public int IdEstado { get; set; }

        public int CantDigitos { get; set; }
        public string NumTarjeta { get; set; }
        public string Password { get; set; }
        public string CodUsuario { get; set; }
    }

    public class PocAutorizacionApi
    {
        public string usuario { get; set; }
        public string contrasena { get; set; }
    }

    public class PocEmpresasApi
    {
        public string idEntidad { get; set; }
        public int idTarjeta { get; set; }
        public int idEmpresa { get; set; }
    }
}

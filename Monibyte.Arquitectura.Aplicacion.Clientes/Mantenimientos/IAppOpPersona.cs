﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Clientes;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public interface IAppOpPersona : IAplicacionBase
    {
        void Eliminar(PocInfoPersona registro);
        void Insertar(PocInfoPersona registro);
        void Modificar(PocInfoPersona registro);
        PocInfoPersona Obtener(PocInfoPersona filtro);
        DataResult<PocInfoPersona> Listar(PocInfoPersona filtro, DataRequest request);
        bool ComprobarIdentificacion(string numIdentificacion, int idIdentificacion);
        void AsociarEjecutivo(int[] empresas, int? idEjecutivo);
        string ObtenerCorreoPersona(int idPersona, string codUsuario);
    }
}

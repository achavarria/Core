﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Clientes;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public interface IAppOpProfesion : IAplicacionBase
    {
        List<PocProfesion> ListaProfesiones();
    }
}

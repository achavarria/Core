﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Procesador;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Notificacion;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Poco.Notificacion;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public partial class AppNotificacion : AppTraductorFiltro, IAppNotificacion
    {
        private IAppAutorizaciones _appAutorizaciones;
        private IRepositorioMnb<GL_Usuario> _repUsuario;
        private IRepositorioMnb<TC_Tarjeta> _repTarjeta;
        private IRepositorioNotificacion _repNotificacion;
        private IRepositorioMnb<CL_Comunicacion> _repComunicacion;
        private IRepositorioMnb<CL_EmpresasUsuario> _repEmpresaUsuario;
        private IRepositorioMnb<NT_IndicadorEnvio> _repNTIndicadorEnvio;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _repvwEmpresaUsuario;

        public AppNotificacion(
            IRepositorioCatalogo repCatalogo,
            IAppAutorizaciones appAutorizaciones,
            IRepositorioMnb<GL_Usuario> repUsuario,
            IRepositorioMnb<TC_Tarjeta> repTarjeta,
            IRepositorioNotificacion repNotificacion,
            IRepositorioMnb<CL_Comunicacion> repComunicacion,
            IRepositorioMnb<CL_EmpresasUsuario> repEmpresaUsuario,
            IRepositorioMnb<NT_IndicadorEnvio> repNTIndicadorEnvio,
            IRepositorioMnb<CL_vwEmpresasUsuario> repvwEmpresaUsuario
        )
            : base()
        {
            _repTarjeta = repTarjeta;
            _repUsuario = repUsuario;
            _repCatalogo = repCatalogo;
            _repNotificacion = repNotificacion;
            _repComunicacion = repComunicacion;
            _repEmpresaUsuario = repEmpresaUsuario;
            _appAutorizaciones = appAutorizaciones;
            _repNTIndicadorEnvio = repNTIndicadorEnvio;
            _repvwEmpresaUsuario = repvwEmpresaUsuario;
        }

        public IEnumerable<PocConfigNotificacion> ObtenerNotificaciones(int? idContexto, int? idEmpresa, int? idEsActivo)
        {
            return _repNotificacion.ObtenerNotificaciones(idContexto, idEmpresa, idEsActivo).Select(item =>
            {
                var not = item.ProyectarComo<PocConfigNotificacion>();
                not.DetalleFiltro = Traducir(item.DetalleFiltro);
                return not;
            }).ToList();
        }

        public IEnumerable<PocConfigNotificacion> EstablecerNotificacion(List<PocConfigNotificacion> notificaciones)
        {
            if (notificaciones == null)
            {
                throw new CoreException("Datos nulos al guardar la notificacion", "Err_0035");
            }
            //traducir los datos del detalle y establecerlos de acuerdo al tipo
            using (var ts = new TransactionScope())
            {
                notificaciones.ForEach(notificacion =>
                {
                    var validacion = Validar(notificacion);
                    if (validacion.IsValid)
                    {
                        var _record = notificacion.ProyectarComo<NT_Configuracion>();
                        _record.DetalleFiltro = Traducir(notificacion.DetalleFiltro);

                        if (!notificacion.IdConfiguracion.HasValue)
                        {
                            var _notifyId = _fGenerales.ObtieneSgteSecuencia("SEQ_IdNotificacion");
                            notificacion.IdConfiguracion = _record.IdConfiguracion = _notifyId;
                            notificacion.IdEmpresa = _record.IdEmpresa;
                            _repNotificacion.Insert(_record);
                            _repNotificacion.UnidadTbjo.Save();
                        }
                        else
                        {
                            _repNotificacion.UpdateOn(n => n.IdConfiguracion == notificacion.IdConfiguracion,
                                up => new NT_Configuracion
                                {
                                    IdEstado = notificacion.IdEstado,
                                    Destinatario = _record.Destinatario,
                                    DetalleFiltro = _record.DetalleFiltro,
                                    FecVigenciaHasta = _record.FecVigenciaHasta,
                                    IdNotificaAdministradores = _record.IdNotificaAdministradores,
                                    IdNotificaTarjetahabiente = _record.IdNotificaTarjetahabiente
                                });
                        }
                    }
                    else
                    {
                        var msj = string.Format("Error al Insertar {0}", typeof(PocConfigNotificacion));
                        throw new CoreException(msj, validation: validacion);
                    }
                });
                ts.Complete();
            }
            return notificaciones;
        }

        private ValidationResponse Validar(PocConfigNotificacion notificacion)
        {
            var validacion = new ValidationResponse();
            if (notificacion.IdEstado == (int)EnumIdEstado.Activo)
            {
                if (notificacion.FecVigenciaHasta < DateTime.Today)
                {
                    validacion.AddError("Err_0112");
                }
                if (!notificacion.Destinatarios.IsNullOrEmpty())
                {
                    notificacion.Destinatarios.Update(x =>
                    {
                        var r = x.Valores.ToList();
                        r.RemoveAll(v => string.IsNullOrEmpty(v));
                        x.Valores = r.ToArray();
                        if (x.Canal == CanalNotificacion.Email)
                        {
                            x.Valores.ToList().ForEach(email =>
                            {
                                try { new System.Net.Mail.MailAddress(email); }
                                catch { validacion.AddError("Err_0122", email); }
                            });
                        }
                    });
                    notificacion.Destinatarios.RemoveAll
                        (x => x.Valores.IsNullOrEmpty());
                }
                if (notificacion.Destinatarios.IsNullOrEmpty())
                {
                    notificacion.Destinatarios = null;
                    if (notificacion.IdEstado == (int)EnumIdEstado.Activo &&
                        !notificacion.NotificaAdministradores &&
                        !notificacion.NotificaTarjetahabiente)
                    {
                        validacion.AddError("Err_0121");
                    }
                }
            }
            return validacion;
        }

        public IEnumerable<PocDestinatarioDefault> ObtenerDestinatarios(int idEmpresa)
        {
            var result =
                from us in _repUsuario.Table
                join eu in _repEmpresaUsuario.Table on
                    us.IdUsuario equals eu.IdUsuario
                join ct in _repCatalogo.Table on
                    eu.IdTipoUsuario equals ct.IdCatalogo
                let em = _repComunicacion.Table.Where(x =>
                    x.IdPersona == us.IdPersona &&
                    x.IdTipo == (int)EnumTipoComunicacion.Correo
                ).FirstOrDefault()
                let ce = _repComunicacion.Table.Where(x =>
                    x.IdPersona == us.IdPersona &&
                    x.IdTipo == (int)EnumTipoComunicacion.Celular
                ).FirstOrDefault()
                where eu.IdEmpresa == idEmpresa &&
                    (em != null || ce != null) &&
                    ct.Referencia3 == "Adm"
                select new PocDestinatarioDefault
                {
                    IdPersona = us.IdPersona,
                    Correo = em != null ? em.ValComunicacion : null,
                    Celular = ce != null ? ce.ValComunicacion : null
                };
            return result.ToList();
        }

        public void ProcesarNotificaciones(PocoQueue pocoQueue)
        {
            if (pocoQueue != null && pocoQueue.EntityId != 0 && !string.IsNullOrEmpty(pocoQueue.MessageType))
            {
                typeof(AppNotificacion).GetMethod("Method" + pocoQueue.MessageType, BindingFlags.Instance | BindingFlags.Public).Invoke(this, new object[] { pocoQueue });
            }
        }

        private void ProcesarNotificaciones(PocoQueue pocoQueue, bool filter, bool validateSend = false)
        {
            var dataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(pocoQueue.Data);
            var notificationList = ObtenerNotificaciones(pocoQueue.OriginId, pocoQueue.CompanyId, (int)EnumIdEstado.Activo);
            foreach (var not in notificationList)
            {
                var date = System.DateTime.Now.Date;
                if (not.FecVigenciaHasta.HasValue && (date > ((DateTime)not.FecVigenciaHasta).Date))
                {
                    break;
                }

                bool _validated = true;
                if (filter)
                {
                    var conditionSql = ToSqlStr(not.DetalleFiltro);
                    conditionSql = ConstruirWhere(conditionSql, dataDictionary, pocoQueue);
                    _validated = _repNotificacion.ValidarFiltroNotificacion(conditionSql) == (int)EnumIdEstado.Activo;
                    #region ActualizaIndicadorEnvio
                    if (validateSend)
                    {
                        using (var ts = new TransactionScope())
                        {
                            NT_IndicadorEnvio ntIndicador = new NT_IndicadorEnvio()
                            {
                                IdEntidad = Convert.ToInt16(dataDictionary["IdEntidad"].ToString()),
                                IdEmpresa = Convert.ToInt16(dataDictionary["IdEmpresa"].ToString()),
                                IdProducto = Convert.ToInt16(dataDictionary["IdCuenta"].ToString()),
                                IdContexto = pocoQueue.OriginId,
                                SubContexto = pocoQueue.SubOrigin ?? null,
                                Notificar = _validated ? (int)EnumIdSiNo.No : (int)EnumIdSiNo.Si
                            };
                            _repNTIndicadorEnvio.Update(ntIndicador, x => x.Notificar);
                            _repNTIndicadorEnvio.UnidadTbjo.Save();
                            ts.Complete();
                        }
                    }
                    #endregion
                }
                if (_validated)
                {
                    var addressList = _repNotificacion.ObtenerDestinatarios(not, dataDictionary);
                    pocoQueue.Recipients = addressList;
                    pocoQueue.SubOrigin = not.SubContexto;
                    ThreadPool.QueueUserWorkItem(state => EnviarNotificacion(pocoQueue));
                }
            }
        }

        private string ConstruirWhere(string conditionSql, Dictionary<string, object> data, PocoQueue pocoQueue)
        {
            foreach (KeyValuePair<string, object> entry in data)
            {
                string valueStr = null;
                var keyStr = string.Format("@@{0}", entry.Key);
                if (entry.Value is string)
                {
                    valueStr = "'" + entry.Value.ToString() + "'";
                }
                else if (entry.Value is DateTime)
                {
                    var value = entry.Value as DateTime?;
                    valueStr = "'" + value + "'";
                }
                else if (entry.Value is Double)
                {
                    var value = entry.Value as Double?;
                    valueStr = value.Value.ToString(CultureInfo.InstalledUICulture);
                }
                else if (entry.Value is Int64 || entry.Value is Int32 || entry.Value is Int16 || entry.Value is int)
                {
                    var value = entry.Value as Int64?;
                    valueStr = value.Value.ToString();
                }
                conditionSql = conditionSql.Replace(keyStr, valueStr);
            }
            conditionSql = conditionSql.Replace("@@OriginId", pocoQueue.OriginId.ToString());
            conditionSql = conditionSql.Replace("@@SubOrigin", string.IsNullOrEmpty(pocoQueue.SubOrigin) ? "NULL" : pocoQueue.SubOrigin);
            return conditionSql;
        }

        private void EnviarNotificacion(PocoQueue pocoQueue)
        {
            try
            {
                var dataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(pocoQueue.Data);
                if (!dataDictionary.ContainsKey("LocalTransactionDate"))
                {
                    dataDictionary["LocalTransactionDate"] = DateTime.Now.Date.ToString("yyyyMMdd");

                }
                if (!dataDictionary.ContainsKey("LocalTransactionTime"))
                {
                    dataDictionary["LocalTransactionTime"] = DateTime.Now.TimeOfDay.ToString("hhmmss");
                }
                pocoQueue.Data = dataDictionary.ToJson();
                pocoQueue.Language = (InfoSesion.Lang ?? Config.LANG_DEFAULT).Code;
                RestClient.Post<object>("ApiNotification", "notifications", pocoQueue);
            }
            catch (Exception ex)
            {
                Logger.Log(string.Format("Error: {0} / Poco: {1}", ex.Message, pocoQueue.ToJson()), titulo:
                    "EnviarNotificacion", idEvento: 200, severity: System.Diagnostics.TraceEventType.Error);
            }
        }
    }
}

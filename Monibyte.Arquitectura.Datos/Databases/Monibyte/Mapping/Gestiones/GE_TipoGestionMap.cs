using Monibyte.Arquitectura.Dominio.Gestiones;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Gestiones
{
    public class GE_TipoGestionMap : EntityTypeConfiguration<GE_TipoGestion>
    {
        public GE_TipoGestionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipoGestion);

            // Properties
            this.Property(t => t.IdTipoGestion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GE_TipoGestion", "Gestion");
            this.Property(t => t.IdTipoGestion).HasColumnName("IdTipoGestion");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdSistema).HasColumnName("IdSistema");
            this.Property(t => t.AplicaA).HasColumnName("AplicaA");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.ReqAdministracion).HasColumnName("ReqAdministracion");
            this.Property(t => t.IdEntidad).HasColumnName("IdEntidad");
            this.Property(t => t.PlantillaDetalle).HasColumnName("PlantillaDetalle");
            this.Property(t => t.IdVisualizaAdmin).HasColumnName("IdVisualizaAdmin");
        }

    }
}

﻿using System.Collections.Generic;
using Monibyte.Arquitectura.Aplicacion.Nucleo;
using Monibyte.Arquitectura.Poco.Tarjetas;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH
{
    public interface IOperacionesATH : IAplBase
    {
        PocSalidaConsultaTarjeta ConsultaTarjeta(PocEntradaConsultaTarjeta pocEntrada);
        PocSalidaPagoTarjeta PagoTarjeta(PocEntradaPagoTarjeta pocEntrada);
        PocSalidaPagoTarjeta ReversaPagoTarjeta(PocEntradaPagoTarjeta pocEntrada);
        PocSalidaPagoTarjeta PagoEspecialTarjeta(PocEntradaPagoTarjeta pocEntrada);
        PocSalidaPagoTarjeta ReversaPagoEspecialTarjeta(PocEntradaPagoTarjeta pocEntrada);
        PocSalidaActualizaTarjeta ActualizaEstadoTarjeta(PocEntradaActualizaTarjeta pocEntrada);
        PocSalidaActualizaLimiteTarjeta ActualizaLimiteCuenta(PocEntradaActualizaLimiteTarjeta pocEntrada);
        PocSalidaActualizaLimiteTarjeta ActualizaLimiteTarjeta(PocEntradaActualizaLimiteTarjeta pocEntrada);        
        PocSalidaActivaTarjeta ActivaTarjeta(PocEntradaActivaTarjeta pocEntrada);
        PocSalidaReposicionPinTarjeta ReposicionPinTarjeta(PocEntradaReposicionPinTarjeta pocEntrada);
        IEnumerable<PocSalidaMovimientosTarjeta> MovimientosTarjeta(PocEntradaMovimientosTarjeta pocEntrada);
        IEnumerable<PocSalidaMovimientosTarjeta> MovimientosTransitoTarjeta(PocEntradaMovimientosTarjeta pocEntrada);
        IEnumerable<PocSalidaLealtadTarjeta> ConsultaProgLealtadTarjeta(PocEntradaLealtadTarjeta pocEntrada);
        PocSalidaAplicaMovLealtadTarjeta AplicaMovProgLealtadTarjeta(PocEntradaAplicMovLealtadTarjeta pocEntrada);
        IEnumerable<PocSalidaConsultaCuentas> ConsultaCuentasDeCliente(PocEntradaConsultaCuentas pocEntrada);
        PocSalidaMiscelaneosTarjeta AplicaMiscelaneosTarjeta(PocEntradaMiscelaneosTarjeta pocEntrada);
        PocSalidaAdelantoEfectivoTarjeta AdelantoEfectivoTarjeta(PocEntradaAdelantoEfectivoTarjeta pocEntrada);
        PocSalidaAdelantoEfectivoTarjeta ReversaAdelantoEfectivoTarjeta(PocEntradaAdelantoEfectivoTarjeta pocEntrada);
        PocSalidaInclusionSeguroTarjeta InclusionSeguroTarjeta(PocEntradaInclusionSeguroTarjeta pocEntrada);
        PocSalidaCargoAutomaticoTarjeta GeneraCargoAutomaticoTarjeta(PocEntradaCargoAutomaticoTarjeta pocEntrada);
    }
}

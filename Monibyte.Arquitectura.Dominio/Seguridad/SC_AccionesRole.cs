using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Seguridad
{
    public partial class SC_AccionesRole : EntidadBase
    {
        public int IdRole { get; set; }
        public int IdAccion { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Configuracion.Repositorios;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public class MovimientosTarjeta : AplicacionBase, IMovimientosTarjeta
    {
        private IAppGenerales _appGenerales;
        private IRepositorioMoneda _repMoneda;
        private IOperacionesATH _operacionesATH;
        private IRepositorioMnb<TC_MovimientoCorte> _repMovCorte;
        private IRepositorioMnb<TC_MovimientoCorteHist> _repMovCorteHist;
        private IRepositorioMnb<TC_DetalleMovimiento_TT> _repDetalleMovTmp;
        private IReporteadorDinamico _repReporteador;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioVwMovTarjeta _repVwMovCorte;
        private IRepositorioMnb<TC_MovimientoInterno> _repMovInterno;
        private IOperacionesATH _appOperacionesATH;
        private IRepositorioMnb<EF_ArchivoTarjeta> _repArchivoTarjeta;

        public MovimientosTarjeta(
            IAppGenerales appGenerales,
            IRepositorioMoneda repMoneda,
            IOperacionesATH operacionesATH,
            IRepositorioMnb<TC_MovimientoCorte> repMovCorte,
            IRepositorioMnb<TC_MovimientoCorteHist> repMovCorteHist,
            IRepositorioMnb<TC_DetalleMovimiento_TT> repDetalleMovTmp,
            IReporteadorDinamico repReporteador,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioVwMovTarjeta repVwMovCorte,
            IRepositorioMnb<TC_MovimientoInterno> repMovInterno,
            IOperacionesATH appOperacionesATH,
            IRepositorioMnb<EF_ArchivoTarjeta> repArchivoTarjeta)
        {
            _appGenerales = appGenerales;
            _repMoneda = repMoneda;
            _operacionesATH = operacionesATH;
            _repMovCorte = repMovCorte;
            _repMovCorteHist = repMovCorteHist;
            _repDetalleMovTmp = repDetalleMovTmp;
            _repReporteador = repReporteador;
            _repPersona = repPersona;
            _repVwMovCorte = repVwMovCorte;
            _repMovInterno = repMovInterno;
            _appOperacionesATH = appOperacionesATH;
            _repArchivoTarjeta = repArchivoTarjeta;
        }

        public IQueryable<PocMovimientosTarjeta> QueryMovimientosTarjeta(
            ParamMovimientosTc parametros,
            PocTarjetaCuentaUsuario[] tarjetas,
            Criteria<TC_vwMovimientosTarjeta> criteria = null)
        {
            var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
            var entidad = _appGenerales.ObtieneEntidad(idEntidad);
            this.ValidarConsultaMovimientos(parametros.IdCuenta,
                parametros.FecDesde, parametros.FecHasta);

            parametros.IdEmpresa = InfoSesion.Info.IdEmpresa;
            var criteriaTc = new Criteria<TC_vwTarjetaUsuario>();
            criteria = criteria ?? new Criteria<TC_vwMovimientosTarjeta>();
            criteria.And(m => m.IdEstado != (int)EnumEstadoMovimientoTC.Anulado);
            var queryMovs = _repVwMovCorte.ObtenerMovimientos(parametros)
                .Where(criteria.Satisfecho().Compile())
                .Join(tarjetas,
                    mov => new { mov.IdCuenta, mov.IdTarjeta },
                    tar => new { tar.IdCuenta, tar.IdTarjeta },
                    (mov, tar) => new { mov, tar });
            var movCorte = queryMovs.Select(s => new PocMovimientosTarjeta
            {
                EsHistorico = s.mov.EsHistorico == 1,
                IdMovimiento = s.mov.IdMovimiento,
                IdCuenta = s.tar.IdCuenta,
                IdTarjeta = s.tar.IdTarjeta,
                NumTarjeta = s.tar.NumTarjeta,
                NombreImpreso = s.tar.NombreImpreso,
                NumIdentificacion = s.tar.IdTarjetahabiente,
                IdMoneda = s.mov.IdMoneda,
                DescripcionMoneda = _fGenerales.Traducir(s.mov.DesMoneda,
                        "TS_Moneda", s.mov.IdMoneda, Context.Lang.Id, 0, Context.DefaultLang.Id),
                IdTipoMovimiento = s.mov.IdTipoMovimiento,
                CodMoneda = s.mov.CodMoneda,
                CodMonedaLocal = s.mov.IdMoneda == entidad.MonedaLocal.IdMoneda ? s.mov.CodMoneda : null,
                CodMonedaInter = s.mov.IdMoneda == entidad.MonedaInternacional.IdMoneda ? s.mov.CodMoneda : null,
                CodMonedaNormativa = s.mov.CodMonedaNormativa,
                SimboloMonedaLocal = entidad.MonedaLocal.Simbolo,
                SimboloMonedaInter = entidad.MonedaInternacional.Simbolo,
                FecTransaccion = s.mov.FecTransaccion,
                HoraTransaccion = s.mov.HoraTransaccion,
                FecMovimiento = s.mov.FecMovimiento,
                FecCorte = s.mov.FecCorte ?? SiguienteFechaCorte(s.mov.FecMovimiento.Value, s.tar.DiaCorte),
                Descripcion = s.mov.Descripcion,
                IdPais = s.mov.IdPais,
                DescripcionPais = s.mov.DescripcionPais,
                MonMovimiento = s.mov.MonMovimiento,
                MonMovimientoLocal = s.mov.MonMovimientoLocal,
                MonMovimientoInter = s.mov.MonMovimientoInter,
                IdDebCredito = s.mov.IdDebCredito,
                CodUsuarioEmpresa = s.tar.CodUsuarioEmpresa,
                MonCredLocal = s.mov.MonCredLocal,
                MonDebLocal = s.mov.MonDebLocal,
                MonCredInter = s.mov.MonCredInter,
                MonDebInter = s.mov.MonDebInter,
                NumReferencia = s.mov.NumReferencia,
                DynamicJson = s.mov.CamposDinamicos,
                DynamicJsonTarjeta = s.tar.DynamicJson,
                IdEditado = s.mov.IdEditado,
                DebitoCredito = s.mov.DebitoCredito,
                SignoMonMovimiento = (decimal)s.mov.SignoMonMovimiento,
                SignoMonMovimientoInter = s.mov.SignoMonMovimientoInter,
                SignoMonMovimientoLocal = s.mov.SignoMonMovimientoLocal,
                CategoriaMcc = s.mov.CodCategoriaMcc.HasValue ? _fGenerales.Traducir(s.mov.CategoriaMcc,
                        "MB_CategoriaTipoComercio", s.mov.CodCategoriaMcc.Value,
                        Context.Lang.Id, 0, Context.DefaultLang.Id) : null,
                CodCategoriaMcc = s.mov.CodCategoriaMcc,
                SoloConsulta = s.tar.SoloConsulta,
                GeoCamposDinamicos = s.mov.GeoCamposDinamicos,
                CodEditado = s.mov.CodEditado,
                MesMovimiento = s.mov.MesMovimiento.Value.ToString("00"),
                AnoMovimiento = s.mov.AnoMovimiento.Value.ToString("0000"),
                IdPersona = s.mov.IdPersona,
                IdLiquidado = s.mov.IdLiquidado,
                IdGestion = s.mov.IdGestion
            });
            return movCorte.AsQueryable();
        }

        public IEnumerable<PocMovimientosTarjeta> ObtenerMovimientosTarjeta(PocParametrosTarjeta parametros)
        {
            var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
            var entidad = _appGenerales.ObtieneEntidad(idEntidad);
            this.ValidarConsultaMovimientos(parametros.IdCuenta,
                parametros.FecDesde, parametros.FecHasta);
            var resultado = new List<PocMovimientosTarjeta>();
            //incluir los movimientos del dia
            if (parametros.FecHasta == DateTime.Today)
            {
                var entradaMq = new PocEntradaMovimientosTarjeta
                {
                    Mq = parametros.Mq,
                    IdEsCuentaMadre = parametros.IdEsCuentaMadre,
                    IdCuenta = parametros.IdCuenta,
                    IdTarjeta = parametros.IdTarjeta,
                    IdUsuario = parametros.IdUsuario,
                    NumTarjeta = parametros.NumTarjeta,
                    FechaDesde = DateTime.Today,
                    FechaHasta = DateTime.Today
                };
                var movimientosMq = _operacionesATH.MovimientosTarjeta(entradaMq);
                var movimientosAth = MapMovimientosAth(movimientosMq, entidad);
                resultado.AddRange(movimientosAth.GroupJoin(_repDetalleMovTmp.Table,
                        mov => new { mov.NumReferencia, mov.IdTarjeta },
                        det => new { NumReferencia = det.IdReferencia, det.IdTarjeta }, (mov, details) => new { mov, details })
                    .SelectMany(m => m.details.DefaultIfEmpty(), (x, detail) => new { x.mov, detail })
                    .Select(m =>
                    {
                        if (m.detail != null)
                        {
                            m.mov.DynamicJson = m.detail.CamposDinamicos;
                            m.mov.IdEditado = (int)EnumIdSiNo.Si;
                        }
                        return m.mov;
                    }));
            }
            var _params = parametros.ProyectarComo<ParamMovimientosTc>();
            var movCorte = QueryMovimientosTarjeta(_params, parametros.Tarjetas);
            resultado.AddRange(movCorte);
            return resultado;
        }

        public IEnumerable<PocMovimientosTarjeta> ObtenerMovTransitoTarjeta(
            PocParametrosTarjeta parametros, bool metodoSimple = false)
        {
            var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
            var entidad = _appGenerales.ObtieneEntidad(idEntidad);
            //Establecer fechas por defecto para la consulta de mov en transito
            parametros.FecDesde = DateTime.Today.AddMonths(-6);
            parametros.FecHasta = DateTime.Today;
            this.ValidarConsultaMovimientos(parametros.IdCuenta,
                parametros.FecDesde, parametros.FecHasta);
            var entradaMq = new PocEntradaMovimientosTarjeta
            {
                Mq = parametros.Mq,
                IdEsCuentaMadre = parametros.IdEsCuentaMadre,
                IdCuenta = parametros.IdCuenta,
                IdTarjeta = parametros.IdTarjeta,
                NumTarjeta = parametros.NumTarjeta,
                IdUsuario = parametros.IdUsuario,
                FechaDesde = parametros.FecDesde.Value,
                FechaHasta = parametros.FecHasta.Value
            };
            var movimientosMq = _operacionesATH.MovimientosTransitoTarjeta(entradaMq, metodoSimple);
            return MapMovimientosAth(movimientosMq, entidad);
        }

        public IEnumerable<PocMovimientosTarjeta> ObtieneResumenMovimientos(PocParametrosTarjeta parametros)
        {
            var resultado = ObtenerMovimientosPorComercio(parametros)
                .GroupBy(p => p.CategoriaMcc).ToList();
            return resultado.Select(p => new PocMovimientosTarjeta
            {
                CategoriaMcc = p.Min(x => x.CategoriaMcc),
                MonMovimientoLocal = p.Sum(x => x.MonMovimientoLocal),
                MonMovimientoInter = p.Sum(x => x.MonMovimientoInter),
                SimboloMonedaLocal = p.Min(x => x.SimboloMonedaLocal),
                SimboloMonedaInter = p.Min(x => x.SimboloMonedaInter)
            });
        }

        public IEnumerable<PocMovimientosTarjeta> ObtieneMovTipoComercio(
            PocParametrosTarjeta parametros, string categoria)
        {
            var resultado = ObtenerMovimientosPorComercio(parametros)
                .Where(x => categoria == null ||
                    x.CategoriaMcc == categoria)
                .ToList();
            return resultado;
        }

        public IEnumerable<PocMovimientosGeo> ObtieneMovCombustible(PocParametrosTarjeta parametros)
        {
            var _params = parametros.ProyectarComo<ParamMovimientosTc>();
            var movCorte = QueryMovimientosTarjeta(_params, parametros.Tarjetas)
                .Where(m => m.GeoCamposDinamicos != null &&
                    m.CodCategoriaMcc == (int)EnumCategoriaMcc.Gas)
                .ToList()
                .Select(m => new
                {
                    mov = m,
                    datosEntrada = m.GeoCamposDinamicos.FromJson<PocGeoLocalizacion>()
                })
                .Select(m => new PocMovimientosGeo
                {
                    UnidadMedida = m.datosEntrada.UnidadMedida,
                    CantidadCombustible = m.datosEntrada.CantidadCombustible,
                    Conductor = m.datosEntrada.Conductor,
                    DetalleMovimiento = m.mov.Descripcion,
                    Distancia = m.datosEntrada.Distancia,
                    FecMovimiento = m.mov.FecMovimiento.Value,
                    FecTransaccion = m.mov.FecTransaccion,
                    IdUnidadMedida = (int)EnumUnidadMedida.Litros,
                    Localizacion = m.datosEntrada.Localizacion,
                    Moneda = _fGenerales.Traducir(m.mov.DescripcionMoneda, "TS_Moneda",
                    m.mov.IdMoneda, Context.Lang.Id, 0, Context.DefaultLang.Id),
                    MonMovimiento = m.mov.MonMovimiento,
                    NumTarjeta = m.mov.NumTarjeta,
                    NumReferencia = m.mov.NumReferencia,
                    Placa = m.datosEntrada.Placa
                });
            return movCorte;
        }

        private IQueryable<PocMovimientosTarjeta> ObtenerMovimientosPorComercio(PocParametrosTarjeta parametros)
        {
            var criteria = new Criteria<TC_vwMovimientosTarjeta>();
            criteria.And(m => m.IdMcc.HasValue);
            var _params = parametros.ProyectarComo<ParamMovimientosTc>();
            return QueryMovimientosTarjeta(_params, parametros.Tarjetas, criteria);
        }

        public void GuardarDetalleMovimiento(List<PocMovimientosTarjeta> datosEntrada,
            string dynamicJson, bool noEditStr, List<PocArchivoMovimientoTc> archivoMovimientoTc)
        {
            using (var ts = new TransactionScope())
            {
                foreach (var data in datosEntrada)
                {
                    if (data.EsHistorico)
                    {
                        _repMovCorteHist.UpdateOn(m => m.IdMovimiento == data.IdMovimiento,
                            m => new TC_MovimientoCorteHist
                            {
                                IdEditado = noEditStr ? (int)EnumEstadoEdicion.Pendiente : (int)EnumEstadoEdicion.Editado,
                                CamposDinamicos = dynamicJson
                            });
                    }
                    else if (data.IdMovimiento > 0)
                    {
                        _repMovCorte.UpdateOn(m => m.IdMovimiento == data.IdMovimiento,
                            m => new TC_MovimientoCorte
                            {
                                IdEditado = noEditStr ? (int)EnumEstadoEdicion.Pendiente : (int)EnumEstadoEdicion.Editado,
                                CamposDinamicos = dynamicJson
                            });
                    }
                    else
                    {
                        var item = _repDetalleMovTmp.SelectById(data.NumReferencia, data.IdTarjeta);
                        if (item != null)
                        {
                            _repDetalleMovTmp.UpdateOn(x =>
                                x.IdTarjeta == data.IdTarjeta &&
                                x.IdReferencia == data.NumReferencia,
                                z => new TC_DetalleMovimiento_TT
                                {
                                    CamposDinamicos = dynamicJson
                                });
                        }
                        else
                        {
                            _repDetalleMovTmp.Insert(new TC_DetalleMovimiento_TT
                            {
                                IdTarjeta = data.IdTarjeta,
                                IdReferencia = data.NumReferencia,
                                CamposDinamicos = dynamicJson,
                                FecMovimiento = data.FecTransaccion
                            });
                        }
                    }
                }
                _repMovCorte.UnidadTbjo.Save();

                if (archivoMovimientoTc != null)
                {
                    foreach (PocArchivoMovimientoTc file in archivoMovimientoTc)
                    {                        
                        _repArchivoTarjeta.Insert(file.ProyectarComo<EF_ArchivoTarjeta>());
                        _repArchivoTarjeta.UnidadTbjo.Save();
                    }
                }

                ts.Complete();
            }
        }


        #region Metodos privados
        private void ValidarConsultaMovimientos(int? idCuenta, DateTime? fecDesde, DateTime? fecHasta)
        {
            var validacion = new ValidationResponse();
            var FecInicial = DateTime.Parse("01/01/0001");
            if (idCuenta <= 0) { validacion.AddError("Err_0001"); }
            if (fecDesde == FecInicial) { validacion.AddError("Err_0014"); }
            if (fecHasta == FecInicial) { validacion.AddError("Err_0015"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes en la consulta de movimientos", validation: validacion);
            }
        }
        private decimal ValidaDebitoCredito(EnumDebitoCredito idTipoDestino, decimal monto, int idTipoFuente)
        {
            if ((int)idTipoDestino == idTipoFuente)
            {
                return monto;
            }
            return 0;
        }

        private DateTime SiguienteFechaCorte(DateTime fecMovimiento, int diaCorte)
        {
            var diaMovimiento = fecMovimiento.Day;
            if (diaMovimiento > diaCorte)
            {
                fecMovimiento = fecMovimiento.AddMonths(1);
            }
            var year = fecMovimiento.Year;
            var month = fecMovimiento.Month;
            var day = DateTime.DaysInMonth(year, month);
            if (diaCorte > day) { diaCorte = day; }
            return new DateTime(year, month, diaCorte);
        }

        private List<PocMovimientosTarjeta> MapMovimientosAth(IEnumerable<PocSalidaMovimientosTarjeta> movs, PocEntidad entidad)
        {
            var resultado = movs.Select(mov =>
            {
                var item = mov.ProyectarComo<PocMovimientosTarjeta>();
                item.MonMovimiento = mov.Monto;
                item.IdDebCredito = mov.IdDebCredito;
                item.CodMoneda = entidad.MonedaLocal.IdMoneda == mov.IdMoneda ? entidad.MonedaLocal.CodInternacional :
                    entidad.MonedaInternacional.CodInternacional;
                item.CodMonedaLocal = entidad.MonedaLocal.IdMoneda == mov.IdMoneda ? entidad.MonedaLocal.CodInternacional : null;
                item.CodMonedaInter = entidad.MonedaInternacional.IdMoneda == mov.IdMoneda ? entidad.MonedaLocal.CodInternacional : null;
                item.MonCredLocal = entidad.MonedaInternacional.IdMoneda == mov.IdMoneda ? 0 :
                    ValidaDebitoCredito(EnumDebitoCredito.Credito, mov.Monto, mov.IdDebCredito);
                item.MonDebLocal = entidad.MonedaInternacional.IdMoneda == mov.IdMoneda ? 0 :
                    ValidaDebitoCredito(EnumDebitoCredito.Debito, mov.Monto, mov.IdDebCredito);
                item.MonCredInter = entidad.MonedaInternacional.IdMoneda == mov.IdMoneda ?
                    ValidaDebitoCredito(EnumDebitoCredito.Credito, mov.Monto, mov.IdDebCredito) : 0;
                item.MonDebInter = entidad.MonedaInternacional.IdMoneda == mov.IdMoneda ?
                    ValidaDebitoCredito(EnumDebitoCredito.Debito, mov.Monto, mov.IdDebCredito) : 0;
                item.MonMovimiento = mov.Monto;
                item.MonMovimientoLocal = entidad.MonedaLocal.IdMoneda == mov.IdMoneda ?
                     mov.Monto as decimal? : null;
                item.MonMovimientoInter = entidad.MonedaInternacional.IdMoneda == mov.IdMoneda ?
                     mov.Monto as decimal? : null;
                item.SimboloMonedaLocal = entidad.MonedaLocal.Simbolo;
                item.SimboloMonedaInter = entidad.MonedaInternacional.Simbolo;
                item.NombreImpreso = mov.NombreImpreso;
                item.NumTarjeta = mov.NumTarjeta;
                return item;
            });
            return resultado.ToList();
        }
        #endregion

        #region Reporte

        public byte[] ObtenerReporteMovimientos(int idReporte,
            PocParametrosTarjeta datosEntrada)
        {
            var empresa = _repPersona.Table.FirstOrDefault(x =>
                        x.IdPersona == InfoSesion.Info.IdEmpresa);
            var config = new ReportConfig
            {
                ReportId = "Generado por: " + InfoSesion.Info.CodUsuario + " <br /> <br />" +
                "Fecha: " + DateTime.Now,
                ReportTitle = "Movimientos de tarjeta",
                ReportFormat = "PDF",
                Landscape = false,
                SubTitle = empresa.NombreCompleto
            };
            var _no = (int)EnumIdSiNo.No;
            var p1 = new ReportParam { Name = "idReporte", Value = idReporte.ToString() };
            var campos = _repReporteador.ObtenerCamposReporte(idReporte);
            //detalle del reporte de movimientos
            var movimientosDTS = new ReportDataSet();
            movimientosDTS.Columns = campos.Where(x =>
                x.IdEsEncabezado == _no)
                .ProyectarComo<ReportColumn>();
            movimientosDTS.IsDetail = true;
            movimientosDTS.Name = "DTS01";
            movimientosDTS.StoreProcedure = true;
            movimientosDTS.QueryCommand = "General.P_ObtieneQueryReporte";
            var stringJson = new
            {
                idEsEncabezado = _no,
                idCuenta = datosEntrada.IdCuenta,
                idTarjeta = datosEntrada.IdTarjeta,
                fecDesde = datosEntrada.FecDesde,
                fecHasta = datosEntrada.FecHasta
            }.ToJson();
            var p2 = new ReportParam { Name = "stringJson", Value = stringJson };
            var encabezadoParams = new List<ReportParam> { p1, p2 };
            movimientosDTS.Parameters = encabezadoParams;

            var dataSets = new List<ReportDataSet>();
            dataSets.Add(movimientosDTS);

            var report = _repReporteador.CrearReporte(config, dataSets);
            return report.Output;
        }
        #endregion

        public void NuevoMovimiento(PocMovimientoTarjeta registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<TC_MovimientoInterno>();

                entrada.Descripcion = registro.Descripcion;
                entrada.HoraTransaccion = DateTime.Now;
                entrada.FecTransaccion = registro.FecTransaccion;
                entrada.IdMovimiento = _fGenerales.ObtieneSgteSecuencia("SEQ_IdMovInterno");
                entrada.IdTarjeta = registro.IdTarjeta;
                entrada.IdUsuario = registro.IdUsuario;
                entrada.MonTransaccion = registro.MonTransaccion;
                entrada.NumTarjeta = registro.NumTarjeta;
                entrada.IdTipoMovimiento = registro.IdTipoMovimiento;

                _repMovInterno.Insert(entrada);
                _repMovInterno.UnidadTbjo.Save();

                //agrega movimiento mq

                var entradaServicio = new PocEntradaMiscelaneosTarjeta();
                entradaServicio.MontoTransaccion = registro.MonTransaccion;
                entradaServicio.CodMovimiento = registro.IdTipoMovimiento;
                entradaServicio.NumTarjeta = registro.NumTarjeta;
                entradaServicio.NumReferencia = _fGenerales.ObtieneSgteSecuencia("SEQ_IdRefProcesador").ToString();
                entradaServicio.MonedaTransaccion = _repMoneda.SelectById(registro.IdMoneda).CodAlpha2;
                entradaServicio.FechaConsumo = registro.FecTransaccion;


                var resultado = _appOperacionesATH.AplicaMiscelaneosTarjeta(entradaServicio);
                ts.Complete();
            }
        }
    }
}

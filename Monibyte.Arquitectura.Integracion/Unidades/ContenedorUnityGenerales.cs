﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Generales;
    using Monibyte.Arquitectura.Aplicacion.Generales.Mantenimientos;
    using Monibyte.Arquitectura.Datos.Generales;
    using Monibyte.Arquitectura.Datos.Generales.Repositorios;
    using Monibyte.Arquitectura.Dominio.Generales;
    using Monibyte.Arquitectura.Dominio.Generales.Repositorios;

    public class ContenedorUnityGenerales
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            _unityContainer.RegisterType<IAppUtilitarios, AppUtilitarios>();
            _unityContainer.RegisterType<IParametroSistema, ParametroSistema>();
            _unityContainer.RegisterType<IAppGenerales, AppGenerales>();
            _unityContainer.RegisterType<IAppOpUsuario, AppOpUsuario>();
            _unityContainer.RegisterType<IParametroEmpresa, ParametroEmpresa>();
            _unityContainer.RegisterType<IAppOpSucursal, AppOpSucursal>();
            _unityContainer.RegisterType<IAppOpIdioma, AppOpIdioma>();
            #endregion

            #region Repositorios
            _unityContainer.RegisterType<IFuncionesGenerales, FuncionesGenerales>();
            _unityContainer.RegisterType<IRepositorioReportServer, RepositorioReportServer>(
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(new object[] { "report-mb-connection", true }));
            _unityContainer.RegisterType<IReporteadorDinamico, ReporteadorDinamico>(
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(new object[] { "report-mb-connection", true }));

            _unityContainer.RegisterType<IRepositorioPerfilUsuario, RepositorioPerfilUsuario>();
            _unityContainer.RegisterType<IRepositorioUsuario, RepositorioUsuario>();
            _unityContainer.RegisterType<IRepositorioCatalogo, RepositorioCatalogo>();
            _unityContainer.RegisterType<IRepositorioCampoContexto, RepositorioCampoContexto>();
            #endregion
        }
    }
}

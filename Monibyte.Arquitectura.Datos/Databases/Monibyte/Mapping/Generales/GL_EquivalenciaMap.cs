using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_EquivalenciaMap : EntityTypeConfiguration<GL_Equivalencia>
    {
        public GL_EquivalenciaMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Lista)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Origen)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ValOrigen)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Destino)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ValDestino)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GL_Equivalencia", "General");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Lista).HasColumnName("Lista");
            this.Property(t => t.Origen).HasColumnName("Origen");
            this.Property(t => t.ValOrigen).HasColumnName("ValOrigen");
            this.Property(t => t.Destino).HasColumnName("Destino");
            this.Property(t => t.ValDestino).HasColumnName("ValDestino");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ParametroEmpresaContextoMap : EntityTypeConfiguration<GL_ParametroEmpresaContexto>
    {
        public GL_ParametroEmpresaContextoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdParametro, t.IdContexto });

            // Properties
            this.Property(t => t.IdParametro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdContexto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("GL_ParametroEmpresaContexto", "General");
            this.Property(t => t.IdParametro).HasColumnName("IdParametro");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.IdEsRequerido).HasColumnName("IdEsRequerido");
            this.Property(t => t.ValorDefecto).HasColumnName("ValorDefecto");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdBloqueado).HasColumnName("IdBloqueado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.LongitudMax).HasColumnName("LongitudMax");
            this.Property(t => t.IdOrden).HasColumnName("IdOrden");
        }
    }
}

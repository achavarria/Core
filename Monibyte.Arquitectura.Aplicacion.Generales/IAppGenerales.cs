﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Tesoreria;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Generales
{
    public interface IAppGenerales : IAplicacionBase
    {
        PocEntidad ObtieneEntidad(int idEntidad);
        PocEntidad ObtenerEntidadFinanciera();
        PocConfiguracionFinanciera ObtenerInformacionFinanciera();
        List<PocEntidad> ListaEntidades(int? idTipoEntidad = null);
        List<PocUnidadNegocio> ListaUnidadesPorEntidad(int idEntidad);
        PocPais ObtienePaisProcesador();
        List<PocProvincia> ListaProvincias();
        List<PocCanton> ListaCantones(int? idProvincia);
        List<PocDistrito> ListaDistritos(int? idCanton);
        string ObtieneParametroCompania(string idParametro, int idConpania);
        List<PocParametros> ListasPredefinidas();
        void EliminarReporte(int idReporte);
        void GuardarParametroCompania(string idParametro, int idCompania, string valorParametro);
        IEnumerable<PocCampoContexto> ObtenerCamposContexto(int idContexto);
    }
}

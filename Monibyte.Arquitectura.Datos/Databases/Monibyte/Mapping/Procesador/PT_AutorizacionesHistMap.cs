using Monibyte.Arquitectura.Dominio.Procesador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Procesador
{
    public class PT_AutorizacionesHistMap : EntityTypeConfiguration<PT_AutorizacionesHist>
    {
        public PT_AutorizacionesHistMap()
        {
            // Primary Key
            this.HasKey(t => t.IdAutorizacion);

            // Properties
            this.Property(t => t.NumeroCuenta)
                .HasMaxLength(20);

            this.Property(t => t.NumeroTarjeta)
                .HasMaxLength(30);

            this.Property(t => t.CodFranquicia)
                .HasMaxLength(5);

            this.Property(t => t.NombreComercio)
                .HasMaxLength(30);

            this.Property(t => t.NombreCliente)
                .HasMaxLength(60);

            this.Property(t => t.HoraTransaccion)
                .HasMaxLength(20);

            this.Property(t => t.Moneda)
                .HasMaxLength(5);

            this.Property(t => t.FormaEntrada)
                .HasMaxLength(5);

            this.Property(t => t.Respuesta)
                .HasMaxLength(60);

            this.Property(t => t.Mcc)
                .HasMaxLength(5);

            this.Property(t => t.CodigoRespuesta)
                .HasMaxLength(5);

            this.Property(t => t.Autoriza)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("PT_AutorizacionesHist", "ProcesadorTC");
            this.Property(t => t.IdAutorizacion).HasColumnName("IdAutorizacion");
            this.Property(t => t.NumeroCuenta).HasColumnName("NumeroCuenta");
            this.Property(t => t.NumeroTarjeta).HasColumnName("NumeroTarjeta");
            this.Property(t => t.CodFranquicia).HasColumnName("CodFranquicia");
            this.Property(t => t.NombreComercio).HasColumnName("NombreComercio");
            this.Property(t => t.NombreCliente).HasColumnName("NombreCliente");
            this.Property(t => t.FechaCompra).HasColumnName("FechaCompra");
            this.Property(t => t.HoraTransaccion).HasColumnName("HoraTransaccion");
            this.Property(t => t.MontoAutorizacion).HasColumnName("MontoAutorizacion");
            this.Property(t => t.Moneda).HasColumnName("Moneda");
            this.Property(t => t.FormaEntrada).HasColumnName("FormaEntrada");
            this.Property(t => t.Respuesta).HasColumnName("Respuesta");
            this.Property(t => t.Mcc).HasColumnName("Mcc");
            this.Property(t => t.CodigoRespuesta).HasColumnName("CodigoRespuesta");
            this.Property(t => t.FechaEmisionTarjet).HasColumnName("FechaEmisionTarjet");
            this.Property(t => t.Autoriza).HasColumnName("Autoriza");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ReportesMap : EntityTypeConfiguration<GL_Reportes>
    {
        public GL_ReportesMap()
        {
            // Primary Key
            this.HasKey(t => t.IdReporte);

            // Properties
            this.Property(t => t.IdReporte)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GL_Reportes", "General");
            this.Property(t => t.IdReporte).HasColumnName("IdReporte");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.IdIncluyeEncabTxt).HasColumnName("IdIncluyeEncabTxt");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdIncluyeContrapartida).HasColumnName("IdIncluyeContrapartida");
            this.Property(t => t.TipoMovimiento).HasColumnName("TipoMovimiento");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdTipoReporte).HasColumnName("IdTipoReporte");
        }
    }
}

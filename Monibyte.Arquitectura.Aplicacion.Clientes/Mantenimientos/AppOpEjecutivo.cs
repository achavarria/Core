﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Clientes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public class AppOpEjecutivo : AplicacionBase, IAppOpEjecutivo
    {
        IRepositorioMnb<GL_Catalogo> _repCatalogo;
        IRepositorioMnb<CL_Persona> _repPersona;
        IRepositorioMnb<CL_Ejecutivo> _repEjecutivo;

        public AppOpEjecutivo(
            IRepositorioMnb<GL_Catalogo> repCatalogo,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<CL_Ejecutivo> repEjecutivo)
        {
            _repCatalogo = repCatalogo;
            _repPersona = repPersona;
            _repEjecutivo = repEjecutivo;
        }

        public void Eliminar(PocEjecutivo registro)
        {
            try
            {
                using (var ts = new TransactionScope())
                {
                    if (registro != null && registro.IdEjecutivo > 0)
                    {
                        _repEjecutivo.DeleteOn(x => x.IdEjecutivo == registro.IdEjecutivo);
                        _repEjecutivo.UnidadTbjo.Save();
                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new CoreException("Error al eliminar ejecutivo", "Err_0111");
            }
        }

        public void Insertar(PocEjecutivo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CL_Ejecutivo>();
                    entrada.IdEjecutivo = _fGenerales.ObtieneSgteSecuencia("SEQ_IdEjecutivo");
                    entrada.IdEstado = registro.IdEstado.Value;
                    entrada.IdPersona = registro.IdPersona;

                    _repEjecutivo.Insert(entrada);
                    _repEjecutivo.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocEjecutivo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocEjecutivo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CL_Ejecutivo>();
                    //Valor a modificar
                    _repEjecutivo.Update(entrada, m => m.IdEstado);
                    _repEjecutivo.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocEjecutivo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public PocEjecutivo Obtener(PocEjecutivo filtro)
        {
            var salida = _repEjecutivo.Table.FirstOrDefault(m => m.IdEjecutivo == filtro.IdEjecutivo);
            return salida.ProyectarComo<PocEjecutivo>();
        }

        public DataResult<PocEjecutivo> Listar(PocEjecutivo filtro, DataRequest request)
        {
            var resultado = ObtenerEjecutivos(filtro != null ? filtro.IdEstado : null);
            return resultado.OrderBy(x => x.IdEjecutivo).ToDataResult(request);
        }

        public List<PocEjecutivo> ObtenerEjecutivos(int? idEstado)
        {
            var criteria = new Criteria<CL_Ejecutivo>();
            if (idEstado.HasValue)
            {
                criteria.And(X => X.IdEstado == idEstado);
            }

            var resultado = (from eje in _repEjecutivo.SelectBy(criteria)
                             join per in _repPersona.Table
                                 on eje.IdPersona equals per.IdPersona
                             join est in _repCatalogo.Table
                                on eje.IdEstado equals est.IdCatalogo
                             join emp in
                                 (from emp in _repPersona.Table
                                  join tip in _repCatalogo.Table
                                      on emp.IdTipoPersona equals tip.IdCatalogo
                                  select new PocInfoPersona
                                  {
                                      TipoIdentificacion = tip.Descripcion,
                                      IdPersona = emp.IdPersona,
                                      NombreCompleto = emp.NombreCompleto,
                                      IdEjecutivo = emp.IdEjecutivo
                                  })
                                on eje.IdEjecutivo equals emp.IdEjecutivo
                                into emps
                             orderby eje.IdEjecutivo
                             select new { eje, per, est, emps }).ToList()
                             .Select(x => new PocEjecutivo
                             {
                                 IdPersona = x.per.IdPersona,
                                 IdEjecutivo = x.eje.IdEjecutivo,
                                 Nombre = x.per.NombreCompleto,
                                 IdEstado = x.eje.IdEstado,
                                 NumIdentificacion = x.per.NumIdentificacion,
                                 DesEstado = x.est.Descripcion,
                                 Empresas = x.emps
                             });
            return resultado.ToList<PocEjecutivo>();
        }

        private ValidationResponse Validar(PocEjecutivo registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            return validacion;
        }
    }
}

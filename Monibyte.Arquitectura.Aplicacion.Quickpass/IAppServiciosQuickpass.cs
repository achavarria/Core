﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Quickpass;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public interface IAppServiciosQuickpass : IAplicacionBase
    {
        int ObtenerIdTarjeta(string NumTarjeta);
        void ImportarListaQuickpass(PocObtenerQuickpass pocObtenerQuickpass);
        bool ImportarMovimientosQuickpass(PocConsultaMovQp PocConsultaMovQp);
        void GenerarCargosMovimientosQuickpass();
        void GenerarCargosCorteQuickpass(int? numDia, int? diaPlazo);
        void EnviarListasQP(PocListaQP pocListaQP);
        void ResultadoListasDeTags(PocListaTagsTabla pocListaTags);
        void EjecutarProcesosQuickpass(PocConsultaMovQp PocConsultaMovQp);
        void AplicarCargosMovimientosQP(List<PocQuickpassMovimientos> listaMovs, DateTime? fecDesde = null, DateTime? fecHasta = null);
        PocTarjeta ObtenerInfoTarjeta(int IdTarjeta);
    }
}

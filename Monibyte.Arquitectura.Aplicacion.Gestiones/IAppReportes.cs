﻿using Monibyte.Arquitectura.Dominio;

namespace Monibyte.Arquitectura.Aplicacion.Gestiones
{
    public interface IAppReportes : IAplicacionBase
    {
        byte[] ObtenerGestionLiquidacion(string json, int idGestion,
             int idEmpresa, int idMonedaLocal, int idMonedaInter, int idReporteBase);
        byte[] ObtenerGestionKilometraje(string Json);
        byte[] ObtenerReporteLiquidacion(int idGestion, int? idReporte);
    }
}
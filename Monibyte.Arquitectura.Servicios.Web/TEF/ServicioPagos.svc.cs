﻿using Monibyte.Arquitectura.Aplicacion.TEF.Mantenimientos;
using Monibyte.Arquitectura.Aplicacion.TEF.SINPE;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.TEF;
using Monibyte.Arquitectura.Servicios.Nucleo;
using Monibyte.Arquitectura.Servicios.Web.TEF;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.TEF
{
    public class ServicioPagos : ServicioUnity, IServicioPagos
    {
        //Para su futura integración revisar cada método
        #region Mantenimiento Cedulas Autorizadas
        public Transporte IncluirCedulaAutorizada(Transporte transporte)
        {
            var app = GestorUnity.CargarUnidad<IAppOpCedulaRegistrada>();
            var entrada = transporte.GetDatos<PocCedulaRegistrada>();
            app.Insertar(entrada);
            return transporte;
        }

        public Transporte EliminaCedulaAutorizada(Transporte transporte)
        {
            var app = GestorUnity.CargarUnidad<IAppOpCedulaRegistrada>();
            var entrada = transporte.GetDatos<PocCedulaRegistrada>();
            app.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaCedulaAutorizada(Transporte transporte)
        {
            var app = GestorUnity.CargarUnidad<IAppOpCedulaRegistrada>();
            var entrada = transporte.GetDatos<PocCedulaRegistrada>();
            app.Modificar(entrada);
            return transporte;
        }

        public Transporte ListarCedulaAutorizada(Transporte transporte)
        {
            var app = GestorUnity.CargarUnidad<IAppOpCedulaRegistrada>();
            var entrada = transporte.GetDatos<PocCedulaRegistrada>();
            var resultado = app.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }
        #endregion

        #region PagosPendientes
        public DataResult<PocPagoPendiente> ListarPagosPendientes
            (PocPagoPendiente filtro, DataRequest request)
        {
            var app = GestorUnity.CargarUnidad<IOperacionesSINPE>();
            var resultado = app.ListarPagosPendientes(filtro, request);
            return resultado;
        }
        public List<PocPagoPendiente> ListarPagos(int IdTrasaccion)
        {
            var app = GestorUnity.CargarUnidad<IOperacionesSINPE>();
            return app.ListarPagos(IdTrasaccion);
        }
        public void EliminarPagosPendientes(PocPagoPendiente pagoSinpe)
        {
            var app = GestorUnity.CargarUnidad<IOperacionesSINPE>();
            app.EliminarPagosPendientes(pagoSinpe);
        }
        public void AplicarPagoPendiente(PocPagoPendiente pagoSinpe)
        {
            var app = GestorUnity.CargarUnidad<IOperacionesSINPE>();
            app.AplicarPagoPendiente(pagoSinpe);
        }
        #endregion

        #region Pagos Distribuidos
        public PocEncPagoDirigido ListarPagosAplicados(int IdCuenta)
        {
            var app = GestorUnity.CargarUnidad<IOperacionesSINPE>();
            return app.ListarPagosAplicados(IdCuenta);
        }
        public List<PocDirigirPago> AplicarPagoDistribuido(List<PocDirigirPago> entrada)
        {
            var app = GestorUnity.CargarUnidad<IOperacionesSINPE>();
            return app.AplicarPagoDistribuido(entrada);
        }
        #endregion
    }
}

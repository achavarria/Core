﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Tarjetas;
    using Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH;
    using Monibyte.Arquitectura.Datos.Tarjetas.MQATH;
    using Monibyte.Arquitectura.Datos.Tarjetas.Repositorios;
    using Monibyte.Arquitectura.Datos.Tarjetas.WSATH;
    using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
    using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios.MQATH;
    using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios.WSATH;

    public class ContenedorUnityTarjetas
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {          

            #region Servicios
            _unityContainer.RegisterType<IOperacionesATH, OperacionesATH>();
            _unityContainer.RegisterType<IMovimientosTarjeta, MovimientosTarjeta>();
            _unityContainer.RegisterType<IConsultasTarjeta, ConsultasTarjeta>();
            _unityContainer.RegisterType<IOperacionesTarjeta, OperacionesTarjeta>();
            _unityContainer.RegisterType<IAppReporteEstCuenta, AppReporteEstCuenta>();
            _unityContainer.RegisterType<IGeoLocalizacion, GeoLocalizacion>();
            _unityContainer.RegisterType<IServiciosSatGeo, ServiciosSatGeo>();
            _unityContainer.RegisterType<IPagosTarjeta, PagosTarjeta>();
            #endregion

            #region Repositorios
            _unityContainer.RegisterType<IRepositorioATH, RepositorioATH>(new InjectionConstructor(new object[] { "ATH", "", true }));
            _unityContainer.RegisterType<IRepositorioCuenta, RepositorioCuenta>();
            _unityContainer.RegisterType<IRepositorioTarjeta, RepositorioTarjeta>();
            _unityContainer.RegisterType<IRepositorioVwTarjetaUsuario, RepositorioVwTarjetaUsuario>();
            _unityContainer.RegisterType<IRepositorioVwMovTarjeta, RepositorioVwMovTarjeta>();
            _unityContainer.RegisterType<IServiciosProcesador, ServiciosProcesador>();
            _unityContainer.RegisterType<IAppReportesConsumo, AppReportesConsumo>();
            #endregion
        }
    }
}
﻿using System;
namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocEntradaActualizaTarjeta
    {
        /// <summary>
        /// Propiedad interna no es pertenece a las tramas de MQ. 
        /// </summary>
        public int IdCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string EstadoDeseado { get; set; }
        public string MotivoCancelacion { get; set; }
        public string Usuario { get; set; }
    }

    public class PocSalidaActualizaTarjeta
    {
        public string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public string EstadoDeseado { get; set; }
        public string MotivoCancelacion { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }

    public class PocEntradaActualizaLimiteTarjeta
    {
        /// <summary>
        /// Propiedad interna no es pertenece a las tramas de MQ. 
        /// </summary>
        public int IdCuenta { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string LocalidadTramite { get; set; }
        public string LocalidadRetiro { get; set; }
        public decimal? LimiteCredito { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public int IdTipoLimite { get; set; }
    }

    public class PocSalidaActualizaLimiteTarjeta
    {
        public string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public decimal Monto { get; set; }
        public string SuperFranquicia { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }

    public class PocEntradaActivaTarjeta
    {
        /// <summary>
        /// Propiedad interna no es pertenece a las tramas de MQ. 
        /// </summary>
        public int IdCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string LocalidadCustodia { get; set; }
        public string LocalidadDestino { get; set; }
    }

    public class PocSalidaActivaTarjeta
    {
        public string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string LocalidadCustodia { get; set; }
        public string LocalidadDestino { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }

    public class PocEntradaReposicionPinTarjeta
    {
        /// <summary>
        /// Propiedad interna no es pertenece a las tramas de MQ. 
        /// </summary>
        public int IdCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string LocalidadTramite { get; set; }
        public string LocalidadRetiro { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
    }

    public class PocSalidaReposicionPinTarjeta
    {
        private string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }

    public class PocEntradaInclusionSeguroTarjeta
    {
        /// <summary>
        /// Propiedad interna no es pertenece a las tramas de MQ. 
        /// </summary>
        public int IdCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string TipoPoliza { get; set; }
        public string NumPoliza { get; set; }
        public string Comentario { get; set; }
    }

    public class PocSalidaInclusionSeguroTarjeta
    {
        public string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string TipoPoliza { get; set; }
        public string NumPoliza { get; set; }
        public string Comentario { get; set; }
        public string CodigoRespuesta { get; set; }
    }
}

﻿using System;

namespace Monibyte.Arquitectura.Poco.Contabilidad
{
    public class PocParametro
    {
        public int IdParametro { get; set; }
        public int IdTipoParametro { get; set; }
        public string Descripcion { get; set; }
        public string TipoParametro { get; set; }
    }

    public class PocConfiguracionAsiento
    {
        public int IdConfAsiento { get; set; }
        public int IdTipoMovimiento { get; set; }
        public int IdOrigenMovimiento { get; set; }
        public int IdMoneda { get; set; }
        public int? Orden { get; set; }
        public int IdCustomer { get; set; }
        public int IdAccount { get; set; }
        public int IdItem { get; set; }
        public string TipoMovimiento { get; set; }
        public string OrigenMovimiento { get; set; }
        public string Customer { get; set; }
        public string Account { get; set; }
        public string Item { get; set; }
        public string Moneda { get; set; }
        public string DescMemo { get; set; }
    }

    public class PocMovimientoCorte
    {
        //public int IdMovimiento   { get; set; }
        public int IdTipoMovimiento { get; set; }
        public int IdOrigenMovimiento { get; set; }
        public int IdMoneda { get; set; }
        public DateTime? FecMovimiento { get; set; }
        public decimal MonMovimiento { get; set; }
        //public int IdCuenta { get; set; }
        //public int IdTarjeta { get; set; }
        //public DateTime FecTransaccion { get; set; }  
        //public DateTime HoraTransaccion { get; set; }
        //public string NumAutorizacion { get; set; } 
        //public decimal? TasaComision { get; set; }    
        //public string Comercio { get; set; }
        //public int? IdMcc { get; set; }    
        //public string DetalleMovimiento { get; set; }   
        //public int IdDebCredito { get; set; }     
        //public int IdPais { get; set; }
        //public int? Documento1 { get; set; }    
        //public string Documento2 { get; set; }    
        //public string NumReferencia { get; set; }
        //public decimal? MonTipoCambio { get; set; }   
        //public int IdEstado { get; set; }     
    }

    public class PocFactura
    {
        public int IdTipoMovimiento { get; set; }
        public int IdOrigenMovimiento { get; set; }
        public int IdMoneda { get; set; }
        public int? Orden { get; set; }
        public int IdCustomer { get; set; }
        public int IdAccount { get; set; }
        public int IdItem { get; set; }
        public string DescMemo { get; set; }
        public DateTime? FecMovimiento { get; set; }
        public decimal MonMovimiento { get; set; }
    }

    public class PocGenerarArchivo
    {
        public DateTime FecMovimientoDesde { get; set; }
        public DateTime FecMovimientoHasta { get; set; }
    }
}

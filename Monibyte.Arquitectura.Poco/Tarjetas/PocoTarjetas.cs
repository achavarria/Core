﻿
using System;
using System.Collections.Generic;
namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocMillas
    {
        public decimal SaldoInicialMillas { get; set; }
        public decimal DebitoMillas { get; set; }
        public decimal CreditoMillas { get; set; }
        public decimal TotalMillas { get; set; }
    }

    public class PocCuentasCorte
    {
        public int IdCuenta { get; set; }
        public int IdEsCuentaMadre { get; set; }
        public int? IdCuentaMadre { get; set; }
        public int IdTipoTarjeta { get; set; }
        public int IdEstado { get; set; }
        public int IdIdioma { get; set; }
        public int TieneDatos { get; set; }
        public string Correo { get; set; }
        public string CorreoCopiaA { get; set; }
        public int? IdTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreTarjetahabiente { get; set; }
        public int IdRelacion { get; set; }
        public decimal PagoContadoLocal { get; set; }
        public decimal PagoContadoInter { get; set; }
        public decimal PagoMinLocal { get; set; }
        public decimal PagoMinInter { get; set; }
        public DateTime FecPagoMinimo { get; set; }
        public DateTime FecPagoContado { get; set; }
        public DateTime FecCorte { get; set; }
        public string RemitenteCorreo { get; set; }
        public string NombreRemitente { get; set; }
        public int? IdCompania { get; set; }
    }
    public class PocComerciosTarjeta
    {
        public int IdCuenta { get; set; }
        public int? IdRelacion { get; set; }
        public int IdIdioma { get; set; }
        public string MesCorte { get; set; }
        public string AnnoCorte { get; set; }
        public string NumTarjeta { get; set; }
        public int agrupacion { get; set; }
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
        public DateTime? FecCorte { get; set; }
        public int PorCorte { get; set; }
    }

    public class PocResumenEstCuenta
    {
        public int IdCuenta { get; set; }
        public int IdEmpresa { get; set; }
        public int IdRelacion { get; set; }
        public int IdListaPredefinida { get; set; }
        public int IdCentroCosto { get; set; }
        public string CodMovimiento { get; set; }
        public string Mes { get; set; }
        public string Anno { get; set; }
    }

    public class PocRespuestaServicio
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }

    public class PocMigracionTarjeta
    {
        public string Detalle { get; set; }
        public string Identificacion { get; set; }
        public string NombreCompleto { get; set; }
        public string NumTarjetaMC { get; set; }
        public string NumTarjetaVISA { get; set; }
    }

    public class PocAplicarMovimiento
    {
        public string DescripcionCodRespuesta { get; set; }
        public string EstadoCuenta { get; set; }
        public string EstadoTarjeta { get; set; }
        public DateTime FecMovimiento { get; set; }
        public DateTime FecTransaccion { get; set; }
        public int IdCuenta { get; set; }
        public int IdMoneda { get; set; }
        public int IdMovimiento { get; set; }
        public int IdOrigenFondos { get; set; }
        public int IdPersona { get; set; }
        public int IdTarjeta { get; set; }
        public string IdTipoMovimiento { get; set; }
        public string Moneda { get; set; }
        public decimal MonTransaccion { get; set; }
        public string NombrePersona { get; set; }
        public string NumComprobante { get; set; }
        public string NumCuenta { get; set; }
        public string NumReferencia { get; set; }
        public string NumTarjeta { get; set; }
        public decimal PagoContado { get; set; }
        public decimal PagoContadoInter { get; set; }
        public decimal PagoMinimo { get; set; }
        public decimal PagoMinInter { get; set; }
        public decimal SaldoActual { get; set; }
        public decimal SaldoExtra { get; set; }
        public decimal SaldoExtraInter { get; set; }
        public decimal SaldoInter { get; set; }
        public string SimboloMonedaInter { get; set; }
        public string SimboloMonedaLocal { get; set; }
        public string TipoMovimiento { get; set; }
        public string Usuario { get; set; }
    }

    public class PocInfoEncabezadoEstCuenta
    {
        public string TipoTarjeta { get; set; }
        public string Estado { get; set; }

        /*******Info del Encabezado************/

        public int IdEstCuenta { get; set; }
        public int IdDiaCorte { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreCliente { get; set; }
        public string CorreoElectronico { get; set; }
        public string DireccionDeEnvio { get; set; }
        public decimal? LimiteCreditoLocal { get; set; }
        public decimal? SaldoCortePrincLocal { get; set; }
        public decimal? SaldoDisponibleLocal { get; set; }
        public decimal? LimiteCreditoInter { get; set; }
        public decimal? SaldoCortePrincInter { get; set; }
        public decimal? SaldoDisponibleInter { get; set; }
        public DateTime? FecPagoMinimo { get; set; }
        public DateTime FecCorte { get; set; }
        public decimal? PagoMinPrincLocal { get; set; }
        public decimal? PagoMinPrincInter { get; set; }
        public decimal? PagoMinInteresLocal { get; set; }
        public decimal? PagoMinInteresInter { get; set; }
        public decimal? PagoMinLocal { get; set; }
        public decimal? PagoMinInter { get; set; }
        public DateTime? FecPagoContado { get; set; }
        public decimal? PagoContadoLocal { get; set; }
        public decimal? PagoContadoInter { get; set; }
        public decimal? InterDiarioMoraLocal { get; set; }
        public decimal? InterDiarioMoraInter { get; set; }
        public decimal? MontoMoraLocal { get; set; }
        public decimal? MontoMoraInter { get; set; }
        public decimal? InteresMoraLocal { get; set; }
        public decimal? InteresMoraInter { get; set; }
        public decimal? OtrosCargosLocal { get; set; }
        public decimal? OtrosCargosInter { get; set; }
        public DateTime? FecCorteAnterior { get; set; }
        public decimal? SaldoAntInteresLocal { get; set; }
        public decimal? SaldoAntInteresInter { get; set; }
        public decimal? SaldoAnteriorLocal { get; set; }
        public decimal? SaldoAnteriorInter { get; set; }
        public decimal? SaldoIniPremCorte { get; set; }
        public decimal? DebitosPremCorte { get; set; }
        public decimal? CreditosPremCorte { get; set; }
        public decimal? PuntosPorRedimir { get; set; }
        public decimal? PuntosDelCorte { get; set; }
        public string ProgramaPuntos { get; set; }
        public decimal? LimiteExtraf { get; set; }
        public decimal? SobregiroLocal { get; set; }
        public decimal? SobregiroInter { get; set; }
        public string Observaciones { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }
        public int IdEstado { get; set; }
        public decimal InteresFinanciadoInter { get; set; }
        public decimal InteresFinanciadoLocal { get; set; }
        public decimal? TipoDeCambio { get; set; }
        public string NumeroCuentaMadre { get; set; }
        public string DetalleError { get; set; }
        public decimal? LimiteAdelantoLocal { get; set; }
        public decimal? LimiteAdelantoInter { get; set; }
        public int? CantImpVencidosLocal { get; set; }
        public int? CantImpVencidosInter { get; set; }
        public decimal? MonImpVencidosLocal { get; set; }
        public decimal? MonImpVencidosInter { get; set; }
        public decimal? CreditoLocal { get; set; }
        public decimal? CreditoInter { get; set; }
        public decimal? DebitoLocal { get; set; }
        public decimal? DebitoInter { get; set; }
        public decimal? TasaInteresLocal { get; set; }
        public decimal? TasaInteresInter { get; set; }
    }

    public class PocInfoEstCuenta
    {
        public string TipoTarjeta { get; set; }
        public string Estado { get; set; }

        /*******Info del Encabezado************/

        public int IdEstCuenta { get; set; }
        public int IdDiaCorte { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreCliente { get; set; }
        public string CorreoElectronico { get; set; }
        public string DireccionDeEnvio { get; set; }
        public decimal? LimiteCreditoLocal { get; set; }
        public decimal? SaldoCortePrincLocal { get; set; }
        public decimal? SaldoDisponibleLocal { get; set; }
        public decimal? LimiteCreditoInter { get; set; }
        public decimal? SaldoCortePrincInter { get; set; }
        public decimal? SaldoDisponibleInter { get; set; }
        public DateTime? FecPagoMinimo { get; set; }
        public DateTime FecCorte { get; set; }
        public decimal? PagoMinPrincLocal { get; set; }
        public decimal? PagoMinPrincInter { get; set; }
        public decimal? PagoMinInteresLocal { get; set; }
        public decimal? PagoMinInteresInter { get; set; }
        public decimal? PagoMinLocal { get; set; }
        public decimal? PagoMinInter { get; set; }
        public DateTime? FecPagoContado { get; set; }
        public decimal? PagoContadoLocal { get; set; }
        public decimal? PagoContadoInter { get; set; }
        public decimal? InterDiarioMoraLocal { get; set; }
        public decimal? InterDiarioMoraInter { get; set; }
        public decimal? MontoMoraLocal { get; set; }
        public decimal? MontoMoraInter { get; set; }
        public decimal? InteresMoraLocal { get; set; }
        public decimal? InteresMoraInter { get; set; }
        public decimal? OtrosCargosLocal { get; set; }
        public decimal? OtrosCargosInter { get; set; }
        public DateTime? FecCorteAnterior { get; set; }
        public decimal? SaldoAntInteresLocal { get; set; }
        public decimal? SaldoAntInteresInter { get; set; }
        public decimal? SaldoAnteriorLocal { get; set; }
        public decimal? SaldoAnteriorInter { get; set; }
        public decimal? SaldoIniPremCorte { get; set; }
        public decimal? DebitosPremCorte { get; set; }
        public decimal? CreditosPremCorte { get; set; }
        public decimal? PuntosPorRedimir { get; set; }
        public decimal? PuntosDelCorte { get; set; }
        public string ProgramaPuntos { get; set; }
        public decimal? LimiteExtraf { get; set; }
        public decimal? SobregiroLocal { get; set; }
        public decimal? SobregiroInter { get; set; }
        public string Observaciones { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }
        public int IdEstado { get; set; }
        public decimal InteresFinanciadoInter { get; set; }
        public decimal InteresFinanciadoLocal { get; set; }
        public decimal? TipoDeCambio { get; set; }
        public string NumeroCuentaMadre { get; set; }
        public string DetalleError { get; set; }
        public decimal? LimiteAdelantoLocal { get; set; }
        public decimal? LimiteAdelantoInter { get; set; }
        public int? CantImpVencidosLocal { get; set; }
        public int? CantImpVencidosInter { get; set; }
        public decimal? MonImpVencidosLocal { get; set; }
        public decimal? MonImpVencidosInter { get; set; }
        public decimal? CreditoLocal { get; set; }
        public decimal? CreditoInter { get; set; }
        public decimal? DebitoLocal { get; set; }
        public decimal? DebitoInter { get; set; }
        public decimal? TasaInteresLocal { get; set; }
        public decimal? TasaInteresInter { get; set; }
        public List<PocInfoDetalleEstCuenta> Detalle { get; set;}
    }

    public class PocInfoDetalleEstCuenta
    {
        /*******Info del Detalle************/

        public DateTime Fecha { get; set; }
        public string Comprobante { get; set; }
        public string DetalleTransacción { get; set; }
        public decimal ImporteLocal { get; set; }
        public decimal ImporteDolares { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreTarjetahabiente { get; set; }
    }
}

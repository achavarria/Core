using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_CamposContextoMap : EntityTypeConfiguration<GL_CamposContexto>
    {
        public GL_CamposContextoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCampo);

            // Properties
            this.Property(t => t.IdCampo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Campo)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Descripcion)
                .HasMaxLength(30);

            this.Property(t => t.SQLOrigen)
                .HasMaxLength(2000);

            this.Property(t => t.CampoRelacionado)
                .HasMaxLength(100);

            this.Property(t => t.Referencia1)
                .HasMaxLength(20);

            this.Property(t => t.Referencia2)
                .HasMaxLength(20);

            this.Property(t => t.Referencia3)
                .HasMaxLength(20);

            this.Property(t => t.Referencia4)
                .HasMaxLength(20);

            this.Property(t => t.Referencia5)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("GL_CamposContexto", "General");
            this.Property(t => t.IdCampo).HasColumnName("IdCampo");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.Campo).HasColumnName("Campo");
            this.Property(t => t.IdTipo).HasColumnName("IdTipo");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.SQLOrigen).HasColumnName("SQLOrigen");
            this.Property(t => t.CampoRelacionado).HasColumnName("CampoRelacionado");
            this.Property(t => t.Referencia1).HasColumnName("Referencia1");
            this.Property(t => t.Referencia2).HasColumnName("Referencia2");
            this.Property(t => t.Referencia3).HasColumnName("Referencia3");
            this.Property(t => t.Referencia4).HasColumnName("Referencia4");
            this.Property(t => t.Referencia5).HasColumnName("Referencia5");
        }
    }
}

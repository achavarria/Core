using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Autorizador
{
    public partial class MB_Pais : EntidadBaseCtrl
    {
        public string CodPaisISO { get; set; }
        public int IdPais { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public string Nacionalidad { get; set; }
        public string CodMonedaISO { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

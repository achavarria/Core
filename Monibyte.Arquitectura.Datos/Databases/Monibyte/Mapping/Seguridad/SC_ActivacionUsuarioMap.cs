using Monibyte.Arquitectura.Dominio.Seguridad;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Seguridad
{
    public class SC_ActivacionUsuarioMap : EntityTypeConfiguration<SC_ActivacionUsuario>
    {
        public SC_ActivacionUsuarioMap()
        {
            // Primary Key
            this.HasKey(t => t.IdUsuario);

            // Properties
            this.Property(t => t.IdUsuario)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodActivacion)
                .IsRequired()
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("SC_ActivacionUsuario", "Seguridad");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.CodActivacion).HasColumnName("CodActivacion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

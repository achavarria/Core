﻿using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Datos.Nucleo;
using System;
using System.Data;
using System.Linq.Expressions;

namespace Monibyte.Arquitectura.Datos
{
    public class UnidadTbjoBase : UnidadDeTrabajo
    {
        public UnidadTbjoBase(DbContextBase ctx) : base(ctx) { }

        public override void Save()
        {
            SetContextInfo();
            base.Save();
        }

        public override int UpdateOn<TEntity>(Expression<Func<TEntity, bool>> condition,
            Expression<Func<TEntity, TEntity>> updateExp)
        {
            SetContextInfo();
            return base.UpdateOn<TEntity>(condition, updateExp);
        }

        public override int RemoveOn<TEntity>(Expression<Func<TEntity, bool>> condition)
        {
            SetContextInfo();
            return base.RemoveOn<TEntity>(condition);
        }

        public void SetContextInfo()
        {
            // Open the connection early and leave it open
            if (this._ctx.Database.Connection.State == ConnectionState.Closed)
            {
                this._ctx.Database.Connection.Open();
            }
            if (InfoSesion.Info != null)
            {
                //{0}=Auditoria por trigger|{1}=Usuario|{2}=IP
                var command = string.Format(@"EXEC Logs.P_SetContextInfo '{0}|{1}|{2}'",
                    1, InfoSesion.Info.CodUsuario, InfoSesion.Info.DireccionIpv4);
                this.ExecuteCommand(command);
            }
        }
    }
}

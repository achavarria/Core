﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Trama;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio.Tarjetas.MQATH;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios.MQATH;
using Monibyte.Arquitectura.MQ.Nucleo.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Monibyte.Arquitectura.Datos.Tarjetas.MQATH
{
    public class RepositorioATH : RepositorioMQ, IRepositorioATH
    {
        private const string EXITOSO = "00";
        private const string ColaPorDefecto = "ATH-MONIBYTE";

        public RepositorioATH(string configuracion, string archivoConf = "", bool web = true) : base(configuracion, archivoConf, web) { }
        #region métodos privados
        private object ObtieneValorPropiedad<T>(T entrada, string propiedad = "") where T : class
        {
            if (string.IsNullOrEmpty(propiedad))
            {
                return null;
            }
            foreach (PropertyInfo p in entrada.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static))
            {
                if (p.Name == propiedad)
                {
                    var valor = p.GetValue(entrada, null);
                    return valor == null ? null : valor;
                }
            }
            return null;
        }
        private static string ObtieneMensajeMQ(string tipoMensaje, string xmlFile = "SimuladorMQ")
        {
            var filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                                                  string.Format(@"Tarjetas\{0}.xml", xmlFile));
            var document = XDocument.Load(filePath);
            var sqlPath = string.Format("/MENSAJESMQ/{0}", tipoMensaje);
            var result = document.XPathSelectElements(sqlPath);
            if (result != null && result.Any())
            {
                return result.FirstOrDefault().Value;
            }
            return string.Empty;
        }
        private string[] MensajeSalidaSimulador<TFuente>(TFuente modeloEntrada)
            where TFuente : class
        {
            var tipoMensaje = ObtieneValorPropiedad<TFuente>(modeloEntrada, "TipoMensaje");
            var mensaje = ObtieneMensajeMQ(string.Format("M{0}", tipoMensaje)).TrimStart().TrimStart('\t');

            if (!string.IsNullOrEmpty(mensaje))
            {
                foreach (PropertyInfo p in modeloEntrada.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static))
                {
                    var valor = p.GetValue(modeloEntrada, null);
                    var valorStr = valor == null ? null : valor.ToString();
                    mensaje = mensaje.Replace(string.Format("#{0}#", p.Name), valorStr);
                }
                var messageArray = mensaje.Split('\n');
                return messageArray;
            }
            return null;
        }
        #endregion
        private IEnumerable<TDestino> AccionaMQ<TFuente, TDestino>(string seccion, TFuente modeloEntrada, string tipoMsjSalida = "")
            where TFuente : class, new()
            where TDestino : RespuestaBase, new()
        {
            var mensajeSalida = string.Empty;
            var usoMQ = false;
            string inMsg = string.Empty;
            ParamSalida outMQParams = new ParamSalida();
            try
            {
                int result = TramaUtil.ValidateModel<TFuente>(modeloEntrada);
                inMsg = TramaUtil.BuildTrama<TFuente>(modeloEntrada);
                var resultado = new List<TDestino>();

                var propiedadesCola = ObtenerPropiedadesCola(seccion);

                if (propiedadesCola.MqChannel != "SIMULADORMQ")
                {
                    usoMQ = true;
                    _servidorMQ.AbrirConeccionMQ(propiedadesCola);
                    outMQParams = _servidorMQ.EscribirMensaje(message: inMsg);
                    _servidorMQ.CommitMQ();
                }
                var correlativoLectura = outMQParams == null ? null : outMQParams.messageId;
                string separadorMsg = _propiedadesMQ.ObtieneValPropiedad<string>("SepTramas");
                int indice = 0;
                string[] simulador = null;
                bool firstMessage = true;
                do
                {
                    if (usoMQ)
                    {
                        outMQParams = _servidorMQ.LeerMensaje(firstMessage, correlationId: correlativoLectura);
                        _servidorMQ.CommitMQ();
                    }
                    else
                    {
                        var mensaje = outMQParams == null ? string.Empty : outMQParams.message;
                        if (indice == 0)
                        {
                            simulador = MensajeSalidaSimulador<TFuente>(modeloEntrada);
                        }
                        outMQParams.message = simulador[indice].TrimStart('\t');
                        indice++;
                    }
                    mensajeSalida = ObtieneCondicionSalida(outMQParams.message, separadorMsg, tipoMsjSalida);
                    if (string.IsNullOrEmpty(tipoMsjSalida) || mensajeSalida != tipoMsjSalida)
                    {
                        var modeloSalida = TramaUtil.BuildModel<TDestino>(outMQParams.message, separadorMsg);
                        if ((string.IsNullOrEmpty(modeloSalida.CodigoRespuesta) ? EXITOSO : modeloSalida.CodigoRespuesta) != EXITOSO)
                        {
                            var descripcionRespuesta =
                                CodRespuestaATH.ResourceManager.GetString(string.Concat("ATH_", modeloSalida.CodigoRespuesta.ToString(), "_", modeloSalida.TipoMensaje));

                            var validation = new ValidationResponse();
                            if (string.IsNullOrEmpty(descripcionRespuesta))
                            {
                                validation.AddError("Err_0000");
                            }
                            if (!validation.IsValid)
                            {
                                Logger.Log("Error en Monibyte.Arquitectura.Datos.Tarjetas.RepositorioATHC.AccionaMQ" + ";" + modeloSalida.TipoMensaje + "/" + modeloSalida.CodigoRespuesta + "/" + modeloSalida.DescripcionCodRespuesta, "", tipoLog: new string[] { "TramaMQ" }, idEvento: 1001, severity: System.Diagnostics.TraceEventType.Information);
                                Logger.Log("Error en Monibyte.Arquitectura.Datos.Tarjetas.RepositorioATHC.AccionaMQ", modeloSalida.DescripcionCodRespuesta);
                                throw new CoreException("Error en Monibyte.Arquitectura.Datos.Tarjetas.RepositorioATHC.AccionaMQ", validation: validation);
                            }
                            else
                            {
                                modeloSalida.DescripcionCodRespuesta = string.Format("{0}-{1}", modeloSalida.CodigoRespuesta, modeloSalida.DescripcionCodRespuesta);
                                Logger.Log("Error en Monibyte.Arquitectura.Datos.Tarjetas.RepositorioATHC.AccionaMQ" + ";" + modeloSalida.TipoMensaje + "/" + modeloSalida.CodigoRespuesta + "/" + modeloSalida.DescripcionCodRespuesta, "", tipoLog: new string[] { "TramaMQ" }, idEvento: 1001, severity: System.Diagnostics.TraceEventType.Information);
                                throw new CoreException("Error en Monibyte.Arquitectura.Datos.Tarjetas.RepositorioATHC.AccionaMQ", modeloSalida.CodigoRespuesta,
                                    modeloSalida.DescripcionCodRespuesta, reasonCode: modeloSalida.CodigoRespuesta);
                            }
                        }
                        resultado.Add(modeloSalida);
                    }
                    firstMessage = false;
                } while (mensajeSalida != tipoMsjSalida && !string.IsNullOrEmpty(tipoMsjSalida));

                if (resultado != null && resultado.Count > 0)
                {
                    Logger.Log(inMsg + ";" + resultado[0].TipoMensaje + "/" + resultado[0].CodigoRespuesta + "/" + resultado[0].DescripcionCodRespuesta, "", tipoLog: new string[] { "TramaMQ" }, idEvento: 1001, severity: System.Diagnostics.TraceEventType.Information);
                }
                else
                {
                    Logger.Log(inMsg + ";" + outMQParams.message, "", tipoLog: new string[] { "TramaMQ" }, idEvento: 1001, severity: System.Diagnostics.TraceEventType.Information);
                }

                if (usoMQ)
                {
                    _servidorMQ.CommitMQ();
                    _servidorMQ.CerrarMQ();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                Logger.Log(inMsg + "; ;ERROR;" + (String.IsNullOrEmpty(outMQParams.message) ? ex.StackTrace : outMQParams.message), "", tipoLog: new string[] { "TramaMQ" }, idEvento: 1001, severity: System.Diagnostics.TraceEventType.Information);
                Logger.Log(ex.StackTrace, ex.Message);
                if (usoMQ)
                {
                    _servidorMQ.CerrarMQ();
                }
                throw ex;
            }
        }
        public string ObtieneCondicionSalida(string tramaRespuesta, string separador, string tipoMsjSalida)
        {
            var mensajeSalida = tramaRespuesta.Substring(0, 4);
            if (mensajeSalida == TipoSalidaMsj.RESP_CONSULTA_CTAS_CLIENTE)
            {
                var valores = tramaRespuesta.Split(new string[] { separador }, StringSplitOptions.None);
                mensajeSalida = string.Empty;
                if (valores.Count() > 7)
                {
                    mensajeSalida = tipoMsjSalida;
                }
            }
            return (mensajeSalida);
        }
        public void LimpiarCola(bool pCommit = false, string seccion = "")
        {
            _servidorMQ.AbrirConeccionMQ(ObtenerPropiedadesCola(seccion));
            for (; ; )
            {
                try
                {
                    var outMQParams = _servidorMQ.LeerMensaje(correlationId: null);
                    if (pCommit)
                    {
                        _servidorMQ.CommitMQ();
                    }
                }
                catch (Exception)
                {
                    break;
                }
            }
            _servidorMQ.CerrarMQ();
        }
        private MqQueueProperties ObtenerPropiedadesCola(string seccion)
        {
            seccion = string.IsNullOrEmpty(seccion) ? ColaPorDefecto : seccion;
            var queueProperties = new MqQueueProperties
            {
                MqChannel = _propiedadesMQ.ObtieneValPropiedad<string>("channelMQ", seccion),
                PutQueueName = _propiedadesMQ.ObtieneValPropiedad<string>("QueueNamePut", seccion),
                GetQueueName = _propiedadesMQ.ObtieneValPropiedad<string>("QueueNameGet", seccion)
            };
            return queueProperties;
        }
        public string ConvierteValorPropiedad(string valor, string propiedad, string seccion)
        {
            seccion = string.IsNullOrEmpty(seccion) ? ColaPorDefecto : seccion;
            return (string.IsNullOrEmpty(valor) ? _propiedadesMQ.ObtieneValPropiedad<string>(propiedad, seccion) : valor);
        }
        public SalidaConsultaTarjeta ProcesarConsultaTarjeta(EntradaConsultaTarjeta entrada, string seccion = "")
        {
            SalidaConsultaTarjeta tramaSalida = new SalidaConsultaTarjeta();
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaConsultaTarjeta, SalidaConsultaTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaPagoTarjeta ProcesarPagoTarjeta(EntradaPagoTarjeta entrada, string seccion = "")
        {
            SalidaPagoTarjeta tramaSalida = new SalidaPagoTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Localidad = ConvierteValorPropiedad(entrada.Localidad, "Localidad", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaPagoTarjeta, SalidaPagoTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaActualizaTarjeta ProcesarEstadoTarjeta(EntradaActualizaTarjeta entrada, string seccion = "")
        {
            SalidaActualizaTarjeta tramaSalida = new SalidaActualizaTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaActualizaTarjeta, SalidaActualizaTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaActualizaLimiteCuenta ProcesarLimiteCuenta(EntradaActualizaLimiteCuenta entrada, string seccion = "")
        {
            SalidaActualizaLimiteCuenta tramaSalida = new SalidaActualizaLimiteCuenta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.LocalidadTramite = ConvierteValorPropiedad(entrada.LocalidadTramite, "Localidad", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            entrada.LocalidadRetiro = string.IsNullOrEmpty(entrada.LocalidadRetiro) ? entrada.LocalidadTramite : entrada.LocalidadRetiro;
            tramaSalida = AccionaMQ<EntradaActualizaLimiteCuenta, SalidaActualizaLimiteCuenta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaActualizaLimiteTarjeta ProcesarLimiteTarjeta(EntradaActualizaLimiteTarjeta entrada, string seccion = "")
        {
            SalidaActualizaLimiteTarjeta tramaSalida = new SalidaActualizaLimiteTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.LocalidadTramite = ConvierteValorPropiedad(entrada.LocalidadTramite, "Localidad", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            entrada.LocalidadRetiro = string.IsNullOrEmpty(entrada.LocalidadRetiro) ? entrada.LocalidadTramite : entrada.LocalidadRetiro;
            tramaSalida = AccionaMQ<EntradaActualizaLimiteTarjeta, SalidaActualizaLimiteTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaActivaTarjeta ProcesarActivaTarjeta(EntradaActivaTarjeta entrada, string seccion = "")
        {
            SalidaActivaTarjeta tramaSalida = new SalidaActivaTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.LocalidadCustodia = ConvierteValorPropiedad(entrada.LocalidadCustodia, "Localidad", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            entrada.LocalidadDestino = string.IsNullOrEmpty(entrada.LocalidadDestino) ? entrada.LocalidadCustodia : entrada.LocalidadDestino;
            tramaSalida = AccionaMQ<EntradaActivaTarjeta, SalidaActivaTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaReposicionPinTarjeta ProcesarReposicionPinTarjeta(EntradaReposicionPinTarjeta entrada, string seccion = "")
        {
            SalidaReposicionPinTarjeta tramaSalida = new SalidaReposicionPinTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.LocalidadTramite = ConvierteValorPropiedad(entrada.LocalidadTramite, "Localidad", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            entrada.LocalidadRetiro = string.IsNullOrEmpty(entrada.LocalidadRetiro) ? entrada.LocalidadTramite : entrada.LocalidadRetiro;
            tramaSalida = AccionaMQ<EntradaReposicionPinTarjeta, SalidaReposicionPinTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public IEnumerable<SalidaMovimientosTarjeta> ProcesarMovimientosTarjeta(EntradaMovimientosTarjeta entrada, string seccion = "")
        {
            IEnumerable<SalidaMovimientosTarjeta> tramaSalida = new List<SalidaMovimientosTarjeta>();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaMovimientosTarjeta, SalidaMovimientosTarjeta>(seccion, entrada,
                TipoSalidaMsj.FINAL_MOVIMIENTOS_TARJETA);
            return tramaSalida;
        }
        public IEnumerable<SalidaMovTransitoTarjeta> ProcesarMovTransitoTarjeta(EntradaMovimientosTarjeta entrada, string seccion = "")
        {
            IEnumerable<SalidaMovTransitoTarjeta> tramaSalida = new List<SalidaMovTransitoTarjeta>();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaMovimientosTarjeta, SalidaMovTransitoTarjeta>(seccion, entrada,
                TipoSalidaMsj.FINAL_MOVIMIENTOS_TRANSITO);
            return tramaSalida;
        }
        public IEnumerable<SalidaLealtadTarjeta> ProcesarProgLealtadTarjeta(EntradaLealtadTarjeta entrada, string seccion = "")
        {
            IEnumerable<SalidaLealtadTarjeta> tramaSalida = new List<SalidaLealtadTarjeta>();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaLealtadTarjeta, SalidaLealtadTarjeta>(seccion, entrada,
                TipoSalidaMsj.FINAL_LEALTAD_TARJETA);
            return tramaSalida;
        }
        public SalidaAplicMovLealtadTarjeta ProcesarAplicaMovLealtadTarjeta(EntradaAplicMovLealtadTarjeta entrada, string seccion = "")
        {
            SalidaAplicMovLealtadTarjeta tramaSalida = new SalidaAplicMovLealtadTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaAplicMovLealtadTarjeta, SalidaAplicMovLealtadTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public IEnumerable<SalidaConsultaCuentas> ProcesarConsultaCuentas(EntradaConsultaCuentas entrada, string seccion = "")
        {
            IEnumerable<SalidaConsultaCuentas> tramaSalida = new List<SalidaConsultaCuentas>();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaConsultaCuentas, SalidaConsultaCuentas>(seccion, entrada,
                TipoSalidaMsj.FINAL_CONSULTA_CTAS_CLIENTE);
            return tramaSalida;
        }
        public SalidaMiscelaneosTarjeta ProcesarMiscelaneosTarjeta(EntradaMiscelaneosTarjeta entrada, string seccion = "")
        {
            SalidaMiscelaneosTarjeta tramaSalida = new SalidaMiscelaneosTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            entrada.Localidad = ConvierteValorPropiedad(entrada.Localidad, "Localidad", seccion);
            tramaSalida = AccionaMQ<EntradaMiscelaneosTarjeta, SalidaMiscelaneosTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaAdelantoEfectivoTarjeta ProcesarAdelantoEfectivoTarjeta(EntradaAdelantoEfectivoTarjeta entrada, string seccion = "")
        {
            SalidaAdelantoEfectivoTarjeta tramaSalida = new SalidaAdelantoEfectivoTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            entrada.Localidad = ConvierteValorPropiedad(entrada.Localidad, "Localidad", seccion);
            tramaSalida = AccionaMQ<EntradaAdelantoEfectivoTarjeta, SalidaAdelantoEfectivoTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaInclusionSeguroTarjeta ProcesarInclusionSeguroTarjeta(EntradaInclusionSeguroTarjeta entrada, string seccion = "")
        {
            SalidaInclusionSeguroTarjeta tramaSalida = new SalidaInclusionSeguroTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaInclusionSeguroTarjeta, SalidaInclusionSeguroTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaCargoAutomaticoTarjeta ProcesarCargoAutomaticoTarjeta(EntradaCargoAutomaticoTarjeta entrada, string seccion = "")
        {
            SalidaCargoAutomaticoTarjeta tramaSalida = new SalidaCargoAutomaticoTarjeta();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            entrada.CodSucursal = ConvierteValorPropiedad(entrada.CodSucursal, "Localidad", seccion);
            tramaSalida = AccionaMQ<EntradaCargoAutomaticoTarjeta, SalidaCargoAutomaticoTarjeta>(seccion, entrada).First();
            return tramaSalida;
        }
        public SalidaPagoExtraFinanciamiento ProcesarPagoExtraFinanciamientoa(EntradaPagoExtraFinanciamiento entrada, string seccion = "")
        {
            SalidaPagoExtraFinanciamiento tramaSalida = new SalidaPagoExtraFinanciamiento();
            entrada.SuperFranquicia = ConvierteValorPropiedad(entrada.SuperFranquicia, "SuperFranquicia", seccion);
            entrada.Usuario = ConvierteValorPropiedad(entrada.Usuario, "UsuarioMQ", seccion);
            tramaSalida = AccionaMQ<EntradaPagoExtraFinanciamiento, SalidaPagoExtraFinanciamiento>(seccion, entrada).First();
            return tramaSalida;
        }
    }
}

using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_TarjetaHistMap : EntityTypeConfiguration<TC_TarjetaHist>
    {
        public TC_TarjetaHistMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdTarjeta, t.IdCorte });

            // Properties
            this.Property(t => t.IdTarjeta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdCorte)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdTipoPlastico)
                .HasMaxLength(10);

            this.Property(t => t.NombreImpreso)
                .HasMaxLength(60);

            this.Property(t => t.NumTarjeta)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.NumPoliza)
                .HasMaxLength(30);

            this.Property(t => t.CorreoEstCuenta)
                .HasMaxLength(120);

            this.Property(t => t.CopiaCorreoEstCuenta)
                .HasMaxLength(120);

            this.Property(t => t.Placa)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("TC_TarjetaHist", "TCredito");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.IdCorte).HasColumnName("IdCorte");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdTipoPlastico).HasColumnName("IdTipoPlastico");
            this.Property(t => t.NombreImpreso).HasColumnName("NombreImpreso");
            this.Property(t => t.IdRelacion).HasColumnName("IdRelacion");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdGrupoEmpresa).HasColumnName("IdGrupoEmpresa");
            this.Property(t => t.FecApertura).HasColumnName("FecApertura");
            this.Property(t => t.LimiteCreditoInter).HasColumnName("LimiteCreditoInter");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.IdEstadoTarjeta).HasColumnName("IdEstadoTarjeta");
            this.Property(t => t.FecVencimiento).HasColumnName("FecVencimiento");
            this.Property(t => t.FecActivacion).HasColumnName("FecActivacion");
            this.Property(t => t.IdUsuarioActiva).HasColumnName("IdUsuarioActiva");
            this.Property(t => t.FecEntrega).HasColumnName("FecEntrega");
            this.Property(t => t.IdUsuarioEntrega).HasColumnName("IdUsuarioEntrega");
            this.Property(t => t.FecRenovacion).HasColumnName("FecRenovacion");
            this.Property(t => t.IdUsuarioRenueva).HasColumnName("IdUsuarioRenueva");
            this.Property(t => t.FecReposicion).HasColumnName("FecReposicion");
            this.Property(t => t.IdTarjetaAnterior).HasColumnName("IdTarjetaAnterior");
            this.Property(t => t.IdUsuarioRepone).HasColumnName("IdUsuarioRepone");
            this.Property(t => t.IdTipoReposicion).HasColumnName("IdTipoReposicion");
            this.Property(t => t.DisponibleLocalTarjeta).HasColumnName("DisponibleLocalTarjeta");
            this.Property(t => t.DisponibleInterTarjeta).HasColumnName("DisponibleInterTarjeta");
            this.Property(t => t.DispAdelantoLocalTarjeta).HasColumnName("DispAdelantoLocalTarjeta");
            this.Property(t => t.DispAdelantoInterTarjeta).HasColumnName("DispAdelantoInterTarjeta");
            this.Property(t => t.IdTipoPoliza).HasColumnName("IdTipoPoliza");
            this.Property(t => t.NumPoliza).HasColumnName("NumPoliza");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.LimiteCreditoLocal).HasColumnName("LimiteCreditoLocal");
            this.Property(t => t.SaldoTarjetaLocal).HasColumnName("SaldoTarjetaLocal");
            this.Property(t => t.SaldoTarjetaInter).HasColumnName("SaldoTarjetaInter");
            this.Property(t => t.CamposDinamicos).HasColumnName("CamposDinamicos");
            this.Property(t => t.IdTipoLimite).HasColumnName("IdTipoLimite");
            this.Property(t => t.CorreoEstCuenta).HasColumnName("CorreoEstCuenta");
            this.Property(t => t.CopiaCorreoEstCuenta).HasColumnName("CopiaCorreoEstCuenta");
            this.Property(t => t.Placa).HasColumnName("Placa");
        }
    }
}

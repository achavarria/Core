﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.TEF;
using Monibyte.Arquitectura.Dominio.TEF;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<TF_CedulasRegistrada> TF_CedulasRegistrada { get; set; }
        public DbSet<TF_TransaccionSinpe> TF_TransaccionSinpe { get; set; }
        public DbSet<TF_PagoTrasaccion> TF_PagoTrasaccion { get; set; }
        public DbSet<TF_TramaSinpe> TF_TramaSinpe { get; set; }

        void CreateContextModelTF(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TF_CedulasRegistradaMap());
            modelBuilder.Configurations.Add(new TF_TransaccionSinpeMap());
            modelBuilder.Configurations.Add(new TF_PagoTrasaccionMap());
            modelBuilder.Configurations.Add(new TF_TramaSinpeMap());
        }
    }
}

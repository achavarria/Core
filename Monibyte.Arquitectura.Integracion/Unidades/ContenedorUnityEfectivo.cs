﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Efectivo;
    using Monibyte.Arquitectura.Datos.Efectivo.Repositorios;
    using Monibyte.Arquitectura.Dominio.Efectivo.Repositorios;

    public class ContenedorUnityEfectivo
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            _unityContainer.RegisterType<IAppMovimientos, AppMovimientos>();
            #endregion

            #region Repositorios
           
            _unityContainer.RegisterType<IRepositorioMovimientos, RepositorioMovimientos>();
          
            #endregion
        }
    }
}

﻿using Monibyte.Arquitectura.Poco.Seguridad;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.Generales
{
    public class PocInfoUsuario
    {
        /* Informacion del Usuario datos de seguridad, regionalidad y los personales */
        public string CodUsuario { get; set; }
        public int IdUsuario { get; set; }
        public string Correo { get; set; }
        public string AliasUsuario { get; set; }
        public int? IdIdiomaPreferencia { get; set; }
        public int? IdPaisPreferencia { get; set; }
        public string Pais { get; set; }
        public string Idioma { get; set; }
        public int IdRequiereOTP { get; set; }
        public int IdUsaOtp { get; set; }
    }

    public class PocPais
    {
        public int IdPais { get; set; }
        public int IdRegion { get; set; }
        public string Descripcion { get; set; }
        public string Nacionalidad { get; set; }
    }

    public class PocUsuario
    {
        public int IdUsuario { get; set; }
        public int IdRole { get; set; }
        public string NumIdentificacion { get; set; }
        public int IdEmpresa { get; set; }
        public int IdPersona { get; set; }
        public int IdTipoUsuario { get; set; }
        public int IdPaisPreferencia { get; set; }
        public int IdIdiomaPreferencia { get; set; }
        public int IdRequiereOTP { get; set; }
        public int IdCompania { get; set; }
        public int IdUnidad { get; set; }
        public int IdEstado { get; set; }
        public int? IdEjecutivo { get; set; }
        public string Estado { get; set; }
        public string Correo { get; set; }
        public string NombreCompleto { get; set; }
        public string NombreEmpresa { get; set; }
        public int? TipoPersona { get; set; }
        public string NumIdentificacionEmpresa { get; set; }
        public int NumIntentosFallidos { get; set; }
        public string TipoUsuario { get; set; }
        public string CodUsuario { get; set; }
        public string CodUsuarioEmpresa { get; set; }

        public int MostrarAdicionales { get; set; }
        public bool EsBloqueado { get; set; }
        public string DescEsBloqueado { get; set; }
        public string Grupo { get; set; }
        public string Alias { get; set; }

        public int? IdRoleOperativo { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public string TiempoTranscurrido { get; set; }
        public DateTime? FecInicio { get; set; }
        public string Ref4TipoUsuario { get; set; }
        public bool EsInterno { get; set; }    }

    public class PocSesionActiva
    {
        public string Ticket { get; set; }
        public string CodSesion { get; set; }
        public string CodUsuario { get; set; }
        public string DireccionIp { get; set; }
        public int? IdEstadoUsr { get; set; }
        public DateTime FecInicio { get; set; }
    }

    public class PocUsuarioRoleOperativo
    {
        public int IdUsuario { get; set; }
        public int IdTipoUsuario { get; set; }
        public int? IdRoleOperativo { get; set; }
        public string NombreCompleto { get; set; }


        public bool Equals(PocUsuarioRoleOperativo o)
        {
            return this.IdUsuario == o.IdUsuario &&
                this.IdRoleOperativo == o.IdRoleOperativo;
        }
    }

    public class PocSeguridadUsuario
    {
        public int IdUsuario { get; set; }
        public string CodUsuario { get; set; }
        public string Contrasena { get; set; }
        public bool InicializarContadores { get; set; }
        public bool BloquearUsuario { get; set; }
        public bool RenovarContraseña { get; set; }
    }

    public class PocUsuariosGrupos
    {
        public string NumIdentificacion { get; set; }
        public string NombreCompleto { get; set; }
        public string CodUsuario { get; set; }
    }

    public class PocRegistroUsuario
    {
        public string AliasUsuario { get; set; }
        public string CodUsuario { get; set; }
        public string Correo { get; set; }
        public int? IdPaisPreferencia { get; set; }
        public int? IdIdiomaPreferencia { get; set; }
        public int IdRequiereOTP { get; set; }
        public IEnumerable<PocPreguntaUsuario> ListaPreguntas { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte;
using Monibyte.Arquitectura.Dominio.Autorizador;
using Monibyte.Arquitectura.Dominio.Autorizador.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Autorizador.Repositorios
{
    public class RepositorioCondicionTarjeta : RepositorioCtrl<MB_CondicionTarjeta>, IRepositorioCondicionTarjeta
    {
        public RepositorioCondicionTarjeta(IUnidadTbjoCtrlMonibyte unidadTbjo) : base(unidadTbjo) { }

        public MB_CondicionTarjeta ObtenerPorNumTarjeta(string numTarjeta)
        {
            var criteria = new Criteria<MB_CondicionTarjeta>();
            criteria.And(p => p.NumTarjeta == numTarjeta);
            return SelectBy(criteria).FirstOrDefault();
        }
    }    

}

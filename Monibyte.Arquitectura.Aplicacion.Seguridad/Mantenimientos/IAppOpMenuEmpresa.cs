﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Seguridad;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad.Mantenimientos
{
    public interface IAppOpMenuEmpresa : IAplicacionBase
    {
        void Insertar(PocMenuEmpresa registro);
        void Eliminar(PocMenuEmpresa registro);
        void Modificar(PocMenuEmpresa registro);
        DataResult<PocMenuEmpresa> Listar(PocMenuEmpresa filtro, DataRequest request);
    }
}

using Monibyte.Arquitectura.Dominio.Quickpass;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Quickpass
{
    public class QP_MovimientosMap : EntityTypeConfiguration<QP_Movimientos>
    {
        public QP_MovimientosMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMovimiento);

            // Properties
            this.Property(t => t.Detalle)
                .HasMaxLength(300);

            this.Property(t => t.NumReferenciaPago)
                .HasMaxLength(20);

            this.Property(t => t.CodLocalizador)
                .HasMaxLength(10);

            this.Property(t => t.NumRefProcesador)
                .HasMaxLength(20);

            this.Property(t => t.DetAplicacionPago)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("QP_Movimientos", "Quickpass");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdTipoMovimiento).HasColumnName("IdTipoMovimiento");
            this.Property(t => t.CodMovimiento).HasColumnName("CodMovimiento");
            this.Property(t => t.IdQuickpass).HasColumnName("IdQuickpass");
            this.Property(t => t.FecConsumo).HasColumnName("FecConsumo");
            this.Property(t => t.Monto).HasColumnName("Monto");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecProcesamiento).HasColumnName("FecProcesamiento");
            this.Property(t => t.FecAplicacion).HasColumnName("FecAplicacion");
            this.Property(t => t.IdLote).HasColumnName("IdLote");
            this.Property(t => t.Detalle).HasColumnName("Detalle");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.NumReferenciaPago).HasColumnName("NumReferenciaPago");
            this.Property(t => t.IdTipoRegistro).HasColumnName("IdTipoRegistro");
            this.Property(t => t.CodLocalizador).HasColumnName("CodLocalizador");
            this.Property(t => t.NumRefProcesador).HasColumnName("NumRefProcesador");
            this.Property(t => t.DetAplicacionPago).HasColumnName("DetAplicacionPago");
        }
    }
}

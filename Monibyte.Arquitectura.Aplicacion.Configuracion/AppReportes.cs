﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public class AppReportes : AplicacionBase, IAppReportes
    {
        private IRepositorioReportServer _repReportServer;

        public AppReportes(IRepositorioReportServer repReportServer)
        {
            _repReportServer = repReportServer;
        }

        public PocReporte GenerarReporteGrupos(int idEmpresa, int? idGrupo, int idEntidad, string nombreCompleto)
        {
            //Parameters
            var parameters = new List<ReportParam>();
            parameters.Add(new ReportParam { Name = "IdEmpresa", Value = idEmpresa.ToString() });
            if (idGrupo <= 0)
            {
                parameters.Add(new ReportParam { Name = "IdGrupo", Value = null });
            }
            else
            {
                parameters.Add(new ReportParam { Name = "IdGrupo", Value = idGrupo.ToString() });
            }
            parameters.Add(new ReportParam { Name = "IdEntidad", Value = idEntidad.ToString() });
            parameters.Add(new ReportParam { Name = "NombreCompleto", Value = nombreCompleto });

            Reporte resultado = _repReportServer.ExportPdf("/Reports/ConfiguracionDeEmpresa/r-GrupEmpresa", parameters.ToArray());
            return resultado.ProyectarComo<PocReporte>();
        }
    }
}

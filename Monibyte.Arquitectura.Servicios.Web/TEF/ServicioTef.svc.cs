﻿using Monibyte.Arquitectura.Aplicacion.TEF.SINPE;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.TEF;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace Monibyte.Arquitectura.Servicios.Web.TEF
{
    public class ServicioTef : ServicioUnity, IServicioTef
    {
        private IOperacionesSINPE _appOpeSinpe;

        public ServicioTef(
            IOperacionesSINPE appOpeSinpe)
        {
            _appOpeSinpe = appOpeSinpe;
        }

        public void RegistraResultadoPagoEntrante(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<List<PocTransEntranteSINPE>>();
            var xmlData = transporte.GetValor<string>("xmlData");
            var app = GestorUnity.CargarUnidad<IOperacionesSINPE>();

            ThreadStart LystoDelegado = delegate { app.RegistraResultadoPagoEntrante(entradaServicio, xmlData); };
            Thread LystoHilo = new Thread(LystoDelegado);
            LystoHilo.Name = "MonibyteHiloSinpe";
            LystoHilo.Start();

        }
    }
}



﻿using Monibyte.Arquitectura.Dominio;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public interface IGeoLocalizacion : IAplicacionBase
    {
        void GuardarGeoLocalizacion();
    }
}

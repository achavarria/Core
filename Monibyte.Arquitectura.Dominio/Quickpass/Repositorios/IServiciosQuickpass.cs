﻿using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Quickpass.Repositorios
{
    public interface IServiciosQuickpass
    {
        List<QP_Quickpass> ImportarListaQuickpass(string documento, string entidad);
        string ImportarMovimientosQuickpass(string entidad, DateTime fecInicio, DateTime fecFinal, int montoMaximo);
        bool EnviarListasQP(string emisor, int tipoLista, List<string> listaQP, DateTime fechaGeneracion, int secGeneracion);
        string ResultadoListasDeTags(string entidad, DateTime fechaGeneracion, int tipoLista, int secuencia);
    }
}

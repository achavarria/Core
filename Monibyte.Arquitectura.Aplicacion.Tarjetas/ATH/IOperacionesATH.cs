﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH
{
    public interface IOperacionesATH : IAplicacionBase
    {
        PocSalidaConsultaTarjeta ConsultaTarjeta(PocEntradaConsultaTarjeta pocEntrada);
        PocSalidaPagoTarjeta PagoTarjeta(PocEntradaPagoTarjeta pocEntrada);
        PocSalidaPagoTarjeta ReversaPagoTarjeta(PocEntradaPagoTarjeta pocEntrada);
        PocSalidaPagoTarjeta PagoEspecialTarjeta(PocEntradaPagoTarjeta pocEntrada);
        PocSalidaPagoTarjeta ReversaPagoEspecialTarjeta(PocEntradaPagoTarjeta pocEntrada);
        PocSalidaActualizaTarjeta ActualizaEstadoTarjeta(PocEntradaActualizaTarjeta pocEntrada);
        PocSalidaActualizaLimiteTarjeta ActualizaLimiteCuenta(PocEntradaActualizaLimiteTarjeta pocEntrada);
        PocSalidaActualizaLimiteTarjeta ActualizaLimiteTarjeta(PocEntradaActualizaLimiteTarjeta pocEntrada);
        PocSalidaActivaTarjeta ActivaTarjeta(PocEntradaActivaTarjeta pocEntrada);
        PocSalidaReposicionPinTarjeta ReposicionPinTarjeta(PocEntradaReposicionPinTarjeta pocEntrada);
        IEnumerable<PocSalidaMovimientosTarjeta> MovimientosTarjeta(PocEntradaMovimientosTarjeta pocEntrada);
        IEnumerable<PocSalidaMovimientosTarjeta> MovimientosTransitoTarjeta
            (PocEntradaMovimientosTarjeta pocEntrada, bool metodoSimple = false);
        IEnumerable<PocSalidaLealtadTarjeta> ConsultaProgLealtadTarjeta(PocEntradaLealtadTarjeta pocEntrada);
        PocSalidaAplicaMovLealtadTarjeta AplicaMovProgLealtadTarjeta(PocEntradaAplicMovLealtadTarjeta pocEntrada);
        IEnumerable<PocSalidaConsultaCuentas> ConsultaCuentasDeCliente(PocEntradaConsultaCuentas pocEntrada);
        PocSalidaMiscelaneosTarjeta AplicaMiscelaneosTarjeta(PocEntradaMiscelaneosTarjeta pocEntrada);
        PocSalidaAdelantoEfectivoTarjeta AdelantoEfectivoTarjeta(PocEntradaAdelantoEfectivoTarjeta pocEntrada);
        PocSalidaAdelantoEfectivoTarjeta ReversaAdelantoEfectivoTarjeta(PocEntradaAdelantoEfectivoTarjeta pocEntrada);
        PocSalidaInclusionSeguroTarjeta InclusionSeguroTarjeta(PocEntradaInclusionSeguroTarjeta pocEntrada);
        PocSalidaCargoAutomaticoTarjeta GeneraCargoAutomaticoTarjeta(PocEntradaCargoAutomaticoTarjeta pocEntrada);
        PocParametrosVip ActivarVip(PocParametrosVip paramsVip);
        PocParametrosVip InactivarVip(PocParametrosVip paramsVip);
        PocSalidaPagoTarjeta PagarExtraFinanciamiento(PocEntradaPagoTarjeta pocEntrada);
        PocSalidaPagoTarjeta ReversaPagoExtraFinanciamiento(PocEntradaPagoTarjeta pocEntrada);
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Dominio.Notificacion;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Notificacion.Repositorios
{
    public class RepositorioPlantillaNotificacion : RepositorioMnb<NT_PlantillaNotificacion>,
        IRepositorioPlantillaNotificacion
    {
        public RepositorioPlantillaNotificacion(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public NT_PlantillaNotificacion ObtenerPlantillaNot(int idPlantilla)
        {
            return this.TableSet.FirstOrDefault(c => c.IdPlantilla == idPlantilla);
        }

        public NT_PlantillaNotificacion ObtenerPlantillaNot
            (int idContexto, int idValorContexto)
        {
            var idIdioma = InfoSesion.Lang.Id;
            return ObtenerPlantillaNot(idContexto, idValorContexto, idIdioma);
        }

        public NT_PlantillaNotificacion ObtenerPlantillaNot(int idContexto,
            int idValorContexto, int idIdioma, string subContexto = null)
        {
            var query = this.TableSet.Where(c =>
                    c.IdContexto == idContexto &&
                    c.IdValorContexto == idValorContexto &&
                    (subContexto == null || c.SubContexto == subContexto))
                .OrderBy(c => c.IdIdioma == idIdioma ? 1 : 2);
            var notificacion = query.FirstOrDefault();
            if (notificacion == null)
            {
                idValorContexto = 0;
                notificacion = query.FirstOrDefault();
            }
            return notificacion;
        }
    }
}

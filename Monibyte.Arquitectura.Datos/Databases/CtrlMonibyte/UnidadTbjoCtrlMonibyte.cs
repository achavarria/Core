﻿using Monibyte.Arquitectura.Datos.Nucleo;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte
{
    public interface IUnidadTbjoCtrlMonibyte : IUnidadTbjoOperaciones { }

    public class UnidadTbjoCtrlMonibyte : UnidadTbjoBase, IUnidadTbjoCtrlMonibyte
    {
        public UnidadTbjoCtrlMonibyte(DbContextCtrlMonibyte ctx) : base(ctx) { }
    }
}

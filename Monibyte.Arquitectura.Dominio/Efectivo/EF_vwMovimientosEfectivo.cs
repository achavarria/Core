using System;

namespace Monibyte.Arquitectura.Dominio.Efectivo
{
    public partial class EF_vwMovimientosEfectivo : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public Nullable<int> IdMoneda { get; set; }
        public int IdEmpresa { get; set; }
        public string DetalleMovimiento { get; set; }
        public System.DateTime FecMovimiento { get; set; }
        public Nullable<decimal> MonMovimiento { get; set; }
        public Nullable<decimal> MonMovimientoLocal { get; set; }
        public Nullable<decimal> MonMovimientoInter { get; set; }
        public string NumFactura { get; set; }
        public int IdContexto { get; set; }
        public string NombreCompleto { get; set; }
        public string Moneda { get; set; }
        public string CodMoneda { get; set; }
        public string Simbolo { get; set; }
        public string CamposDinamicos { get; set; }
    }
}

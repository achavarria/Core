using Monibyte.Arquitectura.Dominio.Autorizador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador
{
    public class MB_MccTarjetaMap : EntityTypeConfiguration<MB_MccTarjeta>
    {
        public MB_MccTarjetaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMcc);

            // Properties
            this.Property(t => t.IdMcc)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodMccDesde)
                .IsRequired()
                .HasMaxLength(6);

            this.Property(t => t.CodMccHasta)
                .IsRequired()
                .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("MB_MccTarjeta", "Mnb");
            this.Property(t => t.IdMcc).HasColumnName("IdMcc");
            this.Property(t => t.IdCondicionTarjeta).HasColumnName("IdCondicionTarjeta");
            this.Property(t => t.IdCategoriaMcc).HasColumnName("IdCategoriaMcc");
            this.Property(t => t.CodMccDesde).HasColumnName("CodMccDesde");
            this.Property(t => t.CodMccHasta).HasColumnName("CodMccHasta");
            this.Property(t => t.LimiteConsumo).HasColumnName("LimiteConsumo");
            this.Property(t => t.CantidadConsumo).HasColumnName("CantidadConsumo");
            this.Property(t => t.SaldoLimiteConsumo).HasColumnName("SaldoLimiteConsumo");
            this.Property(t => t.SaldoCantidadConsumo).HasColumnName("SaldoCantidadConsumo");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdSoloCategoria).HasColumnName("IdSoloCategoria");
            this.Property(t => t.FecUltMiscelaneo).HasColumnName("FecUltMiscelaneo");
            this.Property(t => t.IdPeriodicidad).HasColumnName("IdPeriodicidad");
            this.Property(t => t.LimiteConsumoInter).HasColumnName("LimiteConsumoInter");
            this.Property(t => t.SaldoLimiteConsumoInter).HasColumnName("SaldoLimiteConsumoInter");

        }
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;

namespace Monibyte.Arquitectura.Dominio.Cargos
{
    public partial class CA_Movimientos : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public int IdCargo { get; set; }
        public int IdTipoCargo { get; set; }
        public int IdTipoMovimiento { get; set; }
        public string CodMovimiento { get; set; }
        public int IdTarjeta { get; set; }
        public System.DateTime FecMovimiento { get; set; }
        public int IdMoneda { get; set; }
        public decimal Monto { get; set; }
        public string Detalle { get; set; }
        public string NumReferencia { get; set; }
        public string NumRefProcesador { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

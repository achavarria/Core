﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Cargos;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public interface IAppCargosGeolocalizacion : IAplicacionBase
    {
        void GuardarCargosGeo(PocRegistrarPlacas entrada, List<PocCargosPlaca> modeloBase);
        PocRegistrarPlacas ObtieneRegistroCargosGeo(int idEmpresa);
        void CambiarEstadoGeo(int idEmpresa, int idEstado);
    }
}
using Monibyte.Arquitectura.Dominio.Gestiones;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_ParametroEmpresa : EntidadBase
    {
        public int IdParametro { get; set; }
        public int IdEmpresa { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int IdTipo { get; set; }
        public int IdOrdenadoPor { get; set; }
        public int Ascendente { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdParametroInterno { get; set; }
        public Nullable<int> IdDependeDe { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.TEF;

namespace Monibyte.Arquitectura.Aplicacion.TEF.Mantenimientos
{
    public interface IAppOpCedulaRegistrada : IAplicacionBase
    {
        void Insertar(PocCedulaRegistrada registro);
        void Eliminar(PocCedulaRegistrada registro);
        void Modificar(PocCedulaRegistrada registro);
        DataResult<PocCedulaRegistrada> Listar(PocCedulaRegistrada filtro, DataRequest request);
    }
}

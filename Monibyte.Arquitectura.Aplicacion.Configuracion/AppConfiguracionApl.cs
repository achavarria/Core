﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Configuracion.Repositorios;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Logs;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Configuracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public class AppConfiguracionApl : AdmCondicion, IAppConfiguracionApl
    {
        protected override int TipoAutorizacion
        {
            get { return (int)EnumTipoAutorizacion.RestriccionesGrupo; }
        }

        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<MB_GruposEmpresa> _repGruposEmpresa;
        private IRepositorioPlantilla _repPlantilla;
        private IRepositorioMnb<MB_MccPlantilla> _repMccPlantilla;
        private IRepositorioMnb<MB_HorarioPlantilla> _repHorarioPlantilla;
        private IRepositorioMnb<MB_RegionPaisPlantilla> _repRegionPaisPlantilla;

        private IRepositorioMnb<GL_Pais> _repPais;
        private IRepositorioMnb<MB_Region> _repRegion;
        private IRepositorioTipoComercio _repTipoComercio;
        private IRepositorioMnb<MB_CategoriaTipoComercio> _repCategoriaTipoComercio;

        private IRepositorioMnb<LG_BitacoraPanelControl> _repBitacoraPanelControl;

        private IFuncionesConfiguracion _funcConf;
        private IRepositorioMnb<GL_Entidad> _repEntidad;

        public AppConfiguracionApl(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<MB_GruposEmpresa> repGruposEmpresa,
            IRepositorioPlantilla repPlantilla,
            IRepositorioMnb<MB_MccPlantilla> repMccPlantilla,
            IRepositorioMnb<MB_HorarioPlantilla> repHorarioPlantilla,
            IRepositorioMnb<MB_RegionPaisPlantilla> repRegionPaisPlantilla,
            IRepositorioMnb<GL_Pais> repPais,
            IRepositorioMnb<MB_Region> repRegion,
            IRepositorioTipoComercio repTipoComercio,
            IRepositorioMnb<LG_BitacoraPanelControl> repBitacoraPanelControl,
            IRepositorioMnb<MB_CategoriaTipoComercio> repCategoriaTipoComercio,
            IFuncionesConfiguracion funcConf,
            IRepositorioMnb<GL_Entidad> repEntidad)
        {
            _repCatalogo = repCatalogo;
            _repGruposEmpresa = repGruposEmpresa;
            _repPlantilla = repPlantilla;
            _repMccPlantilla = repMccPlantilla;
            _repHorarioPlantilla = repHorarioPlantilla;
            _repRegionPaisPlantilla = repRegionPaisPlantilla;
            _repPais = repPais;
            _repRegion = repRegion;
            _repTipoComercio = repTipoComercio;
            _repBitacoraPanelControl = repBitacoraPanelControl;
            _repCategoriaTipoComercio = repCategoriaTipoComercio;
            _funcConf = funcConf;
            _repEntidad = repEntidad;
        }

        public void EliminarPlantilla(int? idPlantilla)
        {
            if (!idPlantilla.HasValue) { return; }
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    //Elimina horarios asociados
                    _repHorarioPlantilla.DeleteOn(p => p.IdPlantilla == idPlantilla);
                    //Elimina mcc asociados
                    _repMccPlantilla.DeleteOn(p => p.IdPlantilla == idPlantilla);
                    //Elimina regiones asociadas
                    _repRegionPaisPlantilla.DeleteOn(p => p.IdPlantilla == idPlantilla);
                    //Elimina la plantilla
                    _repPlantilla.DeleteOn(p => p.IdPlantilla == idPlantilla);
                    //Salvar los cambios
                    _repPlantilla.UnidadTbjo.Save();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                ts.Complete();
            }
        }

        public int? DuplicarPlantilla(PocCondicionDuplica poco, int idGrupo)
        {
            var validation = new ValidationResponse();
            if (poco == null)
            {
                validation.AddError("Err_0035");
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Error al duplicar plantilla", validation: validation);
            }
            InfoSesion.IncluirSesion("BUSCARAUTORIZACION", true);
            var plantilla = ObtenerCondicion(poco.IdCondicion);
            plantilla.IdCondicion = idGrupo;
            plantilla.Descripcion = poco.NombreNuevo;
            return EstablecerCondicion(plantilla, esNuevoGrupo: true);
        }

        public void CambioEstadoPlantilla(int idPlantilla, int idEstado)
        {
            if (idEstado == (int)EnumIdEstado.Activo)
            {
                _repPlantilla.UpdateOn(x => x.IdPlantilla == idPlantilla, z => new MB_Plantilla
                {
                    IdEstado = (int)EnumIdEstado.Inactivo
                });
            }
            else
            {
                _repPlantilla.UpdateOn(x => x.IdPlantilla == idPlantilla, z => new MB_Plantilla
                {
                    IdEstado = (int)EnumIdEstado.Activo
                });
            }
        }

        public List<PocCondicion> ObtenerPlantillasEmpresa(int idEmpresa)
        {
            var r = (from p in _repPlantilla.Table
                     join c in _repCatalogo.List("LACTINACTIVA") on
                         p.IdEstado equals c.IdCatalogo
                     where p.IdEmpresa == idEmpresa
                     select new
                     {
                         Valores = p,
                         Estado = c.Descripcion
                     }).ToList();
            var resultado = r.Select(x =>
            {
                var cond = x.Valores.ProyectarComo<PocCondicion>();
                cond.DescripcionEstado = x.Estado;
                return cond;
            }).ToList();
            return resultado;
        }

        public List<PocCondicion> ObtenerPlantillasDisponibles(int idEmpresa)
        {
            var plantilla = from pln in _repPlantilla.Table
                            where pln.IdEmpresa == idEmpresa
                            orderby pln.Descripcion
                            select new PocCondicion
                            {
                                IdCondicion = pln.IdPlantilla,
                                Descripcion = pln.Descripcion
                            };
            return plantilla.ToList();
        }


        #region metodos heredados y sobreescritos

        //registro en bd y validaciones
        protected override void SalvarCambios()
        {
            //Salvar la configuracion de la plantilla
            _repPlantilla.UnidadTbjo.Save();
        }

        protected override void SalvarBitacora(PocCondicion condicion, MultilanguageTopic tracker, bool notificaCambioConfig)
        {
            //Salva la bitacora de la configuracion
            var _topic = InfoSesion.ObtenerSesion<MultilanguageTopic>("TOPICTRACK");
            var _bitacora = new LG_BitacoraPanelControl
            {
                IdCondicion = condicion.IdCondicion,
                DatosBitacora = tracker.ToJson().CompressStr()
            };
            _repBitacoraPanelControl.Insert(_bitacora);
            _repBitacoraPanelControl.UnidadTbjo.Save();
        }

        protected override void ValidarCondicion(PocCondicion condicion, bool update = false)
        {
            var validation = new ValidationResponse();
            if (!update)
            {
                if (_repPlantilla.ExistePlantilla(condicion.Descripcion,
                    InfoSesion.Info.IdEmpresa))
                {
                    validation.AddError("Err_0038", condicion.Descripcion);
                }
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Error al establecer la plantilla", validation: validation);
            }
        }

        //consultas
        protected override PocCondicion ObtenerCondicionPrincipal(object criterioBusqueda)
        {
            var idGrupo = criterioBusqueda as int?;
            var query = _repGruposEmpresa.Table
                .Join(_repPlantilla.Table,
                    gr => gr.IdGrupo,
                    pl => pl.IdPlantilla,
                    (gr, pl) => new { gr, pl })
                .FirstOrDefault(m => m.gr.IdGrupo == idGrupo);
            if (query != null)
            {
                var config = query.pl.ProyectarComo<PocCondicion>();
                config.LimiteCredito = query.gr.LimiteCredito;
                return config;
            }
            return null;
        }

        protected override List<PocMccCondicion> ObtenerMccs(int idCondicion,
                                        int? idMoneda, bool esActualizar = false)
        {
            var entidad = _repEntidad.SelectById(InfoSesion.ObtenerSesion<int>("IDPROCESADOR"));
            var esLocal = entidad.IdMonedaLocal.Value == idMoneda;

            var criteria = new Criteria<MB_MccPlantilla>();
            criteria.And(m => m.IdPlantilla == idCondicion);
            var resulMcc =
                from mcc in _repMccPlantilla.SelectBy(criteria)
                join cat in _repCategoriaTipoComercio.Table
                    on mcc.IdCategoriaMcc equals cat.IdCategoria
                join per in _repCatalogo.List("LPERIODICIDAD")
                    on mcc.IdPeriodicidad equals per.IdCatalogo
                join tip in _repTipoComercio.Table
                    on new
                    {
                        CodMcc = mcc.CodMccDesde,
                        mcc.IdSoloCategoria
                    } equals new
                    {
                        tip.CodMcc,
                        IdSoloCategoria = (int)EnumIdSiNo.No
                    } into tipCom
                from tip in tipCom.DefaultIfEmpty()
                where (mcc.IdEstado == (int)EnumIdEstado.Activo &&
                    cat.IdEstado == (int)EnumIdEstado.Activo &&
                    (mcc.IdSoloCategoria == (int)EnumIdSiNo.No ||
                        (mcc.IdSoloCategoria == (int)EnumIdSiNo.Si &&
                            mcc.CodMccDesde == _repMccPlantilla.Table//.SelectBy(criteria)
                            .Where(com =>
                                com.IdPlantilla == idCondicion &&
                                com.IdCategoriaMcc == mcc.IdCategoriaMcc)
                            .Min(m => m.CodMccDesde))))
                select new PocMccCondicion
                {
                    IdCondicion = mcc.IdPlantilla,
                    IdCategoriaMcc = cat.IdCategoria,
                    IdMcc = mcc.IdMcc,
                    IdEstado = mcc.IdEstado,
                    LimiteConsumo = esLocal || esActualizar ? mcc.LimiteConsumo : mcc.LimiteConsumoInter,
                    LimiteConsumoInter = mcc.LimiteConsumoInter,
                    SaldoLimiteConsumo = esLocal || esActualizar ? mcc.LimiteConsumo : mcc.LimiteConsumoInter,
                    SaldoLimiteConsumoInter = mcc.LimiteConsumoInter,
                    CantidadConsumo = mcc.CantidadConsumo,
                    SaldoCantidadConsumo = mcc.CantidadConsumo,
                    CodMcc = tip != null ? tip.CodMcc : null,
                    DescripcionMcc =
                        (tip != null ? tip.CodMcc : null) + " - " +
                        (tip != null ? tip.Descripcion : null),
                    DescripcionCategoria = _fGenerales.Traducir(cat.Descripcion,
                        "MB_CategoriaTipoComercio", cat.IdCategoria,
                        Context.Lang.Id, 0, Context.DefaultLang.Id),
                    DescripcionPeriodicidad = per.Descripcion,
                    IdPeriodicidad = mcc.IdPeriodicidad,
                    IdSoloCategoria = mcc.IdSoloCategoria
                };
            return resulMcc.ToList();
        }

        protected override List<PocHorarioCondicion> ObtenerHorarios(int idCondicion)
        {
            var criteria = new Criteria<MB_HorarioPlantilla>();
            criteria.And(m => m.IdPlantilla == idCondicion);
            var restHorario = _repHorarioPlantilla
                .SelectBy(criteria)
                .GroupJoin(_repCatalogo.List("LDIASSEMANA"),
                   d => d.IdDia.ToString(),
                   c => c.Codigo,
                   (d, c) => new { d, c })
                .SelectMany(res => res.c.DefaultIfEmpty(), (res, b) => new PocHorarioCondicion
                {
                    IdCondicion = res.d.IdPlantilla,
                    IdDia = res.d.IdDia,
                    IdHorario = res.d.IdHorario,
                    HoraDesde = res.d.HoraDesde,
                    HoraHasta = res.d.HoraHasta,
                    IdRestriccion = res.d.IdRestriccion,
                    IdEstado = res.d.IdEstado,
                    DescripcionDia = res.d.IdDia != 0 ? b.Descripcion : null
                });
            return restHorario.ToList();
        }

        protected override List<PocRegionPaisCondicion> ObtenerRegionesPaises(int idCondicion)
        {
            var criteria = new Criteria<MB_RegionPaisPlantilla>();
            criteria.And(m => m.IdPlantilla == idCondicion);
            var restRegionPais = _repRegionPaisPlantilla.SelectBy(criteria)
                .GroupJoin(_repRegion.Table,
                    cond => cond.IdRegion,
                    region => region.IdRegion,
                    (condicion, regiones) => new
                    {
                        condicion,
                        region = regiones.FirstOrDefault()
                    })
                .GroupJoin(_repPais.Table,
                    rep => rep.condicion.IdPais,
                    pais => pais.IdPais,
                    (condicion, paises) => new
                    {
                        condicion = condicion.condicion,
                        region = condicion.region,
                        pais = paises.FirstOrDefault()
                    })
                .Select(res => new PocRegionPaisCondicion
                {
                    IdRegionPais = res.condicion.IdRegionPais,
                    IdCondicion = res.condicion.IdPlantilla,
                    IdEstado = res.condicion.IdEstado,
                    IdPais = res.pais != null ? (int?)res.pais.IdPais : null,
                    IdRegion = res.region != null ? (int?)res.region.IdRegion : null,
                    DescripcionPais = res.pais != null ? res.pais.Descripcion : string.Empty,
                    DescripcionRegion = res.region != null ? res.region.Descripcion : string.Empty
                });

            return restRegionPais.ToList();
        }

        //registros nuevos
        protected override int? InsertarCondicionPrincipal(PocCondicion condicion)
        {
            var entradaPlantilla = condicion.ProyectarComo<MB_Plantilla>();
            entradaPlantilla.IdPlantilla = condicion.IdCondicion;
            entradaPlantilla.IdEstado = (int)EnumIdEstado.Activo;
            _repPlantilla.Insert(entradaPlantilla);
            _repPlantilla.UnidadTbjo.Save();
            return entradaPlantilla.IdPlantilla;
        }

        protected override void InsertarMcc(PocMccCondicion item)
        {
            var mcc = item.ProyectarComo<MB_MccPlantilla>();
            mcc.IdEstado = (int)EnumIdEstado.Activo;
            mcc.IdMcc = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdMccPlantilla");
            mcc.IdSoloCategoria = item.CodMcc == this.DefaultMcc ?
                (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
            _repMccPlantilla.Insert(mcc);
        }

        protected override void InsertarHorario(PocHorarioCondicion item)
        {
            var hrs = item.ProyectarComo<MB_HorarioPlantilla>();
            hrs.IdHorario = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdHorarioPlantilla");
            hrs.IdEstado = (int)EnumIdEstado.Activo;
            hrs.IdRestriccion = null;
            _repHorarioPlantilla.Insert(hrs);
        }

        protected override void InsertaVigencia(int idCondicion, EnumTipoRestriccion tipoRestriccion, DateTime horaDesde, DateTime horaHasta)
        {
            MB_HorarioPlantilla mb = new MB_HorarioPlantilla();
            mb.IdHorario = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdHorarioPlantilla");
            mb.IdDia = 0;
            mb.IdEstado = (int)EnumIdEstado.Activo;
            mb.IdPlantilla = idCondicion;
            mb.IdRestriccion = (int)tipoRestriccion;
            mb.HoraDesde = horaDesde;
            mb.HoraHasta = horaHasta;
            _repHorarioPlantilla.Insert(mb);
        }

        protected override void InsertarRegionPais(PocRegionPaisCondicion item)
        {
            var regionPais = item.ProyectarComo<MB_RegionPaisPlantilla>();
            regionPais.IdRegionPais = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdRegionPaisPlantilla");
            regionPais.IdEstado = (int)EnumIdEstado.Activo;
            _repRegionPaisPlantilla.Insert(regionPais);
        }

        //modificar registros
        protected override int? ModificarCondicionPrincipal(PocCondicion condicion)
        {
            var entradaPlantilla = condicion.ProyectarComo<MB_Plantilla>();
            entradaPlantilla.IdEstado = (int)EnumIdEstado.Activo;
            _repPlantilla.UpdateExclude(entradaPlantilla, p => p.FecInclusionAud, p => p.IdUsuarioIncluyeAud);
            return entradaPlantilla.IdPlantilla;
        }

        protected override void ModificarMcc(PocMccCondicion item)
        {
            _repMccPlantilla.UpdateOn(m => m.IdCategoriaMcc == item.IdCategoriaMcc &&
                m.IdPlantilla == item.IdCondicion,
                up => new MB_MccPlantilla
                {
                    LimiteConsumo = item.LimiteConsumo,
                    LimiteConsumoInter = item.LimiteConsumoInter.Value,
                    CantidadConsumo = item.CantidadConsumo,
                    IdPeriodicidad = item.IdPeriodicidad
                });
        }

        protected override void ModificarHorario(PocHorarioCondicion item)
        {
            var horario = item.ProyectarComo<MB_HorarioPlantilla>();
            _repHorarioPlantilla.Update(horario, m => m.HoraDesde, m => m.HoraHasta);
        }

        protected override void ModificarVigencia(PocHorarioCondicion item)
        {
            var horario = item.ProyectarComo<MB_HorarioPlantilla>();
            _repHorarioPlantilla.UpdateOn(m => m.IdPlantilla == item.IdCondicion && m.IdDia == 0 &&
                m.IdRestriccion == item.IdRestriccion,
                up => new MB_HorarioPlantilla
                {
                    HoraDesde = item.HoraDesde,
                    HoraHasta = item.HoraHasta
                });
        }

        protected override void ModificarRegionPais(PocRegionPaisCondicion item)
        {
            var regionPais = item.ProyectarComo<MB_RegionPaisPlantilla>();
            _repRegionPaisPlantilla.Update(regionPais, m => m.IdRegion, m => m.IdPais);
        }

        protected override void EliminarMcc(PocMccCondicion item)
        {
            _repMccPlantilla.DeleteOn(m => m.IdCategoriaMcc == item.IdCategoriaMcc &&
                m.IdPlantilla == item.IdCondicion);
        }

        protected override void EliminarHorario(PocHorarioCondicion item)
        {
            _repHorarioPlantilla.DeleteOn(m => m.IdDia == item.IdDia && m.IdDia != 0 &&
                m.IdPlantilla == item.IdCondicion);
        }

        protected override void EliminarVigencia(PocHorarioCondicion item)
        {
            _repHorarioPlantilla.DeleteOn(m => m.IdPlantilla == item.IdCondicion && m.IdDia == 0 &&
                m.IdRestriccion == item.IdRestriccion);
        }

        protected override void EliminarRegionPais(PocRegionPaisCondicion item)
        {
            _repRegionPaisPlantilla.DeleteOn(m => m.IdRegionPais == item.IdRegionPais &&
                m.IdPlantilla == item.IdCondicion);
        }

        protected override bool RestriccionVigente(ParamRestriccionVigente parameters)
        {
            return _funcConf.RestriccionVigente(parameters);
        }

        #endregion
    }
}

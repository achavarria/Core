﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Clientes;
using Monibyte.Arquitectura.Dominio.Clientes;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public virtual DbSet<CL_Direccion> CL_Direccion { get; set; }
        public virtual DbSet<CL_Ejecutivo> CL_Ejecutivo { get; set; }
        public virtual DbSet<CL_Profesion> CL_Profesion { get; set; }
        public virtual DbSet<CL_Comunicacion> CL_Comunicacion { get; set; }
        public virtual DbSet<CL_Persona> CL_Persona { get; set; }
        public virtual DbSet<CL_EmpresasUsuario> CL_EmpresasUsuario { get; set; }
        public virtual DbSet<CL_PerfilEmpresa> CL_PerfilEmpresa { get; set; }
        public virtual DbSet<CL_vwEmpresasUsuario> CL_vwEmpresasUsuario { get; set; }

        void CreateContextModelCL(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CL_ComunicacionMap());
            modelBuilder.Configurations.Add(new CL_DireccionMap());
            modelBuilder.Configurations.Add(new CL_EjecutivoMap());
            modelBuilder.Configurations.Add(new CL_EmpresasUsuarioMap());
            modelBuilder.Configurations.Add(new CL_PerfilEmpresaMap());
            modelBuilder.Configurations.Add(new CL_PersonaMap());
            modelBuilder.Configurations.Add(new CL_ProfesionMap());
            modelBuilder.Configurations.Add(new CL_vwEmpresasUsuarioMap());
        }
    }
}

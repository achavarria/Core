﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Generales.Repositorios
{
    public class RepositorioCampoContexto : RepositorioMnb<GL_CamposContexto>, IRepositorioCampoContexto
    {
        IFuncionesGenerales _fGenerales;
        public RepositorioCampoContexto(
            IUnidadTbjoMonibyte unidadTbjo,
            IFuncionesGenerales fGenerales)
            : base(unidadTbjo)
        {
            _fGenerales = fGenerales;
        }

        public IQueryable<GL_CamposContexto> List(int idContexto)
        {
            var criteria = new Criteria<GL_CamposContexto>();
            criteria.And(c => c.IdContexto == idContexto);
            return List(criteria);
        }

        public IQueryable<GL_CamposContexto> List(Criteria<GL_CamposContexto> criteria)
        {
            return this.Table
                .Where(criteria.Satisfecho())
                .Select(c => new GL_CamposContextoWrapper
                {
                    IdCampo = c.IdCampo,
                    IdContexto = c.IdContexto,
                    Campo = c.Campo,
                    IdTipo = c.IdTipo,
                    SQLOrigen = c.SQLOrigen,
                    CampoRelacionado = c.CampoRelacionado,
                    Referencia1 = c.Referencia1,
                    Referencia2 = c.Referencia2,
                    Referencia3 = c.Referencia3,
                    Referencia4 = c.Referencia4,
                    Referencia5 = c.Referencia5,
                    Descripcion = _fGenerales.Traducir(c.Descripcion, "GL_CAMPOSCONTEXTO", 
                        c.IdCampo, Context.Lang.Id, 0, Context.DefaultLang.Id),                    
                    FecInclusionAud = c.FecInclusionAud,
                    IdUsuarioIncluyeAud = c.IdUsuarioIncluyeAud,
                    FecActualizaAud = c.FecActualizaAud,
                    IdUsuarioActualizaAud = c.IdUsuarioActualizaAud
                });
        }
    }
    class GL_CamposContextoWrapper : GL_CamposContexto { }
}
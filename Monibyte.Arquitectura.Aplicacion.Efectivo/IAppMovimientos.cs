﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Efectivo;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Efectivo
{
    public interface IAppMovimientos : IAplicacionBase
    {
        void Insertar(PocMovimientosEfectivo registro);
        void Modificar(PocMovimientosEfectivo registro);
        void Eliminar(PocMovimientosEfectivo registro);
        PocMovimientosEfectivo Obtener(PocMovimientosEfectivo filtro);
        DataResult<PocMovimientosEfectivo> ListarMovEfectivo(PocMovimientosEfectivo filtro, DataRequest request, DateTime? fecDesde, DateTime? fecHasta);
        DataResult<PocMovimientosEfectivo> ListarMovKilometraje(PocMovimientosEfectivo filtro, DataRequest request, DateTime? fecDesde, DateTime? fecHasta);
        DataResult<PocArchivoMovimiento> ListarArchivoMovimiento(PocArchivoMovimiento filtro, DataRequest request);
        void EliminaArchivoMovimiento(PocArchivoMovimiento registro);
        byte[] AbrirArchivoMovimiento(PocArchivoMovimiento registro);
        int? ExisteArchivoMovimiento(int idMovimiento, string nombreArchivo);
        DataResult<PocTarifaKilometraje> ObtenerTarifas(DataRequest request, int idEmpresa);
        void ModificarTarifa(IEnumerable<PocTarifaKilometraje> tarifas);
        void EliminarTarifa(IEnumerable<PocTarifaKilometraje> tarifas);
        void CrearTarifa(IEnumerable<PocTarifaKilometraje> tarifas, int idEmpresa);
        int? ExisteArchivoTarjeta(int idMovimiento, string nombreArchivo);
        DataResult<PocArchivoTarjeta> ListarArchivoMovimientoTc(PocArchivoTarjeta filtro, DataRequest request);
        void EliminaArchivoMovimientoTc(PocArchivoTarjeta registro);
        byte[] AbrirArchivoMovimientoTc(PocArchivoTarjeta registro);
    }
}

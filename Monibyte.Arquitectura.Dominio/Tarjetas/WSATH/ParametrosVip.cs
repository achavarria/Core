﻿using System;
namespace Monibyte.Arquitectura.Dominio.Tarjetas.WSATH
{
    public class EntradaVip
    {
        public string NumCuenta { get; set; }   
        public string NumTarjeta { get; set; }
        public string LocalidadTramite { get; set; }
        public bool ValidaVigencia { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Contabilidad;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Contabilidad;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Contabilidad
{
    public class AppParametroContabilidad : AplicacionBase, IAppParametroContabilidad
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CG_Parametro> _repParametro;

        public AppParametroContabilidad(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CG_Parametro> repParametro)
        {
            _repCatalogo = repCatalogo;
            _repParametro = repParametro;
        }

        public void Eliminar(PocParametro registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CG_Parametro>();
                _repParametro.Delete(entrada);
                _repParametro.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocParametro registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CG_Parametro>();
                    _repParametro.Insert(entrada);
                    _repParametro.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocParametro));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocParametro registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CG_Parametro>();
                    _repParametro.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repParametro.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocParametro));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocParametro> Listar(PocParametro filtro, DataRequest request)
        {
            var criteria = new Criteria<CG_Parametro>();
            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.Descripcion))
                {
                    criteria.And(m => m.Descripcion.Contains(filtro.Descripcion));
                }
            }

            var resultado = _repParametro
                .SelectBy(criteria)
                .Join(_repCatalogo.List("LTIPOPARAMETROCONTA"),
                    p => p.IdTipoParametro,
                    c => c.IdCatalogo,
                    (p, c) => new { p, c })
                .Select(item => new PocParametro
                {
                    Descripcion = item.p.Descripcion,
                    IdParametro = item.p.IdParametro,
                    IdTipoParametro = item.p.IdTipoParametro,
                    TipoParametro = item.c.Descripcion

                });
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocParametro registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.IdParametro < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdTipoParametro < 0) { validacion.AddError("Err_0035"); }
            if (registro.Descripcion == "") { validacion.AddError("Err_0035"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }

        public IEnumerable<PocParametro> ListaCustomer()
        {
            var parametros = _repParametro.Table.Where(p => p.IdTipoParametro == (int)EnumTipoParametroContabilidad.CustomerJob).ToList();
            return parametros.ProyectarComo<PocParametro>();
        }

        public IEnumerable<PocParametro> ListaAccount()
        {
            var parametros = _repParametro.Table.Where(p => p.IdTipoParametro == (int)EnumTipoParametroContabilidad.Account).ToList();
            return parametros.ProyectarComo<PocParametro>();
        }

        public IEnumerable<PocParametro> ListaItem()
        {
            var parametros = _repParametro.Table.Where(p => p.IdTipoParametro == (int)EnumTipoParametroContabilidad.Item).ToList();
            return parametros.ProyectarComo<PocParametro>();
        }
    }
}

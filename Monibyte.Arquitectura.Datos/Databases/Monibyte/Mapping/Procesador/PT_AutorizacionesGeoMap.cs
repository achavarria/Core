﻿using Monibyte.Arquitectura.Dominio.Procesador;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Procesador
{
    public class PT_AutorizacionesGeoMap : EntityTypeConfiguration<PT_AutorizacionesGeo>
    {
        public PT_AutorizacionesGeoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.FecInclusionAud });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.MessageTypeId)
                .HasMaxLength(5);

            this.Property(t => t.CardNumber)
                .HasMaxLength(30);

            this.Property(t => t.ResponseCode)
                .HasMaxLength(5);

            this.Property(t => t.LocalTransactionDate)
                .HasMaxLength(8);

            this.Property(t => t.LocalTransactionTime)
                .HasMaxLength(20);

            this.Property(t => t.AcquiringCountryCode)
                .HasMaxLength(5);

            this.Property(t => t.TransactionCurrencyCode)
                .HasMaxLength(5);

            this.Property(t => t.MerchantCategory)
                .HasMaxLength(5);

            this.Property(t => t.CardAcceptorNameLocation)
                .HasMaxLength(60);

            this.Property(t => t.SystemTraceAuditNumber)
                .HasMaxLength(20);

            this.Property(t => t.AuthorizationIdResponse)
                .HasMaxLength(20);

            this.Property(t => t.CurrencyCodeCb)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("PT_AutorizacionesGeo", "ProcesadorTC");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.MessageTypeId).HasColumnName("MessageTypeId");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.CardNumber).HasColumnName("CardNumber");
            this.Property(t => t.ResponseCode).HasColumnName("ResponseCode");
            this.Property(t => t.LocalTransactionDate).HasColumnName("LocalTransactionDate");
            this.Property(t => t.LocalTransactionTime).HasColumnName("LocalTransactionTime");
            this.Property(t => t.AcquiringCountryCode).HasColumnName("AcquiringCountryCode");
            this.Property(t => t.TransactionCurrencyCode).HasColumnName("TransactionCurrencyCode");
            this.Property(t => t.TransactionAmount).HasColumnName("TransactionAmount");
            this.Property(t => t.MerchantCategory).HasColumnName("MerchantCategory");
            this.Property(t => t.CardAcceptorNameLocation).HasColumnName("CardAcceptorNameLocation");
            this.Property(t => t.SystemTraceAuditNumber).HasColumnName("SystemTraceAuditNumber");
            this.Property(t => t.AuthorizationIdResponse).HasColumnName("AuthorizationIdResponse");
            this.Property(t => t.CurrencyCodeCb).HasColumnName("CurrencyCodeCb");
        }
    }
}

﻿using System;

namespace Monibyte.Arquitectura.Dominio.Procesador
{
    public partial class PT_AutorizacionesGeo :EntidadBase
    {
        public int Id { get; set; }
        public string MessageTypeId { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public string CardNumber { get; set; }
        public string ResponseCode { get; set; }
        public string LocalTransactionDate { get; set; }
        public string LocalTransactionTime { get; set; }
        public string AcquiringCountryCode { get; set; }
        public string TransactionCurrencyCode { get; set; }
        public Nullable<decimal> TransactionAmount { get; set; }
        public string MerchantCategory { get; set; }
        public string CardAcceptorNameLocation { get; set; }
        public string SystemTraceAuditNumber { get; set; }
        public string AuthorizationIdResponse { get; set; }
        public string CurrencyCodeCb { get; set; }
    }
}

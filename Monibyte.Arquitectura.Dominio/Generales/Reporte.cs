﻿using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public class Reporte
    {
        public string MimeType { get; set; }
        public string Encoding { get; set; }
        public string Extension { get; set; }
        public byte[] Output { get; set; }
    }
    public class ReportConfig
    {
        public string ReportId { get; set; }
        public string ReportTitle { get; set; }
        public string SubTitle { get; set; }
        public string ReportFormat { get; set; }
        public bool Landscape { get; set; }
        public bool InsertSignature { get; set; }
        public List<ReportParam> Parameters { get; set; }
    }


    public class ReportColumn
    {
        public string Name { get; set; }
        public string HeaderName { get; set; }
        public string Width { get; set; }
        public int Order { get; set; }
        public string FieldType { get; set; }
        public bool CanGrow { get; set; }
        public bool Group { get; set; }
        public bool Sum { get; set; }
    }
    public class ReportDataSet
    {
        public string Name { get; set; }
        public bool IsDetail { get; set; }
        public bool StoreProcedure { get; set; }
        public string QueryCommand { get; set; }
        public string SubReportUrl { get; set; }
        public List<ReportColumn> Columns { get; set; }
        public List<ReportParam> Parameters { get; set; }
    }

    public partial class ReportParam
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public string Label { get; set; }
    }
}

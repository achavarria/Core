using Monibyte.Arquitectura.Dominio.Quickpass;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Quickpass
{
    public class QP_ListaTagsMap : EntityTypeConfiguration<QP_ListaTags>
    {
        public QP_ListaTagsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdSecuencia, t.IdQuickpass });

            // Properties
            this.Property(t => t.IdSecuencia)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdQuickpass)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("QP_ListaTags", "Quickpass");
            this.Property(t => t.IdSecuencia).HasColumnName("IdSecuencia");
            this.Property(t => t.IdQuickpass).HasColumnName("IdQuickpass");
            this.Property(t => t.FechaGeneracion).HasColumnName("FechaGeneracion");
            this.Property(t => t.IdTipoLista).HasColumnName("IdTipoLista");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Seguridad;
using System;

namespace Monibyte.Arquitectura.Dominio.Clientes
{
    public partial class CL_EmpresasUsuario : EntidadBase
    {
        public int IdEmpresa { get; set; }
        public int IdUsuario { get; set; }
        public int IdDefault { get; set; }
        public int IdTipoUsuario { get; set; }
        public int IdRole { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdRoleOperativo { get; set; }
    }
}

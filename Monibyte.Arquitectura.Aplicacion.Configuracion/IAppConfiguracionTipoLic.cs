﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Configuracion;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public interface IAppConfiguracionTipoLic : IAplicacionBase
    {
        void Insertar(PocTipoLicencia registro);
        void Eliminar(PocTipoLicencia registro);
        void Modificar(PocTipoLicencia registro);
        DataResult<PocTipoLicencia> Listar(PocTipoLicencia filtro, DataRequest request);
        IEnumerable<PocTipoLicencia> ListadoTipoLicencia();
    }
}

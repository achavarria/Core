using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Contabilidad
{
    public partial class CG_Parametro : EntidadBase
    {
        public int IdParametro { get; set; }
        public int IdTipoParametro { get; set; }
        public string Descripcion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

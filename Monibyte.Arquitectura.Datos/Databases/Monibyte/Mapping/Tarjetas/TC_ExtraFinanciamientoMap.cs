﻿using Monibyte.Arquitectura.Dominio.Tarjetas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_ExtraFinanciamientoMap : EntityTypeConfiguration<TC_ExtraFinanciamiento>
    {
        public TC_ExtraFinanciamientoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdExtraFinanciamiento);

            // Properties
            this.Property(t => t.IdExtraFinanciamiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TC_ExtraFinanciamiento", "TCredito");
            this.Property(t => t.IdExtraFinanciamiento).HasColumnName("IdExtraFinanciamiento");
            this.Property(t => t.NumExtraFinanciamiento).HasColumnName("NumExtraFinanciamiento");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.Consecutivo).HasColumnName("Consecutivo");
            this.Property(t => t.CodigoMovimiento).HasColumnName("CodigoMovimiento");
            this.Property(t => t.TasaDeInteres).HasColumnName("TasaDeInteres");
            this.Property(t => t.PlazoEnMeses).HasColumnName("PlazoEnMeses");
            this.Property(t => t.FechaAutorizacion).HasColumnName("FechaAutorizacion");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.SaldoInicial).HasColumnName("SaldoInicial");
            this.Property(t => t.MontoCobradoInter).HasColumnName("MontoCobradoInter");
            this.Property(t => t.MontoCobradoLocal).HasColumnName("MontoCobradoLocal");
            this.Property(t => t.Debitos).HasColumnName("Debitos");
            this.Property(t => t.CuotaMensual).HasColumnName("CuotaMensual");
            this.Property(t => t.CuotasCobradas).HasColumnName("CuotasCobradas");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

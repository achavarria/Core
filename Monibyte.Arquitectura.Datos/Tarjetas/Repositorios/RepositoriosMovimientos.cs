﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Monibyte.Arquitectura.Datos.Tarjetas.Repositorios
{
    public class RepositorioVwMovTarjeta : RepositorioMnb<TC_vwMovimientosTarjeta>, IRepositorioVwMovTarjeta
    {
        public RepositorioVwMovTarjeta(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public IEnumerable<TC_vwMovimientosTarjeta> ObtenerMovimientos(ParamMovimientosTc parameters)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", parameters.IdEmpresa));

            listaParams.Add(new SqlParameter("p2", (object)parameters.IdCuenta ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p3", (object)parameters.IdTarjeta ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p4", (object)parameters.FecDesde ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p5", (object)parameters.FecHasta ?? DBNull.Value));

            var _buscarMovimientos = parameters.FiltroGestiones != null ?
                parameters.FiltroGestiones.MatchExists == true ? 1 as int? :
                parameters.FiltroGestiones.MatchExists == false ? 0 as int? : null : null;
            listaParams.Add(new SqlParameter("p6", (object)_buscarMovimientos ?? DBNull.Value));

            var _intgestiones = parameters.FiltroGestiones != null ?
                parameters.FiltroGestiones.Gestiones : null;
            var _gestiones = _intgestiones != null ? _intgestiones.ToJson() : null;
            listaParams.Add(new SqlParameter("p7", (object)_gestiones ?? DBNull.Value));

            var _intmovs = parameters.FiltroGestiones != null ?
                parameters.FiltroGestiones.IdMovimientos : null;
            var movs = _intmovs != null ? _intmovs.ToJson() : null;
            listaParams.Add(new SqlParameter("p8", (object)movs ?? DBNull.Value));

            listaParams.Add(new SqlParameter("p9", DBNull.Value));//pIdUsuario

            var resultado = (this.UnidadTbjo as ISql).ExecuteQuery<TC_vwMovimientosTarjeta>
               (@"TCredito.P_MovimientosCorte @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9",
               listaParams.ToArray());

            return resultado;
        }
    }
}

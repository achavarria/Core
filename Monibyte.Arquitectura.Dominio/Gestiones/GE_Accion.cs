using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_Accion : EntidadBase
    {
        public int IdAccion { get; set; }
        public System.DateTime FecAccion { get; set; }
        public int IdTipoAccion { get; set; }
        public string Detalle { get; set; }
        public int IdEstado { get; set; }
        public int IdGestion { get; set; }
        public Nullable<int> IdResponsable { get; set; }
        public Nullable<int> IdUsuarioRespuesta { get; set; }
        public Nullable<int> IdTipoRespuesta { get; set; }
        public Nullable<System.DateTime> FecProgramada { get; set; }
        public Nullable<System.DateTime> FecRespuesta { get; set; }
        public string DetalleRespuesta { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

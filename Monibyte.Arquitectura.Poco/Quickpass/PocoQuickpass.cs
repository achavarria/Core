﻿using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.Quickpass
{
    public class PocTipoQuickpass
    {
        public int IdTipoQuickpass { get; set; }
        public string Descripcion { get; set; }
        public int? IdPrepago { get; set; }
        public decimal MonCargaInicial { get; set; }
        public decimal MonMinRecargar { get; set; }
        public decimal MonMinListaNegra { get; set; }
        public int? IdMonedaPeaje { get; set; }
        public int? IdCargoDiario { get; set; }
        public decimal? MonCostoAdm { get; set; }
        public decimal? MonCostoUnidad { get; set; }
        public int? MesesVigencia { get; set; }
        public int? IdMonedaAdm { get; set; }
        public int? IdEstado { get; set; }
        public string Prepago { get; set; }
        public string MonedaPeaje { get; set; }
        public string MonedaAdm { get; set; }
        public string CargoDiario { get; set; }
        public string Estado { get; set; }
        public int? MesesFinanciamiento { get; set; }
    }

    public class PocObtenerQuickpass
    {
        public string NumDocumento { get; set; }
        public string CodEntidad { get; set; }
    }

    public class PocConsultaMovQp
    {
        public string CodEntidad { get; set; }
        public DateTime FecInicio { get; set; }
        public DateTime FecFinal { get; set; }
        public int MontoMaximo { get; set; }
        public int IdCierre { get; set; }
        public int? NumDia { get; set; }
        public int? diaPlazo { get; set; }
        public int IdMonedaLocal { get; set; }
    }

    public class PocQuickpass
    {
        public int IdQuickpass { get; set; }
        public int? IdTipoQuickpass { get; set; }
        public string NumQuickpass { get; set; }
        public int? IdPersona { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdCuenta { get; set; }
        public string NumPlaca { get; set; }
        public int IdEstado { get; set; }
        public DateTime? FecRevisionCostoAdm { get; set; }
        public int? IdFinanciado { get; set; }
        public decimal? SaldoDispositivo { get; set; }
        public decimal? SaldoPeaje { get; set; }
        public int? IdPrepago { get; set; }
        public decimal? MonCostoAdm { get; set; }
        public decimal? MonCargoInicial { get; set; }
        public decimal? MonMinRecarga { get; set; }
        public decimal? MonCostoUnidad { get; set; }
        public decimal? MonMinListaNegra { get; set; }
        public int? IdMonedaAdm { get; set; }
        public int? IdMonedaPeaje { get; set; }
        public int? IdCargoDiario { get; set; }
        public int? MesesFinanciamiento { get; set; }
        public string DescEstado { get; set; }
        public string DescEstadoOperativo { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreCliente { get; set; }
        public string Financiado { get; set; }
        public string DescPrepago { get; set; }
        public string MonedaAdm { get; set; }
        public string MonedaPeaje { get; set; }
        public string CargoDiario { get; set; }
        public string DescTipoQuickpass { get; set; }
        public string NumCuenta { get; set; }
        public string NumDocumento { get; set; }
        public int DiaCorte { get; set; }
        public int? IdEstadoOperativo { get; set; }
        public DateTime? FecVencimiento { get; set; }

        /*Campo creado exclusivamente para la modificación del estado*/
        public string Detalle { get; set; }
    }

    public class PocQPMovimientos
    {
        public int IdMovimiento { get; set; }
        public int? IdTipoMovimiento { get; set; }
        public int CodMovimiento { get; set; }
        public int IdQuickpass { get; set; }
        public DateTime FecConsumo { get; set; }
        public TimeSpan HoraConsumo { get; set; }
        public decimal Monto { get; set; }
        public int IdMoneda { get; set; }
        public int? IdEstado { get; set; }
        public DateTime? FecProcesamiento { get; set; }
        public DateTime? FecAplicacion { get; set; }
        public int? IdLote { get; set; }
        public string Detalle { get; set; }
        public string NumQuickpass { get; set; }
        public string NumReferenciaPago { get; set; }
        public string NumRefProcesador { get; set; }
        public int? IdTipoRegistro { get; set; }
        public string CodLocalizador { get; set; }
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
        public int? IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }
        public string SimboloMonedaLocal { get; set; }
        public string Descripcion { get; set; }
        public string NombreImpreso { get; set; }
        public string NumTarjeta { get; set; }

        public string DescEstado { get; set; }
        public string DescTipoMovimiento { get; set; }
        public string DetAplicacionPago { get; set; }

    }

    public class PocQuickpassMovimientos
    {
        public int IdQuickpass { get; set; }
        public string NumQuickpass { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdEstado { get; set; }
        public int CodMovimiento { get; set; }
        public decimal Monto { get; set; }
        public int IdMoneda { get; set; }
        public int? IdTipoMovimiento { get; set; }
        public DateTime FecConsumo { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreCompleto { get; set; }
    }

    //public class PocListaTags
    //{
    //    public string emisor { get; set; }
    //    public int tipoLista { get; set; }
    //    public List<string> listaQP { get; set; }
    //    public DateTime fechaGeneracion { get; set; }
    //    public int? secGeneracion { get; set; }
    //}

    public class PocListaQP
    {
        public Boolean listaNegra { get; set; }
        public Boolean listaGris { get; set; }
        public Boolean listaBlanca { get; set; }
    }

    public class PocListaTagsTabla
    {
        public string emisor { get; set; }
        public int tipoLista { get; set; }
        public string numQuickpass { get; set; }
        public DateTime? fechaGeneracion { get; set; }
        public int? secGeneracion { get; set; }
        public string descTipoLista { get; set; }
        public string fechahora { get; set; }
        
    }

    public class PocListaQPMontos
    {
        public List<int> ListaQP { get; set; }
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
    }

}

﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace Monibyte.Arquitectura.Datos.Generales
{
    public class ReporteadorDinamico : IReporteadorDinamico
    {
        ISql _unidadTbjo;
        IRepositorioReportServer _exportador;
        private string DataSourceName { get; set; }

        public ReporteadorDinamico(string sectionName, bool web = true)
        {
            _unidadTbjo = GestorUnity.CargarUnidad<IUnidadTbjoMonibyte>();
            _exportador = GestorUnity.CargarUnidad<IRepositorioReportServer>();
            var conf = ConfigHelper.GetConfig(string.Empty, true);
            var section = ConfigHelper.GetConfigSection(conf, "ConfiguracionesReportServer", sectionName);
            DataSourceName = ConfigHelper.ObtieneValPropiedad<string>(section, "DataSourceName");
        }

        public List<Cplx_GL_CamposReporte> ObtenerCamposReporte(int idReporte)
        {
            var param = new SqlParameter("p1", idReporte);
            return _unidadTbjo.ExecuteQuery<Cplx_GL_CamposReporte>
                ("General.P_ObtieneCamposReporte @p1", param)
                .ToList();
        }

        public Reporte CrearReporte(ReportConfig config, List<ReportDataSet> dataSets)
        {
            var doc = CreateDocument();
            var report = CreateReportBase(doc);
            CreateReportDataSets(doc, report, dataSets);
            var reportSection = CreateReportSection(report);
            var reportPage = CreateReportPage(reportSection, config.Landscape);
            CreateReportHeader(reportPage, doc, config.ReportTitle, config.SubTitle,
                config.ReportId);
            CreateReportFooter(reportPage, doc);
            var reportItems = CreateReportBody(reportSection);
            double top = 0;
            foreach (var data in dataSets)
            {
                if (!string.IsNullOrEmpty(data.SubReportUrl))
                {
                    CreateSubreReport(doc, reportItems,
                        data.Parameters, data.SubReportUrl, top);
                    top += 0.5;
                }
                else
                {
                    if (data.IsDetail)
                    {
                        top += 0.5;
                        CreateSplitLine(doc, reportItems, data.Name, top);
                        top += 0.1;
                    }
                    top = CreateReportDetail(doc, reportItems, data, top);
                }
                top += 1;
            }
            if (config.InsertSignature)
            {
                CreateSignatureDetail(doc, reportItems, top);
            }

            config.Parameters = config.Parameters ?? new List<ReportParam>();
            config.Parameters.Add(new ReportParam
            {
                Name = "IdEmpresa",
                Label = "=Parameters!IdEmpresa.Value",
                Value = InfoSesion.Info.IdEmpresa.ToString()
            }); config.Parameters.Add(new ReportParam
            {
                Name = "IdCompania",
                Label = "=Parameters!IdCompania.Value",
                Value = InfoSesion.Info.IdCompania.ToString()
            }); config.Parameters.Add(new ReportParam
            {
                Name = "IdIdioma",
                Label = "=Parameters!Idioma.Value",
                Value = 1.ToString()
            }); config.Parameters.Add(new ReportParam
            {
                Name = "IdIdiomaBase",
                Label = "=Parameters!IdIdiomaBase.Value",
                Value = 1.ToString()
            });
            CreateReportParameters(report, doc, config.Parameters);
            //Convert to Stream and send to generate file
            var rformat = config.ReportFormat;
            var rparams = config.Parameters.ToArray();
            byte[] myByteArray = System.Text.Encoding.UTF8.GetBytes(doc.OuterXml);
            return _exportador.ExportDefinition(myByteArray, rformat, rparams);
        }

        private XmlDocument CreateDocument()
        {
            var sb = new StringBuilder();
            sb.Append("<Report ");
            sb.AppendFormat("xmlns:rd=\"{0}\" ", "http://schemas.microsoft.com/SQLServer/reporting/reportdesigner");
            sb.AppendFormat("xmlns:cl=\"{0}\" ", "http://schemas.microsoft.com/SQLServer/reporting/2010/01/componentdefinition");
            sb.AppendFormat("xmlns=\"{0}\">", "http://schemas.microsoft.com/sqlserver/reporting/2010/01/reportdefinition");
            sb.Append("</Report>");
            var doc = new XmlDocument();
            doc.Load(new StringReader(sb.ToString()));
            return doc;
        }

        private XmlElement CreateReportBase(XmlDocument doc)
        {
            var report = (XmlElement)doc.FirstChild;
            AddElement(report, "AutoRefresh", "0");
            AddElement(report, "ConsumeContainerWhitespace", "true");
            var dataSources = AddElement(report, "DataSources", null);
            var dataSource = AddElement(dataSources, "DataSource", null);
            AddAttribute(dataSource, doc, "Name", this.DataSourceName);
            AddElement(dataSource, "DataSourceReference", "/" + this.DataSourceName);
            AddRdElement(dataSource, "rd:SecurityType", "None");
            return report;
        }

        private void CreateReportDataSets(XmlDocument doc, XmlElement report, List<ReportDataSet> dataSets)
        {
            var ds = AddElement(report, "DataSets", null);
            foreach (var data in dataSets)
            {
                //DataSets element
                var dataSet = AddElement(ds, "DataSet", null);
                AddAttribute(dataSet, doc, "Name", data.Name);
                var fields = AddElement(dataSet, "Fields", null);
                data.Columns = data.Columns.DistinctBy(p => p.Name).ToList();
                foreach (var property in data.Columns/*.OrderBy(x => x.Order)*/)
                {
                    var field = AddElement(fields, "Field", null);
                    AddAttribute(field, doc, "Name", property.Name);
                    AddElement(field, "DataField", property.Name);
                    AddRdElement(field, "rd:TypeName", property.FieldType);
                }
                //Query
                var query = AddElement(dataSet, "Query", null);
                AddElement(query, "DataSourceName", this.DataSourceName);
                AddElement(query, "CommandText", data.QueryCommand);
                if (data.StoreProcedure)
                {
                    AddElement(query, "CommandType", "StoredProcedure");
                }
                AddRdElement(query, "rd:UseGenericDesigner", "true");
                var paramets = AddElement(query, "QueryParameters", null);
                if (data.Parameters != null && data.Parameters.Count > 0)
                {
                    foreach (var item in data.Parameters)
                    {
                        var field = AddElement(paramets, "QueryParameter", null);
                        AddAttribute(field, doc, "Name", "@" + item.Name);
                        AddElement(field, "Value", item.Value);
                    }
                }
            }
        }

        private XmlElement CreateReportSection(XmlElement report)
        {
            XmlElement reportSections = AddElement(report, "ReportSections", null);
            XmlElement reportSection = AddElement(reportSections, "ReportSection", null);
            return reportSection;
        }

        private XmlElement CreateReportPage(XmlElement reportSection, bool landscape = false)
        {
            var page = AddElement(reportSection, "Page", null);
            //portrait
            if (landscape)
            {
                AddElement(page, "InteractiveHeight", "8.5in");
                AddElement(page, "InteractiveWidth", "11in");
                AddElement(page, "PageHeight", "11in");
                AddElement(page, "PageWidth", "8.5in");
            }
            else
            {
                AddElement(page, "InteractiveHeight", "11in");
                AddElement(page, "InteractiveWidth", "8.5in");
                AddElement(page, "PageHeight", "11in");
                AddElement(page, "PageWidth", "8.5in");
            }
            AddElement(page, "LeftMargin", "0.3937in");
            AddElement(page, "RightMargin", "0.3937in");
            AddElement(page, "TopMargin", "0.3937in");
            AddElement(page, "BottomMargin", "0.3937in");
            AddElement(page, "Style", null);
            return page;
        }

        private XmlElement CreateReportBody(XmlElement reportSection)
        {
            AddElement(reportSection, "Width", "7.5in");
            var body = AddElement(reportSection, "Body", null);
            AddElement(body, "Height", "1.85546in");
            var reportItems = AddElement(body, "ReportItems", null);
            return reportItems;
        }

        private double CreateReportDetail(XmlDocument doc,
            XmlElement reportItems,
            ReportDataSet dataSet, double marginTop)
        {
            double widthSum = 0;
            double maxWidth = 19;
            double heigth = 0.9;
            double newMarginTop = marginTop;
            var dataColumns = new List<ReportColumn>();
            var dataColumnsRow = new List<ReportColumn>();

            var reportColumns = dataSet.Columns;
            var agrupacion = reportColumns.Where(x => x.Group)
                .OrderBy(x => x.Order).ToList() ?? new List<ReportColumn>();
            reportColumns = reportColumns.Where(x => !x.Group)
                .OrderBy(m => m.Order).ToList();
            foreach (var column in reportColumns)
            {
                widthSum += Convert.ToDouble(column.Width);
                if (widthSum <= maxWidth)
                {
                    dataColumns.Add(column);
                }
                else
                {
                    dataColumnsRow.Add(column);
                }
            }
            var tablix = CreateTablix(reportItems, doc, dataSet.Name,
                dataColumns, dataColumnsRow, agrupacion, marginTop);
            //Create Tablix Columns And Rows
            var tablixBody = AddElement(tablix, "TablixBody", null);
            var tablixColumns = AddElement(tablixBody, "TablixColumns", null);
            var sum = dataColumns.Sum(x => Convert.ToDouble(x.Width));
            var addWidth = (maxWidth - sum) / dataColumns.Count();
            foreach (var column in dataColumns)
            {
                column.Width = GetNumber(double.Parse(column.Width) + addWidth);
                var tablixColumn = AddElement(tablixColumns, "TablixColumn", null);
                AddElement(tablixColumn, "Width", column.Width);
            }
            var tablixRows = AddElement(tablixBody, "TablixRows", null);
            var tablixCellHeaders = CreateRow(tablixRows);
            if (agrupacion.Count() > 0)
            {
                foreach (var agrupador in agrupacion)
                {
                    newMarginTop += 0.25;
                    var tablixRow = AddElement(tablixRows, "TablixRow", null);
                    AddElement(tablixRow, "Height", "0.25in");
                    var tablixCells = AddElement(tablixRow, "TablixCells", null);
                    CreateColSpanRow(tablixCells, doc, agrupador.Name,
                        null, agrupador.FieldType, dataColumns.Count(),
                        agrupador.Group, dataSet.IsDetail);
                }
            }
            var tablixCellRows = CreateRow(tablixRows);
            foreach (var column in dataColumns)
            {
                var tablixCell = AddElement(tablixCellHeaders, "TablixCell", null);
                var cellContents = AddElement(tablixCell, "CellContents", null);
                var textbox = AddElement(cellContents, "Textbox", null);
                AddAttribute(textbox, doc, "Name", string.Format("Header{0}", CreateUniqueId()));
                AddElement(textbox, "KeepTogether", "true");
                var paragraphs = AddElement(textbox, "Paragraphs", null);
                var paragraph = AddElement(paragraphs, "Paragraph", null);
                var textRuns = AddElement(paragraph, "TextRuns", null);
                var textRun = AddElement(textRuns, "TextRun", null);
                AddElement(textRun, "Value", column.HeaderName);
                var style = AddElement(textRun, "Style", null);
                AddElement(style, "Color", "White");
                AddElement(style, "FontSize", "8pt");
                AddElement(style, "FontFamily", "Courier New");
                style = AddElement(textbox, "Style", null);
                AddElement(style, "VerticalAlign", "Middle");
                var border = AddElement(style, "Border", null);
                AddElement(border, "Color", "Black");
                AddElement(border, "Style", "Solid");
                AddElement(style, "PaddingLeft", "2pt");
                AddElement(style, "PaddingRight", "2pt");
                AddElement(style, "PaddingTop", "2pt");
                AddElement(style, "PaddingBottom", "2pt");
                AddElement(style, "BackgroundColor", "#2d4e53");
                CreateRowCell(tablixCellRows, doc, column.Name, column.FieldType,
                    reportColumns.Count, dataSet.IsDetail, column.CanGrow);
            }
            foreach (var column in dataColumnsRow)
            {
                newMarginTop += heigth;
                var tablixRow = AddElement(tablixRows, "TablixRow", null);
                AddElement(tablixRow, "Height", "0.25in");
                var tablixCells = AddElement(tablixRow, "TablixCells", null);
                CreateColSpanRow(tablixCells, doc, column.Name, column.HeaderName,
                    column.FieldType, dataColumns.Count(), false, dataSet.IsDetail);
            }
            //Dentro del Detalle
            if (dataColumns.Any(x => x.Sum) && agrupacion.Any())
            {
                newMarginTop += heigth * 2;
                var tablixRow = AddElement(tablixRows, "TablixRow", null);
                AddElement(tablixRow, "Height", "0.4in");
                var tablixCells = AddElement(tablixRow, "TablixCells", null);
                CreateSummaryRows(tablixCells, doc, dataColumns, "Sub-total :");
            }
            foreach (var column in dataColumnsRow.Where(x => x.Sum))
            {
                newMarginTop += heigth * 2;
                var tablixRow = AddElement(tablixRows, "TablixRow", null);
                AddElement(tablixRow, "Height", "0.4in");
                var tablixCells = AddElement(tablixRow, "TablixCells", null);
                var rowText = "=" + '\u0022' + "Sub-total " + column.HeaderName + " : " +
                    '\u0022' + " & " + " FormatNumber(CStr(Sum(Fields!" + column.Name + ".Value)),2)";
                CreateColSpanRow(tablixCells, doc, rowText, dataColumns.Count());
            }
            //Fuera del Detalle
            if (dataColumns.Any(x => x.Sum))
            {
                newMarginTop += heigth * 2;
                var tablixRow = AddElement(tablixRows, "TablixRow", null);
                AddElement(tablixRow, "Height", "0.4in");
                var tablixCells = AddElement(tablixRow, "TablixCells", null);
                CreateSummaryRows(tablixCells, doc, dataColumns, "Total :");
            }
            foreach (var column in dataColumnsRow.Where(x => x.Sum))
            {
                newMarginTop += heigth * 2;
                var tablixRow = AddElement(tablixRows, "TablixRow", null);
                AddElement(tablixRow, "Height", "0.4in");
                var tablixCells = AddElement(tablixRow, "TablixCells", null);
                var rowText = "=" + '\u0022' + "Total " + column.HeaderName + " : " +
                    '\u0022' + " & " + " FormatNumber(CStr(Sum(Fields!" + column.Name + ".Value)),2)";
                CreateColSpanRow(tablixCells, doc, rowText, dataColumns.Count());
            }
            return newMarginTop;
        }

        private void CreateSubreReport(XmlDocument doc, XmlElement reportItems,
            List<ReportParam> parametros, string value, double marginTop)
        {
            var subreReprot = AddElement(reportItems, "Subreport", null);
            AddAttribute(subreReprot, doc, "Name", string.Format("Subre{0}", CreateUniqueId()));
            AddElement(subreReprot, "ReportName", value);
            var parameters = AddElement(subreReprot, "Parameters", null);
            foreach (var item in parametros)
            {
                var parameter = AddElement(parameters, "Parameter", null);
                AddAttribute(parameter, doc, "Name", item.Name);
                AddElement(parameter, "Value", "=Parameters!" + item.Name + ".Value");
            }
            AddElement(subreReprot, "Top", GetNumber(marginTop));
            AddElement(subreReprot, "Left", "0in");
            AddElement(subreReprot, "Height", "2in");
            AddElement(subreReprot, "Width", "8in");
        }

        private void CreateReportHeader(XmlElement page, XmlDocument doc,
            string titulo, string subTitulo, string rightValue)
        {
            /*****************Incluir Header******************/
            var Head = AddElement(page, "PageHeader", null);
            AddElement(Head, "Height", "20mm");
            AddElement(Head, "PrintOnFirstPage", "true");
            AddElement(Head, "PrintOnLastPage", "true");

            //Incluimos sección de elementos del header
            var repItems = AddElement(Head, "ReportItems", null);

            if (!string.IsNullOrEmpty(titulo))
            {
                //Incluimos el Texto central del encabezado
                var Textbox = AddElement(repItems, "Textbox", null);
                AddAttribute(Textbox, doc, "Name", string.Format("Tbox{0}", CreateUniqueId()));
                AddElement(Textbox, "CanGrow", "true");
                AddElement(Textbox, "KeepTogether", "true");
                var paragraphs = AddElement(Textbox, "Paragraphs", null);
                var paragraph = AddElement(paragraphs, "Paragraph", null);
                var textRuns = AddElement(paragraph, "TextRuns", null);
                var styleParagraph = AddElement(paragraph, "Style", null);
                AddElement(styleParagraph, "TextAlign", "Center");
                var textRun = AddElement(textRuns, "TextRun", null);
                AddElement(textRun, "Value", titulo);
                var style = AddElement(textRun, "Style", null);
                AddElement(style, "FontFamily", "Century Gothic");
                AddElement(style, "FontSize", "20pt");
                AddElement(style, "FontWeight", "Bold");
                AddRdElement(Textbox, "rd:DefaultName", string.Format("Textb{0}", CreateUniqueId()));
                AddElement(Textbox, "Top", "2.76428mm");
                AddElement(Textbox, "Left", "42.28047mm");
                AddElement(Textbox, "Height", "11.76161mm");
                AddElement(Textbox, "Width", "92.59476mm");
            }


            if (!string.IsNullOrEmpty(subTitulo))
            {
                //Incluimos el Subtitulo del encabezado
                var Textbox = AddElement(repItems, "Textbox", null);
                AddAttribute(Textbox, doc, "Name", string.Format("Tbox{0}", CreateUniqueId()));
                //AddElement(Textbox, "CanGrow", "false");
                //AddElement(Textbox, "KeepTogether", "true");
                var paragraphs = AddElement(Textbox, "Paragraphs", null);
                var paragraph = AddElement(paragraphs, "Paragraph", null);
                var textRuns = AddElement(paragraph, "TextRuns", null);
                var styleParagraph = AddElement(paragraph, "Style", null);
                AddElement(styleParagraph, "TextAlign", "Center");
                var textRun = AddElement(textRuns, "TextRun", null);
                AddElement(textRun, "Value", subTitulo);
                var style = AddElement(textRun, "Style", null);
                AddElement(style, "FontFamily", "Century Gothic");
                AddElement(style, "FontSize", "11pt");
                AddElement(style, "FontWeight", "Bold");
                AddRdElement(Textbox, "rd:DefaultName", string.Format("Textb{0}", CreateUniqueId()));
                AddElement(Textbox, "Top", "13.56428mm");
                AddElement(Textbox, "Left", "32.28047mm");
                AddElement(Textbox, "Height", "11.76161mm");
                AddElement(Textbox, "Width", "102.59476mm");
            }

            //Incluimos una imagen en el encabezado
            var Image = AddElement(repItems, "Image", null);
            AddAttribute(Image, doc, "Name", string.Format("Image{0}", CreateUniqueId()));
            AddElement(Image, "Value", "=" + '\u0022' + "/Resources/LogosEmpresariales/Logo-" +
                '\u0022' + "+CStr(Parameters!IdEmpresa.Value)+" + '\u0022' + ".gif" + '\u0022');
            AddElement(Image, "Source", "External");
            AddElement(Image, "Sizing", "FitProportional");
            AddElement(Image, "Top", "2.15517mm");
            AddElement(Image, "Left", "2.34561mm");
            AddElement(Image, "Height", "14.76683mm");
            AddElement(Image, "Width", "42.8793mm");
            AddElement(Image, "ZIndex", "1");

            if (!string.IsNullOrEmpty(rightValue))
            {
                //Incluimos el Texto a la derecha del encabezado
                var Textbox = AddElement(repItems, "Textbox", null);
                AddAttribute(Textbox, doc, "Name", string.Format("Tbx{0}", CreateUniqueId()));
                AddElement(Textbox, "CanGrow", "true");
                AddElement(Textbox, "KeepTogether", "true");
                var paragraphs = AddElement(Textbox, "Paragraphs", null);
                var paragraph = AddElement(paragraphs, "Paragraph", null);
                var styleParagraph = AddElement(paragraph, "Style", null);
                AddElement(styleParagraph, "TextAlign", "Right");
                var textRuns = AddElement(paragraph, "TextRuns", null);
                var textRun = AddElement(textRuns, "TextRun", null);
                AddElement(textRun, "Value", rightValue);
                AddElement(textRun, "MarkupType", "HTML");
                var style = AddElement(textRun, "Style", null);
                AddElement(style, "FontFamily", "Courier New");
                AddElement(style, "FontSize", "8pt");
                AddElement(style, "FontWeight", "Bold");
                AddRdElement(Textbox, "rd:DefaultName", string.Format("Textbx{0}", CreateUniqueId()));
                AddElement(Textbox, "Top", "4.922mm");
                AddElement(Textbox, "Left", "140.10741mm");
                AddElement(Textbox, "Height", "20mm");
                AddElement(Textbox, "Width", "53.52112mm");
                AddElement(Textbox, "ZIndex", "2");
            }
        }

        private void CreateReportFooter(XmlElement page, XmlDocument doc)
        {
            /*****************Incluir Footer******************/
            var Foot = AddElement(page, "PageFooter", null);
            AddElement(Foot, "Height", "8mm");
            AddElement(Foot, "PrintOnFirstPage", "true");
            AddElement(Foot, "PrintOnLastPage", "true");
            //Crando el item contador de páginas
            var repItems = AddElement(Foot, "ReportItems", null);
            var Textbox = AddElement(repItems, "Textbox", null);
            AddAttribute(Textbox, doc, "Name", string.Format("Box{0}", CreateUniqueId()));
            AddElement(Textbox, "CanGrow", "true");
            AddElement(Textbox, "KeepTogether", "true");
            var paragraphs = AddElement(Textbox, "Paragraphs", null);
            var paragraph = AddElement(paragraphs, "Paragraph", null);
            var textRuns = AddElement(paragraph, "TextRuns", null);
            var textRun = AddElement(textRuns, "TextRun", null);
            AddElement(textRun, "Value", "=" + '\u0022' + "Página" + '\u0022' + " &" + " Globals!PageNumber" + " & " +
                '\u0022' + "/" + '\u0022' + " &" + " Globals!TotalPages");

            AddElement(Textbox, "Top", "0.2mm");
            AddElement(Textbox, "Left", "158.5mm");
            AddElement(Textbox, "Height", "6mm");
            AddElement(Textbox, "Width", "29mm");
        }

        private void CreateSignatureDetail(XmlDocument doc, XmlElement reportItems,
            double marginTop)
        {
            marginTop += 4;
            var Textbox = AddElement(reportItems, "Textbox", null);
            AddAttribute(Textbox, doc, "Name", string.Format("Box{0}", CreateUniqueId()));
            AddElement(Textbox, "CanGrow", "true");
            AddElement(Textbox, "KeepTogether", "true");
            var paragraphs = AddElement(Textbox, "Paragraphs", null);
            var paragraph = AddElement(paragraphs, "Paragraph", null);
            var textRuns = AddElement(paragraph, "TextRuns", null);
            var textRun = AddElement(textRuns, "TextRun", null);
            AddElement(textRun, "Value", "Firma del funcionario");

            AddElement(Textbox, "Top", GetNumber(marginTop));
            AddElement(Textbox, "Left", "0.56162in");
            AddElement(Textbox, "Height", "0.25in");
            AddElement(Textbox, "Width", "2.0625in");

            var Textbox2 = AddElement(reportItems, "Textbox", null);
            AddAttribute(Textbox2, doc, "Name", string.Format("Box{0}", CreateUniqueId()));
            AddElement(Textbox2, "CanGrow", "true");
            AddElement(Textbox2, "KeepTogether", "true");
            var paragraphs2 = AddElement(Textbox2, "Paragraphs", null);
            var paragraph2 = AddElement(paragraphs2, "Paragraph", null);
            var textRuns2 = AddElement(paragraph2, "TextRuns", null);
            var textRun2 = AddElement(textRuns2, "TextRun", null);
            AddElement(textRun2, "Value", "Revisado por: Nombre / Firma");

            AddElement(Textbox2, "Top", GetNumber(marginTop));
            AddElement(Textbox2, "Left", "3.49925in");
            AddElement(Textbox2, "Height", "0.25in");
            AddElement(Textbox2, "Width", "2.40625in");


            var line = AddElement(reportItems, "Line", null);
            AddAttribute(line, doc, "Name", string.Format("Line{0}", CreateUniqueId()));
            AddElement(line, "Top", GetNumber(marginTop));
            AddElement(line, "Left", "0.56162in");
            AddElement(line, "Height", "0.in");
            AddElement(line, "Width", "2.0625in");

            var style = AddElement(line, "Style", null);
            var border = AddElement(style, "Border", null);
            AddElement(border, "Color", "Black");
            AddElement(border, "Style", "Solid");
            AddElement(border, "Width", "1pt");


            var line2 = AddElement(reportItems, "Line", null);
            AddAttribute(line2, doc, "Name", string.Format("Line{0}", CreateUniqueId()));
            AddElement(line2, "Top", GetNumber(marginTop));
            AddElement(line2, "Left", "3.49925in");
            AddElement(line2, "Height", "0.in");
            AddElement(line2, "Width", "3.38047in");

            var styleline2 = AddElement(line2, "Style", null);
            var borderline2 = AddElement(styleline2, "Border", null);
            AddElement(borderline2, "Color", "Black");
            AddElement(borderline2, "Style", "Solid");
            AddElement(borderline2, "Width", "1pt");
        }

        private void CreateSplitLine(XmlDocument doc, XmlElement reportItems,
            string dataSetName, double marginTop)
        {
            var line = AddElement(reportItems, "Line", null);
            AddAttribute(line, doc, "Name", string.Format("Line{0}", CreateUniqueId()));
            AddElement(line, "Top", GetNumber(marginTop));
            AddElement(line, "Left", "0mm");
            AddElement(line, "Height", "0mm");
            AddElement(line, "Width", "190mm");
            AddElement(line, "ZIndex", "4");
            var visible = AddElement(line, "Visibility", null);
            AddElement(visible, "Hidden", "=IIF(CountRows(" + '\u0022' + dataSetName + '\u0022' + ") > 0,False,True)");

            var style = AddElement(line, "Style", null);
            var border = AddElement(style, "Border", null);
            AddElement(border, "Color", "#f99a00");
            AddElement(border, "Style", "Solid");
            AddElement(border, "Width", "5pt");
        }

        private void CreateReportParameters(XmlElement report, XmlDocument doc,
            List<ReportParam> parametros)
        {
            XmlElement ReportParameters = AddElement(report, "ReportParameters", null);
            foreach (var item in parametros)
            {
                XmlElement parameter = AddElement(ReportParameters, "ReportParameter", null);
                AddAttribute(parameter, doc, "Name", item.Name);
                AddElement(parameter, "DataType", "String");
                AddElement(parameter, "Prompt", item.Name);
            }
        }

        private XmlElement CreateTablix(XmlElement reportItems, XmlDocument doc, string dataSetName,
            List<ReportColumn> dataColumns, List<ReportColumn> dataColumnsRow,
            List<ReportColumn> agrupador, double marginTop)
        {
            var aditionalData = dataColumnsRow.Count;
            var aditionals = dataColumnsRow.Count(x => x.Sum);
            var tablix = AddElement(reportItems, "Tablix", null);
            AddAttribute(tablix, doc, "Name", string.Format("Tablix{0}", CreateUniqueId()));
            AddElement(tablix, "DataSetName", dataSetName);
            AddElement(tablix, "Top", GetNumber(marginTop));
            AddElement(tablix, "Left", "0in");
            AddElement(tablix, "Height", "0.5in");
            AddElement(tablix, "Width", "0.5in");
            var visible = AddElement(tablix, "Visibility", null);
            AddElement(visible, "Hidden", "=IIF(CountRows(" + '\u0022' + dataSetName + '\u0022' + ") > 0,False,True)");
            var tablixColumnHierarchy = AddElement(tablix, "TablixColumnHierarchy", null);
            var tablixMembers = AddElement(tablixColumnHierarchy, "TablixMembers", null);
            foreach (var property in dataColumns)
            {
                AddElement(tablixMembers, "TablixMember", null);
            }
            var tablixRowHierarchy = AddElement(tablix, "TablixRowHierarchy", null);
            AddElement(tablix, "RepeatColumnHeaders", "true");
            AddElement(tablix, "RepeatRowHeaders", "true");
            tablixMembers = AddElement(tablixRowHierarchy, "TablixMembers", null);

            var tablixMember = AddElement(tablixMembers, "TablixMember", null);
            AddElement(tablixMember, "KeepWithGroup", "After");
            AddElement(tablixMember, "KeepTogether", "true");
            AddElement(tablixMember, "RepeatOnNewPage", "true");
            tablixMember = AddElement(tablixMembers, "TablixMember", null);
            AddElement(tablixMember, "DataElementName", "Detail_Collection");

            var group = AddElement(tablixMember, "Group", null);
            AddAttribute(group, doc, "Name", string.Format("Table{0}_Details_Group", CreateUniqueId()));
            if (agrupador.Count() > 0)
            {
                var agrupacion = agrupador.ToArray()[0];
                var groupExpressions = AddElement(group, "GroupExpressions", null);
                AddElement(groupExpressions, "GroupExpression", "=Fields!" + agrupacion.Name + ".Value");
                var sortExpressions = AddElement(tablixMember, "SortExpressions", null);
                var sortExpresion = AddElement(sortExpressions, "SortExpression", null);
                AddElement(sortExpresion, "Value", "=Fields!" + agrupacion.Name + ".Value");
            }
            AddElement(group, "DataElementName", "Detail");

            var tablixMembersNested = AddElement(tablixMember, "TablixMembers", null);
            if (agrupador.Count() > 0)
            {
                if (agrupador.Count() == 1)
                {
                    var memberGroup = AddElement(tablixMembersNested, "TablixMember", null);
                    AddElement(memberGroup, "KeepWithGroup", "After");
                }
                var member = AddElement(tablixMembersNested, "TablixMember", null);
                XmlElement groupmember;
                XmlElement aditionalmembers;
                if (agrupador.Count() > 1)
                {
                    var grupo = AddElement(member, "Group", null);
                    AddAttribute(grupo, doc, "Name", string.Format("Table{0}_Details_Group", CreateUniqueId()));
                    var agrupacion = agrupador.ToArray()[1];
                    var groupExpressions = AddElement(grupo, "GroupExpressions", null);
                    AddElement(groupExpressions, "GroupExpression", "=Fields!" + agrupacion.Name + ".Value");

                    var sortExpressions = AddElement(member, "SortExpressions", null);
                    var sortExpresion = AddElement(sortExpressions, "SortExpression", null);
                    AddElement(sortExpresion, "Value", "=Fields!" + agrupacion.Name + ".Value");

                    var t1members = AddElement(member, "TablixMembers", null);

                    for (int i = 0; i < agrupador.Count(); i++)
                    {
                        AddElement(t1members, "TablixMember", null);
                    }
                    var lastTbMember = AddElement(t1members, "TablixMember", null);
                    groupmember = AddElement(lastTbMember, "Group", null);
                    AddAttribute(groupmember, doc, "Name", string.Format("Detail{0}", CreateUniqueId()));
                    aditionalmembers = AddElement(lastTbMember, "TablixMembers", null);

                    if (dataColumns.Any(x => x.Sum) && agrupador.Count() > 1)
                    {
                        AddElement(t1members, "TablixMember", null);
                    }

                    if (dataColumns.Any(x => x.Sum))
                    {
                        var memberBefore = AddElement(tablixMembersNested, "TablixMember", null);
                        AddElement(memberBefore, "KeepWithGroup", "After");
                    }
                    if ((agrupador.Count() > 0) && aditionals > 0)
                    {
                        for (int i = 0; i < aditionals; i++)
                        {
                            var memberBefore = AddElement(tablixMembersNested, "TablixMember", null);
                            AddElement(memberBefore, "KeepWithGroup", "Before");
                        }
                    }
                }
                else
                {
                    groupmember = AddElement(member, "Group", null);
                    AddAttribute(groupmember, doc, "Name", string.Format("Detail{0}", CreateUniqueId()));
                    aditionalmembers = AddElement(member, "TablixMembers", null);
                }
                AddElement(aditionalmembers, "TablixMember", null);
                for (int i = 0; i < aditionalData; i++)
                {
                    AddElement(aditionalmembers, "TablixMember", null);
                }
                //Dentro del Detalle
                if (dataColumns.Any(x => x.Sum) && agrupador.Count() == 1)
                {
                    var memberBefore = AddElement(tablixMembersNested, "TablixMember", null);
                    AddElement(memberBefore, "KeepWithGroup", "Before");
                }
                if ((agrupador.Count() > 0) && aditionals > 0)
                {
                    for (int i = 0; i < aditionals; i++)
                    {
                        var memberBefore = AddElement(tablixMembersNested, "TablixMember", null);
                        AddElement(memberBefore, "KeepWithGroup", "Before");
                    }
                }
                if (agrupador.Count() == 1)
                {
                    //Fuera del Detalle
                    if (dataColumns.Any(x => x.Sum))
                    {
                        var memberBefore = AddElement(tablixMembers, "TablixMember", null);
                        AddElement(memberBefore, "KeepWithGroup", "Before");
                    }
                    if ((agrupador.Count() > 0) && aditionals > 0)
                    {
                        for (int i = 0; i < aditionals; i++)
                        {
                            var memberBefore = AddElement(tablixMembers, "TablixMember", null);
                            AddElement(memberBefore, "KeepWithGroup", "Before");
                        }
                    }
                }
            }
            else
            {
                AddElement(tablixMembersNested, "TablixMember", null);
                for (int i = 0; i < aditionalData; i++)
                {
                    AddElement(tablixMembersNested, "TablixMember", null);
                }
                if (agrupador.Count == 0 && dataColumns.Count(x => x.Sum) > 0)
                {
                    var memberBefore = AddElement(tablixMembers, "TablixMember", null);
                    AddElement(memberBefore, "KeepWithGroup", "Before");
                }
                if (agrupador.Count == 0 && dataColumnsRow.Count(x => x.Sum) > 0)
                {
                    for (int j = 0; j < dataColumnsRow.Count(x => x.Sum); j++)
                    {
                        var memberBefore = AddElement(tablixMembers, "TablixMember", null);
                        AddElement(memberBefore, "KeepWithGroup", "Before");
                    }
                }
            }
            return tablix;
        }

        private XmlElement CreateRow(XmlElement tablixRows)
        {
            var tablixRow = AddElement(tablixRows, "TablixRow", null);
            AddElement(tablixRow, "Height", "0.27373in");
            var tablixCells = AddElement(tablixRow, "TablixCells", null);
            return tablixCells;
        }

        private void CreateRowCell(XmlElement tablixCells, XmlDocument doc,
            string fieldname, string fieldType, int fields,
            bool esDetalle = false, bool canGrow = false)
        {
            var tablixCell = AddElement(tablixCells, "TablixCell", null);
            var cellContents = AddElement(tablixCell, "CellContents", null);
            var textbox = AddElement(cellContents, "Textbox", null);
            AddAttribute(textbox, doc, "Name", fieldname + CreateUniqueId());
            AddElement(textbox, "KeepTogether", "true");
            if (canGrow)
            {
                AddElement(textbox, "CanGrow", "true");
            }
            var paragraphs = AddElement(textbox, "Paragraphs", null);
            var paragraph = AddElement(paragraphs, "Paragraph", null);
            var textRuns = AddElement(paragraph, "TextRuns", null);
            var textRun = AddElement(textRuns, "TextRun", null);
            if (fieldType == "System.DateTime")
            {
                AddElement(textRun, "Value",
                   string.Format("=IIF(FORMAT(Fields!{0}.Value," + '\u0022' + "dd/MM/yyyy" + '\u0022' + ")" +
                   " < FORMAT(01-01-001," + '\u0022' + "dd/MM/yyyy" + '\u0022' + "), " + '\u0022' + '\u0022' +
                   ", " + "FORMAT(Fields!{0}.Value," + '\u0022' + "dd/MM/yyyy" + '\u0022' + "))", fieldname));
                var styleParagraph = AddElement(paragraph, "Style", null);
                AddElement(styleParagraph, "TextAlign", "Center");
            }
            else if (fieldType == "System.Decimal")
            {
                AddElement(textRun, "Value", "=FormatNumber(CStr(Sum(Fields!" + fieldname + ".Value)),2)");
                var styleParagraph = AddElement(paragraph, "Style", null);
                AddElement(styleParagraph, "TextAlign", "Right");
            }
            else
            {
                AddElement(textRun, "Value", string.Format("=Fields!{0}.Value", fieldname));
            }

            var styleText = AddElement(textRun, "Style", null);
            var style = AddElement(textbox, "Style", null);
            var border = AddElement(style, "Border", null);
            AddElement(style, "PaddingLeft", "2pt");
            AddElement(style, "PaddingRight", "2pt");
            AddElement(style, "PaddingTop", "2pt");
            if (esDetalle)
            {
                AddElement(style, "BackgroundColor", "White");
            }
            else
            {
                AddElement(border, "Color", "Black");
                AddElement(border, "Style", "Solid");
                AddElement(border, "Width", "0.5pt");
                AddElement(style, "BackgroundColor", "LightGrey");
            }
            AddElement(style, "PaddingBottom", "2pt");
            AddElement(styleText, "FontFamily", "Courier New");
            AddElement(styleText, "FontSize", "7pt");
            AddElement(style, "VerticalAlign", "Middle");
        }

        private void CreateColSpanRow(XmlElement tablixCells,
            XmlDocument doc, string value, int tbCells)
        {
            var tablixCell = AddElement(tablixCells, "TablixCell", null);
            var cellContents = AddElement(tablixCell, "CellContents", null);
            var textbox = AddElement(cellContents, "Textbox", null);
            AddAttribute(textbox, doc, "Name", string.Format("Descript" + "{0}", CreateUniqueId()));
            AddElement(textbox, "KeepTogether", "true");
            AddElement(textbox, "CanGrow", "false");
            var paragraphs = AddElement(textbox, "Paragraphs", null);
            var paragraph = AddElement(paragraphs, "Paragraph", null);
            var textRuns = AddElement(paragraph, "TextRuns", null);
            var styleParagraph = AddElement(paragraph, "Style", null);
            AddElement(styleParagraph, "TextAlign", "Left");
            var textRun = AddElement(textRuns, "TextRun", null);

            AddElement(textRun, "Value", value);
            var styleTex = AddElement(textRun, "Style", null);
            var style = AddElement(textbox, "Style", null);
            var border = AddElement(style, "Border", null);

            AddElement(border, "Color", "Black");
            AddElement(style, "BackgroundColor", "White");
            AddElement(border, "Style", "None");
            AddElement(styleTex, "FontSize", "7pt");

            var TopBorder = AddElement(style, "TopBorder", null);
            AddElement(TopBorder, "Color", "Black");
            AddElement(TopBorder, "Style", "Solid");
            AddElement(TopBorder, "Width", "0.5pt");

            AddElement(style, "PaddingLeft", "2pt");
            AddElement(style, "PaddingRight", "2pt");
            AddElement(style, "PaddingTop", "2pt");
            AddElement(style, "PaddingBottom", "2pt");
            AddElement(styleTex, "FontFamily", "Courier New");
            AddElement(style, "VerticalAlign", "Top");
            AddElement(cellContents, "ColSpan", (tbCells).ToString());
            for (int i = 1; i < tbCells; i++)
            {
                AddElement(tablixCells, "TablixCell", null);
            }
        }

        private void CreateColSpanRow(XmlElement tablixCells, XmlDocument doc,
            string fieldname, string displayName, string fieldType, int colSpan,
            bool agrupacion = false, bool esDetalle = false)
        {
            var tablixCell = AddElement(tablixCells, "TablixCell", null);
            var cellContents = AddElement(tablixCell, "CellContents", null);
            var textbox = AddElement(cellContents, "Textbox", null);
            AddAttribute(textbox, doc, "Name", string.Format(fieldname + "{0}", CreateUniqueId()));
            AddElement(textbox, "KeepTogether", "true");
            AddElement(textbox, "CanGrow", "true");
            var paragraphs = AddElement(textbox, "Paragraphs", null);
            var paragraph = AddElement(paragraphs, "Paragraph", null);
            var textRuns = AddElement(paragraph, "TextRuns", null);
            var styleParagraph = AddElement(paragraph, "Style", null);
            var textRun = AddElement(textRuns, "TextRun", null);
            var valuePrint = "";
            if (fieldType == "System.DateTime")
            {
                valuePrint =
                   string.Format("IIF(FORMAT(Fields!{0}.Value," + '\u0022' + "dd/MM/yyyy" + '\u0022' + ")" +
                   " < FORMAT(01-01-001," + '\u0022' + "dd/MM/yyyy" + '\u0022' + "), " + '\u0022' +
                   '\u0022' + ", " + "FORMAT(Fields!{0}.Value," + '\u0022' + "dd/MM/yyyy" + '\u0022' + "))",
                   fieldname);
            }
            if (fieldType == "System.Decimal")
            {
                valuePrint = "FormatNumber(CStr(Sum(Fields!" + fieldname + ".Value)),2)";
            }
            else
            {
                valuePrint = string.Format("Fields!{0}.Value", fieldname);
            }
            if (agrupacion)
            {
                AddElement(textRun, "Value", string.Format("=Fields!{0}.Value", fieldname));
            }
            else
            {
                AddElement(textRun, "Value", "=" + '\u0022' + "<b> " + displayName +
                    ": </b> " + '\u0022' + "& " + valuePrint);
                AddElement(textRun, "MarkupType", "HTML");
            }
            var styleTex = AddElement(textRun, "Style", null);
            var style = AddElement(textbox, "Style", null);
            var border = AddElement(style, "Border", null);
            if (agrupacion)
            {
                AddElement(style, "BackgroundColor", "White");
                AddElement(border, "Color", "LightGrey");
                AddElement(border, "Style", "None");
                var BottomBorder = AddElement(style, "BottomBorder", null);
                AddElement(BottomBorder, "Color", "LightGrey");
                AddElement(BottomBorder, "Style", "Solid");
                AddElement(BottomBorder, "Width", "0.5pt");
                AddElement(styleTex, "FontSize", "9pt");
                AddElement(styleTex, "FontWeight", "Bold");
            }
            else
            {
                AddElement(border, "Color", "Black");
                if (esDetalle)
                {
                    AddElement(style, "BackgroundColor", "White");
                    AddElement(border, "Style", "None");
                }
                else
                {
                    AddElement(style, "BackgroundColor", "LightGrey");
                    AddElement(border, "Style", "Solid");
                    AddElement(border, "Width", "0.5pt");
                }
                AddElement(styleTex, "FontSize", "7pt");
            }
            AddElement(style, "PaddingLeft", "2pt");
            AddElement(style, "PaddingRight", "2pt");
            AddElement(style, "PaddingTop", "2pt");
            AddElement(style, "PaddingBottom", "2pt");
            AddElement(styleTex, "FontFamily", "Courier New");
            AddElement(style, "VerticalAlign", "Middle");
            AddElement(cellContents, "ColSpan", (colSpan).ToString());
            for (int i = 1; i < colSpan; i++)
            {
                AddElement(tablixCells, "TablixCell", null);
            }
        }

        private void CreateSummaryRows(XmlElement tablixCells, XmlDocument doc,
            List<ReportColumn> summaryData, string value)
        {
            var totalColumns = summaryData.Count();
            int infoTextColSpan = 0;
            var finish = false;
            var texRows = new List<ReportColumn>();
            for (int i = 0; i < totalColumns; i++)
            {
                if (!summaryData[i].Sum && !finish)
                {
                    infoTextColSpan = infoTextColSpan + 1;
                }
                if (summaryData[i].Sum || finish)
                {
                    texRows.Add(summaryData[i]);
                    finish = true;
                }
            }
            CreateColSpanRow(tablixCells, doc, value, infoTextColSpan);
            foreach (var item in texRows)
            {
                var tablixCell = AddElement(tablixCells, "TablixCell", null);
                var cellContents = AddElement(tablixCell, "CellContents", null);
                var textbox = AddElement(cellContents, "Textbox", null);
                AddAttribute(textbox, doc, "Name", string.Format("name" + "{0}", CreateUniqueId()));
                AddElement(textbox, "KeepTogether", "true");
                AddElement(textbox, "CanGrow", "true");
                var paragraphs = AddElement(textbox, "Paragraphs", null);
                var paragraph = AddElement(paragraphs, "Paragraph", null);
                var textRuns = AddElement(paragraph, "TextRuns", null);
                var textRun = AddElement(textRuns, "TextRun", null);
                if (item.Sum)
                {
                    AddElement(textRun, "Value", "=FormatNumber(CStr(Sum(Fields!" + item.Name + ".Value)),2)");
                    var styleParagraph = AddElement(paragraph, "Style", null);
                    AddElement(styleParagraph, "TextAlign", "Right");
                }
                else
                {
                    AddElement(textRun, "Value", null);
                }
                var styleText = AddElement(textRun, "Style", null);
                var style = AddElement(textbox, "Style", null);
                var border = AddElement(style, "Border", null);
                AddElement(style, "PaddingLeft", "2pt");
                AddElement(style, "PaddingRight", "2pt");
                AddElement(style, "PaddingTop", "2pt");
                AddElement(border, "Color", "Black");
                AddElement(border, "Style", "None");
                AddElement(border, "Width", "0.5pt");
                var TopBorder = AddElement(style, "TopBorder", null);
                AddElement(TopBorder, "Color", "Black");
                AddElement(TopBorder, "Style", "Solid");
                AddElement(TopBorder, "Width", "0.5pt");
                AddElement(style, "BackgroundColor", "White");
                AddElement(style, "PaddingBottom", "2pt");
                AddElement(styleText, "FontFamily", "Courier New");
                AddElement(styleText, "FontSize", "7pt");
                AddElement(style, "VerticalAlign", "Top");
            }
        }

        private XmlElement AddElement(XmlElement parent, string name, string value)
        {
            XmlElement newelement = parent.OwnerDocument.CreateElement(name,
                "http://schemas.microsoft.com/sqlserver/reporting/2010/01/reportdefinition");
            parent.AppendChild(newelement);
            if (value != null) newelement.InnerText = value;
            return newelement;
        }

        private XmlElement AddRdElement(XmlElement parent, string name, string value)
        {
            XmlElement newelement = parent.OwnerDocument.CreateElement(name,
                "http://schemas.microsoft.com/SQLServer/reporting/reportdesigner");
            parent.AppendChild(newelement);
            if (value != null) newelement.InnerText = value;
            return newelement;
        }

        private XmlAttribute AddAttribute(XmlElement parentElement, XmlDocument doc,
            string attributeName, string attributeValue)
        {
            XmlAttribute attr = parentElement.Attributes.Append(doc.CreateAttribute(attributeName));
            attr.Value = attributeValue;
            return attr;
        }

        private string GetNumber(double number)
        {
            return string.Format("{0}cm", number.
                ToString(CultureInfo.InvariantCulture));
        }

        private string CreateUniqueId()
        {
            return System.Guid.NewGuid().ToString("N");
        }
    }
}

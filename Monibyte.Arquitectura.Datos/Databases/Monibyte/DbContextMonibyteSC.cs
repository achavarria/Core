﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Seguridad;
using Monibyte.Arquitectura.Dominio.Seguridad;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public virtual DbSet<SC_Accion> SC_Accion { get; set; }
        public virtual DbSet<SC_Menu> SC_Menu { get; set; }
        public virtual DbSet<SC_RolesGrupo> SC_RolesGrupo { get; set; }
        public virtual DbSet<SC_AccionesRole> SC_AccionesRole { get; set; }
        public virtual DbSet<SC_MenuEmpresa> SC_MenuEmpresa { get; set; }
        public virtual DbSet<SC_Autorizacion> SC_Autorizacion { get; set; }
        public virtual DbSet<SC_Role> SC_Role { get; set; }
        public virtual DbSet<SC_ActivacionUsuario> SC_ActivacionUsuario { get; set; }

        void CreateContextModelSC(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SC_AccionMap());
            modelBuilder.Configurations.Add(new SC_AccionesRoleMap());
            modelBuilder.Configurations.Add(new SC_ActivacionUsuarioMap());
            modelBuilder.Configurations.Add(new SC_AutorizacionMap());
            modelBuilder.Configurations.Add(new SC_MenuMap());
            modelBuilder.Configurations.Add(new SC_MenuEmpresaMap());
            modelBuilder.Configurations.Add(new SC_RoleMap());
            modelBuilder.Configurations.Add(new SC_RolesGrupoMap());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public class EncabezadoEstCuenta
    {
        public string TipoTarjeta { get; set; }
        public string NombreImpreso { get; set; }
        public string Estado { get; set; }
        public decimal? CreditoLocal { get; set; }
        public decimal? CreditoInter { get; set; }
        public decimal? DebitoLocal { get; set; }
        public decimal? DebitoInter { get; set; }
        public decimal? DispAvanceEfectivoLocal { get; set; }
        public decimal? DispAvanceEfectivoInter { get; set; }
        public decimal? PagoVencidoLocal { get; set; }
        public decimal? PagoVencidoInter { get; set; }
        public decimal? DebitosTransitoLocal { get; set; }
        public decimal? DebitosTransitoInter { get; set; }
        public decimal? SaldoAdeudadoLocal { get; set; }
        public decimal? SaldoAdeudadoInter { get; set; }
//----------------------------------------------------------------------------------------------------------
        public int IdEstCuenta { get; set; }
        public int IdDiaCorte { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreCliente { get; set; }
        public string CorreoElectronico { get; set; }
        public string DireccionDeEnvio { get; set; }
        public int PlazoFinanc { get; set; }
        public decimal TasaInteresLocal { get; set; }
        public decimal TasaInteresInter { get; set; }
        public decimal TasaMoraLocal { get; set; }
        public decimal TasaMoraInter { get; set; }
        public DateTime FecCorte { get; set; }
        public decimal LimiteCreditoLocal { get; set; }
        public decimal SaldoCortePrincLocal { get; set; }
        public decimal SaldoDisponibleLocal { get; set; }
        public decimal LimiteCreditoInter { get; set; }
        public decimal SaldoCortePrincInter { get; set; }
        public decimal SaldoDisponibleInter { get; set; }
        public DateTime FecPagoMinimo { get; set; }
        public decimal PagoMinPrincLocal { get; set; }
        public decimal PagoMinPrincInter { get; set; }
        public decimal PagoMinInteresLocal { get; set; }
        public decimal PagoMinInteresInter { get; set; }
        public decimal PagoMinLocal { get; set; }
        public decimal PagoMinInter { get; set; }
        public DateTime FecPagoContado { get; set; }
        public decimal PagoContadoLocal { get; set; }
        public decimal PagoContadoInter { get; set; }
        public decimal InterDiarioMoraLocal { get; set; }
        public decimal InterDiarioMoraInter { get; set; }
        public decimal MontoMoraLocal { get; set; }
        public decimal MontoMoraInter { get; set; }
        public decimal InteresMoraLocal { get; set; }
        public decimal InteresMoraInter { get; set; }
        public decimal OtrosCargosLocal { get; set; }
        public decimal OtrosCargosInter { get; set; }
        public DateTime FecCorteAnterior { get; set; }
        public decimal SaldoAntInteresLocal { get; set; }
        public decimal SaldoAntInteresInter { get; set; }
        public decimal SaldoAnteriorLocal { get; set; }
        public decimal SaldoAnteriorInter { get; set; }
        public decimal SaldoIniPremCorte { get; set; }
        public decimal DebitosPremCorte { get; set; }
        public decimal CreditosPremCorte { get; set; }
        public decimal PuntosPorRedimir { get; set; }
        public decimal PuntosDelCorte { get; set; }
        public string ProgramaPuntos { get; set; }
        public decimal LimiteExtraf { get; set; }
        public decimal SobregiroLocal { get; set; }
        public decimal SobregiroInter { get; set; }
        public string Observaciones { get; set; }
        public string ValParametro { get; set; }
        public int? IdCuentaMadre { get; set; }
        public string Descripcion { get; set; }
        public int IdMarca { get; set; }
        public int IdCompania { get; set; }
        public decimal? ResConsumoLocal { get; set; }
        public decimal? ResConsumoInter { get; set; }
        public decimal? ResInteresLocal { get; set; }
        public decimal? ResInteresInter { get; set; }
        public decimal? ResOtrosCargosLocal { get; set; }
        public decimal? ResOtrosCargosInter { get; set; }
        public decimal? SaldoCorteLocal { get; set; }
        public decimal? SaldoCorteInter { get; set; }
        public int IdAplicaMillas { get; set; }

    }
}

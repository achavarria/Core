using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Autorizador
{
    public partial class MB_Emisor : EntidadBaseCtrl
    {
        public int IdEmisor { get; set; }
        public string CodEmisor { get; set; }
        public string Descripcion { get; set; }
        public string CodPaisLocal { get; set; }
        public string CodMonedaLocal { get; set; }
        public string CodMonedaInternacional { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdValPais { get; set; }
        public int IdValPostEntryMode { get; set; }
        public int IdValPostCondCode { get; set; }
        public int IdValMCC { get; set; }
        public int IdValBIN { get; set; }
        public int IdValChip { get; set; }
        public int IdValFactorInt { get; set; }
        public int IdValCliente { get; set; }
    }
}

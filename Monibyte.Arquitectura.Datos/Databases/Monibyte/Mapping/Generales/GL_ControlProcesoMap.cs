using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ControlProcesoMap : EntityTypeConfiguration<GL_ControlProceso>
    {
        public GL_ControlProcesoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdEjecucion);

            // Properties
            this.Property(t => t.Referencia)
                .HasMaxLength(30);

            this.Property(t => t.DetEjecucion)
                .HasMaxLength(1000);

            this.Property(t => t.DetError)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("GL_ControlProceso", "General");
            this.Property(t => t.IdEjecucion).HasColumnName("IdEjecucion");
            this.Property(t => t.IdProceso).HasColumnName("IdProceso");
            this.Property(t => t.Referencia).HasColumnName("Referencia");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.DetEjecucion).HasColumnName("DetEjecucion");
            this.Property(t => t.DetError).HasColumnName("DetError");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

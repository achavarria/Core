using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_SecuenciaMap : EntityTypeConfiguration<GL_Secuencia>
    {
        public GL_SecuenciaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdSecuencia);

            // Properties
            this.Property(t => t.IdSecuencia)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GL_Secuencia", "General");
            this.Property(t => t.IdSecuencia).HasColumnName("IdSecuencia");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.ValorActual).HasColumnName("ValorActual");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.Incremento).HasColumnName("Incremento");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

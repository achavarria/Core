﻿using System;
using Monibyte.Arquitectura.Aplicacion.Seguridad;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Seguridad
{
    public class ServicioRoles : ServicioUnity, IServicioRoles
    {
        private IRolesApp _appRoles;
        public ServicioRoles(IRolesApp appRoles)
        {
            _appRoles = appRoles;
        }

        public Transporte ExisteRol(Transporte transporte)
        {
            var codRol = transporte.GetDatos<string>();
            var resultado = _appRoles.ExisteRol(Int32.Parse(codRol));
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerRolesPorUsuario(Transporte transporte)
        {
            var idUsuario = transporte.GetDatos<string>();
            var intIdUsuario = Int32.Parse(idUsuario);
            var resultado = _appRoles.ObtenerRolesPorUsuario(intIdUsuario);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaRoles(Transporte transporte)
        {
            var resultado = _appRoles.ListaRoles();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerRolesTipoUsuario(Transporte transporte)
        {
            var idTipoUsuario = transporte.GetValor<int>("idTipoUsuario");
            var resultado = _appRoles.ObtenerRolesTipoUsuario(idTipoUsuario);
            transporte.SetDatos(resultado);
            return transporte;
        }
    }
}

using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Contabilidad;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tesoreria
{
    public partial class TS_Moneda : EntidadBase
    {
        public int IdMoneda { get; set; }
        public string Descripcion { get; set; }
        public string Simbolo { get; set; }
        public string CodInternacional { get; set; }
        public int IdEstado { get; set; }
        public string OperadorConversion { get; set; }
        public string CodigoNormativa { get; set; }
        public string NumInternacional { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string CodAlpha2 { get; set; }
    }
}

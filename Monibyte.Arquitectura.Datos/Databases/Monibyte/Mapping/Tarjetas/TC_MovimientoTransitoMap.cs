using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_MovimientoTransitoMap : EntityTypeConfiguration<TC_MovimientoTransito>
    {
        public TC_MovimientoTransitoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMovimiento);

            // Properties
            this.Property(t => t.IdMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumReferencia)
                .HasMaxLength(18);

            this.Property(t => t.Establecimiento)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TC_MovimientoTransito", "TCredito");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.FecAutorizacion).HasColumnName("FecAutorizacion");
            this.Property(t => t.NumReferencia).HasColumnName("NumReferencia");
            this.Property(t => t.Establecimiento).HasColumnName("Establecimiento");
            this.Property(t => t.MonAutorizado).HasColumnName("MonAutorizado");
            this.Property(t => t.IdMonedaAutorizacion).HasColumnName("IdMonedaAutorizacion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

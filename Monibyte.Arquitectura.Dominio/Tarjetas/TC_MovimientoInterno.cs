using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_MovimientoInterno : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public int IdUsuario { get; set; }
        public int IdTarjeta { get; set; }
        public string IdTipoMovimiento { get; set; }
        public string NumTarjeta { get; set; }
        public Nullable<System.DateTime> FecTransaccion { get; set; }
        public Nullable<System.DateTime> HoraTransaccion { get; set; }
        public string Descripcion { get; set; }
        public decimal MonTransaccion { get; set; }
        public Nullable<System.DateTime> FecInclusionAud { get; set; }
        public Nullable<int> IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

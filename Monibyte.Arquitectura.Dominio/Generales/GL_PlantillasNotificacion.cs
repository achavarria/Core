using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_PlantillasNotificacion : EntidadBase
    {
        public int IdPlantilla { get; set; }
        public int IdIdioma { get; set; }
        public string Descripcion { get; set; }
        public int IdAplicaA { get; set; }
        public int IdOrigen { get; set; }
        public string Asunto { get; set; }
        public string Cuerpo { get; set; }
        public string NomAdjunto { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

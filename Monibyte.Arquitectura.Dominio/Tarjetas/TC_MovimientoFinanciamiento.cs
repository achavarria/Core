using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_MovimientoFinanciamiento : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public int IdFinanciamiento { get; set; }
        public Nullable<System.DateTime> FecMovimiento { get; set; }
        public Nullable<System.DateTime> HoraMovimiento { get; set; }
        public Nullable<decimal> MonMoratorio { get; set; }
        public Nullable<int> DiasMoratorio { get; set; }
        public Nullable<decimal> TasaMoratorio { get; set; }
        public Nullable<int> IdCuota { get; set; }
        public Nullable<int> IdUsuarioMovimiento { get; set; }
        public Nullable<decimal> MonPrincipal { get; set; }
        public Nullable<decimal> MonInteres { get; set; }
        public Nullable<decimal> MonSeguro { get; set; }
        public Nullable<int> IdTipoMovimiento { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<decimal> MonPrincipalPagado { get; set; }
        public Nullable<decimal> MonInteresPagado { get; set; }
        public Nullable<decimal> MonSeguroPagado { get; set; }
        public Nullable<decimal> MonMoratorioPagado { get; set; }
        public Nullable<System.DateTime> FecAnulacion { get; set; }
        public Nullable<int> IdUsuarioAnula { get; set; }
        public Nullable<System.DateTime> HoraAnulacion { get; set; }
        public Nullable<int> IdMotivoAnulacion { get; set; }
        public string DetalleAnulacion { get; set; }
        public Nullable<System.DateTime> FecCuota { get; set; }
        public Nullable<int> IdMovimientoOrigen { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

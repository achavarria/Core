using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_PaisMap : EntityTypeConfiguration<GL_Pais>
    {
        public GL_PaisMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPais);

            // Properties
            this.Property(t => t.IdPais)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Nacionalidad)
                .HasMaxLength(60);

            this.Property(t => t.CodISO)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.CodISOAlpha2)
                .HasMaxLength(2);

            this.Property(t => t.CodISOAlpha3)
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("GL_Pais", "General");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.Nacionalidad).HasColumnName("Nacionalidad");
            this.Property(t => t.CodISO).HasColumnName("CodISO");
            this.Property(t => t.CodISOAlpha2).HasColumnName("CodISOAlpha2");
            this.Property(t => t.CodISOAlpha3).HasColumnName("CodISOAlpha3");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

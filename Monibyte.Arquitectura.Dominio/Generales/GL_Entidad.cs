using System;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Entidad : EntidadBase
    {
       public int IdEntidad { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int IdEsEmisor { get; set; }
        public string CodigoBIC { get; set; }
        public int IdPais { get; set; }
        public Nullable<int> IdTipoEntidad { get; set; }
        public string CodSuperFranquicia { get; set; }
        public string CodEmisor { get; set; }
        public Nullable<int> IdPersona { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdMonedaLocal { get; set; }
        public Nullable<int> IdMonedaInternacional { get; set; }
    }
}

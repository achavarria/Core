﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio.Autorizador;
using Monibyte.Arquitectura.Poco.Configuracion;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public interface IAppConfiguracionCtrl : IAdmCondicion
    {
        DataResult<PocGrupoEmpresaCatalogo> ObtenerGruposEmpresaCatalogo(PocConsultaGrupoEmpresa consulta, DataRequest request);
        DataResult<PocTarjetaGrupoCatalogo> ObtenerTarjetasGrupoCatalogo(PocConsultaGrupoEmpresa consulta, DataRequest request);
        void EstablecerCondicionPorDefecto(int idEmpresa, string numTarjeta);
        void PermitirConsumoInternet(int idCondicion, bool consumeInternet);
        void ResetCategorias(List<PocMccCondicion> condiciones, int idMoneda);
        void RestriccionesDesactivadas(string numTarjeta, ref bool restriccionesDesactivadas, ref int vigencia, ref int tiempoRemanente);
        bool VerificaEsVip(string numeroTarjeta);
        MB_CondicionTarjeta RestriccionesTarjeta(string numTarjeta);

        PocHorarioCondicion VerificaDetalleVip(string numeroTarjeta);
    }
}

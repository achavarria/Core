﻿using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte
{
    public class RepositorioCtrl<TEntity> : Repositorio<TEntity>,
        IRepositorioCtrl<TEntity> where TEntity : EntidadBaseCtrl
    {
        public RepositorioCtrl(IUnidadTbjoCtrlMonibyte unidadTbjo)
            : base(unidadTbjo) { }
    }
}

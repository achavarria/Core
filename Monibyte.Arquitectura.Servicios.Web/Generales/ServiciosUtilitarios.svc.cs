﻿using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Servicios.Nucleo;

namespace Monibyte.Arquitectura.Servicios.Web.Generales
{
    public class ServiciosUtilitarios : ServicioUnity, IServiciosUtilitarios
    {
        private IAppUtilitarios _appUtilitarios;
        public ServiciosUtilitarios(IAppUtilitarios appUtilitarios)
        {
            _appUtilitarios = appUtilitarios;
        }

        public Transporte ConsultarIdiomas(Transporte transporte)
        {
            var resultado = _appUtilitarios.ListaIdiomas();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ConsultarPaises(Transporte transporte)
        {
            var resultado = _appUtilitarios.ListaPaises();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaTipoIdentificacion(Transporte transporte)
        {
            var resultado = _appUtilitarios.ListaTipoIdentificacion();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ConsultarInfoValorLista(Transporte transporte)
        {
            var idLista = transporte.GetDatos<int>();
            var resultado = _appUtilitarios.ObtieneInfoValorLista(idLista);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ConsultarInfoLista(Transporte transporte)
        {
            var nombreLista = transporte.GetDatos<string>();
            int?[] excluir = transporte.GetValor<int?[]>("excluir");
            var ref1 = transporte.GetValor<string>("ref1");
            var ref2 = transporte.GetValor<string>("ref2");
            var ref3 = transporte.GetValor<string>("ref3");
            var ref4 = transporte.GetValor<string>("ref4");
            var ref5 = transporte.GetValor<string>("ref5");
            var resultado = _appUtilitarios.ObtieneInfoLista(nombreLista, excluir, ref1, ref2, ref3, ref4, ref5);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte RolPorTipoUsuario(Transporte transporte)
        {
            var idTipoUsuario = transporte.GetValor<int>("idTipoUsuario");
            var resultado = _appUtilitarios.RolPorTipoUsuario(idTipoUsuario);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public void EnviarCorreo(Transporte transporte)
        {
            var sender = transporte.GetValor<SenderConfig>("sender");
            var email = transporte.GetValor<EmailConfig>("email");
            _appUtilitarios.EnviarEmail(sender, email);
        }

        public void EnviarNotificacion(Transporte transporte)
        {
            var from = transporte.GetValor<string>("from");
            var to = transporte.GetValor<string>("to");
            var cc = transporte.GetValor<string>("cc");
            var bcc = transporte.GetValor<string>("bcc");
            var idPlantilla = transporte.GetValor<int>("idPlantilla");
            var subject = transporte.GetValor<string>("subject");
            var body = transporte.GetValor<string>("body");
            var subjectParams = transporte.GetValor<object[]>("subjectParams");
            var bodyParams = transporte.GetValor<object[]>("bodyParams");
            _appUtilitarios.EnviarNotificacion(from, to, cc, bcc, idPlantilla, subject, body, subjectParams, bodyParams);
        }

        public Transporte ObtenerSistemaPorTipoUsuario(Transporte transporte)
        {
            var idTipoUsuario = transporte.GetValor<int>("idTipoUsuario");
            var resultado = _appUtilitarios.ObtenerSistemaPorTipoUsuario(idTipoUsuario);
            transporte.SetDatos(resultado);
            return transporte;
        }
    }
}

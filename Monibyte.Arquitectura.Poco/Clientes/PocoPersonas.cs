﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Monibyte.Arquitectura.Poco.Clientes
{
    public class PocInfoPersona
    {
        public int IdPersona { get; set; }
        public int IdIdentificacion { get; set; }
        public int IdTipoIdentificacion { get; set; }
        public int? IdTipoRelacion { get; set; }
        public int? IdEjecutivo { get; set; }
        public int IdEstado { get; set; }
        public int? IdProfesion { get; set; }
        public int IdTipoPersona { get; set; }
        public int? IdGenero { get; set; }
        public int IdFallecido { get; set; }
        public int? IdEstadoCivil { get; set; }
        public int IdPaisNacimiento { get; set; }
        public string NumIdentificacion { get; set; }
        public string TipoIdentificacion { get; set; }
        public string Nombre { get; set; }
        public string NombreCompleto { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string RazonSocial { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        //public string Correo { get; set; }
        public string Empresa { get; set; }
        public DateTime? FecNacimiento { get; set; }
        public DateTime FecIngreso { get; set; }
        public DateTime FecFallecido { get; set; }
        public DateTime FecVenceIdentificacion { get; set; }
        public List<PocComunicacion> Comunicacion { get; set; }
        public List<PocDireccion> Direccion { get; set; }
    }

    public class PocComunicacion
    {
        public int IdComunicacion { get; set; }
        public string ValComunicacion { get; set; }
        public int IdEstado { get; set; }
        public int IdTipo { get; set; }
        public int? IdUbicacion { get; set; }
        public int IdPersona { get; set; }
        public string DescripcionTipo { get; set; }
        public string DescripcionUbicacion { get; set; }
        public bool Borrado { get; set; }
        public int IdOrigenProcesador { get; set; }
    }

    public class PocDireccion
    {
        public int IdDireccion { get; set; }
        public string DescripcionTipo { get; set; }
        public int IdTipo { get; set; }
        public int IdPais { get; set; }
        public string Pais { get; set; }
        public int IdProvincia { get; set; }
        public string Provincia { get; set; }
        public int IdCanton { get; set; }
        public string Canton { get; set; }
        public int IdDistrito { get; set; }
        public string Distrito { get; set; }
        public string Direccion { get; set; }
        public int IdEstado { get; set; }
        public int IdPersona { get; set; }
        public bool Borrado { get; set; }

    }

    public class PocEmpresa
    {
        public int IdPersona { get; set; }
        public string NombreCompleto { get; set; }
    }

    public class PocEmpresasUsuario : IEquatable<PocEmpresasUsuario>
    {
        public int IdRole { get; set; }
        public int IdUsuario { get; set; }
        public int IdEmpresa { get; set; }
        public int IdDefault { get; set; }
        public int IdTipoUsuario { get; set; }
        public int? IdRoleOperativo { get; set; }
        public int IdRequiereAutorizacion { get; set; }
        public string NombreEmpresa { get; set; }

        public bool Equals(PocEmpresasUsuario o)
        {
            return this.IdEmpresa == o.IdEmpresa &&
                this.IdRole == o.IdRole &&
                this.IdTipoUsuario == o.IdTipoUsuario &&
                this.IdDefault == o.IdDefault;
        }
    }

    public class PocPerfilEmpresa
    {
        public int IdEmpresa { get; set; }
        public string FormatoFecha { get; set; }
        public string SepDecimal { get; set; }
        public string SepMiles { get; set; }
        public int NumDecimales { get; set; }
        public int IdValidaEdicionMov { get; set; }
        public int IdDesmarcableMovLiq { get; set; }
        public int? IdRepMovTC { get; set; }
        public int IdInfDenegaciones { get; set; }
        public byte[] LogoEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
        public int IdRequiereAutorizacion { get; set; }
        public int? IdEnviaEstCtaIndividual { get; set; }
        public int IdIncluyeDebCred { get; set; }
        public int IdPermiteDetalleTarjeta { get; set; }
        public PocImagenEmpresa Imagen { get; set; }
    }

    public class PocImagenEmpresa
    {
        public byte[] Data { get; set; }
        public string MimeType { get; set; }
        public string Extension { get; set; }
    }
}

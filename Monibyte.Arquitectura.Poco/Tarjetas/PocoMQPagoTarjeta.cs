﻿using System;
namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocEntradaPagoTarjeta
    {
        private string Emisor { get; set; }
        private string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public int MonedaISO { get; set; }
        public string TipoPago { get; set; }
        public decimal Monto { get; set; }
        public string CodBancoEmisor { get; set; }
        public string NumCheque { get; set; }
        public string NumFinanEspecial { get; set; }
        public string IdTransaccion { get; set; }
        public string PlazaCheques { get; set; }
        public string NumReferencia { get; set; }
        public string CodExtraFinanciamiento { get; set; }
        public string Usuario { get; set; }
    }

    public class PocSalidaPagoTarjeta
    {
        public string TipoMensaje { get; set; }
        public string Emisor { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string SuperFranquicia { get; set; }
        public string Localidad { get; set; }
        public int Moneda { get; set; }
        public string TipoPago { get; set; }
        public decimal Monto { get; set; }
        public string CodBancoEmisor { get; set; }
        public string NumCheque { get; set; }
        public string NumFinanEspecial { get; set; }
        public string IdTransaccion { get; set; }
        public string CodigoRespuesta { get; set; }
        public string NumReferencia { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }

    public class PocEntradaPagoExtraFinanciamiento
    {
        public string NumTarjeta { get; set; }
        public decimal Cantidad { get; set; }
        public string CodPlan { get; set; }
        public string CodMovimiento { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
    }

    public class PocSalidaPagoExtraFinanciamiento
    {
        public string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public decimal Cantidad { get; set; }
        public string CodPlan { get; set; }
        public string CodMovimiento { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Procesador;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Procesador
{
    public interface IAppReportes : IAplicacionBase
    {
        byte[] ObtieneReporteDenegacion(string numTarjeta, string numCuenta, string usuario,
            DateTime FecDesde, DateTime FecHasta, int? idEmpresa);
        void EnviarReporteEmpresa(List<PocDenegacionesDiarias> datosEntrada);
    }
}

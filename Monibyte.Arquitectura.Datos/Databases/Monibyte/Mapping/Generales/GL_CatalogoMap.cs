using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_CatalogoMap : EntityTypeConfiguration<GL_Catalogo>
    {
        public GL_CatalogoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCatalogo);

            // Properties
            this.Property(t => t.IdCatalogo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Lista)
                .HasMaxLength(30);

            this.Property(t => t.Codigo)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Referencia1)
                .HasMaxLength(20);

            this.Property(t => t.Referencia2)
                .HasMaxLength(20);

            this.Property(t => t.Referencia3)
                .HasMaxLength(20);

            this.Property(t => t.Referencia4)
                .HasMaxLength(20);

            this.Property(t => t.Referencia5)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("GL_Catalogo", "General");
            this.Property(t => t.IdCatalogo).HasColumnName("IdCatalogo");
            this.Property(t => t.Lista).HasColumnName("Lista");
            this.Property(t => t.Codigo).HasColumnName("Codigo");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.Orden).HasColumnName("Orden");
            this.Property(t => t.Referencia1).HasColumnName("Referencia1");
            this.Property(t => t.Referencia2).HasColumnName("Referencia2");
            this.Property(t => t.Referencia3).HasColumnName("Referencia3");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.Referencia4).HasColumnName("Referencia4");
            this.Property(t => t.Referencia5).HasColumnName("Referencia5");
        }
    }
}

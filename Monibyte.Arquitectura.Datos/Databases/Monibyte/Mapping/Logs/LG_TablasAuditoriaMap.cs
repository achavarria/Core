using Monibyte.Arquitectura.Dominio.Logs;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Logs
{
    public class LG_TablasAuditoriaMap : EntityTypeConfiguration<LG_TablasAuditoria>
    {
        public LG_TablasAuditoriaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTabla);

            // Properties
            this.Property(t => t.IdTabla)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.BaseDatos)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Esquema)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Tabla)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Descripcion)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("LG_TablasAuditoria", "Logs");
            this.Property(t => t.IdTabla).HasColumnName("IdTabla");
            this.Property(t => t.BaseDatos).HasColumnName("BaseDatos");
            this.Property(t => t.Esquema).HasColumnName("Esquema");
            this.Property(t => t.Tabla).HasColumnName("Tabla");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdAudInserta).HasColumnName("IdAudInserta");
            this.Property(t => t.IdAudActualiza).HasColumnName("IdAudActualiza");
            this.Property(t => t.IdAudBorra).HasColumnName("IdAudBorra");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

﻿using Monibyte.Arquitectura.Aplicacion.Quickpass;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Quickpass;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Quickpass
{
    public class ServicioQuickpass : ServicioUnity, IServicioQuickpass
    {
        private IAppQuickpass _appQuickpass;
        private IAppMovimientosQuickpass _appMovimientosQuickpass;
        private IAppTipoQuickpass _appTipoQuickpass;
        private IAppServiciosQuickpass _appServiciosQuickpass;
        private IAppListaTags _appListaTags;

        public ServicioQuickpass(IAppQuickpass appQuickpass, IAppTipoQuickpass appTipoQuickpass, IAppServiciosQuickpass appServiciosQuickpass, IAppMovimientosQuickpass appMovimientosQuickpass, IAppListaTags appListaTags)
        {
            _appQuickpass = appQuickpass;
            _appTipoQuickpass = appTipoQuickpass;
            _appServiciosQuickpass = appServiciosQuickpass;
            _appMovimientosQuickpass = appMovimientosQuickpass;
            _appListaTags = appListaTags;
        }

        public Transporte IncluirTipoQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoQuickpass>();
            _appTipoQuickpass.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarTipoQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoQuickpass>();
            var resultado = _appTipoQuickpass.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaTipoQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoQuickpass>();
            _appTipoQuickpass.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaTipoQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoQuickpass>();
            _appTipoQuickpass.Modificar(entrada);
            return transporte;
        }

        public Transporte ImportarListaQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocObtenerQuickpass>();
            _appServiciosQuickpass.ImportarListaQuickpass(entrada);
            //transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte IncluirQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQuickpass>();
            _appQuickpass.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQuickpass>();
            var resultado = _appQuickpass.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListarMovimientosQP(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQPMovimientos>();
            var resultado = _appMovimientosQuickpass.ListarMovimientosQP(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQuickpass>();
            _appQuickpass.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQuickpass>();
            _appQuickpass.Modificar(entrada);
            return transporte;
        }

        public Transporte ListaTipoQuickpass(Transporte transporte)
        {
            var resultado = _appTipoQuickpass.ListaTipoQuickpass();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListarMovimientosQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQPMovimientos>();
            var resultado = _appMovimientosQuickpass.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ModificaMovimientosQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQPMovimientos>();
            _appMovimientosQuickpass.Modificar(entrada);
            return transporte;
        }

        public Transporte EliminaMovimientosQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQPMovimientos>();
            _appMovimientosQuickpass.Eliminar(entrada);
            return transporte;
        }

        public Transporte IncluirMovimientosQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQPMovimientos>();
            _appMovimientosQuickpass.Insertar(entrada);
            return transporte;
        }

        public Transporte ImportarMovimientosQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocConsultaMovQp>();
            _appServiciosQuickpass.ImportarMovimientosQuickpass(entrada);
            //transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte GenerarCargosMovimientosQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocConsultaMovQp>();
            _appServiciosQuickpass.GenerarCargosMovimientosQuickpass();            
            return transporte;
        }

        public Transporte EnviarListasQP(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocListaQP>();
            _appQuickpass.ActualizaEstadosOPxTarjeta();
            _appServiciosQuickpass.EnviarListasQP(entrada);
            return transporte;
        }

        public Transporte ListarListaTags(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocListaTagsTabla>();
            var resultado = _appListaTags.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ResultadoListasDeTags(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocListaTagsTabla>();
            _appServiciosQuickpass.ResultadoListasDeTags(entrada);
            return transporte;
        }

        public Transporte GenerarCargosCorteQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocConsultaMovQp>();
            var numDia = entrada.NumDia.HasValue ?
                entrada.NumDia.Value : 0;
            var diaPlazo = entrada.diaPlazo.HasValue ?
                entrada.diaPlazo.Value : 0;
            _appServiciosQuickpass.GenerarCargosCorteQuickpass(numDia, diaPlazo);
            return transporte;
        }

        public Transporte EjecutarProcesosQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocConsultaMovQp>();
            _appServiciosQuickpass.EjecutarProcesosQuickpass(entrada);
            return transporte;
        }

        public Transporte ListarQuickpassAsignados(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQuickpass>();
            var resultado = _appQuickpass.ListarQuickpassAsignados(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EditarQuickpass(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocQuickpass>();
            if (entrada.IdTarjeta.HasValue)
            {
                var tarjeta = _appServiciosQuickpass.ObtenerInfoTarjeta((int)entrada.IdTarjeta);
                entrada.NumTarjeta = tarjeta.NumTarjeta;
                entrada.IdCuenta = tarjeta.IdCuenta;
            }
            _appQuickpass.EditarOp(entrada);
            return transporte;
        }

        public Transporte ModificarEstadoQP(Transporte transporte)
        {
            var entrada = transporte.GetDatos<List<PocQuickpass>>();
            _appQuickpass.ModificarEstadoQP(entrada, true);
            return transporte;
        }

        public Transporte ConsultarMontosMovQP(Transporte transporte)
        {
            var filtro = transporte.GetDatos<PocListaQPMontos>();
            transporte.SetDatos(_appMovimientosQuickpass.ConsultarMontosQP(filtro));
            return transporte;
        }

        public Transporte AplicarMontosMovQP(Transporte transporte)
        {
            var filtro = transporte.GetDatos<PocListaQPMontos>();
            _appMovimientosQuickpass.AplicarMontosQP(filtro);
            return transporte;
        }

        //public Transporte ActualizaEstadosOPxTarjeta(Transporte transporte)
        //{
        //    _appQuickpass.ActualizaEstadosOPxTarjeta();
        //    return transporte;
        //}
    }
}

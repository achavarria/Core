﻿using System;
using System.Collections.Generic;
using System.Web;


namespace Monibyte.Arquitectura.Poco.Efectivo
{
    public class PocMovimientosEfectivo
    {
        public int IdMovimiento { get; set; }
        public int IdPersona { get; set; }
        public int IdEmpresa { get; set; }
        public int IdContexto { get; set; }
        public string NumFactura { get; set; }
        public DateTime? FecMovimiento { get; set; }
        public string DetalleMovimiento { get; set; }
        public int? IdMoneda { get; set; }
        public decimal? MonMovimiento { get; set; }
        public int? IdGestion { get; set; }
        public string CentroCosto { get; set; }
        public string CuentaContable { get; set; }
        public int? NumKmSalida { get; set; }
        public int? NumKmLlegada { get; set; }
        public int? NumKmTotal { get; set; }
        public int? IdEstado { get; set; }
        public int? IdEnumerador { get; set; }
        public int IdDebCredito { get; set; }
        public string Accion { get; set; }
        public string DescMoneda { get; set; }
        public string CodMoneda { get; set; }
        public string NombreCompleto { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public List<PocArchivoMovimiento> Archivos { get; set; }
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
        public string DynamicJson { get; set; }
        public bool SoloConsulta { get; set; }
        public string MesMovimiento { get; set; }
        public string AnoMovimiento { get; set; }
        /* Movimientos de Kilometraje */
        public int? IdTipoVehiculo { get; set; }
        public int? Modelo { get; set; }
        public string Placa { get; set; }
        /* Movimientos de Kilometraje */
        public int IdLiquidado { get; set; }
    }

    public class PocGestionKilometraje
    {
        public int IdEmpresa { get; set; }
        public int IdPersona { get; set; }
        public int IdGestion { get; set; }
        public string Detalle { get; set; }
        public DateTime FecInclusion { get; set; }
        public DateTime? FecCierre { get; set; }
        public int? IdUsuarioCierra { get; set; }
        public int? IdUsuarioIncluye { get; set; }
        public int IdTipoVehiculo { get; set; }
        public int Modelo { get; set; }
        public string Placa { get; set; }
        public int IdEstado { get; set; }
        public string DescEstado { get; set; }
        public int IdCompania { get; set; }
        public List<PocMovimientosEfectivo> ListaMovimientosKM { get; set; }
        public string NombrePersona { get; set; }
        public string NumIdentificacion { get; set; }
        public string NombreCentroCosto { get; set; }
        public string DescTipoVehiculo { get; set; }
        public string NumTarjeta { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public int IdDebCredito { get; set; }
        public string DynamicJson { get; set; }
        public bool SoloConsulta { get; set; }

        public bool Equals(PocGestionKilometraje o)
        {
            return this.Modelo == o.Modelo &&
                this.IdTipoVehiculo == o.IdTipoVehiculo;
        }
    }

    public class PocParametrosGestionKM
    {
        public int IdTipoVehiculo { get; set; }
        public string DescTipoVehiculo { get; set; }
        public int Modelo { get; set; }
        public string Placa { get; set; }
    }

    public class PocArchivoMovimiento
    {
        public int IdArchivo { get; set; }
        public int IdMovimiento { get; set; }
        public string TipoArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public byte[] DatosArchivo { get; set; }
        public int IdEstado { get; set; }
        public string Accion { get; set; }
    }

    public class PocTarifaKilometraje
    {
        public int IdTarifa { get; set; }
        public int Antiguedad { get; set; }
        public int IdTipoVehiculo { get; set; }
        public string TipoVehiculo { get; set; }
        public decimal Monto { get; set; }
        public int? IdEstado { get; set; }
        public int IdEmpresa { get; set; }
    }

    public class PocArchivoTarjeta
    {
        public int IdArchivo { get; set; }
        public int IdMovimiento { get; set; }
        public string TipoArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public byte[] DatosArchivo { get; set; }
        public int IdEstado { get; set; }
        public string Accion { get; set; }
    }
}



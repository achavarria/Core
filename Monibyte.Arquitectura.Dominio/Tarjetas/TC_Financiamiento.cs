using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_Financiamiento : EntidadBase
    {
        public int IdFinanciamiento { get; set; }
        public Nullable<int> IdTipo { get; set; }
        public Nullable<System.DateTime> FecApertura { get; set; }
        public Nullable<decimal> PlazoMeses { get; set; }
        public Nullable<int> IdUsuarioApertura { get; set; }
        public Nullable<System.DateTime> FecVencimiento { get; set; }
        public Nullable<decimal> TasaInteres { get; set; }
        public Nullable<decimal> TasaMoratorio { get; set; }
        public Nullable<int> IdCuenta { get; set; }
        public Nullable<decimal> MonApertura { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<int> IdMoneda { get; set; }
        public Nullable<decimal> MonSaldo { get; set; }
        public Nullable<decimal> MonCuota { get; set; }
        public Nullable<decimal> FecUltPago { get; set; }
        public Nullable<int> IdPeriodicidad { get; set; }
        public int DiasGracia { get; set; }
        public int PeriodoGracia { get; set; }
        public int DiaDePago { get; set; }
        public int IdTipoCuota { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

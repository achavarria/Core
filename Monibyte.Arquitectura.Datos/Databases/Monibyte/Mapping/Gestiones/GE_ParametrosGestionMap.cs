using Monibyte.Arquitectura.Dominio.Gestiones;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Gestiones
{
    public class GE_ParametrosGestionMap : EntityTypeConfiguration<GE_ParametrosGestion>
    {
        public GE_ParametrosGestionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdParamGestion);

            // Properties
            this.Property(t => t.IdParamGestion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ValorDefecto)
                .HasMaxLength(120);

            // Table & Column Mappings
            this.ToTable("GE_ParametrosGestion", "Gestion");
            this.Property(t => t.IdParamGestion).HasColumnName("IdParamGestion");
            this.Property(t => t.IdParametro).HasColumnName("IdParametro");
            this.Property(t => t.IdTipoGestion).HasColumnName("IdTipoGestion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdRequerido).HasColumnName("IdRequerido");
            this.Property(t => t.ValorDefecto).HasColumnName("ValorDefecto");
            this.Property(t => t.IdTipo).HasColumnName("IdTipo");
            this.Property(t => t.IdFormato).HasColumnName("IdFormato");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

﻿using System;
namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocEntradaMiscelaneosTarjeta
    {
        public string NumTarjeta { get; set; }
        public DateTime FechaConsumo { get; set; }
        public decimal MontoTransaccion { get; set; }
        public string MonedaTransaccion { get; set; }
        public string CodMovimiento { get; set; }
        public string NumReferencia { get; set; }
        public string Localidad { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
    }

    public class PocSalidaMiscelaneosTarjeta
    {
        public string TipoMensaje { get; set; }
        public string NumTarjeta { get; set; }
        public DateTime FechaConsumo { get; set; }
        public decimal MontoTransaccion { get; set; }
        public string MonedaTransaccion { get; set; }
        public string CodMovimiento { get; set; }
        public string NumReferencia { get; set; }
        public string Localidad { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
        public string NumReferenciaSiscard { get; set; }
    }
}

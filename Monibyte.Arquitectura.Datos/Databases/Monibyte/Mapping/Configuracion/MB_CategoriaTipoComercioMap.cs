using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_CategoriaTipoComercioMap : EntityTypeConfiguration<MB_CategoriaTipoComercio>
    {
        public MB_CategoriaTipoComercioMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCategoria);

            // Properties
            this.Property(t => t.IdCategoria)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("MB_CategoriaTipoComercio", "Mnb");
            this.Property(t => t.IdCategoria).HasColumnName("IdCategoria");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdEsGrupo).HasColumnName("IdEsGrupo");
        }
    }
}

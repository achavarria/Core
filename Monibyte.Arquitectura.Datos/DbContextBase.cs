﻿using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos
{
    public class DbContextBase : DbContext
    {
        public DbContextBase(string nameOrConnectionString)
            : base(nameOrConnectionString) { }
    }
}

using Monibyte.Arquitectura.Dominio.Cargos;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Cargos
{
    public class CA_RangosTablaMap : EntityTypeConfiguration<CA_RangosTabla>
    {
        public CA_RangosTablaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTablaRangos);

            // Properties
            this.Property(t => t.IdTablaRangos)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("CA_RangosTabla", "Cargos");
            this.Property(t => t.IdTablaRangos).HasColumnName("IdTablaRangos");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");

        }
    }
}

using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_DetalleEstCuentaExtFinHistMap : EntityTypeConfiguration<TC_DetalleEstCuentaExtFinHist>
    {
        public TC_DetalleEstCuentaExtFinHistMap()
        {
            // Primary Key
            this.HasKey(t => t.IdDetalle);

            // Properties
            this.Property(t => t.IdDetalle)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumeroExtra)
                .HasMaxLength(20);

            this.Property(t => t.Descripcion)
                .HasMaxLength(60);

            this.Property(t => t.Referencia)
                .HasMaxLength(20);

            this.Property(t => t.DetalleMovimiento)
                .HasMaxLength(60);

            // Table & Column Mappings
            this.ToTable("TC_DetalleEstCuentaExtFinHist", "TCredito");
            this.Property(t => t.IdDetalle).HasColumnName("IdDetalle");
            this.Property(t => t.IdEncabezado).HasColumnName("IdEncabezado");
            this.Property(t => t.NumeroExtra).HasColumnName("NumeroExtra");
            this.Property(t => t.FechaTransaccion).HasColumnName("FechaTransaccion");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.SaldoAnterior).HasColumnName("SaldoAnterior");
            this.Property(t => t.MonPrincipal).HasColumnName("MonPrincipal");
            this.Property(t => t.SaldoAlCorte).HasColumnName("SaldoAlCorte");
            this.Property(t => t.FechaMovimiento).HasColumnName("FechaMovimiento");
            this.Property(t => t.Referencia).HasColumnName("Referencia");
            this.Property(t => t.DetalleMovimiento).HasColumnName("DetalleMovimiento");
            this.Property(t => t.MontoCuota).HasColumnName("MontoCuota");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");

        }
    }
}

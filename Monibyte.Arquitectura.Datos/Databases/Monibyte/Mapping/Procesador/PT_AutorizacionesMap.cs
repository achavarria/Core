using Monibyte.Arquitectura.Dominio.Procesador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Procesador
{
    public class PT_AutorizacionesMap : EntityTypeConfiguration<PT_Autorizaciones>
    {
        public PT_AutorizacionesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.MessageTypeId)
                .HasMaxLength(5);

            this.Property(t => t.PrimaryBitMap)
                .HasMaxLength(30);

            this.Property(t => t.SecondaryBitMap)
                .HasMaxLength(30);

            this.Property(t => t.PrimaryNumberAccount)
                .HasMaxLength(30);

            this.Property(t => t.ProcessingCode)
                .HasMaxLength(20);

            this.Property(t => t.TransmissionDateTime)
                .HasMaxLength(20);

            this.Property(t => t.SystemTraceAuditNumber)
                .HasMaxLength(20);

            this.Property(t => t.LocalTransactionTime)
                .HasMaxLength(20);

            this.Property(t => t.LocalTransactionDate)
                .HasMaxLength(8);

            this.Property(t => t.SettlementDate)
                .HasMaxLength(8);

            this.Property(t => t.CaptureDate)
                .HasMaxLength(8);

            this.Property(t => t.MerchantCategory)
                .HasMaxLength(5);

            this.Property(t => t.AcquiringCountryCode)
                .HasMaxLength(5);

            this.Property(t => t.PointOfServiceEntryMode)
                .HasMaxLength(5);

            this.Property(t => t.PosConditionCode)
                .HasMaxLength(5);

            this.Property(t => t.AcquiringId)
                .HasMaxLength(20);

            this.Property(t => t.Track2Data)
                .HasMaxLength(60);

            this.Property(t => t.RetrievalNumberReference)
                .HasMaxLength(20);

            this.Property(t => t.CardAcceptorTerminalId)
                .HasMaxLength(30);

            this.Property(t => t.CardAcceptorIdentificationCode)
                .HasMaxLength(30);

            this.Property(t => t.CardAcceptorNameLocation)
                .HasMaxLength(60);

            this.Property(t => t.AdditionalRetailerData)
                .HasMaxLength(60);

            this.Property(t => t.TransactionCurrencyCode)
                .HasMaxLength(5);

            this.Property(t => t.CurrencyCodeCb)
                .HasMaxLength(5);

            this.Property(t => t.AtmTerminalData)
                .HasMaxLength(30);

            this.Property(t => t.AtmCardIssuerAuthorizerData)
                .HasMaxLength(30);

            this.Property(t => t.ReceivingIdCode)
                .HasMaxLength(20);

            this.Property(t => t.AtmTerminalAddressBranchReg)
                .HasMaxLength(60);

            this.Property(t => t.AtmAdditionalData)
                .HasMaxLength(630);

            this.Property(t => t.ResponseCode)
                .HasMaxLength(5);

            this.Property(t => t.AuthorizationIdResponse)
                .HasMaxLength(20);

            this.Property(t => t.AccountIdentification1)
                .HasMaxLength(60);

            this.Property(t => t.AccountIdentification2)
                .HasMaxLength(60);

            this.Property(t => t.OriginalDataElements)
                .HasMaxLength(60);

            this.Property(t => t.ReplacementAmounts)
                .HasMaxLength(60);

            this.Property(t => t.CardNumber)
                .HasMaxLength(30);

            this.Property(t => t.DenegationDetail)
                .HasMaxLength(2000);

            this.Property(t => t.InputParameter)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("PT_Autorizaciones", "ProcesadorTC");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.MessageTypeId).HasColumnName("MessageTypeId");
            this.Property(t => t.PrimaryBitMap).HasColumnName("PrimaryBitMap");
            this.Property(t => t.SecondaryBitMap).HasColumnName("SecondaryBitMap");
            this.Property(t => t.PrimaryNumberAccount).HasColumnName("PrimaryNumberAccount");
            this.Property(t => t.ProcessingCode).HasColumnName("ProcessingCode");
            this.Property(t => t.TransactionAmount).HasColumnName("TransactionAmount");
            this.Property(t => t.TransmissionDateTime).HasColumnName("TransmissionDateTime");
            this.Property(t => t.ConversionRateCb).HasColumnName("ConversionRateCb");
            this.Property(t => t.SystemTraceAuditNumber).HasColumnName("SystemTraceAuditNumber");
            this.Property(t => t.LocalTransactionTime).HasColumnName("LocalTransactionTime");
            this.Property(t => t.LocalTransactionDate).HasColumnName("LocalTransactionDate");
            this.Property(t => t.SettlementDate).HasColumnName("SettlementDate");
            this.Property(t => t.CaptureDate).HasColumnName("CaptureDate");
            this.Property(t => t.MerchantCategory).HasColumnName("MerchantCategory");
            this.Property(t => t.AcquiringCountryCode).HasColumnName("AcquiringCountryCode");
            this.Property(t => t.PointOfServiceEntryMode).HasColumnName("PointOfServiceEntryMode");
            this.Property(t => t.PosConditionCode).HasColumnName("PosConditionCode");
            this.Property(t => t.AcquiringId).HasColumnName("AcquiringId");
            this.Property(t => t.Track2Data).HasColumnName("Track2Data");
            this.Property(t => t.RetrievalNumberReference).HasColumnName("RetrievalNumberReference");
            this.Property(t => t.CardAcceptorTerminalId).HasColumnName("CardAcceptorTerminalId");
            this.Property(t => t.CardAcceptorIdentificationCode).HasColumnName("CardAcceptorIdentificationCode");
            this.Property(t => t.CardAcceptorNameLocation).HasColumnName("CardAcceptorNameLocation");
            this.Property(t => t.AdditionalRetailerData).HasColumnName("AdditionalRetailerData");
            this.Property(t => t.TransactionCurrencyCode).HasColumnName("TransactionCurrencyCode");
            this.Property(t => t.CurrencyCodeCb).HasColumnName("CurrencyCodeCb");
            this.Property(t => t.AtmTerminalData).HasColumnName("AtmTerminalData");
            this.Property(t => t.AtmCardIssuerAuthorizerData).HasColumnName("AtmCardIssuerAuthorizerData");
            this.Property(t => t.ReceivingIdCode).HasColumnName("ReceivingIdCode");
            this.Property(t => t.AtmTerminalAddressBranchReg).HasColumnName("AtmTerminalAddressBranchReg");
            this.Property(t => t.AtmAdditionalData).HasColumnName("AtmAdditionalData");
            this.Property(t => t.ResponseCode).HasColumnName("ResponseCode");
            this.Property(t => t.AuthorizationIdResponse).HasColumnName("AuthorizationIdResponse");
            this.Property(t => t.AccountIdentification1).HasColumnName("AccountIdentification1");
            this.Property(t => t.AccountIdentification2).HasColumnName("AccountIdentification2");
            this.Property(t => t.CardholderBillingAmount).HasColumnName("CardholderBillingAmount");
            this.Property(t => t.OriginalDataElements).HasColumnName("OriginalDataElements");
            this.Property(t => t.ReplacementAmounts).HasColumnName("ReplacementAmounts");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.CardNumber).HasColumnName("CardNumber");
            this.Property(t => t.DenegationId).HasColumnName("DenegationId");
            this.Property(t => t.DenegationDetail).HasColumnName("DenegationDetail");
            this.Property(t => t.InputParameter).HasColumnName("InputParameter");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Logs;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Logs
{
    public class LG_BitacoraPanelControlMap : EntityTypeConfiguration<LG_BitacoraPanelControl>
    {
        public LG_BitacoraPanelControlMap()
        {
            // Primary Key
            this.HasKey(t => t.IdBitacora);

            // Properties
            this.Property(t => t.NumTarjeta)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("LG_BitacoraPanelControl", "Logs");
            this.Property(t => t.IdBitacora).HasColumnName("IdBitacora");
            this.Property(t => t.IdCondicion).HasColumnName("IdCondicion");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.DatosBitacora).HasColumnName("DatosBitacora");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

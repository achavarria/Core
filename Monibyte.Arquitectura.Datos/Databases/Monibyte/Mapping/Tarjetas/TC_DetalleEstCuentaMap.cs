using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_DetalleEstCuentaMap : EntityTypeConfiguration<TC_DetalleEstCuenta>
    {
        public TC_DetalleEstCuentaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdDetEstCuenta);

            // Properties
            this.Property(t => t.IdDetEstCuenta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumTarjeta)
                .HasMaxLength(20);

            this.Property(t => t.NomTarjetahabiente)
                .HasMaxLength(100);

            this.Property(t => t.DetMovimiento)
                .HasMaxLength(120);

            this.Property(t => t.Ciudad)
                .HasMaxLength(60);

            this.Property(t => t.Pais)
                .HasMaxLength(10);

            this.Property(t => t.IndDebCredito)
                .HasMaxLength(1);

            this.Property(t => t.CodigoMovimiento)
                .HasMaxLength(5);

            this.Property(t => t.NumAutorizacion)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("TC_DetalleEstCuenta", "TCredito");
            this.Property(t => t.IdDetEstCuenta).HasColumnName("IdDetEstCuenta");
            this.Property(t => t.IdEstCuenta).HasColumnName("IdEstCuenta");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.NomTarjetahabiente).HasColumnName("NomTarjetahabiente");
            this.Property(t => t.FecTransaccion).HasColumnName("FecTransaccion");
            this.Property(t => t.DetMovimiento).HasColumnName("DetMovimiento");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.MonTransaccion).HasColumnName("MonTransaccion");
            this.Property(t => t.Ciudad).HasColumnName("Ciudad");
            this.Property(t => t.Pais).HasColumnName("Pais");
            this.Property(t => t.MonPrincipalLocal).HasColumnName("MonPrincipalLocal");
            this.Property(t => t.MonPrincipalInter).HasColumnName("MonPrincipalInter");
            this.Property(t => t.InteresesLocal).HasColumnName("InteresesLocal");
            this.Property(t => t.InteresesInter).HasColumnName("InteresesInter");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IndDebCredito).HasColumnName("IndDebCredito");
            this.Property(t => t.CodigoMovimiento).HasColumnName("CodigoMovimiento");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
            this.Property(t => t.MonCapital).HasColumnName("MonCapital");
            this.Property(t => t.MonIntereses).HasColumnName("MonIntereses");
            this.Property(t => t.MonOtrosCargos).HasColumnName("MonOtrosCargos");
            this.Property(t => t.MonMoratorios).HasColumnName("MonMoratorios");
            this.Property(t => t.NumAutorizacion).HasColumnName("NumAutorizacion");
        }
    }
}

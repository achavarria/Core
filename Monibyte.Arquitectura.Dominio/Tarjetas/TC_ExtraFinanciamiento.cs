﻿using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_ExtraFinanciamiento : EntidadBase
    {
        public int IdExtraFinanciamiento { get; set; }
        public int NumExtraFinanciamiento { get; set; }
        public int IdCuenta { get; set; }
        public Nullable<int> Consecutivo { get; set; }
        public Nullable<int> CodigoMovimiento { get; set; }
        public Nullable<decimal> TasaDeInteres { get; set; }
        public Nullable<int> PlazoEnMeses { get; set; }
        public Nullable<System.DateTime> FechaAutorizacion { get; set; }
        public int IdMoneda { get; set; }
        public decimal SaldoInicial { get; set; }
        public decimal MontoCobradoInter { get; set; }
        public decimal MontoCobradoLocal { get; set; }
        public decimal Debitos { get; set; }
        public decimal CuotaMensual { get; set; }
        public int CuotasCobradas { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public virtual TC_Cuenta TC_Cuenta { get; set; }
        public virtual TS_Moneda TS_Moneda { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Clientes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Clientes
{
    public class CL_vwEmpresasUsuarioMap : EntityTypeConfiguration<CL_vwEmpresasUsuario>
    {
        public CL_vwEmpresasUsuarioMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdEmpresa, t.IdTipoPersonaEmpresa, t.IdUsuario, t.Correo, t.IdIdiomaPreferencia, t.IdTipoUsuario, t.IdRole, t.IdDefault, t.IdEstado, t.IdPersona, t.IdCompania, t.IdUnidad });

            // Properties
            this.Property(t => t.IdEmpresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NombreEmpresa)
                .HasMaxLength(160);

            this.Property(t => t.IdentificacionEmpresa)
                .HasMaxLength(30);

            this.Property(t => t.IdTipoPersonaEmpresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdUsuario)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodUsuario)
                .HasMaxLength(20);

            this.Property(t => t.AliasUsuario)
                .HasMaxLength(100);

            this.Property(t => t.Correo)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.IdIdiomaPreferencia)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdTipoUsuario)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdRole)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdDefault)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdEstado)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdPersona)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodUsuarioEmpresa)
                .HasMaxLength(20);

            this.Property(t => t.IdCompania)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdUnidad)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CL_vwEmpresasUsuario", "Cliente");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.NombreEmpresa).HasColumnName("NombreEmpresa");
            this.Property(t => t.IdentificacionEmpresa).HasColumnName("IdentificacionEmpresa");
            this.Property(t => t.IdTipoPersonaEmpresa).HasColumnName("IdTipoPersonaEmpresa");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.AliasUsuario).HasColumnName("AliasUsuario");
            this.Property(t => t.Correo).HasColumnName("Correo");
            this.Property(t => t.IdIdiomaPreferencia).HasColumnName("IdIdiomaPreferencia");
            this.Property(t => t.IdTipoUsuario).HasColumnName("IdTipoUsuario");
            this.Property(t => t.IdRole).HasColumnName("IdRole");
            this.Property(t => t.IdRoleOperativo).HasColumnName("IdRoleOperativo");
            this.Property(t => t.IdDefault).HasColumnName("IdDefault");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.CodUsuarioEmpresa).HasColumnName("CodUsuarioEmpresa");
            this.Property(t => t.IdCompania).HasColumnName("IdCompania");
            this.Property(t => t.IdUnidad).HasColumnName("IdUnidad");
        }
    }
}

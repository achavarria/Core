using Monibyte.Arquitectura.Dominio.Autorizador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador
{
    public class MB_TipoTarjetaMap : EntityTypeConfiguration<MB_TipoTarjeta>
    {
        public MB_TipoTarjetaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipoTarjeta);

            // Properties
            this.Property(t => t.IdTipoTarjeta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.CodMoneda)
                .IsRequired()
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("MB_TipoTarjeta", "Mnb");
            this.Property(t => t.IdTipoTarjeta).HasColumnName("IdTipoTarjeta");
            this.Property(t => t.Bin).HasColumnName("Bin");
            this.Property(t => t.IdEmisor).HasColumnName("IdEmisor");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.CodMoneda).HasColumnName("CodMoneda");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdMarca).HasColumnName("IdMarca");

        }
    }
}

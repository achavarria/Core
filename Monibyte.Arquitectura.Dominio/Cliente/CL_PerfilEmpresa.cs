using Monibyte.Arquitectura.Dominio.Generales;
using System;

namespace Monibyte.Arquitectura.Dominio.Clientes
{
    public partial class CL_PerfilEmpresa : EntidadBase
    {
        public int IdEmpresa { get; set; }
        public string FormatoFecha { get; set; }
        public string SepDecimal { get; set; }
        public string SepMiles { get; set; }
        public int NumDecimales { get; set; }
        public int IdValidaEdicionMov { get; set; }
        public int IdDesmarcableMovLiq { get; set; }
        public Nullable<int> IdRepMovTC { get; set; }
        public byte[] LogoEmpresa { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdInfDenegaciones { get; set; }
        public int IdRequiereAutorizacion { get; set; }
        public Nullable<int> IdEnviaEstCtaIndividual { get; set; }
        public int IdIncluyeDebCred { get; set; }
        public int IdPermiteDetalleTarjeta { get; set; }
        public int IdVerSubCatMcc { get; set; }
    }
}

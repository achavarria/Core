using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_CierreCorte : EntidadBase
    {
        public int IdCorte { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<int> IdUsuarioCorte { get; set; }
        public Nullable<System.DateTime> FecCorte { get; set; }
        public Nullable<System.DateTime> FecCierre { get; set; }
        public Nullable<System.DateTime> HoraCierre { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

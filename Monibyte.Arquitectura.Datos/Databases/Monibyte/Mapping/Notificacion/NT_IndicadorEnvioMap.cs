﻿using Monibyte.Arquitectura.Dominio.Notificacion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Notificacion
{
    public class NT_IndicadorEnvioMap : EntityTypeConfiguration<NT_IndicadorEnvio>
    {
        public NT_IndicadorEnvioMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdEntidad, t.IdEmpresa, t.IdProducto, t.IdContexto });

            // Properties
            this.Property(t => t.IdEntidad)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdEmpresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdProducto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdContexto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdValContexto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SubContexto)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("NT_IndicadorEnvio", "Notificacion");
            this.Property(t => t.IdEntidad).HasColumnName("IdEntidad");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdProducto).HasColumnName("IdProducto");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.IdValContexto).HasColumnName("IdValContexto");
            this.Property(t => t.SubContexto).HasColumnName("SubContexto");
            this.Property(t => t.Notificar).HasColumnName("Notificar");
        }
    }
}
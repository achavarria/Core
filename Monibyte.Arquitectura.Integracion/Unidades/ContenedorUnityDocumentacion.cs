﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Documentacion;

    public class ContenedorUnityDocumentacion
    {
       public static void Configurar(IUnityContainer _unityContainer)
       {
           #region Servicios
           _unityContainer.RegisterType<IAppDocumentacion, AppDocumentacion>();
           #endregion
                     
       }
    }
}

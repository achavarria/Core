using Monibyte.Arquitectura.Dominio.Efectivo;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Efectivo
{
    public class EF_TarifaKilometrajeMap : EntityTypeConfiguration<EF_TarifaKilometraje>
    {
        public EF_TarifaKilometrajeMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTarifa);

            // Properties
            this.Property(t => t.IdTarifa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("EF_TarifaKilometraje", "Efectivo");
            this.Property(t => t.IdTarifa).HasColumnName("IdTarifa");
            this.Property(t => t.Antiguedad).HasColumnName("Antiguedad");
            this.Property(t => t.IdTipoVehiculo).HasColumnName("IdTipoVehiculo");
            this.Property(t => t.Monto).HasColumnName("Monto");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
        }
    }
}

﻿using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Functions
{
    public class GL_ObtieneTraduccion : IConvention, IStoreModelConvention<EntityContainer>
    {
        public void Apply(EntityContainer item, DbModel dbModel)
        {
            var intType = PrimitiveType.GetEdmPrimitiveType(PrimitiveTypeKind.Int32);
            var strType = PrimitiveType.GetEdmPrimitiveType(PrimitiveTypeKind.String);
            var inParams = new[] {
                FunctionParameter.Create("p1", strType, ParameterMode.In),
                FunctionParameter.Create("p2", strType, ParameterMode.In),
                FunctionParameter.Create("p3", intType, ParameterMode.In),
                FunctionParameter.Create("p4", intType, ParameterMode.In),
                FunctionParameter.Create("p5", intType, ParameterMode.In),
                FunctionParameter.Create("p6", intType, ParameterMode.In)
            };
            var outParams = new[] {
                FunctionParameter.Create("ReturnType", strType, ParameterMode.ReturnValue)
            };
            var function = EdmFunction.Create("f_ObtieneTraduccion", "General",
                DataSpace.SSpace, new EdmFunctionPayload
                {
                    Schema = "General",
                    IsAggregate = true,
                    IsComposable = true,
                    Parameters = inParams,
                    ReturnParameters = outParams,
                    ParameterTypeSemantics = ParameterTypeSemantics.AllowImplicitConversion
                }, null);

            dbModel.StoreModel.AddItem(function);
            dbModel.Compile();
        }
    }
}
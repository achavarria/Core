﻿using Monibyte.Arquitectura.Dominio.SMS;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.SMS
{
    public class AC_MENSAJES_SMSMap : EntityTypeConfiguration<AC_MENSAJES_SMS>
    {
        public AC_MENSAJES_SMSMap()
        {
            // Primary Key
            this.HasKey(t => t.ID_MENSAJE);

            // Properties
            this.Property(t => t.TEXTO)
                .HasMaxLength(160);

            this.Property(t => t.DES_ERROR)
                .HasMaxLength(3000);

            // Table & Column Mappings
            this.ToTable("AC_MENSAJES_SMS", "SMS");
            this.Property(t => t.ID_MENSAJE).HasColumnName("ID_MENSAJE");
            this.Property(t => t.NUM_TELEFONO).HasColumnName("NUM_TELEFONO");
            this.Property(t => t.TEXTO).HasColumnName("TEXTO");
            this.Property(t => t.FECHA_HORA_REGISTRO).HasColumnName("FECHA_HORA_REGISTRO");
            this.Property(t => t.FECHA_HORA_PROCESADO).HasColumnName("FECHA_HORA_PROCESADO");
            this.Property(t => t.DES_ERROR).HasColumnName("DES_ERROR");
            this.Property(t => t.IND_ESTADO).HasColumnName("IND_ESTADO");
            this.Property(t => t.ID_TIPORIGEN).HasColumnName("ID_TIPORIGEN");
            this.Property(t => t.ID_ORIGEN).HasColumnName("ID_ORIGEN");
        }
    }
}

﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Gestiones.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Gestiones.Repositorios
{
    public class RepositorioGestion : RepositorioMnb<GE_Gestion>, IRepositorioGestion
    {
        public RepositorioGestion(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }
        public IEnumerable<GE_Gestion> ConsultaGestiones(
                int? idTipoGestion = null,
                int? idGestion = null,
                int? idEmpresa = null,
                int? idPersona = null,
                int? idTipoProducto = null,
                int? idProducto = null,
                int[] filtroEstado = null,
                int[] filtroEstadoExc = null,
                Criteria<GE_Gestion> criteria = null)
        {
            criteria = criteria ?? new Criteria<GE_Gestion>();
            if (idTipoGestion != null)
            {
                criteria.And(m => m.IdTipoGestion == idTipoGestion);
            }
            if (idGestion != null)
            {
                criteria.And(m => m.IdGestion == idGestion);
            }
            if (idEmpresa != null)
            {
                criteria.And(m => m.IdEmpresa == idEmpresa);
            }
            if (idPersona != null)
            {
                criteria.And(m => m.IdPersona == idPersona);
            }
            if (idTipoProducto != null)
            {
                criteria.And(m => m.IdTipoProducto == idTipoProducto);
            }
            if (idProducto != null)
            {
                criteria.And(m => m.IdProducto == idProducto);
            }
            if (filtroEstado != null && filtroEstado.Any())
            {
                criteria.And(m => filtroEstado.Contains(m.IdEstado));
            }
            if (filtroEstadoExc != null && filtroEstadoExc.Any())
            {
                criteria.And(m => !filtroEstadoExc.Contains(m.IdEstado));
            }
            return TableSet.Where(criteria.Satisfecho());
        }
    }

    public class RepositorioMovimientosGestion : RepositorioMnb<GE_MovimientoGestion>, IRepositorioMovimientosGestion
    {
        public RepositorioMovimientosGestion(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public void ActualizarIdLiquidadoMovimientosGestion(ParamMovGestion parameters)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("pIdGestion", parameters.IdGestion));
            listaParams.Add(new SqlParameter("pIdLiquidado", parameters.IdLiquidado));
            var sql = @"UPDATE	x
                        SET		x.IdLiquidado = @pIdLiquidado
                        FROM	TCredito.TC_MovimientoCorte x
                        WHERE	EXISTS(	SELECT	IdMovimiento 
				                        FROM	Gestion.GE_MovimientoGestion m
				                        WHERE	m.IdGestion = @pIdGestion
				                        AND		m.IdContexto = 609
				                        AND		m.IdMovimiento = x.IdMovimiento)				
	                    UPDATE	x
                        SET		x.IdLiquidado = @pIdLiquidado
                        FROM	TCredito.TC_MovimientoCorteHist x
                        WHERE	EXISTS(	SELECT	IdMovimiento 
				                        FROM	Gestion.GE_MovimientoGestion m
				                        WHERE	m.IdGestion = @pIdGestion
				                        AND		m.IdContexto = 609
				                        AND		m.IdMovimiento = x.IdMovimiento)
                        UPDATE	x
                        SET		x.IdLiquidado = @pIdLiquidado
                        FROM	Efectivo.EF_Movimiento x
                        WHERE	EXISTS(	SELECT	IdMovimiento 
				                        FROM	Gestion.GE_MovimientoGestion m
				                        WHERE	m.IdGestion = @pIdGestion
				                        AND		m.IdContexto IN (610,611)
				                        AND		m.IdMovimiento = x.IdMovimiento)";
            (this.UnidadTbjo as ISql).ExecuteCommand(sql, listaParams.ToArray());
        }

        public int MovimientosSinEditar(int idGestion)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("pIdGestion", idGestion));
            var sql = @"SELECT 1 from TCredito.TC_MovimientoCorte TC
                        JOIN General.GL_Catalogo C ON
	                        TC.IdEditado = C.IdCatalogo
                        WHERE	EXISTS (	SELECT	M.IdMovimiento 
							                FROM	Gestion.GE_MovimientoGestion M
							                WHERE	M.IdGestion = @pIdGestion	
							                AND		M.IdContexto = 609
                                            AND     M.IdMovimiento = TC.IdMovimiento )
                        AND		C.Referencia1 <> 'V'
                        UNION ALL
                        SELECT 1 from TCredito.TC_MovimientoCorteHist TC
                        JOIN General.GL_Catalogo C ON
	                        TC.IdEditado = C.IdCatalogo
                        WHERE	EXISTS  (	SELECT	M.IdMovimiento 
							                FROM	Gestion.GE_MovimientoGestion M
							                WHERE	M.IdGestion = @pIdGestion	
							                AND		M.IdContexto = 609	
                                            AND     M.IdMovimiento = TC.IdMovimiento )
                        AND		C.Referencia1 <> 'V'";
            return (this.UnidadTbjo as ISql).ExecuteQuery<int>(sql, listaParams.ToArray()).Count();
        }
    }
}

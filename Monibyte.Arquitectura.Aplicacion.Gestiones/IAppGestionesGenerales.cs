﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Gestiones;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Gestiones
{
    public interface IAppGestionesGenerales : IAplicacionBase
    {
        IEnumerable<PocParametroEmpresa> ObtenerParametrosEmpresaGestion(
            EnumTipoGestion tipoGestion, EnumContexto contexto);
        PocRepuestaLiquidacion GuardarLiquidacionGastos(PocLiquidacionGastos entrada, int? idReporte);
        void EliminarMovimientoLiquidacion(int idGestion, PocMovimientosDeLiquidacion[] movimientos);
        IEnumerable<PocMovimientosLiquidacion> ObtenerMovimientosLiquidacion(PocParametrosLiquidacion parametros);
        void ModificarValorDefecto(IEnumerable<PocParametrosGestionEmpresa> datosEntrada);
        void CerrarLiqGastos(int idGestion);
        IEnumerable<PocParametrosGestionEmpresa> ObtenerParamGestionEmpresa(int? idTipoGestion, bool reqTodo = false);
        void GuardarParametrosEmpresa(List<PocParametrosGestionEmpresa> datosEntrada, List<PocParametrosGestionEmpresa> datosActuales);
        DateTime? ObtenerUltimoRangoLiquidacion(int idPersona);
        void ModificarEstadoLiquidacion(PocCambioEstadoGestion poco);
        void EliminarLiquidacion(int idGestion);
    }
}

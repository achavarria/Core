using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_Gestion : EntidadBase
    {
        public int IdGestion { get; set; }
        public int IdTipoGestion { get; set; }
        public System.DateTime FecInclusion { get; set; }
        public int IdEmpresa { get; set; }
        public int IdPersona { get; set; }
        public string Detalle { get; set; }
        public int IdTipoProducto { get; set; }
        public int IdContexto { get; set; }        
        public Nullable<int> IdProducto { get; set; }
        public int IdEstado { get; set; }
        public Nullable<System.DateTime> FecCierre { get; set; }
        public Nullable<int> IdUsuarioCierra { get; set; }
        public Nullable<int> IdUsuarioIncluye { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdCompania { get; set; }
        public int IdUnidad { get; set; }
        public Nullable<int> IdSucursal { get; set; }
        public Nullable<int> IdEjecutivo { get; set; }
        public Nullable<int> IdExportado { get; set; }
        public Nullable<int> IdContextoMovimientos { get; set; }
    }
}

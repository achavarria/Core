﻿using System;

namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocEntradaConsultaCuentas
    {   
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Cedula { get; set; }
        public string Usuario { get; set; }
        public string superFranquicia { get; set; }
        public string SeccionMq { get; set; }
    }

    public class PocSalidaConsultaCuentas
    {
        public string TipoMensaje { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Cedula { get; set; }
        public string Cuenta { get; set; }
    }
    public class PocEntradaConsultaTarjeta
    {
        /// <summary>
        /// Propiedad interna no es pertenece a las tramas de MQ. 
        /// </summary>
        public int IdCuenta { get; set; }
        public string Emisor { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public DateTime? FechaCorte { get; set; }
        public string Usuario  { get; set; }
    }
    public class PocSalidaConsultaTarjeta
    {
        public string TipoMensaje { get; set; }
        public string Emisor { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public decimal? SaldoLocal { get; set; }
        public decimal? PagoMinLocal { get; set; }
        public decimal? PagoContadoLocal { get; set; }
        public decimal? DisponibleLocal { get; set; }
        public decimal? DispAdelantoLocal { get; set; }
        public decimal? DisponibleLocalTarjeta { get; set; }
        public decimal? DispAdelantoLocalTarjeta { get; set; }
        public decimal? DebitosLocal { get; set; }
        public decimal? CreditosLocal { get; set; }
        public decimal? SubtSaldoCorteLocal { get; set; }
        public decimal? CargosBonifLocal { get; set; }
        public decimal? SaldoTotalCorteLocal { get; set; }
        public decimal? SaldofPlazoLocal { get; set; }
        public decimal? CargosNoBonifLocal { get; set; }
        public decimal? RecargoCuotaLocal { get; set; }
        public decimal? TotalCalcPMLocal { get; set; }
        //------------------------------------------------------------------------------------
        public decimal? SaldoInter { get; set; }
        public decimal? PagoMinInter { get; set; }
        public decimal? PagoContadoInter { get; set; }
        public decimal? DisponibleInter { get; set; }
        public decimal? DispAdelantoInter { get; set; }
        public decimal? DisponibleInterTarjeta { get; set; }
        public decimal? DispAdelantoInterTarjeta { get; set; }
        public decimal? DebitosInter { get; set; }
        public decimal? CreditosInter { get; set; }
        public decimal? SubtSaldoCorteInter { get; set; }
        public decimal? CargosBonifInter { get; set; }
        public decimal? SaldoTotalCorteInter { get; set; }
        public decimal? SaldofPlazoInter { get; set; }
        public decimal? CargosNoBonifInter { get; set; }
        public decimal? RecargoCuotaInter { get; set; }
        public decimal? TotalCalcPMInter { get; set; }
        //------------------------------------------------------------------------------------
        public decimal? LimiteCuentaLocal { get; set; }
        public decimal? LimiteCuentaInter { get; set; }
        public decimal? LimiteTarjetaLocal { get; set; }
        public decimal? LimiteTarjetaInter { get; set; }
        public decimal? SaldoTarjetaLocal { get; set; }
        public decimal? SaldoTarjetaInter { get; set; }
        public DateTime? FecUltCorte { get; set; }
        public DateTime? FecVencimientoPago { get; set; }
        public string Nombre { get; set; }
        //------------------------------------------------------------------------------------
        public decimal? LimiteEFLocal { get; set; }
        public decimal? SaldoInicialEFLocal { get; set; }
        public decimal? DebitosEFLocal { get; set; }
        public decimal? CreditosEFLocal { get; set; }
        public decimal? SaldoFinalEFLocal { get; set; }
        public decimal? DisponibleEFLocal { get; set; }
        public decimal? LimiteEFInter { get; set; }
        public decimal? SaldoInicialEFInter { get; set; }
        public decimal? DebitosEFInter { get; set; }
        public decimal? CreditosEFInter { get; set; }
        public decimal? SaldoFinalEFInter { get; set; }
        public decimal? DisponibleEFInter { get; set; }
        //------------------------------------------------------------------------------------
        public string EstadoCuenta { get; set; }
        public string EstadoTarjeta { get; set; }
        //------------------------------------------------------------------------------------
        public int? ImportesVencLocal { get; set; }
        public decimal? MonImportesVencLocal { get; set; }
        public int? ImportesVencInter { get; set; }
        public decimal? MonImportesVencInter { get; set; }
        //------------------------------------------------------------------------------------
        public string NumExtrafinamiento { get; set; }
        public decimal? DebitoTransitoLocal { get; set; }
        public decimal? CreditoTransitoLocal { get; set; }
        public decimal? DebitoTransitoInter { get; set; }
        public decimal? CreditoTransitoInter { get; set; }
        public decimal? TasaInteresMensLocal { get; set; }
        public decimal? TasaInteresMensInter { get; set; }
        public decimal? TasaMoraMensLocal { get; set; }
        public decimal? TasaMoraMensInter { get; set; }
        public decimal? SaldoInicialCorteLocal { get; set; }
        public decimal? SaldoInicialCorteInter { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }
}

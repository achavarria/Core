﻿using Monibyte.Arquitectura.Dominio.Nucleo;

namespace Monibyte.Arquitectura.Dominio
{
    public interface IRepositorioMnb<TEntity> : IRepositorio<TEntity>
        where TEntity : EntidadBase
    {
        //new IQueryable<TEntity> Table { get; }
    }
}

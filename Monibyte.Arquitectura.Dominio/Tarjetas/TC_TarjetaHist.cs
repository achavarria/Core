using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_TarjetaHist : EntidadBase
    {
        public int IdTarjeta { get; set; }
        public int IdCorte { get; set; }
        public int IdCuenta { get; set; }
        public string IdTipoPlastico { get; set; }
        public string NombreImpreso { get; set; }
        public int IdRelacion { get; set; }
        public string NumTarjeta { get; set; }
        public Nullable<int> IdEmpresa { get; set; }
        public Nullable<int> IdGrupoEmpresa { get; set; }
        public System.DateTime FecApertura { get; set; }
        public Nullable<decimal> LimiteCreditoInter { get; set; }
        public int IdPersona { get; set; }
        public Nullable<int> IdEstadoTarjeta { get; set; }
        public System.DateTime FecVencimiento { get; set; }
        public Nullable<System.DateTime> FecActivacion { get; set; }
        public Nullable<int> IdUsuarioActiva { get; set; }
        public Nullable<System.DateTime> FecEntrega { get; set; }
        public Nullable<int> IdUsuarioEntrega { get; set; }
        public Nullable<System.DateTime> FecRenovacion { get; set; }
        public Nullable<int> IdUsuarioRenueva { get; set; }
        public Nullable<System.DateTime> FecReposicion { get; set; }
        public Nullable<int> IdTarjetaAnterior { get; set; }
        public Nullable<int> IdUsuarioRepone { get; set; }
        public Nullable<int> IdTipoReposicion { get; set; }
        public Nullable<decimal> DisponibleLocalTarjeta { get; set; }
        public Nullable<decimal> DisponibleInterTarjeta { get; set; }
        public Nullable<decimal> DispAdelantoLocalTarjeta { get; set; }
        public Nullable<decimal> DispAdelantoInterTarjeta { get; set; }
        public Nullable<int> IdTipoPoliza { get; set; }
        public string NumPoliza { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<decimal> LimiteCreditoLocal { get; set; }
        public decimal SaldoTarjetaLocal { get; set; }
        public decimal SaldoTarjetaInter { get; set; }
        public string CamposDinamicos { get; set; }
        public int IdTipoLimite { get; set; }
        public string CorreoEstCuenta { get; set; }
        public string CopiaCorreoEstCuenta { get; set; }
        public string Placa { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Gestiones;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Gestiones
{
    public class GE_HistorialGestionMap : EntityTypeConfiguration<GE_HistorialGestion>
    {
        public GE_HistorialGestionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Detalle)
                .IsRequired()
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("GE_HistorialGestion", "Gestion");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdGestion).HasColumnName("IdGestion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.FecEstado).HasColumnName("FecEstado");
            this.Property(t => t.HoraEstado).HasColumnName("HoraEstado");
            this.Property(t => t.Detalle).HasColumnName("Detalle");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

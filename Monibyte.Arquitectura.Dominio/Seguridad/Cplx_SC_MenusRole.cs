using System;

namespace Monibyte.Arquitectura.Dominio.Seguridad
{
    public partial class Cplx_SC_MenusRole : EntidadBase
    {
        public int IdMenu { get; set; }
        public Nullable<int> IdAccion { get; set; }
        public string DescMenu { get; set; }
        public Nullable<int> IdMenuPadre { get; set; }
        public string Area { get; set; }
        public string Controlador { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> IdOrden { get; set; }
        public Nullable<int> AyudaVigente { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio.TEF.Repositorios;

namespace Monibyte.Arquitectura.Datos.TEF.Repositorios
{
    public class ParametrosSINPE : IParametrosSINPE
    {
        public string ObtenerParamSinpe(string param, string sectionName, bool web = true)
        {
            var conf = ConfigHelper.GetConfig(string.Empty, web);
            var section = ConfigHelper.GetConfigSection(conf, "Sinpe", sectionName);
            return ConfigHelper.ObtieneValPropiedad<string>(section, param);
        }
    }
}

﻿using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System;

namespace Monibyte.Arquitectura.Dominio.TEF
{
    public partial class TF_CedulasRegistrada : EntidadBase
    {
        public int IdCedulaRegistrada { get; set; }
        public int IdCuenta { get; set; }
        public int IdUsuario { get; set; }
        public string NumIdentCuenta { get; set; }
        public string NumIdentificacion { get; set; }
        public string NombreCompleto { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public virtual GL_Usuario GL_Usuario { get; set; }
        public virtual TC_Cuenta TC_Cuenta { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_PlantillasNotificacionMap : EntityTypeConfiguration<GL_PlantillasNotificacion>
    {
        public GL_PlantillasNotificacionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPlantilla);

            // Properties
            this.Property(t => t.IdPlantilla)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .HasMaxLength(200);

            this.Property(t => t.Asunto)
                .HasMaxLength(200);

            this.Property(t => t.NomAdjunto)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("GL_PlantillasNotificacion", "General");
            this.Property(t => t.IdPlantilla).HasColumnName("IdPlantilla");
            this.Property(t => t.IdIdioma).HasColumnName("IdIdioma");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdAplicaA).HasColumnName("IdAplicaA");
            this.Property(t => t.IdOrigen).HasColumnName("IdOrigen");
            this.Property(t => t.Asunto).HasColumnName("Asunto");
            this.Property(t => t.Cuerpo).HasColumnName("Cuerpo");
            this.Property(t => t.NomAdjunto).HasColumnName("NomAdjunto");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

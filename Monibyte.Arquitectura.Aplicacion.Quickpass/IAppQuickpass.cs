﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Quickpass;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public interface IAppQuickpass : IAplicacionBase
    {
        void Insertar(PocQuickpass registro);
        void Eliminar(PocQuickpass registro);
        void Modificar(PocQuickpass registro);
        DataResult<PocQuickpass> Listar(PocQuickpass filtro, DataRequest request);

        void EditarOp(PocQuickpass registro);
        DataResult<PocQuickpass> ListarQuickpassAsignados(PocQuickpass filtro, DataRequest request);
        void ModificarEstadoQP(List<PocQuickpass> lista, bool validaEstado);
        void ActualizaEstadosOPxTarjeta();
    }
}

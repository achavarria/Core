using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_CamposReporteMap : EntityTypeConfiguration<GL_CamposReporte>
    {
        public GL_CamposReporteMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCampoReporte);

            // Properties
            this.Property(t => t.IdCampoReporte)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Alias)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("GL_CamposReporte", "General");
            this.Property(t => t.IdCampoReporte).HasColumnName("IdCampoReporte");
            this.Property(t => t.IdReporte).HasColumnName("IdReporte");
            this.Property(t => t.IdOrigen).HasColumnName("IdOrigen");
            this.Property(t => t.IdSeparador).HasColumnName("IdSeparador");
            this.Property(t => t.IdCampo).HasColumnName("IdCampo");
            this.Property(t => t.Orden).HasColumnName("Orden");
            this.Property(t => t.ValorContrapartida).HasColumnName("ValorContrapartida");
            this.Property(t => t.IdSumaContrapartida).HasColumnName("IdSumaContrapartida");
            this.Property(t => t.IdAgrupaContrapartida).HasColumnName("IdAgrupaContrapartida");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdEsEncabezado).HasColumnName("IdEsEncabezado");
            this.Property(t => t.IdSumariza).HasColumnName("IdSumariza");
            this.Property(t => t.IdAgrupable).HasColumnName("IdAgrupable");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.LongitudMax).HasColumnName("LongitudMax");
            this.Property(t => t.IdDescripcion).HasColumnName("IdDescripcion");
            this.Property(t => t.Alias).HasColumnName("Alias");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Notificacion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Notificacion
{
    public class NT_ConfiguracionMap : EntityTypeConfiguration<NT_Configuracion>
    {
        public NT_ConfiguracionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdConfiguracion);

            // Properties
            this.Property(t => t.IdConfiguracion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("NT_Configuracion", "Notificacion");
            this.Property(t => t.IdConfiguracion).HasColumnName("IdConfiguracion");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.SubContexto).HasColumnName("SubContexto");
            this.Property(t => t.IdTipoNotificacion).HasColumnName("IdTipoNotificacion");
            this.Property(t => t.FecVigenciaHasta).HasColumnName("FecVigenciaHasta");
            this.Property(t => t.DetalleFiltro).HasColumnName("DetalleFiltro");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.Destinatario).HasColumnName("Destinatario");
            this.Property(t => t.Orden).HasColumnName("Orden");
            this.Property(t => t.IdNotificacion).HasColumnName("IdNotificacion");
            this.Property(t => t.IdAdministracionInterna).HasColumnName("IdAdministracionInterna");
            this.Property(t => t.IdNotificaAdministradoresSms).HasColumnName("IdNotificaAdministradoresSms");
            this.Property(t => t.IdNotificaAdministradoresEmail).HasColumnName("IdNotificaAdministradoresEmail");
            this.Property(t => t.IdAplicaTarjetaHabiente).HasColumnName("IdAplicaTarjetaHabiente");
            this.Property(t => t.IdNotificaTarjetaHabienteSms).HasColumnName("IdNotificaTarjetaHabienteSms");
            this.Property(t => t.IdNotificaTarjetaHabienteEmail).HasColumnName("IdNotificaTarjetaHabienteEmail");
            this.Property(t => t.IdAplicaUsuario).HasColumnName("IdAplicaUsuario");
            this.Property(t => t.IdNotificaUsuarioSms).HasColumnName("IdNotificaUsuarioSms");
            this.Property(t => t.IdNotificaUsuarioEmail).HasColumnName("IdNotificaUsuarioEmail");
        }
    }
}
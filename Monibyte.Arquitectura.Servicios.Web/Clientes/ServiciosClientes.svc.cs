﻿using Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Clientes;
using Monibyte.Arquitectura.Servicios.Nucleo;

namespace Monibyte.Arquitectura.Servicios.Web.Clientes
{
    public class ServiciosClientes : ServicioUnity, IServiciosClientes
    {
        private IAppOpPersona _appOpPersona;
        private IAppOpProfesion _appOpProfesion;
        private IAppOpEjecutivo _appOpEjecutivo;
        private IAppOpPerfilEmpresa _appPerfilEmpresa;
        public ServiciosClientes(
            IAppOpPersona appOpPersona,
            IAppOpProfesion appOpProfesion,
            IAppOpEjecutivo appOpEjecutivo,
            IAppOpPerfilEmpresa appPerfilEmpresa)
        {
            _appOpPersona = appOpPersona;
            _appOpProfesion = appOpProfesion;
            _appOpEjecutivo = appOpEjecutivo;
            _appPerfilEmpresa = appPerfilEmpresa;
        }

        public Transporte ListarPersonasBuscadas(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocInfoPersona>();
            var resultado = _appOpPersona.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerInfoPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocInfoPersona>();
            var resultado = _appOpPersona.Obtener(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaProfesiones(Transporte transporte)
        {
            var resultado = _appOpProfesion.ListaProfesiones();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte GuardarPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocInfoPersona>();
            _appOpPersona.Insertar(entrada);
            return transporte;
        }

        public Transporte ActualizarPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocInfoPersona>();
            _appOpPersona.Modificar(entrada);
            return transporte;
        }

        public Transporte ComprobarIdentificacion(Transporte transporte)
        {
            var numIdentificacion = transporte.GetValor<string>("numIdentificacion");
            var idIdentificacion = transporte.GetValor<int>("idIdentificacion");
            var resultado = _appOpPersona.ComprobarIdentificacion(numIdentificacion, idIdentificacion);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminarPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocInfoPersona>();
            _appOpPersona.Eliminar(entrada);
            return transporte;
        }

        public Transporte ObtienePerfil(Transporte transporte)
        {
            var resultado = _appPerfilEmpresa.ObtienePerfil();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ActualizarPerfilEmpresa(Transporte transporte)
        {
            var datos = transporte.GetDatos<PocPerfilEmpresa>();
            _appPerfilEmpresa.ActualizarPerfilEmpresa(datos);
            return transporte;
        }

        public Transporte ObtenerEjecutivos(Transporte transporte)
        {
            var datos = transporte.GetDatos<PocEjecutivo>();
            var dataRequest = transporte.GetDataRequest();
            var resultado = _appOpEjecutivo.Listar(datos, dataRequest);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerListaEjecutivos(Transporte transporte)
        {
            var idEstado = transporte.GetValor<int?>("idEstado");
            var resultado = _appOpEjecutivo.ObtenerEjecutivos(idEstado);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminarEjecutivos(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocEjecutivo>();
            _appOpEjecutivo.Eliminar(entrada);
            return transporte;
        }

        public Transporte InsertarEjecutivos(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocEjecutivo>();
            _appOpEjecutivo.Insertar(entrada);
            return transporte;
        }

        public Transporte ActualizarEjecutivos(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocEjecutivo>();
            _appOpEjecutivo.Modificar(entrada);
            return transporte;
        }

        public void AsociarEjecutivo(int[] empresas, int? idEjecutivo)
        {
            _appOpPersona.AsociarEjecutivo(empresas, idEjecutivo);
        }
        public string ObtenerCorreoPersona(int idPersona, string codUsuario)
        {
            return _appOpPersona.ObtenerCorreoPersona(idPersona, codUsuario);
        }
    }
}

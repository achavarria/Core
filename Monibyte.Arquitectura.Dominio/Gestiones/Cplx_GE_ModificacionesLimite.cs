﻿using System;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public class Cplx_GE_ModificacionesLimite : EntidadBase
    {
        public int IdGestion { get; set; }
        public string UsuarioIngresa { get; set; }
        public string UsuarioActualiza { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaActualiza { get; set; }
        public string NumCuenta { get; set; }
        public string NombreTitular { get; set; }
        public string DesEstado { get; set; }
        public string ParamValor { get; set; }
        public string Detalle { get; set; }
    }
}

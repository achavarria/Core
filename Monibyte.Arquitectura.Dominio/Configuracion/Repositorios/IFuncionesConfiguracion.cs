﻿using Monibyte.Arquitectura.Dominio.Nucleo;

namespace Monibyte.Arquitectura.Dominio.Configuracion.Repositorios
{
    public interface IFuncionesConfiguracion : IRepositorioBase
    {
        bool RestriccionVigente(ParamRestriccionVigente parameters);
    }
}

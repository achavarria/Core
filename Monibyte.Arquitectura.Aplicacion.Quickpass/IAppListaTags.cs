﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Quickpass;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public interface IAppListaTags : IAplicacionBase
    {
        DataResult<PocListaTagsTabla> Listar(PocListaTagsTabla filtro, DataRequest request);        
    }
}

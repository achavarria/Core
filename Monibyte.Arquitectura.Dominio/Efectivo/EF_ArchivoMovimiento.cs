using Monibyte.Arquitectura.Dominio.Generales;
using System;

namespace Monibyte.Arquitectura.Dominio.Efectivo
{
    public partial class EF_ArchivoMovimiento : EntidadBase
    {
        public int IdArchivo { get; set; }
        public int IdMovimiento { get; set; }
        public string TipoArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public byte[] DatosArchivo { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

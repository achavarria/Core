﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;


namespace Monibyte.Arquitectura.Aplicacion.Generales.Mantenimientos
{
    public interface IAppOpIdioma: IAplicacionBase
    {
        void Insertar(PocIdioma registro);
        void Eliminar(PocIdioma registro);
        void Modificar(PocIdioma registro);
        PocIdioma ObtenerIdiomas(int idIdioma);
        //List<PocIdioma> ListarIdioma(int? idIdioma = null);             
        List<PocIdioma> ListarIdioma(PocIdioma registro = null);             
    }
}

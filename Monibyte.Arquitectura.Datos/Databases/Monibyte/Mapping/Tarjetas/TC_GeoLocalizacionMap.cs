using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_GeoLocalizacionMap : EntityTypeConfiguration<TC_GeoLocalizacion>
    {
        public TC_GeoLocalizacionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdGeoLocalizacion);

            // Properties
            this.Property(t => t.IdTransaccion)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.NumTarjeta)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Placa)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Localizacion)
                .IsRequired();

            this.Property(t => t.Conductor)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.NumAutorizacion)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Latitud)
                .HasMaxLength(50);

            this.Property(t => t.Longitud)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TC_GeoLocalizacion", "TCredito");
            this.Property(t => t.IdGeoLocalizacion).HasColumnName("IdGeoLocalizacion");
            this.Property(t => t.IdTransaccion).HasColumnName("IdTransaccion");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.Placa).HasColumnName("Placa");
            this.Property(t => t.Localizacion).HasColumnName("Localizacion");
            this.Property(t => t.Conductor).HasColumnName("Conductor");
            this.Property(t => t.NumAutorizacion).HasColumnName("NumAutorizacion");
            this.Property(t => t.Distancia).HasColumnName("Distancia");
            this.Property(t => t.CantidadCombustible).HasColumnName("CantidadCombustible");
            this.Property(t => t.IdUnidadMedida).HasColumnName("IdUnidadMedida");
            this.Property(t => t.MonMovimiento).HasColumnName("MonMovimiento");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
            this.Property(t => t.IdUsaSensor).HasColumnName("IdUsaSensor");
            this.Property(t => t.Latitud).HasColumnName("Latitud");
            this.Property(t => t.Longitud).HasColumnName("Longitud");
            this.Property(t => t.PrecioPorLitro).HasColumnName("PrecioPorLitro");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.NumIntentosFallidos).HasColumnName("NumIntentosFallidos");
        }
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Logs
{
    public partial class LG_BitacoraPanelControl : EntidadBase
    {
        public int IdBitacora { get; set; }
        public int IdCondicion { get; set; }
        public string NumTarjeta { get; set; }
        public string DatosBitacora { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

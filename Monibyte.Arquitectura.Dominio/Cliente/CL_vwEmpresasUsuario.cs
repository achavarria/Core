using System;

namespace Monibyte.Arquitectura.Dominio.Clientes
{
    public partial class CL_vwEmpresasUsuario : EntidadBase
    {
        public int IdEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
        public string IdentificacionEmpresa { get; set; }
        public int IdTipoPersonaEmpresa { get; set; }
        public int IdUsuario { get; set; }
        public string CodUsuario { get; set; }
        public string AliasUsuario { get; set; }
        public string Correo { get; set; }
        public int IdIdiomaPreferencia { get; set; }
        public int IdTipoUsuario { get; set; }
        public int IdRole { get; set; }
        public Nullable<int> IdRoleOperativo { get; set; }
        public int IdDefault { get; set; }
        public int IdEstado { get; set; }
        public int IdPersona { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public int IdCompania { get; set; }
        public int IdUnidad { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Autorizador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador
{
    public class MB_PaisMap : EntityTypeConfiguration<MB_Pais>
    {
        public MB_PaisMap()
        {
            // Primary Key
            this.HasKey(t => t.CodPaisISO);

            // Properties
            this.Property(t => t.CodPaisISO)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Nacionalidad)
                .HasMaxLength(60);

            this.Property(t => t.CodMonedaISO)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("MB_Pais", "Mnb");
            this.Property(t => t.CodPaisISO).HasColumnName("CodPaisISO");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.Nacionalidad).HasColumnName("Nacionalidad");
            this.Property(t => t.CodMonedaISO).HasColumnName("CodMonedaISO");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");


        }
    }
}

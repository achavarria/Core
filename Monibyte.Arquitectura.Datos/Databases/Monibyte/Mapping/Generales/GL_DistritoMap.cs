using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_DistritoMap : EntityTypeConfiguration<GL_Distrito>
    {
        public GL_DistritoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdDistrito);

            // Properties
            this.Property(t => t.IdDistrito)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Codigo)
                .HasMaxLength(10);

            this.Property(t => t.Descripcion)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GL_Distrito", "General");
            this.Property(t => t.IdDistrito).HasColumnName("IdDistrito");
            this.Property(t => t.IdCanton).HasColumnName("IdCanton");
            this.Property(t => t.Codigo).HasColumnName("Codigo");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

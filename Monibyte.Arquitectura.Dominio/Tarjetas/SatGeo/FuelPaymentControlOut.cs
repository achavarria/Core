﻿namespace Monibyte.Arquitectura.Dominio.Tarjetas.SatGeo
{
    public partial class SatGeoOut
    {
        public double AmountFuel { get; set; }
        public double AmountFuelRecope { get; set; }
        public double PriceFuelRecope { get; set; }
        public string DateLocation { get; set; }
        public int Distance { get; set; }
        public string DriverName { get; set; }
        public double Latitude { get; set; }
        public string Location { get; set; }
        public double Longitude { get; set; }
        public string Plate { get; set; }
        public bool isSensorFuel { get; set; }
        public int CodError { get; set; }
        public string Mensaje { get; set; }
    }
}

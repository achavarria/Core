﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Contabilidad;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Contabilidad
{
    public class AppMovimientoCorte : AplicacionBase, IAppMovimientoCorte
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<TC_MovimientoCorte> _repMovimientoCorte;
        private IRepositorioMnb<CG_ConfiguracionAsiento> _repConfAsiento;
        private IRepositorioMnb<CG_Parametro> _repParametro;
        private IRepositorioCuenta _repCuenta;
        private IRepositorioMnb<TC_TipoTarjeta> _repTipoTarjeta;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;

        public AppMovimientoCorte(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<TC_MovimientoCorte> repMovimientoCorte,
            IRepositorioMnb<CG_ConfiguracionAsiento> repConfAsiento,
            IRepositorioMnb<CG_Parametro> repParametro,
            IRepositorioCuenta repCuenta,
            IRepositorioMnb<TC_TipoTarjeta> repTipoTarjeta,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema)
        {
            _repCatalogo = repCatalogo;
            _repMovimientoCorte = repMovimientoCorte;
            _repConfAsiento = repConfAsiento;
            _repParametro = repParametro;
            _repCuenta = repCuenta;
            _repTipoTarjeta = repTipoTarjeta;
            _repParamSistema = repParamSistema;
        }

        public string GenerarArchivoInvoice(DateTime fecMovimientoDesde, DateTime fecMovimientoHasta)
        {
            string resArch = "";
            string binValido = _repParamSistema.SelectById("BIN_MONIBYTE").ValParametro;

            var movimientosGenerar = _repMovimientoCorte.Table
                .Join(_repCatalogo.Table,
                    mov => mov.IdTipoMovimiento,
                    cat => cat.IdCatalogo,
                    (mov, cat) => new
                    {
                        IdTipoMovimiento = mov.IdTipoMovimiento,
                        IdOrigenMovimiento = mov.IdOrigenMovimiento,
                        FecMovimiento = mov.FecMovimiento,
                        IdMoneda = mov.IdMoneda,
                        MonMovimiento = cat.Referencia2 == "D" ? mov.MonMovimiento : mov.MonMovimiento * -1,
                        IdCuenta = mov.IdCuenta
                    })
                .Where(movGen => movGen.FecMovimiento >= fecMovimientoDesde &&
                    movGen.FecMovimiento <= fecMovimientoHasta &&
                    _repTipoTarjeta.Table
                .Join(_repCuenta.Table,
                    tt => tt.IdTipoTarjeta,
                    tar => tar.IdTipoTarjeta,
                    (tt, tar) => new { Bin = tt.Bin, IdCuenta = tar.IdCuenta })
                .Any(a => a.IdCuenta == movGen.IdCuenta &&
                    string.Concat("|", a.Bin, "|").IndexOf(binValido) >= 0))
                .GroupBy(m => new { m.IdTipoMovimiento, m.IdOrigenMovimiento, m.FecMovimiento, m.IdMoneda })
                .Select(m => new PocMovimientoCorte
                {
                    IdTipoMovimiento = m.Key.IdTipoMovimiento,
                    IdOrigenMovimiento = m.Key.IdOrigenMovimiento,
                    FecMovimiento = m.Key.FecMovimiento,
                    IdMoneda = m.Key.IdMoneda,
                    MonMovimiento = m.Sum(mov => mov.MonMovimiento)
                });

            var movInvoice = movimientosGenerar.Join(_repConfAsiento.Table,
                        mov => new { mov.IdTipoMovimiento, mov.IdOrigenMovimiento, mov.IdMoneda },
                        conf => new { conf.IdTipoMovimiento, conf.IdOrigenMovimiento, conf.IdMoneda },
                        (mov, conf) => new { mov, conf })
                .GroupBy(m => new { m.conf.IdCustomer, m.conf.IdAccount, m.conf.IdItem, m.mov.FecMovimiento })
                .Select(m => new PocFactura
                {
                    IdCustomer = m.Key.IdCustomer,
                    IdAccount = m.Key.IdAccount,
                    IdItem = m.Key.IdItem,
                    MonMovimiento = m.Sum(mov => mov.mov.MonMovimiento),
                    FecMovimiento = m.Key.FecMovimiento,
                    Orden = m.Min(mov => mov.conf.Orden)
                });

            List<PocFactura> listaMovs = movInvoice.ToList();

            foreach (PocFactura element in listaMovs)
            {
                string TRNS, TRNSID, TRNSTYPE, DATE, ACCNT, NAME, CLASS, AMOUNT, DOCNUM, MEMO,
                       CLEAR, TOPRINT, NAMEISTAXABLE, ADDR1, ADDR3, TERMS, SHIPVIA, SHIPDATE,
                       SPL, SPLID, QNTY, PRICE, INVITEM, TAXABLE, OTHER2, YEARTODATE, WAGEBASE;


                var descAccount = _repParametro.Table.FirstOrDefault(x => x.IdParametro == element.IdAccount);
                var descCustomer = _repParametro.Table.FirstOrDefault(x => x.IdParametro == element.IdCustomer);
                var descItem = _repParametro.Table.FirstOrDefault(x => x.IdParametro == element.IdItem);
                var memo = _repConfAsiento.Table.FirstOrDefault(x => x.IdAccount == element.IdAccount &&
                                                                     x.IdCustomer == element.IdCustomer &&
                                                                     x.IdItem == element.IdItem);

                var fecMov = FechasExt.ToDateStr((DateTime)element.FecMovimiento, "dd-MM-yy", invariant: true);
                var fecha = string.IsNullOrEmpty(fecMov) ? DateTime.Now : fecMov.ToDate("dd-MM-yy");

                if ((element.Orden == 1) || (element.Orden == null))
                {
                    if (resArch != "")
                        resArch += string.Format("{0}\r\n", "ENDTRNS");
                    else
                    {
                        resArch += string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\r\n", "!TRNS", "TRNSID", "TRNSTYPE", "DATE", "ACCNT", "NAME", "CLASS", "AMOUNT", "DOCNUM", "MEMO", "CLEAR", "TOPRINT", "NAMEISTAXABLE", "ADDR1", "ADDR3", "TERMS", "SHIPVIA", "SHIPDATE");
                        resArch += string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\r\n", "!SPL", "SPLID", "TRNSTYPE", "DATE", "ACCNT", "NAME", "CLASS", "AMOUNT", "DOCNUM", "MEMO", "CLEAR", "QNTY", "PRICE", "INVITEM", "TAXABLE", "OTHER2", "YEARTODATE", "WAGEBASE");
                        resArch += string.Format("{0}\r\n", "!ENDTRNS");
                    }

                    TRNS = "TRNS";
                    TRNSID = "";
                    TRNSTYPE = "INVOICE";
                    DATE = fecha.ToDateStr("MM/dd/yy");
                    ACCNT = descAccount.Descripcion;
                    NAME = "\"" + descCustomer.Descripcion + "\"";
                    CLASS = "";
                    AMOUNT = element.MonMovimiento.ToString("n2");
                    DOCNUM = fecha.ToDateStr("ddMMyy");
                    MEMO = string.IsNullOrEmpty(memo.DescMemo) ? "" : memo.DescMemo + " " + fecMov;//string.IsNullOrEmpty(element.DescMemo) ? "" : element.DescMemo + " " + fecMov;
                    CLEAR = "N";
                    TOPRINT = "N";
                    NAMEISTAXABLE = "N";
                    ADDR1 = "";
                    ADDR3 = "";
                    TERMS = "30 dias";
                    SHIPVIA = "";
                    SHIPDATE = fecha.ToDateStr("MM/dd/yy");

                    resArch += string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\r\n", TRNS, TRNSID, TRNSTYPE, DATE, ACCNT, NAME, CLASS, AMOUNT, DOCNUM, MEMO, CLEAR, TOPRINT, NAMEISTAXABLE, ADDR1, ADDR3, TERMS, SHIPVIA, SHIPDATE);
                }
                else
                {
                    SPL = "SPL";
                    SPLID = "";
                    TRNSTYPE = "INVOICE";
                    DATE = fecha.ToDateStr("MM/dd/yy");
                    ACCNT = descAccount.Descripcion;
                    NAME = "\"" + descCustomer.Descripcion + "\"";
                    CLASS = "";
                    AMOUNT = "-" + element.MonMovimiento.ToString("n2");
                    DOCNUM = "";
                    MEMO = string.IsNullOrEmpty(memo.DescMemo) ? "" : memo.DescMemo + " " + fecMov;
                    CLEAR = "N";
                    QNTY = "";
                    PRICE = element.MonMovimiento.ToString("n2");
                    ADDR1 = "";
                    ADDR3 = "";
                    TERMS = "";
                    SHIPVIA = "";
                    SHIPDATE = fecha.ToDateStr("MM/dd/yy");
                    INVITEM = descItem.Descripcion;
                    TAXABLE = "N";
                    OTHER2 = "";
                    YEARTODATE = "0";
                    WAGEBASE = "0";

                    resArch += string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\r\n", SPL, SPLID, TRNSTYPE, DATE, ACCNT, NAME, CLASS, AMOUNT, DOCNUM, MEMO, CLEAR, QNTY, PRICE, INVITEM, TAXABLE, OTHER2, YEARTODATE, WAGEBASE);
                }

            }
            resArch += string.Format("{0}\r\n", "ENDTRNS");

            return resArch;

        }
    }
}

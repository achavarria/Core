﻿using Monibyte.Arquitectura.Aplicacion.Cargos;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Cargos;
using Monibyte.Arquitectura.Servicios.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Cargos
{
    public class ServicioCargos : ServicioUnity, IServicioCargos
    {
        private IAppTipoCargo _appTipoCargo;
        private IAppRangosTabla _appRangosTabla;
        private IAppDetRangosCargo _appDetRangosCargo;
        private IAppDetRangosCargosPer _appDetRangosCargosPer;
        private IAppCargosPorPersona _appCargosPorPersona;
        private IAppValOrigenCargo _appValOrigenCargo;
        private IAppValOrigenCargoPer _appValOrigenCargoPer;

        public ServicioCargos(
            IAppTipoCargo appTipoCargo, 
            IAppRangosTabla appRangosTabla, 
            IAppDetRangosCargo appDetRangosCargo, 
            IAppCargosPorPersona appCargosPorPersona, 
            IAppDetRangosCargosPer appDetRangosCargosPer, 
            IAppValOrigenCargo appValOrigenCargo, 
            IAppValOrigenCargoPer appValOrigenCargoPer)
        {
            _appTipoCargo = appTipoCargo;
            _appRangosTabla = appRangosTabla;
            _appDetRangosCargo = appDetRangosCargo;
            _appCargosPorPersona = appCargosPorPersona;
            _appDetRangosCargosPer = appDetRangosCargosPer;
            _appValOrigenCargo = appValOrigenCargo;
            _appValOrigenCargoPer = appValOrigenCargoPer;
        }

        public Transporte IncluirTipoCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoCargo>();
            _appTipoCargo.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarTipoCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoCargo>();
            var resultado = _appTipoCargo.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaTipoCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoCargo>();
            _appTipoCargo.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaTipoCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoCargo>();
            _appTipoCargo.Modificar(entrada);
            return transporte;
        }

        public Transporte ListadoTipoCargo(Transporte transporte)
        {
            var resultado = _appTipoCargo.ListadoTipoCargo();
            transporte.SetDatos(resultado);
            return transporte;
        }


        public Transporte IncluirRangosTabla(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocRangosTabla>();
            _appRangosTabla.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarRangosTabla(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocRangosTabla>();
            var resultado = _appRangosTabla.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaRangosTabla(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocRangosTabla>();
            _appRangosTabla.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaRangosTabla(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocRangosTabla>();
            _appRangosTabla.Modificar(entrada);
            return transporte;
        }

        public Transporte ListadoRangosTabla(Transporte transporte)
        {
            var resultado = _appRangosTabla.ListadoRangosTabla();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListadoOrigenCargo(Transporte transporte)
        {
            var resultado = _appTipoCargo.ListadoOrigenCargo();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte IncluirDetRangosCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocDetRangosCargo>();
            _appDetRangosCargo.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarDetRangosCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocDetRangosCargo>();
            var resultado = _appDetRangosCargo.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaDetRangosCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocDetRangosCargo>();
            _appDetRangosCargo.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaDetRangosCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocDetRangosCargo>();
            _appDetRangosCargo.Modificar(entrada);
            return transporte;
        }

        public Transporte IncluirCargosPorPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocCargosPorPersona>();
            _appCargosPorPersona.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarCargosPorPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocCargosPorPersona>();
            var resultado = _appCargosPorPersona.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaCargosPorPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocCargosPorPersona>();
            _appCargosPorPersona.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaCargosPorPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocCargosPorPersona>();
            _appCargosPorPersona.Modificar(entrada);
            return transporte;
        }

        public Transporte IncluirDetRangosCargosPer(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocDetRangosCargosPer>();
            _appDetRangosCargosPer.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarDetRangosCargosPer(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocDetRangosCargosPer>();
            var resultado = _appDetRangosCargosPer.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaDetRangosCargosPer(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocDetRangosCargosPer>();
            _appDetRangosCargosPer.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaDetRangosCargosPer(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocDetRangosCargosPer>();
            _appDetRangosCargosPer.Modificar(entrada);
            return transporte;
        }

        public Transporte ListarDetalleCargos(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocDetalleCargos>();
            var resultado = _appRangosTabla.ListarDetalleCargos(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerTipoCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoCargo>();
            var resultado = _appTipoCargo.ObtenerTipoCargo(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte IncluirValOrigenCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocValOrigenCargo>();
            _appValOrigenCargo.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarValOrigenCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocValOrigenCargo>();
            var resultado = _appValOrigenCargo.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaValOrigenCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocValOrigenCargo>();
            _appValOrigenCargo.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaValOrigenCargo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocValOrigenCargo>();
            _appValOrigenCargo.Modificar(entrada);
            return transporte;
        }

        public Transporte ConsultarListaGeneral(Transporte transporte)
        {
            var idLista = transporte.GetDatos<int>();
            int?[] excluir = transporte.GetValor<int?[]>("excluir");
            var resultado = _appValOrigenCargo.ObtieneInfoLista(idLista, excluir);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListarValOrigenCargoPer(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocValOrigenCargoPer>();
            var resultado = _appValOrigenCargoPer.ListarOrigenCargo(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        #region CargosGeolocalizacion

        public void GuardarCargosGeo(PocRegistrarPlacas entrada, List<PocCargosPlaca> modeloBase)
        {
            var app = GestorUnity.CargarUnidad<IAppCargosGeolocalizacion>();
            app.GuardarCargosGeo(entrada, modeloBase);
        }

        public PocRegistrarPlacas ObtieneRegistroCargosGeo(int idEmpresa)
        {
            var app = GestorUnity.CargarUnidad<IAppCargosGeolocalizacion>();
            return app.ObtieneRegistroCargosGeo(idEmpresa);
        }

        public void CambiarEstadoGeo(int idEmpresa, int idEstado)
        {
            var app = GestorUnity.CargarUnidad<IAppCargosGeolocalizacion>();
            app.CambiarEstadoGeo(idEmpresa, idEstado);
        }

        #endregion
    }
}

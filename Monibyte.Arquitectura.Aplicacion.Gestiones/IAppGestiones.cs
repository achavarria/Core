﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Gestiones;
using Monibyte.Arquitectura.Poco.Notificacion;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Gestiones
{
    public interface IAppGestiones : IAplicacionBase
    {
        int InsertaGestion(EnumTipoGestion idTipoGestion, EnumEstadoGestion idEstado,
            EnumContexto idContexto, int idProducto, string detalle,
            DateTime? fecInclusion = null, int? idSucursal = null, int? idEjecutivo = null,
            int? idContextoMovimientos = null, int? idEmpresa = null);
        int InsertaGestion(EnumTipoGestion idTipoGestion, EnumEstadoGestion idEstado,
            EnumContexto idContexto, EnumTipoProducto idTipoProducto, int idPersona,
            string detalle, int? idSucursal = null, int? idEjecutivo = null);
        PocDetalleGestion ObtenerGestion(int idGestion);
        PocDetalleGestion ObtenerUltimaGestionPorPersona(int idUsuario);
        IEnumerable<GE_Gestion> ObtenerGestionPorProducto(int idProducto,
            EnumTipoGestion tipoGestion,
            int[] filtroEstado = null,
            int[] filtroEstadoExc = null);
        void ModificarEstado(PocCambioEstadoGestion poco, bool ignorarCorreo = false);
        void ModificarDatos(int idGestion, int? idSucursal, int? idEjecutivo);
        bool ExisteGestionAnterior(int idProducto, EnumTipoGestion tipoGestion);
        int? BuscarGestionPendiente(int idEmpresa,
            EnumTipoGestion tipoGestion, int? idProducto = null);
        int? BuscarGestionEnEdicion(int idPersona, EnumTipoGestion tipoGestion);
        PocDetalleGestion BuscarGestionRegistrada(int idGestion);
        PocDetalleGestion BuscarGestionHabilitada(int idGestion);
        List<GE_Gestion> GestionesAutorizadas(
            int idProducto, EnumTipoGestion tipoGestion);
        PocDetalleGestion BuscarGestion(int idGestion);
        DataResult<PocDetalleGestion> ObtenerGestionesEmpresa(
            PocParametrosGestion parametros, DataRequest request, bool incluirOperativas = false);
        IEnumerable<PocDetalleGestion> ConsultarGestionesRealizadas(PocParametrosGestion parametros);
        DataResult<PocDetalleHistoriaGestion> ObtenerHistorial(
            int idGestion, DataRequest request);
        void IncluirHistorialGestion(string justificacion,
            int idEstado, int idGestion);
        void IncluirHistorialGestion(PocCambioEstadoGestion modelo);
        List<PocTipoGestion> ListaTipoGestion();
        string ObtenerValorParametro(int idGestion, bool descomprime = true);
        void IncluirValorParametro(int idGestion, string jsonValue);
        void ModificarValorParametro(int idGestion, string jsonValue);
        string ObtenerDetalleTipoGestion(EnumTipoGestion tipoGestion);
        PocProductoGestion ObtenerProducto(int idContexto, int idProducto);
        void EliminarGestionRegistrada(int idGestion);
        CL_Persona ObtenerInfoPersona(int? IdPersona);
        void NotificaGestion(PocoQueue notificacion, bool notificaProcesador = false, bool notificaExterno = false);
        void NotificaGestionInterno(PocoQueue notificacion, string idParamCorreo = null);
        void MarcarExportadas(params int[] gestiones);

        #region Reporte Gestiones por Usuario

        PocReporte ObtenerReporteGestiones(DateTime fecDesde,
            DateTime fecHasta, bool esExcel = true);

        #endregion

        void ModificaLimiteGlobal(PocModificaLimite cambioLimite);
        List<PocReporteModificacionesLimite> ObtenerModificacionesLimite(PocReporteModificacionesLimite filtro);
    }
}

﻿using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios
{

    public interface IRepositorioVwMovTarjeta : IRepositorioMnb<TC_vwMovimientosTarjeta>
    {
        IEnumerable<TC_vwMovimientosTarjeta> ObtenerMovimientos(ParamMovimientosTc parameters);
    }
}

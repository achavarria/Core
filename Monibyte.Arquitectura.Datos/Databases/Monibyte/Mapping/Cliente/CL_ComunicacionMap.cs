using Monibyte.Arquitectura.Dominio.Clientes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Clientes
{
    public class CL_ComunicacionMap : EntityTypeConfiguration<CL_Comunicacion>
    {
        public CL_ComunicacionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdComunicacion);

            // Properties
            this.Property(t => t.IdComunicacion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ValComunicacion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("CL_Comunicacion", "Cliente");
            this.Property(t => t.IdComunicacion).HasColumnName("IdComunicacion");
            this.Property(t => t.ValComunicacion).HasColumnName("ValComunicacion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdTipo).HasColumnName("IdTipo");
            this.Property(t => t.IdUbicacion).HasColumnName("IdUbicacion");
            this.Property(t => t.IdFormato).HasColumnName("IdFormato");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdOrigenProcesador).HasColumnName("IdOrigenProcesador");

        }
    }
}

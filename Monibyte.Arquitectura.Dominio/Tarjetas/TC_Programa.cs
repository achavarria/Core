using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_Programa : EntidadBase
    {
        public int IdPrograma { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> IdPlan { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<System.DateTime> FecInicio { get; set; }
        public Nullable<System.DateTime> FecVencimiento { get; set; }
        public Nullable<int> IdTipoTarjeta { get; set; }
        public int IdTipoPrograma { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

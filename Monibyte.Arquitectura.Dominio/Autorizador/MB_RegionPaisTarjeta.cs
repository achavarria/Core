using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Autorizador
{
    public partial class MB_RegionPaisTarjeta : EntidadBaseCtrl
    {
        public int IdRegionPais { get; set; }
        public int IdCondicionTarjeta { get; set; }
        public string CodPaisISO { get; set; }
        public Nullable<int> IdRegion { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

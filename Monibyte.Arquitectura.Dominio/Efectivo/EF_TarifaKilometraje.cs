using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using System;

namespace Monibyte.Arquitectura.Dominio.Efectivo
{
    public partial class EF_TarifaKilometraje : EntidadBase
    {
        public int IdTarifa { get; set; }
        public int Antiguedad { get; set; }
        public int IdTipoVehiculo { get; set; }
        public decimal Monto { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdEmpresa { get; set; }
    }
}

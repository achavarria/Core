﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Efectivo;
using Monibyte.Arquitectura.Poco.Gestiones;

namespace Monibyte.Arquitectura.Aplicacion.Gestiones
{
    public interface IAppGestionesEfectivo : IAplicacionBase
    {
        DataResult<PocGestionKilometraje> ObtieneListaGestionKm(PocParametrosGestion filtro, DataRequest request);
        int IncluirGestionKilometraje(PocGestionKilometraje registro);
        void EditarGestionKilometraje(PocGestionKilometraje registro, PocGestionKilometraje registroPrevio);
    }
}

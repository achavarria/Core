﻿using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public class RepositorioMnb<TEntity> : Repositorio<TEntity>,
        IRepositorioMnb<TEntity> where TEntity : EntidadBase
    {
        public RepositorioMnb(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }
        
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Tesoreria;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Tesoreria
{

    public class AppTesoreria : AplicacionBase, IAppTesoreria
    {
        private IRepositorioMoneda _repMoneda;
        private IFuncionesTesoreria _repFuncionTesoreria;
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IServiciosEconomicos _repServiciosEconomicos;

        public AppTesoreria(IRepositorioMoneda repMoneda,
            IFuncionesTesoreria repFuncionTesoreria,
            IRepositorioMnb<GL_Entidad> repEntidad,
            IServiciosEconomicos repServiciosEconomicos)
        {
            _repMoneda = repMoneda;
            _repFuncionTesoreria = repFuncionTesoreria;
            _repEntidad = repEntidad;
            _repServiciosEconomicos = repServiciosEconomicos;
        }

        public IEnumerable<PocMoneda> ListaMonedas()
        {
            var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
            var entidad = _repEntidad.Table.FirstOrDefault(x => x.IdEntidad == idEntidad);
            var parametros = _repMoneda.Table.ToList().Where(x => x.IdMoneda == entidad.IdMonedaInternacional || x.IdMoneda == entidad.IdMonedaLocal);
            return parametros.ProyectarComo<PocMoneda>();
        }
        public void InsertaMoneda(PocMoneda pocEntrada)
        {
            var entrada = pocEntrada.ProyectarComo<TS_Moneda>();
            _repMoneda.Insert(entrada);
        }
        public void ActualizaMoneda(PocMoneda pocEntrada)
        {
            var entrada = pocEntrada.ProyectarComo<TS_Moneda>();
            _repMoneda.Update(entrada);
        }
        public void EliminaMoneda(PocMoneda pocEntrada)
        {
            var entrada = pocEntrada.ProyectarComo<TS_Moneda>();
            _repMoneda.Delete(entrada);
        }

        public decimal ConvierteMonto(PocMontoConvertir poco)
        {
            var resultado = _repFuncionTesoreria.ConvierteMonto(poco.IdMonedaOrigen, poco.Monto, poco.IdMonedaDestino);
            if (poco.Factor.HasValue && poco.Factor > 0)
            {
                resultado = Math.Floor(resultado / poco.Factor.Value) * poco.Factor.Value;
            }
            return resultado;
        }

        public PocMoneda ObtieneMoneda(int idMoneda)
        {
            var salida = _repMoneda.Table.FirstOrDefault(x => x.IdMoneda == idMoneda);
            return salida.ProyectarComo<PocMoneda>();
        }
        public decimal? ObtieneCambioBCCR(DateTime fecCambio, bool esCompra)
        {
            decimal? resultado;
            if (esCompra)
            {
                resultado = (decimal?)_repServiciosEconomicos.ObtenerTipoCambioCompra(fecCambio);
            }
            else
            {
                resultado = (decimal?)_repServiciosEconomicos.ObtenerTipoCambioVenta(fecCambio);
            }

            return resultado;
        }

        public decimal ObtieneUltimoTipoCambio(int IdTipoCambio, System.DateTime FecCambio, int IdMoneda, int? IdEntidad = null)
        {
            return _repFuncionTesoreria.ObtieneTipoCambio(new ParamsObtieneTipoCambioTS
            {
                IdTipoCambio = IdTipoCambio,
                FecCambio = FecCambio,
                IdMoneda = IdMoneda,
                IdEntidad = IdEntidad
            });
        }
        public decimal ConvertirMontoHistorico(int idMonedaOrigen, decimal monto, int idMonedaDestino,
            System.DateTime fecReferencia, int? idEntidadFinanciera = null, int idTipoCambio = (int)EnumIdTipoDeCambio.Compra)
        {
            return _repFuncionTesoreria.ConvierteMontoHistorico(new ParamsConvierteMontoHistoricoTS
            {
                idMonedaOrigen = idMonedaOrigen,
                monto = monto,
                idMonedaDestino = idMonedaDestino,
                fecReferencia = fecReferencia,
                idEntidadFinanciera = idEntidadFinanciera,
                idTipoCambio = idTipoCambio
            });
        }
    }
}

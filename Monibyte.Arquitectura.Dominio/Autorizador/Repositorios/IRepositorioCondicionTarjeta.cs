﻿namespace Monibyte.Arquitectura.Dominio.Autorizador.Repositorios
{
    public interface IRepositorioCondicionTarjeta : IRepositorioCtrl<MB_CondicionTarjeta>
    {
        MB_CondicionTarjeta ObtenerPorNumTarjeta(string numTarjeta);
    }   
}

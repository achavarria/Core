﻿using System;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public class ParamMovimientosTc
    {
        public int IdEmpresa { get; set; }
        public int? IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdUsuario { get; set; }
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
        public ParamMovimientosGe FiltroGestiones { get; set; }
    }
    public class ParamMovimientosGe
    {
        public int[] Gestiones { get; set; }
        public int[] IdMovimientos { get; set; }
        public bool? MatchExists { get; set; }
    }

    public class PocConsultaEstCuenta
    {
        public int IdCuenta { get; set; }
        public int IdIdioma { get; set; }
        public int IdIdiomaBase { get; set; }
        public int IdEsCuentaMadre { get; set; }
        public DateTime FecCorte { get; set; }
        public String TablaEncabezado { get; set; }
        public String TablaDetalle { get; set; }
    }
}

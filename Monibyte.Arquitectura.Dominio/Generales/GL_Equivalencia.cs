using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Equivalencia : EntidadBase
    {
        public int Id { get; set; }
        public string Lista { get; set; }
        public string Origen { get; set; }
        public string ValOrigen { get; set; }
        public string Destino { get; set; }
        public string ValDestino { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

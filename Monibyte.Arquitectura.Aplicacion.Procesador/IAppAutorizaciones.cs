﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Procesador;

namespace Monibyte.Arquitectura.Aplicacion.Procesador
{
    public interface IAppAutorizaciones : IAplicacionBase
    {
        DataResult<PocAutorizaciones> ObetenerDenegaciones(PocFiltroAutorizaciones entrada, DataRequest request);
        string ObtieneDetalleDenegacion(string denailDetail);
    }
}

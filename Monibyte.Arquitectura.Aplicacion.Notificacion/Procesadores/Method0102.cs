﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Poco.Notificacion;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public partial class AppNotificacion
    {
        //Notificaiones provenientes del CORE (Gestiones)
        public void Method0102(PocoQueue pocoQueue)
        {
            var dataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(pocoQueue.Data);
            var numTar = dataDictionary.ContainsKey("CardNumber") ? dataDictionary["CardNumber"] as string : null;
            if (numTar != null)
            {
                var tarjeta = _repTarjeta.Table.FirstOrDefault(x => x.NumTarjeta == numTar);
                dataDictionary.Add("CardholderName", tarjeta.NombreImpreso);
            }
            else
            {
                var cuenta = dataDictionary.ContainsKey("IdCuenta") ? dataDictionary["IdCuenta"].ToString() : null;
                if (cuenta != null)
                {
                    var idCuenta = int.Parse(cuenta);
                    var titular =
                        (from t in _repTarjeta.Table
                         join c in _repPersona.Table
                             on t.IdEmpresa equals c.IdPersona
                         where t.IdCuenta == idCuenta
                         select new
                         {
                             t.NumTarjeta,
                             t.NombreImpreso,
                             c.NombreCompleto
                         }).FirstOrDefault();
                    dataDictionary.Add("CardNumber", titular.NumTarjeta);
                    dataDictionary.Add("CardholderName", titular.NombreImpreso ?? titular.NombreCompleto);
                }
            }
            pocoQueue.Data = dataDictionary.ToJson();
            ProcesarNotificaciones(pocoQueue, false, false, pocoQueue.CompanyId);
        }
    }
}
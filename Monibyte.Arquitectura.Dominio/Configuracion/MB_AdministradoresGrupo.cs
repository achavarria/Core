using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Configuracion
{
    public partial class MB_AdministradoresGrupo : EntidadBase
    {
        public int IdAdminGrupo { get; set; }
        public int IdUsuario { get; set; }
        public int IdGrupoEmpresa { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdSoloConsulta { get; set; }
        public int IdAutoriza { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Configuracion
{
    public partial class MB_HorarioPlantilla : EntidadBase
    {
        public int IdHorario { get; set; }
        public int IdPlantilla { get; set; }
        public int IdDia { get; set; }
        public Nullable<int> IdRestriccion { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime HoraDesde { get; set; }
        public System.DateTime HoraHasta { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Cargos
{
    public partial class CA_ValOrigenCargo : EntidadBase
    {
        public int IdTipoCargo { get; set; }
        public int IdValOrigen { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public Nullable<int> IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Seguridad.Repositorios;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public class SeguridadApp : AplicacionBase, ISeguridadApp
    {
        #region CONSTRUCTOR UNITY
        private IFuncionesSeguridad _fSeguridad;

        public SeguridadApp(IFuncionesSeguridad fSeguridad)
        {
            _fSeguridad = fSeguridad;
        }

        #endregion

        public IEnumerable<PocAccion> ObtenerPermisosPorUsuario()
        {
            return _fSeguridad.ObtenerPermisosUsuario()
                .ProyectarComo<PocAccion>();
        }

        public IEnumerable<PocAccionMenu> ObtenerMenuUsuario()
        {
            return _fSeguridad.ObtenerMenuUsuario()
                .ProyectarComo<PocAccionMenu>();
        }
    }
}
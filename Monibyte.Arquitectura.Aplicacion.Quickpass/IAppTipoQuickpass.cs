﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Quickpass;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public interface IAppTipoQuickpass : IAplicacionBase
    {
        void Insertar(PocTipoQuickpass registro);
        void Eliminar(PocTipoQuickpass registro);
        void Modificar(PocTipoQuickpass registro);
        DataResult<PocTipoQuickpass> Listar(PocTipoQuickpass filtro, DataRequest request);
        IEnumerable<PocTipoQuickpass> ListaTipoQuickpass();
    }
}

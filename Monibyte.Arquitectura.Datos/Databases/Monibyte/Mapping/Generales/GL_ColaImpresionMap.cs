using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ColaImpresionMap : EntityTypeConfiguration<GL_ColaImpresion>
    {
        public GL_ColaImpresionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCola);

            // Properties
            this.Property(t => t.IdCola)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Formato)
                .HasMaxLength(10);

            this.Property(t => t.NombreCompleto)
                .HasMaxLength(100);

            this.Property(t => t.CodUsuario)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Clave)
                .HasMaxLength(20);

            this.Property(t => t.PIN)
                .HasMaxLength(20);

            this.Property(t => t.Semilla)
                .HasMaxLength(100);

            this.Property(t => t.NombreEmpresa)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GL_ColaImpresion", "General");
            this.Property(t => t.IdCola).HasColumnName("IdCola");
            this.Property(t => t.Formato).HasColumnName("Formato");
            this.Property(t => t.Contenido).HasColumnName("Contenido");
            this.Property(t => t.NombreCompleto).HasColumnName("NombreCompleto");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Clave).HasColumnName("Clave");
            this.Property(t => t.PIN).HasColumnName("PIN");
            this.Property(t => t.Semilla).HasColumnName("Semilla");
            this.Property(t => t.NombreEmpresa).HasColumnName("NombreEmpresa");
            this.Property(t => t.IdTipoPersona).HasColumnName("IdTipoPersona");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_EstCuentaIndividualMap : EntityTypeConfiguration<TC_EstCuentaIndividual>
    {
        public TC_EstCuentaIndividualMap()
        {
            // Primary Key
            this.HasKey(t => t.IdEstIndividual);

            // Properties
            this.Property(t => t.CorreoEstCuenta)
                .HasMaxLength(120);

            this.Property(t => t.Detalle)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.CopiaCorreoEstCuenta)
                .HasMaxLength(120);

            // Table & Column Mappings
            this.ToTable("TC_EstCuentaIndividual", "TCredito");
            this.Property(t => t.IdEstIndividual).HasColumnName("IdEstIndividual");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.CorreoEstCuenta).HasColumnName("CorreoEstCuenta");
            this.Property(t => t.Detalle).HasColumnName("Detalle");
            this.Property(t => t.CopiaCorreoEstCuenta).HasColumnName("CopiaCorreoEstCuenta");
            this.Property(t => t.FecCorte).HasColumnName("FecCorte");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
        }
    }
}

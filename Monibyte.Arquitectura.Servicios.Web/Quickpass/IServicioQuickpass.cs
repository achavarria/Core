﻿using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Monibyte.Arquitectura.Servicios.Web.Quickpass
{
    [ServiceContract]
    public interface IServicioQuickpass : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "IncluirTipoQuickpass")]
        [Description("Servicio para incluir un Tipo de Quickpass")]
        Transporte IncluirTipoQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminaTipoQuickpass")]
        [Description("Servicio para eliminar un Tipo de Quickpass")]
        Transporte EliminaTipoQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarTipoQuickpass")]
        [Description("Servicio que devuelve el dataresult con la lista de los Tipos de Quickpass")]
        Transporte ListarTipoQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificaTipoQuickpass")]
        [Description("Servicio para modificar un Tipo de Quickpass")]
        Transporte ModificaTipoQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ImportarListaQuickpass")]
        [Description("Servicio para importar los quickpass dependiendo del número de factura")]
        Transporte ImportarListaQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "IncluirQuickpass")]
        [Description("Servicio para incluir un Quickpass")]
        Transporte IncluirQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminaQuickpass")]
        [Description("Servicio para eliminar un Quickpass")]
        Transporte EliminaQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarQuickpass")]
        [Description("Servicio que devuelve el dataresult con la lista de Quickpass")]
        Transporte ListarQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarMovimientosQP")]
        [Description("Servicio que devuelve el dataresult con la lista de los Movimientos de Quickpass que ya estan asignados")]
        Transporte ListarMovimientosQP(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificaQuickpass")]
        [Description("Servicio para modificar un Quickpass")]
        Transporte ModificaQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListaTipoQuickpass")]
        [Description("Servicio que devuelve la lista de los Tipos de Quickpass para utilizar en un listado")]
        Transporte ListaTipoQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarMovimientosQuickpass")]
        [Description("Servicio que devuelve el dataresult con la lista de los Movimientos de Quickpass")]
        Transporte ListarMovimientosQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "IncluirMovimientosQuickpass")]
        [Description("Servicio para incluir un Movimiento de Quickpass")]
        Transporte IncluirMovimientosQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminaMovimientosQuickpass")]
        [Description("Servicio para eliminar un Movimiento de Quickpass")]
        Transporte EliminaMovimientosQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificaMovimientosQuickpass")]
        [Description("Servicio para modificar un Movimiento de Quickpass")]
        Transporte ModificaMovimientosQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ImportarMovimientosQuickpass")]
        [Description("Servicio para importar desde webservice de ETC los Movimientos de Quickpass")]
        Transporte ImportarMovimientosQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "GenerarCargosMovimientosQuickpass")]
        [Description("Servicio para generar los cargos a la tarjeta dependiendo de los Movimientos de Quickpass")]
        Transporte GenerarCargosMovimientosQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EnviarListasQP")]
        [Description("Servicio para generar XML con lista de tags para enviar a ETC")]
        Transporte EnviarListasQP(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarListaTags")]
        [Description("Servicio que devuelve el dataresult con la lista de las listas de tags que se han enviado")]
        Transporte ListarListaTags(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ResultadoListasDeTags")]
        [Description("Servicio verificar el resultado de la lista de tags enviada a ETC")]
        Transporte ResultadoListasDeTags(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "GenerarCargosCorteQuickpass")]
        [Description("Servicio para generar los cargos a la tarjeta en la fecha de corte")]
        Transporte GenerarCargosCorteQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EjecutarProcesosQuickpass")]
        [Description("Servicio para ejecutar los procesos de importar movimientos, procesar los cargos, generación de cargos automáticos")]
        Transporte EjecutarProcesosQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarQuickpassAsignados")]
        [Description("Servicio que devuelve el dataresult con la lista de los Quickpass asignados")]
        Transporte ListarQuickpassAsignados(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EditarQuickpass")]
        [Description("Servicio para modificar un Quickpass Asignado")]
        Transporte EditarQuickpass(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificarEstadoQP")]
        [Description("Servicio para modificar el estado de un Quickpass")]
        Transporte ModificarEstadoQP(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ConsultarMontosMovQP")]
        [Description("Servicio que devuelve el dataresult con la lista de los QP con sus respectivos montos a cancelar")]
        Transporte ConsultarMontosMovQP(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "AplicarMontosMovQP")]
        [Description("Servicio que aplica los movimientos de QP según los parámetros otorgados")]
        Transporte AplicarMontosMovQP(Transporte transporte);

    }
}

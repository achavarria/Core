﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.TEF;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Monibyte.Arquitectura.Poco.TEF;
using System;
using System.Globalization;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public class PagosTarjeta : AplBase, IPagosTarjeta
    {
        private IOperacionesATH _appOperacionesATH;
        private IRepositorioMnb<GL_ParametroSistema> _repParametroSistema;
        private IRepositorioMnb<TF_PagoTrasaccion> _repPagosTrasaccion;
        private IRepositorioMnb<TS_Moneda> _repMoneda;
        private IRepositorioMnb<TC_vwTarjetasCuenta> _repVwTarjetasCuenta;
        private IFuncionesGenerales _funcionesGenerales;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;

        public PagosTarjeta(
         IOperacionesATH appOperacionesATH,
         IRepositorioMnb<GL_ParametroSistema> repParametroSistema,
         IRepositorioMnb<TF_PagoTrasaccion> repPagosTrasaccion,
         IRepositorioMnb<TS_Moneda> repMoneda,
         IRepositorioMnb<TC_vwTarjetasCuenta> repVwTarjetasCuenta,
         IFuncionesGenerales funcionesGenerales,
         IRepositorioMnb<GL_ParametroSistema> repParamSistema)
        {
            _appOperacionesATH = appOperacionesATH;
            _repParametroSistema = repParametroSistema;
            _repPagosTrasaccion = repPagosTrasaccion;
            _repMoneda = repMoneda;
            _repVwTarjetasCuenta = repVwTarjetasCuenta;
            _funcionesGenerales = funcionesGenerales;
            _repParamSistema = repParamSistema;
        }

        #region Transacciones SINPE
        public PocPagoTransaccion PagarTarjetaTEF(PocPagoTransaccion pocEntrada, bool EsPagoDirecto = true)
        {
            #region Aplicar el Pago a la tarjeta
            var pocMQ = new PocRespuestaMQ();
            if (EsPagoDirecto)
            {
                var _entrada = pocEntrada.ProyectarComo<PocPagoMQ>();
                pocMQ = AplicaPagoTarjeta(_entrada);
            }
            else
            {
                var _entrada = pocEntrada.ProyectarComo<PocPagoMQ>();
                pocMQ = AplicaPagoMiselaneo(_entrada);
            }
            #endregion

            pocEntrada.IdEstado = pocMQ.EstadoTrans;
            pocEntrada.DetalleError = pocMQ.DetalleError;
            pocEntrada.CodRefSiscard = pocMQ.NumReferencia;

            //Actualiza el pago con el estado de la Aplicacion en MQ
            if (InfoSesion.Info == null)
            {
                _repPagosTrasaccion.UpdateOn(w => w.IdPagoTrasaccion == pocEntrada.IdPagoTrasaccion,
                    up => new TF_PagoTrasaccion
                    {
                        IdEstado = pocMQ.EstadoTrans,
                        CodRefSiscard = pocMQ.NumReferencia,
                        IdUsuarioActualizaAud = Convert.ToInt32(_repParametroSistema.SelectById("ID_USUARIO_BASE").ValParametro),
                    });
            }
            else
            {
                _repPagosTrasaccion.UpdateOn(w => w.IdPagoTrasaccion == pocEntrada.IdPagoTrasaccion,
                    up => new TF_PagoTrasaccion
                    {
                        IdEstado = pocMQ.EstadoTrans,
                        CodRefSiscard = pocMQ.NumReferencia
                    });
            }

            return pocEntrada;
        }

        public PocPagoTransaccion PagarExtraIntra(PocPagoTransaccion pago)
        {
            var tipoPago = _repParametroSistema.SelectById("TIPO_PAGO_TARJETA_EFECTIVO").ValParametro.ToString();
            var entradaServicio = new PocEntradaPagoTarjeta
            {
                NumTarjeta = pago.NumTarjeta,
                MonedaISO = pago.IdMoneda == (int)EnumMoneda.Colones ? (int)MonedasInternacional.Colones : (int)MonedasInternacional.Dolares,
                TipoPago = tipoPago,
                Monto = decimal.Parse(pago.MontoPago.ToString(), CultureInfo.CurrentCulture),
                NumFinanEspecial = pago.NumExtraFinanciamiento,
                IdTransaccion = pago.NumComprobanteInterno.ToString()
            };
            var extraMQ = _appOperacionesATH.PagarExtraFinanciamiento(entradaServicio);

            if (extraMQ.NumReferencia != null)
            {
                pago.IdEstado = (int)EnumEstadoPago.Aplicada;
            }
            else
            {
                pago.IdEstado = (int)EnumEstadoPago.MQNoAplico;
            }

            pago.CodRefSiscard = extraMQ.NumReferencia;
            pago.DetalleError = extraMQ.DescripcionCodRespuesta;

            _repPagosTrasaccion.UpdateOn(w => w.IdPagoTrasaccion == pago.IdPagoTrasaccion,
                up => new TF_PagoTrasaccion
                {
                    IdEstado = pago.IdEstado,
                    CodRefSiscard = extraMQ.NumReferencia,
                    IdUsuarioActualizaAud = Convert.ToInt32(_repParametroSistema.SelectById("ID_USUARIO_BASE").ValParametro),
                });

            return pago;
        }
        #endregion

        #region Pagos Extrafinanciamientos
        public PocSalidaPagoTarjeta PagarExtraIntraDirigido(PocPagoTransaccion pago)
        {
            var tipoPago = _repParametroSistema.SelectById("TIPO_PAGO_TARJETA_EFECTIVO").ValParametro.ToString();

            var entradaServicio = new PocEntradaPagoTarjeta
            {
                NumTarjeta = pago.NumTarjeta,
                MonedaISO = pago.IdMoneda == (int)EnumMoneda.Colones ? (int)MonedasInternacional.Colones : (int)MonedasInternacional.Dolares,
                TipoPago = tipoPago,
                Monto = decimal.Parse(pago.MontoPago.ToString(), CultureInfo.CurrentCulture),
                NumFinanEspecial = pago.NumExtraFinanciamiento,
                IdTransaccion = pago.NumComprobanteInterno.ToString()
            };
            return _appOperacionesATH.PagarExtraFinanciamiento(entradaServicio);
        }
        public PocSalidaPagoTarjeta ReversarExtraIntraDirigido(PocPagoTransaccion pago)
        {
            var tipoPago = _repParametroSistema.SelectById("TIPO_PAGO_TARJETA_EFECTIVO").ValParametro.ToString();

            var entradaServicio = new PocEntradaPagoTarjeta
            {
                NumTarjeta = pago.NumTarjeta,
                MonedaISO = pago.IdMoneda == (int)EnumMoneda.Colones ? (int)MonedasInternacional.Colones : (int)MonedasInternacional.Dolares,
                TipoPago = tipoPago,
                Monto = decimal.Parse(pago.MontoPago.ToString(), CultureInfo.CurrentCulture),
                CodExtraFinanciamiento = _repParametroSistema.SelectById("COD_MOV_CRE_EXTRAFINAN").ValParametro,
                NumFinanEspecial = pago.NumExtraFinanciamiento,
                IdTransaccion = pago.NumComprobanteInterno.ToString()
            };
            return _appOperacionesATH.ReversaPagoExtraFinanciamiento(entradaServicio);
        }
        #endregion

        #region Metodos Privados
        private PocRespuestaMQ AplicaAdelantoEfectivo(PocPagoMQ pagoTras)
        {
            PocRespuestaMQ pocRespuesta = new PocRespuestaMQ();
            try
            {
                var entradaServicio = new PocEntradaAdelantoEfectivoTarjeta
                {
                    NumTarjeta = pagoTras.NumTarjeta,
                    MonedaISO = pagoTras.IdMoneda == (int)EnumMoneda.Colones ? (int)MonedasInternacional.Colones : (int)MonedasInternacional.Dolares,
                    Monto = decimal.Parse(pagoTras.Monto.ToString(), CultureInfo.CurrentCulture),
                    IdTransaccion = pagoTras.CodRefSiscard.ToString()
                };
                var resultado = _appOperacionesATH.AdelantoEfectivoTarjeta(entradaServicio);

                if (resultado.NumReferencia != null)
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.Aplicada;
                    pocRespuesta.FecLiquidacion = DateTime.Now;
                    pocRespuesta.NumReferencia = resultado.NumReferencia;
                    return pocRespuesta;
                }
                else
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                    pocRespuesta.DetalleError = "MQ no proceso pago";
                    return pocRespuesta;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.FullStackTrace(), ex.Message);
                pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                pocRespuesta.DetalleError = "Error de datos o comunicación con MQ";
                return pocRespuesta;
            }
        }
        private PocRespuestaMQ ReversarAdelantoEfectivo(PocPagoMQ pagoTras)
        {
            PocRespuestaMQ pocRespuesta = new PocRespuestaMQ();
            try
            {
                var entradaServicio = new PocEntradaAdelantoEfectivoTarjeta
                {
                    NumTarjeta = pagoTras.NumTarjeta,
                    MonedaISO = pagoTras.IdMoneda == (int)EnumMoneda.Colones ? (int)MonedasInternacional.Colones : (int)MonedasInternacional.Dolares,
                    Monto = decimal.Parse(pagoTras.Monto.ToString(), CultureInfo.CurrentCulture),
                    IdTransaccion = pagoTras.CodRefSiscard,
                    NumReferencia = pagoTras.NumReferencia.ToString()
                };
                var resultado = _appOperacionesATH.ReversaAdelantoEfectivoTarjeta(entradaServicio);

                if (resultado.NumReferencia != null)
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.Aplicada;
                    pocRespuesta.FecLiquidacion = DateTime.Now;
                    pocRespuesta.NumReferencia = resultado.NumReferencia;
                    return pocRespuesta;
                }
                else
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                    pocRespuesta.DetalleError = "MQ no proceso pago";
                    return pocRespuesta;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.FullStackTrace(), ex.Message);
                pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                pocRespuesta.DetalleError = "Error de datos o comunicación con MQ";
                return pocRespuesta;
            }
        }
        private PocRespuestaMQ AplicaPagoTarjeta(PocPagoMQ pagoTras)
        {
            PocRespuestaMQ pocRespuesta = new PocRespuestaMQ();
            try
            {
                var tipoPago = _repParametroSistema.SelectById("TIPO_PAGO_TARJETA_EFECTIVO").ValParametro.ToString();

                //Invocar método del app que realiza el pago de la tarjeta
                var entradaServicio = new PocEntradaPagoTarjeta
                {
                    NumTarjeta = pagoTras.NumTarjeta,
                    MonedaISO = pagoTras.IdMoneda == (int)EnumMoneda.Colones ? (int)MonedasInternacional.Colones : (int)MonedasInternacional.Dolares,
                    TipoPago = tipoPago,
                    Monto = decimal.Parse(pagoTras.Monto.ToString(), CultureInfo.CurrentCulture),
                    IdTransaccion = pagoTras.NumReferencia.ToString()
                };
                var resultado = _appOperacionesATH.PagoTarjeta(entradaServicio);

                if (resultado.NumReferencia != null)
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.Aplicada;
                    pocRespuesta.FecLiquidacion = DateTime.Now;
                    pocRespuesta.NumReferencia = resultado.NumReferencia;
                    return pocRespuesta;
                }
                else
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                    pocRespuesta.DetalleError = "MQ no proceso pago";
                    return pocRespuesta;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.FullStackTrace(), ex.Message);
                pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                pocRespuesta.DetalleError = "Error de datos o comunicación con MQ";
                return pocRespuesta;
            }
        }
        private PocRespuestaMQ AplicaPagoMiselaneo(PocPagoMQ pago)
        {
            PocRespuestaMQ pocRespuesta = new PocRespuestaMQ();
            try
            {
                //Invocar método del app que realiza el pago de la tarjeta
                var entradaServicio = new PocEntradaMiscelaneosTarjeta();
                entradaServicio.CodMovimiento = _repParametroSistema.SelectById("PAGOS_MISELANEOS").ValParametro;
                entradaServicio.MontoTransaccion = decimal.Parse(pago.Monto.ToString(), CultureInfo.CurrentCulture);
                entradaServicio.NumTarjeta = pago.NumTarjeta;
                entradaServicio.NumReferencia = pago.NumReferencia.ToString();
                entradaServicio.MonedaTransaccion = _repMoneda.SelectById(pago.IdMoneda).CodAlpha2;
                entradaServicio.FechaConsumo = pago.FecConsumo;

                var resultado = _appOperacionesATH.AplicaMiscelaneosTarjeta(entradaServicio);

                if (resultado.NumReferenciaSiscard != null)
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.Aplicada;
                    pocRespuesta.FecLiquidacion = DateTime.Now;
                    pocRespuesta.NumReferencia = resultado.NumReferenciaSiscard;
                    return pocRespuesta;
                }
                else
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                    pocRespuesta.DetalleError = "MQ no proceso pago";
                    return pocRespuesta;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.FullStackTrace(), ex.Message);
                pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                pocRespuesta.DetalleError = "Error de datos o comunicación con MQ";
                return pocRespuesta;
            }
        }
        private PocRespuestaMQ AplicarMiselaneoEspecial(PocPagoMQ pago, string CodMovimientoMQ)
        {
            PocRespuestaMQ pocRespuesta = new PocRespuestaMQ();
            try
            {
                //Invocar método del app que realiza el pago de la tarjeta
                var entradaServicio = new PocEntradaMiscelaneosTarjeta();
                entradaServicio.CodMovimiento = CodMovimientoMQ;
                entradaServicio.MontoTransaccion = decimal.Parse(pago.Monto.ToString(), CultureInfo.CurrentCulture);
                entradaServicio.NumTarjeta = pago.NumTarjeta;
                entradaServicio.NumReferencia = pago.NumReferencia.ToString();
                entradaServicio.MonedaTransaccion = _repMoneda.SelectById(pago.IdMoneda).CodAlpha2;
                entradaServicio.FechaConsumo = pago.FecConsumo;

                var resultado = _appOperacionesATH.AplicaMiscelaneosTarjeta(entradaServicio);

                if (resultado.NumReferenciaSiscard != null)
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.Aplicada;
                    pocRespuesta.FecLiquidacion = DateTime.Now;
                    pocRespuesta.NumReferencia = resultado.NumReferenciaSiscard;
                    return pocRespuesta;
                }
                else
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                    pocRespuesta.DetalleError = "MQ no proceso pago";
                    return pocRespuesta;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.FullStackTrace(), ex.Message);
                pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                pocRespuesta.DetalleError = "Error de datos o comunicación con MQ";
                return pocRespuesta;
            }
        }
        private PocRespuestaMQ AnularMiselaneoEspecial(PocPagoMQ pago, string CodMovimientoMQ)
        {
            PocRespuestaMQ pocRespuesta = new PocRespuestaMQ();
            try
            {
                //Invocar método del app que realiza el pago de la tarjeta
                var entradaServicio = new PocEntradaMiscelaneosTarjeta();
                entradaServicio.CodMovimiento = CodMovimientoMQ;
                entradaServicio.MontoTransaccion = decimal.Parse(pago.Monto.ToString(), CultureInfo.CurrentCulture);
                entradaServicio.NumTarjeta = pago.NumTarjeta;
                entradaServicio.NumReferencia = pago.NumReferencia.ToString();
                entradaServicio.MonedaTransaccion = _repMoneda.SelectById(pago.IdMoneda).CodAlpha2;
                entradaServicio.FechaConsumo = pago.FecConsumo;

                var resultado = _appOperacionesATH.AplicaMiscelaneosTarjeta(entradaServicio);

                if (resultado.NumReferenciaSiscard != null)
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.Aplicada;
                    pocRespuesta.FecLiquidacion = DateTime.Now;
                    pocRespuesta.NumReferencia = resultado.NumReferenciaSiscard;
                    return pocRespuesta;
                }
                else
                {
                    pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                    pocRespuesta.DetalleError = "MQ no proceso pago";
                    return pocRespuesta;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.FullStackTrace(), ex.Message);
                pocRespuesta.EstadoTrans = (int)EnumEstadoPago.MQNoAplico;
                pocRespuesta.DetalleError = "Error de datos o comunicación con MQ";
                return pocRespuesta;
            }
        }
        #endregion
    }
}

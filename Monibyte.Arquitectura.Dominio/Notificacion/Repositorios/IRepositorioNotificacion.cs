﻿using Monibyte.Arquitectura.Poco.Notificacion;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Notificacion.Repositorios
{
    public interface IRepositorioNotificacion : IRepositorioMnb<NT_Configuracion>
    {
        int ValidarFiltroNotificacion(string pWhere);
        IEnumerable<NT_Configuracion> ObtenerNotificaciones(int? idContexto, int? idEmpresa, int? idEsActivo, int ExcluirAdminInt);
        IEnumerable<PocDestinatarioDefault> ObtenerDestinatarios(int idEmpresa, string codUsuario, 
            string numTarjeta, bool notAdmSms, bool notAdmEmail, bool notTarSms, bool notTarEmail, 
            bool notUsrSms, bool notUsrEmail);
    }
}
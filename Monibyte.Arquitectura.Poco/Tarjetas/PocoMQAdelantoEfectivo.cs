﻿using System;

namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocEntradaAdelantoEfectivoTarjeta
    {
        public string NumTarjeta { get; set; }
        public string SuperFranquicia { get; set; }
        public string Localidad { get; set; }
        public int MonedaISO { get; set; }
        public decimal Monto { get; set; }
        public string IdTransaccion { get; set; }
        public string NumReferencia { get; set; }
        public string Usuario { get; set; }
    }


    public class PocSalidaAdelantoEfectivoTarjeta
    {
        public string TipoMensaje { get; set; }
        public string Emisor { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string SuperFranquicia { get; set; }
        public string Localidad { get; set; }
        public int MonedaISO { get; set; }
        public string TipoPago { get; set; }
        public decimal Monto { get; set; }
        public string CodBancoEmisor { get; set; }
        public string NumCheque { get; set; }
        public string NumFinanEspecial { get; set; }
        public string IdTransaccion { get; set; }
        public string CodigoRespuesta { get; set; }
        public string NumReferencia { get; set; }
        public string Usuario { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }

}

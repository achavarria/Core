﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Seguridad;
    using Monibyte.Arquitectura.Aplicacion.Seguridad.Mantenimientos;
    using Monibyte.Arquitectura.Datos.Seguridad.Repositorios;
    using Monibyte.Arquitectura.Dominio.Seguridad.Repositorios;

    public class ContenedorUnitySeguridad
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            _unityContainer.RegisterType<IRolesApp, RolesApp>();
            _unityContainer.RegisterType<ISeguridadApp, SeguridadApp>();
            _unityContainer.RegisterType<IDirectorioUsuarioApp, DirectorioUsuarioApp>();
            _unityContainer.RegisterType<IAppReportes, AppReportes>();
            _unityContainer.RegisterType<IAppAutorizacion, AppAutorizacion>();
            _unityContainer.RegisterType<IAppActivacionUsuario, AppActivacionUsuario>();
            _unityContainer.RegisterType<IAppOpMenuEmpresa, AppOpMenuEmpresa>();
            _unityContainer.RegisterType<IAppOpMenu, AppOpMenu>();
            #endregion

            #region Repositorios
            _unityContainer.RegisterType<IFuncionesSeguridad, FuncionesSeguridad>();
            _unityContainer.RegisterType<IRepositorioRole, RepositorioRole>();
            #endregion
        }
    }
}

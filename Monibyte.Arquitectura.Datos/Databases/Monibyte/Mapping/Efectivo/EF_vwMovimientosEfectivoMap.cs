using Monibyte.Arquitectura.Dominio.Efectivo;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Efectivo
{
    public class EF_vwMovimientosEfectivoMap : EntityTypeConfiguration<EF_vwMovimientosEfectivo>
    {
        public EF_vwMovimientosEfectivoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdMovimiento, t.IdEmpresa, t.FecMovimiento, t.IdContexto, t.Moneda });

            // Properties
            this.Property(t => t.IdMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdEmpresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DetalleMovimiento)
                .HasMaxLength(2000);

            this.Property(t => t.NumFactura)
                .HasMaxLength(20);

            this.Property(t => t.IdContexto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NombreCompleto)
                .HasMaxLength(160);

            this.Property(t => t.Moneda)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.CodMoneda)
                .HasMaxLength(2);

            this.Property(t => t.Simbolo)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("EF_vwMovimientosEfectivo", "Efectivo");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.DetalleMovimiento).HasColumnName("DetalleMovimiento");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
            this.Property(t => t.MonMovimiento).HasColumnName("MonMovimiento");
            this.Property(t => t.MonMovimientoLocal).HasColumnName("MonMovimientoLocal");
            this.Property(t => t.MonMovimientoInter).HasColumnName("MonMovimientoInter");
            this.Property(t => t.NumFactura).HasColumnName("NumFactura");
            this.Property(t => t.IdContexto).HasColumnName("IdContexto");
            this.Property(t => t.NombreCompleto).HasColumnName("NombreCompleto");
            this.Property(t => t.Moneda).HasColumnName("Moneda");
            this.Property(t => t.CodMoneda).HasColumnName("CodMoneda");
            this.Property(t => t.Simbolo).HasColumnName("Simbolo");
            this.Property(t => t.CamposDinamicos).HasColumnName("CamposDinamicos");
        }
    }
}

﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Quickpass;
    using Monibyte.Arquitectura.Datos.Quickpass.Repositorios;
    using Monibyte.Arquitectura.Dominio.Quickpass.Repositorios;

    public class ContenedorUnityQuickpass
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
          
            #region Servicios
            _unityContainer.RegisterType<IAppQuickpass, AppQuickpass>();
            _unityContainer.RegisterType<IAppTipoQuickpass, AppTipoQuickpass>();
            _unityContainer.RegisterType<IAppServiciosQuickpass, AppServiciosQuickpass>();
            _unityContainer.RegisterType<IAppMovimientosQuickpass, AppMovimientosQuickpass>();
            _unityContainer.RegisterType<IAppListaTags, AppListaTags>();
            #endregion

            #region Repositorios
            _unityContainer.RegisterType<IServiciosQuickpass, ServiciosQuickpass>();
            _unityContainer.RegisterType<IFuncionesQuickpass, FuncionesQuickpass>();           
            #endregion
        }
    }
}

﻿using Monibyte.Arquitectura.Aplicacion.Tarjetas;
using Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Poco.Configuracion;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Servicios.Web.Tarjetas
{
    public class ServicioInfoTarjeta : ServicioUnity, IServicioInfoTarjeta
    {
        public Transporte ObtenerTarjetasCuentaUsuario(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var idCuenta = transporte.GetValor<int?>("idCuenta");
                var incluirHijas = transporte.GetValor<bool>("incluirHijas");
                var filtroEstado = transporte.GetValor<int[]>("filtroEstado");
                var filtroEstadoExc = transporte.GetValor<int[]>("filtroEstadoExc");
                var ocultarNoEditables = transporte.GetValor<bool>("ocultarNoEditables");
                var incluirOperativas = transporte.GetValor<bool>("incluirOperativas");
                var resultado = app.ObtenerTarjetasCuentaUsuario(idCuenta,
                    incluirHijas, filtroEstado, filtroEstadoExc, ocultarNoEditables, incluirOperativas);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtenerTarjetasCuentaUsuarioDataResult(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var dr = transporte.GetDataRequest();
                var idCuenta = transporte.GetValor<int>("idCuenta");
                var incluirHijas = transporte.GetValor<bool>("incluirHijas");
                var filtroEstado = transporte.GetValor<int[]>("filtroEstado");
                var filtroEstadoExc = transporte.GetValor<int[]>("filtroEstadoExc");
                var incluirOperativas = transporte.GetValor<bool>("incluirOperativas");
                var resultado = app.ObtenerTarjetasCuentaUsuarioDataResult(dr,
                    idCuenta, incluirHijas, filtroEstado, filtroEstadoExc, incluirOperativas);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtenerCuentaUsuario(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var numCuenta = transporte.GetValor<string>("numCuenta");
                var resultado = app.ObtenerCuentaUsuario(numCuenta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtenerTarjetaUsuario(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var numTarjeta = transporte.GetValor<string>("numTarjeta");
                var resultado = app.ObtenerTarjetaUsuario(numTarjeta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ConsultarTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaConsultaTarjeta>();
                var resultado = app.ConsultaTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte PagarTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaPagoTarjeta>();
                var resultado = app.PagoTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ReversarPagoTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaPagoTarjeta>();
                var resultado = app.ReversaPagoTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte PagoEspecialTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaPagoTarjeta>();
                var resultado = app.PagoEspecialTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ReversarPagoEspecialTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaPagoTarjeta>();
                var resultado = app.ReversaPagoEspecialTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ActualizarEstadoTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaActualizaTarjeta>();
                var resultado = app.ActualizaEstadoTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ActualizarLimiteTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var numTarjeta = transporte.GetValor<string>("NUMTARJETA");
                var nuevoLimite = transporte.GetValor<decimal>("NUEVOLIMITE");
                app.AplicarCambioLimiteTarjeta(numTarjeta, nuevoLimite);
                return transporte;
            }
        }
        public Transporte ActualizarVipTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocParametrosVip>();
                var resultado = app.ActivarVip(new PocParametrosVip
                {
                    NumCuenta = entradaServicio.NumCuenta,
                    NumTarjeta = entradaServicio.NumTarjeta,
                    ValidaVigencia = true,
                    FechaHasta = entradaServicio.FechaHasta
                });
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ActivarTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaActivaTarjeta>();
                var resultado = app.ActivaTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ReponerPinTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaReposicionPinTarjeta>();
                var resultado = app.ReposicionPinTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte MovimientosTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaMovimientosTarjeta>();
                var resultado = app.MovimientosTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte MovimientosTransitoProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaMovimientosTarjeta>();
                var resultado = app.MovimientosTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ConsultaProgLealtadTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaLealtadTarjeta>();
                var resultado = app.ConsultaProgLealtadTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte AplicaMovProgLealtadTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaAplicMovLealtadTarjeta>();
                var resultado = app.AplicaMovProgLealtadTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ConsultaCuentasDeClienteProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaConsultaCuentas>();
                var resultado = app.ConsultaCuentasDeCliente(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte AplicaMiscelaneosTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaMiscelaneosTarjeta>();
                var resultado = app.AplicaMiscelaneosTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte AdelantoEfectivoTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaAdelantoEfectivoTarjeta>();
                var resultado = app.AdelantoEfectivoTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ReversaAdelantoEfectivoTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaAdelantoEfectivoTarjeta>();
                var resultado = app.AdelantoEfectivoTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte InclusionSeguroTarjetaProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaInclusionSeguroTarjeta>();
                var resultado = app.InclusionSeguroTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte GeneraCargoAutomaticoProcesador(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesATH>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaCargoAutomaticoTarjeta>();
                var resultado = app.GeneraCargoAutomaticoTarjeta(entradaServicio);
                transporte.SetDatos(null);
                return transporte;
            }
        }
        public Transporte ConsultarDetalleCtaTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var entradaServicio = transporte.GetDatos<PocTarjetaCuentaUsuario>();
                var consolida = transporte.GetValor<bool>("consolida");
                var consolidaMq = transporte.GetValor<bool>("consolidaMq");
                var resultado = app.ObtenerDetalleCtaTarjeta(
                    entradaServicio, consolida, consolidaMq);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ConsultarMillas(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var numCuenta = transporte.GetValor<string>("numCuenta");
                var resultado = app.ConsultarMillas(numCuenta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ConsultarTarjetasCliente(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var idCliente = transporte.GetValor<int>("idCliente");
                var idCuenta = transporte.GetValor<int?>("idCuenta");
                var resultado = app.ObtenerTarjetasCliente(idCliente, idCuenta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ActualizaDetalleTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaConsultaTarjeta>();
                app.ActualizarDetalleTarjeta(entradaServicio);
                transporte.SetDatos(null);
                return transporte;
            }
        }
        public Transporte ActivarTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaActivaTarjeta>();
                app.ActivarTarjeta(entradaServicio);
                transporte.SetDatos(null);
                return transporte;
            }
        }
        public Transporte ReponerPINTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var entradaServicio = transporte.GetDatos<PocEntradaReposicionPinTarjeta>();
                app.ReponerPinTarjeta(entradaServicio);
                transporte.SetDatos(null);
                return transporte;
            }
        }
        public Transporte ObtieneFechaCorte(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var idCuenta = transporte.GetDatos<int>();
                var resultado = app.ObtieneFechaCorte(idCuenta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte MovimientosTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IMovimientosTarjeta>())
            {
                var entradaServicio = transporte.GetDatos<PocParametrosTarjeta>();
                var resultado = app.ObtenerMovimientosTarjeta(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte MovimientosTransitoTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IMovimientosTarjeta>())
            {
                var entradaServicio = transporte.GetDatos<PocParametrosTarjeta>();
                var metodoSimple = transporte.GetValor<bool>("metodoSimple");
                var resultado = app.ObtenerMovTransitoTarjeta(entradaServicio, metodoSimple);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtieneResumenMovimientos(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IMovimientosTarjeta>())
            {
                var entradaServicio = transporte.GetDatos<PocParametrosTarjeta>();
                var resultado = app.ObtieneResumenMovimientos(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtieneMovTipoComercio(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IMovimientosTarjeta>())
            {
                var entradaServicio = transporte.GetDatos<PocParametrosTarjeta>();
                var categoria = transporte.GetValor<string>("categoria");
                var resultado = app.ObtieneMovTipoComercio(entradaServicio, categoria);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ListaTiposTarjetaCliente(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var idPersona = transporte.GetValor<int>("idPersona");
                var resultado = app.ListaTiposTarjetaCliente(idPersona);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte AplicarBloqueo(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var idTarjeta = transporte.GetValor<int>("idTarjeta");
                var idTipoBloqueo = transporte.GetValor<int>("idTipoBloqueo");
                var idCuenta = transporte.GetValor<int>("idCuenta");
                app.AplicarBloqueo(idTarjeta, idTipoBloqueo, idCuenta);
                return transporte;
            }
        }
        public Transporte ObtenerDireccion(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var idTarjeta = transporte.GetValor<int>("idTarjeta");
                var resultado = app.ObtenerDireccion(idTarjeta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte GuardarDetalleMovimiento(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IMovimientosTarjeta>())
            {
                var entrada = transporte.GetDatos<List<PocMovimientosTarjeta>>();
                var dynamicJson = transporte.GetValor<string>("DYNAMICJSON");
                var noEditStr = transporte.GetValor<bool>("noEditStr");
                var archivo = transporte.GetValor<List<PocArchivoMovimientoTc>>("Archivos");
                app.GuardarDetalleMovimiento(entrada, dynamicJson, noEditStr, archivo);
                return transporte;
            }
        }
        public Transporte GuardarDetalleTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var entrada = transporte.GetDatos<PocTarjetaCuentaUsuario>();
                app.GuardarDetalleTarjeta(entrada);
                return transporte;
            }
        }
        public Transporte GenerarReporteEstCuenta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>())
            {
                var entrada = transporte.GetDatos<List<PocCuentasCorte>>();
                var idMonedaLocal = transporte.GetValor<int>("idMonedaLocal");
                var idMonedaInter = transporte.GetValor<int>("idMonedaInter");
                var tablaDetalle = transporte.GetValor<string>("tablaDetalle");
                var tablaEncabezado = transporte.GetValor<string>("tablaEncabezado");
                app.GenerarReporteEstCuenta(entrada, idMonedaLocal, idMonedaInter, tablaDetalle, tablaEncabezado);
                return transporte;
            }
        }
        public Transporte DescargarEstCuenta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>())
            {
                var entrada = transporte.GetDatos<PocComerciosTarjeta>();
                string anno = transporte.GetValor<string>("anno");
                string mes = transporte.GetValor<string>("mes");
                int idCuenta = transporte.GetValor<int>("idCuenta");
                int esCuentaMadre = transporte.GetValor<int>("esCuentaMadre");
                var resultado = app.DescargarEstCuenta(anno, mes, idCuenta, esCuentaMadre);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtenerEstCuentaComerciosTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>())
            {
                var entrada = transporte.GetDatos<PocComerciosTarjeta>();
                var idMonedaLocal = transporte.GetValor<int>("IdMonedaLocal");
                var idMonedaInter = transporte.GetValor<int>("IdMonedaInter");
                var resultado = app.ObtenerEstCuentaComerciosTarjeta(entrada, idMonedaLocal, idMonedaInter);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte EnviarCorreoEstCuenta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>())
            {
                var entrada = transporte.GetDatos<List<PocCuentasCorte>>();
                var idMonedaLocal = transporte.GetValor<int>("idMonedaLocal");
                var idMonedaInter = transporte.GetValor<int>("idMonedaInter");
                var tablaDetalle = transporte.GetValor<string>("tablaDetalle");
                var tablaEncabezado = transporte.GetValor<string>("tablaEncabezado");
                app.EnviarCorreoEstCuenta(entrada, idMonedaLocal, idMonedaInter, tablaDetalle, tablaEncabezado);
                return transporte;
            }
        }
        public Transporte ConsultarCuentasCliente(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var idCliente = transporte.GetValor<int>("idCliente");
                var resultado = app.ObtenerCuentasCliente(idCliente);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte CantidadEstCuenta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                int idCuenta = transporte.GetValor<int>("idCuenta");
                var resultado = app.CantidadEstCuenta(idCuenta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte CantidadEstCuentaGlobal(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var resultado = app.CantidadEstCuentaGlobal();
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtenerPersonasTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var resultado = app.ObtenerPersonasTarjeta();
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtenerTarjetas(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var data = transporte.GetDatos<PocConsultaTarjetasAdmin>();
                var resultado = app.ObtenerTarjetas(data, transporte.GetDataRequest());
                transporte.SetDatos(resultado);
                return transporte;
            }
        }

        public Transporte ObtenerTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var entrada = transporte.GetDatos<PocTarjeta>();
                var resultado = app.ObtenerTarjeta(entrada.IdTarjeta, entrada.NumTarjeta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }

        public Transporte ObtenerLimitesTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var numTarjeta = transporte.GetValor<string>("NumTarjeta");
                var resultado = app.ObtenerLimitesTarjeta(numTarjeta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte DescargarResumenEstCuenta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>())
            {
                var datosEntrada = transporte.GetDatos<PocResumenEstCuenta>();
                var resultado = app.DescargarResumenEstCuenta(datosEntrada);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtenerMovtsDetEstadoCuenta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>())
            {
                var datosEntrada = transporte.GetDatos<PocParametrosTarjeta>();
                var resultado = app.ObtenerMovtsDetEstadoCuenta(datosEntrada);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ActualizarCorreosTarjeta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var datosEntrada = transporte.GetValor<List<PocTarjetaCuentaUsuario>>("datosSalida");
                var modPrevio = transporte.GetValor<List<PocTarjetaCuentaUsuario>>("modPrevio");
                app.ActualizarCorreosTarjeta(datosEntrada, modPrevio);
                return transporte;
            }
        }
        public Transporte GuardarCopiasDeCorreo(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var tarjetas = transporte.GetValor<int[]>("tarjetas");
                var texto = transporte.GetValor<string>("texto");
                app.GuardarCopiasDeCorreo(tarjetas, texto);
                return transporte;
            }
        }
        public Transporte GenerarEstCuentaIndividual(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>())
            {
                var anno = transporte.GetValor<int>("anno");
                var mes = transporte.GetValor<int>("mes");
                var idCuenta = transporte.GetValor<int>("idCuenta");
                var idTarjeta = transporte.GetValor<int>("idTarjeta");
                var resultado = app.GenerarEstCuentaIndividual
                    (idCuenta, mes, anno, idTarjeta, InfoSesion.Info.IdCompania);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ReenviarEstCuenta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>())
            {
                var entrada = transporte.GetDatos<PocConsultaEstadoCuenta>();
                var resultado = app.ReenviarEstCuenta(entrada);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtenerReporteMovimientos(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IMovimientosTarjeta>())
            {
                var entrada = transporte.GetValor<PocParametrosTarjeta>("datosEntrada");
                var idReporte = transporte.GetValor<int>("idReporte");
                var resultado = app.ObtenerReporteMovimientos(idReporte, entrada);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte AgregarMovimientos(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IMovimientosTarjeta>())
            {
                var entrada = transporte.GetDatos<PocMovimientoTarjeta>();
                app.NuevoMovimiento(entrada);
                return transporte;
            }
        }
        public Transporte GenerarEstMultiCuenta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>())
            {
                var entrada = transporte.GetDatos<PocGeneraEstCuenta>();
                var resultado = app.GenerarEstMultiCuenta(entrada);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte GuardarGeoLocalizacion(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IGeoLocalizacion>())
            {
                app.GuardarGeoLocalizacion();
                return transporte;
            }
        }
        public Transporte MovimientosCombustible(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IMovimientosTarjeta>())
            {
                var entradaServicio = transporte.GetDatos<PocParametrosTarjeta>();
                var resultado = app.ObtieneMovCombustible(entradaServicio);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ActualizarPlaca(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IOperacionesTarjeta>())
            {
                var nombreImpreso = transporte.GetValor<string>("nombreImpreso");
                var placa = transporte.GetValor<string>("placa");
                var resultado = app.ActualizarPlaca(nombreImpreso, placa);
                if (resultado.Codigo == "02")
                {
                    resultado.Descripcion = "Error no manejado";
                }
                else if (resultado.Codigo == "01")
                {
                    resultado.Descripcion = "Tarjeta no encontrada";
                }
                else { resultado.Descripcion = "Exitoso"; }
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public byte[] ObtenerReporteConsumo(PocDetalleConsumo datosEntrada)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReportesConsumo>())
            {
                var bytes = app.ObtenerReporteConsumo(datosEntrada);
                return bytes;
            }
        }
        public Transporte ConsultarDetalleCuenta(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var entrada = transporte.GetDatos<PocTarjeta>();
                var idTarjeta = entrada.IdTarjeta;
                var resultado = app.ObtenerDetalleCtaTarjeta(idTarjeta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public Transporte ObtenerCuentasUsuarioPermitidas(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var filtroEstado = transporte.GetValor<int[]>("filtroEstado");
                var titular = transporte.GetValor<int?>("IdTitular");
                var resultado = app.ObtenerCuentasUsuarioPermitidas(filtroEstado, titular);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        
        public Transporte ObtenerLimites(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var entrada = transporte.GetDatos<PocTarjeta>();
                var resultado = app.ObtenerLimitesTarjeta(entrada.IdTarjeta);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
        public byte[] ObtenerReporteEjecutivos(PocDetalleConsumo datosEntrada)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReportesConsumo>())
            {
                var bytes = app.ObtenerReporteEjecutivos(datosEntrada);
                return bytes;
            }
        }
        public PocDetalleCtaTarjeta ConsultarDetTarjEspecifica(int idCuenta, int idTarjeta)
        {
            using (var app = GestorUnity.CargarUnidad<IConsultasTarjeta>())
            {
                var resultado = app.ConsultarDetTarjEspecifica(idCuenta, idTarjeta);
                return resultado;
            }
        }

        public List<PocExtraFinanciamiento> ConsultarExtraFinancUsuario(int IdUsuario, int IdCuenta)
        {
            var app = GestorUnity.CargarUnidad<IConsultasTarjeta>();
            return app.ConsultarExtraFinancUsuario(IdUsuario, IdCuenta);
        }

        public EncabezadoEstCuenta ObtieneEncabezadoEsteCuenta(PocCuentasCorte entrada)
        {
            var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>();
            return app.ObtieneEncabezadoEsteCuenta(entrada);
        }

        public PocInfoEstCuenta ObtieneEstadoCuenta(PocCuentasCorte entrada)
        {
            var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>();
            return app.ObtieneEstCuenta(entrada);
        }

        public PocCuenta ObtenerCuenta(int idCuenta, string numCuenta)
        {
            var app = GestorUnity.CargarUnidad<IConsultasTarjeta>();
            return app.ObtenerCuenta(idCuenta, numCuenta);
        }

        public List<PocInfoDetalleEstCuenta> ObtieneDetalleEstCuenta(PocCuentasCorte entrada)
        {
            var app = GestorUnity.CargarUnidad<IAppReporteEstCuenta>();
            return app.ObtieneDetalleEstCuenta(entrada);
        }
        

        public IEnumerable<PocTarjetasAdministrativo> ObtieneTarjetas(
            PocConsultaTarjetasAdmin consulta)
        {
            var app = GestorUnity.CargarUnidad<IConsultasTarjeta>();
            return app.ObtenerTarjetas(consulta);
        }
    }
}

﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tesoreria;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<TS_Moneda> TS_Moneda { get; set; }
        public DbSet<TS_TipoDeCambio> TS_TipoDeCambio { get; set; }
        public DbSet<TS_TipoDeCambioH> TS_TipoDeCambioH { get; set; }

        void CreateContextModelTS(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TS_MonedaMap());
            modelBuilder.Configurations.Add(new TS_TipoDeCambioMap());
            modelBuilder.Configurations.Add(new TS_TipoDeCambioHMap());
        }
    }
}

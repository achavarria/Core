﻿using System;
using System.Collections.Generic;
using System.Linq;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.ServiceModel;
using System.ServiceModel.Web;
using Monibyte.Arquitectura.Poco.TEF;
using Monibyte.Arquitectura.Comun.Nucleo.Data;

namespace Monibyte.Arquitectura.Servicios.Web.TEF
{
    [ServiceContract]
    public interface IServicioPagos : IServicioUnity
    {
        #region Mantenimiento Cedulas Autorizadas
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "IncluirCedulaAutorizada")]
        Transporte IncluirCedulaAutorizada(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminaCedulaAutorizada")]
        Transporte EliminaCedulaAutorizada(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificaCedulaAutorizada")]
        Transporte ModificaCedulaAutorizada(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListarCedulaAutorizada")]
        Transporte ListarCedulaAutorizada(Transporte transporte);
        #endregion

        #region PagosPendientes
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ListarPagosPendientes")]
        DataResult<PocPagoPendiente> ListarPagosPendientes
            (PocPagoPendiente filtro, DataRequest request);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ListarPagos")]
        List<PocPagoPendiente> ListarPagos(int IdTrasaccion);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminarPagosPendientes")]
        void EliminarPagosPendientes(PocPagoPendiente pagoSinpe);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "AplicarPagoPendiente")]
        void AplicarPagoPendiente(PocPagoPendiente pagoSinpe);
        #endregion

        #region PagosDistribuidos
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ListarPagosAplicados")]
        PocEncPagoDirigido ListarPagosAplicados(int IdCuenta);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "AplicarPagoDistribuido")]
        List<PocDirigirPago> AplicarPagoDistribuido(List<PocDirigirPago> transporte);
        #endregion
    }
}

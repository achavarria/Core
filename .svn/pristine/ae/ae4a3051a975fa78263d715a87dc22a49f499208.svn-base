﻿using System;

namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocCuentaUsuario
    {
        public int IdEsCuentaMadre { get; set; }
        public int? IdCuentaMadre { get; set; }
        public int IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public decimal MonDisponibleLocal { get; set; }
        public decimal MonDisponibleInter { get; set; }
        public decimal? LimiteGlobal { get; set; }
        public decimal? LimiteCreditoCtaLocal { get; set; }
        public decimal? LimiteCreditoCtaInter { get; set; }
        public DateTime? FecPagoContado { get; set; }
        public DateTime? FecUltCorte { get; set; }
        public int IdTipoTarjeta { get; set; }
        public string Descripcion { get; set; }
        public int IdEstadoCta { get; set; }
        public string DesEstadoCuenta { get; set; }
        public string TarjetaHabiente { get; set; }
        public string Texto { get; set; }
    }

    public class PocConsultaEstadoCuenta
    {
        public int idCuenta { get; set; }
        public int? idTarjeta { get; set; }
        public int idTipoTarjeta { get; set; }
        public int idRelacion { get; set; }
        public string anno { get; set; }
        public string mes { get; set; }
        public string correo { get; set; }
        public string numTarjeta { get; set; }
        public int agrupador { get; set; }
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
        public byte[] bytes { get; set; }
    }

    public class PocTarjetaCuentaUsuario : IEquatable<PocTarjetaCuentaUsuario>
    {
        public int IdEsCuentaMadre { get; set; }
        public int? IdCuentaMadre { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public int IdPersona { get; set; }
        public string NumIdentificacion { get; set; }
        public int IdRelacion { get; set; }
        public int? IdUsuarioTarjeta { get; set; }
        public DateTime FecVencimiento { get; set; }
        public string NombreImpreso { get; set; }
        public int IdEstadoTarjeta { get; set; }
        public string Texto { get; set; }
        public string DynamicJson { get; set; }
        public string IdTarjetahabiente { get; set; }
        public decimal? LimiteCreditoTarjetaLocal { get; set; }
        public decimal? LimiteCreditoTarjetaInter { get; set; }
        public bool Autoriza { get; set; }
        public bool SoloConsulta { get; set; }
        public string Correo { get; set; }
        public string CorreoEstCuenta { get; set; }
        public string CopiaCorreoEstCuenta { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public string CodUsuarioTarjeta { get; set; }
        public int IdTipoPersonaEmpresa { get; set; }
        public string Descripcion { get; set; }
        public bool FueAgregada { get; set; }
        public int IdAplicaMillas { get; set; }
        public int DiaCorte { get; set; }
        //----------------------------------------------------------------
        public int IdEstadoCta { get; set; }
        public string NumCuenta { get; set; }
        public int IdTipoTarjeta { get; set; }
        public decimal MonDisponibleLocal { get; set; }
        public decimal MonDisponibleInter { get; set; }
        public decimal? LimiteGlobal { get; set; }
        public decimal? LimiteCreditoCtaLocal { get; set; }
        public decimal? LimiteCreditoCtaInter { get; set; }
        public DateTime? FecUltCorte { get; set; }
        public DateTime? FecPagoContado { get; set; }
        public string TarjetaHabiente { get; set; }
        //----------------------------------------------------------------        
        public string TextoTitular { get; set; }
        public string EstadoTarjeta { get; set; }        
        public string DescripcionEstadoTarjeta { get; set; }

        public bool Equals(PocTarjetaCuentaUsuario obj)
        {
            return this.IdTarjeta == obj.IdTarjeta &&
                this.CorreoEstCuenta == obj.CorreoEstCuenta &&
                this.CopiaCorreoEstCuenta == obj.CopiaCorreoEstCuenta;
        }
    }

    public class PocParametrosTarjeta
    {
        public bool Mq { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdEmpresa { get; set; }
        public int? IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdPersona { get; set; }
        public int? IdMcc { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
        public string TipoAccion { get; set; }
        public int IdEsCuentaMadre { get; set; }
        public PocTarjetaCuentaUsuario[] Tarjetas { get; set; }
    }

    public class PocDetalleCtaTarjeta
    {
        public int IdCuenta { get; set; }
        public int IdPersona { get; set; }
        public string NumCuenta { get; set; }
        public string TarjetaHabiente { get; set; }
        public decimal? SaldoLocal { get; set; }
        public decimal? PagoMinLocal { get; set; }
        public decimal? PagoContadoLocal { get; set; }
        public decimal? DisponibleLocal { get; set; }
        public decimal? DispAdelantoLocal { get; set; }
        public decimal? DebitosLocal { get; set; }
        public decimal? CreditosLocal { get; set; }
        public decimal? SubtSaldoCorteLocal { get; set; }
        public decimal? CargosBonifLocal { get; set; }
        public decimal? SaldoTotalCorteLocal { get; set; }
        public decimal? SaldofPlazoLocal { get; set; }
        public decimal? CargosNoBonifLocal { get; set; }
        public decimal? RecargoCuotaLocal { get; set; }
        public decimal? TotalCalcPMLocal { get; set; }
        public string CuentaClienteLocal { get; set; }
        public string CuentaClienteInter { get; set; }
        //------------------------------------------------------------------------------------
        public decimal? SaldoInter { get; set; }
        public decimal? PagoMinInter { get; set; }
        public decimal? PagoContadoInter { get; set; }
        public decimal? DisponibleInter { get; set; }
        public decimal? DispAdelantoInter { get; set; }
        public decimal? DebitosInter { get; set; }
        public decimal? CreditosInter { get; set; }
        public decimal? SubtSaldoCorteInter { get; set; }
        public decimal? CargosBonifInter { get; set; }
        public decimal? SaldoTotalCorteInter { get; set; }
        public decimal? SaldofPlazoInter { get; set; }
        public decimal? CargosNoBonifInter { get; set; }
        public decimal? RecargoCuotaInter { get; set; }
        public decimal? TotalCalcPMInter { get; set; }
        //------------------------------------------------------------------------------------
        public decimal? LimiteCuentaLocal { get; set; }
        public decimal? LimiteCuentaInter { get; set; }
        public DateTime? FecUltCorte { get; set; }
        public DateTime? FecVencePagoMinimo { get; set; }
        public DateTime? FecVencePagoContado { get; set; }
        //------------------------------------------------------------------------------------
        public decimal? LimiteEFLocal { get; set; }
        public decimal? SaldoInicialEFLocal { get; set; }
        public decimal? DebitosEFLocal { get; set; }
        public decimal? CreditosEFLocal { get; set; }
        public decimal? SaldoFinalEFLocal { get; set; }
        public decimal? DisponibleEFLocal { get; set; }
        public decimal? LimiteEFInter { get; set; }
        public decimal? SaldoInicialEFInter { get; set; }
        public decimal? DebitosEFInter { get; set; }
        public decimal? CreditosEFInter { get; set; }
        public decimal? SaldoFinalEFInter { get; set; }
        public decimal? DisponibleEFInter { get; set; }
        public decimal? DispAdelantoEfectivoLocal { get; set; }
        public decimal? DispAdelantoEfectivoInter { get; set; }
        //------------------------------------------------------------------------------------
        public string EstadoCuenta { get; set; }
        public string EstadoTarjeta { get; set; }
        //------------------------------------------------------------------------------------
        public int? ImportesVencLocal { get; set; }
        public decimal? MonImportesVencLocal { get; set; }
        public int? ImportesVencInter { get; set; }
        public decimal? MonImportesVencInter { get; set; }
        //------------------------------------------------------------------------------------
        public string NumExtrafinamiento { get; set; }
        public decimal? DebitoTransitoLocal { get; set; }
        public decimal? CreditoTransitoLocal { get; set; }
        public decimal? DebitoTransitoInter { get; set; }
        public decimal? CreditoTransitoInter { get; set; }
        public decimal? TasaInteresMensLocal { get; set; }
        public decimal? TasaInteresMensInter { get; set; }
        public decimal? TasaMoraMensLocal { get; set; }
        public decimal? TasaMoraMensInter { get; set; }
        public decimal? SaldoInicialCorteLocal { get; set; }
        public decimal? SaldoInicialCorteInter { get; set; }
        //--------------------------------------------------------------------------------------
        public string SimboloMonedaLocal { get; set; }
        public string SimboloMonedaInter { get; set; }
        //--------------------------------------------------------------------------------------
        public decimal SaldoInicialMillas { get; set; }
        public decimal DebitoMillas { get; set; }
        public decimal CreditoMillas { get; set; }
        public decimal TotalMillas { get; set; }
        public DateTime? HoraUltActualizacion { get; set; }

        public decimal? SaldoInicioMesLocal { get; set; }
        public decimal? SaldoInicioMesInter { get; set; }
        //--------------------------------------------------------------------------------------
        public PocDetalleTarjeta DetalleAdicional { get; set; }

        public int IdTarjeta { get; set; }
        public int IdEstado { get; set; }
        public string Relacion { get; set; }
        public int? IdRelacion { get; set; }
        public decimal? SaldoExtLocal { get; set; }
        public decimal? SaldoExtInter { get; set; }
        public decimal? LimiteCreditoAutorizado { get; set; }
        public decimal? SaldoAcumInter { get; set; }
    }

    public class PocDetalleTarjeta
    {
        public int IdTipoPersona { get; set; }
        public string NumTarjeta { get; set; }
        public string EstadoTarjeta { get; set; }
        public string PersonaTitular { get; set; }
        public decimal? DisponibleLocalTarjeta { get; set; }
        public decimal? DispAdelantoLocalTarjeta { get; set; }
        //------------------------------------------------------------------------------------
        public decimal? DisponibleInterTarjeta { get; set; }
        public decimal? DispAdelantoInterTarjeta { get; set; }
        //------------------------------------------------------------------------------------
        public decimal? LimiteCreditoLocal { get; set; }
        public decimal? LimiteCreditoInter { get; set; }
        public decimal? SaldoTarjetaLocal { get; set; }
        public decimal? SaldoTarjetaInter { get; set; }
    }
    public class PocConsultaTarjeta
    {
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdPersona { get; set; }
        public int IdRelacion { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombrePlastico { get; set; }
        public int IdEstado { get; set; }
        public string DescripcionEstado { get; set; }
        public string Texto { get; set; }
    }
    public class PocMovimientosTarjeta
    {
        public int? IdMcc { get; set; }
        public int? IdCorte { get; set; }
        public int IdMovimiento { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public DateTime FecTransaccion { get; set; }
        public DateTime? FecMovimiento { get; set; }
        public DateTime FecCorte { get; set; }
        public string NumReferencia { get; set; }
        public string Descripcion { get; set; }
        public int IdMoneda { get; set; }
        public string CodMoneda { get; set; }
        public string CodMonedaLocal { get; set; }
        public string CodMonedaInter { get; set; }
        public string CodMonedaNormativa { get; set; }
        public string DescripcionMoneda { get; set; }
        public string SimboloMonedaLocal { get; set; }
        public string SimboloMonedaInter { get; set; }
        public decimal MonCredLocal { get; set; }
        public decimal MonCredInter { get; set; }
        public decimal MonDebLocal { get; set; }
        public decimal MonDebInter { get; set; }
        public decimal MonMovimiento { get; set; }
        public decimal SignoMonMovimiento { get; set; }
        public decimal? SignoMonMovimientoLocal { get; set; }
        public decimal? SignoMonMovimientoInter { get; set; }
        public decimal? MonMovimientoLocal { get; set; }
        public decimal? MonMovimientoInter { get; set; }
        public int IdDebCredito { get; set; }
        public int IdPais { get; set; }
        public string DescripcionPais { get; set; }
        public int IdTipoMovimiento { get; set; }
        public string Establecimiento { get; set; }
        public DateTime HoraTransaccion { get; set; }
        public int? CodCategoriaMcc { get; set; }
        public string CategoriaMcc { get; set; }
        public bool EsHistorico { get; set; }
        public string NombreImpreso { get; set; }
        public string DynamicJson { get; set; }
        public string DynamicJsonTarjeta { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public int IdEditado { get; set; }
        public string NumIdentificacion { get; set; }
        public string DebitoCredito { get; set; }
        public bool SoloConsulta { get; set; }
        public string GeoCamposDinamicos { get; set; }
        public string CodEditado { get; set; }
        public string MesMovimiento { get; set; }
        public string AnoMovimiento { get; set; }
        public int IdPersona { get; set; }
        public int IdLiquidado { get; set; }
        public int? IdGestion { get; set; }
    }

    public class PocCuenta
    {
        public int IdCuenta { get; set; }
        public int? IdCuentaMadre { get; set; }
        public int IdEsCuentaMadre { get; set; }
        public string NumCuenta { get; set; }
        public int IdTipoTarjeta { get; set; }
        public DateTime FechaVctoLineaCredito { get; set; }
        public DateTime FecApertura { get; set; }
        public DateTime? FecActivacion { get; set; }
        public DateTime? FecBloqueo { get; set; }
        public int? IdUsuarioBloqueo { get; set; }
        public int? IdUsuarioActivacion { get; set; }
        public int IdUsuarioApertura { get; set; }
        public decimal SaldoLocal { get; set; }
        public int IdEstadoCta { get; set; }
        public int? IdEnvioEstCuenta { get; set; }
        public string CuentaClienteLocal { get; set; }
        public decimal? PorcAvanceEfectivo { get; set; }
        public decimal SaldoInternacional { get; set; }
        public decimal MillasAcumuladas { get; set; }
        public int? DiasAtrasoLocal { get; set; }
        public int? IdDiaCorte { get; set; }
        public int? DiasAtrasoInter { get; set; }
        public decimal? PagoMinLocal { get; set; }
        public decimal? PagoMinInter { get; set; }
        public decimal? PagoContadoLocal { get; set; }
        public decimal? PagoContadoInter { get; set; }
        public DateTime? FecUltPagoLocal { get; set; }
        public DateTime? FecUltPagoInter { get; set; }
        public string CuentaClienteInter { get; set; }
        public decimal TasaInteresLocal { get; set; }
        public decimal TasaInteresInter { get; set; }
        public decimal TasaIntMoraLocal { get; set; }
        public decimal TasaIntMoraInter { get; set; }
        public decimal? InteresMoratorioLocal { get; set; }
        public decimal? InteresMoratorioInter { get; set; }
        public decimal TasaIntCJLocal { get; set; }
        public decimal? TasaIntCJInter { get; set; }
        public int IdCalificacion { get; set; }
        public decimal PlazoMeses { get; set; }
        public decimal MonDisponibleLocal { get; set; }
        public decimal MonDisponibleInter { get; set; }
        public decimal? MonCargosBonifLocal { get; set; }
        public decimal? MonCargosBonifInter { get; set; }
        public decimal? MonCreditosLocal { get; set; }
        public decimal? MonCreditosInter { get; set; }
        public decimal? SaldoTotalCorteLocal { get; set; }
        public decimal? SaldoTotalCorteInter { get; set; }
        public int? CantImpVencidosLocal { get; set; }
        public int? CantImpVencidosInter { get; set; }
        public decimal? MonImpVencidosLocal { get; set; }
        public decimal? DebitosTransitoLocal { get; set; }
        public decimal? DebitosTransitoInter { get; set; }
        public decimal? CreditosTransitoLocal { get; set; }
        public decimal? CreditosTransitoInter { get; set; }
        public decimal? LimiteExtraFinLocal { get; set; }
        public decimal? LimiteExtraFinInter { get; set; }
        public decimal? SaldoExtraFinLocal { get; set; }
        public decimal? SaldoExtraFinInter { get; set; }
        public decimal? MonDispExtraFinLocal { get; set; }
        public decimal? MonDispExtraFinInter { get; set; }
        public decimal? MonImpVencidosInter { get; set; }
        public decimal LimiteCreditoLocal { get; set; }
        public DateTime? HoraUltActualizacion { get; set; }
        public decimal LimiteCreditoInter { get; set; }
        public DateTime? FecUltCorte { get; set; }
        public DateTime? FecPagoContado { get; set; }
        public DateTime? FecPagoMinimo { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }
        public int? IdPersonaTitular { get; set; }
        public decimal? DispAvanceEfectivoInter { get; set; }
        public decimal? DispAvanceEfectivoLocal { get; set; }
        public int? IdDirEnvioEstCuenta { get; set; }
        public string DireccionEnvio { get; set; }
        public int? IdCantonEnvio { get; set; }
        public int? IdRestringeDistLimite { get; set; }
        public int IdCompania { get; set; }
    }

    public class PocTarjeta
    {
        public int IdTarjeta { get; set; }
        public int IdCuenta { get; set; }
        public int IdTipoPlastico { get; set; }
        public string NombreImpreso { get; set; }
        public int IdRelacion { get; set; }
        public string NumTarjeta { get; set; }
        public DateTime FecApertura { get; set; }
        public int IdPersona { get; set; }
        public int? IdEstadoTarjeta { get; set; }
        public DateTime FecVencimiento { get; set; }
        public DateTime? FecActivacion { get; set; }
        public int? IdUsuarioActiva { get; set; }
        public DateTime? FecEntrega { get; set; }
        public int? IdUsuarioEntrega { get; set; }
        public DateTime? FecRenovacion { get; set; }
        public int? IdUsuarioRenueva { get; set; }
        public DateTime? FecReposicion { get; set; }
        public int? IdUsuarioRepone { get; set; }
        public int? IdTarjetaAnterior { get; set; }
        public int? IdTipoReposicion { get; set; }
        public decimal? DisponibleLocalTarjeta { get; set; }
        public decimal? DisponibleInterTarjeta { get; set; }
        public decimal? DispAdelantoLocalTarjeta { get; set; }
        public decimal? DispAdelantoInterTarjeta { get; set; }
        public DateTime? HoraUltActualizacion { get; set; }
        public int? IdTipoPoliza { get; set; }
        public string NumPoliza { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }
        public decimal? LimiteCreditoInter { get; set; }
        public decimal? LimiteCreditoLocal { get; set; }
        public int? IdGrupoEmpresa { get; set; }
        public int? IdEmpresa { get; set; }
        public decimal? SaldoTarjetaLocal { get; set; }
        public decimal? SaldoTarjetaInter { get; set; }
        public string CamposDinamicos { get; set; }
    }

    public class PocTipoTarjeta
    {
        public int IdTipoTarjeta { get; set; }
        public int IdClase { get; set; }
        public int IdAlcance { get; set; }
        public string Descripcion { get; set; }
        public int Bin { get; set; }
        public int? IdEntidad { get; set; }
        public int? IdMarca { get; set; }
        public int? IdMonedaLimite { get; set; }
        public int? MonLimiteMinimo { get; set; }
        public int? MonLimiteMaximo { get; set; }
        public int? MonIngresoMínimo { get; set; }
        public int? PlazoVctoLineaCredito { get; set; }
        public decimal TasaInteresLocal { get; set; }
        public decimal TasaIntMoraLocal { get; set; }
        public decimal TasaIntMoraInternacional { get; set; }
        public decimal TasaIntCJInternacional { get; set; }
        public decimal TasaIntCJLocal { get; set; }
        public decimal TasaIntraFinLocal { get; set; }
        public decimal TasaIntraFinInternacional { get; set; }
        public int DiasGraciaMora { get; set; }
        public decimal MonCargoAdminAtraso { get; set; }
        public int IdExcCargoAdmin { get; set; }
        public decimal MonMaxReversionCargo { get; set; }
        public decimal PorcSobregiroAutorizado { get; set; }
        public int MinDiasSobregiro { get; set; }
        public int? PlazoIntraFinLocal { get; set; }
        public int? PlazoIntraFinInternacional { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }
    }

    public class PocTarjetaTitular
    {
        public int IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public string TarjetaHabiente { get; set; }
        public int IdPersona { get; set; }
        public int? IdEmpresa { get; set; }
    }

    public class PocConsultaCuentas
    {
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdPersona { get; set; }
        public string NumCuenta { get; set; }
        public int IdEstado { get; set; }
        public string DescripcionEstado { get; set; }
    }

    public class PocLimitesTarjeta
    {
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public bool EsAdicional { get; set; }
        public int LimiteMinimo { get; set; }
        public decimal LimiteCuenta { get; set; }
        public decimal LimiteTarjeta { get; set; }
        public decimal SaldoLocalTc { get; set; }
        public decimal SaldoInterTc { get; set; }
        public decimal SaldoTotalTc { get; set; }
        public decimal DisponibleLocalTc { get; set; }
        public decimal DisponibleInterTc { get; set; }
        public decimal? LimiteGrupo { get; set; }
        public decimal? LimiteMinimoTarjeta { get; set; }
        public decimal? LimiteAsignadoGrupo { get; set; }
        public decimal? LimiteAsignadoCuenta { get; set; }
        public decimal? LimitePorAsignarGrupo { get; set; }
        public decimal? LimitePorAsignarCuenta { get; set; }
        public decimal? LimitePorAsignarSeparado { get; set; }
        public decimal? MonLimiteActual { get; set; }
    }

    public class PocPeriodoFecCorte
    {
        public int CantidadMesesCorte { get; set; }
        public DateTime FecCorte { get; set; }
    }

    public class PocGeneraEstCuenta
    {
        public int Anno { get; set; }
        public int Mes { get; set; }
        public int? IdCuenta { get; set; }
        public string Formato { get; set; }
    }

    public class PocDetalleConsumo
    {
        public int TipoConsumo { get; set; }
        public DateTime FecDesde { get; set; }
        public DateTime FecHasta { get; set; }
        public int IdEntidad { get; set; }
    }

    public class PocPrivilegiosCuenta
    {
        public int IdCuenta { get; set; }
        public int? IdUsuario { get; set; }
        public string CodUsuario { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public string NombreCompleto { get; set; }
        public string NumCuenta { get; set; }
        public decimal LimiteCuenta { get; set; }
        public string TipoTarjeta { get; set; }
        public string DescripcionEstado { get; set; }
        public string Texto { get; set; }
    }

    public class PocAdmAgrupacion
    {
        public int IdUsuario { get; set; }
        public bool Agrupada { get; set; }
    }

    public class PocConsultaCuentaTarjeta : PocCuentaUsuario
    {
        public int IdRelacion { get; set; }
        public string Relacion { get; set; }
        public int? IdEstadoTarjeta { get; set; }
        public string DesEstadoTarjeta { get; set; }
        public bool EsVip { get; set; }
        public int IdPersona { get; set; }
        public decimal? LimiteTarjeta { get; set; }
        public bool RestriccionesDesactivadas { get; set; }
        public int TiempoDesactRestricciones { get; set; }
        public int TiempoRemanenteDesact { get; set; }
    }

    public class PocExtraFinanciamiento
    {
        public int IdExtraFinanciamiento { get; set; }
        public int NumExtraFinanciamiento { get; set; }
        public int IdCuenta { get; set; }
        public int? Consecutivo { get; set; }
        public int? CodigoMovimiento { get; set; }
        public decimal? TasaDeInteres { get; set; }
        public int? PlazoEnMeses { get; set; }
        public DateTime? FechaAutorizacion { get; set; }

        public int IdMoneda { get; set; }
        public decimal SaldoInicial { get; set; }
        public decimal MontoCobradoInter { get; set; }
        public decimal MontoCobradoLocal { get; set; }
        public decimal Debitos { get; set; }
        public decimal CuotaMensual { get; set; }
        public int CuotasCobradas { get; set; }
    }

    public class PocoInfoCuentaNot
    {
        public string NombreCompleto { get; set; }
        public int IdPersona { get; set; }
        public int IdEmpresa { get; set; }
        public int IdCuenta { get; set; }
        public string NumCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public string CardNumber { get; set; }
        public int DiasAntesFecPago { get; set; }
        public decimal PagoContadoInter { get; set; }
        public decimal PagoContadoLocal { get; set; }
        public decimal PagoMinInter { get; set; }
        public decimal PagoMinLocal { get; set; }
        public decimal LimiteCreditoInter { get; set; }
        public decimal LimiteCreditoLocal { get; set; }
        public decimal SaldoInternacional { get; set; }
        public decimal SaldoLocal { get; set; }
        public int IdEntidad { get; set; }
        public int PorcentajeConsumo { get; set; }
        public string LocalTransactionDate { get; set; }
        public string LocalTransactionTime { get; set; }
    }

    public class PocArchivoMovimientoTc
    {
        public int IdArchivo { get; set; }
        public int IdMovimiento { get; set; }
        public string TipoArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public byte[] DatosArchivo { get; set; }
        public int IdEstado { get; set; }
        public string Accion { get; set; }
    }
}

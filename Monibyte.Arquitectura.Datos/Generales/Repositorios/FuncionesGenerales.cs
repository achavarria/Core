﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Datos.Generales.Repositorios
{
    public class FuncionesGenerales : RepositorioBase, IFuncionesGenerales
    {
        ISql _unidadTbjo;
        IRepositorioMnb<GL_Parametro> _repParamGen;
        IRepositorioMnb<GL_ParametroCompania> _repParamCia;
        public FuncionesGenerales(IUnidadTbjoMonibyte unidadTbjo)
            : base(unidadTbjo)
        {
            _unidadTbjo = unidadTbjo;
            _repParamGen = GestorUnity.CargarUnidad<IRepositorioMnb<GL_Parametro>>();
            _repParamCia = GestorUnity.CargarUnidad<IRepositorioMnb<GL_ParametroCompania>>();
        }

        [DbFunction("General", "f_ObtieneTraduccion")]
        public string Traducir(string pTexto, string pContexto, int pIdContexto,
            int pIdIdioma, int pAbreviatura, int pIdIdiomaBase)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", (object)pTexto ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p2", (object)pContexto ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p3", pIdContexto));
            listaParams.Add(new SqlParameter("p4", pIdIdioma));            
            listaParams.Add(new SqlParameter("p5", pAbreviatura));
            listaParams.Add(new SqlParameter("p6", pIdIdiomaBase));
            return _unidadTbjo.ExecuteQuery<string>(@"SELECT General.f_ObtieneTraduccion
            (@p1, @p2, @p3, @p4, @p5, @p6)", listaParams.ToArray()).FirstOrDefault();
        }

        public int ObtieneSgteSecuencia(string pSecuencia)
        {
            int resultado;
            using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
            {
                resultado = _unidadTbjo.ExecuteQuery<int>
                    ("General.P_ObtieneSgteSecuencia @p1",
                    new SqlParameter("p1", pSecuencia))
                    .FirstOrDefault();
                ts.Complete();
            }
            return resultado;
        }

        public IEnumerable<Cplx_SqlLista> ObtenerListaSql(string sql)
        {
            if (string.IsNullOrEmpty(sql))
            {
                return null;
            }
            return _unidadTbjo.ExecuteQuery<Cplx_SqlLista>(sql);
        }

        public IEnumerable<Cplx_SqlLista> ObtenerListaPredefinida(int idParametro)
        {
            var param = _repParamGen.SelectById(idParametro);
            return ObtenerListaSql(param != null ? param.IdOrigen : null);
        }

        public string ObtieneParametroCompania(string idParametro, int idCompania)
        {
            var param = _repParamCia.Table
                .FirstOrDefault(x =>
                    x.IdCompania == idCompania &&
                    x.IdParametro == idParametro);
            return param != null ? param.ValParametro : string.Empty;
        }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Clientes;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public interface IAppOpEjecutivo : IAplicacionBase
    {
        void Insertar(PocEjecutivo registro);
        void Modificar(PocEjecutivo registro);
        void Eliminar(PocEjecutivo registro);
        PocEjecutivo Obtener(PocEjecutivo filtro);
        DataResult<PocEjecutivo> Listar(PocEjecutivo filtro, DataRequest request);
        List<PocEjecutivo> ObtenerEjecutivos(int? idEstado);
    }
}

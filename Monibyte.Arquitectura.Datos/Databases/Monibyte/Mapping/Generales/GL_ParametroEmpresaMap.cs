using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ParametroEmpresaMap : EntityTypeConfiguration<GL_ParametroEmpresa>
    {
        public GL_ParametroEmpresaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdParametro);

            // Properties
            this.Property(t => t.IdParametro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.Descripcion)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("GL_ParametroEmpresa", "General");
            this.Property(t => t.IdParametro).HasColumnName("IdParametro");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdTipo).HasColumnName("IdTipo");
            this.Property(t => t.IdOrdenadoPor).HasColumnName("IdOrdenadoPor");
            this.Property(t => t.Ascendente).HasColumnName("Ascendente");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdParametroInterno).HasColumnName("IdParametroInterno");
            this.Property(t => t.IdDependeDe).HasColumnName("IdDependeDe");
        }
    }
}

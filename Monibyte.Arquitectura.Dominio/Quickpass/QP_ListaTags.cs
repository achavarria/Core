using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Quickpass
{
    public partial class QP_ListaTags : EntidadBase
    {
        public int IdSecuencia { get; set; }
        public int IdQuickpass { get; set; }
        public Nullable<System.DateTime> FechaGeneracion { get; set; }
        public Nullable<int> IdTipoLista { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

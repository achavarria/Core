using Monibyte.Arquitectura.Dominio.Logs;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Logs
{
    public class LG_LogEventoMap : EntityTypeConfiguration<LG_LogEvento>
    {
        public LG_LogEventoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdEvento);

            // Properties
            this.Property(t => t.IdEvento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodUsuario)
                .HasMaxLength(50);

            this.Property(t => t.DireccionIpv4)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.CodigoSesion)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Detalle)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("LG_LogEvento", "Logs");
            this.Property(t => t.IdEvento).HasColumnName("IdEvento");
            this.Property(t => t.IdTipoEvento).HasColumnName("IdTipoEvento");
            this.Property(t => t.FecEvento).HasColumnName("FecEvento");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.IdSistema).HasColumnName("IdSistema");
            this.Property(t => t.FecInicio).HasColumnName("FecInicio");
            this.Property(t => t.FecFinal).HasColumnName("FecFinal");
            this.Property(t => t.HoraEvento).HasColumnName("HoraEvento");
            this.Property(t => t.DireccionIpv4).HasColumnName("DireccionIpv4");
            this.Property(t => t.CodigoSesion).HasColumnName("CodigoSesion");
            this.Property(t => t.Detalle).HasColumnName("Detalle");
        }
    }
}

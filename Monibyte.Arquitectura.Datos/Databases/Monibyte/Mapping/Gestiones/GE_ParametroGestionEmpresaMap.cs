using Monibyte.Arquitectura.Dominio.Gestiones;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Gestiones
{
    public class GE_ParametroGestionEmpresaMap : EntityTypeConfiguration<GE_ParametroGestionEmpresa>
    {
        public GE_ParametroGestionEmpresaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdParametroEmpresa, t.IdTipoGestion });

            // Properties
            this.Property(t => t.IdParametroEmpresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdTipoGestion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ValorDefecto)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("GE_ParametroGestionEmpresa", "Gestion");
            this.Property(t => t.IdParametroEmpresa).HasColumnName("IdParametroEmpresa");
            this.Property(t => t.IdTipoGestion).HasColumnName("IdTipoGestion");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.ValorDefecto).HasColumnName("ValorDefecto");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

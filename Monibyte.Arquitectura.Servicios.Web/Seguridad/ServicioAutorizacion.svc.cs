﻿using Monibyte.Arquitectura.Aplicacion.Configuracion;
using Monibyte.Arquitectura.Aplicacion.Seguridad;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;
using System.Collections.Generic;
using Monibyte.Arquitectura.Poco.Seguridad;

namespace Monibyte.Arquitectura.Servicios.Web.Seguridad
{
    public class ServicioAutorizacion : ServicioUnity, IServicioAutorizacion
    {
        private IAppConfiguracionMisc _appConfMisc;
        private IAppAutorizacion _appAutorizacion;
        public ServicioAutorizacion(IAppConfiguracionMisc appConfMisc,
            IAppAutorizacion appAutorizacion)
        {
            _appConfMisc = appConfMisc;
            _appAutorizacion = appAutorizacion;
        }

        public Transporte ObtenerAutorizaciones(Transporte transporte)
        {
            var resultado = _appConfMisc.ObtenerAutorizaciones();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerAutorizacionesCount(Transporte transporte)
        {
            var resultado = _appConfMisc.ObtenerAutorizacionesCount();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtieneAutorizacionPendiente(Transporte transporte)
        {
            var tipoAutorizacion = transporte.GetValor<int?>("tipoAutorizacion");
            var numReferencia = transporte.GetValor<string>("numReferencia");
            var resultado = _appAutorizacion.ObtieneAutorizacionPendiente(tipoAutorizacion, numReferencia);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte RechazarAutorizacion(Transporte transporte)
        {
            var tipoAutorizacion = transporte.GetValor<int>("tipoAutorizacion");
            var numReferencia = transporte.GetValor<string>("numReferencia");
            var infoAutorizacion = transporte.GetDatos<PocInfoAutorizacion>();
            var resultado = _appAutorizacion.RechazarAutorizacion(tipoAutorizacion, numReferencia, infoAutorizacion);
            return transporte;
        }

        public Transporte AnularAutorizacion(Transporte transporte)
        {
            var tipoAutorizacion = transporte.GetValor<int>("tipoAutorizacion");
            var numReferencia = transporte.GetValor<string>("numReferencia");
            var infoAutorizacion = transporte.GetDatos<PocInfoAutorizacion>();
            var resultado = _appAutorizacion.AnularAutorizacion(tipoAutorizacion, numReferencia, infoAutorizacion);
            return transporte;
        } 

        public bool ExisteDependenciaPendiente(int idTipoAutorizacion, string numReferencia)
        {
           return _appAutorizacion.ExisteDependenciaPendiente(idTipoAutorizacion, numReferencia);
        }

        public bool EsNuevoGrupo(string numReferencia)
        {
            return _appAutorizacion.EsNuevoGrupo(numReferencia);
        }

        public bool PermiteEditar(string numReferencia)
        {
            return _appAutorizacion.PermiteEditar(numReferencia);
        }

        public string ObtieneObervacion(int idTipoAutorizacion, string numReferencia)
        {
            return _appAutorizacion.ObtieneObervacion(idTipoAutorizacion, numReferencia);
        }
    }
}

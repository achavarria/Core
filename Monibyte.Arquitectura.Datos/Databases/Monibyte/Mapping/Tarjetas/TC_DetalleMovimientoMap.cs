using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_DetalleMovimientoMap : EntityTypeConfiguration<TC_DetalleMovimiento>
    {
        public TC_DetalleMovimientoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMovimiento);

            // Properties
            this.Property(t => t.IdMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumOrdenCompra)
                .HasMaxLength(30);

            this.Property(t => t.NumFactura)
                .HasMaxLength(30);

            this.Property(t => t.NumReferencia)
                .HasMaxLength(20);

            this.Property(t => t.Observaciones)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("TC_DetalleMovimiento", "TCredito");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.NumOrdenCompra).HasColumnName("NumOrdenCompra");
            this.Property(t => t.NumFactura).HasColumnName("NumFactura");
            this.Property(t => t.NumReferencia).HasColumnName("NumReferencia");
            this.Property(t => t.Observaciones).HasColumnName("Observaciones");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

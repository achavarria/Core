using System;

namespace Monibyte.Arquitectura.Dominio.Seguridad
{
    public partial class Cplx_SC_Accion : EntidadBase
    {
        public int IdAccion { get; set; }
        public string Descripcion { get; set; }
        public string Controlador { get; set; }
        public string Area { get; set; }
        public int IdEstado { get; set; }
    }
}

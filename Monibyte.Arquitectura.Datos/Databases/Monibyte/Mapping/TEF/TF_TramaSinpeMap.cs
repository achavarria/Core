﻿using System.ComponentModel.DataAnnotations.Schema;
using Monibyte.Arquitectura.Dominio.TEF;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.TEF
{
    public class TF_TramaSinpeMap : EntityTypeConfiguration<TF_TramaSinpe>
    {
        public TF_TramaSinpeMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTramaSinpe);

            // Properties
            this.Property(t => t.IdTramaSinpe)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Metodo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.TramaXML)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("TF_TramaSinpe", "TEF");
            this.Property(t => t.IdTramaSinpe).HasColumnName("IdTramaSinpe");
            this.Property(t => t.Metodo).HasColumnName("Metodo");
            this.Property(t => t.TramaXML).HasColumnName("TramaXML");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

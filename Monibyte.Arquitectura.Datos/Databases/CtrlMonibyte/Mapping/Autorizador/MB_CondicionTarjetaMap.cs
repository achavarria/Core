using Monibyte.Arquitectura.Dominio.Autorizador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador
{
    public class MB_CondicionTarjetaMap : EntityTypeConfiguration<MB_CondicionTarjeta>
    {
        public MB_CondicionTarjetaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCondicionTarjeta);

            // Properties
            this.Property(t => t.IdCondicionTarjeta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumTarjeta)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("MB_CondicionTarjeta", "Mnb");
            this.Property(t => t.IdCondicionTarjeta).HasColumnName("IdCondicionTarjeta");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.SinRestriccion).HasColumnName("SinRestriccion");
            this.Property(t => t.EsVip).HasColumnName("EsVip");
            this.Property(t => t.MonLimite).HasColumnName("MonLimite");
            this.Property(t => t.ConsumeColones).HasColumnName("ConsumeColones");
            this.Property(t => t.ConsumeDolares).HasColumnName("ConsumeDolares");
            this.Property(t => t.ConsumeLocal).HasColumnName("ConsumeLocal");
            this.Property(t => t.ConsumeInternacional).HasColumnName("ConsumeInternacional");
            this.Property(t => t.ConsumeInternet).HasColumnName("ConsumeInternet");
            this.Property(t => t.PermiteAvanceEfectivo).HasColumnName("PermiteAvanceEfectivo");
            this.Property(t => t.RestringeMcc).HasColumnName("RestringeMcc");
            this.Property(t => t.RestringeHorario).HasColumnName("RestringeHorario");
            this.Property(t => t.RestringeRegion).HasColumnName("RestringeRegion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.SaldoCantTranDiaria).HasColumnName("SaldoCantTranDiaria");
            this.Property(t => t.SaldoMonTranDiaria).HasColumnName("SaldoMonTranDiaria");
            this.Property(t => t.SaldoCantTranSemanal).HasColumnName("SaldoCantTranSemanal");
            this.Property(t => t.SaldoMonTranSemanal).HasColumnName("SaldoMonTranSemanal");
            this.Property(t => t.SaldoCantTranMensual).HasColumnName("SaldoCantTranMensual");
            this.Property(t => t.SaldoMonTranMensual).HasColumnName("SaldoMonTranMensual");
            this.Property(t => t.SaldoCantAvanceEfecDia).HasColumnName("SaldoCantAvanceEfecDia");
            this.Property(t => t.SaldoMonAvanceEfecDia).HasColumnName("SaldoMonAvanceEfecDia");
            this.Property(t => t.SaldoCantTranCuasiDia).HasColumnName("SaldoCantTranCuasiDia");
            this.Property(t => t.SaldoMonTranCuasiDia).HasColumnName("SaldoMonTranCuasiDia");
            this.Property(t => t.IdFactorInterno).HasColumnName("IdFactorInterno");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
        }
    }
}

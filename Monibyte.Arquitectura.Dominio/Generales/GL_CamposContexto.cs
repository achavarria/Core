using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_CamposContexto : EntidadBase
    {
        public int IdCampo { get; set; }
        public int IdContexto { get; set; }
        public string Campo { get; set; }
        public int IdTipo { get; set; }
        public string Descripcion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public string SQLOrigen { get; set; }
        public string CampoRelacionado { get; set; }
        public string Referencia1 { get; set; }
        public string Referencia2 { get; set; }
        public string Referencia3 { get; set; }
        public string Referencia4 { get; set; }
        public string Referencia5 { get; set; }        
    }
}

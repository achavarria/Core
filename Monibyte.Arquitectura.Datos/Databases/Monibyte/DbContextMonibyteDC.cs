﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Documentacion;
using Monibyte.Arquitectura.Dominio.Documentacion;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<DC_Ayuda> DC_Ayuda { get; set; }

        void CreateContextModelDC(DbModelBuilder modelBuilder) 
        {
            modelBuilder.Configurations.Add(new DC_AyudaMap());
        }
    }
}

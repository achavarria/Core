﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class TipoUsuario : EntidadBase
    {
        public bool MultiEntidad { get; set; }
        public bool PorUnidad { get; set; }
        public int IdSistema { get; set; }
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
        public string Lista { get; set; }
    }
}

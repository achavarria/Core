﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Clientes;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public interface IAppOpDireccion : IAplicacionBase
    {
        void Insertar(PocDireccion registro);
        void Eliminar(PocDireccion registro);
        void Modificar(PocDireccion registro);
        List<PocDireccion> ObtenerPorPersona(int idPersona);
        void EliminarPorPersona(int idPersona);
    }
}

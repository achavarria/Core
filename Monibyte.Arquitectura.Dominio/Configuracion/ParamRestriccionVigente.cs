﻿
using System;
namespace Monibyte.Arquitectura.Dominio.Configuracion
{
    public class ParamRestriccionVigente
    {
        public int IdCondicionTarjeta { get; set; }
        public int IdRestriccion { get; set; }
        public DateTime FecTransacion { get; set; }
    }
}

﻿using AutoMapper;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Notificacion;
using Monibyte.Arquitectura.Poco.Notificacion;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Integracion.Adaptaciones
{
    class PerfilNotificacion : Profile
    {
        protected override void Configure()
        {
            var pocNotificacion_nt = Mapper.CreateMap<PocConfigNotificacion, NT_Configuracion>();
            pocNotificacion_nt.ForMember(dto => dto.DetalleFiltro, opt => opt.Ignore());
            pocNotificacion_nt.ForMember(dto => dto.IdEmpresa, opt =>
                opt.MapFrom(x => x.IdEmpresa ?? InfoSesion.Info.IdEmpresa));

            pocNotificacion_nt.ForMember(dto => dto.IdAdministracionInterna, opt =>
                opt.MapFrom(x => x.AdministracionInterna ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));

            pocNotificacion_nt.ForMember(dto => dto.IdNotificaAdministradoresSms, opt => 
                opt.MapFrom(x => x.NotificaAdministradoresSms ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            pocNotificacion_nt.ForMember(dto => dto.IdNotificaAdministradoresEmail, opt =>
                opt.MapFrom(x => x.NotificaAdministradoresEmail ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            
            pocNotificacion_nt.ForMember(dto => dto.IdAplicaTarjetaHabiente, opt =>
                opt.MapFrom(x => x.AplicaTarjetaHabiente ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            pocNotificacion_nt.ForMember(dto => dto.IdNotificaTarjetaHabienteSms, opt =>
                opt.MapFrom(x => x.NotificaTarjetaHabienteSms ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            pocNotificacion_nt.ForMember(dto => dto.IdNotificaTarjetaHabienteEmail, opt =>
                opt.MapFrom(x => x.NotificaTarjetaHabienteEmail ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            
            pocNotificacion_nt.ForMember(dto => dto.IdAplicaUsuario, opt =>
                opt.MapFrom(x => x.AplicaUsuario ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            pocNotificacion_nt.ForMember(dto => dto.IdNotificaUsuarioSms, opt =>
                opt.MapFrom(x => x.NotificaUsuarioSms ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            pocNotificacion_nt.ForMember(dto => dto.IdNotificaUsuarioEmail, opt =>
                opt.MapFrom(x => x.NotificaUsuarioEmail ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));

            pocNotificacion_nt.ForMember(dto => dto.Destinatario,
                opt => opt.MapFrom(x => ToJson(x.Destinatarios)));

            var ntNotificacion_poc = Mapper.CreateMap<NT_Configuracion, PocConfigNotificacion>();
            ntNotificacion_poc.ForMember(dto => dto.IdConfiguracion, opt => opt.MapFrom(x =>
                x.IdEmpresa == null ? null : x.IdConfiguracion as int?));
            ntNotificacion_poc.ForMember(dto => dto.DetalleFiltro, opt => opt.Ignore());

            ntNotificacion_poc.ForMember(dto => dto.AdministracionInterna, opt =>
                opt.MapFrom(x => x.IdAdministracionInterna == (int)EnumIdSiNo.Si));

            ntNotificacion_poc.ForMember(dto => dto.NotificaAdministradoresSms, opt => 
                opt.MapFrom(x => x.IdNotificaAdministradoresSms == (int)EnumIdSiNo.Si));
            ntNotificacion_poc.ForMember(dto => dto.NotificaAdministradoresEmail, opt => 
                opt.MapFrom(x => x.IdNotificaAdministradoresEmail == (int)EnumIdSiNo.Si));

            ntNotificacion_poc.ForMember(dto => dto.AplicaTarjetaHabiente, opt =>
                opt.MapFrom(x => x.IdAplicaTarjetaHabiente == (int)EnumIdSiNo.Si));
            ntNotificacion_poc.ForMember(dto => dto.NotificaTarjetaHabienteSms, opt => 
                opt.MapFrom(x => x.IdNotificaTarjetaHabienteSms == (int)EnumIdSiNo.Si));
            ntNotificacion_poc.ForMember(dto => dto.NotificaTarjetaHabienteEmail, opt => 
                opt.MapFrom(x => x.IdNotificaTarjetaHabienteEmail == (int)EnumIdSiNo.Si));

            ntNotificacion_poc.ForMember(dto => dto.AplicaUsuario, opt =>
                opt.MapFrom(x => x.IdAplicaUsuario == (int)EnumIdSiNo.Si));
            ntNotificacion_poc.ForMember(dto => dto.NotificaUsuarioSms, opt => 
                opt.MapFrom(x => x.IdNotificaUsuarioSms == (int)EnumIdSiNo.Si));
            ntNotificacion_poc.ForMember(dto => dto.NotificaUsuarioEmail, opt => 
                opt.MapFrom(x => x.IdNotificaUsuarioEmail == (int)EnumIdSiNo.Si));

            ntNotificacion_poc.ForMember(dto => dto.Destinatarios, opt =>
                opt.MapFrom(x => StringExt.FromJson<List<PocDestinatario>>(x.Destinatario)));
        }

        private string ToJson<T>(T obj)
        {
            var result = StringExt.ToJson<T>(obj);
            if (string.IsNullOrEmpty(result))
            {
                return null;
            }
            return result;
        }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Configuracion;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public interface IConsultasTarjeta : IAplicacionBase
    {
        PocDetalleCtaTarjeta ObtenerDetalleCtaTarjeta(PocTarjetaCuentaUsuario entrada,
            bool consolida = false, bool consolidaMq = false);
        PocDetalleCtaTarjeta ConsultarDetalleTarjetaMQ(int idCuenta,
            string numTarjeta = "", bool adicional = false);
        PocMillas ConsultarMillas(string numCuenta);
        IEnumerable<PocConsultaTarjeta> ObtenerTarjetasCliente(int idCliente, int? idCuenta);
        IEnumerable<PocTarjetaCuentaUsuario> ObtenerTarjetasCuentaUsuario(int? idCuenta,
            bool incluirHijas, int[] filtroEstado, int[] filtroEstadoExc, bool ocultarNoEditables,
            bool incluirOperativas = false);
        DataResult<PocTarjetaCuentaUsuario> ObtenerTarjetasCuentaUsuarioDataResult(DataRequest request,
            int idCuenta, bool incluirHijas, int[] filtroEstado, int[] filtroEstadoExc,
            bool incluirOperativas = false);
        PocTarjetaCuentaUsuario ObtenerCuentaUsuario(string numCuenta);
        PocTarjetaCuentaUsuario ObtenerTarjetaUsuario(string numTarjeta);
        IEnumerable<PocTarjetaCuentaUsuario> ObtenerPersonasTarjeta();
        IEnumerable<PocTipoTarjeta> ListaTiposTarjetaCliente(int idPersona);
        int CantidadEstCuenta(int idCuenta);
        PocPeriodoFecCorte CantidadEstCuentaGlobal();
        PocCuenta ObtenerCuenta(int idCuenta, string numCuenta = null);
        PocTipoTarjeta ObtenerTipoTarjeta(int idTipoTarjeta);
        PocTarjeta ObtenerTarjeta(int? idTarjeta, string numTarjeta = null);
        PocTarjetaTitular ObtenerTarjetaTitular(int idCuenta);
        IEnumerable<PocConsultaCuentas> ObtenerCuentasCliente(int idCliente);
        PocLimitesTarjeta ObtenerLimitesTarjeta(string numTarjeta);
        DataResult<PocTarjetasAdministrativo> ObtenerTarjetas(
            PocConsultaTarjetasAdmin consulta, DataRequest request);
        IEnumerable<PocTarjetasAdministrativo> ObtenerTarjetas(
              PocConsultaTarjetasAdmin consulta);
        PocDetalleCtaTarjeta ObtenerDetalleCtaTarjeta(int idTarjeta);
        IEnumerable<PocConsultaCuentaTarjeta> ObtenerCuentasUsuarioPermitidas(int[] filtroEstado,
             int? IdTitular, bool soloMadres = true, bool incluirOperativas = false);
        PocLimitesTarjeta ObtenerLimitesTarjeta(int idTarjeta);
        PocDetalleCtaTarjeta ConsultarDetTarjEspecifica(int idCuenta, int idTarjeta);
        List<PocExtraFinanciamiento> ConsultarExtraFinancUsuario(int IdUsuario, int IdCuenta);
        List<PocConsultaCuentasPersonas> ObtenerCuentasPersona(int idPersona, string numTarjeta = null, string numCuenta = null);
    }
}

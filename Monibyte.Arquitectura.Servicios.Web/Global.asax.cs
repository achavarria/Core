﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Integracion;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;
using System.Web;

namespace Monibyte.Arquitectura.Servicios.Web
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            SessionReader.SetReader(new AppSessionReader());
            RestClient.SetSessionWriter(new AppSessionWriter());
            GestorUnity.Contenedor = ContenedorUnity.Contenedor;
        }

        protected void Application_PostRequestHandlerExecute(object sender, EventArgs e)
        {
            if (HttpContext.Current != null &&
                HttpContext.Current.Session != null)
            {
                Session.Clear();
                Session.Abandon();
            }
        }
    }
}
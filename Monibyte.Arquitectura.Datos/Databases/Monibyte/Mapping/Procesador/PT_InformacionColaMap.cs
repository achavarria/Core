using Monibyte.Arquitectura.Dominio.Procesador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Procesador
{
    public class PT_InformacionColaMap : EntityTypeConfiguration<PT_InformacionCola>
    {
        public PT_InformacionColaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCola);

            // Properties
            this.Property(t => t.IdCola)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TipoReferencia)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.CodReferencia)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.TipoMensajeSolicitud)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.TramaSolicitud)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.IdMensajeSolicitud)
                .IsRequired()
                .HasMaxLength(24);

            this.Property(t => t.TramaRespuesta)
                .IsRequired()
                .HasMaxLength(2000);

            this.Property(t => t.IdMensajeRespuesta)
                .IsRequired()
                .HasMaxLength(24);

            // Table & Column Mappings
            this.ToTable("PT_InformacionCola", "ProcesadorTC");
            this.Property(t => t.IdCola).HasColumnName("IdCola");
            this.Property(t => t.TipoReferencia).HasColumnName("TipoReferencia");
            this.Property(t => t.CodReferencia).HasColumnName("CodReferencia");
            this.Property(t => t.FechaSolicitud).HasColumnName("FechaSolicitud");
            this.Property(t => t.TipoMensajeSolicitud).HasColumnName("TipoMensajeSolicitud");
            this.Property(t => t.TramaSolicitud).HasColumnName("TramaSolicitud");
            this.Property(t => t.IdMensajeSolicitud).HasColumnName("IdMensajeSolicitud");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FechaRespuesta).HasColumnName("FechaRespuesta");
            this.Property(t => t.TramaRespuesta).HasColumnName("TramaRespuesta");
            this.Property(t => t.IdMensajeRespuesta).HasColumnName("IdMensajeRespuesta");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdTipoUsuario).HasColumnName("IdTipoUsuario");
        }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco.Cargos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public class AppCargosGeolocalizacion : AplicacionBase, IAppCargosGeolocalizacion
    {
        private IRepositorioMnb<CA_CargosGeo> _repCargosGeo;
        private IRepositorioMnb<CA_CargosPorPersona> _repCargosPersona;
        private IFuncionesGenerales _funcionesGenerales;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<CL_Ejecutivo> _repEjecutivo;
        private IRepositorioCatalogo _repLista;
        private IRepositorioCuenta _repCuenta;

        public AppCargosGeolocalizacion(
               IRepositorioMnb<CA_CargosGeo> repCargosGeo,
               IRepositorioMnb<CA_CargosPorPersona> repCargosPersona,
               IFuncionesGenerales funcionesGenerales,
               IRepositorioMnb<CL_Persona> repPersona,
               IRepositorioMnb<CL_Ejecutivo> repEjecutivo,
               IRepositorioCatalogo repLista,
               IRepositorioCuenta repCuenta)
        {
            _repCargosGeo = repCargosGeo;
            _repCargosPersona = repCargosPersona;
            _funcionesGenerales = funcionesGenerales;
            _repPersona = repPersona;
            _repEjecutivo = repEjecutivo;
            _repLista = repLista;
            _repCuenta = repCuenta;
        }

        public void GuardarCargosGeo(PocRegistrarPlacas entrada, List<PocCargosPlaca> modeloBase)
        {
            _repPersona.UpdateOn(x => x.IdPersona == entrada.IdEmpresa, z => new CL_Persona
            {
                IdEjecutivo = entrada.IdEjecutivo
            });
            if (entrada.Registros != null && entrada.Registros.Any(x => x.IdCargo > 0))
            {
                var gruposTipoCargo = entrada.Registros.GroupBy(x => x.IdCargo).ToList();
                var cuentaxCargo = entrada.Registros.Where(x => x.IdCargo > 0)
                    .Select(x => new { x.IdCargo, x.IdCuenta }).ToList();
                using (var ts = new TransactionScope())
                {
                    gruposTipoCargo.ForEach(grupoTipoCargo =>
                    {
                        var _tipoCargo = grupoTipoCargo.First();
                        var dataCargo = cuentaxCargo.FirstOrDefault
                            (x => x.IdCuenta == _tipoCargo.IdCuenta);

                        if (dataCargo == null)
                        {
                            var idNuevoCargo = AgregarCargo(entrada, _tipoCargo.IdCuenta);
                            dataCargo = new { IdCargo = idNuevoCargo, IdCuenta = _tipoCargo.IdCuenta };
                            cuentaxCargo.Add(dataCargo);
                        }

                        grupoTipoCargo.ToList().ForEach(item =>
                        {
                            if (item.IdCargo > 0)
                            {
                                var cargoAnterior = modeloBase.FirstOrDefault
                                    (x => x.IdRegistro == item.IdRegistro);
                                if (cargoAnterior != null)
                                {
                                    ActualizarCargosGeo(item, cargoAnterior);
                                    modeloBase.Remove(cargoAnterior);
                                }
                            }
                            else
                            {
                                var cargoGeo = item.ProyectarComo<CA_CargosGeo>();
                                cargoGeo.IdCargo = dataCargo.IdCargo;
                                cargoGeo.CargoActual = item.PaqueteBase +
                                    item.SensorCombustible + item.BotonPanico;
                                cargoGeo.FecUltActividad = DateTime.Now;
                                cargoGeo.FecUltAplicacion = DateTime.Now;
                                cargoGeo.Sensor = item.SensorCombustible;
                                _repCargosGeo.Insert(cargoGeo);
                            }
                        });
                    });
                    _repCargosGeo.UnidadTbjo.Save();
                    ts.Complete();
                }
                modeloBase.ForEach(item =>
                {
                    _repCargosGeo.DeleteOn(x => x.IdRegistro == item.IdRegistro);
                });
                cuentaxCargo.ForEach(item =>
                {
                    if (!_repCargosGeo.Table.Any(x => x.IdCargo == item.IdCargo))
                    {
                        _repCargosPersona.DeleteOn(x => x.IdCargo == item.IdCargo);
                    }
                });
            }
            else if (entrada.Registros != null && entrada.Registros.Any(x => x.IdCargo == 0))
            {
                var idsCuenta = entrada.Registros.Select(x => x.IdCuenta).Distinct().ToList();
                using (var ts = new TransactionScope())
                {
                    idsCuenta.ForEach(idCuenta =>
                    {
                        var idNuevoCargo = AgregarCargo(entrada, idCuenta);
                        var data = entrada.Registros.Where(x => x.IdCuenta == idCuenta).ToList();
                        data.ForEach(placa =>
                        {
                            var cargoGeo = placa.ProyectarComo<CA_CargosGeo>();
                            cargoGeo.IdCuenta = idCuenta;
                            cargoGeo.IdCargo = idNuevoCargo;
                            cargoGeo.CargoActual = placa.PaqueteBase +
                                placa.SensorCombustible + placa.BotonPanico;
                            cargoGeo.FecUltActividad = DateTime.Now;
                            cargoGeo.FecUltAplicacion = DateTime.Now;
                            cargoGeo.Sensor = placa.SensorCombustible;
                            _repCargosGeo.Insert(cargoGeo);
                        });
                    });
                    _repCargosGeo.UnidadTbjo.Save();
                    ts.Complete();
                }
            }
            else
            {
                EliminarCargosGeo(entrada.IdEmpresa);
            }
        }

        private int AgregarCargo(PocRegistrarPlacas entrada, int idCuenta)
        {
            using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
            {
                var cargosPersona = new CA_CargosPorPersona();
                cargosPersona.IdCargo = _funcionesGenerales.ObtieneSgteSecuencia("SEQ_IdCargo");
                cargosPersona.IdPersona = entrada.IdEmpresa;
                cargosPersona.IdCuentaCobro = idCuenta;
                cargosPersona.IdTipoCargo = (int)EnumTipoCargo.CargoGeo;
                cargosPersona.IdPeriodicidad = (int)EnumPeriodicidad.Mensual;
                cargosPersona.IdTipoCobro = (int)EnumTipoCobroCargo.Cantidad;
                cargosPersona.IdEstado = (int)EnumIdEstado.Activo;
                cargosPersona.FecInicio = DateTime.Now;
                _repCargosPersona.Insert(cargosPersona);
                _repCargosPersona.UnidadTbjo.Save();
                ts.Complete();

                return cargosPersona.IdCargo;
            }
        }

        public PocRegistrarPlacas ObtieneRegistroCargosGeo(int idEmpresa)
        {
            var criteria = new Criteria<CA_CargosPorPersona>();
            criteria.And(x => x.IdPersona == idEmpresa);
            criteria.And(x => x.IdTipoCargo == (int)EnumTipoCargo.CargoGeo);

            Func<int, List<PocCargosPlaca>> registros = (a) =>
            {
                var crit = new Criteria<CA_CargosGeo>();
                crit.And(x => x.IdCargo == a);
                return _repCargosGeo.SelectBy(crit)
                          .Join(_repCuenta.Table,
                              cg => cg.IdCuenta,
                              cu => cu.IdCuenta, (cg, cu) => new { cg, cu })
                          .Join(_repLista.Table,
                              itm => itm.cg.IdTipoVehiculo,
                              cta => cta.IdCatalogo, (itm, cta) => new { itm.cg, itm.cu, cta })
                          .Select(x => new PocCargosPlaca
                          {
                              IdCargo = x.cg.IdCargo,
                              IdRegistro = x.cg.IdRegistro,
                              CargoActual = (decimal)x.cg.CargoActual,
                              CargoOriginal = (decimal)x.cg.CargoOriginal,
                              Cuenta = x.cu.NumCuenta,
                              IdCuenta = x.cg.IdCuenta,
                              Placa = x.cg.Placa,
                              IdTipoVehiculo = x.cg.IdTipoVehiculo,
                              TipoVehiculo = x.cta.Descripcion,
                              Marca = x.cg.Marca,
                              Estilo = x.cg.Estilo,
                              Modelo = x.cg.Modelo,
                              PaqueteBase = (decimal)x.cg.PaqueteBase,
                              SensorCombustible = (decimal)x.cg.Sensor,
                              BotonPanico = (decimal)x.cg.BotonPanico
                          }).ToList();
            };


            var result = _repCargosPersona.SelectBy(criteria).ToList()
                        .Join(_repPersona.Table,
                            cp => cp.IdPersona,
                            p => p.IdPersona, (cp, p) => new { cp, p })
                        .GroupJoin(_repEjecutivo.Table,
                            tmp => tmp.p.IdEjecutivo,
                            ej => ej.IdEjecutivo, (tmp, ej) =>
                                new { tmp.cp, tmp.p, ej = ej.FirstOrDefault() })
                        .Join(_repLista.Table,
                            tmp => tmp.cp.IdEstado,
                            ct => ct.IdCatalogo, (tmp, ct) =>
                                new { tmp.cp, tmp.p, tmp.ej, ct })
                        .Select(item => new PocRegistrarPlacas
                        {
                            IdEjecutivo = item.ej != null ? item.ej.IdEjecutivo : 0,
                            IdEmpresa = item.cp.IdPersona,
                            IdEstado = item.cp.IdEstado.Value,
                            DescripcionEstado = item.ct.Descripcion,
                            Cliente = item.p.NombreCompleto,
                            FecInclusion = DateTime.Now,
                            Registros = registros(item.cp.IdCargo)
                        });
            if (result != null)
            {
                var cargosPlaca = new List<PocCargosPlaca>();
                foreach (var cargo in result)
                {
                    cargosPlaca.AddRange(cargo.Registros);
                }
                var resultado = result.FirstOrDefault();
                if (cargosPlaca.Any())
                {
                    resultado.Registros = cargosPlaca;
                }
                return resultado;
            }

            return null;
        }

        private void ActualizarCargosGeo(PocCargosPlaca cargoNuevo, PocCargosPlaca cargoAnterior)
        {
            var _cargo = _repCargosPersona.Table.FirstOrDefault(x =>
                x.IdCuentaCobro == cargoNuevo.IdCuenta &&
                x.IdTipoCargo == (int)EnumTipoCargo.CargoGeo);
            _repCargosGeo.UpdateOn(x => x.IdRegistro == cargoNuevo.IdRegistro,
                z => new CA_CargosGeo
                {
                    IdCargo = _cargo != null ? _cargo.IdCargo : cargoNuevo.IdCargo,
                    Placa = cargoNuevo.Placa,
                    IdCuenta = cargoNuevo.IdCuenta,
                    CargoOriginal = cargoAnterior.CargoActual,
                    CargoActual = cargoNuevo.PaqueteBase +
                        cargoNuevo.SensorCombustible + cargoNuevo.BotonPanico,
                    FecUltActividad = DateTime.Now,
                    IdTipoVehiculo = cargoNuevo.IdTipoVehiculo,
                    Marca = cargoNuevo.Marca,
                    Estilo = cargoNuevo.Estilo,
                    Modelo = cargoNuevo.Modelo,
                    PaqueteBase = cargoNuevo.PaqueteBase,
                    Sensor = cargoNuevo.SensorCombustible,
                    BotonPanico = cargoNuevo.BotonPanico
                });
        }

        public void EliminarCargosGeo(int idEmpresa)
        {
            var cargos = _repCargosPersona.Table
                .Where(x => x.IdPersona == idEmpresa &&
                       x.IdTipoCargo == (int)EnumTipoCargo.CargoGeo);
            using (var ts = new TransactionScope())
            {
                foreach (var item in cargos)
                {
                    _repCargosPersona.UpdateOn(x => x.IdCargo == item.IdCargo,
                        z => new CA_CargosPorPersona { IdEstado = (int)EnumIdEstado.Inactivo });

                    _repCargosGeo.DeleteOn(x => x.IdCargo == item.IdCargo);
                }
                _repCargosGeo.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void CambiarEstadoGeo(int idEmpresa, int idEstado)
        {
            using (var ts = new TransactionScope())
            {
                _repCargosPersona.UpdateOn(x => x.IdPersona == idEmpresa &&
                    x.IdTipoCargo == (int)EnumTipoCargo.CargoGeo,
                    z => new CA_CargosPorPersona
                    {
                        IdEstado = idEstado
                    });
                _repCargosPersona.UnidadTbjo.Save();
                ts.Complete();
            }
        }
    }
}
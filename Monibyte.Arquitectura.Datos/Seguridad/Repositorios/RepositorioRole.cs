﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Dominio.Seguridad.Repositorios;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Seguridad.Repositorios
{
    public class RepositorioRole : RepositorioMnb<SC_Role>, IRepositorioRole
    {
        public RepositorioRole(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public SC_Role ObtenerRolPorCodigo(int codRol)
        {
            var criteria = new Criteria<SC_Role>();
            criteria.And(rol => rol.IdRole == codRol);
            return this.SelectBy(criteria).First();
        }
    }
}

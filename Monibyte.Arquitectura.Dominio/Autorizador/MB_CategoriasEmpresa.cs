using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Autorizador
{
    public partial class MB_CategoriasEmpresa : EntidadBaseCtrl
    {
        public int IdCategoriaPadre { get; set; }
        public int IdCategoria { get; set; }
        public int IdEmpresa { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

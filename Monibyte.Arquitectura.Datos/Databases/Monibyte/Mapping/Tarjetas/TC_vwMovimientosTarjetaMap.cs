using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_vwMovimientosTarjetaMap : EntityTypeConfiguration<TC_vwMovimientosTarjeta>
    {
        public TC_vwMovimientosTarjetaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdMovimiento, t.IdMoneda, t.IdCuenta, t.IdTarjeta, t.IdPais, t.IdDebCredito, t.IdEditado, t.IdEstado, t.IdOrigenMovimiento, t.IdEmpresa, t.FecTransaccion, t.MonMovimiento, t.MonMovimientoLocal, t.MonMovimientoInter, t.IdTipoMovimiento, t.HoraTransaccion, t.DesMoneda, t.NumCuenta, t.NumTarjeta, t.MonCredLocal, t.MonDebLocal, t.MonCredInter, t.MonDebInter });

            // Properties
            this.Property(t => t.IdMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdMoneda)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdCuenta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdTarjeta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdPais)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdDebCredito)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdEditado)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdEstado)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdOrigenMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdEmpresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DetalleMovimiento)
                .HasMaxLength(2000);

            this.Property(t => t.Descripcion)
                .HasMaxLength(100);

            this.Property(t => t.MonMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MonMovimientoLocal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MonMovimientoInter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdTipoMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumAutorizacion)
                .HasMaxLength(20);

            this.Property(t => t.DesMoneda)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.CodAlpha2)
                .HasMaxLength(2);

            this.Property(t => t.CodMoneda)
                .HasMaxLength(5);

            this.Property(t => t.CodMonedaLocal)
                .HasMaxLength(5);

            this.Property(t => t.CodMonedaInter)
                .HasMaxLength(5);

            this.Property(t => t.CodMonedaNormativa)
                .HasMaxLength(5);

            this.Property(t => t.Simbolo)
                .HasMaxLength(5);

            this.Property(t => t.NumCuenta)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.NumTarjeta)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.NombreImpreso)
                .HasMaxLength(60);

            this.Property(t => t.DebitoCredito)
                .HasMaxLength(20);

            this.Property(t => t.MonCredLocal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MonDebLocal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MonCredInter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MonDebInter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumReferencia)
                .HasMaxLength(30);

            this.Property(t => t.CodEditado)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("TC_vwMovimientosTarjeta", "TCredito");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.IdDebCredito).HasColumnName("IdDebCredito");
            this.Property(t => t.IdEditado).HasColumnName("IdEditado");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdMcc).HasColumnName("IdMcc");
            this.Property(t => t.IdOrigenMovimiento).HasColumnName("IdOrigenMovimiento");
            this.Property(t => t.IdDiaCorte).HasColumnName("IdDiaCorte");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.DetalleMovimiento).HasColumnName("DetalleMovimiento");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.CamposDinamicos).HasColumnName("CamposDinamicos");
            this.Property(t => t.FecCorte).HasColumnName("FecCorte");
            this.Property(t => t.FecTransaccion).HasColumnName("FecTransaccion");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
            this.Property(t => t.MonMovimiento).HasColumnName("MonMovimiento");
            this.Property(t => t.MonMovimientoLocal).HasColumnName("MonMovimientoLocal");
            this.Property(t => t.MonMovimientoInter).HasColumnName("MonMovimientoInter");
            this.Property(t => t.IdTipoMovimiento).HasColumnName("IdTipoMovimiento");
            this.Property(t => t.NumAutorizacion).HasColumnName("NumAutorizacion");
            this.Property(t => t.Documento1).HasColumnName("Documento1");
            this.Property(t => t.HoraMovimiento).HasColumnName("HoraMovimiento");
            this.Property(t => t.HoraTransaccion).HasColumnName("HoraTransaccion");
            this.Property(t => t.GeoCamposDinamicos).HasColumnName("GeoCamposDinamicos");
            this.Property(t => t.DesMoneda).HasColumnName("DesMoneda");
            this.Property(t => t.CodAlpha2).HasColumnName("CodAlpha2");
            this.Property(t => t.CodMoneda).HasColumnName("CodMoneda");
            this.Property(t => t.CodMonedaLocal).HasColumnName("CodMonedaLocal");
            this.Property(t => t.CodMonedaInter).HasColumnName("CodMonedaInter");
            this.Property(t => t.CodMonedaNormativa).HasColumnName("CodMonedaNormativa");
            this.Property(t => t.Simbolo).HasColumnName("Simbolo");
            this.Property(t => t.NumCuenta).HasColumnName("NumCuenta");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.NombreImpreso).HasColumnName("NombreImpreso");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.DebitoCredito).HasColumnName("DebitoCredito");
            this.Property(t => t.SignoMonMovimiento).HasColumnName("SignoMonMovimiento");
            this.Property(t => t.SignoMonMovimientoLocal).HasColumnName("SignoMonMovimientoLocal");
            this.Property(t => t.SignoMonMovimientoInter).HasColumnName("SignoMonMovimientoInter");
            this.Property(t => t.MonCredLocal).HasColumnName("MonCredLocal");
            this.Property(t => t.MonDebLocal).HasColumnName("MonDebLocal");
            this.Property(t => t.MonCredInter).HasColumnName("MonCredInter");
            this.Property(t => t.MonDebInter).HasColumnName("MonDebInter");
            this.Property(t => t.NumReferencia).HasColumnName("NumReferencia");
            this.Property(t => t.CodEditado).HasColumnName("CodEditado");
            this.Property(t => t.MesMovimiento).HasColumnName("MesMovimiento");
            this.Property(t => t.AnoMovimiento).HasColumnName("AnoMovimiento");
            this.Property(t => t.IdLiquidado).HasColumnName("IdLiquidado");
            this.Property(t => t.IdGestion).HasColumnName("IdGestion");
        }
    }
}

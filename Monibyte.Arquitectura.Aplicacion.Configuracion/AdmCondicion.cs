﻿using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Aplicacion.Seguridad;
using Monibyte.Arquitectura.Aplicacion.Tesoreria;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Configuracion.Repositorios;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Configuracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public abstract class AdmCondicion : AplicacionBase, IAdmCondicion
    {
        protected abstract int TipoAutorizacion { get; }

        private IAppAutorizacion _appAutorizacion = GestorUnity.CargarUnidad<IAppAutorizacion>();
        private IFuncionesTesoreria _repFuncionesTesoreria = GestorUnity.CargarUnidad<IFuncionesTesoreria>();
        private IAppGenerales _appGenerales = GestorUnity.CargarUnidad<IAppGenerales>();


        protected string DefaultMcc { get { return "00000"; } }
        private int[] CategoriasVirtuales = new int[] { 
            (int)EnumCategoriaMcc.AvanceEfectivo,
            (int)EnumCategoriaMcc.ComprasInternet
        };

        public int? EstablecerCondicion(PocCondicion condicion, bool esActualizar = false,
            bool notificaCambioConfig = true, bool esNuevoGrupo = false)
        {
            int? resultado = null;
            condicion = EstabilizarCondicion(condicion, esActualizar);
            var condicionPrevia = InfoSesion.ObtenerSesion<PocCondicion>("CONDICIONPREVIA");
            if (condicion != null)
            {
                var numReferencia = this.TipoAutorizacion == (int)EnumTipoAutorizacion.RestriccionesTarjeta ? condicion.NumTarjeta : condicion.IdCondicion.ToString();
                if (Context.IsEditor)
                {
                    var detalle = condicion.ToJson();
                    IDictionary<string, object> dynamicData;

                    if (esNuevoGrupo)
                    {
                        dynamicData = detalle.MergeJson("{" + "EsNuevo" + ": true }") as IDictionary<string, object>;
                        detalle = StringExt.ToJson(dynamicData);
                    }
                    else
                    {
                        var data = _appAutorizacion.ObtieneDetalleAutorizacion((int)EnumTipoAutorizacion.RestriccionesGrupo,
                            numReferencia);
                        if (data == null)
                        {
                            if (condicionPrevia != null)
                            {
                                var stringData = new
                                {
                                    ViejoNombre = condicionPrevia.Descripcion,
                                    ViejoMonto = condicionPrevia.LimiteCredito
                                }.ToJson();
                                dynamicData = detalle.MergeJson(stringData) as IDictionary<string, object>;
                                detalle = StringExt.ToJson(dynamicData);
                            }
                        }
                    }
                    return _appAutorizacion.PrepararAutorizacion(this.TipoAutorizacion, numReferencia, detalle);
                }

                try
                {
                    var _topicTracker = new MultilanguageTopic("RecTopicConf", "TC_AA_CONFIGURACION");
                    InfoSesion.IncluirSesion("TOPICTRACK", _topicTracker);
                    using (var ts = new TransactionScope())
                    {
                        if (condicion.IdCondicion == 0 || condicionPrevia == null || condicionPrevia.IdCondicion == 0)
                        {
                            ValidarCondicion(condicion);
                            resultado = InsertarCondicion(condicion);
                        }
                        else
                        {
                            ValidarCondicion(condicion, true);
                            resultado = ModificarCondicion(condicion, esActualizar);
                        }
                        _appAutorizacion.AprobarAutorizacion(this.TipoAutorizacion, numReferencia);
                        SalvarCambios();
                        TrackCambiosNivel1(condicion);
                        SalvarBitacora(condicion, _topicTracker, notificaCambioConfig);
                        ts.Complete();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return resultado;
        }

        public PocCondicion ObtenerCondicion(object criterioBusqueda, bool esActualizar = false)
        {
            if (criterioBusqueda == null)
            {
                return null;
            }
            var autorizacion = _appAutorizacion.ObtenerAutorizacion(TipoAutorizacion, criterioBusqueda.ToString());
            if (InfoSesion.ObtenerSesion<bool?>("BUSCARAUTORIZACION") == true)
            {
                if (autorizacion != null)
                {
                    var detAutorizacion = autorizacion.DetalleAutorizacion.FromJson<PocCondicion>();
                    if (detAutorizacion.IdMoneda != detAutorizacion.IdMonedaLocal)
                    {
                        detAutorizacion.RestriccionesMcc.Update(up =>
                          {
                              up.SaldoLimiteConsumo = up.LimiteConsumoInter;
                              up.LimiteConsumo = up.LimiteConsumoInter;
                              up.SaldoCantidadConsumo = up.CantidadConsumo;
                          });
                    }
                    else
                    {
                        detAutorizacion.RestriccionesMcc.Update(up =>
                        {
                            up.SaldoLimiteConsumo = up.LimiteConsumo;
                            up.SaldoCantidadConsumo = up.CantidadConsumo;
                        });
                    }
                    detAutorizacion.EsAutorizacion = true;
                    detAutorizacion.Observaciones = autorizacion.Observaciones;
                    return detAutorizacion;
                }
            }
            PocCondicion resultado = ObtenerCondicionPrincipal(criterioBusqueda);
            if (resultado != null)
            {
                if (autorizacion != null)
                {
                    var dictionary = autorizacion.DetalleAutorizacion.FromJson<IDictionary<string, object>>();
                    if (autorizacion.DetalleAutorizacion.Contains("ViejoNombre"))
                    {
                        var nombre = dictionary["ViejoNombre"].ToString();
                        resultado.Descripcion = nombre;
                    }
                    if (autorizacion.DetalleAutorizacion.Contains("ViejoMonto"))
                    {
                        var limite = (double)dictionary["ViejoMonto"];
                        var limteR = limite.ToString("R");
                        var enDecimal = decimal.Parse(limteR);
                        resultado.LimiteCredito = enDecimal;
                    }
                }

                resultado.EsAutorizacion = false;
                resultado.RestriccionesMcc = ObtenerMccs(resultado.IdCondicion, resultado.IdMoneda, esActualizar);
                resultado.RestriccionesRegionPais = ObtenerRegionesPaises(resultado.IdCondicion);
                var horarios = ObtenerHorarios(resultado.IdCondicion);
                resultado.RestriccionesHorario = horarios.Where(m => m.IdDia != 0).ToList();
                /*obtiene el horario de cada restriccion*/
                if (resultado.EsVip)
                {
                    var variable = horarios.FirstOrDefault(p => p.IdRestriccion == (int)EnumTipoRestriccion.VIP);
                    if (variable != null)
                    {
                        resultado.EsVipDesde = variable.HoraDesde;
                        resultado.EsVipHasta = variable.HoraHasta;
                    }
                }
                if (resultado.RestringeRangoConsumeLocal)
                {
                    var variable = horarios.FirstOrDefault(p => p.IdRestriccion == (int)EnumTipoRestriccion.ConsumeLocal);
                    if (variable != null)
                    {
                        resultado.ConsumeLocalDesde = variable.HoraDesde;
                        resultado.ConsumeLocalHasta = variable.HoraHasta;
                    }
                    else
                    {
                        resultado.RestringeRangoConsumeLocal = false;
                    }
                }
                if (resultado.RestringeRangoConsumeInternacional)
                {
                    var variable = horarios.FirstOrDefault(p => p.IdRestriccion == (int)EnumTipoRestriccion.ConsumeInternacional);
                    if (variable != null)
                    {
                        resultado.ConsumeInternacionalDesde = variable.HoraDesde;
                        resultado.ConsumeInternacionalHasta = variable.HoraHasta;
                    }
                    else
                    {
                        resultado.RestringeRangoConsumeInternacional = false;
                    }
                }
                if (resultado.RestringeRangoPermiteAvanceEfectivo)
                {
                    var variable = horarios.FirstOrDefault(p => p.IdRestriccion == (int)EnumTipoRestriccion.AvanceEfectivo);
                    if (variable != null)
                    {
                        resultado.PermiteAvanceEfectivoDesde = variable.HoraDesde;
                        resultado.PermiteAvanceEfectivoHasta = variable.HoraHasta;
                    }
                    else
                    {
                        resultado.RestringeRangoPermiteAvanceEfectivo = false;
                    }
                }
                if (resultado.RestringeRangoConsumeInternet)
                {
                    var variable = horarios.FirstOrDefault(p => p.IdRestriccion == (int)EnumTipoRestriccion.ConsumeInternet);
                    if (variable != null)
                    {
                        resultado.ConsumeInternetDesde = variable.HoraDesde;
                        resultado.ConsumeInternetHasta = variable.HoraHasta;
                    }
                    else
                    {
                        resultado.RestringeRangoConsumeInternet = false;
                    }
                }
                if (resultado.DesactivarRestriccionesPorLapso)
                {
                    var restriccionesDesactivadas = horarios.FirstOrDefault(p => p.IdRestriccion == (int)EnumTipoRestriccion.SinRestriccion);
                    if (restriccionesDesactivadas != null)
                    {
                        if (RestriccionVigente(new ParamRestriccionVigente
                        {
                            IdRestriccion = (int)EnumTipoRestriccion.SinRestriccion,
                            IdCondicionTarjeta = resultado.IdCondicion,
                            FecTransacion = DateTime.Now
                        }))
                        {
                            resultado.DesactivarRestriccionesDesde = restriccionesDesactivadas.HoraDesde;
                            resultado.DesactivarRestriccionesHasta = restriccionesDesactivadas.HoraHasta;
                        }
                        else
                        {
                            resultado.DesactivarRestriccionesPorLapso = false;
                            resultado.SinRestriccion = false;
                        }
                    }
                    else
                    {
                        resultado.DesactivarRestriccionesPorLapso = false;
                        resultado.SinRestriccion = false;
                    }
                }
            }

            return resultado;
        }

        private int? InsertarCondicion(PocCondicion condicion)
        {
            condicion.IdEstado = (int)EnumIdEstado.Activo;
            var resultado = InsertarCondicionPrincipal(condicion);
            if (resultado != null)
            {
                condicion.IdCondicion = resultado.Value;
                //Establece el nuevo id de condicion en las restricciones hijas
                condicion.RestriccionesMcc.Update(m => m.IdCondicion = resultado.Value);
                condicion.RestriccionesHorario.Update(m => m.IdCondicion = resultado.Value);
                condicion.RestriccionesRegionPais.Update(m => m.IdCondicion = resultado.Value);
                MantenimientoVigencia(condicion);
                MantenimientoListaMcc(condicion.RestriccionesMcc, condicion.IdMoneda, condicion.RestringeMcc);
                MantenimientoListaHorario(condicion.RestriccionesHorario);
                MantenimientoListaRegionPais(condicion.RestriccionesRegionPais);
            }
            return resultado;
        }

        private int? ModificarCondicion(PocCondicion condicion, bool esActualizar = false)
        {
            int? resultado = null;
            var condicionPrevia = InfoSesion.ObtenerSesion<PocCondicion>("CONDICIONPREVIA");
            condicionPrevia.IdCondicion = condicion.IdCondicion;
            condicionPrevia = EstabilizarCondicion(condicionPrevia, esActualizar);
            if (!condicion.Equals(condicionPrevia))
            {
                MantenimientoVigencia(condicion, condicionPrevia);
                MantenimientoListaMcc(condicion.RestriccionesMcc, condicion.IdMoneda, condicion.RestringeMcc, condicionPrevia.RestriccionesMcc);
                MantenimientoListaHorario(condicion.RestriccionesHorario, condicionPrevia.RestriccionesHorario);
                MantenimientoListaRegionPais(condicion.RestriccionesRegionPais, condicionPrevia.RestriccionesRegionPais);
                if (!condicion.EqualsLvl1(condicionPrevia))
                {
                    resultado = ModificarCondicionPrincipal(condicion);
                }
                else
                {
                    resultado = condicionPrevia.IdCondicion;
                }
            }
            return resultado;
        }

        private PocCondicion EstabilizarCondicion(PocCondicion condicion, bool esActualizar = false)
        {
            var enteFinanciera = _appGenerales.ObtenerInformacionFinanciera();
            condicion.IdMonedaLocal = condicion.IdMonedaLocal == 0 ?
                    enteFinanciera.IdMonedaLocal : condicion.IdMonedaLocal;
            var esLocal = condicion.IdMoneda == condicion.IdMonedaLocal;
            //se asegura de que todas las condiciones hijas tengan el id correcto
            condicion.RestriccionesMcc = condicion.RestriccionesMcc ?? new List<PocMccCondicion>();

            Func<decimal?, PocCondicion, decimal> _convert = (monto, data) =>
            {
                if (monto == 0) { return monto.Value; }
                var valor = _repFuncionesTesoreria.ConvierteMonto(
                                data.IdMoneda, monto, data.IdMonedaLocal);
                return valor;
            };

            condicion.RestriccionesMcc.Update(m =>
            {
                m.IdCondicion = condicion.IdCondicion;
                m.LimiteConsumoInter = esActualizar ? m.LimiteConsumoInter : esLocal ? 0 : m.LimiteConsumo;
                m.SaldoLimiteConsumoInter = esActualizar ? m.SaldoLimiteConsumoInter : esLocal ? 0 : m.SaldoLimiteConsumo;
                m.LimiteConsumo = esLocal || esActualizar ? m.LimiteConsumo : _convert(m.LimiteConsumo, condicion);
                m.SaldoLimiteConsumo = esLocal || esActualizar ? m.SaldoLimiteConsumo : _convert(m.SaldoLimiteConsumo, condicion);
            });

            condicion.RestriccionesHorario = condicion.RestriccionesHorario ?? new List<PocHorarioCondicion>();
            condicion.RestriccionesHorario.Update(m => m.IdCondicion = condicion.IdCondicion);
            condicion.RestriccionesRegionPais = condicion.RestriccionesRegionPais ?? new List<PocRegionPaisCondicion>();
            condicion.RestriccionesRegionPais.Update(m => m.IdCondicion = condicion.IdCondicion);
            /*******Modificar valores inconsistentes que puedan venir dentro del poco*******/
            condicion.ConsumeLocal = condicion.SinRestriccion ? false :
                condicion.PermiteSoloAvanceEfectivo ? false : condicion.ConsumeLocal;
            condicion.ConsumeInternacional = condicion.SinRestriccion ? false :
                condicion.PermiteSoloAvanceEfectivo ? false : condicion.ConsumeInternacional;
            condicion.PermiteAvanceEfectivo = condicion.SinRestriccion ? false :
                condicion.PermiteSoloAvanceEfectivo ? true : condicion.PermiteAvanceEfectivo;
            condicion.ConsumeInternet = condicion.SinRestriccion ? false :
                condicion.PermiteSoloAvanceEfectivo ? false : condicion.ConsumeInternet;
            condicion.RestringeMcc = condicion.SinRestriccion ? false :
                condicion.PermiteSoloAvanceEfectivo ? false :
                !condicion.RestriccionesMcc.Any(item =>
                    !CategoriasVirtuales.Contains(item.IdCategoriaMcc)) ?
                false : condicion.RestringeMcc;
            condicion.RestringeHorario = condicion.SinRestriccion ? false :
                !condicion.RestriccionesHorario.Any() ? false : condicion.RestringeHorario;
            condicion.RestringeRegion = condicion.SinRestriccion ? false :
                !condicion.ConsumeInternacional ? false :
                !condicion.RestriccionesRegionPais.Any() ? false : condicion.RestringeRegion;
            /*******Modificar valores inconsistentes que puedan venir dentro del poco*******/
            return condicion;
        }

        private void TrackCambiosNivel1(PocCondicion condicion)
        {
            var condicionPrevia = InfoSesion.ObtenerSesion<PocCondicion>("CONDICIONPREVIA");
            //Obtiene el topic para hacer track de los cambios
            var _topic = InfoSesion.ObtenerSesion<MultilanguageTopic>("TOPICTRACK");

            //EntidadFinanciera
            var enteFinanciera = _appGenerales.ObtenerInformacionFinanciera();
            var esLocal = condicion.IdMoneda == enteFinanciera.IdMonedaLocal;

            if (condicionPrevia != null && condicionPrevia.IdMoneda != condicion.IdMoneda)
            {
                if (condicion.IdMoneda == enteFinanciera.IdMonedaLocal)
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_CAMBIO_MONEDA_LOCA");
                }
                else
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_CAMBIO_MONEDA_INTER");
                }
            }
            if (!esLocal && (condicionPrevia != null && condicionPrevia.IdMoneda == enteFinanciera.IdMonedaLocal))
            {
                _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DESCARGO_RESPONSABILIDAD");
            }

            //sin restricciones de ningun tipo
            if (condicion.SinRestriccion)
            {
                if (condicionPrevia == null || !condicionPrevia.SinRestriccion)
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_SINRESTRICCIONES");
                }
            }
            else if (condicionPrevia != null && condicionPrevia.SinRestriccion)
            {
                //_topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_SINRESTRICCIONES");
            }
            //vip
            if (condicion.EsVip)
            {
                if (condicionPrevia == null || !condicionPrevia.EsVip)
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_VIP");
                }
            }
            else if (condicionPrevia != null && condicionPrevia.EsVip)
            {
                _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_VIP");
            }
            if (!condicion.SinRestriccion)
            {
                //consumos locales
                if (condicion.ConsumeLocal)
                {
                    if (condicionPrevia == null || !condicionPrevia.ConsumeLocal)
                    {
                        _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_LOCAL");
                    }
                }
                else if (condicionPrevia != null && condicionPrevia.ConsumeLocal)
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_LOCAL");
                }
                //consumos internacionales
                if (condicion.ConsumeInternacional)
                {
                    if (condicionPrevia == null || !condicionPrevia.ConsumeInternacional)
                    {
                        _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_INTER");
                    }
                }
                else if (condicionPrevia != null && condicionPrevia.ConsumeInternacional)
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_INTER");
                }
                //avances de efectivo
                if (condicion.PermiteAvanceEfectivo)
                {
                    if (condicionPrevia == null || !condicionPrevia.PermiteAvanceEfectivo)
                    {
                        _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_AVANCE");
                    }
                    //solamente avance de efectivo
                    if (condicion.PermiteSoloAvanceEfectivo)
                    {
                        if (condicionPrevia == null || !condicionPrevia.PermiteSoloAvanceEfectivo)
                        {
                            _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_AVANCE_SOLO");
                        }
                    }
                    else if (condicionPrevia != null && condicionPrevia.PermiteSoloAvanceEfectivo)
                    {
                        _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_AVANCE_SOLO");
                    }
                    TrackCambiosMccNivel1(ref _topic, condicion, EnumCategoriaMcc.AvanceEfectivo,
                        "TC_ADD_LIMITE_AVANCE", "TC_UP_LIMITE_AVANCE_NEW");
                }
                else if (condicionPrevia != null && (condicionPrevia.PermiteAvanceEfectivo ||
                    condicionPrevia.PermiteSoloAvanceEfectivo))
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_AVANCE");
                }
                //compras por internet
                if (condicion.ConsumeInternet)
                {
                    if (condicionPrevia == null || !condicionPrevia.ConsumeInternet)
                    {
                        _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_INTERNET");
                    }
                    TrackCambiosMccNivel1(ref _topic, condicion, EnumCategoriaMcc.ComprasInternet,
                        "TC_ADD_LIMITE_INTERNET", "TC_UP_LIMITE_INTERNET_NEW");
                }
                else if (condicionPrevia != null && condicionPrevia.ConsumeInternet)
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_INTERNET");
                }
            }
            //restringe comercios
            if (condicion.RestringeMcc)
            {
                if (condicionPrevia == null || !condicionPrevia.RestringeMcc)
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_SINRESTMCC");
                }
            }
            else if (condicionPrevia != null && condicionPrevia.RestringeMcc)
            {
                _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_SINRESTMCC");
            }
            //restringe horarios
            if (condicion.RestringeHorario)
            {
                if (condicionPrevia == null || !condicionPrevia.RestringeHorario)
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_SINRESTHOR");
                }
            }
            else if (condicionPrevia != null && condicionPrevia.RestringeHorario)
            {
                _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_SINRESTHOR");
            }
            //restringe regiones
            if (condicion.RestringeRegion)
            {
                if (condicionPrevia == null || !condicionPrevia.RestringeRegion)
                {
                    _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_ADD_SINRESTREG");
                }
            }
            else if (condicionPrevia != null && condicionPrevia.RestringeRegion)
            {
                _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_DEL_SINRESTREG");
            }
            //Totalmente restringida
            if (_topic.Details.Count() == 0 && _topic.Items.Count() == 0)
            {
                _topic.AddTopicDetail("TC_AA_CONFIGURACION", "TC_TOTAL_RESTRINGIDA");
            }
        }

        private void TrackCambiosMccNivel1(ref MultilanguageTopic _topic,
            PocCondicion condicion, EnumCategoriaMcc categoria,
            string resourceAddMcc, string resourceUpMcc)
        {
            var condicionPrevia = InfoSesion.ObtenerSesion<PocCondicion>("CONDICIONPREVIA");
            var mccActual = condicion.RestriccionesMcc.FirstOrDefault(m =>
                m.IdCategoriaMcc == (int)categoria);

            var enteFinanciera = _appGenerales.ObtenerInformacionFinanciera();
            var esLocal = condicion.IdMoneda == enteFinanciera.IdMonedaLocal;
            var simboloMoneda = esLocal ? enteFinanciera.SimboloLocal : enteFinanciera.SimboloInter;

            if (mccActual != null)
            {
                PocMccCondicion mccPrevio = null;
                var periodicidadResx = ObtenerResxPeriodicidad(mccActual.IdPeriodicidad);
                if (condicionPrevia != null && condicionPrevia.RestriccionesMcc != null)
                {
                    mccPrevio = condicionPrevia.RestriccionesMcc.FirstOrDefault(m =>
                        m.IdCategoriaMcc == (int)categoria);
                    if (mccPrevio != null)
                    {
                        if (!mccActual.Equals(mccPrevio))
                        {
                            var periodicidadResxPrevia = ObtenerResxPeriodicidad(mccPrevio.IdPeriodicidad);
                            if (resourceUpMcc == "TC_UP_LIMITE_AVANCE_NEW" || resourceUpMcc == "TC_UP_LIMITE_INTERNET_NEW")
                            {
                                _topic.AddTopicDetail("TC_AA_CONFIGURACION", resourceUpMcc,
                                periodicidadResxPrevia,
                                esLocal ? mccPrevio.LimiteConsumo : mccPrevio.LimiteConsumoInter,
                                periodicidadResx,
                                esLocal ? mccActual.LimiteConsumo : mccActual.LimiteConsumoInter,
                                mccPrevio.CantidadConsumo,
                                mccActual.CantidadConsumo,
                                esLocal ? mccPrevio.SaldoLimiteConsumo : mccPrevio.SaldoLimiteConsumoInter,
                                esLocal ? mccActual.SaldoLimiteConsumo : mccActual.SaldoLimiteConsumoInter,
                                mccPrevio.SaldoCantidadConsumo,
                                mccActual.SaldoCantidadConsumo,
                                simboloMoneda);
                            }
                            else
                            {
                                _topic.AddTopicDetail("TC_AA_CONFIGURACION", resourceUpMcc,
                                periodicidadResxPrevia,
                                esLocal ? mccPrevio.LimiteConsumo : mccPrevio.LimiteConsumoInter,
                                periodicidadResx,
                                esLocal ? mccActual.LimiteConsumo : mccActual.LimiteConsumoInter,
                                mccPrevio.CantidadConsumo,
                                mccActual.CantidadConsumo,
                                simboloMoneda);
                            }
                        }
                    }
                    if (mccPrevio == null)
                    {
                        _topic.AddTopicDetail("TC_AA_CONFIGURACION", resourceAddMcc,
                            periodicidadResx,
                            esLocal ? mccActual.LimiteConsumo : mccActual.LimiteConsumoInter,
                            mccActual.CantidadConsumo,
                            simboloMoneda);
                    }
                }
            }
        }

        private void MantenimientoVigencia(PocCondicion condicion, PocCondicion condicionPrevia = null)
        {
            //Obtiene el topic para hacer track de los cambios
            var _topic = InfoSesion.ObtenerSesion<MultilanguageTopic>("TOPICTRACK");

            if (condicion.EsVip)
            {
                if (condicionPrevia == null || condicionPrevia.EsVipDesde == null)
                {
                    InsertaVigencia(condicion.IdCondicion, EnumTipoRestriccion.VIP,
                        (DateTime)condicion.EsVipDesde, (DateTime)condicion.EsVipHasta);
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_ADD_VIGENCIA_VIP",
                        condicion.EsVipDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.EsVipHasta.ToDateStr("dd/MMM/yyyy"));
                }
                else if (!condicion.EsVipDesde.Equals(condicionPrevia.EsVipDesde) ||
                    !condicion.EsVipHasta.Equals(condicionPrevia.EsVipHasta))
                {
                    ModificarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.VIP,
                        HoraDesde = (DateTime)condicion.EsVipDesde,
                        HoraHasta = (DateTime)condicion.EsVipHasta
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_UP_VIGENCIA_VIP",
                        condicionPrevia.EsVipDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.EsVipHasta.ToDateStr("dd/MMM/yyyy"),
                        condicion.EsVipDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.EsVipHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            else
            {
                if (condicionPrevia != null && condicionPrevia.EsVipDesde != null)
                {
                    EliminarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.VIP
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_DEL_VIGENCIA_VIP",
                        condicionPrevia.EsVipDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.EsVipHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            if (condicion.RestringeRangoConsumeLocal)
            {
                if (condicionPrevia == null || condicionPrevia.ConsumeLocalDesde == null)
                {
                    InsertaVigencia(condicion.IdCondicion, EnumTipoRestriccion.ConsumeLocal,
                        (DateTime)condicion.ConsumeLocalDesde, (DateTime)condicion.ConsumeLocalHasta);
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_ADD_VIGENCIA_LOCAL",
                        condicion.ConsumeLocalDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.ConsumeLocalHasta.ToDateStr("dd/MMM/yyyy"));
                }
                else if (!condicion.ConsumeLocalDesde.Equals(condicionPrevia.ConsumeLocalDesde) ||
                    !condicion.ConsumeLocalHasta.Equals(condicionPrevia.ConsumeLocalHasta))
                {
                    ModificarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.ConsumeLocal,
                        HoraDesde = (DateTime)condicion.ConsumeLocalDesde,
                        HoraHasta = (DateTime)condicion.ConsumeLocalHasta
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_UP_VIGENCIA_LOCAL",
                        condicionPrevia.ConsumeLocalDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.ConsumeLocalHasta.ToDateStr("dd/MMM/yyyy"),
                        condicion.ConsumeLocalDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.ConsumeLocalHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            else
            {
                if (condicionPrevia != null && condicionPrevia.ConsumeLocalDesde != null)
                {
                    EliminarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.ConsumeLocal
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_DEL_VIGENCIA_LOCAL",
                        condicionPrevia.ConsumeLocalDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.ConsumeLocalHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            if (condicion.RestringeRangoConsumeInternacional)
            {
                if (condicionPrevia == null || condicionPrevia.ConsumeInternacionalDesde == null)
                {
                    InsertaVigencia(condicion.IdCondicion, EnumTipoRestriccion.ConsumeInternacional,
                        (DateTime)condicion.ConsumeInternacionalDesde, (DateTime)condicion.ConsumeInternacionalHasta);
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_ADD_VIGENCIA_INTER",
                        condicion.ConsumeInternacionalDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.ConsumeInternacionalHasta.ToDateStr("dd/MMM/yyyy"));
                }
                else if (!condicion.ConsumeInternacionalDesde.Equals(condicionPrevia.ConsumeInternacionalDesde) ||
                    !condicion.ConsumeInternacionalHasta.Equals(condicionPrevia.ConsumeInternacionalHasta))
                {
                    ModificarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.ConsumeInternacional,
                        HoraDesde = (DateTime)condicion.ConsumeInternacionalDesde,
                        HoraHasta = (DateTime)condicion.ConsumeInternacionalHasta
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_UP_VIGENCIA_INTER",
                        condicionPrevia.ConsumeInternacionalDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.ConsumeInternacionalHasta.ToDateStr("dd/MMM/yyyy"),
                        condicion.ConsumeInternacionalDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.ConsumeInternacionalHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            else
            {
                if (condicionPrevia != null && condicionPrevia.ConsumeInternacionalDesde != null)
                {
                    EliminarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.ConsumeInternacional
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_DEL_VIGENCIA_INTER",
                        condicionPrevia.ConsumeInternacionalDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.ConsumeInternacionalHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            if (condicion.RestringeRangoPermiteAvanceEfectivo)
            {
                if (condicionPrevia == null || condicionPrevia.PermiteAvanceEfectivoDesde == null)
                {
                    InsertaVigencia(condicion.IdCondicion, EnumTipoRestriccion.AvanceEfectivo,
                        (DateTime)condicion.PermiteAvanceEfectivoDesde, (DateTime)condicion.PermiteAvanceEfectivoHasta);
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_ADD_VIGENCIA_ADELANTO",
                        condicion.PermiteAvanceEfectivoDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.PermiteAvanceEfectivoHasta.ToDateStr("dd/MMM/yyyy"));
                }
                else if (!condicion.PermiteAvanceEfectivoDesde.Equals(condicionPrevia.PermiteAvanceEfectivoDesde) ||
                    !condicion.PermiteAvanceEfectivoHasta.Equals(condicionPrevia.PermiteAvanceEfectivoHasta))
                {
                    ModificarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.AvanceEfectivo,
                        HoraDesde = (DateTime)condicion.PermiteAvanceEfectivoDesde,
                        HoraHasta = (DateTime)condicion.PermiteAvanceEfectivoHasta
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_UP_VIGENCIA_ADELANTO",
                        condicionPrevia.PermiteAvanceEfectivoDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.PermiteAvanceEfectivoHasta.ToDateStr("dd/MMM/yyyy"),
                        condicion.PermiteAvanceEfectivoDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.PermiteAvanceEfectivoHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            else
            {
                if (condicionPrevia != null && condicionPrevia.PermiteAvanceEfectivoDesde != null)
                {
                    EliminarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.AvanceEfectivo
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_DEL_VIGENCIA_ADELANTO",
                        condicionPrevia.PermiteAvanceEfectivoDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.PermiteAvanceEfectivoHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            if (condicion.RestringeRangoConsumeInternet)
            {
                if (condicionPrevia == null || condicionPrevia.ConsumeInternetDesde == null)
                {
                    InsertaVigencia(condicion.IdCondicion, EnumTipoRestriccion.ConsumeInternet,
                        (DateTime)condicion.ConsumeInternetDesde, (DateTime)condicion.ConsumeInternetHasta);
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_ADD_VIGENCIA_INTERNET",
                        condicion.ConsumeInternetDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.ConsumeInternetHasta.ToDateStr("dd/MMM/yyyy"));
                }
                else if (!condicion.ConsumeInternetDesde.Equals(condicionPrevia.ConsumeInternetDesde) ||
                    !condicion.ConsumeInternetHasta.Equals(condicionPrevia.ConsumeInternetHasta))
                {
                    ModificarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.ConsumeInternet,
                        HoraDesde = (DateTime)condicion.ConsumeInternetDesde,
                        HoraHasta = (DateTime)condicion.ConsumeInternetHasta
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_UP_VIGENCIA_INTERNET",
                        condicionPrevia.ConsumeInternetDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.ConsumeInternetHasta.ToDateStr("dd/MMM/yyyy"),
                        condicion.ConsumeInternetDesde.ToDateStr("dd/MMM/yyyy"),
                        condicion.ConsumeInternetHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            else
            {
                if (condicionPrevia != null && condicionPrevia.ConsumeInternetDesde != null)
                {
                    EliminarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.ConsumeInternet
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_DEL_VIGENCIA_INTERNET",
                        condicionPrevia.ConsumeInternetDesde.ToDateStr("dd/MMM/yyyy"),
                        condicionPrevia.ConsumeInternetHasta.ToDateStr("dd/MMM/yyyy"));
                }
            }
            if (condicion.DesactivarRestriccionesPorLapso)
            {
                EliminarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.SinRestriccion,
                        IdDia = 0
                    });

                InsertaVigencia(condicion.IdCondicion, EnumTipoRestriccion.SinRestriccion, (DateTime)condicion.DesactivarRestriccionesDesde,
                    (DateTime)condicion.DesactivarRestriccionesHasta);
                _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_ADD_VIGENCIA_DESACTIVACION",
                    condicion.DesactivarRestriccionesDesde.ToDateStr("dd/MMM/yyyy HH:mm"),
                    condicion.DesactivarRestriccionesHasta.ToDateStr("dd/MMM/yyyy HH:mm"));

            }
            else
            {
                if (condicionPrevia != null && condicionPrevia.DesactivarRestriccionesDesde != null)
                {
                    EliminarVigencia(new PocHorarioCondicion
                    {
                        IdCondicion = condicion.IdCondicion,
                        IdRestriccion = (int)EnumTipoRestriccion.SinRestriccion,
                        IdDia = 0
                    });
                    _topic.AddTopicDetail("TC_AA_VIGENCIA", "TC_DEL_VIGENCIA_DESACTIVACION",
                        condicionPrevia.DesactivarRestriccionesDesde.ToDateStr("dd/MMM/yyyy HH:mm"),
                        condicionPrevia.DesactivarRestriccionesHasta.ToDateStr("dd/MMM/yyyy HH:mm"));
                }
            }
        }

        private string ObtenerResxPeriodicidad(int idPeriodicidad)
        {
            string resultado = null;
            switch (idPeriodicidad)
            {
                case (int)EnumPeriodicidad.Diario: resultado = "gresx_Diario"; break;
                case (int)EnumPeriodicidad.Semanal: resultado = "gresx_Semanal"; break;
                case (int)EnumPeriodicidad.Mensual: resultado = "gresx_Mensual"; break;
            }
            return resultado;
        }

        private void MantenimientoListaMcc(List<PocMccCondicion> actuales, int idMoneda,
            bool restringeMcc, List<PocMccCondicion> previas = null)
        {
            previas = previas ?? new List<PocMccCondicion>();
            //Obtiene el topic para hacer track de los cambios
            var _topic = InfoSesion.ObtenerSesion<MultilanguageTopic>("TOPICTRACK");

            var enteFinanciera = _appGenerales.ObtenerInformacionFinanciera();
            var esLocal = idMoneda == enteFinanciera.IdMonedaLocal;
            var simboloMoneda = esLocal ? enteFinanciera.SimboloLocal : enteFinanciera.SimboloInter;

            foreach (var item in actuales)
            {
                bool aplicaBitacora = !CategoriasVirtuales.Contains(item.IdCategoriaMcc) && restringeMcc;
                //busca un item similar en la configuracion previa
                var itemPrevio = previas.FirstOrDefault(m => m.IdCategoriaMcc == item.IdCategoriaMcc &&
                    (m.CodMcc ?? string.Empty) == (item.CodMcc ?? string.Empty));
                if (itemPrevio == null)
                {
                    item.CodMcc = string.IsNullOrEmpty(item.CodMcc) ? this.DefaultMcc : item.CodMcc;
                    item.CantidadConsumo = item.CantidadConsumo ?? 0;
                    item.SaldoCantidadConsumo = item.SaldoCantidadConsumo ?? 0;

                    InsertarMcc(item);
                    if (aplicaBitacora)
                    {
                        var periodicidadResx = ObtenerResxPeriodicidad(item.IdPeriodicidad);
                        _topic.AddTopicDetail("TC_AA_MCC", "TC_ADD_MCC",
                            new
                            {
                                Context = "MB_CategoriaTipoComercio",
                                ContextId = item.IdCategoriaMcc
                            },
                            esLocal ? item.LimiteConsumo : item.LimiteConsumoInter,
                            item.CantidadConsumo,
                            periodicidadResx,
                            simboloMoneda);
                    }
                }
                else if (itemPrevio != null && !item.Equals(itemPrevio))
                {
                    item.IdMcc = itemPrevio.IdMcc;
                    ModificarMcc(item);
                    if (aplicaBitacora)
                    {
                        var periodicidadResxPrevia = ObtenerResxPeriodicidad(itemPrevio.IdPeriodicidad);
                        var periodicidadResx = ObtenerResxPeriodicidad(item.IdPeriodicidad);
                        _topic.AddTopicDetail("TC_AA_MCC", "TC_UP_MCC",
                            new
                            {
                                Context = "MB_CategoriaTipoComercio",
                                ContextId = itemPrevio.IdCategoriaMcc
                            },
                            esLocal ? itemPrevio.LimiteConsumo : itemPrevio.LimiteConsumoInter,
                            itemPrevio.CantidadConsumo,
                            esLocal ? item.LimiteConsumo : item.LimiteConsumoInter,
                            item.CantidadConsumo,
                            periodicidadResxPrevia,
                            periodicidadResx,
                            esLocal ? itemPrevio.SaldoLimiteConsumo : itemPrevio.SaldoLimiteConsumoInter,
                            itemPrevio.SaldoCantidadConsumo,
                            esLocal ? item.SaldoLimiteConsumo : item.SaldoLimiteConsumoInter,
                            item.SaldoCantidadConsumo,
                            simboloMoneda);
                    }
                }
            }
            if (previas != null)
            {
                foreach (var itemPrevio in previas)
                {
                    bool aplicaBitacora = !CategoriasVirtuales.Contains(itemPrevio.IdCategoriaMcc);
                    var item = actuales.FirstOrDefault(m => m.IdCategoriaMcc == itemPrevio.IdCategoriaMcc &&
                           (m.CodMcc ?? string.Empty) == (itemPrevio.CodMcc ?? string.Empty));
                    if (item == null)
                    {
                        EliminarMcc(itemPrevio);
                        if (aplicaBitacora)
                        {
                            _topic.AddTopicDetail("TC_AA_MCC", "TC_DEL_MCC",
                            new
                            {
                                Context = "MB_CategoriaTipoComercio",
                                ContextId = itemPrevio.IdCategoriaMcc
                            });
                        }
                    }
                }
            }
        }

        private void MantenimientoListaHorario(List<PocHorarioCondicion> actuales, List<PocHorarioCondicion> previas = null)
        {
            //Obtiene el topic para hacer track de los cambios
            var _topic = InfoSesion.ObtenerSesion<MultilanguageTopic>("TOPICTRACK");
            foreach (var item in actuales)
            {
                //busca un item similar en la configuracion previa
                var itemPrevio = previas != null ? previas.FirstOrDefault(m => m.IdDia == item.IdDia &&
                       m.IdRestriccion == item.IdRestriccion) : null;
                if (itemPrevio == null)
                {
                    InsertarHorario(item);
                    _topic.AddTopicDetail("TC_AA_HORARIOS", "TC_ADD_HORARIOS", new
                    {
                        Context = "LDIASSEMANA",
                        ContextId = item.IdDia
                    }, item.HoraDesde.ToString("hh tt"), item.HoraHasta.ToString("hh tt"));
                }
                else if (itemPrevio != null && !item.Equals(itemPrevio))
                {
                    item.IdHorario = itemPrevio.IdHorario;
                    ModificarHorario(item);
                    _topic.AddTopicDetail("TC_AA_HORARIOS", "TC_UP_HORARIOS", new
                                    {
                                        Context = "LDIASSEMANA",
                                        ContextId = itemPrevio.IdDia
                                    },
                        itemPrevio.HoraDesde.ToString("hh tt"), itemPrevio.HoraHasta.ToString("hh tt"),
                        item.HoraDesde.ToString("hh tt"), item.HoraHasta.ToString("hh tt"));
                }
            }
            if (previas != null)
            {
                foreach (var itemPrevio in previas)
                {
                    var item = actuales.FirstOrDefault(m => m.IdDia == itemPrevio.IdDia &&
                           m.IdRestriccion == itemPrevio.IdRestriccion);
                    if (item == null)
                    {
                        EliminarHorario(itemPrevio);
                        _topic.AddTopicDetail("TC_AA_HORARIOS", "TC_DEL_HORARIOS", new
                        {
                            Context = "LDIASSEMANA",
                            ContextId = itemPrevio.IdDia
                        }, itemPrevio.HoraDesde.ToString("hh tt"), itemPrevio.HoraHasta.ToString("hh tt"));
                    }
                }
            }
        }

        private void MantenimientoListaRegionPais(List<PocRegionPaisCondicion> actuales, List<PocRegionPaisCondicion> previas = null)
        {
            //Obtiene el topic para hacer track de los cambios
            var _topic = InfoSesion.ObtenerSesion<MultilanguageTopic>("TOPICTRACK");
            foreach (var item in actuales)
            {
                //busca un item similar en la configuracion previa
                var itemPrevio = previas != null ? previas.FirstOrDefault(m => m.IdRegion == item.IdRegion &&
                       m.IdPais == item.IdPais) : null;
                if (itemPrevio == null)
                {
                    InsertarRegionPais(item);
                    _topic.AddTopicDetail("TC_AA_REGIONPAIS", "TC_ADD_REGIONPAIS",
                         new { Context = "MB_Region", ContextId = item.IdRegion },
                         new { Context = "GL_Pais", ContextId = item.IdPais });
                }
                else if (itemPrevio != null && !item.Equals(itemPrevio))
                {
                    item.IdRegionPais = itemPrevio.IdRegionPais;
                    ModificarRegionPais(item);
                    _topic.AddTopicDetail("TC_AA_REGIONPAIS", "TC_UP_REGIONPAIS",
                         new { Context = "MB_Region", ContextId = itemPrevio.IdRegion },
                         new { Context = "GL_Pais", ContextId = itemPrevio.IdPais });
                }
            }
            if (previas != null)
            {
                foreach (var itemPrevio in previas)
                {
                    var item = actuales.FirstOrDefault(m => m.IdRegion == itemPrevio.IdRegion &&
                           m.IdPais == itemPrevio.IdPais);
                    if (item == null)
                    {
                        EliminarRegionPais(itemPrevio);
                        _topic.AddTopicDetail("TC_AA_REGIONPAIS", "TC_DEL_REGIONPAIS",
                            new { Context = "MB_Region", ContextId = itemPrevio.IdRegion },
                            new { Context = "GL_Pais", ContextId = itemPrevio.IdPais });
                    }
                }
            }
        }

        //registro en bd y validaciones
        protected abstract void SalvarCambios();
        protected abstract void SalvarBitacora(PocCondicion condicion, MultilanguageTopic tracker, bool notificaCambioConfig);
        protected abstract void ValidarCondicion(PocCondicion condicion, bool update = false);
        //consultas
        protected abstract PocCondicion ObtenerCondicionPrincipal(object criterioBusqueda);
        protected abstract List<PocMccCondicion> ObtenerMccs(int idCondicion,
                                        int? idMoneda, bool esActualizar = false);
        protected abstract List<PocHorarioCondicion> ObtenerHorarios(int idCondicion);
        protected abstract List<PocRegionPaisCondicion> ObtenerRegionesPaises(int idCondicion);
        protected abstract bool RestriccionVigente(ParamRestriccionVigente parameters);
        //registros nuevos
        protected abstract int? InsertarCondicionPrincipal(PocCondicion condicion);
        protected abstract void InsertarMcc(PocMccCondicion item);
        protected abstract void InsertarHorario(PocHorarioCondicion item);
        protected abstract void InsertaVigencia(int idCondicion, EnumTipoRestriccion tipoRestriccion, DateTime horaDesde, DateTime horaHasta);
        protected abstract void InsertarRegionPais(PocRegionPaisCondicion item);
        //modificar registros
        protected abstract int? ModificarCondicionPrincipal(PocCondicion condicion);
        protected abstract void ModificarMcc(PocMccCondicion item);
        protected abstract void ModificarHorario(PocHorarioCondicion item);
        protected abstract void ModificarVigencia(PocHorarioCondicion item);
        protected abstract void ModificarRegionPais(PocRegionPaisCondicion item);
        //eliminar registros no utilizados
        protected abstract void EliminarMcc(PocMccCondicion item);
        protected abstract void EliminarHorario(PocHorarioCondicion item);
        protected abstract void EliminarVigencia(PocHorarioCondicion item);
        protected abstract void EliminarRegionPais(PocRegionPaisCondicion item);
    }
}

﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Configuracion;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public interface IAdmCondicion : IAplicacionBase
    {
        int? EstablecerCondicion(PocCondicion condicion, bool esActualizar = false,
             bool notificaCambioConfig = true, bool esNuevoGrupo = false);
        PocCondicion ObtenerCondicion(object criterioBusqueda, bool esActualizar = false);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "12.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class RecTopicConf {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal RecTopicConf() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.RecTopicConf", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to diario.
        /// </summary>
        internal static string gresx_Diario {
            get {
                return ResourceManager.GetString("gresx_Diario", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to mensual.
        /// </summary>
        internal static string gresx_Mensual {
            get {
                return ResourceManager.GetString("gresx_Mensual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to semanal.
        /// </summary>
        internal static string gresx_Semanal {
            get {
                return ResourceManager.GetString("gresx_Semanal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;Mantenimiento de configuración&lt;/strong&gt;&lt;br/&gt;.
        /// </summary>
        internal static string TC_AA_CONFIGURACION {
            get {
                return ResourceManager.GetString("TC_AA_CONFIGURACION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;br/&gt;&lt;strong&gt;Horarios permitidos&lt;/strong&gt;&lt;br/&gt;.
        /// </summary>
        internal static string TC_AA_HORARIOS {
            get {
                return ResourceManager.GetString("TC_AA_HORARIOS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;br/&gt;&lt;strong&gt;Comercios autorizados&lt;/strong&gt;&lt;br/&gt;.
        /// </summary>
        internal static string TC_AA_MCC {
            get {
                return ResourceManager.GetString("TC_AA_MCC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;br/&gt;&lt;strong&gt;Regiones y países autorizados&lt;/strong&gt;&lt;br/&gt;.
        /// </summary>
        internal static string TC_AA_REGIONPAIS {
            get {
                return ResourceManager.GetString("TC_AA_REGIONPAIS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;br/&gt;&lt;strong&gt;Vigencia de parámetros&lt;/strong&gt;&lt;br/&gt;.
        /// </summary>
        internal static string TC_AA_VIGENCIA {
            get {
                return ResourceManager.GetString("TC_AA_VIGENCIA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permite adelanto de efectivo&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_AVANCE {
            get {
                return ResourceManager.GetString("TC_ADD_AVANCE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sólo adelanto de efectivo --activa--&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_AVANCE_SOLO {
            get {
                return ResourceManager.GetString("TC_ADD_AVANCE_SOLO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nueva restricción de horario: {0} de las {1} a las {2}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_HORARIOS {
            get {
                return ResourceManager.GetString("TC_ADD_HORARIOS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permite consumos internacionales&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_INTER {
            get {
                return ResourceManager.GetString("TC_ADD_INTER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permite compras por internet&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_INTERNET {
            get {
                return ResourceManager.GetString("TC_ADD_INTERNET", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se incluye el límite {0} para --adelantos de efectivo-- en {3}{1} y cantidad {2}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_LIMITE_AVANCE {
            get {
                return ResourceManager.GetString("TC_ADD_LIMITE_AVANCE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se incluye el límite {0} para --compras por Internet-- en {3}{1} y cantidad {2}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_LIMITE_INTERNET {
            get {
                return ResourceManager.GetString("TC_ADD_LIMITE_INTERNET", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permite consumo local&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_LOCAL {
            get {
                return ResourceManager.GetString("TC_ADD_LOCAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nuevo comercio autorizado: {0}. Lim. {3} de consumo {4}{1} y cant. {3} de consumos {2}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_MCC {
            get {
                return ResourceManager.GetString("TC_ADD_MCC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nueva región autorizada: {0} -- país {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_REGIONPAIS {
            get {
                return ResourceManager.GetString("TC_ADD_REGIONPAIS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restricción de horarios --activa--&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_SINRESTHOR {
            get {
                return ResourceManager.GetString("TC_ADD_SINRESTHOR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restricción de comercios --activa--&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_SINRESTMCC {
            get {
                return ResourceManager.GetString("TC_ADD_SINRESTMCC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restricción de regiones --activa--&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_SINRESTREG {
            get {
                return ResourceManager.GetString("TC_ADD_SINRESTREG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SIN Restricciones&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_SINRESTRICCIONES {
            get {
                return ResourceManager.GetString("TC_ADD_SINRESTRICCIONES", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se incluye la restricción por vigencia para --adelantos de efectivo-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_VIGENCIA_ADELANTO {
            get {
                return ResourceManager.GetString("TC_ADD_VIGENCIA_ADELANTO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se incluye la restricción por vigencia para --Desactivación temporal de restricciones-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_VIGENCIA_DESACTIVACION {
            get {
                return ResourceManager.GetString("TC_ADD_VIGENCIA_DESACTIVACION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se incluye la restricción por vigencia para --consumos internacionales-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_VIGENCIA_INTER {
            get {
                return ResourceManager.GetString("TC_ADD_VIGENCIA_INTER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se incluye la restricción por vigencia para --compras por Internet-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_VIGENCIA_INTERNET {
            get {
                return ResourceManager.GetString("TC_ADD_VIGENCIA_INTERNET", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se incluye la restricción por vigencia para --consumos locales-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_VIGENCIA_LOCAL {
            get {
                return ResourceManager.GetString("TC_ADD_VIGENCIA_LOCAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se incluye la restricción por vigencia para --VIP-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_VIGENCIA_VIP {
            get {
                return ResourceManager.GetString("TC_ADD_VIGENCIA_VIP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to VIP --activa--&lt;br/&gt;.
        /// </summary>
        internal static string TC_ADD_VIP {
            get {
                return ResourceManager.GetString("TC_ADD_VIP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;br/&gt;&lt;strong&gt;Configuración de Moneda&lt;/strong&gt;&lt;br/&gt; Se cambia de Moneda Local a Moneda Internacional &lt;br/&gt;.
        /// </summary>
        internal static string TC_CAMBIO_MONEDA_INTER {
            get {
                return ResourceManager.GetString("TC_CAMBIO_MONEDA_INTER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;br/&gt;&lt;strong&gt;Configuración de Moneda&lt;/strong&gt;&lt;br/&gt; Se cambia de Moneda Internacional a Moneda Local &lt;br/&gt;.
        /// </summary>
        internal static string TC_CAMBIO_MONEDA_LOCA {
            get {
                return ResourceManager.GetString("TC_CAMBIO_MONEDA_LOCA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No permite adelanto de efectivo&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_AVANCE {
            get {
                return ResourceManager.GetString("TC_DEL_AVANCE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sólo adelanto de efectivo --inactiva--&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_AVANCE_SOLO {
            get {
                return ResourceManager.GetString("TC_DEL_AVANCE_SOLO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se elimina la restricción de horario: {0} de las {1} a las {2}&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_HORARIOS {
            get {
                return ResourceManager.GetString("TC_DEL_HORARIOS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No permite consumos internacionales&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_INTER {
            get {
                return ResourceManager.GetString("TC_DEL_INTER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No permite compras por internet&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_INTERNET {
            get {
                return ResourceManager.GetString("TC_DEL_INTERNET", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No permite consumos locales&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_LOCAL {
            get {
                return ResourceManager.GetString("TC_DEL_LOCAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se excluye el comercio {0} de los comercios autorizados&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_MCC {
            get {
                return ResourceManager.GetString("TC_DEL_MCC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se excluye la región {0} -- país {1} de las regiones autorizadas&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_REGIONPAIS {
            get {
                return ResourceManager.GetString("TC_DEL_REGIONPAIS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restricción de horarios --inactiva--&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_SINRESTHOR {
            get {
                return ResourceManager.GetString("TC_DEL_SINRESTHOR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restricción de comercios --inactiva--&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_SINRESTMCC {
            get {
                return ResourceManager.GetString("TC_DEL_SINRESTMCC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restricción de regiones --inactiva--&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_SINRESTREG {
            get {
                return ResourceManager.GetString("TC_DEL_SINRESTREG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;span/&gt;.
        /// </summary>
        internal static string TC_DEL_SINRESTRICCIONES {
            get {
                return ResourceManager.GetString("TC_DEL_SINRESTRICCIONES", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se elimina la restricción de vigencia para --adelantos de efectivo-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_VIGENCIA_ADELANTO {
            get {
                return ResourceManager.GetString("TC_DEL_VIGENCIA_ADELANTO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se elimina la restricción de vigencia para --Desactivación temporal de restricciones-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_VIGENCIA_DESACTIVACION {
            get {
                return ResourceManager.GetString("TC_DEL_VIGENCIA_DESACTIVACION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se elimina la restricción de vigencia para --consumos internacionales-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_VIGENCIA_INTER {
            get {
                return ResourceManager.GetString("TC_DEL_VIGENCIA_INTER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se elimina la restricción de vigencia para --compras por Internet-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_VIGENCIA_INTERNET {
            get {
                return ResourceManager.GetString("TC_DEL_VIGENCIA_INTERNET", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se elimina la restricción de vigencia para --consumos locales-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_VIGENCIA_LOCAL {
            get {
                return ResourceManager.GetString("TC_DEL_VIGENCIA_LOCAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se elimina la restricción de vigencia para --VIP-- del {0} al {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_VIGENCIA_VIP {
            get {
                return ResourceManager.GetString("TC_DEL_VIGENCIA_VIP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to VIP --inactiva--&lt;br/&gt;.
        /// </summary>
        internal static string TC_DEL_VIP {
            get {
                return ResourceManager.GetString("TC_DEL_VIP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aviso Legal: Se advierte al usuario que la presente restricción en moneda dólares, utiliza como referencia el tipo de cambio de compra del Banco Central de Costa Rica, del día en el que se realice el consumo. Lo anterior, podrá representar variaciones en el monto disponible para consumos en colones en virtud del tipo de cambio vigente, lo cual es aceptado por el usuario al configurar la restricción en el panel de control, liberando expresamente a IMPESA de cualquier reclamo o responsabilidad.&lt;br/&gt;&lt;br/&gt;.
        /// </summary>
        internal static string TC_DESCARGO_RESPONSABILIDAD {
            get {
                return ResourceManager.GetString("TC_DESCARGO_RESPONSABILIDAD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Totalmente Restringida &lt;br/&gt;.
        /// </summary>
        internal static string TC_TOTAL_RESTRINGIDA {
            get {
                return ResourceManager.GetString("TC_TOTAL_RESTRINGIDA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Horario modificado para {0}. &lt;ul&gt;&lt;li&gt;Valores anteriores: de las {1} a las {2}&lt;/li&gt;&lt;li&gt;Valores nuevos: de las {3} a las {4}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_HORARIOS {
            get {
                return ResourceManager.GetString("TC_UP_HORARIOS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica el límite de --adelantos de efectivo-- &lt;ul&gt;&lt;li&gt;Valores anteriores: {6}{1} {0} y cantidad {4}&lt;/li&gt;&lt;li&gt;Valores nuevos: {6}{3} {2} y cantidad {5}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_LIMITE_AVANCE {
            get {
                return ResourceManager.GetString("TC_UP_LIMITE_AVANCE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica el límite de --adelantos de efectivo-- &lt;ul&gt;&lt;li&gt;Valores anteriores: Lim.  {0} de consumo  {10}{1} y saldo: {10}{6}; Cant.  {0} de consumos {4} y saldo {8}&lt;/li&gt;&lt;li&gt;Valores nuevos:Lim.  {2} de consumo  {10}{3} y saldo: {10}{7}; Cant.  {2} de consumos {5} y saldo {9}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_LIMITE_AVANCE_NEW {
            get {
                return ResourceManager.GetString("TC_UP_LIMITE_AVANCE_NEW", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica el límite de --compras por Internet-- &lt;ul&gt;&lt;li&gt;Valores anteriores: {6}{1} {0} y cantidad {4}&lt;/li&gt;&lt;li&gt;Valores nuevos: {6}{3} {2} y cantidad {5}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_LIMITE_INTERNET {
            get {
                return ResourceManager.GetString("TC_UP_LIMITE_INTERNET", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica el límite de --compras por Internet-- &lt;ul&gt;&lt;li&gt;Valores anteriores: Lim.  {0} de consumo  {10}{1} y saldo: {10}{6}; Cant.  {0} de consumos {4} y saldo {8}&lt;/li&gt;&lt;li&gt;Valores nuevos:Lim.  {2} de consumo  {10}{3} y saldo: {10}{7}; Cant.  {2} de consumos {5} y saldo {9}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_LIMITE_INTERNET_NEW {
            get {
                return ResourceManager.GetString("TC_UP_LIMITE_INTERNET_NEW", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comercio modificado: {0}. &lt;ul&gt; &lt;li&gt;Valores anteriores: Lim. {5} de consumo {11}{1} y saldo: {11}{7}; cant. {5} de consumos {2} y saldo: {8}&lt;/li&gt;&lt;li&gt; Valores nuevos: Lim. {6} de consumo {11}{3} y saldo: {11}{9}; cant. {6} de consumos {4} y saldo: {10}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_MCC {
            get {
                return ResourceManager.GetString("TC_UP_MCC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica la restricción de región {0} y país {1}&lt;br/&gt;.
        /// </summary>
        internal static string TC_UP_REGIONPAIS {
            get {
                return ResourceManager.GetString("TC_UP_REGIONPAIS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica la restricción de vigencia para --adelantos de efectivo-- &lt;ul&gt;&lt;li&gt;Valores anteriores: del {0} al {1}&lt;/li&gt;&lt;li&gt;Valores nuevos: del {2} al {3}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_VIGENCIA_ADELANTO {
            get {
                return ResourceManager.GetString("TC_UP_VIGENCIA_ADELANTO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica la restricción de vigencia para --Desactivación temporal de restricciones-- &lt;ul&gt;&lt;li&gt;Valores anteriores: del {0} al {1}&lt;/li&gt;&lt;li&gt;Valores nuevos: del {2} al {3}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_VIGENCIA_DESACTIVACION {
            get {
                return ResourceManager.GetString("TC_UP_VIGENCIA_DESACTIVACION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica la restricción de vigencia para --consumos internacionales-- &lt;ul&gt;&lt;li&gt;Valores anteriores: del {0} al {1}&lt;/li&gt;&lt;li&gt;Valores nuevos: del {2} al {3}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_VIGENCIA_INTER {
            get {
                return ResourceManager.GetString("TC_UP_VIGENCIA_INTER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica la restricción de vigencia para --compras por Internet-- &lt;ul&gt;&lt;li&gt;Valores anteriores: del {0} al {1}&lt;/li&gt;&lt;li&gt;Valores nuevos: del {2} al {3}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_VIGENCIA_INTERNET {
            get {
                return ResourceManager.GetString("TC_UP_VIGENCIA_INTERNET", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica la restricción de vigencia para --consumos locales-- &lt;ul&gt;&lt;li&gt;Valores anteriores: del {0} al {1}&lt;/li&gt;&lt;li&gt;Valores nuevos: del {2} al {3}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_VIGENCIA_LOCAL {
            get {
                return ResourceManager.GetString("TC_UP_VIGENCIA_LOCAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se modifica la restricción de vigencia para --VIP-- &lt;ul&gt;&lt;li&gt;Valores anteriores: del {0} al {1}&lt;/li&gt;&lt;li&gt;Valores nuevos: actualmente del {2} al {3}&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string TC_UP_VIGENCIA_VIP {
            get {
                return ResourceManager.GetString("TC_UP_VIGENCIA_VIP", resourceCulture);
            }
        }
    }
}

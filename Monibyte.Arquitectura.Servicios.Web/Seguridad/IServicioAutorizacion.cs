﻿using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Monibyte.Arquitectura.Servicios.Web.Seguridad
{
    [ServiceContract]
    public interface IServicioAutorizacion : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerAutorizaciones")]
        Transporte ObtenerAutorizaciones(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerAutorizacionesCount")]
        Transporte ObtenerAutorizacionesCount(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "RechazarAutorizacion")]
        Transporte RechazarAutorizacion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "AnularAutorizacion")]
        Transporte AnularAutorizacion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtieneAutorizacionPendiente")]
        Transporte ObtieneAutorizacionPendiente(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ExisteDependenciaPendiente",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        bool ExisteDependenciaPendiente(int idTipoAutorizacion, string numReferencia);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "EsNuevoGrupo",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        bool EsNuevoGrupo(string numReferencia);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "PermiteEditar",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        bool PermiteEditar(string numReferencia);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ObtieneObervacion",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        string ObtieneObervacion(int idTipoAutorizacion, string numReferencia);
    }
}

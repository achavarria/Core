﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Generales.Repositorios
{
    public class RepositorioCatalogo : RepositorioMnb<GL_Catalogo>, IRepositorioCatalogo
    {
        IFuncionesGenerales _fGenerales;
        public RepositorioCatalogo(
            IUnidadTbjoMonibyte unidadTbjo,
            IFuncionesGenerales fGenerales)
            : base(unidadTbjo)
        {
            _fGenerales = fGenerales;
        }

        public IQueryable<GL_Catalogo> List(string list)
        {
            var criteria = new Criteria<GL_Catalogo>();
            criteria.And(c => c.Lista == list);
            return List(criteria);
        }

        public IQueryable<GL_Catalogo> List(Criteria<GL_Catalogo> criteria)
        {
            return this.Table
                .Where(criteria.Satisfecho())
                .Select(c => new GL_CatalogoWrapper
                {
                    IdCatalogo = c.IdCatalogo,
                    Lista = c.Lista,
                    Codigo = c.Codigo,
                    IdEstado = c.IdEstado,
                    Orden = c.Orden,

                    Descripcion = _fGenerales.Traducir(c.Descripcion, c.Lista, c.IdCatalogo,
                        Context.Lang.Id, 0, Context.DefaultLang.Id),
                    Referencia1 = c.Referencia1,
                    Referencia2 = c.Referencia2,
                    Referencia3 = c.Referencia3,
                    Referencia4 = c.Referencia4,
                    Referencia5 = c.Referencia5,
                    FecInclusionAud = c.FecInclusionAud,
                    IdUsuarioIncluyeAud = c.IdUsuarioIncluyeAud,
                    FecActualizaAud = c.FecActualizaAud,
                    IdUsuarioActualizaAud = c.IdUsuarioActualizaAud
                });
        }

        public List<Cplx_GL_Catalogo> ListSp(string list, int? value = null)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", (object)list ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p2", Context.Lang.Id));
            listaParams.Add(new SqlParameter("p3", (object)value ?? DBNull.Value));
            return (UnidadTbjo as ISql).ExecuteQuery<Cplx_GL_Catalogo>
                ("General.P_ObtieneListaCatalogo @p1, @p2, @p3",
                listaParams.ToArray()).ToList();
        }
    }
    class GL_CatalogoWrapper : GL_Catalogo { }
}
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_TablaPagoMap : EntityTypeConfiguration<TC_TablaPago>
    {
        public TC_TablaPagoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdCuota, t.IdFinanciamiento });

            // Properties
            this.Property(t => t.IdCuota)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdFinanciamiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TC_TablaPago", "TCredito");
            this.Property(t => t.IdCuota).HasColumnName("IdCuota");
            this.Property(t => t.IdFinanciamiento).HasColumnName("IdFinanciamiento");
            this.Property(t => t.FecCuota).HasColumnName("FecCuota");
            this.Property(t => t.MonPrincipal).HasColumnName("MonPrincipal");
            this.Property(t => t.MonInteres).HasColumnName("MonInteres");
            this.Property(t => t.TasaInteres).HasColumnName("TasaInteres");
            this.Property(t => t.MonSaldo).HasColumnName("MonSaldo");
            this.Property(t => t.IdCancelada).HasColumnName("IdCancelada");
            this.Property(t => t.FecCancelada).HasColumnName("FecCancelada");
            this.Property(t => t.MonSeguro).HasColumnName("MonSeguro");
            this.Property(t => t.MonPrincipalPagado).HasColumnName("MonPrincipalPagado");
            this.Property(t => t.MonInteresPagado).HasColumnName("MonInteresPagado");
            this.Property(t => t.MonSeguroPagado).HasColumnName("MonSeguroPagado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

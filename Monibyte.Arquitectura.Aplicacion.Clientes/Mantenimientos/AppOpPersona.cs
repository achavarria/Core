﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Clientes;
using System;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public class AppOpPersona : AplicacionBase, IAppOpPersona
    {
        private IAppOpDireccion _appDireccion;
        private IRepositorioUsuario _repUsuario;
        private IAppOpComunicacion _appComunicacion;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<GL_TipoIdentificacion> _repTipoIdentificacion;

        public AppOpPersona(
            IAppOpDireccion appDireccion,
            IRepositorioUsuario repUsuario,
            IAppOpComunicacion appComunicacion,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<GL_TipoIdentificacion> repTipoIdentificacion)
        {
            _appDireccion = appDireccion;
            _repUsuario = repUsuario;
            _appComunicacion = appComunicacion;
            _repPersona = repPersona;
            _repTipoIdentificacion = repTipoIdentificacion;
        }

        public DataResult<PocInfoPersona> Listar(PocInfoPersona filtro, DataRequest request)
        {
            var criteria = new Criteria<CL_Persona>();
            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.NumIdentificacion))
                {
                    criteria.And(m => m.NumIdentificacion.Contains(filtro.NumIdentificacion));
                }
                if (filtro.IdIdentificacion != 0)
                {
                    criteria.And(m => m.IdIdentificacion == filtro.IdIdentificacion);
                }

                if (filtro.IdTipoPersona != 0)
                {
                    criteria.And(m => m.IdTipoPersona == filtro.IdTipoPersona);
                }

                var subCriteria = new Criteria<CL_Persona>();
                if (filtro.IdTipoPersona == (int)EnumTipoPersona.Juridica)
                {
                    if (!string.IsNullOrEmpty(filtro.Nombre)) { subCriteria.And(m => m.NombreCompleto.Contains(filtro.Nombre)); }
                    if (!string.IsNullOrEmpty(filtro.RazonSocial)) { subCriteria.And(m => m.RazonSocial.Contains(filtro.RazonSocial)); }
                }
                else if (filtro.IdTipoPersona == (int)EnumTipoPersona.Fisica)
                {
                    if (!string.IsNullOrEmpty(filtro.Nombre))
                    {
                        subCriteria.And(m => m.PrimerNombre.Contains(filtro.Nombre) ||
                            m.NombreCompleto.Contains(filtro.Nombre));
                    }
                    if (!string.IsNullOrEmpty(filtro.PrimerApellido))
                    {
                        subCriteria.And(m => m.PrimerApellido.Contains(filtro.PrimerApellido));
                    }
                    if (!string.IsNullOrEmpty(filtro.SegundoApellido))
                    {
                        subCriteria.And(m => m.SegundoApellido.Contains(filtro.SegundoApellido));
                    }
                }
                criteria.And(subCriteria.Satisfecho());
            }
            var query =
                from per in _repPersona.SelectBy(criteria)
                join ide in _repTipoIdentificacion.Table on
                    per.IdIdentificacion equals ide.IdIdentificacion
                select new
                {
                    per,
                    tipo = _fGenerales.Traducir(ide.Descripcion, "GL_TIPOIDENTIFICACION",
                        ide.IdIdentificacion, Context.Lang.Id, 0, Context.DefaultLang.Id)
                };
            var resultado = query.ToList()
                .Select(x =>
                {
                    var persona = x.per.ProyectarComo<PocInfoPersona>();
                    persona.TipoIdentificacion = x.tipo;
                    return persona;
                });
            return resultado.ToDataResult(request);
        }

        public PocInfoPersona Obtener(PocInfoPersona filtro)
        {
            var criteria = new Criteria<CL_Persona>();
            var validacion = new ValidationResponse();
            if (filtro.IdPersona == 0 && filtro.NumIdentificacion == null)
            {
                validacion.AddError("Err_0054");
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Error al obtener datos del usuario", validation: validacion);
            }
            else
            {
                if (filtro.IdPersona > 0)
                {
                    criteria.And(m => m.IdPersona == filtro.IdPersona);
                }
                if (!String.IsNullOrEmpty(filtro.NumIdentificacion))
                {
                    criteria.And(m => m.NumIdentificacion == filtro.NumIdentificacion);
                }
                var clpersona = _repPersona.SelectBy(criteria).FirstOrDefault();
                if (clpersona != null)
                {
                    var infoPersona = clpersona.ProyectarComo<PocInfoPersona>();
                    infoPersona.Comunicacion = _appComunicacion.ObtenerPorPersona(clpersona.IdPersona);
                    infoPersona.Direccion = _appDireccion.ObtenerPorPersona(clpersona.IdPersona);
                    return infoPersona;
                }
                return null;
            }
        }

        public void Eliminar(PocInfoPersona registro)
        {
            using (var ts = new TransactionScope())
            {
                var usuario = _repUsuario.Table.FirstOrDefault(x => x.IdPersona == registro.IdPersona);
                var validacion = new ValidationResponse();
                if (usuario != null)
                {
                    validacion.AddError("Err_0070");
                }
                if (!validacion.IsValid)
                {
                    throw new CoreException("Error al eliminar persona", validation: validacion);
                }
                else
                {
                    _appComunicacion.EliminarPorPersona(registro.IdPersona);

                    _appDireccion.EliminarPorPersona(registro.IdPersona);

                    _repPersona.DeleteOn(x => x.IdPersona == registro.IdPersona);
                    _repPersona.UnidadTbjo.Save();
                }
                ts.Complete();
            }
        }

        public void Insertar(PocInfoPersona registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CL_Persona>();
                if (entrada.IdTipoPersona == (int)EnumTipoPersona.Fisica)
                {
                    entrada.NombreCompleto = entrada.SegundoNombre != null ? string.Format("{0} {1} {2} {3} ",
                        entrada.PrimerNombre, entrada.SegundoNombre, entrada.PrimerApellido, entrada.SegundoApellido)
                     : string.Format("{0} {1} {2} ", entrada.PrimerNombre, entrada.PrimerApellido, entrada.SegundoApellido);
                }
                entrada.IdPersona = _fGenerales.ObtieneSgteSecuencia("SEQ_IdPersona");
                entrada.FecIngreso = DateTime.Today;
                entrada.IdFallecido = (int)EnumIdSiNo.No;
                _repPersona.Insert(entrada);
                _repPersona.UnidadTbjo.Save();
                if (registro.Comunicacion != null && registro.Comunicacion.Any())
                {
                    foreach (var item in registro.Comunicacion)
                    {
                        item.IdPersona = entrada.IdPersona;
                        _appComunicacion.Insertar(item);
                    }
                }
                if (registro.Direccion != null && registro.Direccion.Any())
                {
                    foreach (var item in registro.Direccion)
                    {
                        item.IdPersona = entrada.IdPersona;
                        _appDireccion.Insertar(item);
                    }
                }
                ts.Complete();
            }
        }

        public void Modificar(PocInfoPersona registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CL_Persona>();
                if (entrada.IdIdentificacion == (int)EnumTipoIdentificacion.CedulaIdentidad)
                {
                    entrada.NombreCompleto = entrada.SegundoNombre != null ? string.Format("{0} {1} {2} {3} ",
                        entrada.PrimerNombre, entrada.SegundoNombre, entrada.PrimerApellido, entrada.SegundoApellido)
                     : string.Format("{0} {1} {2} ", entrada.PrimerNombre, entrada.PrimerApellido, entrada.SegundoApellido);
                }
                _repPersona.UpdateExclude(entrada, p => p.IdPersona, p => p.FecInclusionAud, p => p.IdUsuarioIncluyeAud);
                if (registro.Comunicacion != null && registro.Comunicacion.Any())
                {
                    foreach (var item in registro.Comunicacion.OrderBy(x => x.Borrado))
                    {
                        if (item.Borrado == true && item.IdComunicacion > 0)
                        {
                            _appComunicacion.Eliminar(item);
                        }

                        if (item.Borrado == false && item.IdComunicacion > 0)
                        {
                            _appComunicacion.Modificar(item);
                        }
                        else if (item.IdComunicacion <= 0)
                        {
                            _appComunicacion.Insertar(item);
                        }
                    }
                }
                if (registro.Direccion != null && registro.Direccion.Any())
                {
                    foreach (var item in registro.Direccion.OrderBy(x => x.Borrado))
                    {
                        if (item.Borrado == true && item.IdDireccion > 0)
                        {
                            _appDireccion.Eliminar(item);
                        }
                        if (item.Borrado == false && item.IdDireccion > 0)
                        {
                            _appDireccion.Modificar(item);
                        }
                        else if (item.IdDireccion <= 0)
                        {
                            _appDireccion.Insertar(item);
                        }
                    }
                }
                _repPersona.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public bool ComprobarIdentificacion(string numIdentificacion, int idIdentificacion)
        {
            var datos = _repPersona.Table.FirstOrDefault(x =>
                x.NumIdentificacion == numIdentificacion &&
                x.IdIdentificacion == idIdentificacion);
            if (datos != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void AsociarEjecutivo(int[] empresas, int? idEjecutivo)
        {
            using (var ts = new TransactionScope())
            {
                foreach (var id in empresas)
                {
                    _repPersona.UpdateOn(x => x.IdPersona == id,
                        z => new CL_Persona
                        {
                            IdEjecutivo = idEjecutivo
                        });
                }
                _repPersona.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public string ObtenerCorreoPersona(int idPersona, string codUsuario)
        {
            var correos = _appComunicacion.ObtenerPorPersona(idPersona)
                            .Where(x => x.IdTipo == (int)EnumTipoComunicacion.Correo);
            if (correos != null && correos.Any())
            {
                var correoProcesador = correos.FirstOrDefault(
                    x => x.IdOrigenProcesador == (int)EnumIdSiNo.Si);
                if (correoProcesador != null)
                {
                    return correoProcesador.ValComunicacion;
                }
                return correos.First().ValComunicacion;
            }
            else
            {
                var usuario = _repUsuario.ObtenerUsuarioPorCodigo(codUsuario);
                return usuario.Correo;
            }
        }
    }
}


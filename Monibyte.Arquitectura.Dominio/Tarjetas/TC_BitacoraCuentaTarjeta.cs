using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_BitacoraCuentaTarjeta : EntidadBase
    {
        public int IdCuenta { get; set; }
        public string NumCuenta { get; set; }
        public int IdTipoTarjeta { get; set; }
        public System.DateTime FechaVencimiento { get; set; }
        public System.DateTime FecApertura { get; set; }
        public Nullable<System.DateTime> FecActivacion { get; set; }
        public Nullable<System.DateTime> FecBloqueo { get; set; }
        public Nullable<int> IdUsuarioBloqueo { get; set; }
        public Nullable<int> IdUsuarioActivacion { get; set; }
        public int IdUsuarioApertura { get; set; }
        public decimal SaldoLocal { get; set; }
        public int IdEstadoCta { get; set; }
        public Nullable<int> IdEnvioEstCuenta { get; set; }
        public string CuentaClienteLocal { get; set; }
        public Nullable<decimal> PorcAvanceEfectivo { get; set; }
        public decimal SaldoInternacional { get; set; }
        public Nullable<decimal> DispAvanceEfectivoInter { get; set; }
        public decimal MillasAcumuladas { get; set; }
        public Nullable<int> DiasAtrasoLocal { get; set; }
        public Nullable<int> IdDiaCorte { get; set; }
        public Nullable<int> DiasAtrasoInternacional { get; set; }
        public Nullable<decimal> PagoMinLocal { get; set; }
        public Nullable<decimal> PagoMinInternacional { get; set; }
        public Nullable<decimal> PagoContadoLocal { get; set; }
        public Nullable<decimal> PagoContadoInternacional { get; set; }
        public Nullable<System.DateTime> FecUltPagoLocal { get; set; }
        public Nullable<System.DateTime> FecUltPagoInternacional { get; set; }
        public Nullable<System.DateTime> FecPagoContado { get; set; }
        public Nullable<System.DateTime> FecPagoMinimo { get; set; }
        public string CuentaClienteInternacional { get; set; }
        public decimal TasaInteresLocal { get; set; }
        public decimal TasaInteresInternacional { get; set; }
        public decimal TasaIntMoraLocal { get; set; }
        public decimal TasaIntMoraInternacional { get; set; }
        public Nullable<decimal> InteresMoratorioLocal { get; set; }
        public Nullable<decimal> InteresMoratorioInternacional { get; set; }
        public decimal TasaIntCJLocal { get; set; }
        public Nullable<decimal> TasaIntCJInternacional { get; set; }
        public int IdCalificacionRiesgo { get; set; }
        public decimal PlazoMeses { get; set; }
        public Nullable<int> IdTarjeta { get; set; }
        public int IdBitacora { get; set; }
        public Nullable<int> IdTipoPlastico { get; set; }
        public string NombreImpreso { get; set; }
        public string NumTarjeta { get; set; }
        public Nullable<int> IdRelacion { get; set; }
        public Nullable<decimal> LimiteCreditoInter { get; set; }
        public Nullable<int> IdEstadoTarjeta { get; set; }
        public Nullable<System.DateTime> FecEntrega { get; set; }
        public Nullable<int> IdUsuarioEntrega { get; set; }
        public Nullable<System.DateTime> FecRenovacion { get; set; }
        public Nullable<int> IdUsuarioRenueva { get; set; }
        public Nullable<System.DateTime> FecReposicion { get; set; }
        public Nullable<int> IdUsuarioRepone { get; set; }
        public Nullable<int> IdTarjetaAnterior { get; set; }
        public Nullable<int> IdTipoReposicion { get; set; }
        public decimal MonDisponibleLocal { get; set; }
        public decimal MonDisponibleInter { get; set; }
        public Nullable<decimal> MonCargosBonifLocal { get; set; }
        public Nullable<decimal> MonCargosBonifInter { get; set; }
        public Nullable<decimal> MonCreditosLocal { get; set; }
        public Nullable<decimal> MonCreditosInter { get; set; }
        public Nullable<decimal> SaldoTotalCorteLocal { get; set; }
        public Nullable<decimal> SaldoTotalCorteInter { get; set; }
        public Nullable<decimal> DisponibleLocalTarjeta { get; set; }
        public Nullable<decimal> DisponibleInterTarjeta { get; set; }
        public Nullable<decimal> DispAdelantoLocalTarjeta { get; set; }
        public Nullable<decimal> DispAdelantoInterTarjeta { get; set; }
        public Nullable<int> CantImpVencidosLocal { get; set; }
        public Nullable<int> CantImpVencidosInter { get; set; }
        public Nullable<decimal> MonImpVencidosLocal { get; set; }
        public Nullable<decimal> MonImpVencidosInter { get; set; }
        public Nullable<decimal> DebitosTransitoLocal { get; set; }
        public Nullable<decimal> DebitosTransitoInter { get; set; }
        public Nullable<decimal> CreditosTransitoLocal { get; set; }
        public Nullable<decimal> CreditosTransitoInter { get; set; }
        public Nullable<decimal> LimiteExtraFinLocal { get; set; }
        public Nullable<decimal> LimiteExtraFinInter { get; set; }
        public Nullable<decimal> SaldoExtraFinLocal { get; set; }
        public Nullable<decimal> SaldoExtraFinInter { get; set; }
        public Nullable<decimal> MonDispExtraFinLocal { get; set; }
        public Nullable<decimal> MonDispExtraFinInter { get; set; }
        public Nullable<System.DateTime> FecUltCorte { get; set; }
        public Nullable<int> IdTipoPoliza { get; set; }
        public string NumPoliza { get; set; }
        public Nullable<decimal> LimiteCreditoLocal { get; set; }
        public Nullable<decimal> DispAvanceEfectivoLocal { get; set; }
        public Nullable<int> IdGrupoEmpresa { get; set; }
        public Nullable<int> IdEmpresa { get; set; }
    }
}

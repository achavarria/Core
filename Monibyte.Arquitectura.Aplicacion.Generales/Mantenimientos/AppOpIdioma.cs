﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Poco.Generales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;




namespace Monibyte.Arquitectura.Aplicacion.Generales.Mantenimientos
{
    public class AppOpIdioma : AplicacionBase, IAppOpIdioma
    {
        private IRepositorioMnb<GL_Idioma> _repIdioma;

        public AppOpIdioma(
            IRepositorioMnb<GL_Idioma> repIdioma)
        {
            _repIdioma = repIdioma;
        }


        public void Eliminar(PocIdioma registro)
        {
            using (var ts = new TransactionScope())
            {
                _repIdioma.DeleteOn(p => p.IdIdioma == registro.IdIdioma);
                _repIdioma.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocIdioma registro)
        {
            var validacion = Validar(registro, true);
            if (validacion.IsValid)
            {
                using (var ts = new TransactionScope())
                {
                    var entrada = registro.ProyectarComo<GL_Idioma>();
                    entrada.IdIdioma = _fGenerales.ObtieneSgteSecuencia("SEQ_IdIdioma");
                    entrada.Idioma = registro.Idioma;
                    entrada.Abreviatura = registro.Abreviatura;
                    entrada.CodCultura = registro.CodCultura;
                    entrada.IdEstado = (int)EnumIdEstado.Activo;

                    _repIdioma.Insert(entrada);
                    _repIdioma.UnidadTbjo.Save();
                    ts.Complete();
                }
            }
            else
            {
                var msj = string.Format("error al insertar {0}", typeof(PocIdioma));
                throw new CoreException(msj, validation: validacion);
            }
        }

        public void Modificar(PocIdioma registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<GL_Idioma>();
                    _repIdioma.UpdateExclude(entrada,
                        ex => ex.IdUsuarioIncluyeAud,
                        ex => ex.FecInclusionAud);
                    _repIdioma.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocIdioma));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public PocIdioma ObtenerIdiomas(int registro)
        {
            var idiomas = _repIdioma.SelectById(registro);
            return idiomas.ProyectarComo<PocIdioma>();
        }

        public List<PocIdioma> ListarIdioma(PocIdioma registro = null)
        {
            var criteria = new Criteria<GL_Idioma>();
            if (registro.IdIdioma > 0)
            { 
                criteria.And(m => m.IdIdioma == registro.IdIdioma);
            }
            if (!string.IsNullOrEmpty(registro.Abreviatura))
            {
                criteria.And(x => x.Abreviatura.Contains(registro.Abreviatura));
            }
            if (!string.IsNullOrEmpty(registro.CodCultura))
            {
                criteria.And(x => x.CodCultura.Contains(registro.CodCultura));
            }
            if (!string.IsNullOrEmpty(registro.Idioma))
            {
                criteria.And(x => x.Idioma.Contains(registro.Idioma));
            }
            var unidades = _repIdioma.SelectBy(criteria);
            var resultado = unidades.ProyectarComo<PocIdioma>();
            return resultado;
        }

        private ValidationResponse Validar(PocIdioma registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (inserta)
            {
                if (String.IsNullOrEmpty(registro.Abreviatura)) { validacion.AddError("Err_0106"); }
                if (String.IsNullOrEmpty(registro.CodCultura)) { validacion.AddError("Err_0107"); }
                if (String.IsNullOrEmpty(registro.Idioma)) { validacion.AddError("Err_0108"); }
            }
            else
            {
                if (registro.IdIdioma <= 0) { validacion.AddError("Err_0078"); }
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }

       

    }
}

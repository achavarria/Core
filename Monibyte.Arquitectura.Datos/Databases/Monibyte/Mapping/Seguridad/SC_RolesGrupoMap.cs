using Monibyte.Arquitectura.Dominio.Seguridad;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Seguridad
{
    public class SC_RolesGrupoMap : EntityTypeConfiguration<SC_RolesGrupo>
    {
        public SC_RolesGrupoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdGrupo, t.IdRole });

            // Properties
            this.Property(t => t.IdGrupo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdRole)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("SC_RolesGrupo", "Seguridad");
            this.Property(t => t.IdGrupo).HasColumnName("IdGrupo");
            this.Property(t => t.IdRole).HasColumnName("IdRole");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

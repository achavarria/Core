using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tesoreria
{
    public partial class TS_TipoDeCambioH : EntidadBase
    {
        public int IdCambio { get; set; }
        public int IdMoneda { get; set; }
        public int IdTipoCambio { get; set; }
        public Nullable<System.DateTime> FecCambio { get; set; }
        public Nullable<decimal> MonTipoCambio { get; set; }
        public int IdEstado { get; set; }
        public Nullable<int> IdMonedaConversion { get; set; }
        public System.DateTime HoraCambio { get; set; }
        public System.DateTime FecTrasladoHist { get; set; }
        public int IdUsuarioTraslado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdEntidadFinanciera { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Autorizador
{
    public partial class MB_TipoTarjeta : EntidadBaseCtrl
    {
        public int IdTipoTarjeta { get; set; }
        public int Bin { get; set; }
        public int IdEmisor { get; set; }
        public string Descripcion { get; set; }
        public string CodMoneda { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdMarca { get; set; }
    }
}

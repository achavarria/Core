﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Tesoreria;
    using Monibyte.Arquitectura.Datos.Tesoreria.Repositorios;
    using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;

    public class ContenedorUnityTesoreria
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {            
            #region Servicios
            _unityContainer.RegisterType<IAppTesoreria, AppTesoreria>();
            _unityContainer.RegisterType<IAppOpTipoCambio, AppOpTipoCambio>();
            #endregion

            #region Repositorios
            _unityContainer.RegisterType<IRepositorioMoneda, RepositorioMoneda>();
            _unityContainer.RegisterType<IFuncionesTesoreria, FuncionesTesoreria>();
            _unityContainer.RegisterType<IServiciosEconomicos, ServiciosEconomicos>();
            _unityContainer.RegisterType<IRepositorioTipoCambio, RepositorioTipoCambio>();
            #endregion
           
        }
    }
}

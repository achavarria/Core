﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Poco;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Gestiones
{
    public class AppReportes : AplicacionBase, IAppReportes
    {
        private IAppGenerales _appGenerales;
        private IAppGestiones _appGestiones;
        private IReporteadorDinamico _repReporteador;
        private IRepositorioReportServer _repReportServer;
        private IRepositorioMnb<GL_ParametroEmpresa> _repParamEmpresa;

        public AppReportes(
            IAppGenerales appGenerales,
            IAppGestiones appGestiones,
            IReporteadorDinamico repReporteador,
            IRepositorioReportServer repReportServer,
            IRepositorioMnb<GL_ParametroEmpresa> repParamEmpresa)
        {
            _appGenerales = appGenerales;
            _appGestiones = appGestiones;
            _repReporteador = repReporteador;
            _repReportServer = repReportServer;
            _repParamEmpresa = repParamEmpresa;
        }
        public byte[] ObtenerGestionLiquidacion(string json, int idGestion,
            int idEmpresa, int idMonedaLocal, int idMonedaInter, int idSubReporte)
        {
            var contextoMovs = (int)EnumContexto.TC_MOVIMIENTO;
            var contextoEfec = (int)EnumContexto.EF_MOVIMIENTO;
            var parameters = new List<ReportParam>();
            parameters.Add(new ReportParam { Name = "pLista", Value = json });
            parameters.Add(new ReportParam { Name = "idSubReporte", Value = idSubReporte.ToString() });
            parameters.Add(new ReportParam { Name = "idGestion", Value = idGestion.ToString() });
            parameters.Add(new ReportParam { Name = "idEmpresa", Value = idEmpresa.ToString() });
            parameters.Add(new ReportParam { Name = "idMonedaLocal", Value = idMonedaLocal.ToString() });
            parameters.Add(new ReportParam { Name = "idMonedaInter", Value = idMonedaInter.ToString() });
            parameters.Add(new ReportParam { Name = "contextoMovimiento", Value = contextoMovs.ToString() });
            parameters.Add(new ReportParam { Name = "contextoEfectivo", Value = contextoEfec.ToString() });
            var logoEmpresa = string.Format("/Resources/LogosEmpresariales/Logo-{0}.gif", idEmpresa.ToString());
            if (!_repReportServer.RemoteFileExists(logoEmpresa))
            {
                logoEmpresa = string.Format("/Resources/logocia{0}.gif", InfoSesion.Info.IdCompania);
            }
            parameters.Add(new ReportParam { Name = "logoEmpresa", Value = logoEmpresa });
            var resultado = _repReportServer.ExportPdf("/Reports/Gestiones/LiquidacionDeGastos/r-LiqGto", parameters.ToArray());

            return resultado.Output;
        }

        public byte[] ObtenerGestionKilometraje(string Json)
        {
            var idEmpresa = InfoSesion.Info.IdEmpresa;
            var parameters = new List<ReportParam>();
            parameters.Add(new ReportParam { Name = "pLista", Value = Json });
            parameters.Add(new ReportParam { Name = "idEmpresa", Value = idEmpresa.ToString() });
            var logoEmpresa = string.Format("/Resources/LogosEmpresariales/Logo-{0}.gif", idEmpresa.ToString());
            if (!_repReportServer.RemoteFileExists(logoEmpresa))
            {
                logoEmpresa = string.Format("/Resources/logocia{0}.gif", InfoSesion.Info.IdCompania);
            }
            parameters.Add(new ReportParam { Name = "logoEmpresa", Value = logoEmpresa });
            var resultado = _repReportServer.ExportPdf("/Reports/Gestiones/LiquidacionDeKilometraje/r-LiqKm", parameters.ToArray());

            return resultado.Output;
        }

        public byte[] ObtenerReporteLiquidacion(int idGestion, int? idReporte)
        {
            if (idReporte.HasValue)
            {
                var config = new ReportConfig
                {
                    ReportId = idGestion.ToString(),
                    ReportTitle = "Liquidación de gastos",
                    ReportFormat = "PDF",
                    InsertSignature = true,
                    Landscape = false
                };
                var contextTC = (int)EnumContexto.TC_MOVIMIENTO;
                var contextEF = (int)EnumContexto.EF_MOVIMIENTO;

                var _si = (int)EnumIdSiNo.Si;
                var _no = (int)EnumIdSiNo.No;
                var p1 = new ReportParam { Name = "idReporte", Value = idReporte.ToString() };

                var campos = _repReporteador.ObtenerCamposReporte(idReporte.Value);

                //encabezado de la liquidación
                var encabezadoDTS = new ReportDataSet();
                encabezadoDTS.Columns = campos.Where(x =>
                    x.IdEsEncabezado == _si)
                    .ProyectarComo<ReportColumn>();
                encabezadoDTS.IsDetail = false;
                encabezadoDTS.Name = "DTS01";
                encabezadoDTS.StoreProcedure = true;
                encabezadoDTS.QueryCommand = "General.P_ObtieneQueryReporte";
                var stringJson = new
                {
                    idEmpresa = InfoSesion.Info.IdEmpresa,
                    idGestion = idGestion,
                    idEsEncabezado = _si
                }.ToJson();
                var p2 = new ReportParam { Name = "stringJson", Value = stringJson };
                var encabezadoParams = new List<ReportParam> { p1, p2 };
                encabezadoDTS.Parameters = encabezadoParams;

                //detalle de los movimientos de tarjeta
                var detalleDTS_TC = new ReportDataSet();
                detalleDTS_TC.Columns = campos.Where(x =>
                  x.IdEsEncabezado == _no &&
                  x.IdContexto != contextEF)
                  .ProyectarComo<ReportColumn>();
                detalleDTS_TC.IsDetail = true;
                detalleDTS_TC.Name = "DTS02";
                detalleDTS_TC.StoreProcedure = true;
                detalleDTS_TC.QueryCommand = "General.P_ObtieneQueryReporte";
                stringJson = new
                {
                    idEmpresa = InfoSesion.Info.IdEmpresa,
                    idGestion = idGestion,
                    idEsEncabezado = _no,
                    idContexto = contextEF
                }.ToJson();
                p2 = new ReportParam { Name = "stringJson", Value = stringJson };
                var detalleTcParams = new List<ReportParam> { p1, p2 };
                detalleDTS_TC.Parameters = detalleTcParams;

                //detalle de los movimientos de efectivo
                var detalleDTS_EF = new ReportDataSet();
                detalleDTS_EF.Columns = campos.Where(x =>
                  x.IdEsEncabezado == _no &&
                  x.IdContexto != contextTC)
                  .ProyectarComo<ReportColumn>();
                detalleDTS_EF.IsDetail = true;
                detalleDTS_EF.Name = "DTS03";
                detalleDTS_EF.StoreProcedure = true;
                detalleDTS_EF.QueryCommand = "General.P_ObtieneQueryReporte";
                stringJson = new
                {
                    idEmpresa = InfoSesion.Info.IdEmpresa,
                    idGestion = idGestion,
                    idEsEncabezado = _no,
                    idContexto = contextTC
                }.ToJson();
                p2 = new ReportParam { Name = "stringJson", Value = stringJson };
                var detalleEfParams = new List<ReportParam> { p1, p2 };
                detalleDTS_EF.Parameters = detalleEfParams;

                var dataSets = new List<ReportDataSet>();
                dataSets.Add(encabezadoDTS);
                dataSets.Add(detalleDTS_TC);
                dataSets.Add(detalleDTS_EF);
                var report = _repReporteador.CrearReporte(config, dataSets);
                return report.Output;
            }
            else
            {
                var json = _appGestiones.ObtenerValorParametro(idGestion, false);
                var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
                var entidad = _appGenerales.ObtieneEntidad(idEntidad);
                var gestion = _appGestiones.ObtenerGestion(idGestion);
                if (gestion != null)
                {
                    var exist = _repParamEmpresa.Table.Any(x => x.IdEmpresa == InfoSesion.Info.IdEmpresa &&
                        (x.IdParametroInterno == (int)EnumParametrosMONIBYTE.CentroCosto ||
                        x.IdParametroInterno == (int)EnumParametrosMONIBYTE.CuentaContable));

                    return ObtenerGestionLiquidacion(json, idGestion,
                        InfoSesion.Info.IdEmpresa, entidad.MonedaLocal.IdMoneda,
                        entidad.MonedaInternacional.IdMoneda, exist ? (int)EnumSubReporte.SubReporte2 :
                        (int)EnumSubReporte.SubReporte1);
                }
                else
                {
                    return null;
                }
            }
        }
    }
}

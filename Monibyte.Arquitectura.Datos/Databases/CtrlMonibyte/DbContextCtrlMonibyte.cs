using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte
{
    public partial class DbContextCtrlMonibyte : DbContextBase
    {
        public DbContextCtrlMonibyte()
            : base("name=CtrlMonibyteConn")
        {
            Database.SetInitializer<DbContextCtrlMonibyte>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            CreateContextModelCtrlMB(modelBuilder);
        }
    }
}

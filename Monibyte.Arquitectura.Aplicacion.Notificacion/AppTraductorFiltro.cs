﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Notificacion;
using System;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public abstract class AppTraductorFiltro : AplicacionBase
    {
        protected IRepositorioCatalogo _repCatalogo;
        protected IRepositorioCampoContexto _repCamposContexto;

        public AppTraductorFiltro()
        {
            _repCatalogo = GestorUnity.CargarUnidad<IRepositorioCatalogo>();
            _repCamposContexto = GestorUnity.CargarUnidad<IRepositorioCampoContexto>();
        }

        protected PocDetalleFiltro Traducir(string strdetalle)
        {
            Func<PocDetalleFiltro, PocDetalleFiltro> _func = null;
            _func = delegate(PocDetalleFiltro response)
            {
                if (response == null) { return null; }
                if (response.IdCampo.HasValue)
                {
                    var criteria = new Criteria<GL_CamposContexto>();
                    criteria.And(x => x.IdCampo == response.IdCampo);
                    var param = _repCamposContexto.List(criteria).First();
                    response.NombreCampo = param.SQLOrigen ?? param.Campo;
                    response.Descripcion = param.Descripcion;
                    response.IdTipoCampo = param.IdTipo;
                    response.Visible = param.Referencia1 != null && param.Referencia1.Contains("P");
                    if (param.SQLOrigen != null && (param.IdTipo == (int)EnumTipoParamEmpresa.Lista
                        || param.IdTipo == (int)EnumTipoParamEmpresa.ListaPredefinida))
                    {
                        var x = _fGenerales.ObtenerListaSql(param.SQLOrigen);
                        response.Coll = x.ProyectarComo<PocListaSql>();
                    }
                }
                if (response.IdOperadorRel != null)
                {
                    var rel = _repCatalogo.SelectById(response.IdOperadorRel);
                    response.IdOperadorRel = rel.IdCatalogo;
                    response.OperadorRel = rel.Codigo;
                }
                if (response.IdOperadorLog != null)
                {
                    var log = _repCatalogo.SelectById(response.IdOperadorLog);
                    response.IdOperadorLog = log.IdCatalogo;
                    response.OperadorLog = log.Codigo;
                }
                response.IdFiltro = Guid.NewGuid();
                return Iterar(response, _func, true);
            };
            var detalle = strdetalle.FromJson<PocDetalleFiltro>();
            return _func(detalle);
        }

        protected string Traducir(PocDetalleFiltro detalle)
        {
            Func<PocDetalleFiltro, PocDetalleFiltro> _func = null;

            _func = delegate(PocDetalleFiltro response)
            {
                if (response == null) { return null; }
                if (response.IdCampo.HasValue)
                {
                    var valor1 = response.Valor1;
                    var valor2 = response.Valor2;
                    if (response.IdTipoCampo == (int)EnumTipoParamEmpresa.Monto ||
                        response.IdTipoCampo == (int)EnumTipoParamEmpresa.Cantidad)
                    {
                        response.Valor1 = valor1 != null ? Convert.ToDouble(valor1) as double? : null;
                        response.Valor2 = valor2 != null ? Convert.ToDouble(valor2) as double? : null;
                    }
                    else if (response.IdTipoCampo == (int)EnumTipoParamEmpresa.Fecha)
                    {
                    }
                }
                response.Coll = null;
                response.IdFiltro = null;
                response.NombreCampo = null;
                response.Descripcion = null;
                response.IdTipoCampo = null;
                response.OperadorLog = null;
                response.OperadorRel = null;
                return Iterar(response, _func, false);
            };
            detalle = _func(detalle);
            return detalle.ToJson();
        }

        private PocDetalleFiltro Iterar(PocDetalleFiltro detalle,
            Func<PocDetalleFiltro, PocDetalleFiltro> callback, bool evalMisc)
        {
            if (detalle.Comp != null)
            {
                if (detalle.Comp.Count > 0)
                {
                    using (var num = detalle.Comp.GetEnumerator())
                    {
                        while (num.MoveNext())
                        {
                            var cp = num.Current;
                            cp = callback(cp);
                        }
                    }
                }
                else
                {
                    detalle.Comp = null;
                }
            }
            detalle.Visible = detalle.Visible == true || detalle.Comp
                != null && detalle.Comp.Any(x => x.Visible == true);
            detalle.Visible = evalMisc ? detalle.Visible : null;

            return detalle;
        }

        protected string ToSqlStr(PocDetalleFiltro filtro)
        {
            if (filtro == null) return null;
            var sb = new System.Text.StringBuilder();
            sb.Append("(");
            if (filtro.NombreCampo != null)
            {
                sb.Append(filtro.NombreCampo);
                sb.Append(" ");
                sb.Append(filtro.OperadorRel);
                if (filtro.Valor1 != null)
                {
                    sb.Append(" ");
                    if (filtro.IdTipoCampo == (int)EnumTipoParamEmpresa.Monto ||
                        filtro.IdTipoCampo == (int)EnumTipoParamEmpresa.Cantidad)
                    {
                        sb.Append(filtro.Valor1.ToString());
                    }
                    else
                    {
                        sb.Append("'" + filtro.Valor1.ToString() + "'");
                    }
                }
                if (filtro.OperadorRel == "between")
                {
                    sb.Append(" AND ");
                    sb.Append(filtro.Valor2);
                }
            }
            if (filtro.Comp != null && filtro.Comp.Any())
            {
                using (var enumerator = filtro.Comp.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        var current = enumerator.Current;
                        if (!string.IsNullOrEmpty(current.OperadorLog))
                        {
                            sb.Append(" ");
                            sb.Append(current.OperadorLog);
                            sb.Append(" ");
                        }
                        sb.Append(ToSqlStr(current));
                    }
                }                
            }
            sb.Append(")");
            return sb.ToString();
        }
    }
}
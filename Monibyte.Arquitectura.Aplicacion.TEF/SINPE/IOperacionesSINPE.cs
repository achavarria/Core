﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.TEF;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.TEF.SINPE
{
    public interface IOperacionesSINPE : IAplicacionBase
    {
        void RegistraResultadoPagoEntrante(List<PocTransEntranteSINPE> listaTrasacciones, string xmlData);
        DataResult<PocPagoPendiente> ListarPagosPendientes(PocPagoPendiente filtro, DataRequest request);
        List<PocPagoPendiente> ListarPagos(int IdTrasaccion);
        void AplicarPagoPendiente(PocPagoPendiente transSinpe);
        void EliminarPagosPendientes(PocPagoPendiente transSinpe);
        PocEncPagoDirigido ListarPagosAplicados(int IdCuenta);
        List<PocDirigirPago> AplicarPagoDistribuido(List<PocDirigirPago> modelo);
    }
}

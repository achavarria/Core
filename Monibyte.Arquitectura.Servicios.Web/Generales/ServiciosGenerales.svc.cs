﻿using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Aplicacion.Generales.Mantenimientos;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Tesoreria;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Generales
{
    public class ServiciosGenerales : ServicioUnity, IServiciosGenerales
    {
        #region CONTRUCTOR UNITY
        private IParametroSistema _paramSistema;
        private IParametroEmpresa _paramEmpresa;
        private IAppOpSucursal _appSucursal;
        private IAppGenerales _appGenerales;
        private IAppOpIdioma _appIdioma;

        public ServiciosGenerales(IParametroSistema paramSistema,
            IParametroEmpresa paramEmpresa,
            IAppOpSucursal appSucursal,
            IAppGenerales appGenerales,
            IAppOpIdioma appIdioma)
        {
            _paramSistema = paramSistema;
            _paramEmpresa = paramEmpresa;
            _appSucursal = appSucursal;
            _appGenerales = appGenerales;
            _appIdioma = appIdioma;

        }
        #endregion

        public Transporte ConsultarParametroSistema(Transporte transporte)
        {
            var resultado = _paramSistema.ListaParametroSistema();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerParametroSistema(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocParametroSistema>();
            var resultado = _paramSistema.ObtenerParametroSistema(entradaServicio);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte InsertarParametroSistema(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocParametroSistema>();
            _paramSistema.InsertaParametroSistema(entradaServicio);
            transporte.SetDatos(null);
            return transporte;
        }

        public Transporte ActualizarParametroSistema(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocParametroSistema>();
            _paramSistema.ActualizaParametroSistema(entradaServicio);
            transporte.SetDatos(null);
            return transporte;
        }

        public Transporte EliminarParametroSistema(Transporte transporte)
        {
            var entradaServicio = transporte.GetDatos<PocParametroSistema>();
            _paramSistema.EliminaParametroSistema(entradaServicio);
            transporte.SetDatos(null);
            return transporte;
        }

        public Transporte ObtieneEntidad(Transporte transporte)
        {
            var idEntidad = transporte.GetDatos<int>();
            var resultado = _appGenerales.ObtieneEntidad(idEntidad);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaEntidades(Transporte transporte)
        {
            var idTipo = transporte.GetValor<int?>("IDTIPO");
            var resultado = _appGenerales.ListaEntidades(idTipo);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaUnidadesPorEntidad(Transporte transporte)
        {
            var idEntidad = transporte.GetDatos<int>();
            var resultado = _appGenerales.ListaUnidadesPorEntidad(idEntidad);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtienePaisProcesador(Transporte transporte)
        {
            var resultado = _appGenerales.ObtienePaisProcesador();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerParametrosEmpresa(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocFiltroParametroEmpresa>();
            var resultado = _paramEmpresa.ObtenerParametros(entrada);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerListasPadre(Transporte transporte)
        {
            var idParametro = transporte.GetValor<int?>("idParametro");
            var resultado = _paramEmpresa.ObtenerListasPadre(idParametro);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public void InsertarParametrosEmpresa(Transporte transporte)
        {
            var parametros = transporte.GetDatos<List<PocParametroEmpresa>>();
            int idContexto = transporte.GetValor<int>("idContexto");
            _paramEmpresa.MantenimientoParametrosAlContexto(parametros, idContexto);
        }

        public void EliminarParametroDelContexto(Transporte transporte)
        {
            var parametro = transporte.GetDatos<PocParametroEmpresa>();
            int idContexto = transporte.GetValor<int>("idContexto");
            _paramEmpresa.EliminarParametroDelContexto(parametro, idContexto);
        }

        #region Servicios de mantenimiento de sucursales
        public Transporte IncluirSucursal(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocSucursal>();
            _appSucursal.Insertar(entrada);
            var resultado = entrada.IdSucursal;
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaSucursal(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocSucursal>();
            _appSucursal.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaSucursal(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocSucursal>();
            _appSucursal.Modificar(entrada);
            return transporte;
        }

        public Transporte ListarSucursales(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocSucursal>();
            var resultado = _appSucursal.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerSucursales(Transporte transporte)
        {
            var resultado = _appSucursal.ObtenerSucursales();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerSucursal(Transporte transporte)
        {
            var idSucursal = transporte.GetValor<int>("idSucursal");
            var resultado = _appSucursal.ObtenerSucursal(idSucursal);
            transporte.SetDatos(resultado);
            return transporte;
        }
        #endregion

        public Transporte ObtenerReportesEmpresa(Transporte transporte)
        {
            var contextos = transporte.GetValor<int?[]>("contextos");
            var idTipoReporte = transporte.GetValor<int?>("idTipoReporte");
            var soloReportesConCampos = transporte.GetValor<bool>("filtrar");
            var incluirReporteBase = transporte.GetValor<bool>("incluirReporteBase");
            var resultado = _paramEmpresa.ObtenerReportesEmpresa(contextos,
                idTipoReporte, soloReportesConCampos, incluirReporteBase);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerCamposReporte(Transporte transporte)
        {
            var idReporte = transporte.GetValor<int>("idReporte");
            var mostrarTodos = transporte.GetValor<bool>("mostrarTodos");
            var mostrarPersonalizados = transporte.GetValor<bool>("mostrarPersonalizados");
            var resultado = _paramEmpresa.ObtenerCamposReporte(idReporte, mostrarTodos, mostrarPersonalizados);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public void GuardarCamposReporte(Transporte transporte)
        {
            var reporte = transporte.GetDatos<PocReporteGeneral>();
            _paramEmpresa.GuardarCamposReporte(reporte);
        }

        //public Transporte ListaParametrosEmpresa(Transporte transporte)
        //{
        //    var resultado = _paramEmpresa.ListaParametrosEmpresa();
        //    transporte.SetDatos(resultado);
        //    return transporte;
        //}

        public Transporte IncluirReporte(Transporte transporte)
        {
            var nomReporte = transporte.GetValor<string>("nombreReporte");
            var idContexto = transporte.GetValor<int>("idContexto");
            var incluyeHeader = transporte.GetValor<int>("incluyeHeader");
            var reporte = transporte.GetDatos<PocReporteBase>();
            var result = _paramEmpresa.IncluirReporte(reporte);
            transporte.SetDatos(result);
            return transporte;
        }

        public Transporte ObtenerReporte(Transporte transporte)
        {
            var idReporte = transporte.GetValor<int>("idReporte");
            var resultado = _paramEmpresa.ObtenerReporte(idReporte);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ActualizarIdReporteTC(Transporte transporte)
        {
            var idReporte = transporte.GetValor<int>("idReporte");
            _paramEmpresa.ActualizarIdReporteTC(idReporte);
            return transporte;
        }

        public Transporte ObtieneParametroCompania(Transporte transporte)
        {
            var idParametro = transporte.GetValor<string>("idParametro");
            var idCompania = transporte.GetValor<int>("idCompania");
            var resultado = _appGenerales.ObtieneParametroCompania(idParametro, idCompania);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListasPredefinidas(Transporte transporte)
        {
            var resultado = _appGenerales.ListasPredefinidas();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerParametroEmpresaLista(Transporte transporte)
        {
            var idParametroEmpresa = transporte.GetValor<int>("idParametroEmpresa");
            var resultado = _paramEmpresa.ObtenerParametroEmpresaLista(idParametroEmpresa);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerListaPredefinida(Transporte transporte)
        {
            var idParametroInterno = transporte.GetValor<int>("idParametroInterno");
            var resultado = _paramEmpresa.ObtenerListaPredefinida(idParametroInterno);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminarReporte(Transporte transporte)
        {
            var idReporte = transporte.GetValor<int>("idReporte");
            _appGenerales.EliminarReporte(idReporte);
            return transporte;
        }

        public Transporte ObtenerPosiblesDependencias(Transporte transporte)
        {
            var filtro = transporte.GetDatos<PocFiltroParametroEmpresa>();
            var idParametro = transporte.GetValor<int>("idParametro");
            var resultado = _paramEmpresa.ObtenerPosiblesDependencias(filtro, idParametro);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte GuardarParametroCompania(Transporte transporte)
        {
            var idParametro = transporte.GetValor<string>("idParametro");
            var idCompania = transporte.GetValor<int>("idCompania");
            var valorParametro = transporte.GetValor<string>("valorParametro");
            _appGenerales.GuardarParametroCompania(idParametro, idCompania, valorParametro);
            return transporte;
        }

        public Transporte ObtenerCamposContexto(Transporte transporte)
        {
            var idContexto = transporte.GetValor<int>("IDCONTEXTO");
            var result = _appGenerales.ObtenerCamposContexto(idContexto);
            transporte.SetDatos(result);
            return transporte;
        }

        public void InsertarIdioma(PocIdioma registro)
        {
            _appIdioma.Insertar(registro);
        }

        public void EliminarIdioma(PocIdioma registro)
        {
            _appIdioma.Eliminar(registro);
        }

        public void ModificarIdioma(PocIdioma registro)
        {
            _appIdioma.Modificar(registro);
        }

        public PocIdioma ObtenerIdiomas(int registro)
        {

            return _appIdioma.ObtenerIdiomas(registro);
        }


        public List<PocIdioma> ListarIdioma(PocIdioma registro)
        {
            return _appIdioma.ListarIdioma(registro);
        }

        public PocEntidad ObtenerEntidadFinanciera()
        {
            var resultado = _appGenerales.ObtenerEntidadFinanciera();
            return resultado;
        }
        public PocConfiguracionFinanciera ObtenerInformacionFinanciera()
        {
            var resultado = _appGenerales.ObtenerInformacionFinanciera();
            return resultado;
        }

    }
}

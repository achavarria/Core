﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Configuracion;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public interface IAppConfiguracionGrps : IAplicacionBase
    {
        PocGruposEmpresa ObtenerGrupoEmpresa(int idGrupo);
        List<PocGruposEmpresa> ObtenerGruposEmpresa(int idEmpresa, int idTipoGrupo);
        int RegistrarConfiguracionGrupo(PocGruposEmpresa entrada);
        int? DuplicarConfiguracionGrupo(PocGrupoDuplica entrada);
        void EliminarConfiguracionGrupo(int idGrupo, int idTipoGrupo);
        //Tarjetas para agrupar
        List<PocTarjetaEmpresa> ObtenerTarjetasPorGrupo(int? idGrupoEmpresa, int idTipoGrupo,
            bool soloAgrupadas = true);
        PocCondicion AgruparTarjetas(List<PocTarjetaEmpresa> entrada, int idGrupoEmpresa, int idTipoGrupo);
        void DesagruparTarjeta(PocAgrupacionTarjetasItem entrada);
        //Administradores de grupo
        List<PocUsuarioEmpresa> ObtenerUsuariosGrupo(int idEmpresa, int idGrupoEmpresa);
        void DefinirAdministradores(int idGrupo, int idTipoGrupo, List<PocUsuarioEmpresa> entrada);
        //Grupos de Liquidación
        void GuardarGrupoOperativo(PocGruposEmpresa entrada);
        PocGruposEmpresa AutorizacionGrupoPendiente(int idGrupo);
        bool EsAutorizadorGrupo(int idUsuario, int idGrupo);
    }
}

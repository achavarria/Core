﻿using System;
using Monibyte.Arquitectura.Poco.Tesoreria;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.Generales
{
    public class PocParametroSistema
    {
        public int IdSistema { get; set; }
        public string IdParametro { get; set; }
        public string ValParametro { get; set; }
        public int IdEstado { get; set; }
        public string Descripcion { get; set; }
        public string Formato { get; set; }
        public int? IdCompania { get; set; }
    }

    public class PocReporte
    {
        public string MimeType { get; set; }
        public string Encoding { get; set; }
        public string Extension { get; set; }
        public byte[] Output { get; set; }
    }

    public class PocParametrosReporte
    {
        public string CodUsuario { get; set; }
        public string NombreCompleto { get; set; }
        public string Pin { get; set; }
        public string Correo { get; set; }
        public string Semilla { get; set; }
        public string Clave { get; set; }
    }

    public class PocCatalogo
    {
        public int IdCatalogo { get; set; }
        public string Lista { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public string Referencia1 { get; set; }
        public string Referencia2 { get; set; }
        public string Referencia4 { get; set; }
        public string Referencia5 { get; set; }
    }

    public class PocTipoIdentificacion
    {
        public int IdIdentificacion { get; set; }
        public string Descripcion { get; set; }
        public int? IdEstado { get; set; }
        public int? IdFormato { get; set; }
    }

    public class PocEntidad
    {
        public int IdEntidad { get; set; }
        public int IdPais { get; set; }
        public int IdPersona { get; set; }
        public string Descripcion { get; set; }
        public PocMoneda MonedaLocal { get; set; }
        public PocMoneda MonedaInternacional { get; set; }
    }

    public class PocUnidadNegocio
    {
        public int IdUnidad { get; set; }
        public int IdEntidad { get; set; }
        public string Descripcion { get; set; }
        public int IdPais { get; set; }
        public int IdEsPrincipal { get; set; }
    }

    public class PocCierresSistema
    {
        public int IdCierre { get; set; }
        public int IdSistema { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int? OrdenEjecucion { get; set; }
        public TimeSpan? HoraEjecucion { get; set; }
        public int? IdDependeDe { get; set; }
        public string EjecutarMetodo { get; set; }
        public DateTime? FecUltCierre { get; set; }
        public int? IdEstadoEjecucion { get; set; }
    }

    public class PocColaImpresion
    {
        public int IdCola { get; set; }
        public string Formato { get; set; }
        public string Contenido { get; set; }
        public string NombreCompleto { get; set; }
        public string NombreEmpresa { get; set; }
        public int? IdEstado { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }
        public string CodUsuario { get; set; }
        public string PIN { get; set; }
        public string Semilla { get; set; }
        public string Clave { get; set; }
        public int? IdTipoPersona { get; set; }
    }

    public class PocFiltroParametroEmpresa
    {
        public int? IdTipo { get; set; }
        public int IdEmpresa { get; set; }
        public int? IdContexto { get; set; }
        public bool ExcluirInactivos { get; set; }
        public List<int> ListaParametros { get; set; }
    }

    public class PocParametroEmpresa
    {
        public bool Importado { get; set; }
        public int IdParametro { get; set; }
        public int IdEmpresa { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int IdTipo { get; set; }
        public int Ascendente { get; set; }
        public int IdOrdenadoPor { get; set; }
        public int IdEsRequerido { get; set; }
        public int? LongitudMin { get; set; }
        public int? LongitudMax { get; set; }
        public int? IdParametroInterno { get; set; }
        public string ValorDefecto { get; set; }
        public int? IdDependeDe { get; set; }
        public int? IdOrden { get; set; }
        public List<PocParametroEmpresaLista> Lista { get; set; }
        public List<int> ListaPadres { get; set; }
        public string DescParametro { get; set; }
    }

    public class PocParametroEmpresaLista
    {
        public int IdParametroEmpresa { get; set; }
        public int? IdParametro { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string CodigoContrapartida { get; set; }
        public string[] CodigoDependencia { get; set; }
        public string Accion { get; set; }
        public int Orden { get; set; }
        public string Texto { get; set; }
        public string CodigoAnterior { get; set; }
        public List<PocConfiguracionDinamica>
            ConfiguracionDinamica { get; set; }
    }

    public class PocConfiguracionDinamica
    {
        public int IdContexto { get; set; }
        public int IdSubContexto { get; set; }
        public string[] Valor { get; set; }
    }

    public class PocSucursal
    {
        public int IdSucursal { get; set; }
        public int IdEntidad { get; set; }
        public string DescripcionEntidad { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string EncargadoOficina { get; set; }
        public string Horario { get; set; }
    }

    public class PocReporteBase
    {
        public int IdEstado { get; set; }
        public int IdReporte { get; set; }
        public int IdEmpresa { get; set; }
        public int IdContexto { get; set; }
        public bool Actualizar { get; set; }
        public string Descripcion { get; set; }
        public bool IdIncluyeEncabTxt { get; set; }
        public bool IdIncluyeContrapartida { get; set; }
        public string TipoMovimiento { get; set; }
    }

    public class PocReporteGeneral : PocReporteBase
    {
        public List<PocCampoReporteContexto> Campos { get; set; }
    }

    public class PocCampoReporteContexto
    {
        public int IdCampo { get; set; }
        public int IdContexto { get; set; }
        public bool IdSeparador { get; set; }
        public int? Orden { get; set; }
        public int IdTipo { get; set; }
        public int IdOrigen { get; set; }
        public int? LongitudMax { get; set; }
        public string Campo { get; set; }
        public string Descripcion { get; set; }
        public bool EsValido { get; set; }
        public string Formato { get; set; }
        public string ValorDefecto { get; set; }
        public bool SumaContrapartida { get; set; }
        public bool AgrupaContrapartida { get; set; }
        public dynamic ValorContrapartida { get; set; }
        public List<PocParametroEmpresaLista> Lista { get; set; }
        public string Alias { get; set; }
        public string CampoRelacionado { get; set; }
    }

    public class PocParametros
    {
        public int IdParametro { get; set; }
        public string Descripcion { get; set; }
    }

    public class PocCampoContexto
    {
        public int IdCampo { get; set; }
        public int IdContexto { get; set; }
        public string NombreCampo { get; set; }
        public int IdTipo { get; set; }
        public string Descripcion { get; set; }
        public List<PocListaSql> Lista { get; set; }
        public string Referencia1 { get; set; }
        public string Referencia2 { get; set; }
        public string Referencia3 { get; set; }
        public string Referencia4 { get; set; }
        public string Referencia5 { get; set; }
    }

    public class PocListaSql
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
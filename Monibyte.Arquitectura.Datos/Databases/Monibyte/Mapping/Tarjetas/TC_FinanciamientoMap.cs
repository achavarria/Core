using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_FinanciamientoMap : EntityTypeConfiguration<TC_Financiamiento>
    {
        public TC_FinanciamientoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdFinanciamiento);

            // Properties
            this.Property(t => t.IdFinanciamiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TC_Financiamiento", "TCredito");
            this.Property(t => t.IdFinanciamiento).HasColumnName("IdFinanciamiento");
            this.Property(t => t.IdTipo).HasColumnName("IdTipo");
            this.Property(t => t.FecApertura).HasColumnName("FecApertura");
            this.Property(t => t.PlazoMeses).HasColumnName("PlazoMeses");
            this.Property(t => t.IdUsuarioApertura).HasColumnName("IdUsuarioApertura");
            this.Property(t => t.FecVencimiento).HasColumnName("FecVencimiento");
            this.Property(t => t.TasaInteres).HasColumnName("TasaInteres");
            this.Property(t => t.TasaMoratorio).HasColumnName("TasaMoratorio");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.MonApertura).HasColumnName("MonApertura");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.MonSaldo).HasColumnName("MonSaldo");
            this.Property(t => t.MonCuota).HasColumnName("MonCuota");
            this.Property(t => t.FecUltPago).HasColumnName("FecUltPago");
            this.Property(t => t.IdPeriodicidad).HasColumnName("IdPeriodicidad");
            this.Property(t => t.DiasGracia).HasColumnName("DiasGracia");
            this.Property(t => t.PeriodoGracia).HasColumnName("PeriodoGracia");
            this.Property(t => t.DiaDePago).HasColumnName("DiaDePago");
            this.Property(t => t.IdTipoCuota).HasColumnName("IdTipoCuota");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

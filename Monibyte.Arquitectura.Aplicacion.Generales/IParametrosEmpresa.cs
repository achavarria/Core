﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Generales
{
    public interface IParametroEmpresa : IAplicacionBase
    {
        IEnumerable<PocParametroEmpresa> ObtenerParametros(PocFiltroParametroEmpresa filtro);
        void MantenimientoParametrosAlContexto(List<PocParametroEmpresa> parametros, int idContexto);
        void EliminarParametroDelContexto(PocParametroEmpresa parametro, int idContexto);
        IEnumerable<PocReporteBase> ObtenerReportesEmpresa(int?[] contextos,
            int? idTipoReporte, bool soloReportesConCampos, bool incluirReporteBase = false);
        IEnumerable<PocCampoReporteContexto> ObtenerCamposReporte(int idReporte, bool mostrarTodos, bool mostrarPersonalizados);
        void GuardarCamposReporte(PocReporteGeneral reporte);
        //List<PocParametroEmpresa> ListaParametrosEmpresa();
        int? IncluirReporte(PocReporteBase reporte);
        PocReporteBase ObtenerReporte(int idReporte);
        List<PocParametroEmpresaLista> OrdenarParamLista(GL_ParametroEmpresa param,
            IEnumerable<GL_ParametroEmpresaLista> paramlista);
        void ActualizarIdReporteTC(int idReporte);
        List<PocParametroEmpresaLista> ObtenerParametroEmpresaLista(int idParametroEmpesa);
        List<PocParametroEmpresaLista> ObtenerListaPredefinida(int idParametroInterno);
        IEnumerable<int> ObtenerListasPadre(int? idLista);
        IEnumerable<PocParametroEmpresa> ObtenerPosiblesDependencias(PocFiltroParametroEmpresa filtro,
            int idParametro);
    }
}

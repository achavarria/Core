﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco.Quickpass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public class AppMovimientosQuickpass : AplicacionBase, IAppMovimientosQuickpass
    {
        private IAppGenerales _appGenerales;
        private IAppServiciosQuickpass _appServiciosQuickpass;
        private IRepositorioTarjeta _repTarjeta;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioVwTarjetaUsuario _repVwTarjetaUsuario;
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IRepositorioMnb<CL_Persona> _repPersonas;
        private IRepositorioMnb<QP_Quickpass> _repQuickpass;
        private IRepositorioMnb<QP_Movimientos> _repMovimientos;

        public AppMovimientosQuickpass(
            IAppGenerales appGenerales,
            IAppServiciosQuickpass appServiciosQuickpass,
            IRepositorioTarjeta repTarjeta,
            IRepositorioCatalogo repCatalogo,
            IRepositorioVwTarjetaUsuario repVwTarjetaUsuario,
            IRepositorioMnb<GL_Entidad> repEntidad,
            IRepositorioMnb<CL_Persona> repPersonas,
            IRepositorioMnb<QP_Quickpass> repQuickpass,
            IRepositorioMnb<QP_Movimientos> repMovimientos)
        {
            _appGenerales = appGenerales;
            _appServiciosQuickpass = appServiciosQuickpass;
            _repTarjeta = repTarjeta;
            _repCatalogo = repCatalogo;
            _repVwTarjetaUsuario = repVwTarjetaUsuario;
            _repEntidad = repEntidad;
            _repPersonas = repPersonas;
            _repQuickpass = repQuickpass;
            _repMovimientos = repMovimientos;
        }

        public void Eliminar(PocQPMovimientos registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<QP_Movimientos>();
                _repMovimientos.Delete(entrada);
                _repMovimientos.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocQPMovimientos registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<QP_Movimientos>();
                    _repMovimientos.Insert(entrada);
                    _repMovimientos.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocQPMovimientos));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocQPMovimientos registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<QP_Movimientos>();
                    _repMovimientos.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repMovimientos.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocQPMovimientos));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocQPMovimientos> Listar(PocQPMovimientos filtro, DataRequest request)
        {
            var criteria = new Criteria<QP_Movimientos>();
            var criteriaTC = new Criteria<TC_Tarjeta>();
            if (filtro != null)
            {
                if (filtro.FecDesde.HasValue)
                {
                    criteria.And(m => m.FecConsumo >= filtro.FecDesde);
                }
                if (filtro.FecHasta.HasValue)
                {
                    DateTime date = (DateTime)filtro.FecHasta;
                    date = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
                    criteria.And(m => m.FecConsumo <= date);
                }
                if (!string.IsNullOrEmpty(filtro.Detalle))
                {
                    criteria.And(m => m.Detalle.Contains(filtro.Detalle));
                }
                if (!string.IsNullOrEmpty(filtro.NumTarjeta))
                {
                    criteriaTC.And(x => x.NumTarjeta == filtro.NumTarjeta);
                }
            }

            var resultado = _repMovimientos.SelectBy(criteria)
                .Join(_repQuickpass.Table,
                        mv => mv.IdQuickpass,
                        qp => qp.IdQuickpass,
                        (mv, qp) => new { mv, qp })
                .Join(_repTarjeta.SelectBy(criteriaTC),
                        mq => mq.qp.IdTarjeta,
                        tc => tc.IdTarjeta,
                        (mq, tc) => new { mq.mv, mq.qp, tc })
                .Join(_repCatalogo.List("LTIPOMOVQUICKPASS"),
                        mqt => mqt.mv.IdTipoMovimiento,
                        cat => cat.IdCatalogo,
                        (mqt, cat) => new { mqt.mv, mqt.qp, mqt.tc, cat })
                .Join(_repCatalogo.List("LESTADOMOVQUICKPASS"),
                    tmp => tmp.mv.IdEstado,
                    est => est.IdCatalogo,
                    (tmp, est) => new { tmp.mv, tmp.qp, tmp.tc, tmp.cat, est })
                .Select(item => new PocQPMovimientos
                {
                    IdMovimiento = item.mv.IdMovimiento,
                    IdTipoMovimiento = item.mv.IdTipoMovimiento,
                    CodMovimiento = item.mv.CodMovimiento,
                    IdQuickpass = item.mv.IdQuickpass,
                    FecConsumo = item.mv.FecConsumo,
                    Monto = item.mv.Monto,
                    IdMoneda = item.mv.IdMoneda,
                    IdEstado = item.mv.IdEstado,
                    FecProcesamiento = item.mv.FecProcesamiento,
                    FecAplicacion = item.mv.FecAplicacion,
                    IdLote = item.mv.IdLote,
                    Detalle = item.mv.Detalle,
                    NumReferenciaPago = item.mv.NumReferenciaPago,
                    NumRefProcesador = item.mv.NumRefProcesador,
                    IdTipoRegistro = item.mv.IdTipoRegistro,
                    CodLocalizador = item.mv.CodLocalizador,
                    DetAplicacionPago = item.mv.DetAplicacionPago,
                    DescEstado = item.est.Descripcion,
                    DescTipoMovimiento = item.cat.Descripcion
                });

            return resultado.ToDataResult(request);
        }

        public DataResult<PocQPMovimientos> ListarMovimientosQP(PocQPMovimientos filtro, DataRequest request)
        {
            var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
            var entidad = _appGenerales.ObtieneEntidad(idEntidad);
            var criteria = new Criteria<QP_Movimientos>();
            if (filtro != null)
            {
                if (filtro.FecDesde.HasValue)
                {
                    criteria.And(m => m.FecConsumo >= filtro.FecDesde);
                }
                if (filtro.FecHasta.HasValue)
                {
                    criteria.And(m => m.FecConsumo <= filtro.FecHasta);
                }
            }
            var resultado = _repMovimientos.SelectBy(criteria).ToList()
                .Join(_repQuickpass.Table,
                     mv => mv.IdQuickpass,
                     qp => qp.IdQuickpass,
                     (mv, qp) => new { mv, qp })
                .Join(_repVwTarjetaUsuario.TarjetasPermitidas(filtro.IdCuenta),
                    temp => temp.qp.IdTarjeta,
                    tar => tar.IdTarjeta,
                    (temp, tar) => new { temp.mv, temp.qp, tar })
                .Join(_repCatalogo.List("LTIPOMOVQUICKPASS"),
                    temp => temp.mv.IdTipoMovimiento,
                    ct => ct.IdCatalogo,
                    (temp, ct) => new { temp.mv, temp.qp, temp.tar, ct })
                .Where(m => filtro.IdTarjeta == m.tar.IdTarjeta || !filtro.IdTarjeta.HasValue)
                .Select(item => new PocQPMovimientos
                {
                    IdMovimiento = item.mv.IdMovimiento,
                    IdTipoMovimiento = item.mv.IdTipoMovimiento,
                    CodMovimiento = item.mv.CodMovimiento,
                    IdQuickpass = item.mv.IdQuickpass,
                    FecConsumo = item.mv.FecConsumo,
                    Monto = item.mv.Monto,
                    IdMoneda = item.mv.IdMoneda,
                    IdEstado = item.mv.IdEstado,
                    FecProcesamiento = item.mv.FecProcesamiento,
                    FecAplicacion = item.mv.FecAplicacion,
                    IdLote = item.mv.IdLote,
                    Detalle = item.mv.Detalle,
                    Descripcion = item.ct.Descripcion,
                    SimboloMonedaLocal = entidad.MonedaLocal.Simbolo,
                    HoraConsumo = new TimeSpan(item.mv.FecConsumo.Hour, item.mv.FecConsumo.Minute, item.mv.FecConsumo.Second),
                    NumReferenciaPago = item.mv.NumReferenciaPago,
                    NumQuickpass = item.qp.NumQuickpass,
                    NombreImpreso = item.tar.NombreImpreso,
                    IdTipoRegistro = item.mv.IdTipoRegistro,
                    CodLocalizador = item.mv.CodLocalizador
                });
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocQPMovimientos registro, bool inserta)
        {
            var validacion = new ValidationResponse();

            if (registro.CodMovimiento < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdQuickpass < 0) { validacion.AddError("Err_0035"); }
            if (registro.Monto < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdMoneda < 0) { validacion.AddError("Err_0035"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }

        public List<PocQuickpassMovimientos> ConsultarMontosQP(PocListaQPMontos filtro)
        {
            var criteria = new Criteria<QP_Movimientos>();
            criteria.And(m => m.IdEstado == (int)EnumEstadoMovQuickpass.Error);
            criteria.Or(m => m.IdEstado == (int)EnumEstadoMovQuickpass.Registrado);
            if (filtro != null)
            {
                if (filtro.FecDesde.HasValue)
                {
                    criteria.And(m => m.FecConsumo >= filtro.FecDesde);
                }
                if (filtro.FecHasta.HasValue)
                {
                    DateTime date = (DateTime)filtro.FecHasta;
                    date = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
                    criteria.And(m => m.FecConsumo <= date);
                }
                criteria.And(m => filtro.ListaQP.Contains(m.IdQuickpass));
            }
            var resultado = _repMovimientos.SelectBy(criteria)
                .Join(_repQuickpass.Table,
                    mov => mov.IdQuickpass,
                    qp => qp.IdQuickpass,
                    (mov, qp) => new { mov, qp })
                .Join(_repTarjeta.Table,
                    tmp => tmp.qp.IdTarjeta,
                    t => t.IdTarjeta,
                    (tmp, t) => new { tmp.mov, tmp.qp, t })
                .Join(_repPersonas.Table,
                    tmp => tmp.t.IdPersona,
                    p => p.IdPersona,
                    (tmp, p) => new { tmp.mov, tmp.qp, tmp.t, p })
                .Where(movqp => movqp.qp.IdTarjeta.HasValue)
                .GroupBy(movqp => new
                {
                    movqp.mov.IdQuickpass,
                    movqp.qp.NumQuickpass,
                    movqp.t.IdTarjeta,
                    movqp.t.NumTarjeta,
                    movqp.p.NombreCompleto
                })
                .Select(sel => new PocQuickpassMovimientos
                {
                    IdQuickpass = sel.Key.IdQuickpass,
                    NumQuickpass = sel.Key.NumQuickpass,
                    IdTarjeta = sel.Key.IdTarjeta,
                    Monto = sel.Sum(movqp => movqp.mov.Monto),
                    NumTarjeta = sel.Key.NumTarjeta,
                    NombreCompleto = sel.Key.NombreCompleto
                });
            return resultado.ProyectarComo<PocQuickpassMovimientos>();
        }

        public void AplicarMontosQP(PocListaQPMontos filtro)
        {
            var criteria = new Criteria<QP_Movimientos>();
            DateTime date = (DateTime)filtro.FecHasta;
            criteria.And(m => m.IdEstado == (int)EnumEstadoMovQuickpass.Error);
            criteria.Or(m => m.IdEstado == (int)EnumEstadoMovQuickpass.Registrado);
            if (filtro != null)
            {
                if (filtro.FecDesde.HasValue)
                {
                    criteria.And(m => m.FecConsumo >= filtro.FecDesde);
                }
                if (filtro.FecHasta.HasValue)
                {
                    date = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
                    criteria.And(m => m.FecConsumo <= date);
                }
                criteria.And(m => filtro.ListaQP.Contains(m.IdQuickpass));
            }
            var movimientosCargar = _repMovimientos.SelectBy(criteria)
                .Join(_repQuickpass.Table,
                    mov => mov.IdQuickpass,
                    qp => qp.IdQuickpass,
                    (mov, qp) => new
                    {
                        mov.IdQuickpass,
                        qp.NumQuickpass,
                        qp.IdTarjeta,
                        mov.IdEstado,
                        mov.CodMovimiento,
                        mov.IdMoneda,
                        mov.Monto,
                        mov.IdTipoMovimiento,
                        mov.FecConsumo
                    })
                .Join(_repTarjeta.Table,
                    tmp => tmp.IdTarjeta,
                    tc => tc.IdTarjeta,
                    (tmp, tc) => new { tmp, tc.NumTarjeta })
                .Where(movqp => movqp.tmp.IdTarjeta.HasValue)
                .GroupBy(movqp => new
                {
                    movqp.tmp.IdQuickpass,
                    movqp.tmp.NumQuickpass,
                    movqp.tmp.IdTarjeta,
                    movqp.tmp.CodMovimiento,
                    movqp.tmp.IdMoneda,
                    movqp.tmp.IdTipoMovimiento,
                    movqp.NumTarjeta
                })
                .Select(m => new PocQuickpassMovimientos
                {
                    IdQuickpass = m.Key.IdQuickpass,
                    NumQuickpass = m.Key.NumQuickpass,
                    IdTarjeta = m.Key.IdTarjeta,
                    CodMovimiento = m.Key.CodMovimiento,
                    Monto = m.Sum(movqp => movqp.tmp.Monto),
                    IdMoneda = m.Key.IdMoneda,
                    IdTipoMovimiento = m.Key.IdTipoMovimiento,
                    FecConsumo = m.Max(movqp => movqp.tmp.FecConsumo.Date),
                    NumTarjeta = m.Key.NumTarjeta
                });
            List<PocQuickpassMovimientos> listaMovs = movimientosCargar.ToList();
            _appServiciosQuickpass.AplicarCargosMovimientosQP(listaMovs, filtro.FecDesde, date);
        }
    }
}

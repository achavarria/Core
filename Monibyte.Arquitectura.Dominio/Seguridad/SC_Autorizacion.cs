using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Seguridad
{
    public partial class SC_Autorizacion : EntidadBase
    {
        public int IdAutorizacion { get; set; }
        public int IdTipoAutorizacion { get; set; }
        public int IdEmpresa { get; set; }
        public string NumReferencia { get; set; }
        public string Descripcion { get; set; }
        public int IdUsuarioPrepara { get; set; }
        public System.DateTime FecPrepara { get; set; }
        public Nullable<int> IdUsuarioAutoriza { get; set; }
        public Nullable<System.DateTime> FecAutoriza { get; set; }
        public int IdEstado { get; set; }
        public string DetalleAutorizacion { get; set; }
        public string Observaciones { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

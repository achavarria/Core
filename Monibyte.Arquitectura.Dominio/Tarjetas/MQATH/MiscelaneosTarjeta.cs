﻿using System;
using Monibyte.Arquitectura.Comun.Nucleo.Trama;
namespace Monibyte.Arquitectura.Dominio.Tarjetas.MQATH
{  
    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaMiscelaneosTarjeta {
        string _blancos = string.Empty.PadRight(52, ' ');
        private string _tipoMensaje = TipoEntradaMsj.APLICA_MISCELANEOS; 

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string TipoMensaje { get { return _tipoMensaje; } set { _tipoMensaje = value; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 8, format: "yyyyMMdd", 
            regex: @"^\d{4}((0[1-9])|(1[012]))((0[1-9]|[12]\d)|3[01])$", characterFill: Constantes.CARACTERBLANCO)]
        public DateTime FechaConsumo { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal MontoTransaccion { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string MonedaTransaccion { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string CodMovimiento { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumReferencia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string Localidad { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 52)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }

    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaMiscelaneosTarjeta : RespuestaBase
    {

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 8, format: "yyyyMMdd", 
            regex: @"^\d{4}-((0[1-9])|(1[012]))-((0[1-9]|[12]\d)|3[01])$", characterFill: Constantes.CARACTERBLANCO)]
        public DateTime FechaConsumo { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal MontoTransaccion { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string MonedaTransaccion { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string CodMovimiento { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumReferencia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string Localidad { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumReferenciaSiscard { get; set; }
        [TramaColumnSpec(required: false, maxLength: 874)]
        public string Blancos { get; set; }
    }

}

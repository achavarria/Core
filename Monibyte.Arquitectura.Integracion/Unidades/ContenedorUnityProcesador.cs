﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Procesador;

    public class ContenedorUnityProcesador
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            
            #region Servicios
            _unityContainer.RegisterType<IAppAutorizaciones, AppAutorizaciones>();
            _unityContainer.RegisterType<IAppReportes, AppReportes>();
            #endregion
                       
        }
    }
}

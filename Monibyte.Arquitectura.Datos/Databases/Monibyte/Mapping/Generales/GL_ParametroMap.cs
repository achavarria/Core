using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ParametroMap : EntityTypeConfiguration<GL_Parametro>
    {
        public GL_ParametroMap()
        {
            // Primary Key
            this.HasKey(t => t.IdParametro);

            // Properties
            this.Property(t => t.IdParametro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .HasMaxLength(100);

            this.Property(t => t.IdOrigen)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("GL_Parametro", "General");
            this.Property(t => t.IdParametro).HasColumnName("IdParametro");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdTipoDato).HasColumnName("IdTipoDato");
            this.Property(t => t.Longitud).HasColumnName("Longitud");
            this.Property(t => t.IdTipoOrigen).HasColumnName("IdTipoOrigen");
            this.Property(t => t.IdOrigen).HasColumnName("IdOrigen");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Cargos
{
    public partial class CA_TipoCargo : EntidadBase
    {
        public int IdTipoCargo { get; set; }
        public string Descripcion { get; set; }
        public int IdMoneda { get; set; }
        public int IdOrigenCargo { get; set; }
        public int IdTipoCobro { get; set; }
        public Nullable<decimal> MonCargo { get; set; }
        public Nullable<int> IdTablaRangos { get; set; }
        public int IdPeriodicidad { get; set; }
        public int ValPeriodicidad { get; set; }
        public int IdMovimientoTC { get; set; }
        public int IdMovReversaTC { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdMonedaCobro { get; set; }
    }
}

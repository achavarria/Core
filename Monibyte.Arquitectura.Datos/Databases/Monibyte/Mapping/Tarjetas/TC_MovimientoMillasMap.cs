using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_MovimientoMillasMap : EntityTypeConfiguration<TC_MovimientoMillas>
    {
        public TC_MovimientoMillasMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMovimiento);

            // Properties
            this.Property(t => t.IdMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DetalleMovimiento)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("TC_MovimientoMillas", "TCredito");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.FecTransaccion).HasColumnName("FecTransaccion");
            this.Property(t => t.HoraTransaccion).HasColumnName("HoraTransaccion");
            this.Property(t => t.CantidadMillas).HasColumnName("CantidadMillas");
            this.Property(t => t.IdDebCredito).HasColumnName("IdDebCredito");
            this.Property(t => t.FecProceso).HasColumnName("FecProceso");
            this.Property(t => t.DetalleMovimiento).HasColumnName("DetalleMovimiento");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdPrograma).HasColumnName("IdPrograma");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

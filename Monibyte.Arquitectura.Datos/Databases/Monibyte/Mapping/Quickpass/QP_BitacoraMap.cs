using Monibyte.Arquitectura.Dominio.Quickpass;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Quickpass
{
    public class QP_BitacoraMap : EntityTypeConfiguration<QP_Bitacora>
    {
        public QP_BitacoraMap()
        {
            // Primary Key
            this.HasKey(t => t.IdBitacora);

            // Properties
            this.Property(t => t.Detalle)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("QP_Bitacora", "Quickpass");
            this.Property(t => t.IdBitacora).HasColumnName("IdBitacora");
            this.Property(t => t.IdQuickpass).HasColumnName("IdQuickpass");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdEstadoOperativo).HasColumnName("IdEstadoOperativo");
            this.Property(t => t.Detalle).HasColumnName("Detalle");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

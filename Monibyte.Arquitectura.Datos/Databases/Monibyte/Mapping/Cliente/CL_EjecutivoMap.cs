using Monibyte.Arquitectura.Dominio.Clientes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Clientes
{
    public class CL_EjecutivoMap : EntityTypeConfiguration<CL_Ejecutivo>
    {
        public CL_EjecutivoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdEjecutivo);

            // Properties
            this.Property(t => t.IdEjecutivo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CL_Ejecutivo", "Cliente");
            this.Property(t => t.IdEjecutivo).HasColumnName("IdEjecutivo");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdEntidad).HasColumnName("IdEntidad");
        }
    }
}

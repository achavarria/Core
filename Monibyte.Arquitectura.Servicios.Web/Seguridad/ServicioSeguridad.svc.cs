﻿using Monibyte.Arquitectura.Aplicacion.Seguridad;
using Monibyte.Arquitectura.Aplicacion.Seguridad.Mantenimientos;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Poco.Seguridad;
using Monibyte.Arquitectura.Servicios.Nucleo;

namespace Monibyte.Arquitectura.Servicios.Web.Seguridad
{
    public class ServicioSeguridad : ServicioUnity, IServicioSeguridad
    {
        public PocInfoSesion ObtenerUsuarioPorTicket(string ticket)
        {
            using (var app = GestorUnity.CargarUnidad<IDirectorioUsuarioApp>())
            {
                return app.ObtenerCuentaPorTicket(ticket);
            }
        }

        public Transporte ObtenerUsuarioApi(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IDirectorioUsuarioApp>())
            {
                var clientId = transporte.GetDatos<int>();
                var resultado = app.ObtenerUsuarioApi(clientId);
                transporte.SetDatos(resultado);
                return transporte;
            }
        }
     
        public Transporte ObtenerPermisosPorUsuario(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<ISeguridadApp>())
            {
                var resultado = app.ObtenerPermisosPorUsuario();
                transporte.SetDatos(resultado);
                return transporte;
            }
        }

        public Transporte ObtenerMenuUsuario(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<ISeguridadApp>())
            {
                var resultado = app.ObtenerMenuUsuario();
                transporte.SetDatos(resultado);
                return transporte;
            }
        }

        public Transporte GenerarReporteSeguridad(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppReportes>())
            {
                var codUsuario = transporte.GetValor<string>("codUsuario");
                var nombreCompleto = transporte.GetValor<string>("nombreCompleto");
                var filtro = transporte.GetValor<int>("filtro");
                var NombreEmpresa = transporte.GetValor<string>("NombreEmpresa");
                var tipoPersona = transporte.GetValor<int>("tipoPersona");
                app.GenerarReporteSeguridad(codUsuario,
                    nombreCompleto, filtro, NombreEmpresa, tipoPersona);
                return transporte;
            }
        }

        public Transporte ValidaCodigoActivacion(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppActivacionUsuario>())
            {
                var CodActivacion = transporte.GetValor<string>("CodActivacion");
                app.ValidaCodigoActivacion(CodActivacion);
                return transporte;
            }
        }

        public Transporte ValidaAccesoUsuario(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppActivacionUsuario>())
            {
                var datosUsuario = transporte.GetDatos<PocActivacionUsuario>();
                app.ValidaAccesoUsuario(datosUsuario);
                return transporte;
            }
        }

        public Transporte InsertaMenuEmpresa(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppOpMenuEmpresa>())
            {
                var datosMenuEmpresa = transporte.GetDatos<PocMenuEmpresa>();
                app.Insertar(datosMenuEmpresa);
                return transporte;
            }
        }

        public Transporte ModificaMenuEmpresa(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppOpMenuEmpresa>())
            {
                var datosMenuEmpresa = transporte.GetDatos<PocMenuEmpresa>();
                app.Modificar(datosMenuEmpresa);
                return transporte;
            }
        }

        public Transporte EliminaMenuEmpresa(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppOpMenuEmpresa>())
            {
                var datosMenuEmpresa = transporte.GetDatos<PocMenuEmpresa>();
                app.Eliminar(datosMenuEmpresa);
                return transporte;
            }
        }

        public Transporte ConsultaMenuEmpresa(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppOpMenuEmpresa>())
            {
                var datosMenuEmpresa = transporte.GetDatos<PocMenuEmpresa>();
                var resultado = app.Listar(datosMenuEmpresa, transporte.GetDataRequest());
                transporte.SetDatos(resultado);
                return transporte;
            }
        }

        public Transporte ConsultaMenu(Transporte transporte)
        {
            using (var app = GestorUnity.CargarUnidad<IAppOpMenu>())
            {
                var datosMenu = transporte.GetDatos<PocMenu>();
                var resultado = app.ListaConsulta();
                transporte.SetDatos(resultado);
                return transporte;
            }
        }             

        
    }
}

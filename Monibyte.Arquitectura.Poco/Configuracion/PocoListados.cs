﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monibyte.Arquitectura.Poco.Configuracion
{
    public class PocRegion
    {
        public int IdRegion { get; set; }
        public string Descripcion { get; set; }
    }

    public class PocPaisRegion
    {
        public int IdRegion { get; set; }
        public int IdPais { get; set; }
        public string Descripcion { get; set; }
    }

    public class PocCategoriaComercio
    {
        public int IdCategoria { get; set; }
        public string Descripcion { get; set; }
        public string Texto { get; set; }
        public int[] IdsSubCat { get; set; }
    }

    public class PocComercio
    {
        public string CodMccDesde { get; set; }
        public string CodMccHasta { get; set; }
        public string Descripcion { get; set; }
        public string Texto { get; set; }
    }
}

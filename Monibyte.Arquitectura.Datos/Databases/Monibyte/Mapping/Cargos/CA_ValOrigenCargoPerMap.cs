using Monibyte.Arquitectura.Dominio.Cargos;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Cargos
{
    public class CA_ValOrigenCargoPerMap : EntityTypeConfiguration<CA_ValOrigenCargoPer>
    {
        public CA_ValOrigenCargoPerMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdCargo, t.IdValOrigen });

            // Properties
            this.Property(t => t.IdCargo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdValOrigen)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CA_ValOrigenCargoPer", "Cargos");
            this.Property(t => t.IdCargo).HasColumnName("IdCargo");
            this.Property(t => t.IdValOrigen).HasColumnName("IdValOrigen");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");

        }
    }
}

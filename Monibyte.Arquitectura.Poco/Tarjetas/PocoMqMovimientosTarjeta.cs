﻿using System;

namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocEntradaMovimientosTarjeta
    {
        public bool Mq { get; set; }
        public int? IdCuenta { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public string Usuario { get; set; }
        public string SuperFranquicia { get; set; }
        public int IdEsCuentaMadre { get; set; }
    }

    public class PocSalidaMovimientosTarjeta
    {
        public string TipoMensaje { get; set; }
        public int IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreImpreso { get; set; }
        public string CodMovimiento { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public string Emisor { get; set; }
        public string NumCuenta { get; set; }
        public DateTime FecConsumo { get; set; }
        public DateTime FecMovimiento { get; set; }
        public string HoraMovimiento { get; set; }
        public decimal Monto { get; set; }
        public int? IdMoneda { get; set; }
        public string Moneda { get; set; }
        public string DescripcionMoneda { get; set; }
        public string NumAutorizacion { get; set; }
        public string NumReferenciaSiscard { get; set; }
        public string NombreComercio { get; set; }
        public int IdDebCredito { get; set; }
        public string DynamicJsonTarjeta { get; set; }
        public bool SoloConsulta { get; set; }
        public int IdTipoMovimiento { get; set; }
    }
}

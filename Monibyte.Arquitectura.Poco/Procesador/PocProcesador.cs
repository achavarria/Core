﻿using System;
using System.Collections.Generic;
namespace Monibyte.Arquitectura.Poco.Procesador
{
    public class PocFiltroAutorizaciones
    {
        public DateTime? FecDesde { get; set; }
        public DateTime? FecHasta { get; set; }
        public string NumTarjeta { get; set; }
        public int PorCuenta { get; set; }
        public int IdCuenta { get; set; }
    }

    public class PocDenegacionesDiarias
    {
        public int IdEmpresa { get; set; }
        public int IdCompania { get; set; }
        public string Correo { get; set; }
    }

    public class PocAutorizaciones
    {
        public int Id { get; set; }
        public string MessageTypeId { get; set; }
        public string PrimaryBitMap { get; set; }
        public string SecondaryBitMap { get; set; }
        public string PrimaryNumberAccount { get; set; }
        public string ProcessingCode { get; set; }
        public decimal? Monto { get; set; }
        public string TransmissionDateTime { get; set; }
        public decimal? ConversionRateCb { get; set; }
        public string SystemTraceAuditNumber { get; set; }
        public TimeSpan HoraTransaccion { get; set; }
        public DateTime FecTransaccion { get; set; }
        public string SettlementDate { get; set; }
        public string CaptureDate { get; set; }
        public string MerchantCategory { get; set; }
        public string AcquiringCountryCode { get; set; }
        public string PointOfServiceEntryMode { get; set; }
        public string PosConditionCode { get; set; }
        public string AcquiringId { get; set; }
        public string Track2Data { get; set; }
        public string RetrievalNumberReference { get; set; }
        public string CardAcceptorTerminalId { get; set; }
        public string CardAcceptorIdentificationCode { get; set; }
        public string Comercio { get; set; }
        public string AdditionalRetailerData { get; set; }
        public string CodMonedaInternacional { get; set; }
        public string CurrencyCodeCb { get; set; }
        public string AtmTerminalData { get; set; }
        public string AtmCardIssuerAuthorizerData { get; set; }
        public string ReceivingIdCode { get; set; }
        public string AtmTerminalAddressBranchReg { get; set; }
        public string AtmAdditionalData { get; set; }
        public string ResponseCode { get; set; }
        public string AuthorizationIdResponse { get; set; }
        public string AccountIdentification1 { get; set; }
        public string AccountIdentification2 { get; set; }
        public decimal CardholderBillingAmount { get; set; }
        public string OriginalDataElements { get; set; }
        public string ReplacementAmounts { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }
        public string NumTarjeta { get; set; }
        public int? DenegationId { get; set; }
        public string DenegationDetail { get; set; }
        public string InputParameter { get; set; }
        public string TipoDenegacion { get; set; }
        public string Pais { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_MccPlantillaMap : EntityTypeConfiguration<MB_MccPlantilla>
    {
        public MB_MccPlantillaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMcc);

            // Properties
            this.Property(t => t.IdMcc)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodMccDesde)
                .IsRequired()
                .HasMaxLength(6);

            this.Property(t => t.CodMccHasta)
                .IsRequired()
                .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("MB_MccPlantilla", "Mnb");
            this.Property(t => t.IdMcc).HasColumnName("IdMcc");
            this.Property(t => t.IdPlantilla).HasColumnName("IdPlantilla");
            this.Property(t => t.IdCategoriaMcc).HasColumnName("IdCategoriaMcc");
            this.Property(t => t.CodMccDesde).HasColumnName("CodMccDesde");
            this.Property(t => t.CodMccHasta).HasColumnName("CodMccHasta");
            this.Property(t => t.LimiteConsumo).HasColumnName("LimiteConsumo");
            this.Property(t => t.CantidadConsumo).HasColumnName("CantidadConsumo");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdSoloCategoria).HasColumnName("IdSoloCategoria");
            this.Property(t => t.IdPeriodicidad).HasColumnName("IdPeriodicidad");
            this.Property(t => t.LimiteConsumoInter).HasColumnName("LimiteConsumoInter");
        }
    }
}

﻿using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios
{

    public interface IRepositorioVwTarjetaUsuario : IRepositorioMnb<TC_vwTarjetaUsuario>
    {
        TC_vwTarjetaUsuario ObtenerTarjeta(int idTarjeta);
        TC_vwTarjetaUsuario ObtenerTarjeta(string numTarjeta, int idUsuario);
        IEnumerable<TC_vwTarjetaUsuario> TarjetasPermitidas(int? idCuenta = null,
            bool incluirHijas = true, int[] filtroEstado = null, int[] filtroEstadoExc = null,
            Criteria<TC_vwTarjetaUsuario> criterias = null, bool incluirOperativas = false,
            bool ignorarPanelCtrl = false, bool ignorarIdGrupo = true, bool ignorarSoloConsulta = false);
        IEnumerable<TC_vwTarjetaUsuario> TarjetasPermitidasGrupo(
            int? idCuenta = null, int? idGrupoEmpresa = null,
            int[] filtroEstado = null);
    }
    
}

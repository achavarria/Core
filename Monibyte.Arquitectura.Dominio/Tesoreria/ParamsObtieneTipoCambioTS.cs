﻿
namespace Monibyte.Arquitectura.Dominio.Tesoreria
{
    public class ParamsObtieneTipoCambioTS : EntidadBase
    {
        public int IdTipoCambio { get; set; }
        public System.DateTime FecCambio { get; set; }
        public int IdMoneda { get; set; }
        public int? IdEntidad { get; set; }
    }
}

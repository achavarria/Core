using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_DetalleEstCuentaExtFinHist : EntidadBase
    {
        public int IdDetalle { get; set; }
        public int IdEncabezado { get; set; }
        public string NumeroExtra { get; set; }
        public Nullable<System.DateTime> FechaTransaccion { get; set; }
        public string Descripcion { get; set; }
        public Nullable<decimal> SaldoAnterior { get; set; }
        public Nullable<decimal> MonPrincipal { get; set; }
        public Nullable<decimal> SaldoAlCorte { get; set; }
        public Nullable<System.DateTime> FechaMovimiento { get; set; }
        public string Referencia { get; set; }
        public string DetalleMovimiento { get; set; }
        public Nullable<decimal> MontoCuota { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Cargos;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public interface IAppDetRangosCargosPer : IAplicacionBase
    {
        void Insertar(PocDetRangosCargosPer registro);
        void Eliminar(PocDetRangosCargosPer registro);
        void Modificar(PocDetRangosCargosPer registro);
        DataResult<PocDetRangosCargosPer> Listar(PocDetRangosCargosPer filtro, DataRequest request);
    }
}

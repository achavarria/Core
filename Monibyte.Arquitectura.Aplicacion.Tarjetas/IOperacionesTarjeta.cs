﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public interface IOperacionesTarjeta : IAplicacionBase
    {
        void ActualizarDetalleTarjeta(PocEntradaConsultaTarjeta pocEntrada);
        void ActivarTarjeta(PocEntradaActivaTarjeta pocEntrada);
        void ActualizarEstadoTarjeta(PocEntradaActualizaTarjeta pocEntrada);
        void ReponerPinTarjeta(PocEntradaReposicionPinTarjeta pocEntrada);
        PocModificaFecCorteTarjeta ObtieneFechaCorte(int IdCuenta);
        void AplicarFechaCorte(int idCuenta, int idDiaCorte);
        void AplicarCambioLimiteCuenta(int idCuenta, decimal montoInter);
        void AplicarCambioLimiteTarjeta(string numTarjeta, decimal montoInter, int idTipoGestion, DateTime? VigenteHasta = null);
        void ValidarCambioLimiteTarjeta(string numTarjeta, decimal limiteNuevo, int idTipoGestion, DateTime? VigenteHasta = null);
        void AplicarBloqueo(int idTarjeta, int idTipoBloqueo, int idCuenta);
        void AplicarReactivarTarjeta(int idTarjeta, int idEstado, string motivoCancelacion);
        string ObtenerDireccion(int idTarjeta);
        int ObtenerDiaCorte(int diaCorte);
        TC_Cuenta ObtieneInfoCuenta(int idCuenta);
        void GuardarDetalleTarjeta(PocTarjetaCuentaUsuario tarjeta);
        void ActualizarCorreosTarjeta(List<PocTarjetaCuentaUsuario> datosEntrada,
            List<PocTarjetaCuentaUsuario> modeloPrevio);
        void GuardarCopiasDeCorreo(int[] tarjetas, string texto);
        bool LimiteGlobalPermitido(int idCuentaM, decimal monInter);
        void ValidarCambioVipTarjeta(PocModificaVip datosVip);
        void AplicarCambioVipTarjeta(PocModificaVip datosVip);
        void AplicarActivarTarjeta(int idTarjeta, int idEstado);
        PocRespuestaServicio ActualizarPlaca(string nombreImpreso, string placa);
    }
}

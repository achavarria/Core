﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Dominio.Seguridad.Repositorios;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public class RolesApp : AplicacionBase, IRolesApp
    {
        private IRepositorioRole _repRole;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<SC_RolesGrupo> _repRolesGrupo;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _vwEmpresasUsuario;

        public RolesApp(
            IRepositorioRole repRole,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<SC_RolesGrupo> repRolesGrupo,
            IRepositorioMnb<CL_vwEmpresasUsuario> vwEmpresasUsuario)
        {
            _repRole = repRole;
            _repCatalogo = repCatalogo;
            _repRolesGrupo = repRolesGrupo;
            _vwEmpresasUsuario = vwEmpresasUsuario;
        }

        public bool ExisteRol(int codRol)
        {
            var rol = _repRole.ObtenerRolPorCodigo(codRol);
            return rol != null;
        }

        public IEnumerable<PocRol> ObtenerRolesTipoUsuario(int idTipoUsuario)
        {
            var resultado = _repRole.Table
                 .Where(r => _repCatalogo.Table.Any(c =>
                     c.IdCatalogo == idTipoUsuario &&
                     (c.Referencia2 == r.IdRole.ToString() ||
                     c.Referencia3 == r.IdRole.ToString())))
                 .Select(item => new PocRol
                 {
                     IdRole = item.IdRole,
                     Descripcion = item.Descripcion
                 });
            return resultado;
        }

        public IEnumerable<PocRol> ObtenerRolesPorUsuario(int idUsuario)
        {
            var roles = _vwEmpresasUsuario.Table.Join(_repRole.Table,
                    usRol => usRol.IdRole,
                    rol => rol.IdRole,
                    (usRol, rol) => new { usRol, rol })
                .Where(res => res.usRol.IdUsuario == idUsuario &&
                     res.rol.IdEstado == 1)
                .Select(res => new PocRol
                {
                    IdRole = res.rol.IdRole,
                    Descripcion = res.rol.Descripcion,
                    IdEstado = res.rol.IdEstado,
                    IdEsGrupo = res.rol.IdEsGrupo
                });
            return roles.ToList();
        }

        public IEnumerable<PocRol> ObtenerRolesPorGrupo(int codGrupo)
        {
            var roles = _repRole.Table.Join(_repRolesGrupo.Table,
                    rol => rol.IdRole,
                    rolGrupo => rolGrupo.IdRole,
                    (rol, rolGrupo) => new { rol, rolGrupo })
                .Where(res => res.rolGrupo.IdGrupo == codGrupo &&
                    res.rolGrupo.IdEstado == 1 &&
                    res.rol.IdEstado == 1)
                .Select(res => new PocRol
                {
                    IdRole = res.rol.IdRole,
                    Descripcion = res.rol.Descripcion,
                    IdEstado = res.rol.IdEstado,
                    IdEsGrupo = res.rol.IdEsGrupo
                });
            return roles.ToList();
        }

        public IEnumerable<PocRol> ListaRoles()
        {

            var query = _repRole.Table
                .Join(_repRolesGrupo.Table,
                    r => r.IdRole,
                    rg => rg.IdGrupo,
                    (r, rg) => new { r, rg })
                .Join(_vwEmpresasUsuario.Table,
                    tmp => tmp.r.IdRole,
                    vw => vw.IdRole,
                    (tmp, vw) => new { tmp.r, tmp.rg, vw })
                .Where(x => x.r.IdRole == 1)
                .ToList();


            var roles = _repRole.Table
                .Where(x => x.IdEstado == (int)EnumIdEstado.Activo)
                .ProyectarComo<PocRol>();

            //var res = _repRolesGrupo.Table
            //    .Where(x => x.IdEstado == 1)
            //    .ToList();


            return roles;
        }
    }
}

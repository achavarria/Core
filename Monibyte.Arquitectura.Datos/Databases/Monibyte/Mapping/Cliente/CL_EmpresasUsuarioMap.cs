using Monibyte.Arquitectura.Dominio.Clientes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Clientes
{
    public class CL_EmpresasUsuarioMap : EntityTypeConfiguration<CL_EmpresasUsuario>
    {
        public CL_EmpresasUsuarioMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdEmpresa, t.IdUsuario });

            // Properties
            this.Property(t => t.IdEmpresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdUsuario)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CL_EmpresasUsuario", "Cliente");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.IdDefault).HasColumnName("IdDefault");
            this.Property(t => t.IdTipoUsuario).HasColumnName("IdTipoUsuario");
            this.Property(t => t.IdRole).HasColumnName("IdRole");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdRoleOperativo).HasColumnName("IdRoleOperativo");
        }
    }
}

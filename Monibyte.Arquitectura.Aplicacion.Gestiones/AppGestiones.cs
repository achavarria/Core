﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Aplicacion.Notificacion;
using Monibyte.Arquitectura.Aplicacion.Seguridad;
using Monibyte.Arquitectura.Aplicacion.Tarjetas;
using Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Gestiones.Repositorios;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Gestiones;
using Monibyte.Arquitectura.Poco.Notificacion;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Gestiones
{
    public class AppGestiones : AplicacionBase, IAppGestiones
    {
        #region CONSTRUCTOR UNITY
        private IRepositorioCuenta _repCuenta;
        private IAppUtilitarios _appUtilitarios;
        private IRepositorioTarjeta _repTarjeta;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioGestion _repGestion;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<GE_TipoGestion> _repTipoGestion;
        private IRepositorioMnb<GE_TipoGestionRol> _repTipoGestionRol;
        private IRepositorioMnb<GE_ParametrosValor> _repParamValor;
        private IRepositorioMnb<GE_HistorialGestion> _repHistGestion;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;
        private IRepositorioMnb<GL_ParametroCompania> _repParamCompania;
        private IRepositorioVwTarjetaUsuario _repTarjetaUsuario;
        private IRepositorioMnb<CL_Ejecutivo> _repEjecutivo;
        private IRepositorioMnb<GL_Sucursal> _repSucursal;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _vwEmpresasUsuario;
        private IRepositorioMnb<MB_AdministradoresGrupo> _repAdmGrupoEmpresa;
        private IDirectorioUsuarioApp _dirUsuarioApp;
        private IRepositorioReportServer _repReportServer;
        private IAppNotificacion _appNotificacion;
        private IOperacionesTarjeta _opTarjeta;
        private IRepositorioMnb<TC_vwTarjetasCuenta> _repVwTarjetasCuenta;
        private IFuncionesGestion _funcionesGestion;

        public AppGestiones(
            IRepositorioCuenta repCuenta,
            IAppUtilitarios appUtilitarios,
            IRepositorioTarjeta repTarjeta,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioGestion repGestion,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<GE_TipoGestion> repTipoGestion,
            IRepositorioMnb<GE_TipoGestionRol> repTipoGestionRol,
            IRepositorioMnb<GE_ParametrosValor> repParamValor,
            IRepositorioMnb<GE_HistorialGestion> repHistGestion,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema,
            IRepositorioMnb<GL_ParametroCompania> repParamCompania,
            IRepositorioMnb<GE_MovimientoGestion> repMovsGestion,
            IRepositorioVwTarjetaUsuario repTarjetaUsuario,
            IRepositorioMnb<CL_Ejecutivo> repEjecutivo,
            IRepositorioMnb<GL_Sucursal> repSucursal,
            IRepositorioMnb<CL_vwEmpresasUsuario> vwEmpresasUsuario,
            IRepositorioMnb<MB_AdministradoresGrupo> repAdmGrupoEmpresa,
            IDirectorioUsuarioApp dirUsuarioApp,
            IRepositorioReportServer repReportServer,
            IAppNotificacion appNotificacion,
            IOperacionesTarjeta opTarjeta,
            IRepositorioMnb<TC_vwTarjetasCuenta> repVwTarjetasCuenta,
            IFuncionesGestion funcionesGestion)
        {
            _repCuenta = repCuenta;
            _appUtilitarios = appUtilitarios;
            _repTarjeta = repTarjeta;
            _repPersona = repPersona;
            _repGestion = repGestion;
            _repCatalogo = repCatalogo;
            _repTipoGestion = repTipoGestion;
            _repTipoGestionRol = repTipoGestionRol;
            _repParamValor = repParamValor;
            _repHistGestion = repHistGestion;
            _repTarjetaUsuario = repTarjetaUsuario;
            _repParamSistema = repParamSistema;
            _repParamCompania = repParamCompania;
            _repEjecutivo = repEjecutivo;
            _repSucursal = repSucursal;
            _vwEmpresasUsuario = vwEmpresasUsuario;
            _repAdmGrupoEmpresa = repAdmGrupoEmpresa;
            _dirUsuarioApp = dirUsuarioApp;
            _repReportServer = repReportServer;
            _appNotificacion = appNotificacion;
            _opTarjeta = opTarjeta;
            _repVwTarjetasCuenta = repVwTarjetasCuenta;
            _funcionesGestion = funcionesGestion;
        }
        #endregion

        public int InsertaGestion(EnumTipoGestion idTipoGestion, EnumEstadoGestion idEstado,
            EnumContexto idContexto, int idProducto, string detalle,
            DateTime? fecInclusion = null, int? idSucursal = null, int? idEjecutivo = null,
            int? idContextoMovimientos = null, int? idEmpresa = null)
        {
            if (string.IsNullOrEmpty(detalle))
            {
                detalle = ObtenerDetalleTipoGestion(idTipoGestion);
            }
            using (var ts = new TransactionScope())
            {
                var entrada = new GE_Gestion();
                entrada.IdGestion = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdGestion");
                entrada.IdEstado = (int)idEstado;
                entrada.IdTipoGestion = (int)idTipoGestion;
                entrada.Detalle = detalle;
                entrada.IdUsuarioIncluye = InfoSesion.Info.IdUsuario;
                entrada.IdUsuarioCierra = InfoSesion.Info.IdUsuario;
                var productoGestion = ObtenerProducto((int)idContexto, idProducto);
                entrada.IdPersona = productoGestion.IdPersona.Value;
                entrada.IdTipoProducto = productoGestion.IdTipoProducto;
                entrada.IdProducto = idProducto;
                entrada.FecCierre = DateTime.Now.Date;
                entrada.FecInclusion = fecInclusion.HasValue ? fecInclusion.Value : DateTime.Now;
                entrada.IdContexto = (int)idContexto;
                entrada.IdEmpresa = idEmpresa ?? (int)InfoSesion.Info.IdEmpresa;
                entrada.IdCompania = (int)InfoSesion.Info.IdCompania;
                entrada.IdUnidad = (int)InfoSesion.Info.IdUnidad;
                entrada.IdSucursal = idSucursal;
                entrada.IdEjecutivo = idEjecutivo;
                entrada.IdContextoMovimientos = idContextoMovimientos;
                _repGestion.Insert(entrada);
                _repGestion.UnidadTbjo.Save();
                IncluirHistorialGestion(new PocCambioEstadoGestion
                {
                    IdGestion = entrada.IdGestion,
                    Justificacion = entrada.Detalle,
                    IdEstado = entrada.IdEstado
                });
                ts.Complete();
                return entrada.IdGestion;
            }
        }

        public int InsertaGestion(EnumTipoGestion idTipoGestion, EnumEstadoGestion idEstado,
            EnumContexto idContexto, EnumTipoProducto idTipoProducto, int idPersona,
            string detalle, int? idSucursal = null, int? idEjecutivo = null)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = new GE_Gestion();
                entrada.IdGestion = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdGestion");
                entrada.IdEstado = (int)idEstado;
                entrada.IdTipoGestion = (int)idTipoGestion;
                entrada.Detalle = detalle;
                entrada.IdUsuarioIncluye = InfoSesion.Info.IdUsuario;
                entrada.IdUsuarioCierra = InfoSesion.Info.IdUsuario;
                entrada.IdPersona = idPersona;
                entrada.IdTipoProducto = (int)idTipoProducto;
                entrada.FecCierre = DateTime.Now.Date;
                entrada.FecInclusion = DateTime.Now;
                entrada.IdContexto = (int)idContexto;
                entrada.IdEmpresa = (int)InfoSesion.Info.IdEmpresa;
                entrada.IdCompania = (int)InfoSesion.Info.IdCompania;
                entrada.IdUnidad = (int)InfoSesion.Info.IdUnidad;
                entrada.IdSucursal = idSucursal;
                entrada.IdEjecutivo = idEjecutivo;
                _repGestion.Insert(entrada);
                _repGestion.UnidadTbjo.Save();
                IncluirHistorialGestion(new PocCambioEstadoGestion
                {
                    IdGestion = entrada.IdGestion,
                    Justificacion = entrada.Detalle,
                    IdEstado = entrada.IdEstado
                });
                ts.Complete();
                return entrada.IdGestion;
            }
        }

        public PocDetalleGestion ObtenerGestion(int idGestion)
        {
            var resultadoT =
                from gestionRespuesta in _repGestion.Table
                join estados in _repCatalogo.List("LESTADOGESTION") on
                    gestionRespuesta.IdEstado equals estados.IdCatalogo
                join persona in _repPersona.Table on
                    gestionRespuesta.IdPersona equals persona.IdPersona
                join tipoG in _repTipoGestion.Table on
                    gestionRespuesta.IdTipoGestion equals tipoG.IdTipoGestion
                where gestionRespuesta.IdGestion == idGestion
                select new PocDetalleGestion
                {
                    IdGestion = gestionRespuesta.IdGestion,
                    IdProducto = gestionRespuesta.IdProducto,
                    IdEmpresa = (int)gestionRespuesta.IdEmpresa,
                    IdContexto = (int)gestionRespuesta.IdContexto,
                    DescripcionGestion = _fGenerales.Traducir(tipoG.Descripcion,
                        "GE_TipoGestion", tipoG.IdTipoGestion, Context.Lang.Id, 0, Context.DefaultLang.Id),
                    DescripcionEstado = estados.Descripcion,
                    NombrePersona = persona.NombreCompleto,
                    Detalle = gestionRespuesta.Detalle,
                    FecInclusion = gestionRespuesta.FecInclusion,
                    IdTipoGestion = (int)gestionRespuesta.IdTipoGestion,
                    FecCierre = (DateTime)gestionRespuesta.FecCierre
                };
            var datosSalida = resultadoT.FirstOrDefault();
            if (datosSalida.IdProducto > 0)
            {
                var producto = ObtenerProducto(datosSalida.IdContexto, (int)datosSalida.IdProducto);
                datosSalida.DescripcionEstadoProducto = producto.DescripcionEstado;
                datosSalida.NumProducto = producto.NumProducto;
            }
            return datosSalida;
        }

        public PocDetalleGestion ObtenerUltimaGestionPorPersona(int idUsuario)
        {
            var idUltimaGestion = _repGestion.Table
                .Where(x => x.IdUsuarioIncluye == idUsuario)
                .OrderByDescending(x => x.FecInclusionAud)
                .Select(x => (int?)x.IdGestion)
                .FirstOrDefault();
            if (!idUltimaGestion.HasValue)
            {
                throw new CoreException("Error encontrando gestión", "Err_0028");
            }
            return ObtenerGestion(idUltimaGestion.Value);
        }

        public IEnumerable<GE_Gestion> ObtenerGestionPorProducto(int idProducto,
            EnumTipoGestion tipoGestion,
            int[] filtroEstado = null,
            int[] filtroEstadoExc = null)
        {
            var resultado = _repGestion.ConsultaGestiones(idTipoGestion: (int)tipoGestion,
                idProducto: idProducto,
                filtroEstado: filtroEstado,
                filtroEstadoExc: filtroEstadoExc);
            return resultado;
        }
        public void ModificarDatos(int idGestion, int? idSucursal, int? idEjecutivo)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                var update = new GE_Gestion { IdGestion = idGestion };
                if (idSucursal.HasValue)
                {
                    update.IdSucursal = idSucursal;
                }
                if (idEjecutivo.HasValue)
                {
                    update.IdEjecutivo = idEjecutivo;
                }
                _repGestion.Update(update, m => m.IdSucursal, m => m.IdEjecutivo);
                _repGestion.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void ModificarEstado(PocCambioEstadoGestion cambioEstado, bool ignorarCorreo = false)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                _repGestion.UpdateOn(x => x.IdGestion == cambioEstado.IdGestion,
                    z => new GE_Gestion
                    {
                        IdEstado = cambioEstado.IdEstado,
                        FecCierre = DateTime.Now
                    });
                IncluirHistorialGestion(cambioEstado);
                ts.Complete();
            }
            var estadosNotifica = _appUtilitarios.ObtieneInfoLista("LESTADOGESTION", null, "NOT");
            if (estadosNotifica.Any(m => m.IdCatalogo == cambioEstado.IdEstado) && !ignorarCorreo)
            {
                //TODO: HACER NOTIFICACION
                #region EnvioNotificaciones
                var usuario = _vwEmpresasUsuario.Table.FirstOrDefault(x => x.IdUsuario == cambioEstado.IdUsuarioIncluye);
                var tipoG = _repCatalogo.List("LESTADOGESTION").FirstOrDefault
                    (x => x.IdCatalogo == cambioEstado.IdEstado);
                var descripcionEstado = tipoG != null ? tipoG.Descripcion : string.Empty;
                NotificaGestion(new PocoQueue
                {
                    SubOrigin = "CEG",
                    UserCode = usuario.CodUsuario,
                    Data = new
                    {
                        NumGestion = cambioEstado.IdGestion,
                        Estado = descripcionEstado,
                        DetalleGestion = cambioEstado.Justificacion
                    }.ToJson()
                });
                #endregion
            }
        }

        public void EliminarGestionRegistrada(int idGestion)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                var validacion = new ValidationResponse();
                if (idGestion <= 0) { validacion.AddError("Err_0028"); }

                if (!validacion.IsValid)
                {
                    throw new CoreException("Datos faltantes al eliminar gestión", validation: validacion);
                }
                else
                {
                    _repHistGestion.DeleteOn(p => p.IdGestion == idGestion);
                    _repParamValor.DeleteOn(p => p.IdGestion == idGestion);
                    _repGestion.DeleteOn(p => p.IdGestion == idGestion);
                    _repHistGestion.UnidadTbjo.Save();
                    _repParamValor.UnidadTbjo.Save();
                    _repGestion.UnidadTbjo.Save();
                }
                ts.Complete();
            }
        }

        public bool ExisteGestionAnterior(int idProducto, EnumTipoGestion tipoGestion)
        {
            var excluirEstados = new int[] {(int)EnumEstadoGestion.Aprobada,
                          (int)EnumEstadoGestion.Anulada,
                          (int)EnumEstadoGestion.Cerrada};
            var resultado = _repGestion.ConsultaGestiones(idTipoGestion: (int)tipoGestion,
                            idProducto: idProducto,
                            filtroEstadoExc: excluirEstados);

            return resultado == null ? false : resultado.Any();
        }

        public List<GE_Gestion> GestionesAutorizadas(int idProducto,
            EnumTipoGestion tipoGestion)
        {
            var resultado = _repGestion.Table.Where(m =>
                m.IdProducto == idProducto &&
                m.IdTipoGestion == (int)tipoGestion &&
                m.IdEstado != (int)EnumEstadoGestion.Autorizada &&
                m.IdEstado != (int)EnumEstadoGestion.Anulada);
            return resultado.ToList();
        }

        public PocDetalleGestion BuscarGestion(int idGestion)
        {
            var resultado = _repGestion.Table.FirstOrDefault(m =>
                m.IdGestion == idGestion);
            return resultado.ProyectarComo<PocDetalleGestion>();
        }

        public int? BuscarGestionPendiente(int idEmpresa,
            EnumTipoGestion tipoGestion, int? idProducto = null)
        {
            var resultado = _repGestion.Table.FirstOrDefault(m =>
                m.IdEmpresa == idEmpresa &&
                m.IdTipoGestion == (int)tipoGestion &&
                (m.IdProducto == idProducto || idProducto == null) &&
                m.IdEstado == (int)EnumEstadoGestion.EnEdicion);
            return resultado != null ? (int?)resultado.IdGestion : null;
        }

        public int? BuscarGestionEnEdicion(int idPersona, EnumTipoGestion tipoGestion)
        {
            var resultado = _repGestion.Table.FirstOrDefault(m =>
                m.IdPersona == idPersona &&
                m.IdTipoGestion == (int)tipoGestion &&
                m.IdEstado == (int)EnumEstadoGestion.EnEdicion);
            return resultado != null ? (int?)resultado.IdGestion : null;
        }

        public PocDetalleGestion BuscarGestionRegistrada(int idGestion)
        {
            var resultado = _repGestion.Table.FirstOrDefault(m =>
                m.IdGestion == idGestion &&
                m.IdEstado == (int)EnumEstadoGestion.Registrada ||
                m.IdEstado == (int)EnumEstadoGestion.EnAnalisis ||
                m.IdEstado == (int)EnumEstadoGestion.Autorizada ||
                m.IdEstado == (int)EnumEstadoGestion.Condicionada ||
                m.IdEstado == (int)EnumEstadoGestion.RevisionPendiente);
            return resultado.ProyectarComo<PocDetalleGestion>();
        }

        public PocDetalleGestion BuscarGestionHabilitada(int idGestion)
        {
            var resultado = _repGestion.Table.FirstOrDefault(m =>
                m.IdGestion == idGestion &&
                m.IdEstado == (int)EnumEstadoGestion.Registrada ||
                m.IdEstado == (int)EnumEstadoGestion.EnEdicion);
            return resultado.ProyectarComo<PocDetalleGestion>();
        }

        public DataResult<PocDetalleGestion> ObtenerGestionesEmpresa
            (PocParametrosGestion parametros,
            DataRequest request, bool incluirOperativas = false)
        {
            var validation = new ValidationResponse();
            if (parametros == null)
            {
                validation.AddError("Err_0035");
            }
            if (!validation.IsValid)
            {
                throw new CoreException("Datos faltantes en Obtener lista gestión", validation: validation);
            }
            var administrator = InfoSesion.Info.IdTipoUsuario == (int)EnumTipoUsuario.Administrador ||
                InfoSesion.Info.IdTipoUsuario == (int)EnumTipoUsuario.SoloConsulta;
            var subAdministrator = InfoSesion.Info.IdTipoUsuario == (int)EnumTipoUsuario.SubAdministrador;

            var criteriaProductos = new Criteria<TC_vwTarjetaUsuario>();
            criteriaProductos.And(x => (!parametros.IdContexto.HasValue) ||
                (parametros.IdContexto == (int)EnumContexto.TC_CUENTA && x.IdCuenta == parametros.IdProducto) ||
                (parametros.IdContexto == (int)EnumContexto.TC_TARJETA && x.IdTarjeta == parametros.IdProducto));
            var productos = _repTarjetaUsuario.TarjetasPermitidas
                (criterias: criteriaProductos, filtroEstado: new int[1] { (int)EnumEstadoTarjeta.Vigentes },
                incluirOperativas: incluirOperativas);
            var subProductoIds = productos.Select(m => m.IdTarjeta).ToList();
            var personas = productos.Select(x => x.IdPersona).ToList();

            var criteria = new Criteria<GE_Gestion>();
            criteria.And(x => x.IdEmpresa == (parametros.IdEmpresa ?? InfoSesion.Info.IdEmpresa));

            var esAdmOperativo = _repAdmGrupoEmpresa.Table
                .Any(x => x.IdUsuario == InfoSesion.Info.IdUsuario);

            var aplicaLiquidacion = parametros.IdTipoGestion.HasValue &&
                    parametros.IdTipoGestion == (int)EnumTipoGestion.LiquidacionDeGastos &&
                    parametros.IdPersona != null;

            if ((!administrator && !subAdministrator && !esAdmOperativo) || aplicaLiquidacion)
            {
                var id = parametros.IdPersona != null ? parametros.IdPersona : InfoSesion.Info.IdPersona;
                criteria.And(x => x.IdPersona == id);
            }
            else if (esAdmOperativo)
            {
                criteria.And(x => personas.Contains(x.IdPersona));
            }
            if (subAdministrator)
            {
                criteria.And(x => subProductoIds.Contains(x.IdProducto ?? 0));
            }
            if (parametros.IdContexto.HasValue && parametros.IdProducto.HasValue)
            {
                criteria.And(x =>
                    (!x.IdProducto.HasValue && parametros.IdContexto == (int)EnumContexto.TC_CUENTA && administrator) ||
                    (x.IdContexto == (int)EnumContexto.TC_CUENTA && x.IdProducto == parametros.IdProducto) ||
                    (x.IdContexto == (int)EnumContexto.TC_TARJETA && subProductoIds.Contains(x.IdProducto ?? 0)));
            }
            if (parametros.IdTipoGestion > 0)
            {
                criteria.And(m => m.IdTipoGestion == parametros.IdTipoGestion);
            }
            if (parametros.IdExportado.HasValue)
            {
                criteria.And(m => (parametros.IdExportado == (int)EnumIdSiNo.No &&
                    !m.IdExportado.HasValue) || m.IdExportado == parametros.IdExportado);
            }
            if (parametros.IdEstado.HasValue && parametros.IdEstado > 0)
            {
                criteria.And(x => x.IdEstado == parametros.IdEstado.Value);
            }
            if (parametros.FecDesde.HasValue && parametros.FecHasta.HasValue)
            {
                var desde = parametros.FecDesde.Value;
                var hasta = parametros.FecHasta.Value.AddDays(Time.DayFactor);
                criteria.And(x => x.FecInclusion >= desde && x.FecInclusion <= hasta);
            }

            var query =
                (from ges in _repGestion.SelectBy(criteria)
                 join tip in _repTipoGestion.Table
                     on ges.IdTipoGestion equals tip.IdTipoGestion
                 join per in _repPersona.Table
                     on ges.IdPersona equals per.IdPersona
                 join usr in _vwEmpresasUsuario.Table on new
                 {
                     ges.IdEmpresa,
                     IdUsuario = ges.IdUsuarioIncluye
                 } equals new
                 {
                     usr.IdEmpresa,
                     IdUsuario = (int?)usr.IdUsuario
                 }
                 join est in _repCatalogo.List("LESTADOGESTION")
                     on ges.IdEstado equals est.IdCatalogo
                 join suc in _repSucursal.Table
                     on ges.IdSucursal equals suc.IdSucursal into sucGes
                 from suc in sucGes.DefaultIfEmpty()
                 join ejePer in
                     (from eje in _repEjecutivo.Table
                      join pere in _repPersona.Table
                          on eje.IdPersona equals pere.IdPersona
                      select new { eje, pere })
                     on ges.IdEjecutivo equals ejePer.eje.IdEjecutivo into ejePerGes
                 from ejePer in ejePerGes.DefaultIfEmpty()
                 join pval in _repParamValor.Table
                     on ges.IdGestion equals pval.IdGestion into pvalGes
                 from pval in pvalGes.DefaultIfEmpty()
                 where tip.IdVisualizaAdmin == (int)EnumIdSiNo.No
                 orderby ges.FecInclusion descending
                 select new { ges, per, est, usr, tip, ejePer, suc, pval }).ToList()
                .Select(x => new PocDetalleGestion
                {
                    IdGestion = x.ges.IdGestion,
                    IdEmpresa = x.ges.IdEmpresa,
                    IdPersona = x.ges.IdPersona,
                    Detalle = x.ges.Detalle,
                    IdContexto = x.ges.IdContexto,
                    IdExportado = x.ges.IdExportado,
                    FecCierre = x.ges.FecCierre.Value,
                    FecInclusion = x.ges.FecInclusion.Date,
                    HoraInclusion = new TimeSpan(
                        x.ges.FecInclusionAud.Hour,
                        x.ges.FecInclusionAud.Minute,
                        x.ges.FecInclusionAud.Second),
                    IdProducto = x.ges.IdProducto,
                    NombrePersona = x.per.NombreCompleto,
                    CodUsuarioEmpresa = x.usr.CodUsuarioEmpresa,
                    IdEstado = x.ges.IdEstado,
                    DescripcionEstado = x.est.Descripcion,
                    IdTipoGestion = x.ges.IdTipoGestion,
                    DescripcionGestion = _fGenerales.Traducir(x.tip.Descripcion,
                        "GE_TipoGestion", x.ges.IdTipoGestion, Context.Lang.Id, 0, Context.DefaultLang.Id),
                    Ejecutivo = x.ejePer != null ? x.ejePer.pere.NombreCompleto : string.Empty,
                    Sucursal = x.suc != null ? x.suc.Descripcion : string.Empty,
                    ValorParametro = x.pval != null ? x.pval.Valor : string.Empty,
                    IdUsuarioIncluye = (int)x.ges.IdUsuarioIncluye,
                    IdContextoMovimientos = x.ges.IdContextoMovimientos
                });
            var result = query.ToList();
            Func<int, int, dynamic> _lamb = (a, b) =>
            {
                var contextoCta = a == (int)EnumContexto.TC_CUENTA;
                var contextoTar = a == (int)EnumContexto.TC_TARJETA;
                var producto = productos.FirstOrDefault(x =>
                    (contextoCta && b == x.IdCuenta) ||
                    (contextoTar && b == x.IdTarjeta));
                var numProducto = contextoCta ?
                    (producto != null ? producto.NumCuenta : string.Empty) :
                    (producto != null ? producto.NumTarjeta : string.Empty);
                var autoriza = (producto != null && producto.IdAutoriza != null) ?
                    producto.IdAutoriza == (int)EnumIdSiNo.Si : !Context.IsEditor;
                var soloConsulta = producto != null ? producto.IdSoloConsulta == (int)EnumIdSiNo.Si : false;
                return new
                {
                    NumProducto = numProducto,
                    Autoriza = autoriza,
                    SoloConsulta = soloConsulta
                };
            };
            result.Update(up =>
            {
                var prod = _lamb(up.IdContexto, up.IdProducto ?? 0);
                up.NumProducto = prod != null ? prod.NumProducto : null;
                up.Autoriza = prod != null ? prod.Autoriza : false;
                up.SoloConsulta = prod != null ? prod.SoloConsulta : false;
            });
            return result.ToDataResult(request);
        }

        public IEnumerable<PocDetalleGestion> ConsultarGestionesRealizadas(PocParametrosGestion parametros)
        {
            var criteria1 = new Criteria<GE_Gestion>();
            var tipoU = Context.UserType;
            if (!tipoU.MultiEntidad)
            {
                var entidadAdm = InfoSesion.Info.IdCompania;
                criteria1.And(m => m.IdCompania == entidadAdm);
                if (tipoU.PorUnidad)
                {
                    var sucursal = InfoSesion.Info.IdUnidad;
                    criteria1.And(m => m.IdUnidad == sucursal);
                }
            }
            if (parametros != null)
            {
                if (parametros.IdGestion.HasValue)
                {
                    criteria1.And(m => m.IdGestion == parametros.IdGestion.Value);
                }
                else
                {
                    if (parametros.IdPersona > 0)
                    {
                        criteria1.And(m => m.IdPersona == parametros.IdPersona);
                    }
                    if (parametros.IdEmpresa > 0)
                    {
                        criteria1.And(m => m.IdEmpresa == parametros.IdEmpresa);
                    }
                    if (parametros.IdEstado > 0)
                    {
                        if (parametros.IdEstado == 1)
                        {
                            criteria1.And(m => m.IdEstado == (int)EnumEstadoGestion.Registrada
                                || m.IdEstado == (int)EnumEstadoGestion.EnEdicion
                                || m.IdEstado == (int)EnumEstadoGestion.PendienteCorrecion);
                        }
                        else
                        {
                            criteria1.And(m => m.IdEstado == parametros.IdEstado);
                        }
                    }
                    if (parametros.IdTipoGestion > 0)
                    {
                        criteria1.And(m => m.IdTipoGestion == parametros.IdTipoGestion);
                    }
                    if (parametros.FecDesde.HasValue && parametros.FecHasta.HasValue)
                    {
                        var hasta = parametros.FecHasta.Value.AddDays(1);
                        criteria1.And(x => x.FecInclusion >= parametros.FecDesde
                        && x.FecInclusion <= hasta);
                    }
                }
            }
            else
            {
                criteria1.And(m => m.IdEstado != null);
            }
            var criteria2 = new Criteria<GE_TipoGestion>();
            criteria2.And(m => m.ReqAdministracion == (int)EnumIdSiNo.Si);
            var query =
                from ges in _repGestion.SelectBy(criteria1)
                join tip in _repTipoGestion.SelectBy(criteria2)
                    on ges.IdTipoGestion equals tip.IdTipoGestion
                join tgRol in _repTipoGestionRol.Table
                    on tip.IdTipoGestion equals tgRol.IdTipoGestion into tipRol
                from tgRol in tipRol.DefaultIfEmpty()
                join tipp in _repCatalogo.List("LTIPOPRODUCTO")
                    on ges.IdTipoProducto equals tipp.IdCatalogo
                join est in _repCatalogo.List("LESTADOGESTION")
                    on ges.IdEstado equals est.IdCatalogo
                join per in _repPersona.Table
                    on ges.IdPersona equals per.IdPersona
                join emp in _repPersona.Table
                    on ges.IdEmpresa equals emp.IdPersona
                join suc in _repSucursal.Table
                    on ges.IdSucursal equals suc.IdSucursal into sucGes
                from suc in sucGes.DefaultIfEmpty()
                join ejePer in
                    (from eje in _repEjecutivo.Table
                     join pere in _repPersona.Table
                         on eje.IdPersona equals pere.IdPersona
                     select new { eje, pere })
                    on ges.IdEjecutivo equals ejePer.eje.IdEjecutivo into ejePerGes
                from ejePer in ejePerGes.DefaultIfEmpty()
                join pval in _repParamValor.Table
                    on ges.IdGestion equals pval.IdGestion into pvalGes
                from pval in pvalGes.DefaultIfEmpty()
                orderby ges.FecInclusion descending
                select new { ges, tipp, per, est, emp, tip, ejePer, suc, pval, tgRol };
            var gestiones = query.ToList().Select(x =>
            {
                var prod = x.ges.IdProducto.HasValue ?
                       ObtenerProducto(x.ges.IdContexto,
                       x.ges.IdProducto.Value) : null;

                return new PocDetalleGestion
                {
                    IdGestion = x.ges.IdGestion,
                    IdEmpresa = x.ges.IdEmpresa,
                    IdPersona = x.ges.IdPersona,
                    Detalle = x.ges.Detalle,
                    IdContexto = x.ges.IdContexto,
                    IdProducto = x.ges.IdProducto,
                    IdTipoProducto = x.ges.IdTipoProducto,
                    TipoProducto = x.tipp.Descripcion,
                    IdExportado = x.ges.IdExportado,
                    FecInclusion = x.ges.FecInclusion.Date,
                    HoraInclusion = new TimeSpan(
                        x.ges.FecInclusionAud.Hour,
                        x.ges.FecInclusionAud.Minute,
                        x.ges.FecInclusionAud.Second),
                    NombrePersona = x.per.NombreCompleto,
                    Empresa = x.emp.NombreCompleto,
                    IdEstado = x.ges.IdEstado,
                    DescripcionEstado = x.est.Descripcion,
                    IdTipoGestion = x.ges.IdTipoGestion,
                    DescripcionGestion = x.tip.Descripcion,
                    Ejecutivo = x.ejePer != null ? x.ejePer.pere.NombreCompleto : string.Empty,
                    Sucursal = x.suc != null ? x.suc.Descripcion : string.Empty,
                    ValorParametro = x.pval != null ? x.pval.Valor : string.Empty,
                    FecCierre = x.ges.FecCierre != null ? x.ges.FecCierre.Value.Date : x.ges.FecInclusion.Date,
                    IdRolAsociado = x.tgRol != null ? x.tgRol.IdRol : null as int?,
                    NumProducto = prod != null ? prod.NumProducto : null,
                    DescripcionEstadoProducto = prod != null ? prod.DescripcionEstado : null,
                    IdUsuarioIncluye = x.ges.IdUsuarioIncluyeAud
                };
            });
            IEnumerable<PocDetalleGestion> gestionesAExcluir = gestiones.Where(x => x.IdRolAsociado != null && x.IdRolAsociado != InfoSesion.Info.IdRole);
            if (gestionesAExcluir != null && gestionesAExcluir.Count() > 0)
            {
                int[] idGestExcluir = gestionesAExcluir.Select(x => x.IdGestion).ToArray();
                gestiones = gestiones.Where(x => !idGestExcluir.Contains(x.IdGestion));
            }
            return gestiones.ToList();
        }

        public DataResult<PocDetalleHistoriaGestion> ObtenerHistorial(int idGestion, DataRequest request)
        {
            var resultado =
                from historialG in _repHistGestion.Table.ToList()
                join estados in _repCatalogo.List("LESTADOGESTION") on
                    historialG.IdEstado equals estados.IdCatalogo
                where historialG.IdGestion == idGestion
                select new PocDetalleHistoriaGestion
                {
                    Id = historialG.Id,
                    IdEstado = historialG.IdEstado,
                    DescripcionEstado = estados.Descripcion,
                    FecEstado = historialG.FecEstado.Date,
                    HoraEstado = new TimeSpan(historialG.HoraEstado.Hour, historialG.HoraEstado.Minute,
                        historialG.HoraEstado.Second),
                    Detalle = historialG.Detalle
                };

            return resultado.ToDataResult(request);
        }

        public void IncluirHistorialGestion(string justificacion, int idEstado, int idGestion)
        {
            if (justificacion == null)
            {
                justificacion = _repGestion.Table.FirstOrDefault(x => x.IdGestion == idGestion).Detalle;
            }
            using (var ts = new TransactionScope())
            {
                GE_HistorialGestion HistGestion = new GE_HistorialGestion();
                HistGestion.Detalle = justificacion;
                HistGestion.IdEstado = idEstado;
                HistGestion.IdGestion = idGestion;
                HistGestion.IdUsuario = InfoSesion.Info.IdUsuario;
                HistGestion.HoraEstado = DateTime.Now;
                HistGestion.FecEstado = DateTime.Now;
                _repHistGestion.Insert(HistGestion);
                _repHistGestion.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void IncluirHistorialGestion(PocCambioEstadoGestion modelo)
        {
            IncluirHistorialGestion(modelo.Justificacion, modelo.IdEstado, modelo.IdGestion);
        }

        public List<PocTipoGestion> ListaTipoGestion()
        {
            var resultado = _repTipoGestion.Table.ToList();
            return resultado.ProyectarComo<PocTipoGestion>();
        }

        public void IncluirValorParametro(int idGestion, string jsonValue)
        {
            var paramValor = new GE_ParametrosValor();
            paramValor.IdGestion = idGestion;
            paramValor.Valor = jsonValue;
            paramValor.IdAccion = null;
            _repParamValor.Insert(paramValor);
            _repParamValor.UnidadTbjo.Save();
        }

        public string ObtenerValorParametro(int idGestion, bool descomprime = true)
        {
            var resultado = _repParamValor.Table.FirstOrDefault(m => m.IdGestion == idGestion);
            if (resultado != null)
            {
                return descomprime ? resultado.Valor.DecompressStr() : resultado.Valor;
            }
            return null;
        }

        public void ModificarValorParametro(int idGestion, string jsonValue)
        {
            using (var ts = new TransactionScope())
            {
                _repParamValor.UpdateOn(m => m.IdGestion == idGestion, p => new GE_ParametrosValor
                {
                    Valor = jsonValue
                });
                _repParamValor.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public string ObtenerDetalleTipoGestion(EnumTipoGestion tipoGestion)
        {
            var tipoGestionEn = _repTipoGestion.SelectById((int)tipoGestion);
            if (tipoGestionEn != null)
            {
                return _fGenerales.Traducir(tipoGestionEn.Descripcion,
                        "GE_TipoGestion", tipoGestionEn.IdTipoGestion,
                        Context.Lang.Id, 0, Context.DefaultLang.Id);
            }
            return string.Empty;
        }

        public PocProductoGestion ObtenerProducto(int idContexto, int idProducto)
        {
            var resultado = new PocProductoGestion();
            if (idContexto == (int)EnumContexto.TC_CUENTA)
            {
                var cuenta = _repCuenta.Table.FirstOrDefault(x => x.IdCuenta == idProducto);
                resultado.DescripcionEstado = _repCatalogo.ListSp
                    ("LESTADOCUENTA", cuenta.IdEstadoCta)
                    .FirstOrDefault().Descripcion;
                resultado.NumProducto = cuenta.NumCuenta;
                resultado.IdPersona = cuenta.IdPersonaTitular;
                resultado.IdTipoProducto = (int)EnumTipoProducto.TarjetaCredito;
            }
            else if (idContexto == (int)EnumContexto.TC_TARJETA)
            {
                var tarjeta = _repTarjeta.SelectById(idProducto);
                resultado.DescripcionEstado = _repCatalogo.ListSp
                    ("LESTADOTARJETA", tarjeta.IdEstadoTarjeta)
                    .FirstOrDefault().Descripcion;
                resultado.NumProducto = tarjeta.NumTarjeta;
                resultado.IdPersona = tarjeta.IdPersona;
                resultado.IdTipoProducto = (int)EnumTipoProducto.TarjetaCredito;
            }
            return resultado;
        }

        public CL_Persona ObtenerInfoPersona(int? IdPersona)
        {
            var criteria = new Criteria<CL_Persona>();
            criteria.And(per => per.IdPersona == IdPersona);
            return _repPersona.SelectBy(criteria).First();
        }

        public void NotificaGestion(PocoQueue notificacion, bool notificaProcesador = false, bool notificaExterno = false)
        {
            try
            {
                var emailAddresses = new List<string>();
                notificacion.MessageType = "0102";
                notificacion.EntityId = InfoSesion.Info.IdCompania;
                notificacion.CompanyId = InfoSesion.Info.IdEmpresa;
                notificacion.OriginId = notificacion.OriginId != 0 ? notificacion.OriginId : (int)EnumContexto.GE_TIPOGESTION;

                var dataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(notificacion.Data);
                dataDictionary.Add("IdEmpresa", InfoSesion.Info.IdEmpresa);
                dataDictionary.Add("IdPersona", InfoSesion.Info.IdPersona);
                notificacion.Data = dataDictionary.ToJson();

                var remitente = _repParamCompania.Table.FirstOrDefault(w =>
                   w.IdParametro == "REMITENTE_CORREO_GESTION" &&
                   w.IdCompania == InfoSesion.Info.IdCompania);
                if (remitente != null)
                {
                    notificacion.Sender = remitente.ValParametro;
                }
                var tipo = _repTipoGestion.SelectById(notificacion.OriginIdVal);
                if (tipo != null && tipo.ReqAdministracion == (int)EnumIdSiNo.Si)
                {
                    var idParamCorreo = "CORREO_ATENCIONGESTIONES";
                    if (notificaExterno)
                    {
                        var entidadBase = _repParamSistema.Table.FirstOrDefault
                            (x => x.IdParametro == "ID_PERSONA_ENTIDAD_BASE");
                        if (InfoSesion.Info.IdCompania != int.Parse(entidadBase.ValParametro))
                        {
                            idParamCorreo = "CORREOS_ATENCIONGESTIONES_EXT";
                        }
                    }
                    var parameter = _repParamCompania.Table.FirstOrDefault(w =>
                        w.IdParametro == idParamCorreo &&
                        w.IdCompania == InfoSesion.Info.IdCompania);
                    if (parameter != null)
                    {
                        emailAddresses.AddRange(parameter.ValParametro.Split(';'));
                    }
                }
                if (notificaProcesador)
                {
                    var procesador = _repParamCompania.Table.FirstOrDefault(x =>
                        x.IdParametro == "CORREO_ATENCION_PROCESADOR" &&
                        x.IdCompania == InfoSesion.Info.IdCompania);
                    emailAddresses.Add(procesador.ValParametro);
                }
                if (emailAddresses.Count > 0)
                {
                    var recipients = new List<PocoQueueDestinatario>();
                    recipients.Add(new PocoQueueDestinatario
                    {
                        Canal = CanalNotificacion.Email,
                        Valores = emailAddresses.Select(y => new PocoQueueDestinatarioValor { Valor = y }).ToArray()
                    });
                    recipients.Add(new PocoQueueDestinatario { Canal = CanalNotificacion.Sms, Valores = new PocoQueueDestinatarioValor[0] });
                    notificacion.Recipients = recipients;
                }
                _appNotificacion.ProcesarNotificaciones(notificacion);
            }
            catch (Exception ex)
            {
                Logger.Log(string.Format("PocoQueue: {1} / Error: {0}", notificacion.ToJson(), ex.FullStackTrace()),
                    titulo: ex.Message, idEvento: 200, severity: System.Diagnostics.TraceEventType.Error);
            }
        }

        public void NotificaGestionInterno(PocoQueue notificacion, string idParamCorreo = null)
        {
            try
            {
                var emailAddresses = new List<string>();
                notificacion.MessageType = "0102";
                notificacion.EntityId = InfoSesion.Info.IdCompania;
                notificacion.CompanyId = InfoSesion.Info.IdEmpresa;
                notificacion.OriginId = notificacion.OriginId != 0 ? notificacion.OriginId : (int)EnumContexto.GE_TIPOGESTION;

                var dataDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(notificacion.Data);
                dataDictionary.Add("IdEmpresa", InfoSesion.Info.IdEmpresa);
                dataDictionary.Add("IdPersona", InfoSesion.Info.IdPersona);
                notificacion.Data = dataDictionary.ToJson();

                var remitente = _repParamCompania.Table.FirstOrDefault(w =>
                   w.IdParametro == "REMITENTE_CORREO_GESTION" &&
                   w.IdCompania == InfoSesion.Info.IdCompania);
                if (remitente != null)
                {
                    notificacion.Sender = remitente.ValParametro;
                }

                var idParameter = string.IsNullOrEmpty(idParamCorreo) ? "CORREO_ATENCIONGESTIONES" : idParamCorreo;

                var parameter = _repParamCompania.Table.FirstOrDefault(w =>
                    w.IdParametro == idParameter &&
                    w.IdCompania == InfoSesion.Info.IdCompania);
                if (parameter != null)
                {
                    emailAddresses.AddRange(parameter.ValParametro.Split(';'));
                }

                if (emailAddresses.Count > 0)
                {
                    var recipients = new List<PocoQueueDestinatario>();
                    recipients.Add(new PocoQueueDestinatario
                    {
                        Canal = CanalNotificacion.Email,
                        Valores = emailAddresses.Select(y => new PocoQueueDestinatarioValor { Valor = y }).ToArray()
                    });
                    recipients.Add(new PocoQueueDestinatario { Canal = CanalNotificacion.Sms, Valores = new PocoQueueDestinatarioValor[0] });
                    notificacion.Recipients = recipients;
                }

                notificacion.Language = notificacion.Language ?? ((InfoSesion.Lang ?? Config.LANG_DEFAULT).Code);
                ThreadPool.QueueUserWorkItem(state => _appNotificacion.EnviarNotificacion(notificacion));
            }
            catch (Exception ex)
            {
                Logger.Log(string.Format("PocoQueue: {1} / Error: {0}", notificacion.ToJson(), ex.FullStackTrace()),
                    titulo: ex.Message, idEvento: 200, severity: System.Diagnostics.TraceEventType.Error);
            }
        }

        //public void NotificaGestion(int tipoGestion,
        //    string subject, string message, int tipoTarjeta)
        //{
        //    try
        //    {
        //        NotificaGestion(subject, message, InfoSesion.Info.Correo);
        //        var tipo = _repTipoGestion.SelectById((int)tipoGestion);
        //        if (tipo != null && tipo.ReqAdministracion == (int)EnumIdSiNo.Si)
        //        {
        //            var parameter = _repParamCompania.Table
        //                    .FirstOrDefault(w => w.IdParametro == "CORREO_ATENCIONGESTIONES" &&
        //                           w.IdCompania == InfoSesion.Info.IdCompania);
        //            if (parameter != null)
        //            {
        //                var direcciones = parameter.ValParametro.Split(';');
        //                NotificaGestion(subject, message, direcciones);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Log(ex.FullStackTrace(), ex.Message);
        //    }
        //}

        //public void NotificaGestion(string subject, string msg, params string[] addresses)
        //{
        //    var remitente = _repParamCompania.Table.Where(w =>
        //        w.IdParametro == "REMITENTE_CORREO_GESTION" &&
        //        w.IdCompania == InfoSesion.Info.IdCompania).FirstOrDefault();

        //    SenderConfig sender = null;
        //    if (remitente != null)
        //    {
        //        sender = new SenderConfig(remitente.ValParametro);
        //    }
        //    foreach (var country in addresses)
        //    {
        //        var emailConfig = new EmailConfig(subject, msg);
        //        emailConfig.AddTo(country);
        //        if (sender != null)
        //        {
        //            _repEmail.EnviarEmail(sender, emailConfig);
        //        }
        //        else
        //        {
        //            _repEmail.EnviarInfoEmail(emailConfig);
        //        }
        //    }
        //}

        //public void CorreoContactenos(string msj, string correo, string nombreCompleto)
        //{
        //    var senderConfig = new SenderConfig(correo, nombreCompleto);
        //    var emailInterno = new EmailConfig("Mensaje de Contactenos", msj);
        //    var correoAtencion = _repParamSistema.SelectById("CORREO_ATENCIONGESTIONES").ValParametro;
        //    var direcciones = correoAtencion.Split(';');
        //    foreach (var country in direcciones)
        //    {
        //        emailInterno.AddTo(country);
        //        _repEmail.EnviarEmail(senderConfig, emailInterno);
        //    }
        //}

        public void MarcarExportadas(params int[] gestiones)
        {
            if (gestiones != null)
            {
                using (var ts = new TransactionScope())
                {
                    _repGestion.UpdateOn(x => gestiones.Contains(x.IdGestion),
                        up => new GE_Gestion
                        {
                            IdExportado = (int)EnumIdSiNo.Si
                        });
                    ts.Complete();
                }
            }
        }

        #region Reporte Gestiones por Usuario

        public PocReporte ObtenerReporteGestiones(DateTime fecDesde,
            DateTime fecHasta, bool esExcel = true)
        {
            var parameters = new List<ReportParam>();
            Reporte resultado = null;
            parameters.Add(new ReportParam { Name = "fecDesde", Value = fecDesde.ToString("MM-dd-yyyy") });
            parameters.Add(new ReportParam { Name = "fecHasta", Value = fecHasta.ToString("MM-dd-yyyy") });
            parameters.Add(new ReportParam { Name = "idEmpresa", Value = InfoSesion.Info.IdEmpresa.ToString() });
            if (esExcel)
            {
                resultado = _repReportServer.ExportExcel("/Reports/Gestiones/r-GestionesUsuarioExcel", parameters.ToArray());
                return new PocReporte
                {
                    Encoding = resultado.Encoding,
                    Extension = resultado.Extension,
                    MimeType = resultado.MimeType,
                    Output = resultado.Output
                };
            }
            resultado = _repReportServer.ExportPdf("/Reports/Gestiones/r-GestionesUsuario", parameters.ToArray());
            return new PocReporte
            {
                Encoding = resultado.Encoding,
                Extension = resultado.Extension,
                MimeType = resultado.MimeType,
                Output = resultado.Output
            };
        }

        #endregion


        public void ModificaLimiteGlobal(PocModificaLimite cambioLimite)
        {
            using (var ts = new TransactionScope())
            {
                var idTipoGestion = (int)EnumTipoGestion.ModificaLimiteCuenta;
                var tc = _repVwTarjetasCuenta.Table.FirstOrDefault(tar => tar.NumTarjeta == cambioLimite.NumTarjeta);
                if (tc == null)
                {
                    throw new CoreException("Error al cambiar el limite de la tarjeta", "Err_0002");
                }

                //Actualiza la tarjeta titular por MQ                              
                if (cambioLimite.IdEstado == (int)EnumEstadoGestion.Cerrada)
                {
                    _opTarjeta.AplicarCambioLimiteTarjeta(cambioLimite.NumTarjeta, cambioLimite.MonLimiteNuevo,
                        (int)EnumTipoGestion.ModificaLimiteCuenta, cambioLimite.VigenteHasta);

                    cambioLimite.IdEstado = cambioLimite.VigenteHasta.HasValue ? (int)EnumEstadoGestion.AprobacionTransitoria : cambioLimite.IdEstado;

                    #region EnvioNotificaciones

                    NotificaGestionInterno(new PocoQueue
                    {
                        OriginIdVal = (int)EnumTipoGestion.ModificaLimiteCuenta,
                        SubOrigin = "APR",
                        EntityId = InfoSesion.Info.IdCompania,
                        Data = new
                        {
                            CardholderName = tc.NombrePersona,
                            NumGestion = cambioLimite.IdGestion,
                            IdCuenta = cambioLimite.IdCuenta,
                            NumCuenta = cambioLimite.NumTarjeta,
                            IdTarjeta = cambioLimite.IdTarjeta,
                            CardNumber = cambioLimite.NumTarjeta,
                            LimiteActual = cambioLimite.MonLimiteActual,
                            LimiteNuevo = cambioLimite.MonLimiteNuevo
                        }.ToJson()
                    }, "CORREO_OPERACIONESTARJETAS");

                    #endregion
                }

                var gestionCambioLimite = ObtenerValorParametro(cambioLimite.IdGestion.Value, true).FromJson<PocModificaLimite>();

                if (!cambioLimite.Editando)
                {
                    var cambioEstado = new PocCambioEstadoGestion
                    {
                        IdEstado = cambioLimite.IdEstado,
                        IdGestion = (int)cambioLimite.IdGestion,
                        Justificacion = cambioLimite.Observaciones,
                        IdUsuarioIncluye = InfoSesion.Info.IdUsuario,
                        IdTipoGestion = idTipoGestion
                    };

                    ModificarEstado(cambioEstado, true);
                }
                else
                {
                    if (cambioLimite.MonLimiteNuevo < cambioLimite.MonLimiteActual)
                    {
                        _opTarjeta.ValidarCambioLimiteTarjeta(cambioLimite.NumTarjeta, cambioLimite.MonLimiteNuevo, idTipoGestion, cambioLimite.VigenteHasta);
                    }
                    var idEstado = cambioLimite.IdEstado > 0 ? cambioLimite.IdEstado : (int)EnumEstadoGestion.Registrada;
                    IncluirHistorialGestion(cambioLimite.DetalleGestion, idEstado, cambioLimite.IdGestion.Value);
                    _repGestion.UpdateOn(x => x.IdGestion == cambioLimite.IdGestion,
                             z => new GE_Gestion
                             {
                                 Detalle = cambioLimite.DetalleGestion
                             });
                }
                gestionCambioLimite.MonLimiteNuevo = cambioLimite.MonLimiteNuevo;
                gestionCambioLimite.VigenteHasta = cambioLimite.VigenteHasta;
                gestionCambioLimite.IdGestion = cambioLimite.IdGestion;
                gestionCambioLimite.Observaciones = cambioLimite.Observaciones;
                gestionCambioLimite.DetalleGestion = cambioLimite.DetalleGestion;
                ModificarValorParametro(gestionCambioLimite.IdGestion.Value, gestionCambioLimite.ToJson().CompressStr());

                if (cambioLimite.IdEstado == (int)EnumEstadoGestion.Rechazada)
                {
                    #region EnvioNotificaciones

                    NotificaGestionInterno(new PocoQueue
                    {
                        OriginIdVal = (int)EnumTipoGestion.ModificaLimiteCuenta,
                        SubOrigin = "RCH",
                        EntityId = InfoSesion.Info.IdCompania,
                        Data = new
                        {
                            CardholderName = tc.NombrePersona,
                            NumGestion = cambioLimite.IdGestion,
                            IdCuenta = cambioLimite.IdCuenta,
                            NumCuenta = cambioLimite.NumTarjeta,
                            IdTarjeta = cambioLimite.IdTarjeta,
                            CardNumber = cambioLimite.NumTarjeta,
                            LimiteActual = cambioLimite.MonLimiteActual,
                            LimiteNuevo = cambioLimite.MonLimiteNuevo
                        }.ToJson()
                    }, "CORREO_OPERACIONESTARJETAS");

                    #endregion
                }

                ts.Complete();
            }
        }

        public List<PocReporteModificacionesLimite> ObtenerModificacionesLimite(PocReporteModificacionesLimite filtro)
        {
            var modificacionesLimite = _funcionesGestion.ObtieneModificacionesLimite(new ParamReporteModificacionesLimite
            {
                FechaDesde = filtro.FechaDesde,
                FechaHasta = filtro.FechaHasta.AddDays(Time.DayFactor),
                IdEstado = filtro.IdEstado,
                NumCuenta = filtro.NumCuenta
            }).ToList();

            return modificacionesLimite.Select(x => new PocReporteModificacionesLimite
             {
                 IdGestion = x.IdGestion,
                 UsuarioIngresa = x.UsuarioIngresa,
                 UsuarioActualiza = x.UsuarioActualiza,
                 FechaIngreso = x.FechaIngreso,
                 FechaActualiza = x.FechaActualiza,
                 NumCuenta = x.NumCuenta,
                 NombreTitular = x.NombreTitular,
                 DesEstado = x.DesEstado,
                 MonLimiteActual = x.ParamValor.DecompressStr().FromJson<PocModificaLimite>().MonLimiteNuevo,
                 MonLimiteAnterior = x.ParamValor.DecompressStr().FromJson<PocModificaLimite>().MonLimiteActual,
                 Detalle = x.Detalle
             }).ToList();
        }
    }
}
﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Tarjetas;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Efectivo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Efectivo
{
    public class AppMovimientos : AplicacionBase, IAppMovimientos
    {
        private IConsultasTarjeta _consultaTarjetas;
        private IRepositorioMoneda _repMoneda;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<EF_Movimiento> _repMovimientos;
        private IRepositorioMnb<EF_TarifaKilometraje> _repTarifasKM;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;
        private IRepositorioMnb<CL_vwEmpresasUsuario> _vwEmpresasUsuario;
        private IRepositorioMnb<EF_ArchivoMovimiento> _repArchivoMovimiento;
        private IRepositorioMnb<EF_ArchivoTarjeta> _repArchivoTarjeta;

        public AppMovimientos(
            IConsultasTarjeta consultaTarjetas,
            IRepositorioMoneda repMoneda,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<EF_Movimiento> repMovimientos,
            IRepositorioMnb<EF_TarifaKilometraje> repTarifasKM,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema,
            IRepositorioMnb<CL_vwEmpresasUsuario> vwEmpresasUsuario,
            IRepositorioMnb<EF_ArchivoMovimiento> repArchivoMovimiento,
            IRepositorioMnb<EF_ArchivoTarjeta> repArchivoTarjeta)
        {
            _consultaTarjetas = consultaTarjetas;
            _repMoneda = repMoneda;
            _repCatalogo = repCatalogo;
            _repPersona = repPersona;
            _repMovimientos = repMovimientos;
            _repTarifasKM = repTarifasKM;
            _repParamSistema = repParamSistema;
            _vwEmpresasUsuario = vwEmpresasUsuario;
            _repArchivoMovimiento = repArchivoMovimiento;
            _repArchivoTarjeta = repArchivoTarjeta;
        }

        public void Eliminar(PocMovimientosEfectivo registro)
        {
            using (var ts = new TransactionScope())
            {
                _repArchivoMovimiento.DeleteOn(p => p.IdMovimiento == registro.IdMovimiento);

                var entrada = registro.ProyectarComo<EF_Movimiento>();
                _repMovimientos.Delete(entrada);
                _repMovimientos.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocMovimientosEfectivo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    if (registro.IdContexto == (int)EnumContexto.EF_KILOMETRAJE)
                    {
                        var antiguedad = ((DateTime)registro.FecMovimiento).Year - ((int)registro.Modelo);
                        var kilometros = (registro.NumKmLlegada.Value - registro.NumKmSalida.Value);
                        registro.MonMovimiento = MontoKilometraje(((int)registro.IdTipoVehiculo),
                            antiguedad < 0 ? 0 : antiguedad, kilometros);
                    }

                    var idmov = _fGenerales.ObtieneSgteSecuencia("SEQ_IdMovimientoEfectivo");
                    var entrada = registro.ProyectarComo<EF_Movimiento>();
                    entrada.IdMovimiento = idmov;
                    entrada.IdEmpresa = InfoSesion.Info.IdEmpresa;
                    _repMovimientos.Insert(entrada);
                    _repMovimientos.UnidadTbjo.Save();
                    if (registro.Archivos != null)
                    {
                        foreach (PocArchivoMovimiento file in registro.Archivos)
                        {
                            file.IdMovimiento = idmov;
                            _repArchivoMovimiento.Insert(file.ProyectarComo<EF_ArchivoMovimiento>());
                            _repArchivoMovimiento.UnidadTbjo.Save();
                        }
                    }
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocMovimientosEfectivo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocMovimientosEfectivo> Listar(
            PocMovimientosEfectivo filtro, DataRequest request)
        {
            var criteria = new Criteria<EF_Movimiento>();
            criteria.And(m => m.IdEmpresa == InfoSesion.Info.IdEmpresa);
            var personas = _consultaTarjetas.ObtenerPersonasTarjeta()
                .DistinctBy(x => x.IdPersona).ToArray();
            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.DetalleMovimiento))
                {
                    criteria.And(m => m.DetalleMovimiento.Contains(filtro.DetalleMovimiento));
                }
                if (filtro.IdPersona > 0)
                {
                    criteria.And(m => m.IdPersona == filtro.IdPersona);
                }
                if (filtro.IdContexto > 0)
                {
                    criteria.And(m => m.IdContexto == filtro.IdContexto);
                }
                if (filtro.FecDesde.HasValue)
                {
                    criteria.And(m => m.FecMovimiento >= filtro.FecDesde.Value);
                }
                if (filtro.FecHasta.HasValue)
                {
                    filtro.FecHasta = filtro.FecHasta.Value.AddDays(Time.DayFactor);
                    criteria.And(m => m.FecMovimiento <= filtro.FecHasta);
                }
            }
            if (InfoSesion.Info.IdTipoUsuario == (int)EnumTipoUsuario.Adicional)
            {
                criteria.And(m => m.IdPersona == InfoSesion.Info.IdPersona);
            }
            var resultado = _repMovimientos.SelectBy(criteria).ToList()
                 .Join(personas,
                    mov => mov.IdPersona,
                    pe => pe.IdPersona,
                    (mov, pe) => new { mov, pe })
                .Join(_repPersona.Table,
                    tmp => tmp.mov.IdPersona,
                     per => per.IdPersona,
                    (tmp, per) => new { tmp.mov, tmp.pe, per })
                .Join(_repMoneda.Table,
                    tmp => tmp.mov.IdMoneda,
                    mon => mon.IdMoneda,
                    (tmp, mon) => new { tmp.mov, tmp.pe, tmp.per, mon })
                .GroupJoin(_vwEmpresasUsuario.Table,
                    tmp => new { tmp.mov.IdPersona, tmp.mov.IdEmpresa },
                    usr => new { usr.IdPersona, usr.IdEmpresa },
                    (tmp, usr) => new
                    {
                        tmp.mov,
                        tmp.pe,
                        tmp.per,
                        tmp.mon,
                        usr = usr.FirstOrDefault()
                    })
                .Select(item => new PocMovimientosEfectivo
                {
                    IdMovimiento = item.mov.IdMovimiento,
                    IdPersona = item.mov.IdPersona,
                    IdEmpresa = item.mov.IdEmpresa,
                    FecMovimiento = item.mov.FecMovimiento,
                    NumFactura = item.mov.NumFactura,
                    DetalleMovimiento = item.mov.DetalleMovimiento,
                    IdMoneda = item.mov.IdMoneda,
                    MonMovimiento = item.mov.MonMovimiento,
                    IdEnumerador = item.mov.IdMovimiento,
                    NumKmSalida = item.mov.NumKmSalida,
                    NumKmLlegada = item.mov.NumKmLlegada,
                    NumKmTotal = (item.mov.NumKmLlegada - item.mov.NumKmSalida),
                    IdEstado = item.mov.IdEstado,
                    DescMoneda = item.mon.Descripcion,
                    CodMoneda = item.mon.CodInternacional,
                    NombreCompleto = item.per.NombreCompleto,
                    CodUsuarioEmpresa = item.usr != null ? item.usr.CodUsuarioEmpresa : null,
                    IdDebCredito = item.mov.IdDebCredito,
                    SoloConsulta = item.pe.SoloConsulta,
                    MesMovimiento = item.mov.FecMovimiento.ToString("MM"),
                    AnoMovimiento = item.mov.FecMovimiento.ToString("yyyy")
                });
            return resultado.ToDataResult(request);
        }

        public DataResult<PocMovimientosEfectivo> ListarMovKilometraje(PocMovimientosEfectivo filtro,
            DataRequest request, DateTime? fecDesde, DateTime? fecHasta)
        {
            filtro = filtro ?? new PocMovimientosEfectivo();
            filtro.IdContexto = (int)EnumContexto.EF_KILOMETRAJE;
            filtro.FecDesde = fecDesde;
            filtro.FecHasta = fecHasta;
            return Listar(filtro, request);
        }

        public DataResult<PocMovimientosEfectivo> ListarMovEfectivo(PocMovimientosEfectivo filtro,
            DataRequest request, DateTime? fecDesde, DateTime? fecHasta)
        {
            filtro = filtro ?? new PocMovimientosEfectivo();
            filtro.IdContexto = (int)EnumContexto.EF_MOVIMIENTO;
            filtro.FecDesde = fecDesde;
            filtro.FecHasta = fecHasta;
            return Listar(filtro, request);
        }

        public void Modificar(PocMovimientosEfectivo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    if (registro.IdContexto == (int)EnumContexto.EF_KILOMETRAJE)
                    {
                        var antiguedad = ((DateTime)registro.FecMovimiento).Year - ((int)registro.Modelo);
                        var kilometros = (registro.NumKmLlegada.Value - registro.NumKmSalida.Value);
                        registro.MonMovimiento = MontoKilometraje(((int)registro.IdTipoVehiculo),
                            antiguedad < 0 ? 0 : antiguedad, kilometros);
                    }

                    var entrada = registro.ProyectarComo<EF_Movimiento>();
                    _repMovimientos.UpdateExclude(entrada,
                        x => x.IdContexto,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repMovimientos.UnidadTbjo.Save();
                    if (registro.Archivos != null)
                    {
                        foreach (PocArchivoMovimiento file in registro.Archivos)
                        {
                            file.IdMovimiento = registro.IdMovimiento;
                            var archivo = file.ProyectarComo<EF_ArchivoMovimiento>();
                            if (file.Accion == Acciones.Update)
                            {
                                _repArchivoMovimiento.UpdateExclude(archivo,
                                    m => m.FecInclusionAud,
                                    m => m.IdUsuarioIncluyeAud);
                            }
                            else if (file.Accion == Acciones.Delete)
                            {
                                _repArchivoMovimiento.Delete(archivo);
                            }
                            else if (file.Accion == Acciones.Insert)
                            {
                                _repArchivoMovimiento.Insert(archivo);
                            }
                            _repArchivoMovimiento.UnidadTbjo.Save();
                        }
                    }
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocMovimientosEfectivo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public PocMovimientosEfectivo Obtener(PocMovimientosEfectivo filtro)
        {
            var criteria = new Criteria<EF_Movimiento>();
            criteria.And(m => m.IdMovimiento == filtro.IdMovimiento);
            var resultado = _repMovimientos.SelectBy(criteria).FirstOrDefault();
            return resultado.ProyectarComo<PocMovimientosEfectivo>();
        }

        private ValidationResponse Validar(PocMovimientosEfectivo registro, bool inserta)
        {
            if (registro.IdMovimiento < 0 || registro.IdPersona < 0 || registro.IdEmpresa < 0)
            {
                throw new CoreException("Datos faltantes", "Err_0035");
            }
            return new ValidationResponse();
        }

        public DataResult<PocArchivoMovimiento> ListarArchivoMovimiento(PocArchivoMovimiento filtro, DataRequest request)
        {
            var criteria = new Criteria<EF_ArchivoMovimiento>();
            if (filtro.IdMovimiento >= 0)
            {
                criteria.And(p => p.IdMovimiento == filtro.IdMovimiento);
            }
            var resultado = _repArchivoMovimiento.SelectBy(criteria)
                .Select(item => new PocArchivoMovimiento
                {
                    IdArchivo = item.IdArchivo,
                    IdMovimiento = item.IdMovimiento,
                    NombreArchivo = item.NombreArchivo,
                    TipoArchivo = item.TipoArchivo,
                    IdEstado = item.IdEstado
                });
            return resultado.ToDataResult(request);
        }

        public void EliminaArchivoMovimiento(PocArchivoMovimiento registro)
        {
            var entrada = registro.ProyectarComo<EF_ArchivoMovimiento>();
            _repArchivoMovimiento.Delete(entrada);
            _repArchivoMovimiento.UnidadTbjo.Save();
        }

        public byte[] AbrirArchivoMovimiento(PocArchivoMovimiento registro)
        {
            var entrada = registro.ProyectarComo<EF_ArchivoMovimiento>();
            var archivo = _repArchivoMovimiento.Table.FirstOrDefault(x =>
                x.IdArchivo == registro.IdArchivo);
            return archivo.DatosArchivo;
        }

        public int? ExisteArchivoMovimiento(int idMovimiento, string nombreArchivo)
        {
            var resultado = _repArchivoMovimiento.Table.FirstOrDefault(x =>
                x.IdMovimiento == idMovimiento &&
                x.NombreArchivo == nombreArchivo);
            return resultado != null ? resultado.IdArchivo as int? : null;
        }

        public DataResult<PocTarifaKilometraje> ObtenerTarifas(DataRequest request, int idEmpresa)
        {
            var resultado = _repTarifasKM.Table
                .Join(_repCatalogo.List("LTIPOVEHICULOKM"),
                    tf => tf.IdTipoVehiculo,
                    cat => cat.IdCatalogo,
                    (tf, cat) => new { tf, cat })
                .Where(x => x.tf.IdEmpresa == idEmpresa)
                .Select(item => new PocTarifaKilometraje
                {
                    IdTarifa = item.tf.IdTarifa,
                    Antiguedad = item.tf.Antiguedad,
                    IdTipoVehiculo = item.tf.IdTipoVehiculo,
                    Monto = item.tf.Monto,
                    IdEstado = item.tf.IdEstado.HasValue ? item.tf.IdEstado.Value : 0,
                    IdEmpresa = item.tf.IdEmpresa,
                    TipoVehiculo = item.cat.Descripcion
                });
            if (!resultado.Any())
            {
                var entidadBase = _repParamSistema.Table.FirstOrDefault(
                      x => x.IdParametro == "ID_PERSONA_ENTIDAD_BASE");
                var empresaEntidadBase = Int32.Parse(entidadBase.ValParametro);
                var tarifas = _repTarifasKM.Table
                 .Where(x => x.IdEmpresa == empresaEntidadBase).ToList();
                foreach (var item in tarifas)
                {
                    item.IdTarifa = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdTarifa");
                    item.IdEmpresa = idEmpresa;
                    _repTarifasKM.Insert(item);
                }
                _repTarifasKM.UnidadTbjo.Save();
                resultado = _repTarifasKM.Table
                .Join(_repCatalogo.List("LTIPOVEHICULOKM"),
                    tf => tf.IdTipoVehiculo,
                    cat => cat.IdCatalogo,
                    (tf, cat) => new { tf, cat })
                .Where(x => x.tf.IdEmpresa == idEmpresa)
                .Select(item => new PocTarifaKilometraje
                {
                    IdTarifa = item.tf.IdTarifa,
                    Antiguedad = item.tf.Antiguedad,
                    IdTipoVehiculo = item.tf.IdTipoVehiculo,
                    Monto = item.tf.Monto,
                    IdEstado = item.tf.IdEstado.HasValue ? item.tf.IdEstado.Value : 0,
                    IdEmpresa = item.tf.IdEmpresa,
                    TipoVehiculo = item.cat.Descripcion
                });
            }
            return resultado.OrderBy(x => x.TipoVehiculo)
                .ThenBy(x => x.Antiguedad).ToDataResult(request);
        }

        public void ModificarTarifa(IEnumerable<PocTarifaKilometraje> tarifas)
        {
            using (var ts = new TransactionScope())
            {
                foreach (PocTarifaKilometraje item in tarifas)
                {
                    var entrada = item.ProyectarComo<EF_TarifaKilometraje>();
                    _repTarifasKM.Update(entrada, x => x.Monto);
                }
                _repTarifasKM.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void EliminarTarifa(IEnumerable<PocTarifaKilometraje> tarifas)
        {
            using (var ts = new TransactionScope())
            {
                foreach (PocTarifaKilometraje item in tarifas)
                {
                    var entrada = item.ProyectarComo<EF_TarifaKilometraje>();
                    _repTarifasKM.Delete(entrada);
                }
                _repTarifasKM.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void CrearTarifa(IEnumerable<PocTarifaKilometraje> tarifas, int idEmpresa)
        {
            using (var ts = new TransactionScope())
            {
                foreach (PocTarifaKilometraje item in tarifas)
                {
                    var nuevaTarifa = item.ProyectarComo<EF_TarifaKilometraje>();
                    var existente = _repTarifasKM.Table.FirstOrDefault(x =>
                        x.IdTipoVehiculo == nuevaTarifa.IdTipoVehiculo &&
                        x.Antiguedad == nuevaTarifa.Antiguedad && x.IdEmpresa == idEmpresa);
                    if (existente == null)
                    {
                        nuevaTarifa.IdTarifa = (int)_fGenerales.ObtieneSgteSecuencia("SEQ_IdTarifa");
                        nuevaTarifa.IdEmpresa = idEmpresa;
                        nuevaTarifa.IdEstado = (int)EnumIdEstado.Activo;
                        _repTarifasKM.Insert(nuevaTarifa);
                    }
                }
                _repTarifasKM.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        /*Monto de kilometraje*/
        private decimal MontoKilometraje(int idTipoVehiculo, int antiguedad, int kilometros)
        {
            decimal monto = 0;
            int maximo = 0;
            int idEmpresa = InfoSesion.Info.IdEmpresa;

            var maxAntiguedad = _repTarifasKM.Table.OrderByDescending(x => x.Antiguedad)
                .FirstOrDefault(x => x.IdEmpresa == idEmpresa
                               && x.IdTipoVehiculo == idTipoVehiculo);
            if (maxAntiguedad != null)
            {
                maximo = maxAntiguedad.Antiguedad;
            }
            else
            {
                var entidadBase = _repParamSistema.Table.FirstOrDefault(
                        x => x.IdParametro == "ID_PERSONA_ENTIDAD_BASE");
                idEmpresa = Int32.Parse(entidadBase.ValParametro);
                maxAntiguedad = _repTarifasKM.Table.OrderByDescending(x => x.Antiguedad)
                .FirstOrDefault(x => x.IdEmpresa == idEmpresa
                               && x.IdTipoVehiculo == idTipoVehiculo);
                if (maxAntiguedad != null)
                {
                    maximo = maxAntiguedad.Antiguedad;
                }
            }
            if (antiguedad > maximo)
            {
                antiguedad = maximo;
            }
            var tarifa = _repTarifasKM.Table.FirstOrDefault(
                   x => x.IdTipoVehiculo == idTipoVehiculo &&
                   x.Antiguedad == antiguedad && x.IdEmpresa == idEmpresa);
            if (tarifa != null)
            {
                monto = (kilometros * tarifa.Monto);
            }
            return monto;
        }

        public int? ExisteArchivoTarjeta(int idMovimiento, string nombreArchivo)
        {
            var resultado = _repArchivoTarjeta.Table.FirstOrDefault(x =>
                x.IdMovimiento == idMovimiento &&
                x.NombreArchivo == nombreArchivo);
            return resultado != null ? resultado.IdArchivo as int? : null;
        }

        public DataResult<PocArchivoTarjeta> ListarArchivoMovimientoTc(PocArchivoTarjeta filtro, DataRequest request)
        {
            var criteria = new Criteria<EF_ArchivoTarjeta>();
            if (filtro.IdMovimiento >= 0)
            {
                criteria.And(p => p.IdMovimiento == filtro.IdMovimiento);
            }
            var resultado = _repArchivoTarjeta.SelectBy(criteria)
                .Select(item => new PocArchivoTarjeta
                {
                    IdArchivo = item.IdArchivo,
                    IdMovimiento = item.IdMovimiento,
                    NombreArchivo = item.NombreArchivo,
                    TipoArchivo = item.TipoArchivo,
                    IdEstado = item.IdEstado
                });
            return resultado.ToDataResult(request);
        }

        public void EliminaArchivoMovimientoTc(PocArchivoTarjeta registro)
        {
            var entrada = registro.ProyectarComo<EF_ArchivoTarjeta>();
            _repArchivoTarjeta.Delete(entrada);
            _repArchivoTarjeta.UnidadTbjo.Save();
        }

        public byte[] AbrirArchivoMovimientoTc(PocArchivoTarjeta registro)
        {
            var entrada = registro.ProyectarComo<EF_ArchivoTarjeta>();
            var archivo = _repArchivoTarjeta.Table.FirstOrDefault(x =>
                x.IdArchivo == registro.IdArchivo);
            return archivo.DatosArchivo;
        }
    }
}

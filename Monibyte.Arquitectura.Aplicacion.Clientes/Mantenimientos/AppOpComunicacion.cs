﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Clientes;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public class AppOpComunicacion : AplicacionBase, IAppOpComunicacion
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CL_Comunicacion> _repComunicacion;

        public AppOpComunicacion(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CL_Comunicacion> repComunicacion)
        {
            _repCatalogo = repCatalogo;
            _repComunicacion = repComunicacion;
        }

        public void Eliminar(PocComunicacion registro)
        {
            using (var ts = new TransactionScope())
            {
                if (registro != null && registro.IdPersona > 0)
                {
                    _repComunicacion.DeleteOn(x => x.IdComunicacion == registro.IdComunicacion);
                    _repComunicacion.UnidadTbjo.Save();
                }
                ts.Complete();
            }
        }

        public void Insertar(PocComunicacion registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CL_Comunicacion>();
                    entrada.IdComunicacion = _fGenerales.ObtieneSgteSecuencia("SEQ_IdComunicacion");
                    entrada.IdEstado = (int)EnumIdEstado.Activo;
                    if(entrada.IdOrigenProcesador == 0)
                        entrada.IdOrigenProcesador = (int)EnumIdSiNo.No;
                    _repComunicacion.Insert(entrada);
                    _repComunicacion.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocComunicacion));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocComunicacion registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CL_Comunicacion>();
                    _repComunicacion.Update(entrada, m => m.ValComunicacion,
                        m => m.IdEstado, m => m.IdTipo, m => m.IdUbicacion);
                    _repComunicacion.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocComunicacion));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public PocComunicacion Obtener(PocComunicacion filtro)
        {
            var validacion = new ValidationResponse();
            if (filtro.IdPersona == 0)
            {
                validacion.AddError("Err_0054");
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Error al obtener datos de contacto", validation: validacion);
            }
            var criteria = new Criteria<CL_Comunicacion>();
            criteria.And(m => m.IdPersona == filtro.IdPersona);
            criteria.And(m => m.IdComunicacion == filtro.IdComunicacion);
            var registro = Query(criteria).FirstOrDefault();
            return registro != null ? registro : null;
        }

        public DataResult<PocComunicacion> Listar(PocComunicacion filtro, DataRequest request)
        {
            var validacion = new ValidationResponse();
            if (filtro.IdPersona == 0)
            {
                validacion.AddError("Err_0054");
            }
            var criteria = new Criteria<CL_Comunicacion>();
            criteria.And(m => m.IdPersona == filtro.IdPersona);
            var resultado = Query(criteria);
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocComunicacion registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (!inserta)
            {
                if (registro.IdComunicacion == 0)
                {
                    validacion.AddError("Err_0035");
                }
            }
            if (registro.IdPersona == 0)
            {
                validacion.AddError("Err_0027");
            }
            if (string.IsNullOrEmpty(registro.ValComunicacion))
            {
                validacion.AddError("Err_0069");
            }
            return validacion;
        }

        public List<PocComunicacion> ObtenerPorPersona(int idPersona)
        {
            var criteria = new Criteria<CL_Comunicacion>();
            criteria.And(m => m.IdPersona == idPersona);
            var resultado = Query(criteria);
            return resultado.ToList();
        }

        public PocComunicacion ObtenerTipoPorPersona(int idPersona, EnumTipoComunicacion idTipo)
        {
            var criteria = new Criteria<CL_Comunicacion>();
            criteria.And(m => m.IdPersona == idPersona);
            criteria.And(m => m.IdTipo == (int)idTipo);
            var resultado = Query(criteria);
            return resultado.FirstOrDefault();
        }

        private IEnumerable<PocComunicacion> Query(Criteria<CL_Comunicacion> criteria)
        {
            criteria = criteria ?? new Criteria<CL_Comunicacion>();
            var resultado = _repComunicacion
                .SelectBy(criteria)
                .Join(_repCatalogo.List("LTIPOCOMUNICACION"),
                    com => com.IdTipo,
                    cat => cat.IdCatalogo,
                    (com, tipoCom) => new { com, tipoCom })
                .Join(_repCatalogo.List("LUBICACION"),
                    tmp => tmp.com.IdUbicacion,
                    cat => cat.IdCatalogo,
                    (tmp, tipoUbi) => new
                    {
                        tmp.com,
                        tmp.tipoCom,
                        tipoUbi
                    })
                .Select(tmp => new PocComunicacion
                {
                    IdTipo = tmp.com.IdTipo,
                    IdEstado = tmp.com.IdEstado,
                    IdPersona = tmp.com.IdPersona,
                    IdUbicacion = tmp.com.IdUbicacion,
                    IdComunicacion = tmp.com.IdComunicacion,
                    ValComunicacion = tmp.com.ValComunicacion,
                    DescripcionTipo = tmp.tipoCom.Descripcion,
                    DescripcionUbicacion = tmp.tipoUbi.Descripcion
                });
            return resultado;
        }

        public void EliminarPorPersona(int idPersona)
        {
            _repComunicacion.DeleteOn(x => x.IdPersona == idPersona);
            _repComunicacion.UnidadTbjo.Save();
        }
    }
}


﻿using Monibyte.Arquitectura.Poco.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Poco.Notificacion
{
    public abstract class NotBaseFiltro
    {
        public int IdEstado { get; set; }
        public int IdContexto { get; set; }
        public string Descripcion { get; set; }
        public PocDetalleFiltro DetalleFiltro { get; set; }
    }

    public class PocDetalleFiltro
    {
        public Guid? IdFiltro { get; set; }
        public int? IdCampo { get; set; }
        public int? IdTipoCampo { get; set; }
        public string NombreCampo { get; set; }
        public string Descripcion { get; set; }
        public dynamic Valor1 { get; set; }
        public dynamic Valor2 { get; set; }
        public int? IdOperadorRel { get; set; }
        public string OperadorRel { get; set; }
        public int? IdOperadorLog { get; set; }
        public string OperadorLog { get; set; }
        public List<PocListaSql> Coll { get; set; }
        public List<PocDetalleFiltro> Comp { get; set; }
        public bool? Visible { get; set; }
    }

    public class PocConfigNotificacion : NotBaseFiltro
    {
        public string Id { get; set; }
        public string Group { get; set; }
        public string SubContexto { get; set; }
        public int? Orden { get; set; }
        public int? IdNotificacion { get; set; }
        public int? IdEmpresa { get; set; }
        public int? IdConfiguracion { get; set; }
        public int? IdTipoNotificacion { get; set; }
        public bool AdministracionInterna { get; set; }
        public bool NotificaAdministradoresSms { get; set; }
        public bool NotificaAdministradoresEmail { get; set; }
        public bool AplicaTarjetaHabiente { get; set; }
        public bool NotificaTarjetaHabienteSms { get; set; }
        public bool NotificaTarjetaHabienteEmail { get; set; }
        public bool AplicaUsuario { get; set; }
        public bool NotificaUsuarioSms { get; set; }
        public bool NotificaUsuarioEmail { get; set; }
        public DateTime? FecVigenciaHasta { get; set; }
        public List<PocDestinatario> Destinatarios { get; set; }
    }

    public class PocDestinatario
    {
        public string Canal { get; set; }
        public string[] Valores { get; set; }
    }

    public class PocDestinatarioDefault
    {
        public int IdPersona { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string Celular { get; set; }
    }

    public class PocoQueue
    {
        public string Language { get; set; }
        public string MessageType { get; set; }
        public int EntityId { get; set; }
        public int CompanyId { get; set; }
        public string UserCode { get; set; }
        public int OriginId { get; set; }
        public int OriginIdVal { get; set; }
        public string SubOrigin { get; set; }
        public string Data { get; set; }
        public string Sender { get; set; }
        public IEnumerable<PocoQueueDestinatario> Recipients { get; set; }
    }

    public class PocoQueueDestinatario
    {
        public string Canal { get; set; }
        public PocoQueueDestinatarioValor[] Valores { get; set; }
    }

    public class PocoQueueDestinatarioValor
    {
        public string Nombre { get; set; }
        public string Valor { get; set; }
    }
}

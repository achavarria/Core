using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ProcesoMap : EntityTypeConfiguration<GL_Proceso>
    {
        public GL_ProcesoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdProceso);

            // Properties
            this.Property(t => t.IdProceso)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.EjecutarMetodo)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GL_Proceso", "General");
            this.Property(t => t.IdProceso).HasColumnName("IdProceso");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.OrdenEjecucion).HasColumnName("OrdenEjecucion");
            this.Property(t => t.HoraEjecucion).HasColumnName("HoraEjecucion");
            this.Property(t => t.IdDependeDe).HasColumnName("IdDependeDe");
            this.Property(t => t.EjecutarMetodo).HasColumnName("EjecutarMetodo");
            this.Property(t => t.IdEstadoEjecucion).HasColumnName("IdEstadoEjecucion");
            this.Property(t => t.IdPeriodicidad).HasColumnName("IdPeriodicidad");
            this.Property(t => t.ValPeriodicidad).HasColumnName("ValPeriodicidad");
            this.Property(t => t.HoraUltEjecucion).HasColumnName("HoraUltEjecucion");
            this.Property(t => t.FecInicio).HasColumnName("FecInicio");
            this.Property(t => t.FecFinaliza).HasColumnName("FecFinaliza");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

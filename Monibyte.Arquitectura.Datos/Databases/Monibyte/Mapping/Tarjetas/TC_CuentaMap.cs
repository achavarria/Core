using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_CuentaMap : EntityTypeConfiguration<TC_Cuenta>
    {
        public TC_CuentaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCuenta);

            // Properties
            this.Property(t => t.IdCuenta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumCuenta)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.DireccionEnvio)
                .HasMaxLength(500);

            this.Property(t => t.CuentaClienteLocal)
                .HasMaxLength(20);

            this.Property(t => t.CuentaClienteInter)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("TC_Cuenta", "TCredito");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.NumCuenta).HasColumnName("NumCuenta");
            this.Property(t => t.IdPersonaTitular).HasColumnName("IdPersonaTitular");
            this.Property(t => t.IdTipoTarjeta).HasColumnName("IdTipoTarjeta");
            this.Property(t => t.LimiteCreditoLocal).HasColumnName("LimiteCreditoLocal");
            this.Property(t => t.LimiteCreditoInter).HasColumnName("LimiteCreditoInter");
            this.Property(t => t.FechaVctoLineaCredito).HasColumnName("FechaVctoLineaCredito");
            this.Property(t => t.FecApertura).HasColumnName("FecApertura");
            this.Property(t => t.FecActivacion).HasColumnName("FecActivacion");
            this.Property(t => t.FecBloqueo).HasColumnName("FecBloqueo");
            this.Property(t => t.IdUsuarioBloqueo).HasColumnName("IdUsuarioBloqueo");
            this.Property(t => t.IdUsuarioActivacion).HasColumnName("IdUsuarioActivacion");
            this.Property(t => t.IdUsuarioApertura).HasColumnName("IdUsuarioApertura");
            this.Property(t => t.SaldoLocal).HasColumnName("SaldoLocal");
            this.Property(t => t.IdEstadoCta).HasColumnName("IdEstadoCta");
            this.Property(t => t.IdEnvioEstCuenta).HasColumnName("IdEnvioEstCuenta");
            this.Property(t => t.IdDirEnvioEstCuenta).HasColumnName("IdDirEnvioEstCuenta");
            this.Property(t => t.DireccionEnvio).HasColumnName("DireccionEnvio");
            this.Property(t => t.IdCantonEnvio).HasColumnName("IdCantonEnvio");
            this.Property(t => t.CuentaClienteLocal).HasColumnName("CuentaClienteLocal");
            this.Property(t => t.PorcAvanceEfectivo).HasColumnName("PorcAvanceEfectivo");
            this.Property(t => t.SaldoInternacional).HasColumnName("SaldoInternacional");
            this.Property(t => t.DispAvanceEfectivoInter).HasColumnName("DispAvanceEfectivoInter");
            this.Property(t => t.DispAvanceEfectivoLocal).HasColumnName("DispAvanceEfectivoLocal");
            this.Property(t => t.SaldoInicialMillas).HasColumnName("SaldoInicialMillas");
            this.Property(t => t.DebitoMillas).HasColumnName("DebitoMillas");
            this.Property(t => t.CreditoMillas).HasColumnName("CreditoMillas");
            this.Property(t => t.DiasAtrasoLocal).HasColumnName("DiasAtrasoLocal");
            this.Property(t => t.IdDiaCorte).HasColumnName("IdDiaCorte");
            this.Property(t => t.DiasAtrasoInter).HasColumnName("DiasAtrasoInter");
            this.Property(t => t.PagoMinLocal).HasColumnName("PagoMinLocal");
            this.Property(t => t.PagoMinInter).HasColumnName("PagoMinInter");
            this.Property(t => t.PagoContadoLocal).HasColumnName("PagoContadoLocal");
            this.Property(t => t.PagoContadoInter).HasColumnName("PagoContadoInter");
            this.Property(t => t.FecUltPagoLocal).HasColumnName("FecUltPagoLocal");
            this.Property(t => t.FecUltPagoInter).HasColumnName("FecUltPagoInter");
            this.Property(t => t.FecPagoContado).HasColumnName("FecPagoContado");
            this.Property(t => t.FecPagoMinimo).HasColumnName("FecPagoMinimo");
            this.Property(t => t.CuentaClienteInter).HasColumnName("CuentaClienteInter");
            this.Property(t => t.TasaInteresLocal).HasColumnName("TasaInteresLocal");
            this.Property(t => t.TasaInteresInter).HasColumnName("TasaInteresInter");
            this.Property(t => t.TasaIntMoraLocal).HasColumnName("TasaIntMoraLocal");
            this.Property(t => t.TasaIntMoraInter).HasColumnName("TasaIntMoraInter");
            this.Property(t => t.InteresMoratorioLocal).HasColumnName("InteresMoratorioLocal");
            this.Property(t => t.InteresMoratorioInter).HasColumnName("InteresMoratorioInter");
            this.Property(t => t.TasaIntCJLocal).HasColumnName("TasaIntCJLocal");
            this.Property(t => t.TasaIntCJInter).HasColumnName("TasaIntCJInter");
            this.Property(t => t.IdCalificacion).HasColumnName("IdCalificacion");
            this.Property(t => t.PlazoMeses).HasColumnName("PlazoMeses");
            this.Property(t => t.MonDisponibleLocal).HasColumnName("MonDisponibleLocal");
            this.Property(t => t.MonDisponibleInter).HasColumnName("MonDisponibleInter");
            this.Property(t => t.MonCargosBonifLocal).HasColumnName("MonCargosBonifLocal");
            this.Property(t => t.MonCargosBonifInter).HasColumnName("MonCargosBonifInter");
            this.Property(t => t.MonCreditosLocal).HasColumnName("MonCreditosLocal");
            this.Property(t => t.MonCreditosInter).HasColumnName("MonCreditosInter");
            this.Property(t => t.SaldoTotalCorteLocal).HasColumnName("SaldoTotalCorteLocal");
            this.Property(t => t.SaldoTotalCorteInter).HasColumnName("SaldoTotalCorteInter");
            this.Property(t => t.CantImpVencidosLocal).HasColumnName("CantImpVencidosLocal");
            this.Property(t => t.CantImpVencidosInter).HasColumnName("CantImpVencidosInter");
            this.Property(t => t.MonImpVencidosLocal).HasColumnName("MonImpVencidosLocal");
            this.Property(t => t.DebitosTransitoLocal).HasColumnName("DebitosTransitoLocal");
            this.Property(t => t.DebitosTransitoInter).HasColumnName("DebitosTransitoInter");
            this.Property(t => t.CreditosTransitoLocal).HasColumnName("CreditosTransitoLocal");
            this.Property(t => t.CreditosTransitoInter).HasColumnName("CreditosTransitoInter");
            this.Property(t => t.LimiteExtraFinLocal).HasColumnName("LimiteExtraFinLocal");
            this.Property(t => t.LimiteExtraFinInter).HasColumnName("LimiteExtraFinInter");
            this.Property(t => t.SaldoExtraFinLocal).HasColumnName("SaldoExtraFinLocal");
            this.Property(t => t.SaldoExtraFinInter).HasColumnName("SaldoExtraFinInter");
            this.Property(t => t.MonDispExtraFinLocal).HasColumnName("MonDispExtraFinLocal");
            this.Property(t => t.MonDispExtraFinInter).HasColumnName("MonDispExtraFinInter");
            this.Property(t => t.MonImpVencidosInter).HasColumnName("MonImpVencidosInter");
            this.Property(t => t.HoraUltActualizacion).HasColumnName("HoraUltActualizacion");
            this.Property(t => t.FecUltCorte).HasColumnName("FecUltCorte");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdRestringeDistLimite).HasColumnName("IdRestringeDistLimite");
            this.Property(t => t.IdCompania).HasColumnName("IdCompania");
            this.Property(t => t.IdUnidad).HasColumnName("IdUnidad");
            this.Property(t => t.IdCuentaMadre).HasColumnName("IdCuentaMadre");
            this.Property(t => t.SaldoAnteriorLocal).HasColumnName("SaldoAnteriorLocal");
            this.Property(t => t.SaldoAnteriorInter).HasColumnName("SaldoAnteriorInter");
            this.Property(t => t.IdEsCuentaMadre).HasColumnName("IdEsCuentaMadre");
            this.Property(t => t.MonLineaInter).HasColumnName("MonLineaInter");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.LimiteGlobal).HasColumnName("LimiteGlobal");
            //this.Property(t => t.LimiteAdelantoLocal).HasColumnName("LimiteAdelantoLocal");
            //this.Property(t => t.LimiteAdelantoInter).HasColumnName("LimiteAdelantoInter");
            //this.Property(t => t.IdTipoCliente).HasColumnName("IdTipoCliente");
        }
    }
}

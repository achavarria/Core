﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;

namespace Monibyte.Arquitectura.Servicios.Web.Documentacion
{
    [ServiceContract]
   public interface IServicioDocumentacion : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerAyuda")]
        Transporte ObtenerAyuda(Transporte transporte);
    }
}

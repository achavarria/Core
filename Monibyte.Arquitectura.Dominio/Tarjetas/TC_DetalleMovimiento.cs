using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_DetalleMovimiento : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public string NumOrdenCompra { get; set; }
        public string NumFactura { get; set; }
        public string NumReferencia { get; set; }
        public string Observaciones { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

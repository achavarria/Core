﻿using Monibyte.Arquitectura.Aplicacion.Generales.Mantenimientos;
using Monibyte.Arquitectura.Aplicacion.Seguridad;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Clientes;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Seguridad;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Servicios.Web.Generales
{
    public class Membresia : ServicioUnity, IMembresia
    {
        #region CONSTRUCTOR UNITY
        private IAppOpUsuario _appUsuario;
        private IDirectorioUsuarioApp _dirUsuario;

        public Membresia(IAppOpUsuario appUsuario,
            IDirectorioUsuarioApp dirUsuario)
        {
            _appUsuario = appUsuario;
            _dirUsuario = dirUsuario;
        }
        #endregion

        public void ActualizarPerfil(PocInfoUsuario infoUsuario)
        {
            _dirUsuario.ActualizarPerfil(infoUsuario);
        }

        public PocInfoUsuario ObtenerInfoPerfil(string codUsuario)
        {
            return _dirUsuario.ObtenerInfoPerfil(codUsuario);
        }

        public void RegistrarPerfil(PocRegistroUsuario infoUsuario)
        {
            _dirUsuario.RegistrarPerfil(infoUsuario);
        }

        public void ActualizarContrasena(PocCambioContrasena datos)
        {
            _dirUsuario.ActualizarContrasena(datos);
        }

        public void FinalizarSesion(string ticket)
        {
            _dirUsuario.FinalizarSesion(ticket);
        }

        public IEnumerable<PocEmpresasUsuario> ObtenerEmpresasAsociadas(string codUsuario)
        {
            return _appUsuario.ObtenerEmpresasAsociadas(codUsuario);
        }

        #region Servicios de mantenimiento de usuarios

        public Transporte IncluirUsuario(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocUsuario>();
            _appUsuario.Insertar(entrada);
            var resultado = entrada.CodUsuario;
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListarUsuarios(Transporte transporte)
        {
            object resultado = null;
            var entrada = transporte.GetDatos<PocUsuario>();
            var dataReq = transporte.GetDataRequest();
            if (dataReq == null)
            {
                resultado = _appUsuario.Listar(entrada);
            }
            else
            {
                resultado = _appUsuario.Listar(entrada, dataReq);
            }
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaUsuario(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocUsuario>();
            _appUsuario.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaUsuario(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocUsuario>();
            _appUsuario.Modificar(entrada);
            return transporte;
        }

        public Transporte ModificaCodUsuarioEmpresa(Transporte transporte)
        {
            var entrada = transporte.GetDatos<IEnumerable<PocUsuario>>();
            _appUsuario.ModificaCodUsuarioEmpresa(entrada);
            return transporte;
        }

        public Transporte AsociarEmpresasUsuario(Transporte transporte)
        {
            var codUsuario = transporte.GetValor<string>("codUsuario");
            var modelo = transporte.GetValor<List<PocEmpresasUsuario>>("modelo");
            var modeloPrevio = transporte.GetValor<List<PocEmpresasUsuario>>("modeloPrevio");
            _appUsuario.AsociarEmpresasUsuario(codUsuario, modelo, modeloPrevio);
            return transporte;
        }

        public Transporte ListarUsuariosConectados(Transporte transporte)
        {
            var resultado = _appUsuario.ListarUsuariosConectados(transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte CambiosSeguridad(Transporte transporte)
        {
            var datosEntrada = transporte.GetDatos<PocSeguridadUsuario>();
            _appUsuario.CambiosSeguridad(datosEntrada);
            return transporte;
        }

        public Transporte CambiarEstadoUsuario(Transporte transporte)
        {
            var codUsuario = transporte.GetValor<string>("codUsuario");
            var idEstado = transporte.GetValor<int>("idEstado");
            _dirUsuario.CambiarEstado(codUsuario, idEstado);
            return transporte;
        }

        public Transporte InsertaNotificaUsuario(Transporte transporte)
        {
            var usuario = transporte.GetDatos<PocUsuario>();
            _appUsuario.InsertaNotificaUsuario(usuario);
            return transporte;
        }

        public Transporte CambiarRolesOperativos(Transporte transporte)
        {
            var datosEntrada = transporte.GetValor<List<PocUsuarioRoleOperativo>>("datosEntrada");
            _appUsuario.CambiarRolesOperativos(datosEntrada);
            return transporte;
        }

        #endregion
    }
}

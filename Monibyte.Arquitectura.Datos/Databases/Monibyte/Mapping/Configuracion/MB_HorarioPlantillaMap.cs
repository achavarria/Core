using Monibyte.Arquitectura.Dominio.Configuracion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Configuracion
{
    public class MB_HorarioPlantillaMap : EntityTypeConfiguration<MB_HorarioPlantilla>
    {
        public MB_HorarioPlantillaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdHorario);

            // Properties
            this.Property(t => t.IdHorario)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MB_HorarioPlantilla", "Mnb");
            this.Property(t => t.IdHorario).HasColumnName("IdHorario");
            this.Property(t => t.IdPlantilla).HasColumnName("IdPlantilla");
            this.Property(t => t.IdDia).HasColumnName("IdDia");
            this.Property(t => t.IdRestriccion).HasColumnName("IdRestriccion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.HoraDesde).HasColumnName("HoraDesde");
            this.Property(t => t.HoraHasta).HasColumnName("HoraHasta");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");

        }
    }
}

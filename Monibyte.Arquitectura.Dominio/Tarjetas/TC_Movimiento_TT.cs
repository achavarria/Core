using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_Movimiento_TT : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public int IdUsuario { get; set; }
        public Nullable<int> IdCuenta { get; set; }
        public Nullable<int> IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public Nullable<System.DateTime> FecTransaccion { get; set; }
        public Nullable<System.DateTime> HoraTransaccion { get; set; }
        public string NumReferencia { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> IdMoneda { get; set; }
        public Nullable<decimal> MonTransaccion { get; set; }
        public Nullable<System.DateTime> FecInclusionAud { get; set; }
        public Nullable<int> IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdDebCredito { get; set; }
        public Nullable<System.DateTime> FecMovimiento { get; set; }
    }
}

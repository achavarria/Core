﻿namespace Monibyte.Arquitectura.Dominio.Tarjetas.MQATH
{
    public struct Constantes{
        public const string CARACTERBLANCO = " ";
        public const string CARACTERCERO = "0";
    }
    public struct TipoEntradaMsj
    {
        public const string CONSULTA = "0100";
        public const string CONSULTA_CTAS_CLIENTE = "0140";
        public const string LEALTAD_TARJETA = "0160";
        public const string MOVIMIENTOS_TARJETA = "0180";
        public const string PAGO = "0200";
        public const string APLICA_MISCELANEOS = "0220";
        public const string APLICA_MOV_LEALTAD = "0240";
        public const string PAGO_ESPECIAL = "0250";
        public const string PAGO_EXTRAFINANCIAMIENTO = "0250";
        public const string REVERSION_PAGO = "0300";
        public const string REVERSION_PAGO_ESPECIAL = "0350";
        public const string ADELANTO_EFECTIVO = "0400";
        public const string REVERSION_ADELANTO_EFECTIVO = "0500";
        public const string ACTUALIZA_TARJETA = "0600";
        public const string REPOSION_PIN_TARJETA = "0620";
        public const string ACTUALIZA_LIMITE_CUENTA = "0640";
        public const string ACTUALIZA_LIMITE_TARJETA = "0680";
        public const string MOVIMIENTOS_TRANSITO = "0800";
        public const string ACTIVA_TARJETA = "0900";
        public const string INCLUSION_SEGURO = "1200";
        public const string CARGO_AUTOMATICO = "1400";

    }
    public struct TipoSalidaMsj
    {
        public const string RESP_CONSULTA = "0110";
        public const string RESP_CONSULTA_CTAS_CLIENTE = "0150";
        public const string FINAL_CONSULTA_CTAS_CLIENTE = "0151";
        public const string RESP_LEALTAD_TARJETA = "0170";
        public const string FINAL_LEALTAD_TARJETA = "0171";
        public const string RESP_MOVIMIENTOS_TARJETA = "0181";
        public const string FINAL_MOVIMIENTOS_TARJETA = "0189";
        public const string RESP_PAGO = "0210";
        public const string RESP_APLICA_MISCELANEOS = "0230";
        public const string RESP_APLICA_MOV_LEALTAD = "0250";
        public const string RESP_PAGO_ESPECIAL = "0260";
        public const string RESP_PAGO_EXTRAFINANCIAMIENTO = "0260";
        public const string RESP_REVERSION_PAGO = "0310";
        public const string RESP_REVERSION_PAGO_ESPECIAL = "0360";
        public const string RESP_ADELANTO_EFECTIVO = "0410";
        public const string RESP_REVERSION_ADELANTO_EFECTIVO = "0510";
        public const string RESP_ACTUALIZA_TARJETA = "0610";
        public const string RESP_REPOSION_PIN_TARJETA = "0630";
        public const string RESP_ACTUALIZA_LIMITE_CUENTA = "0650";
        public const string RESP_ACTUALIZA_LIMITE_TARJETA = "0690";
        public const string RESP_MOVIMIENTOS_TRANSITO = "0810";
        public const string FINAL_MOVIMIENTOS_TRANSITO = "0820";
        public const string RESP_ACTIVA_TARJETA = "0910";
        public const string RESP_INCLUSION_SEGURO = "1210";
        public const string CARGO_AUTOMATICO = "1410";
    }
    public struct TipoPago
    {
        public const string EFECTIVO = "1000";
        public const string CHEQUE = "1100";
    }
}

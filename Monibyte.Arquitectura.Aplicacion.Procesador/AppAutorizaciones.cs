﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Procesador;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Procesador;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Procesador
{
    public class AppAutorizaciones : AplicacionBase, IAppAutorizaciones
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<PT_Autorizaciones> _repAutorizaciones;
        private IRepositorioMoneda _repMoneda;
        private IRepositorioMnb<GL_Pais> _repPais;
        private IRepositorioCuenta _repCuenta;
        private IRepositorioVwTarjetaUsuario _repVwTarjetaUsuario;
        private IRepositorioMnb<MB_CategoriaTipoComercio> _repCategoriaTipoComercio;
        private IRepositorioMnb<MB_Region> _repRegion;

        public AppAutorizaciones(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<PT_Autorizaciones> repAutorizaciones,
            IRepositorioMoneda repMoneda,
            IRepositorioMnb<GL_Pais> repPais,
            IRepositorioCuenta repCuenta,
            IRepositorioVwTarjetaUsuario repVwTarjetaUsuario,
            IRepositorioMnb<MB_CategoriaTipoComercio> repCategoriaTipoComercio,
            IRepositorioMnb<MB_Region> repRegion)
        {
            _repCatalogo = repCatalogo;
            _repAutorizaciones = repAutorizaciones;
            _repMoneda = repMoneda;
            _repPais = repPais;
            _repCuenta = repCuenta;
            _repVwTarjetaUsuario = repVwTarjetaUsuario;
            _repCategoriaTipoComercio = repCategoriaTipoComercio;
            _repRegion = repRegion;
        }

        public DataResult<PocAutorizaciones> ObetenerDenegaciones(PocFiltroAutorizaciones entrada, DataRequest request)
        {
            var criteria = new Criteria<PT_Autorizaciones>();
            criteria.And(x => x.ResponseCode != "00");
            criteria.And(x => x.MessageTypeId == "0200");
            int? idCuenta = null;
            if (entrada != null)
            {
                if (entrada.PorCuenta == (int)EnumIdSiNo.Si && entrada.IdCuenta > 0)
                {
                    var cuenta = _repCuenta.Table.FirstOrDefault(x => x.IdCuenta == entrada.IdCuenta);
                    if (cuenta != null)
                    {
                        idCuenta = cuenta.IdCuenta;
                        criteria.And(x => x.CardNumber.Contains(cuenta.NumCuenta));
                    }
                }
                else if (!string.IsNullOrEmpty(entrada.NumTarjeta))
                {
                    criteria.And(x => x.CardNumber == entrada.NumTarjeta);
                }
                if (entrada.FecDesde.HasValue)
                {
                    var fecDesdeStr = entrada.FecDesde.Value.ToString("yyyyMMdd");
                    criteria.And(x => x.LocalTransactionDate.CompareTo(fecDesdeStr) >= 0);
                }
                if (entrada.FecHasta.HasValue)
                {
                    var fecHastaStr = entrada.FecHasta.Value.ToString("yyyyMMdd");
                    criteria.And(x => x.LocalTransactionDate.CompareTo(fecHastaStr) <= 0);
                }
            }

            var query = _repAutorizaciones
                .SelectBy(criteria)
                .Join(_repMoneda.Table,
                    at => at.TransactionCurrencyCode,
                    mn => mn.NumInternacional,
                    (at, mn) => new { at, mn })
                .GroupJoin(_repPais.Table,
                      item => item.at.AcquiringCountryCode,
                      pais => pais.CodISO,
                      (item, pais) => new { item.at, item.mn, pais = pais.FirstOrDefault() })
                .GroupJoin(_repCatalogo.List("LRESTRICCIONESMONIBYTE"),
                     item => item.at.DenegationId,
                     list => list.IdCatalogo,
                     (item, list) => new { item.at, item.mn, item.pais, list = list.FirstOrDefault() });

            return query.ToList()
                .Select(item => new PocAutorizaciones
                {
                    Monto = item.at.TransactionAmount,
                    FecTransaccion = item.at.LocalTransactionDate.ToDate("yyyyMMdd").Value,
                    HoraTransaccion = (item.at.LocalTransactionDate +
                        item.at.LocalTransactionTime).ToDate("yyyyMMddHHmmss").Value.TimeOfDay,
                    CodMonedaInternacional = _fGenerales.Traducir(item.mn.Descripcion,
                        "TS_Moneda", item.mn.IdMoneda, Context.Lang.Id, 0, Context.DefaultLang.Id),
                    NumTarjeta = item.at.CardNumber,
                    Comercio = item.at.CardAcceptorNameLocation,
                    ResponseCode = item.at.ResponseCode,
                    MessageTypeId = item.at.MessageTypeId,
                    DenegationId = item.at.DenegationId,
                    DenegationDetail = ObtieneDetalleDenegacion(item.at.DenegationDetail),
                    TipoDenegacion = item.list != null ? item.list.Descripcion : string.Empty,
                    Pais = item.pais != null ? item.pais.Descripcion : item.at.AcquiringCountryCode
                }).ToDataResult(request);
        }

        public string ObtieneDetalleDenegacion(string denailDetail)
        {
            Func<string, string> _lam = (denegacion) =>
            {
                try
                {
                    var detail = denegacion.FromJson<MultilanguageDetail>();
                    if (detail.DetailParams != null)
                    {
                        for (int i = 0; i < detail.DetailParams.Length; i++)
                        {
                            if (detail.DetailParams[i] is JObject)
                            {
                                string translation = null;
                                string description = null;
                                var JO = detail.DetailParams[i] as JObject;
                                var context = JO["Context"].ToString();
                                var contextId = (int?)JO["ContextId"];
                                if (contextId.HasValue)
                                {
                                    switch (context.ToUpper())
                                    {
                                        case "MB_CATEGORIATIPOCOMERCIO":
                                            var cat = _repCategoriaTipoComercio.Table.FirstOrDefault
                                                (x => x.IdCategoria == contextId);
                                            description = cat.Descripcion;
                                            break;
                                        case "LDIASSEMANA":
                                            var dia = _repCatalogo.Table.FirstOrDefault(x =>
                                                x.Codigo == contextId.ToString() &&
                                                x.Lista == context);
                                            contextId = dia.IdCatalogo;
                                            description = dia.Descripcion;
                                            break;
                                        case "MB_REGION":
                                            var reg = _repRegion.Table.FirstOrDefault
                                                (x => x.IdRegion == contextId);
                                            description = reg.Descripcion;
                                            break;
                                        case "GL_PAIS":
                                            var pais = _repPais.Table.FirstOrDefault
                                                (x => x.IdPais == contextId);
                                            description = pais.Descripcion;
                                            break;
                                        case "LPERIODICIDAD":
                                            var periodicidad = _repCatalogo.Table.FirstOrDefault(x =>
                                                x.IdCatalogo == contextId &&
                                                x.Lista == context);
                                            description = periodicidad.Descripcion;
                                            break;
                                        case "TS_MONEDA":
                                            var moneda = _repMoneda.Table.FirstOrDefault(x =>
                                              x.IdMoneda == contextId);
                                            description = moneda.Descripcion;
                                            break;
                                    }
                                    translation = _fGenerales.Traducir(description,
                                        context, contextId.Value, Context.Lang.Id,
                                        0, Context.DefaultLang.Id);
                                }
                                detail.DetailParams[i] = translation;
                            }
                        }
                    }
                    var resx = ResxHelper.Load(detail.Resx, detail.ResxKey);
                    var result = StringExt.ClearFormat(resx, detail.DetailParams);
                    return result;
                }
                catch
                {
                    return denegacion;
                }
            };

            return _lam(denailDetail);
        }
    }
}

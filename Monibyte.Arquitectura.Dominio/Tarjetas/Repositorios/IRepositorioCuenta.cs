﻿using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios
{
    public interface IRepositorioCuenta : IRepositorioMnb<TC_Cuenta>
    {
        IEnumerable<TC_Cuenta> CuentasPorCliente(int idCliente);
        EncabezadoEstCuenta ObtenerEncabezadoEstCuenta(int idCuenta, DateTime fecCorte,
            int? idCuentaMadre, string tablaEnc, int idIdioma, int idIdiomaBase);
    }
       
}

using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_SicvecaConsolidadoMap : EntityTypeConfiguration<TC_SicvecaConsolidado>
    {
        public TC_SicvecaConsolidadoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CodigoSucursal)
                .HasMaxLength(5);

            this.Property(t => t.EmpresaPertenece)
                .HasMaxLength(5);

            this.Property(t => t.CodigoEmisor)
                .HasMaxLength(20);

            this.Property(t => t.NumeroCuenta)
                .HasMaxLength(20);

            this.Property(t => t.NombreDeudor)
                .HasMaxLength(60);

            this.Property(t => t.MonedaLimiteCredito)
                .HasMaxLength(5);

            this.Property(t => t.EstadoOperacion)
                .HasMaxLength(20);

            this.Property(t => t.CatRiesgoLocal)
                .HasMaxLength(5);

            this.Property(t => t.CatRiesgoInter)
                .HasMaxLength(5);

            this.Property(t => t.FrecuenciaPago)
                .HasMaxLength(5);

            this.Property(t => t.FormaPago)
                .HasMaxLength(5);

            this.Property(t => t.IdentificacionDeudor)
                .HasMaxLength(30);

            this.Property(t => t.TipoGarantia)
                .HasMaxLength(5);

            this.Property(t => t.MonedaGarantia)
                .HasMaxLength(5);

            this.Property(t => t.RazonSocial)
                .HasMaxLength(60);

            this.Property(t => t.ClasificacionPagos)
                .HasMaxLength(5);

            this.Property(t => t.ClasificacionConsumos)
                .HasMaxLength(5);

            this.Property(t => t.NumeroTarjetaTitular)
                .HasMaxLength(30);

            this.Property(t => t.IndicadorOperacion)
                .HasMaxLength(5);

            this.Property(t => t.CatRiesgoAnteriorLocal)
                .HasMaxLength(5);

            this.Property(t => t.CatRiesgoAnteriorInter)
                .HasMaxLength(5);

            this.Property(t => t.MonedaUltimaReadecuacion)
                .HasMaxLength(5);

            this.Property(t => t.IndicadorArregloLocal)
                .HasMaxLength(5);

            this.Property(t => t.IndicadorArregloInter)
                .HasMaxLength(5);

            this.Property(t => t.MonedaArregloPagoLocal)
                .HasMaxLength(5);

            this.Property(t => t.MonedaArregloPagoInter)
                .HasMaxLength(5);

            this.Property(t => t.IndicadorFiadores)
                .HasMaxLength(5);

            this.Property(t => t.TipoCobro)
                .HasMaxLength(5);

            this.Property(t => t.CicloCuenta)
                .HasMaxLength(5);

            this.Property(t => t.ArregloImportesVencidosLocal)
                .HasMaxLength(20);

            this.Property(t => t.ArregloImportesVencidosInter)
                .HasMaxLength(20);

            this.Property(t => t.OperacionEspecial)
                .HasMaxLength(5);

            this.Property(t => t.NumeroTarjetaExtFin)
                .HasMaxLength(30);

            this.Property(t => t.EstadoExtFin)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("TC_SicvecaConsolidado", "TCredito");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdCierre).HasColumnName("IdCierre");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.CodigoSucursal).HasColumnName("CodigoSucursal");
            this.Property(t => t.EmpresaPertenece).HasColumnName("EmpresaPertenece");
            this.Property(t => t.CodigoEmisor).HasColumnName("CodigoEmisor");
            this.Property(t => t.NumeroCuenta).HasColumnName("NumeroCuenta");
            this.Property(t => t.NombreDeudor).HasColumnName("NombreDeudor");
            this.Property(t => t.FecConstitucion).HasColumnName("FecConstitucion");
            this.Property(t => t.LimiteCreditoAutorizado).HasColumnName("LimiteCreditoAutorizado");
            this.Property(t => t.MonedaLimiteCredito).HasColumnName("MonedaLimiteCredito");
            this.Property(t => t.PlazoFinanciamientoLocal).HasColumnName("PlazoFinanciamientoLocal");
            this.Property(t => t.PlazoFinanciamientoInter).HasColumnName("PlazoFinanciamientoInter");
            this.Property(t => t.TasaInteresLocal).HasColumnName("TasaInteresLocal");
            this.Property(t => t.TasaInteresInter).HasColumnName("TasaInteresInter");
            this.Property(t => t.FecUltimoPagoInteresLocal).HasColumnName("FecUltimoPagoInteresLocal");
            this.Property(t => t.FecUltimoPagoInteresInter).HasColumnName("FecUltimoPagoInteresInter");
            this.Property(t => t.FecProxPagoLocal).HasColumnName("FecProxPagoLocal");
            this.Property(t => t.FecProxPagoInter).HasColumnName("FecProxPagoInter");
            this.Property(t => t.SaldoActualLocal).HasColumnName("SaldoActualLocal");
            this.Property(t => t.SaldoActualInter).HasColumnName("SaldoActualInter");
            this.Property(t => t.MontoInteresLocal).HasColumnName("MontoInteresLocal");
            this.Property(t => t.MontoInteresInter).HasColumnName("MontoInteresInter");
            this.Property(t => t.MaximoDiasAtrasoLocal).HasColumnName("MaximoDiasAtrasoLocal");
            this.Property(t => t.MaximoDiasAtrasoInter).HasColumnName("MaximoDiasAtrasoInter");
            this.Property(t => t.EstadoOperacion).HasColumnName("EstadoOperacion");
            this.Property(t => t.CatRiesgoLocal).HasColumnName("CatRiesgoLocal");
            this.Property(t => t.CatRiesgoInter).HasColumnName("CatRiesgoInter");
            this.Property(t => t.FrecuenciaPago).HasColumnName("FrecuenciaPago");
            this.Property(t => t.FecPrincPagadoHastaLocal).HasColumnName("FecPrincPagadoHastaLocal");
            this.Property(t => t.FecPrincPagadoHastaInter).HasColumnName("FecPrincPagadoHastaInter");
            this.Property(t => t.FecProxPagoPrinc).HasColumnName("FecProxPagoPrinc");
            this.Property(t => t.FormaPago).HasColumnName("FormaPago");
            this.Property(t => t.IdentificacionDeudor).HasColumnName("IdentificacionDeudor");
            this.Property(t => t.TipoGarantia).HasColumnName("TipoGarantia");
            this.Property(t => t.MonedaGarantia).HasColumnName("MonedaGarantia");
            this.Property(t => t.MontoGarantia).HasColumnName("MontoGarantia");
            this.Property(t => t.FecCorte).HasColumnName("FecCorte");
            this.Property(t => t.FecPago).HasColumnName("FecPago");
            this.Property(t => t.InteresMoratorioLocal).HasColumnName("InteresMoratorioLocal");
            this.Property(t => t.InteresMoratorioInter).HasColumnName("InteresMoratorioInter");
            this.Property(t => t.PagoMinimoLocal).HasColumnName("PagoMinimoLocal");
            this.Property(t => t.PagoMinimoPrincLocal).HasColumnName("PagoMinimoPrincLocal");
            this.Property(t => t.PagoMinimoInteresLocal).HasColumnName("PagoMinimoInteresLocal");
            this.Property(t => t.PagoMinimoInter).HasColumnName("PagoMinimoInter");
            this.Property(t => t.PagoMinimoPrincInter).HasColumnName("PagoMinimoPrincInter");
            this.Property(t => t.PagoMinimoInteresInter).HasColumnName("PagoMinimoInteresInter");
            this.Property(t => t.RazonSocial).HasColumnName("RazonSocial");
            this.Property(t => t.FecVencimientoTarjeta).HasColumnName("FecVencimientoTarjeta");
            this.Property(t => t.SaldoPrincLocal).HasColumnName("SaldoPrincLocal");
            this.Property(t => t.SaldoPrincInter).HasColumnName("SaldoPrincInter");
            this.Property(t => t.CargoInteresCorrienteLocal).HasColumnName("CargoInteresCorrienteLocal");
            this.Property(t => t.CargoInteresCorrienteInter).HasColumnName("CargoInteresCorrienteInter");
            this.Property(t => t.CargoServicioBonificableLocal).HasColumnName("CargoServicioBonificableLocal");
            this.Property(t => t.CargoServicioBonificableInter).HasColumnName("CargoServicioBonificableInter");
            this.Property(t => t.CargoAdministrativoLocal).HasColumnName("CargoAdministrativoLocal");
            this.Property(t => t.CargoAdministrativoInter).HasColumnName("CargoAdministrativoInter");
            this.Property(t => t.MontoAtrasoLocal).HasColumnName("MontoAtrasoLocal");
            this.Property(t => t.MontoAtrasoInter).HasColumnName("MontoAtrasoInter");
            this.Property(t => t.PagoContadoLocal).HasColumnName("PagoContadoLocal");
            this.Property(t => t.PagoContadoInter).HasColumnName("PagoContadoInter");
            this.Property(t => t.MontoUltimoPagoLocal).HasColumnName("MontoUltimoPagoLocal");
            this.Property(t => t.MontoUltimoPagoInter).HasColumnName("MontoUltimoPagoInter");
            this.Property(t => t.SaldoFlotanteLocal).HasColumnName("SaldoFlotanteLocal");
            this.Property(t => t.SaldoFlotanteInter).HasColumnName("SaldoFlotanteInter");
            this.Property(t => t.LimiteExtFinLocal).HasColumnName("LimiteExtFinLocal");
            this.Property(t => t.LimiteExtFinInter).HasColumnName("LimiteExtFinInter");
            this.Property(t => t.SaldoExtFinLocal).HasColumnName("SaldoExtFinLocal");
            this.Property(t => t.SaldoExtFinInter).HasColumnName("SaldoExtFinInter");
            this.Property(t => t.CuotaExtFinLocal).HasColumnName("CuotaExtFinLocal");
            this.Property(t => t.CuotaExtFinInter).HasColumnName("CuotaExtFinInter");
            this.Property(t => t.AbonoExtFinLocal).HasColumnName("AbonoExtFinLocal");
            this.Property(t => t.AbonoExtFinInter).HasColumnName("AbonoExtFinInter");
            this.Property(t => t.OtrosCredExtFinLocal).HasColumnName("OtrosCredExtFinLocal");
            this.Property(t => t.OtrosCredExtFinInter).HasColumnName("OtrosCredExtFinInter");
            this.Property(t => t.DebExtFinLocal).HasColumnName("DebExtFinLocal");
            this.Property(t => t.DebExtFinInter).HasColumnName("DebExtFinInter");
            this.Property(t => t.PagosAcumMesLocal).HasColumnName("PagosAcumMesLocal");
            this.Property(t => t.PagosAcumMesInter).HasColumnName("PagosAcumMesInter");
            this.Property(t => t.OtrosCredAcumMesLocal).HasColumnName("OtrosCredAcumMesLocal");
            this.Property(t => t.OtrosCredAcumMesInter).HasColumnName("OtrosCredAcumMesInter");
            this.Property(t => t.ComprasAcumMesLocal).HasColumnName("ComprasAcumMesLocal");
            this.Property(t => t.ComprasAcumMesInter).HasColumnName("ComprasAcumMesInter");
            this.Property(t => t.OtrosDebAcumMesLocal).HasColumnName("OtrosDebAcumMesLocal");
            this.Property(t => t.OtrosDebAcumMesInter).HasColumnName("OtrosDebAcumMesInter");
            this.Property(t => t.CantidadImportesVencidosLocal).HasColumnName("CantidadImportesVencidosLocal");
            this.Property(t => t.CantidadImportesVencidosInter).HasColumnName("CantidadImportesVencidosInter");
            this.Property(t => t.ClasificacionPagos).HasColumnName("ClasificacionPagos");
            this.Property(t => t.ClasificacionConsumos).HasColumnName("ClasificacionConsumos");
            this.Property(t => t.DispCuentaLocal).HasColumnName("DispCuentaLocal");
            this.Property(t => t.DispCuentaInter).HasColumnName("DispCuentaInter");
            this.Property(t => t.DispAdelantoEfectivoLocal).HasColumnName("DispAdelantoEfectivoLocal");
            this.Property(t => t.DispAdelantoEfectivoInter).HasColumnName("DispAdelantoEfectivoInter");
            this.Property(t => t.MontoSobregiro).HasColumnName("MontoSobregiro");
            this.Property(t => t.FecUltimoPagoLocal).HasColumnName("FecUltimoPagoLocal");
            this.Property(t => t.FecUltimoPagoInter).HasColumnName("FecUltimoPagoInter");
            this.Property(t => t.NumeroTarjetaTitular).HasColumnName("NumeroTarjetaTitular");
            this.Property(t => t.IndicadorOperacion).HasColumnName("IndicadorOperacion");
            this.Property(t => t.CatRiesgoAnteriorLocal).HasColumnName("CatRiesgoAnteriorLocal");
            this.Property(t => t.CatRiesgoAnteriorInter).HasColumnName("CatRiesgoAnteriorInter");
            this.Property(t => t.FecUltimaRenovacion).HasColumnName("FecUltimaRenovacion");
            this.Property(t => t.FecUltimaReadecuacion).HasColumnName("FecUltimaReadecuacion");
            this.Property(t => t.MontoUltimaReadecuacion).HasColumnName("MontoUltimaReadecuacion");
            this.Property(t => t.MonedaUltimaReadecuacion).HasColumnName("MonedaUltimaReadecuacion");
            this.Property(t => t.IndicadorArregloLocal).HasColumnName("IndicadorArregloLocal");
            this.Property(t => t.IndicadorArregloInter).HasColumnName("IndicadorArregloInter");
            this.Property(t => t.FecArregloPagoLocal).HasColumnName("FecArregloPagoLocal");
            this.Property(t => t.FecArregloPagoInter).HasColumnName("FecArregloPagoInter");
            this.Property(t => t.MontoArregloPagoLocal).HasColumnName("MontoArregloPagoLocal");
            this.Property(t => t.MontoArregloPagoInter).HasColumnName("MontoArregloPagoInter");
            this.Property(t => t.MonedaArregloPagoLocal).HasColumnName("MonedaArregloPagoLocal");
            this.Property(t => t.MonedaArregloPagoInter).HasColumnName("MonedaArregloPagoInter");
            this.Property(t => t.PlazoArregloPagoLocal).HasColumnName("PlazoArregloPagoLocal");
            this.Property(t => t.PlazoArregloPagoInter).HasColumnName("PlazoArregloPagoInter");
            this.Property(t => t.TasaArregloPagoLocal).HasColumnName("TasaArregloPagoLocal");
            this.Property(t => t.TasaArregloPagoInter).HasColumnName("TasaArregloPagoInter");
            this.Property(t => t.IndicadorFiadores).HasColumnName("IndicadorFiadores");
            this.Property(t => t.TipoCobro).HasColumnName("TipoCobro");
            this.Property(t => t.CicloCuenta).HasColumnName("CicloCuenta");
            this.Property(t => t.ArregloImportesVencidosLocal).HasColumnName("ArregloImportesVencidosLocal");
            this.Property(t => t.ArregloImportesVencidosInter).HasColumnName("ArregloImportesVencidosInter");
            this.Property(t => t.OperacionEspecial).HasColumnName("OperacionEspecial");
            this.Property(t => t.FecOperacionEspecial).HasColumnName("FecOperacionEspecial");
            this.Property(t => t.NumeroTarjetaExtFin).HasColumnName("NumeroTarjetaExtFin");
            this.Property(t => t.EstadoExtFin).HasColumnName("EstadoExtFin");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Quickpass;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_Cuenta : EntidadBase
    {
        public int IdCuenta { get; set; }
        public string NumCuenta { get; set; }
        public Nullable<int> IdPersonaTitular { get; set; }
        public int IdTipoTarjeta { get; set; }
        public decimal LimiteCreditoLocal { get; set; }
        public decimal LimiteCreditoInter { get; set; }
        public System.DateTime FechaVctoLineaCredito { get; set; }
        public System.DateTime FecApertura { get; set; }
        public Nullable<System.DateTime> FecActivacion { get; set; }
        public Nullable<System.DateTime> FecBloqueo { get; set; }
        public Nullable<int> IdUsuarioBloqueo { get; set; }
        public Nullable<int> IdUsuarioActivacion { get; set; }
        public int IdUsuarioApertura { get; set; }
        public decimal SaldoLocal { get; set; }
        public int IdEstadoCta { get; set; }
        public Nullable<int> IdEnvioEstCuenta { get; set; }
        public Nullable<int> IdDirEnvioEstCuenta { get; set; }
        public string DireccionEnvio { get; set; }
        public Nullable<int> IdCantonEnvio { get; set; }
        public string CuentaClienteLocal { get; set; }
        public Nullable<decimal> PorcAvanceEfectivo { get; set; }
        public decimal SaldoInternacional { get; set; }
        public Nullable<decimal> DispAvanceEfectivoInter { get; set; }
        public Nullable<decimal> DispAvanceEfectivoLocal { get; set; }
        public decimal SaldoInicialMillas { get; set; }
        public decimal DebitoMillas { get; set; }
        public decimal CreditoMillas { get; set; }
        public Nullable<int> DiasAtrasoLocal { get; set; }
        public Nullable<int> IdDiaCorte { get; set; }
        public Nullable<int> DiasAtrasoInter { get; set; }
        public Nullable<decimal> PagoMinLocal { get; set; }
        public Nullable<decimal> PagoMinInter { get; set; }
        public Nullable<decimal> PagoContadoLocal { get; set; }
        public Nullable<decimal> PagoContadoInter { get; set; }
        public Nullable<System.DateTime> FecUltPagoLocal { get; set; }
        public Nullable<System.DateTime> FecUltPagoInter { get; set; }
        public Nullable<System.DateTime> FecPagoContado { get; set; }
        public Nullable<System.DateTime> FecPagoMinimo { get; set; }
        public string CuentaClienteInter { get; set; }
        public decimal TasaInteresLocal { get; set; }
        public decimal TasaInteresInter { get; set; }
        public decimal TasaIntMoraLocal { get; set; }
        public decimal TasaIntMoraInter { get; set; }
        public Nullable<decimal> InteresMoratorioLocal { get; set; }
        public Nullable<decimal> InteresMoratorioInter { get; set; }
        public decimal TasaIntCJLocal { get; set; }
        public Nullable<decimal> TasaIntCJInter { get; set; }
        public int IdCalificacion { get; set; }
        public decimal PlazoMeses { get; set; }
        public decimal MonDisponibleLocal { get; set; }
        public decimal MonDisponibleInter { get; set; }
        public Nullable<decimal> MonCargosBonifLocal { get; set; }
        public Nullable<decimal> MonCargosBonifInter { get; set; }
        public Nullable<decimal> MonCreditosLocal { get; set; }
        public Nullable<decimal> MonCreditosInter { get; set; }
        public Nullable<decimal> SaldoTotalCorteLocal { get; set; }
        public Nullable<decimal> SaldoTotalCorteInter { get; set; }
        public Nullable<int> CantImpVencidosLocal { get; set; }
        public Nullable<int> CantImpVencidosInter { get; set; }
        public Nullable<decimal> MonImpVencidosLocal { get; set; }
        public Nullable<decimal> DebitosTransitoLocal { get; set; }
        public Nullable<decimal> DebitosTransitoInter { get; set; }
        public Nullable<decimal> CreditosTransitoLocal { get; set; }
        public Nullable<decimal> CreditosTransitoInter { get; set; }
        public Nullable<decimal> LimiteExtraFinLocal { get; set; }
        public Nullable<decimal> LimiteExtraFinInter { get; set; }
        public Nullable<decimal> SaldoExtraFinLocal { get; set; }
        public Nullable<decimal> SaldoExtraFinInter { get; set; }
        public Nullable<decimal> MonDispExtraFinLocal { get; set; }
        public Nullable<decimal> MonDispExtraFinInter { get; set; }
        public Nullable<decimal> MonImpVencidosInter { get; set; }
        public Nullable<System.DateTime> HoraUltActualizacion { get; set; }
        public Nullable<System.DateTime> FecUltCorte { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdRestringeDistLimite { get; set; }
        public int IdCompania { get; set; }
        public int IdUnidad { get; set; }
        public Nullable<int> IdCuentaMadre { get; set; }
        public decimal SaldoAnteriorLocal { get; set; }
        public decimal SaldoAnteriorInter { get; set; }
        public int IdEsCuentaMadre { get; set; }
        public Nullable<decimal> MonLineaInter { get; set; }
        public int IdEmpresa { get; set; }
        public Nullable<decimal> LimiteGlobal { get; set; }
        //public Nullable<decimal> LimiteAdelantoLocal { get; set; }
        //public Nullable<decimal> LimiteAdelantoInter { get; set; }
        //public Nullable<int> IdTipoCliente { get; set; }
    }
}

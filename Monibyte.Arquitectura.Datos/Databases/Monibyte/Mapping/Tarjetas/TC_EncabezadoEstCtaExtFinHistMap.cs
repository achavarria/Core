using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_EncabezadoEstCtaExtFinHistMap : EntityTypeConfiguration<TC_EncabezadoEstCtaExtFinHist>
    {
        public TC_EncabezadoEstCtaExtFinHistMap()
        {
            // Primary Key
            this.HasKey(t => t.IdEncabezado);

            // Properties
            this.Property(t => t.IdEncabezado)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumeroExtra)
                .HasMaxLength(20);

            this.Property(t => t.Comercio)
                .HasMaxLength(60);

            this.Property(t => t.Concepto)
                .HasMaxLength(60);

            // Table & Column Mappings
            this.ToTable("TC_EncabezadoEstCtaExtFinHist", "TCredito");
            this.Property(t => t.IdEncabezado).HasColumnName("IdEncabezado");
            this.Property(t => t.IdEstCuenta).HasColumnName("IdEstCuenta");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.NumeroExtra).HasColumnName("NumeroExtra");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.MonApertura).HasColumnName("MonApertura");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.Comercio).HasColumnName("Comercio");
            this.Property(t => t.FecApertura).HasColumnName("FecApertura");
            this.Property(t => t.Plazo).HasColumnName("Plazo");
            this.Property(t => t.TasaInteres).HasColumnName("TasaInteres");
            this.Property(t => t.FechaVencimiento).HasColumnName("FechaVencimiento");
            this.Property(t => t.FechaCorte).HasColumnName("FechaCorte");
            this.Property(t => t.MonCuota).HasColumnName("MonCuota");
            this.Property(t => t.MonIntereses).HasColumnName("MonIntereses");
            this.Property(t => t.MonPrincipal).HasColumnName("MonPrincipal");
            this.Property(t => t.Concepto).HasColumnName("Concepto");
            this.Property(t => t.IdTipo).HasColumnName("IdTipo");
            this.Property(t => t.CuotasPagadas).HasColumnName("CuotasPagadas");
            this.Property(t => t.Filler1).HasColumnName("Filler1");
            this.Property(t => t.Filler2).HasColumnName("Filler2");
            this.Property(t => t.MonSaldo).HasColumnName("MonSaldo");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

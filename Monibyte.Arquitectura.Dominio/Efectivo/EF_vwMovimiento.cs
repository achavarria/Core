﻿using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Efectivo.Repositorios
{
    public partial class EF_vwMovimiento : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public int IdPersona { get; set; }
        public int IdEmpresa { get; set; }
        public System.DateTime FecMovimiento { get; set; }
        public string NumFactura { get; set; }
        public string DetalleMovimiento { get; set; }
        public Nullable<int> IdMoneda { get; set; }
        public Nullable<decimal> MonMovimiento { get; set; }
        public string CentroCosto { get; set; }
        public string CuentaContable { get; set; }
        public Nullable<int> NumKmSalida { get; set; }
        public Nullable<int> NumKmLlegada { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdContexto { get; set; }
        public int IdDebCredito { get; set; }
        public string CamposDinamicos { get; set; }
        public Nullable<int> IdTipoVehiculo { get; set; }
        public Nullable<int> Modelo { get; set; }
        public string Placa { get; set; }
        public int IdLiquidado { get; set; }
        public Nullable<int> ArchivoAdjunto { get; set; }
        public string NombreCompleto { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public Nullable<int> IdGestion { get; set; }
    }
}

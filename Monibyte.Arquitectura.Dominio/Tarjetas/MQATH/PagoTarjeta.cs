﻿using System;
using Monibyte.Arquitectura.Comun.Nucleo.Trama;
namespace Monibyte.Arquitectura.Dominio.Tarjetas.MQATH
{  
    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaPagoTarjeta {
        private string _tipoMensaje = TipoEntradaMsj.PAGO;

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string TipoMensaje { get { return _tipoMensaje; } set { _tipoMensaje = value; } }
        [TramaColumnSpec(required: false, maxLength: 8, characterFill: Constantes.CARACTERBLANCO)]
        public string Emisor { get; set; }
        [TramaColumnSpec(required: false, maxLength: 5, characterFill: Constantes.CARACTERBLANCO)]
        private string NumCuenta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string Localidad { get; set; }
        [TramaColumnSpec(required: true, maxLength: 3, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public int MonedaISO { get; set; }
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string TipoPago { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal Monto { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string CodBancoEmisor { get; set; }
        [TramaColumnSpec(required: false, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string NumCheque { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumFinanEspecial { get; set; }
        [TramaColumnSpec(required: true, maxLength: 15, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string IdTransaccion { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string PlazaCheques { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumReferencia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string CodExtraFinanciamiento { get; set; }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaPagoTarjeta :RespuestaBase
    {
        [TramaColumnSpec(required: true, maxLength: 4)]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: false, maxLength: 8)]
        public string Emisor { get; set; }
        [TramaColumnSpec(required: false, maxLength: 5)]
        public string NumCuenta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16)]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public string Localidad { get; set; }
        [TramaColumnSpec(required: true, maxLength: 3)]
        public int MonedaISO { get; set; }
        [TramaColumnSpec(required: true, maxLength: 4)]
        public string TipoPago { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100)]
        public decimal Monto { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string CodBancoEmisor { get; set; }
        [TramaColumnSpec(required: false, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string NumCheque { get; set; }
        [TramaColumnSpec(required: false, maxLength: 13, characterFill: Constantes.CARACTERBLANCO)]
        public string NumFinanEspecial { get; set; }
        [TramaColumnSpec(required: true, maxLength: 15, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string IdTransaccion { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumReferencia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 839)]
        public string Blancos { get; set; }
    }

    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaPagoExtraFinanciamiento
    {
        private string _blancos = string.Empty.PadRight(75, ' ');
        private string _tipoMensaje = TipoEntradaMsj.PAGO_EXTRAFINANCIAMIENTO;

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return _tipoMensaje; } set { _tipoMensaje = value; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal Cantidad { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string CodPlan { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string CodMovimiento { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 75)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaPagoExtraFinanciamiento : RespuestaBase
    {
        private string _blancos = string.Empty.PadRight(901, ' ');
        [TramaColumnSpec(required: true, maxLength: 4)]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 12, divideBy: 100, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public decimal Cantidad { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string CodPlan { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string CodMovimiento { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: false, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 901)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

}


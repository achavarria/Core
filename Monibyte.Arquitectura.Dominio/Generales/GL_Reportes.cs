using Monibyte.Arquitectura.Dominio.Clientes;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Reportes : EntidadBase
    {
        public int IdReporte { get; set; }
        public int IdEmpresa { get; set; }
        public int IdContexto { get; set; }
        public int IdIncluyeEncabTxt { get; set; }
        public string Descripcion { get; set; }
        public int IdIncluyeContrapartida { get; set; }
        public string TipoMovimiento { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int IdTipoReporte { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Poco.Notificacion;
using Monibyte.Arquitectura.Poco.Tarjetas;

namespace Monibyte.Arquitectura.Aplicacion.Notificacion
{
    public partial class AppNotificacion
    {
        //Notificaciones provenientes del Servicio de Cierres (Notificación porcentaje saldo de cuenta)
        public void Method0105(PocoQueue pocoQueue)
        {
            var cuenta = pocoQueue.Data.FromJson<PocoInfoCuentaNot>();
            pocoQueue.CompanyId = cuenta.IdEmpresa;
            ProcesarNotificaciones(pocoQueue, true, true, pocoQueue.CompanyId);
        }
    }
}
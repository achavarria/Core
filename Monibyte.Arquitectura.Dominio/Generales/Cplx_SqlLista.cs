namespace Monibyte.Arquitectura.Dominio.Generales
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    //[ComplexType]
    public partial class Cplx_SqlLista
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_MovimientoTransitoHist : EntidadBase
    {
        public int IdMovimiento { get; set; }
        public int IdCorte { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public System.DateTime FecAutorizacion { get; set; }
        public string NumReferencia { get; set; }
        public string Establecimiento { get; set; }
        public decimal MonAutorizado { get; set; }
        public int IdMonedaAutorizacion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.TEF.Mantenimientos;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.TEF;
using Monibyte.Arquitectura.Poco.TEF;
using System;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Tef.Mantenimientos
{
    public class AppOpCedulaRegistrada : AplicacionBase, IAppOpCedulaRegistrada
    {
        private IRepositorioMnb<TF_CedulasRegistrada> _repCedRegistrada;
        private IRepositorioCuenta _repCuenta;
        private IFuncionesGenerales _funcionesGenerales;

        public AppOpCedulaRegistrada(
            IRepositorioMnb<TF_CedulasRegistrada> repCedRegistrada,
            IRepositorioCuenta repCuenta,
            IFuncionesGenerales funcionesGenerales)
        {
            _repCedRegistrada = repCedRegistrada;
            _repCuenta = repCuenta;
            _funcionesGenerales = funcionesGenerales;
        }

        public void Insertar(PocCedulaRegistrada registro)
        {
            var entrada = registro.ProyectarComo<TF_CedulasRegistrada>();
            entrada.IdCedulaRegistrada = _funcionesGenerales.ObtieneSgteSecuencia("SEQ_IdCedulaRegistrada");
            entrada.IdCuenta = registro.IdCuenta;
            entrada.IdUsuario = InfoSesion.Info.IdUsuario;
            entrada.NumIdentificacion = registro.NumIdentificacion;
            entrada.NombreCompleto = registro.NombreCompleto;
            entrada.NumIdentCuenta = InfoSesion.Info.NumIdentificacion;
            entrada.IdEstado = (int)EnumIdEstado.Activo;

            _repCedRegistrada.Insert(entrada);
            _repCedRegistrada.UnidadTbjo.Save();
        }
        public void Eliminar(PocCedulaRegistrada registro)
        {
            _repCedRegistrada.DeleteOn(p => p.IdCedulaRegistrada == registro.IdCedulaRegistrada);
            _repCedRegistrada.UnidadTbjo.Save();
        }
        public void Modificar(PocCedulaRegistrada registro)
        {
            var entrada = registro.ProyectarComo<TF_CedulasRegistrada>();
            _repCedRegistrada.UpdateExclude(entrada,
                ex => ex.IdUsuarioIncluyeAud,
                ex => ex.FecInclusionAud);
            _repCedRegistrada.UnidadTbjo.Save();
        }

        public DataResult<PocCedulaRegistrada> Listar(PocCedulaRegistrada filtro, DataRequest request)
        {
            var criteria = new Criteria<TF_CedulasRegistrada>();
            var _idUsuario = InfoSesion.Info.IdUsuario;
            criteria.And(m => m.IdUsuario == _idUsuario);
            criteria.And(m => m.IdEstado == (int)EnumIdEstado.Activo);

            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.NombreCompleto))
                {
                    criteria.And(x => x.NombreCompleto.Contains(filtro.NombreCompleto));
                }
                if (!string.IsNullOrEmpty(filtro.NumIdentificacion))
                {
                    criteria.And(x => x.NumIdentificacion.Contains(filtro.NumIdentificacion));
                }
            }
            var resultado = _repCedRegistrada.SelectBy(criteria)
                .Join(_repCuenta.Table, ca => ca.IdCuenta, cu => cu.IdCuenta,
                    (ca, cu) => new { ca, cu })
                .Select(tmp => new PocCedulaRegistrada
                {
                    IdCedulaRegistrada = tmp.ca.IdCedulaRegistrada,
                    IdCuenta = (int)tmp.cu.IdCuenta,
                    NumCuenta = tmp.cu.NumCuenta,
                    IdUsuario = tmp.ca.IdUsuario,
                    NumIdentCuenta = tmp.ca.NumIdentCuenta,
                    NombreCompleto = tmp.ca.NombreCompleto,
                    NumIdentificacion = tmp.ca.NumIdentificacion,
                    IdEstado = tmp.ca.IdEstado

                });

            if (resultado.Any())
            {
                return resultado.ToDataResult(request);
            }
            return null;

        }
        protected ValidationResponse Validar(PocCedulaRegistrada registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (inserta)
            {
                if (registro.IdCuenta <= 0) { validacion.AddError("Err_0001"); }
                if (String.IsNullOrEmpty(registro.NombreCompleto)) { validacion.AddError("Err_0057"); }
                if (String.IsNullOrEmpty(registro.NumIdentificacion)) { validacion.AddError("Err_0039"); }
            }
            else
            {
                if (registro.IdCedulaRegistrada <= 0) { validacion.AddError("Err_0110"); }
            }

            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            else
            {
                return validacion;
            }
        }
    }
}

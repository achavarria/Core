﻿using Monibyte.Arquitectura.Dominio;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public interface IAppReportes : IAplicacionBase
    {
        void GenerarReporteSeguridad(string codUsuario, string nombreCompleto,
            int filtro, string nombreEmpresa, int? tipoPersona);        
    }
}
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Cargos
{
    public partial class CA_RangosTabla : EntidadBase
    {
        public int IdTablaRangos { get; set; }
        public string Descripcion { get; set; }
        public int IdMoneda { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public Nullable<int> IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

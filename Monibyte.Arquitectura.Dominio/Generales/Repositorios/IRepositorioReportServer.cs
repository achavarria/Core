﻿namespace Monibyte.Arquitectura.Dominio.Generales.Repositorios
{
    public interface IRepositorioReportServer
    {
        Reporte ExportPdf(string report, params ReportParam[] parameters);
        Reporte ExportExcel(string report, params ReportParam[] parameters);
        Reporte ExportCsv(string report, params ReportParam[] parameters);
        Reporte ExportXml(string report, params ReportParam[] parameters);
        Reporte ExportDefinition(byte[] report, string format,
            params ReportParam[] parameters);
        bool RemoteFileExists(string url);
        void UploadFile(string path, string fileName, byte[] bytes,
            string extention, string mimeType);
    }
}

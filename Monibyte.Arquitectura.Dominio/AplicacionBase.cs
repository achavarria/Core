﻿using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using System;

namespace Monibyte.Arquitectura.Dominio
{
    public interface IAplicacionBase : IDisposable
    {
    }

    public abstract class AplicacionBase : Disposable, IAplicacionBase
    {
        protected IFuncionesGenerales _fGenerales;

        public AplicacionBase()
        {
            _fGenerales = GestorUnity.CargarUnidad<IFuncionesGenerales>();
        }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Dominio.Quickpass.Repositorios;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Quickpass;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public class AppServiciosQuickpass : AplicacionBase, IAppServiciosQuickpass
    {
        private IServiciosQuickpass _repServiciosQuickpass;
        private IRepositorioMnb<QP_Quickpass> _repQuickpass;
        private IRepositorioMnb<QP_TipoQuickpass> _repTipoQuickpass;
        private IRepositorioMnb<QP_Movimientos> _repMovimientos;
        private IFuncionesQuickpass _funcionesQuickpass;
        private IRepositorioMnb<GL_CierresSistema> _repCierresSistema;
        private IRepositorioTarjeta _repTarjeta;
        private IRepositorioCatalogo _repCatalogo;
        private IOperacionesATH _appOperacionesATH;
        private IRepositorioMoneda _repMoneda;
        private IRepositorioMnb<QP_Bitacora> _repBitacora;
        private IParametroSistema _repParametroSistema;

        public AppServiciosQuickpass(IServiciosQuickpass repServiciosQuickpass,
            IRepositorioMnb<QP_Quickpass> repQuickpass,
            IRepositorioMnb<QP_Movimientos> repMovimientos,
            IFuncionesQuickpass funcionesQuickpass,
            IRepositorioMnb<GL_CierresSistema> repCierresSistema,
            IRepositorioTarjeta repTarjeta,
            IRepositorioCatalogo repCatalogo,
            IOperacionesATH appOperacionesATH,
            IRepositorioMoneda repMoneda,
            IRepositorioMnb<QP_TipoQuickpass> repTipoQuickpass,
            IRepositorioMnb<QP_Bitacora> repBitacora,
            IParametroSistema repParametroSistema)
        {
            _repServiciosQuickpass = repServiciosQuickpass;
            _repQuickpass = repQuickpass;
            _repMovimientos = repMovimientos;
            _funcionesQuickpass = funcionesQuickpass;
            _repCierresSistema = repCierresSistema;
            _repTarjeta = repTarjeta;
            _repCatalogo = repCatalogo;
            _appOperacionesATH = appOperacionesATH;
            _repMoneda = repMoneda;
            _repTipoQuickpass = repTipoQuickpass;
            _repBitacora = repBitacora;
            _repParametroSistema = repParametroSistema;
        }

        public int ObtenerIdTarjeta(string NumTarjeta)
        {
            var criteria = new Criteria<TC_Tarjeta>();
            criteria.And(m => m.NumTarjeta == NumTarjeta);
            var resultado = _repTarjeta.SelectBy(criteria).FirstOrDefault();

            return resultado.IdTarjeta;
        }

        public PocTarjeta ObtenerInfoTarjeta(int IdTarjeta)
        {
            var criteria = new Criteria<TC_Tarjeta>();
            criteria.And(m => m.IdTarjeta == IdTarjeta);
            var resultado = _repTarjeta.SelectBy(criteria).FirstOrDefault();
            return resultado.ProyectarComo<PocTarjeta>();
        }

        public void ImportarListaQuickpass(PocObtenerQuickpass pocObtenerQuickpass)
        {
            var documento = pocObtenerQuickpass.NumDocumento;
            //var entidad = pocObtenerQuickpass.CodEntidad;
            var pocParametroSistema = new PocParametroSistema() { IdParametro = "COD_ENTIDAD_QP" };
            var entidad = _repParametroSistema.ObtenerParametroSistema(pocParametroSistema).ValParametro;
            List<QP_Quickpass> listaQP = _repServiciosQuickpass.ImportarListaQuickpass(documento, entidad);

            pocParametroSistema = new PocParametroSistema() { IdParametro = "ANOS_VENCIMIENTO" };
            var anosVencimiento = _repParametroSistema.ObtenerParametroSistema(pocParametroSistema);

            foreach (QP_Quickpass numQP in listaQP)
            {
                var criteria = new Criteria<QP_Quickpass>();
                criteria.And(m => m.NumQuickpass == numQP.NumQuickpass);
                var resultado = _repQuickpass.SelectBy(criteria);

                if (!resultado.Any())
                {
                    numQP.IdQuickpass = _fGenerales.ObtieneSgteSecuencia("SEQ_IdQuickpass");
                    numQP.NumDocumento = documento;
                    numQP.IdEstado = (int)EnumEstadoQuickpass.Registrado;

                    var bitacora = new QP_Bitacora
                    {
                        IdQuickpass = numQP.IdQuickpass,
                        IdEstado = numQP.IdEstado,
                        IdEstadoOperativo = numQP.IdEstadoOperativo
                    };

                    _repQuickpass.Insert(numQP);
                    _repBitacora.Insert(bitacora);

                    try
                    {
                        _repQuickpass.UnidadTbjo.Save();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }

        public void EjecutarProcesosQuickpass(PocConsultaMovQp PocConsultaMovQp)
        {
            Logger.Log(string.Format("Iniciando procesos de Quickpass"));
            var respuesta = ImportarMovimientosQuickpass(PocConsultaMovQp);
            if (respuesta)
            {
                GenerarCargosMovimientosQuickpass();
                var numDia = PocConsultaMovQp.NumDia.HasValue ?
                    PocConsultaMovQp.NumDia.Value : 0;
                var diaPlazo = PocConsultaMovQp.diaPlazo.HasValue ?
                    PocConsultaMovQp.diaPlazo.Value : 0;
                GenerarCargosCorteQuickpass(numDia, diaPlazo);
                Logger.Log(string.Format("Los procesos de Quickpass han finalizado exitosamente"));
            }
        }

        public bool ImportarMovimientosQuickpass(PocConsultaMovQp PocConsultaMovQp)
        {
            //var entidad = PocConsultaMovQp.CodEntidad;
            var pocParametroSistema = new PocParametroSistema() { IdParametro = "COD_ENTIDAD_QP" };
            var entidad = _repParametroSistema.ObtenerParametroSistema(pocParametroSistema).ValParametro;
            var montoMaximo = PocConsultaMovQp.MontoMaximo;
            //Revisar si el IdCierre ya viene de forma correcta
            var idCierre = PocConsultaMovQp.IdCierre > 0 ? PocConsultaMovQp.IdCierre : 4; //4
            var cierresSis = _repCierresSistema.SelectById(idCierre);
            var fecInicio = cierresSis.FecUltCierre != null ? cierresSis.FecUltCierre.Value.AddDays(1) : DateTime.Now.Date;

            var fecFinal = PocConsultaMovQp.FecFinal;

            Logger.Log(string.Format("Quickpass, Iniciando conexión web service ETC, carga de movimientos"));
            string movimientos = _repServiciosQuickpass.ImportarMovimientosQuickpass(entidad, fecInicio, fecFinal, montoMaximo);
            Logger.Log(string.Format("Quickpass, Fin de conexión web service ETC, carga de movimientos"));

            int idTipoMov, idQuickpass, idMoneda, idEstado, idLote, idTipoRegistro, codMovimiento;
            string detalle, numQuickpass, localizador;
            DateTime? fecProcesamiento;
            DateTime fecConsumo;
            decimal monto;

            if (movimientos != "")
            {
                XDocument listaMovimientos = XDocument.Parse(movimientos);
                idLote = _fGenerales.ObtieneSgteSecuencia("SEQ_IdLote");
                idEstado = (int)EnumEstadoMovQuickpass.Registrado;
                fecProcesamiento = DateTime.Now;
                codMovimiento = (int)EnumTipoMovimientoTC.PeajeQuickpass;
                idTipoMov = (int)EnumTipoMovQuickpass.PeajeQuickpass;
                /*El IdMoneda deberia salir de la entidad*/
                idMoneda = PocConsultaMovQp.IdMonedaLocal;

                Logger.Log(string.Format("Quickpass, Iniciando carga de movimientos a base de datos"));
                foreach (XElement xe in listaMovimientos.Descendants("Detalle"))
                {
                    numQuickpass = xe.Element("Medio_Pago").Value.Trim();
                    var quickpass = _repQuickpass.Table.FirstOrDefault(m => m.NumQuickpass == numQuickpass);
                    var idQuickpas = quickpass.IdQuickpass;
                    var resultado = quickpass != null ? (int?)quickpass.IdQuickpass : null;

                    if (resultado != null)
                    {
                        localizador = xe.Element("Localizador").Value;
                        idTipoRegistro = int.Parse(xe.Element("Tipo_Registro").Value);
                        var importe = decimal.Parse(xe.Element("Importe").Value) / 100;
                        var fecTransito = xe.Element("Fecha_Transito").Value.ToDate("ddMMyyyyHHmm").Value;
                        var criteria = new Criteria<QP_Movimientos>();
                        criteria.And(m => m.CodLocalizador == localizador);
                        criteria.And(m => m.IdTipoRegistro == idTipoRegistro);
                        criteria.And(m => m.IdQuickpass == idQuickpas);
                        criteria.And(m => m.Monto == importe);
                        criteria.And(m => m.FecConsumo == fecTransito);
                        var resMov = _repMovimientos.SelectBy(criteria);

                        if (!resMov.Any())
                        {

                            idQuickpass = (int)resultado;
                            fecConsumo = xe.Element("Fecha_Transito").Value.ToDate("ddMMyyyyHHmm").Value;
                            if (idTipoRegistro == 10)
                                monto = decimal.Parse(xe.Element("Importe").Value) / 100;
                            else monto = decimal.Parse(xe.Element("Importe").Value) / 100 * -1;
                            detalle = xe.Element("Recorrido").Value;//"Peaje Quickpass";

                            var entrada = new QP_Movimientos
                            {
                                IdTipoMovimiento = idTipoMov,
                                CodMovimiento = codMovimiento,
                                IdQuickpass = idQuickpass,
                                FecConsumo = fecConsumo,
                                Monto = monto,
                                IdMoneda = idMoneda,
                                IdEstado = idEstado,
                                FecProcesamiento = fecProcesamiento,
                                IdLote = idLote,
                                Detalle = detalle,
                                IdTipoRegistro = idTipoRegistro,
                                CodLocalizador = localizador
                            };
                            _repMovimientos.Insert(entrada);

                            try
                            {
                                _repMovimientos.UnidadTbjo.Save();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                }
                Logger.Log(string.Format("Quickpass, Fin carga de movimientos a base de datos"));
                _repCierresSistema.UpdateOn(x => x.IdCierre == idCierre, z => new GL_CierresSistema
                {
                    FecUltCierre = DateTime.Now.Date
                });
                _repCierresSistema.UnidadTbjo.Save();
                return true;
            }
            else
            {
                var fecha = cierresSis.HoraUltEjecucion.HasValue ? cierresSis.HoraUltEjecucion.Value :
                    DateTime.Now;
                var horaOriginal = (DateTime.Now.Date + cierresSis.HoraEjecucion.Value).TrimMilliseconds().TrimSeconds().TrimMinutes().Hour;

                if (fecha.TrimMilliseconds().TrimSeconds().TrimMinutes().Hour == DateTime.Now.TrimMilliseconds().TrimSeconds().TrimMinutes().Hour &&
                    (DateTime.Now.TrimMilliseconds().TrimSeconds().TrimMinutes().Hour - horaOriginal) <= 2
                    )
                {
                    _repCierresSistema.UpdateOn(x => x.IdCierre == idCierre, z => new GL_CierresSistema
                    {
                        HoraUltEjecucion = cierresSis.HoraUltEjecucion.Value.AddHours(2)
                    });
                    Logger.Log(string.Format("Los procesos de Quickpass han sido pospuestos 2 Horas"));
                }
                else
                {
                    _repCierresSistema.UpdateOn(x => x.IdCierre == idCierre, z => new GL_CierresSistema
                    {
                        HoraUltEjecucion = null
                    });
                    Logger.Log(string.Format("Los procesos de Quickpass han sido pospuestos 1 día"));
                }
                _repCierresSistema.UnidadTbjo.Save();
                return false;
            }
        }

        public void GenerarCargosMovimientosQuickpass()
        {
            var movimientosCargar = _funcionesQuickpass.GenerarCargosMovimientosQuickpass();
            var listaCargar = movimientosCargar.ProyectarComo<PocQuickpassMovimientos>();
            AplicarCargosMovimientosQP(listaCargar);
        }

        public void GenerarCargosCorteQuickpass(int? numDia, int? diaPlazo)
        {
            var dispQP = _funcionesQuickpass
                .GenerarcargosCorteQuickpass().ToList();

            var fecActual = DateTime.Today;
            Logger.Log(string.Format("Quickpass, Iniciando generación de cargos de corte por Quickpass"));
            foreach (QP_CorteQuickpas qp in dispQP)
            {
                var codigoRespuesta = string.Empty;
                var descError = string.Empty;
                var dia = qp.DiaCorte;
                PocSalidaMiscelaneosTarjeta resultado = null;
                decimal pMontoCarga = 0;
                decimal pSaldoDispositivo = 0;
                var tipMov = "";
                var fecRevCostoAdm = qp.FecRevisionCostoAdm;
                int diaComparacion = numDia.Value > 0 ? numDia.Value :
                    fecActual.AddDays(diaPlazo.Value > 0 ? diaPlazo.Value : 5).Day;

                if (dia == diaComparacion)
                {
                    var numReferencia = "";
                    var numTC = qp.NumTarjeta;
                    var monAdm = _repMoneda.Table.FirstOrDefault(x => x.IdMoneda == qp.IdMonedaAdm);
                    pSaldoDispositivo = (decimal)qp.SaldoDispositivo;

                    pMontoCarga = pSaldoDispositivo;
                    var financiado = (int)EnumIdSiNo.No;
                    if (qp.IdFinanciado == (int)EnumIdSiNo.Si)
                    {
                        financiado = (int)EnumIdSiNo.Si;
                        pMontoCarga = (decimal)qp.MonCostoUnidad / (decimal)qp.MesesFinanciamiento;
                        pMontoCarga = decimal.Round(pMontoCarga, 2, MidpointRounding.AwayFromZero);

                        pSaldoDispositivo = pSaldoDispositivo - (decimal)pMontoCarga;

                        if (pSaldoDispositivo < pMontoCarga)
                        {
                            pMontoCarga = pMontoCarga + pSaldoDispositivo;
                            pSaldoDispositivo = pSaldoDispositivo - pMontoCarga;
                            financiado = (int)EnumIdSiNo.No;
                        }
                    }
                    if (pMontoCarga > 0)
                    {
                        numReferencia = _fGenerales.ObtieneSgteSecuencia("SEQ_IdRefProcesador").ToString();
                        tipMov = _repCatalogo.SelectById((int)EnumTipoMovimientoTC.AdquisicionQuickpass).Referencia1;
                        resultado = GenerarCargo(numTC, fecActual, pMontoCarga, monAdm.CodAlpha2, tipMov, numReferencia);
                        codigoRespuesta = resultado != null ? resultado.CodigoRespuesta : string.Empty;
                        descError = resultado != null ? resultado.DescripcionCodRespuesta : string.Empty;

                        if (codigoRespuesta == "00")
                        {
                            _repQuickpass.UpdateOn(x => x.IdQuickpass == qp.IdQuickpass,
                               z => new QP_Quickpass
                               {
                                   SaldoDispositivo = financiado == (int)EnumIdSiNo.Si ? pSaldoDispositivo : 0,
                                   IdFinanciado = financiado
                               });
                            _repQuickpass.UnidadTbjo.Save();
                        }
                    }
                    pMontoCarga = 0;

                    var pSaldoPeaje = (decimal)qp.SaldoPeaje;
                    if (qp.IdPrepago == (int)EnumIdSiNo.Si)
                    {
                        var pMontoInicio = (decimal)qp.MonCargoInicial;
                        pMontoCarga = pSaldoPeaje;
                        pSaldoPeaje = pMontoInicio;
                    }
                    else
                    {
                        pMontoCarga = pSaldoPeaje;
                        pSaldoPeaje = 0;
                    }

                    if (pMontoCarga > 0)
                    {
                        numReferencia = _fGenerales.ObtieneSgteSecuencia("SEQ_IdRefProcesador").ToString();
                        var monPeaje = _repMoneda.Table.FirstOrDefault(x => x.IdMoneda == qp.IdMonedaPeaje);
                        tipMov = _repCatalogo.SelectById((int)EnumTipoMovimientoTC.PeajeQuickpass).Referencia1;
                        resultado = GenerarCargo(numTC, fecActual, pMontoCarga, monPeaje.CodAlpha2, tipMov, numReferencia);
                        codigoRespuesta = resultado != null ? resultado.CodigoRespuesta : string.Empty;
                        descError = resultado != null ? resultado.DescripcionCodRespuesta : string.Empty;

                        if (codigoRespuesta == "00")
                        {
                            _repQuickpass.UpdateOn(x => x.IdQuickpass == qp.IdQuickpass,
                                z => new QP_Quickpass { SaldoPeaje = pSaldoPeaje });
                            _repQuickpass.UnidadTbjo.Save();
                        }
                    }


                    //Verificación de Monto a pagar por administrativo
                    if (fecActual >= fecRevCostoAdm)
                    {
                        var tipQuickpass = _repTipoQuickpass.Table.FirstOrDefault(m => m.IdTipoQuickpass == qp.IdTipoQuickpass);
                        if (qp.MonCostoAdm != tipQuickpass.MonCostoAdm)
                        {
                            _repQuickpass.UpdateOn(x => x.IdQuickpass == qp.IdQuickpass,
                                z => new QP_Quickpass
                                {
                                    MonCostoAdm = tipQuickpass.MonCostoAdm,
                                    FecRevisionCostoAdm = fecRevCostoAdm.Value
                                        .AddMonths(tipQuickpass.MesesVigencia.Value)
                                });
                            _repQuickpass.UnidadTbjo.Save();
                        }
                    }

                    //Cargo Administrativo Mensual
                    numReferencia = _fGenerales.ObtieneSgteSecuencia("SEQ_IdRefProcesador").ToString();
                    pMontoCarga = (decimal)qp.MonCostoAdm;
                    tipMov = _repCatalogo.SelectById((int)EnumTipoMovimientoTC.CargoMensualQuickpass).Referencia1;
                    resultado = GenerarCargo(numTC, fecActual, pMontoCarga, monAdm.CodAlpha2, tipMov, numReferencia);
                }

                if (qp.IdCargoDiario == (int)EnumIdSiNo.Si)
                {
                    pMontoCarga = (decimal)qp.SaldoPeaje;
                    decimal pSaldoPeaje = 0;

                    var numReferencia = "0";
                    var numTC = qp.NumTarjeta;
                    var monPeaje = _repMoneda.Table.FirstOrDefault(x => x.IdMoneda == qp.IdMonedaPeaje);

                    if (pMontoCarga > 0)
                    {
                        numReferencia = _fGenerales.ObtieneSgteSecuencia("SEQ_IdRefProcesador").ToString();
                        tipMov = _repCatalogo.SelectById((int)EnumTipoMovimientoTC.PeajeQuickpass).Referencia1;
                        resultado = GenerarCargo(numTC, fecActual, pMontoCarga,
                            monPeaje.CodAlpha2, tipMov, numReferencia);

                        _repQuickpass.UpdateOn(x => x.IdQuickpass == qp.IdQuickpass,
                            z => new QP_Quickpass { SaldoPeaje = pSaldoPeaje });
                        _repQuickpass.UnidadTbjo.Save();
                    }
                }
            }
            Logger.Log(string.Format("Quickpass, Fin generación de cargos de corte por Quickpass"));
        }

        public PocSalidaMiscelaneosTarjeta GenerarCargo(string numTarjeta, DateTime fechaConsumo, decimal montoTransaccion, string monedaTransaccion, string codMovimiento, string numReferencia)
        {
            if (montoTransaccion <= 0)
            {
                return new PocSalidaMiscelaneosTarjeta { CodigoRespuesta = "00" };
            }
            PocEntradaMiscelaneosTarjeta cargaQPTarjeta = new PocEntradaMiscelaneosTarjeta();
            cargaQPTarjeta.NumTarjeta = numTarjeta;
            cargaQPTarjeta.FechaConsumo = fechaConsumo;
            cargaQPTarjeta.MontoTransaccion = montoTransaccion;
            cargaQPTarjeta.MonedaTransaccion = monedaTransaccion;
            cargaQPTarjeta.CodMovimiento = codMovimiento; //Movimiento Real
            cargaQPTarjeta.NumReferencia = numReferencia;

            var resultado = _appOperacionesATH.AplicaMiscelaneosTarjeta(cargaQPTarjeta);
            return resultado;
        }

        public void EnviarListasQP(PocListaQP pocListaQP)
        {
            var pocParametroSistema = new PocParametroSistema() { IdParametro = "COD_ENTIDAD_QP" };
            var emisor = _repParametroSistema.ObtenerParametroSistema(pocParametroSistema).ValParametro;
            var secGeneracion = 0;

            pocParametroSistema = new PocParametroSistema() { IdParametro = "ENVIO_LN_QP" };
            var dias = Int32.Parse(_repParametroSistema.ObtenerParametroSistema(pocParametroSistema).ValParametro);

            var idListaBlanca = Convert.ToString((int)EnumEstadOperativoQP.ListaBlanca);
            var quickpassValidos = _repCatalogo.Table.Where(x => x.Lista == "LESTADOQUICKPASS" &&
                x.Referencia1.Contains(idListaBlanca)).Select(x=>x.IdCatalogo);

            var tipoLista = 0;
            var fecha = DateTime.Now.Date.AddDays(dias);
            var criteria = new Criteria<QP_Quickpass>();
            criteria.And(m => quickpassValidos.Contains(m.IdEstado));
            criteria.Or(x => x.IdEstadoOperativo == (int)EnumEstadOperativoQP.ListaNegra &&
                _repBitacora.Table.Any(y =>
                    y.IdEstadoOperativo == (int)EnumEstadOperativoQP.ListaNegra &&
                    y.FecInclusionAud >= fecha &&
                    y.IdQuickpass == x.IdQuickpass));

            var lista = _repQuickpass.SelectBy(criteria).ToList();

            if (lista != null && lista.Count > 0)
            {
                secGeneracion = _fGenerales.ObtieneSgteSecuencia("SEQ_IdGeneracion");

                /* Se obtienen todos los QP */
                var listaBlanca = new List<string>();
                var listaGris = new List<string>();
                var listaNegra = new List<string>();
                foreach (QP_Quickpass item in lista)
                {
                    listaBlanca.Add(item.NumQuickpass);
                    if (item.IdEstadoOperativo == (int)EnumEstadOperativoQP.ListaGris)
                    {
                        listaGris.Add(item.NumQuickpass);
                    }
                    if (item.IdEstadoOperativo == (int)EnumEstadOperativoQP.ListaNegra)
                    {
                        listaNegra.Add(item.NumQuickpass);
                    }
                }

                var date = DateTime.Now;

                /* Lista Blanca */
                tipoLista = int.Parse(_repCatalogo.SelectById((int)EnumEstadOperativoQP.ListaBlanca).Referencia1);
                _repServiciosQuickpass.EnviarListasQP(emisor, tipoLista, listaBlanca, date, secGeneracion);

                /* Lista Gris */
                tipoLista = int.Parse(_repCatalogo.SelectById((int)EnumEstadOperativoQP.ListaGris).Referencia1);
                _repServiciosQuickpass.EnviarListasQP(emisor, tipoLista, listaGris, date, secGeneracion);

                /* Lista Negra */
                tipoLista = int.Parse(_repCatalogo.SelectById((int)EnumEstadOperativoQP.ListaNegra).Referencia1);
                _repServiciosQuickpass.EnviarListasQP(emisor, tipoLista, listaNegra, date, secGeneracion);
            }
        }

        public void ResultadoListasDeTags(PocListaTagsTabla pocListaTags)
        {
            //var entidad = pocListaTags.emisor;
            //var pocParametroSistema = new PocParametroSistema() { IdParametro = "COD_ENTIDAD_QP" };
            //var entidad = _repParametroSistema.ObtenerParametroSistema(pocParametroSistema).ValParametro; 
            //DateTime fechaGeneracion = (DateTime)pocListaTags.fechahora.ToDate("yyyyMMddHHmmss");
            //var tipoLista = pocListaTags.tipoLista;
            //var secGeneracion = (int)pocListaTags.secGeneracion;
            var entidad = pocListaTags.emisor;
            DateTime fechaGeneracion = (DateTime)pocListaTags.fechaGeneracion;
            var tipoLista = pocListaTags.tipoLista;
            var secGeneracion = (int)pocListaTags.secGeneracion;

            _repServiciosQuickpass.ResultadoListasDeTags(entidad, fechaGeneracion, tipoLista, secGeneracion);
        }

        public void AplicarCargosMovimientosQP(List<PocQuickpassMovimientos> listaMovs, DateTime? fecDesde = null, DateTime? fecHasta = null)
        {
            Logger.Log(string.Format("Quickpass, Iniciando generación de cargos por movimiento"));

            var tipMov = _repCatalogo.SelectById((int)EnumTipoMovimientoTC.PeajeQuickpass).Referencia1;

            foreach (PocQuickpassMovimientos element in listaMovs)
            {
                var dispQP = _repQuickpass.Table.FirstOrDefault(x => x.IdQuickpass == element.IdQuickpass);

                decimal pSaldoPeaje = 0;
                decimal pMontoMinimo = (decimal)dispQP.MonMinRecarga;
                decimal pMontoInicio = (decimal)dispQP.MonCargoInicial;
                decimal pMontoCarga = 0;

                var monPeaje = _repMoneda.Table.FirstOrDefault(x => x.IdMoneda == dispQP.IdMonedaPeaje);
                var numReferencia = "";

                if (dispQP.IdPrepago == (int)EnumIdSiNo.Si)
                {
                    pSaldoPeaje = (decimal)dispQP.SaldoPeaje - element.Monto;

                    if (pSaldoPeaje <= pMontoMinimo)
                    {
                        pMontoCarga = pMontoInicio - pSaldoPeaje;
                        pSaldoPeaje = pMontoInicio;
                    }
                }
                else
                {
                    pSaldoPeaje = (decimal)dispQP.SaldoPeaje + element.Monto;

                    if ((pSaldoPeaje >= pMontoMinimo) || (dispQP.IdCargoDiario == (int)EnumIdSiNo.Si))
                    {
                        pMontoCarga = pSaldoPeaje;
                        pSaldoPeaje = 0;
                    }
                }

                numReferencia = _fGenerales.ObtieneSgteSecuencia("SEQ_IdRefProcesador").ToString();
                var res = GenerarCargo(element.NumTarjeta, element.FecConsumo, pMontoCarga, monPeaje.CodAlpha2, tipMov, numReferencia);
                var codigoRespuesta = res != null ? res.CodigoRespuesta : string.Empty;
                var descError = res != null ? res.DescripcionCodRespuesta : string.Empty;
                if (codigoRespuesta == "00")
                {
                    if (string.IsNullOrEmpty(res.NumReferencia))
                    {
                        numReferencia = "";
                    }

                    _repQuickpass.UpdateOn(x => x.IdQuickpass == element.IdQuickpass, z => new QP_Quickpass { SaldoPeaje = pSaldoPeaje });
                    _repMovimientos.UpdateOn(x => x.IdQuickpass == element.IdQuickpass && x.IdEstado == (int)EnumEstadoMovQuickpass.Registrado,
                                            z => new QP_Movimientos
                                            {
                                                FecAplicacion = DateTime.Now,
                                                NumRefProcesador = res.NumReferencia,
                                                NumReferenciaPago = numReferencia,
                                                IdEstado = (int)EnumEstadoMovQuickpass.Aplicado
                                            });

                    if (fecDesde.HasValue && fecHasta.HasValue)
                    {
                        _repMovimientos.UpdateOn(x => x.IdQuickpass == element.IdQuickpass &&
                                            x.IdEstado == (int)EnumEstadoMovQuickpass.Error &&
                                            x.FecConsumo >= fecDesde && x.FecConsumo <= fecHasta,
                                            z => new QP_Movimientos
                                            {
                                                FecAplicacion = DateTime.Now,
                                                NumRefProcesador = res.NumReferencia,
                                                NumReferenciaPago = numReferencia,
                                                IdEstado = (int)EnumEstadoMovQuickpass.Aplicado
                                            });
                    }

                    _repMovimientos.UnidadTbjo.Save();
                }
                else
                {
                    _repMovimientos.UpdateOn(x => x.IdQuickpass == element.IdQuickpass && x.IdEstado == (int)EnumEstadoMovQuickpass.Registrado,
                                            z => new QP_Movimientos
                                            {
                                                IdEstado = (int)EnumEstadoMovQuickpass.Error,
                                                DetAplicacionPago = string.Format("{0}-{1}", codigoRespuesta, descError)
                                            });
                    if (fecDesde.HasValue && fecHasta.HasValue)
                    {
                        _repMovimientos.UpdateOn(x => x.IdQuickpass == element.IdQuickpass &&
                                            x.IdEstado == (int)EnumEstadoMovQuickpass.Error &&
                                            x.FecConsumo >= fecDesde && x.FecConsumo <= fecHasta,
                                            z => new QP_Movimientos
                                            {
                                                IdEstado = (int)EnumEstadoMovQuickpass.Error,
                                                DetAplicacionPago = string.Format("{0}-{1}", codigoRespuesta, descError)
                                            });
                    }
                    _repMovimientos.UnidadTbjo.Save();
                }
            }
            Logger.Log(string.Format("Quickpass, Fin generación de cargos por movimiento"));
        }


    }
}

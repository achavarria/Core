using Monibyte.Arquitectura.Dominio.Cargos;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Cargos 
{
    public class CA_MovimientosMap : EntityTypeConfiguration<CA_Movimientos>
    {
        public CA_MovimientosMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMovimiento);

            // Properties
            this.Property(t => t.IdMovimiento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodMovimiento)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.Detalle)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.NumReferencia)
                .HasMaxLength(20);

            this.Property(t => t.NumRefProcesador)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("CA_Movimientos", "Cargos");
            this.Property(t => t.IdMovimiento).HasColumnName("IdMovimiento");
            this.Property(t => t.IdCargo).HasColumnName("IdCargo");
            this.Property(t => t.IdTipoCargo).HasColumnName("IdTipoCargo");
            this.Property(t => t.IdTipoMovimiento).HasColumnName("IdTipoMovimiento");
            this.Property(t => t.CodMovimiento).HasColumnName("CodMovimiento");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.FecMovimiento).HasColumnName("FecMovimiento");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.Monto).HasColumnName("Monto");
            this.Property(t => t.Detalle).HasColumnName("Detalle");
            this.Property(t => t.NumReferencia).HasColumnName("NumReferencia");
            this.Property(t => t.NumRefProcesador).HasColumnName("NumRefProcesador");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");

        }
    }
}

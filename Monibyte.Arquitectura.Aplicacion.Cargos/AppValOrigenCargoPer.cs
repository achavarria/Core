﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Cargos;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public class AppValOrigenCargoPer : AplicacionBase, IAppValOrigenCargoPer
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CA_ValOrigenCargoPer> _repValOrigenCargoPer;
        private IRepositorioMnb<CA_ValOrigenCargo> _repValOrigenCargo;

        public AppValOrigenCargoPer(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CA_ValOrigenCargoPer> repValOrigenCargoPer,
            IRepositorioMnb<CA_ValOrigenCargo> repValOrigenCargo)
        {
            _repCatalogo = repCatalogo;
            _repValOrigenCargoPer = repValOrigenCargoPer;
            _repValOrigenCargo = repValOrigenCargo;
        }

        //Carga de detalle de tabla de rangos dependiendo de si tiene valores en CA_ValOrigenCargo o CA_ValOrigenCargoPer
        public DataResult<PocValOrigenCargoPer> ListarOrigenCargo(PocValOrigenCargoPer filtro, DataRequest request)
        {
            var critcata = new Criteria<GL_Catalogo>();
            var criteria = new Criteria<CA_ValOrigenCargo>();
            var criteriaPer = new Criteria<CA_ValOrigenCargoPer>();
            if (filtro != null)
            {
                if (filtro.IdTipoCargo >= 0)
                {
                    criteria.And(m => m.IdTipoCargo == filtro.IdTipoCargo);
                }
                criteriaPer.And(m => m.IdCargo == filtro.IdCargo);
            }
            var resultadoCargos = _repValOrigenCargo
                .SelectBy(criteria)
                .Join(_repCatalogo.List(critcata),
                    v => v.IdValOrigen,
                    c => c.IdCatalogo,
                    (v, c) => new { v, c })
                .Select(item => new PocValOrigenCargoPer
                {
                    IdTipoCargo = item.v.IdTipoCargo,
                    IdValOrigen = item.v.IdValOrigen,
                    Descripcion = item.c.Descripcion
                }).ToList();

            var resultadoCargosPer = _repValOrigenCargoPer
                .SelectBy(criteriaPer)
                .Join(_repCatalogo.List(critcata),
                    v => v.IdValOrigen,
                    c => c.IdCatalogo,
                    (v, c) => new { v, c })
                .Select(item => new PocValOrigenCargoPer
                {
                    IdCargo = item.v.IdCargo,
                    IdValOrigen = item.v.IdValOrigen,
                    Descripcion = item.c.Descripcion
                }).ToList();

            if (resultadoCargosPer.Count() > 0)
                return resultadoCargosPer.ToDataResult(request);
            else
                return resultadoCargos.ToDataResult(request);
        }
    }
}

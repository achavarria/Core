using System;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_GeoLocalizacion : EntidadBase
    {
        public int IdGeoLocalizacion { get; set; }
        public string IdTransaccion { get; set; }
        public int IdTarjeta { get; set; }
        public int IdEstado { get; set; }
        public string NumTarjeta { get; set; }
        public string Placa { get; set; }
        public string Localizacion { get; set; }
        public string Conductor { get; set; }
        public string NumAutorizacion { get; set; }
        public Nullable<decimal> Distancia { get; set; }
        public Nullable<decimal> CantidadCombustible { get; set; }
        public int IdUnidadMedida { get; set; }
        public decimal MonMovimiento { get; set; }
        public System.DateTime FecMovimiento { get; set; }
        public Nullable<int> IdUsaSensor { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public Nullable<decimal> PrecioPorLitro { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public int NumIntentosFallidos { get; set; }
    }
}

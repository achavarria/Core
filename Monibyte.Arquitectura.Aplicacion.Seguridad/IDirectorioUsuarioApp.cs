﻿using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad
{
    public interface IDirectorioUsuarioApp : IAplicacionBase
    {
        //Mantenimiento de la cuenta
        PocInfoSesion ObtenerCuentaPorTicket(string ticket);
        PocInfoSesion ObtenerCuenta(string codUsuario);
        string IncluirCuenta(PocUsuario cuenta);
        void ModificarCuenta(PocUsuario cuenta);
        void EliminarCuenta(string codUsuario);
        //Mantenimiento de la contraseña
        PocCredencialesDirectorio ReestablecerContrasena(string codUsuario);
        void ActualizarContrasena(PocCambioContrasena datos);
        void RenovarContrasena(string codUsuario);
        void ReiniciarIntentos(string codUsuario);
        //Eventos importantes del usuario
        void CambiarEstado(string codUsuario, int nuevoEstado);
        void RegistrarPerfil(PocRegistroUsuario infoUsuario);
        void ActualizarPerfil(PocInfoUsuario infoUsuario);
        PocInfoUsuario ObtenerInfoPerfil(string codUsuario);
        //Consultas
        List<PocInfoSesion> ObtenerUsuarios(string alias);
        List<PocSesionActiva> ObtenerSesionesActivas();
        //Sesion
        void FinalizarSesion(string ticket);
        PocDobleFactorAut GenerarToken(string codUsuario);
        PocInfoSesion ObtenerUsuarioApi(int clientId);    
        
    }
}
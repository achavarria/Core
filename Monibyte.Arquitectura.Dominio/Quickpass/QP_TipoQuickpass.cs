using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Quickpass
{
    public partial class QP_TipoQuickpass : EntidadBase
    {
        public int IdTipoQuickpass { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> IdPrepago { get; set; }
        public decimal MonCargaInicial { get; set; }
        public decimal MonMinRecargar { get; set; }
        public decimal MonMinListaNegra { get; set; }
        public Nullable<int> IdMonedaPeaje { get; set; }
        public Nullable<int> IdCargoDiario { get; set; }
        public Nullable<decimal> MonCostoAdm { get; set; }
        public Nullable<decimal> MonCostoUnidad { get; set; }
        public Nullable<int> MesesVigencia { get; set; }
        public int IdMonedaAdm { get; set; }
        public Nullable<int> MesesFinanciamiento { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public Nullable<int> IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdEstado { get; set; }
    }
}

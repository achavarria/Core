using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_vwTarjetaUsuarioMap : EntityTypeConfiguration<TC_vwTarjetaUsuario>
    {
        public TC_vwTarjetaUsuarioMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.Property(t => t.CodUsuario)
                .HasMaxLength(20);

            this.Property(t => t.AliasUsuario)
                .HasMaxLength(100);

            this.Property(t => t.CodUsuarioTarjeta)
                .HasMaxLength(20);

            this.Property(t => t.CodUsuarioEmpresa)
                .HasMaxLength(20);

            this.Property(t => t.NumCuenta)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.NumTarjeta)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.NumIdentificacion)
                .HasMaxLength(30);

            this.Property(t => t.NombreImpreso)
                .HasMaxLength(60);

            this.Property(t => t.IdTarjetahabiente)
                .HasMaxLength(30);

            this.Property(t => t.CorreoEstCuenta)
                .HasMaxLength(120);

            this.Property(t => t.CopiaCorreoEstCuenta)
                .HasMaxLength(120);

            // Table & Column Mappings
            this.ToTable("TC_vwTarjetaUsuario", "TCredito");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.AliasUsuario).HasColumnName("AliasUsuario");
            this.Property(t => t.IdEmpresa).HasColumnName("IdEmpresa");
            this.Property(t => t.IdTipoUsuario).HasColumnName("IdTipoUsuario");
            this.Property(t => t.IdUsuarioTarjeta).HasColumnName("IdUsuarioTarjeta");
            this.Property(t => t.CodUsuarioTarjeta).HasColumnName("CodUsuarioTarjeta");
            this.Property(t => t.CodUsuarioEmpresa).HasColumnName("CodUsuarioEmpresa");
            this.Property(t => t.IdTipoPersonaEmpresa).HasColumnName("IdTipoPersonaEmpresa");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.NumCuenta).HasColumnName("NumCuenta");
            this.Property(t => t.IdTipoTarjeta).HasColumnName("IdTipoTarjeta");
            this.Property(t => t.IdEstadoCta).HasColumnName("IdEstadoCta");
            this.Property(t => t.IdEsCuentaMadre).HasColumnName("IdEsCuentaMadre");
            this.Property(t => t.IdCuentaMadre).HasColumnName("IdCuentaMadre");
            this.Property(t => t.FechaVctoLineaCredito).HasColumnName("FechaVctoLineaCredito");
            this.Property(t => t.FecUltCorte).HasColumnName("FecUltCorte");
            this.Property(t => t.DiaCorte).HasColumnName("DiaCorte");
            this.Property(t => t.IdCompania).HasColumnName("IdCompania");
            this.Property(t => t.IdUnidad).HasColumnName("IdUnidad");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.NumTarjeta).HasColumnName("NumTarjeta");
            this.Property(t => t.IdRelacion).HasColumnName("IdRelacion");
            this.Property(t => t.IdTipoLimite).HasColumnName("IdTipoLimite");
            this.Property(t => t.IdEstadoTarjeta).HasColumnName("IdEstadoTarjeta");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.NumIdentificacion).HasColumnName("NumIdentificacion");
            this.Property(t => t.NombreImpreso).HasColumnName("NombreImpreso");
            this.Property(t => t.IdTarjetahabiente).HasColumnName("IdTarjetahabiente");
            this.Property(t => t.IdGrupoEmpresa).HasColumnName("IdGrupoEmpresa");
            this.Property(t => t.IdTipoPlastico).HasColumnName("IdTipoPlastico");
            this.Property(t => t.CamposDinamicos).HasColumnName("CamposDinamicos");
            this.Property(t => t.FecVencimiento).HasColumnName("FecVencimiento");
            this.Property(t => t.LimiteCreditoCtaGlobal).HasColumnName("LimiteCreditoCtaGlobal");
            this.Property(t => t.LimiteCreditoCtaLocal).HasColumnName("LimiteCreditoCtaLocal");
            this.Property(t => t.LimiteCreditoCtaInter).HasColumnName("LimiteCreditoCtaInter");
            this.Property(t => t.LimiteCreditoTarjetaLocal).HasColumnName("LimiteCreditoTarjetaLocal");
            this.Property(t => t.LimiteCreditoTarjetaInter).HasColumnName("LimiteCreditoTarjetaInter");
            this.Property(t => t.SaldoLocal).HasColumnName("SaldoLocal");
            this.Property(t => t.SaldoInternacional).HasColumnName("SaldoInternacional");
            this.Property(t => t.SaldoTarjetaLocal).HasColumnName("SaldoTarjetaLocal");
            this.Property(t => t.SaldoTarjetaInter).HasColumnName("SaldoTarjetaInter");
            this.Property(t => t.IdSoloConsulta).HasColumnName("IdSoloConsulta");
            this.Property(t => t.IdAutoriza).HasColumnName("IdAutoriza");
            this.Property(t => t.CorreoEstCuenta).HasColumnName("CorreoEstCuenta");
            this.Property(t => t.CopiaCorreoEstCuenta).HasColumnName("CopiaCorreoEstCuenta");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.FueAgregada).HasColumnName("FueAgregada");
            this.Property(t => t.IdAplicaMillas).HasColumnName("IdAplicaMillas");
            this.Property(t => t.IdTarjetaTitular).HasColumnName("IdTarjetaTitular");
            this.Property(t => t.NumTarjetaTitular).HasColumnName("NumTarjetaTitular");
            this.Property(t => t.NombreImpresoTitular).HasColumnName("NombreImpresoTitular");
        }
    }
}

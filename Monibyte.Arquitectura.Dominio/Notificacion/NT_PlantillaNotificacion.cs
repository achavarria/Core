using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Notificacion
{
    public partial class NT_PlantillaNotificacion : EntidadBase
    {
        public int IdPlantilla { get; set; }
        public int IdIdioma { get; set; }
        public string Descripcion { get; set; }
        public int IdContexto { get; set; }
        public int IdValorContexto { get; set; }
        public string SubContexto { get; set; }
        public string Asunto { get; set; }
        public string Cuerpo { get; set; }
        public string NomAdjunto { get; set; }
        public string Canal { get; set; }
        public Nullable<int> IdEmpresa { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Clientes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Clientes
{
    public class CL_PersonaMap : EntityTypeConfiguration<CL_Persona>
    {
        public CL_PersonaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPersona);

            // Properties
            this.Property(t => t.IdPersona)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumIdentificacion)
                .HasMaxLength(30);

            this.Property(t => t.PrimerApellido)
                .HasMaxLength(40);

            this.Property(t => t.SegundoApellido)
                .HasMaxLength(40);

            this.Property(t => t.PrimerNombre)
                .HasMaxLength(40);

            this.Property(t => t.SegundoNombre)
                .HasMaxLength(40);

            this.Property(t => t.NombreCompleto)
                .HasMaxLength(160);

            this.Property(t => t.RazonSocial)
                .HasMaxLength(160);

            // Table & Column Mappings
            this.ToTable("CL_Persona", "Cliente");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.NumIdentificacion).HasColumnName("NumIdentificacion");
            this.Property(t => t.FecVenceIdentificacion).HasColumnName("FecVenceIdentificacion");
            this.Property(t => t.IdTipoPersona).HasColumnName("IdTipoPersona");
            this.Property(t => t.IdIdentificacion).HasColumnName("IdIdentificacion");
            this.Property(t => t.PrimerApellido).HasColumnName("PrimerApellido");
            this.Property(t => t.SegundoApellido).HasColumnName("SegundoApellido");
            this.Property(t => t.PrimerNombre).HasColumnName("PrimerNombre");
            this.Property(t => t.SegundoNombre).HasColumnName("SegundoNombre");
            this.Property(t => t.NombreCompleto).HasColumnName("NombreCompleto");
            this.Property(t => t.RazonSocial).HasColumnName("RazonSocial");
            this.Property(t => t.FecNacimiento).HasColumnName("FecNacimiento");
            this.Property(t => t.IdGenero).HasColumnName("IdGenero");
            this.Property(t => t.IdEstadoCivil).HasColumnName("IdEstadoCivil");
            this.Property(t => t.IdPaisNacimiento).HasColumnName("IdPaisNacimiento");
            this.Property(t => t.FecIngreso).HasColumnName("FecIngreso");
            this.Property(t => t.IdTipoRelacion).HasColumnName("IdTipoRelacion");
            this.Property(t => t.IdEjecutivo).HasColumnName("IdEjecutivo");
            this.Property(t => t.IdProfesion).HasColumnName("IdProfesion");
            this.Property(t => t.IdFallecido).HasColumnName("IdFallecido");
            this.Property(t => t.FecFallecido).HasColumnName("FecFallecido");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

﻿
namespace Monibyte.Arquitectura.Dominio.TEF.Repositorios
{
    public interface IParametrosSINPE
    {
        string ObtenerParamSinpe(string param, string sectionName, bool web = true);
    }
}

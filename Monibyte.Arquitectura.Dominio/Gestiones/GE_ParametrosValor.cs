using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_ParametrosValor : EntidadBase
    {
        public int Id { get; set; }
        public int IdGestion { get; set; }
        public string Valor { get; set; }
        public Nullable<int> IdAccion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

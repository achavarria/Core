﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Generales
{
    public class ParametroSistema : AplicacionBase, IParametroSistema
    {
        private IRepositorioMnb<GL_ParametroSistema> _repParametroSistema;
        private IRepositorioMnb<GL_ParametroCompania> _repParametroCompania;

        public ParametroSistema(
             IRepositorioMnb<GL_ParametroSistema> repParametroSistema,
             IRepositorioMnb<GL_ParametroCompania> repParametroCompania)
        {
            _repParametroSistema  = repParametroSistema;
            _repParametroCompania = repParametroCompania;
        }

        public IEnumerable<PocParametroSistema> ListaParametroSistema()
        {
            var parametros = _repParametroSistema.Table.ToList();
            return parametros.ProyectarComo<PocParametroSistema>();
        }

        public PocParametroSistema ObtenerParametroSistema(PocParametroSistema pocEntrada)
        {
            var resultado = new PocParametroSistema();
            if (pocEntrada.IdCompania.HasValue)
            {
                var parametros = _repParametroCompania.Table.FirstOrDefault(
                x => x.IdParametro == pocEntrada.IdParametro
                    && x.IdCompania == pocEntrada.IdCompania);
                resultado = parametros.ProyectarComo<PocParametroSistema>();
            }
            else
            {
                var parametros = _repParametroSistema.SelectById(pocEntrada.IdParametro);
                resultado = parametros.ProyectarComo<PocParametroSistema>();
            }
            return resultado;
        }
        public void InsertaParametroSistema(PocParametroSistema pocEntrada)
        {
            var entrada = pocEntrada.ProyectarComo<GL_ParametroSistema>();
            _repParametroSistema.Insert(entrada);
        }
        public void ActualizaParametroSistema(PocParametroSistema pocEntrada)
        {
            var entrada = pocEntrada.ProyectarComo<GL_ParametroSistema>();
            _repParametroSistema.Update(entrada);
        }
        public void EliminaParametroSistema(PocParametroSistema pocEntrada)
        {
            var entrada = pocEntrada.ProyectarComo<GL_ParametroSistema>();
            _repParametroSistema.Delete(entrada);
        }
    }
}

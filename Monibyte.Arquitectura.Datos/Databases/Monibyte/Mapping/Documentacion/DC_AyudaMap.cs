using Monibyte.Arquitectura.Dominio.Documentacion;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Documentacion
{
    public class DC_AyudaMap : EntityTypeConfiguration<DC_Ayuda>
    {
        public DC_AyudaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdAyuda);

            // Properties
            this.Property(t => t.Imagen)
                .HasMaxLength(100);

            this.Property(t => t.Titulo)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("DC_Ayuda", "Documentacion");
            this.Property(t => t.IdAyuda).HasColumnName("IdAyuda");
            this.Property(t => t.IdAccion).HasColumnName("IdAccion");
            this.Property(t => t.IdIdioma).HasColumnName("IdIdioma");
            this.Property(t => t.IdOrden).HasColumnName("IdOrden");
            this.Property(t => t.VigenciaDesde).HasColumnName("VigenciaDesde");
            this.Property(t => t.VigenciaHasta).HasColumnName("VigenciaHasta");
            this.Property(t => t.Parrrafo).HasColumnName("Parrrafo");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.Imagen).HasColumnName("Imagen");
            this.Property(t => t.Titulo).HasColumnName("Titulo");
            this.Property(t => t.IdAlineacionImagen).HasColumnName("IdAlineacionImagen");
            this.Property(t => t.IdTipoParrafo).HasColumnName("IdTipoParrafo");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

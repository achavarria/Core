﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Configuracion.Repositorios;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Procesador;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.Tarjetas.SatGeo;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Linq;
using System.Transactions;
using Monibyte.Arquitectura.Dominio.Tarjetas.SatGeo;
using Monibyte.Arquitectura.Datos.SatGeo;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public class GeoLocalizacion : AplicacionBase, IGeoLocalizacion
    {
        private IRepositorioTarjeta _repTarjeta;
        private IServiciosSatGeo _servicioSatGeo;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<PT_Autorizaciones> _repAutorizaciones;
        private IRepositorioMnb<TC_GeoLocalizacion> _repGeoLocalizacion;

        private IRepositorioTipoComercio _repTipoComercio;
        private IRepositorioMnb<TC_MovimientoCorte> _repMovCorte;
        private IRepositorioMnb<TC_MovimientoCorteHist> _repMovCorteHist;
        private IRepositorioMoneda _repMoneda;
        private IRepositorioMnb<CA_CargosGeo> _repCargosGeo;
        private IRepositorioMnb<PT_AutorizacionesGeo> _repAutorizacionesGeo;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;
        private IRepositorioMnb<GL_ParametroCompania> _repParamCompania;
        private IRepositorioEmail _repEmail;
        private IRepositorioMnb<CA_CargosPorPersona> _repCargosPersona;
        private IRepositorioMnb<TC_Cuenta> _repCuenta;

        public GeoLocalizacion(
            IRepositorioTarjeta repTarjeta,
            IServiciosSatGeo servicioSatGeo,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<PT_Autorizaciones> repAutorizaciones,
            IRepositorioMnb<TC_GeoLocalizacion> repGeoLocalizacion,

            IRepositorioTipoComercio repTipoComercio,
            IRepositorioMnb<TC_MovimientoCorte> repMovCorte,
            IRepositorioMnb<TC_MovimientoCorteHist> repMovCorteHist,
            IRepositorioMoneda repMoneda,
            IRepositorioMnb<CA_CargosGeo> repCargosGeo,
            IRepositorioMnb<PT_AutorizacionesGeo> repAutorizacionesGeo,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema,
            IRepositorioMnb<GL_ParametroCompania> repParamCompania,
            IRepositorioEmail repEmail,
            IRepositorioMnb<CA_CargosPorPersona> repCargosPersona,
            IRepositorioMnb<TC_Cuenta> repCuenta)
        {
            _repTarjeta = repTarjeta;
            _servicioSatGeo = servicioSatGeo;
            _repCatalogo = repCatalogo;
            _repAutorizaciones = repAutorizaciones;
            _repGeoLocalizacion = repGeoLocalizacion;

            _repTipoComercio = repTipoComercio;
            _repMovCorte = repMovCorte;
            _repMovCorteHist = repMovCorteHist;
            _repMoneda = repMoneda;
            _repCargosGeo = repCargosGeo;
            _repAutorizacionesGeo = repAutorizacionesGeo;
            _repParamSistema = repParamSistema;
            _repParamCompania = repParamCompania;
            _repEmail = repEmail;
            _repCargosPersona = repCargosPersona;
            _repCuenta = repCuenta;
        }

        public void GuardarGeoLocalizacion()
        {
            using (var txn = new TransactionScope(TransactionScopeOption.RequiresNew,
                new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                bool actualizo = false;
                var criteriaCargo = new Criteria<CA_CargosPorPersona>();
                criteriaCargo.And(X => X.IdTipoCargo == (int)EnumTipoCargo.CargoGeo);
                var cuentas = _repCuenta.Table
                    .Join(_repCargosPersona.SelectBy(criteriaCargo),
                            c => c.IdEmpresa,
                            cp => cp.IdPersona, (c, cp) => new { c.IdCuenta })
                            .Distinct().Select(x => x.IdCuenta).ToList();

                var fecMenosUno = DateTime.Now.AddDays(-1);

                var criteriaTc = new Criteria<TC_Tarjeta>();
                criteriaTc.And(x => x.Placa != null);
                criteriaTc.And(x => cuentas.Contains(x.IdCuenta));

                var tarjetas = _repTarjeta.SelectBy(criteriaTc).ToList();

                Logger.Log("Buscando Autorización", tipoLog: new string[] { "ArchivoGeo" });
                var movtsAut = _repAutorizacionesGeo.Table.ToList();

                foreach (var tc in tarjetas)
                {
                    var criteriaGeo = new Criteria<TC_GeoLocalizacion>();
                    criteriaGeo.And(x => x.NumTarjeta == tc.NumTarjeta);
                    criteriaGeo.And(x => x.Latitud != null);
                    criteriaGeo.And(x => x.Latitud != "");
                    criteriaGeo.And(x => x.Longitud != null);
                    criteriaGeo.And(x => x.Longitud != "");
                    criteriaGeo.And(x => x.FecInclusionAud >= fecMenosUno);

                    var autorizaciones = movtsAut.Where(x => x.CardNumber == tc.NumTarjeta);

                    var movsGeo = _repGeoLocalizacion
                        .SelectBy(criteriaGeo).ToList();
                    if (movsGeo.Any())
                    {
                        var id = movsGeo.Max(y => Convert.ToInt32(y.IdTransaccion));
                        autorizaciones = autorizaciones.Where(x => x.Id > id);
                    }

                    if (autorizaciones.Any())
                    {
                        foreach (var item in autorizaciones)
                        {
                            var aut = item.AuthorizationIdResponse.Substring(1,
                                item.AuthorizationIdResponse.Length - 1);

                            var amounth = Convert.ToDecimal(item.TransactionAmount);
                            var moneda = _repMoneda.Table.First(x => x.NumInternacional == item.CurrencyCodeCb);

                            var date = (item.LocalTransactionDate +
                                    item.LocalTransactionTime).ToDate("yyyyMMddHHmmss").Value;

                            var model = new SatgeoIn
                            {
                                TransactionID = item.Id,
                                CardNumber = tc.NumTarjeta,
                                CurrencyType = item.TransactionCurrencyCode,
                                Date = string.Format("{0}-{1}-{2} {3}:{4}:{5}",
                                    date.Year, date.Month.ToString("00"), date.Day.ToString("00"),
                                    date.Hour.ToString("00"), date.Minute.ToString("00"), date.Second.ToString("00")),
                                Detail = item.CardAcceptorNameLocation,
                                NameCardHolder = tc.NombreImpreso,
                                Plate = tc.Placa,
                                Value = Convert.ToInt32(item.TransactionAmount).ToString()
                            };

                            Logger.Log("Consultando SatGeo", tipoLog: new string[] { "ArchivoGeo" });
                            var infoGeo = _servicioSatGeo.ObtieneInfoMovimiento(model);
                            if (infoGeo != null && string.IsNullOrEmpty(infoGeo.Mensaje))
                            {
                                Logger.Log("Incluyento Transaccion " + item.Id.ToString() + " JSON: "
                                    + infoGeo.ToJson(), tipoLog: new string[] { "ArchivoGeo" });
                                var numAut = Convert.ToInt32(item.AuthorizationIdResponse);

                                EliminaGeo(item);

                                var entradaGeo = new TC_GeoLocalizacion
                                {
                                    IdTransaccion = item.Id.ToString(),
                                    Conductor = string.IsNullOrEmpty(infoGeo.DriverName) ?
                                        "Sin Conductor" : infoGeo.DriverName,
                                    Distancia = infoGeo.Distance,
                                    FecMovimiento = (item.LocalTransactionDate).ToDate("yyyyMMdd").Value,
                                    CantidadCombustible = infoGeo.AmountFuelRecope != 0 ?
                                        (decimal)infoGeo.AmountFuelRecope : Convert.ToDecimal(infoGeo.AmountFuel),
                                    IdUnidadMedida = (int)EnumUnidadMedida.Litros,
                                    Longitud = infoGeo.Longitude.ToString(),
                                    Latitud = infoGeo.Latitude.ToString(),
                                    IdTarjeta = tc.IdTarjeta,
                                    Localizacion = string.IsNullOrEmpty(infoGeo.Location) ?
                                        "Sin Localización" : infoGeo.Location,
                                    MonMovimiento = Convert.ToDecimal(item.TransactionAmount),
                                    NumAutorizacion = numAut.ToString(),
                                    NumTarjeta = tc.NumTarjeta,
                                    Placa = tc.Placa,
                                    FecInclusionAud = item.FecInclusionAud,
                                    IdEstado = infoGeo.isSensorFuel ? (int)EnumIdEstado.Activo : (int)EnumIdEstado.Inactivo,
                                    IdUsaSensor = infoGeo.isSensorFuel ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No,
                                    PrecioPorLitro = (decimal)infoGeo.PriceFuelRecope
                                };

                                if (!infoGeo.isSensorFuel)
                                {
                                    var maxId = _repGeoLocalizacion.Table.Where(
                                    x => x.IdEstado == (int)EnumIdEstado.Inactivo &&
                                    x.Placa == tc.Placa).Max(x => x.IdTransaccion);

                                    if (!string.IsNullOrEmpty(maxId))
                                    {
                                        _repGeoLocalizacion.UpdateOn(x => x.IdTransaccion == maxId,
                                            z => new TC_GeoLocalizacion
                                            {
                                                Localizacion = infoGeo.Location,
                                                Distancia = infoGeo.Distance,
                                                CantidadCombustible = infoGeo.AmountFuelRecope != 0 ?
                                                    (decimal)infoGeo.AmountFuelRecope : Convert.ToDecimal(infoGeo.AmountFuel),
                                                Conductor = infoGeo.DriverName,
                                                IdEstado = (int)EnumIdEstado.Activo,
                                                Longitud = infoGeo.Longitude.ToString(),
                                                Latitud = infoGeo.Latitude.ToString()
                                            });

                                        var criteriaMvtTc = new Criteria<TC_MovimientoCorte>();
                                        criteriaMvtTc.And(x => x.IdTarjeta == tc.IdTarjeta);
                                        criteriaMvtTc.And(x => x.IdMoneda == moneda.IdMoneda);
                                        criteriaMvtTc.And(x => x.MonMovimiento == amounth);
                                        criteriaMvtTc.And(x => x.NumAutorizacion == aut);
                                        criteriaMvtTc.And(x => x.FecTransaccion == date);

                                        var movTarjeta = _repMovCorte.SelectBy(criteriaMvtTc).FirstOrDefault();

                                        var json = new PocGeoLocalizacion
                                        {
                                            CantidadCombustible = infoGeo.AmountFuelRecope != 0 ?
                                                (decimal)infoGeo.AmountFuelRecope : Convert.ToDecimal(infoGeo.AmountFuel),
                                            Conductor = infoGeo.DriverName,
                                            Distancia = infoGeo.Distance,
                                            Latitud = infoGeo.Latitude.ToString(),
                                            Longitud = infoGeo.Longitude.ToString(),
                                            Localizacion = infoGeo.Location,
                                            Placa = infoGeo.Plate,
                                            UnidadMedida = _repCatalogo.SelectById((int)EnumUnidadMedida.Litros).Codigo
                                        }.ToJson();

                                        if (movTarjeta != null)
                                        {
                                            actualizo = true;
                                            _repMovCorte.UpdateOn(x => x.IdMovimiento == movTarjeta.IdMovimiento,
                                                z => new TC_MovimientoCorte
                                                {
                                                    GeoCamposDinamicos = json
                                                });
                                            _repMovCorte.UnidadTbjo.Save();
                                        }
                                        else
                                        {
                                            var criteriaMovTcHist = new Criteria<TC_MovimientoCorteHist>();
                                            criteriaMovTcHist.And(x => x.IdTarjeta == tc.IdTarjeta);
                                            criteriaMovTcHist.And(x => x.IdMoneda == moneda.IdMoneda);
                                            criteriaMovTcHist.And(x => x.MonMovimiento == amounth);
                                            criteriaMovTcHist.And(x => x.NumAutorizacion == aut);
                                            criteriaMovTcHist.And(x => x.FecTransaccion == date);

                                            var movTarjetaHist = _repMovCorteHist.SelectBy(criteriaMovTcHist).FirstOrDefault();

                                            if (movTarjetaHist != null)
                                            {
                                                actualizo = true;
                                                _repMovCorteHist.UpdateOn(x => x.IdMovimiento == movTarjetaHist.IdMovimiento,
                                                    z => new TC_MovimientoCorteHist
                                                    {
                                                        GeoCamposDinamicos = json
                                                    });
                                                _repMovCorteHist.UnidadTbjo.Save();
                                            }
                                        }
                                        if (actualizo)
                                        {
                                            _repGeoLocalizacion.DeleteOn(x => x.IdTransaccion == maxId);
                                        }
                                    }
                                }
                                _repGeoLocalizacion.Insert(entradaGeo);
                                _repGeoLocalizacion.UnidadTbjo.Save();
                            }
                            else
                            {
                                var exist = ManejaIntentosFallidos(item, tc);
                                if (!exist)
                                {
                                    var correos = _repParamSistema.Table.FirstOrDefault(
                                        x => x.IdParametro == "NOTIFICACIERRESA").ValParametro;
                                    var remitente = _repParamCompania.Table.FirstOrDefault(
                                        x => x.IdParametro == "ESTADO_CUENTA_REMITENTE").ValParametro;

                                    var sender = new SenderConfig(remitente);
                                    var emailExterno = new EmailConfig("GeoLocalización",
                                        string.Format("Error en el servicio SendInfoFuelPayment" +
                                        " con la transacción # {0} <br /> Código: {1} <br /> Detalle: {2}",
                                        item.Id, infoGeo.CodError, infoGeo.Mensaje));
                                    emailExterno.AddTo(correos);
                                    _repEmail.EnviarEmail(sender, emailExterno);
                                }
                                if (infoGeo.CodError == (int)EnumTipoErrorGeo.DispositivoNoCoincide)
                                {
                                    _repTarjeta.UpdateOn(x => x.NumTarjeta == item.CardNumber,
                                        z => new TC_Tarjeta
                                        {
                                            Placa = null
                                        });
                                }
                            }
                        }
                    }
                }
                txn.Complete();
            }
        }

        private bool ManejaIntentosFallidos(PT_AutorizacionesGeo item, TC_Tarjeta tarjeta)
        {
            var exist = _repGeoLocalizacion.Table.Any(x => x.IdTransaccion == item.Id.ToString());
            var intentos = 1;
            if (exist)
            {
                intentos = _repGeoLocalizacion.Table.FirstOrDefault(x =>
                    x.IdTransaccion == item.Id.ToString()).NumIntentosFallidos + intentos;
            }
            if (!exist)
            {
                var entradaGeo = new TC_GeoLocalizacion
                {
                    IdTransaccion = item.Id.ToString(),
                    Conductor = "Sin Conductor",
                    Distancia = 0,
                    FecMovimiento = (item.LocalTransactionDate).ToDate("yyyyMMdd").Value,
                    CantidadCombustible = 0,
                    IdUnidadMedida = (int)EnumUnidadMedida.Litros,
                    Longitud = "",
                    Latitud = "",
                    IdTarjeta = tarjeta.IdTarjeta,
                    Localizacion = "Sin Localizacion",
                    MonMovimiento = Convert.ToDecimal(item.TransactionAmount),
                    NumAutorizacion = item.AuthorizationIdResponse,
                    NumTarjeta = item.CardNumber,
                    Placa = tarjeta.Placa,
                    FecInclusionAud = item.FecInclusionAud,
                    IdEstado = (int)EnumIdEstado.Inactivo,
                    IdUsaSensor = (int)EnumIdSiNo.No,
                    PrecioPorLitro = 0,
                    NumIntentosFallidos = intentos
                };
                _repGeoLocalizacion.Insert(entradaGeo);
            }
            else
            {
                _repGeoLocalizacion.UpdateOn(x =>
                   x.IdTransaccion == item.Id.ToString(),
                       z => new TC_GeoLocalizacion
                       {
                           NumIntentosFallidos = intentos
                       });
            }
            _repGeoLocalizacion.UnidadTbjo.Save();

            return exist;
        }

        private void EliminaGeo(PT_AutorizacionesGeo item)
        {
            var exist = _repGeoLocalizacion.Table
                .Any(x => x.IdTransaccion == item.Id.ToString() &&
                x.NumIntentosFallidos > 0);
            if (exist)
            {
                Logger.Log("Eliminando geo vacio Id: " + item.Id, tipoLog: new string[] { "ArchivoGeo" });
                _repGeoLocalizacion.DeleteOn(x => x.IdTransaccion == item.Id.ToString());
                _repGeoLocalizacion.UnidadTbjo.Save();
            }
        }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Cargos;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public class AppDetRangosCargo : AplicacionBase, IAppDetRangosCargo
    {
        private IRepositorioMnb<CA_DetRangosCargo> _repDetRangosCargo;

        public AppDetRangosCargo(IRepositorioMnb<CA_DetRangosCargo> repDetRangosCargo)
        {
            _repDetRangosCargo = repDetRangosCargo;
        }

        public void Eliminar(PocDetRangosCargo registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CA_DetRangosCargo>();
                _repDetRangosCargo.Delete(entrada);
                _repDetRangosCargo.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocDetRangosCargo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_DetRangosCargo>();
                    _repDetRangosCargo.Insert(entrada);
                    _repDetRangosCargo.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocDetRangosCargo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocDetRangosCargo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_DetRangosCargo>();
                    _repDetRangosCargo.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repDetRangosCargo.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocDetRangosCargo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocDetRangosCargo> Listar(PocDetRangosCargo filtro, DataRequest request)
        {
            var criteria = new Criteria<CA_DetRangosCargo>();
            if (filtro != null)
            {
                if (filtro.IdTablaRangos >= 0)
                {
                    criteria.And(m => m.IdTablaRangos == filtro.IdTablaRangos);
                }
            }
            var resultado = _repDetRangosCargo.SelectBy(criteria)
                .Select(item => new PocDetRangosCargo
                {
                    IdTablaRangos = item.IdTablaRangos,
                    MonCargo = item.MonCargo,
                    RangoMax = item.RangoMax,
                    RangoMin = item.RangoMin

                });

            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocDetRangosCargo registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.IdTablaRangos < 0)
            {
                validacion.AddError("Err_0035");
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Tesoreria;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Tesoreria
{
    public interface IAppOpTipoCambio : IAplicacionBase
    {
        void Insertar(PocTipoCambio registro);
        List<PocTipoCambio> Obtener(PocTipoCambio filtro);
        PocTipoCambio ObtenerTipoDeCambio(PocTipoCambio filtro);
        DataResult<PocTipoCambio> Listar(PocTipoCambio filtro, DataRequest request);
    }
}

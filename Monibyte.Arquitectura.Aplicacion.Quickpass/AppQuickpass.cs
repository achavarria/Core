﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Aplicacion.Tarjetas.MQATH;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Quickpass;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public class AppQuickpass : AplicacionBase, IAppQuickpass
    {
        private IRepositorioCuenta _repCuenta;
        private IRepositorioMoneda _repMoneda;
        private IRepositorioTarjeta _repTarjeta;
        private IRepositorioCatalogo _repCatalogo;
        private IOperacionesATH _appOperacionesATH;
        private IRepositorioMnb<CL_Persona> _repPersona;
        private IRepositorioMnb<QP_Bitacora> _repBitacora;
        private IRepositorioMnb<QP_Quickpass> _repQuickpass;
        private IRepositorioMnb<QP_TipoQuickpass> _repTipoQuickpass;

        public AppQuickpass(
            IRepositorioCuenta repCuenta,
            IRepositorioMoneda repMoneda,
            IRepositorioTarjeta repTarjeta,
            IRepositorioCatalogo repCatalogo,
            IOperacionesATH appOperacionesATH,
            IRepositorioMnb<CL_Persona> repPersona,
            IRepositorioMnb<QP_Bitacora> repBitacora,
            IRepositorioMnb<QP_Quickpass> repQuickpass,
            IRepositorioMnb<QP_TipoQuickpass> repTipoQuickpass)
        {
            _repCuenta = repCuenta;
            _repMoneda = repMoneda;
            _repTarjeta = repTarjeta;
            _repCatalogo = repCatalogo;
            _appOperacionesATH = appOperacionesATH;
            _repPersona = repPersona;
            _repBitacora = repBitacora;
            _repQuickpass = repQuickpass;
            _repTipoQuickpass = repTipoQuickpass;
        }

        public void Eliminar(PocQuickpass registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<QP_Quickpass>();
                _repQuickpass.Delete(entrada);
                _repQuickpass.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocQuickpass registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<QP_Quickpass>();
                    entrada.IdQuickpass = _fGenerales.ObtieneSgteSecuencia("SEQ_IdQuickpass");
                    _repQuickpass.Insert(entrada);
                    InsertarBitacoraQP(entrada, null);
                    _repQuickpass.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocQuickpass));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocQuickpass> Listar(PocQuickpass filtro, DataRequest request)
        {
            var criteria = new Criteria<QP_Quickpass>();
            if (filtro != null)
            {
                criteria.And(m => m.IdPersona == null);
                if (!string.IsNullOrEmpty(filtro.NumQuickpass))
                {
                    criteria.And(m => m.NumQuickpass.Contains(filtro.NumQuickpass));
                }
                if (!string.IsNullOrEmpty(filtro.NumDocumento))
                {
                    criteria.And(m => m.NumDocumento.Contains(filtro.NumDocumento));
                }
            }
            var resultado = _repQuickpass.SelectBy(criteria)
                .Join(_repCatalogo.List("LESTADOQUICKPASS"),
                    qp => qp.IdEstado,
                    ct => ct.IdCatalogo,
                    (qp, ct) => new { qp, ct })
                .GroupJoin(_repCatalogo.List("LESTADOPERATIVOQP"),
                    tmp => tmp.qp.IdEstadoOperativo,
                    ctOP => ctOP.IdCatalogo,
                    (tmp, ctOP) => new { tmp.qp, tmp.ct, ctOP = ctOP.FirstOrDefault() })
                .Select(sel => new PocQuickpass
                {
                    IdQuickpass = sel.qp.IdQuickpass,
                    NumQuickpass = sel.qp.NumQuickpass,
                    IdTipoQuickpass = sel.qp.IdTipoQuickpass,
                    IdTarjeta = sel.qp.IdTarjeta,
                    IdPersona = sel.qp.IdPersona,
                    IdCuenta = sel.qp.IdCuenta,
                    NumPlaca = sel.qp.NumPlaca,
                    IdEstado = sel.qp.IdEstado,
                    FecRevisionCostoAdm = sel.qp.FecRevisionCostoAdm,
                    IdFinanciado = sel.qp.IdFinanciado,
                    SaldoDispositivo = sel.qp.SaldoDispositivo,
                    SaldoPeaje = sel.qp.SaldoPeaje,
                    IdPrepago = sel.qp.IdPrepago,
                    MonCostoAdm = sel.qp.MonCostoAdm,
                    MonCargoInicial = sel.qp.MonCargoInicial,
                    MonMinRecarga = sel.qp.MonMinRecarga,
                    MonCostoUnidad = sel.qp.MonCostoUnidad,
                    IdMonedaAdm = sel.qp.IdMonedaAdm,
                    IdMonedaPeaje = sel.qp.IdMonedaPeaje,
                    IdCargoDiario = sel.qp.IdCargoDiario,
                    MonMinListaNegra = sel.qp.MonMinListaNegra,
                    MesesFinanciamiento = sel.qp.MesesFinanciamiento,
                    NumDocumento = sel.qp.NumDocumento,
                    IdEstadoOperativo = sel.qp.IdEstadoOperativo,
                    FecVencimiento = sel.qp.FecVencimiento,
                    DescEstado = sel.ct.Descripcion,
                    DescEstadoOperativo = sel.ctOP != null ? sel.ctOP.Descripcion : string.Empty,
                    NombreCliente = string.Empty,
                    NumTarjeta = string.Empty,
                    DescTipoQuickpass = string.Empty,
                    NumCuenta = string.Empty,
                    CargoDiario = string.Empty,
                    Financiado = string.Empty,
                    DescPrepago = string.Empty,
                }).OrderBy(qp => qp.NumQuickpass).ToList();
            return resultado.ToDataResult(request);
        }

        public DataResult<PocQuickpass> ListarQuickpassAsignados(PocQuickpass filtro, DataRequest request)
        {
            var criteria = new Criteria<QP_Quickpass>();
            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.NumQuickpass))
                {
                    criteria.And(m => m.NumQuickpass.Contains(filtro.NumQuickpass));
                }
                if (!string.IsNullOrEmpty(filtro.NumDocumento))
                {
                    criteria.And(m => m.NumDocumento.Contains(filtro.NumDocumento));
                }
                if (filtro.IdPersona.HasValue && filtro.IdPersona != 0)
                {
                    criteria.And(m => m.IdPersona == filtro.IdPersona);
                }
            }
            else criteria.And(m => m.IdPersona != null);
            var resultado = _repQuickpass.SelectBy(criteria)
                .Join(_repCatalogo.List("LESTADOQUICKPASS"), /* Estado */
                    qp => qp.IdEstado,
                    catEst => catEst.IdCatalogo,
                    (qp, catEst) => new { qp, catEst })
                .Join(_repCatalogo.List("LESTADOPERATIVOQP"), /*Estado Operativo*/
                    tmp => tmp.qp.IdEstadoOperativo,
                    catEstOP => catEstOP.IdCatalogo,
                    (tmp, catEstOP) => new { tmp.qp, tmp.catEst, catEstOP })
                .Join(_repCatalogo.List("LSINO"), /* Cargo Diario */
                    tmp => tmp.qp.IdCargoDiario,
                    catCargoDiario => catCargoDiario.IdCatalogo,
                    (tmp, catCargoDiario) => new { tmp.qp, tmp.catEst, tmp.catEstOP, catCargoDiario })
                .Join(_repCatalogo.List("LSINO"), /* Financiado */
                    tmp => tmp.qp.IdFinanciado,
                    catFinanciado => catFinanciado.IdCatalogo,
                    (tmp, catFinanciado) => new
                    {
                        tmp.qp,
                        tmp.catEst,
                        tmp.catEstOP,
                        tmp.catCargoDiario,
                        catFinanciado
                    })
                .Join(_repCatalogo.List("LSINO"), /* Prepago */
                    tmp => tmp.qp.IdPrepago,
                    catPrepago => catPrepago.IdCatalogo,
                    (tmp, catPrepago) => new
                    {
                        tmp.qp,
                        tmp.catEst,
                        tmp.catEstOP,
                        tmp.catCargoDiario,
                        tmp.catFinanciado,
                        catPrepago
                    })
                .Join(_repPersona.Table, /* Nombre Cliente*/
                    tmp => tmp.qp.IdPersona,
                    persona => persona.IdPersona,
                    (tmp, persona) => new
                    {
                        tmp.qp,
                        tmp.catEst,
                        tmp.catEstOP,
                        tmp.catCargoDiario,
                        tmp.catFinanciado,
                        tmp.catPrepago,
                        persona
                    })
                .Join(_repCuenta.Table, /* Cuenta */
                    tmp => tmp.qp.IdCuenta,
                    cuenta => cuenta.IdCuenta,
                    (tmp, cuenta) => new
                    {
                        tmp.qp,
                        tmp.catEst,
                        tmp.catEstOP,
                        tmp.catCargoDiario,
                        tmp.catFinanciado,
                        tmp.catPrepago,
                        tmp.persona,
                        cuenta
                    })
                .Join(_repTipoQuickpass.Table, /* Tipo Quick Pass */
                    tmp => tmp.qp.IdTipoQuickpass,
                    catTipoQP => catTipoQP.IdTipoQuickpass,
                    (tmp, catTipoQP) => new
                    {
                        tmp.qp,
                        tmp.catEst,
                        tmp.catEstOP,
                        tmp.catCargoDiario,
                        tmp.catFinanciado,
                        tmp.catPrepago,
                        tmp.persona,
                        tmp.cuenta,
                        catTipoQP
                    })
                .GroupJoin(_repTarjeta.Table, /* Tarjeta */
                    tmp => tmp.qp.IdTarjeta,
                    tarjeta => tarjeta.IdTarjeta,
                    (tmp, tarjeta) => new
                    {
                        tmp.qp,
                        tmp.catEst,
                        tmp.catEstOP,
                        tmp.catCargoDiario,
                        tmp.catFinanciado,
                        tmp.catPrepago,
                        tmp.persona,
                        tmp.cuenta,
                        tmp.catTipoQP,
                        tarjeta = tarjeta.FirstOrDefault()
                    })
                .Select(sel => new PocQuickpass
                {
                    IdQuickpass = sel.qp.IdQuickpass,
                    NumQuickpass = sel.qp.NumQuickpass,
                    IdTipoQuickpass = sel.qp.IdTipoQuickpass,
                    IdTarjeta = sel.qp.IdTarjeta,
                    IdPersona = sel.qp.IdPersona,
                    IdCuenta = sel.qp.IdCuenta,
                    NumPlaca = sel.qp.NumPlaca,
                    IdEstado = sel.qp.IdEstado,
                    FecRevisionCostoAdm = sel.qp.FecRevisionCostoAdm,
                    IdFinanciado = sel.qp.IdFinanciado,
                    SaldoDispositivo = sel.qp.SaldoDispositivo,
                    SaldoPeaje = sel.qp.SaldoPeaje,
                    IdPrepago = sel.qp.IdPrepago,
                    MonCostoAdm = sel.qp.MonCostoAdm,
                    MonCargoInicial = sel.qp.MonCargoInicial,
                    MonMinRecarga = sel.qp.MonMinRecarga,
                    MonCostoUnidad = sel.qp.MonCostoUnidad,
                    IdMonedaAdm = sel.qp.IdMonedaAdm,
                    IdMonedaPeaje = sel.qp.IdMonedaPeaje,
                    IdCargoDiario = sel.qp.IdCargoDiario,
                    MonMinListaNegra = sel.qp.MonMinListaNegra,
                    MesesFinanciamiento = sel.qp.MesesFinanciamiento,
                    NumDocumento = sel.qp.NumDocumento,
                    IdEstadoOperativo = sel.qp.IdEstadoOperativo,
                    FecVencimiento = sel.qp.FecVencimiento,
                    DescEstado = sel.catEst.Descripcion == null ? "" : sel.catEst.Descripcion,
                    DescEstadoOperativo = sel.catEstOP.Descripcion == null ? "" : sel.catEstOP.Descripcion,
                    NombreCliente = sel.persona.NombreCompleto == null ? "" : sel.persona.NombreCompleto,
                    NumTarjeta = sel.tarjeta == null ? "" : sel.tarjeta.NumTarjeta,
                    DescTipoQuickpass = sel.catTipoQP.Descripcion == null ? "" : sel.catTipoQP.Descripcion,
                    NumCuenta = sel.cuenta.NumCuenta == null ? "" : sel.cuenta.NumCuenta,
                    CargoDiario = sel.catCargoDiario.Descripcion == null ? "" : sel.catCargoDiario.Descripcion,
                    Financiado = sel.catFinanciado.Descripcion == null ? "" : sel.catFinanciado.Descripcion,
                    DescPrepago = sel.catPrepago.Descripcion == null ? "" : sel.catPrepago.Descripcion,
                }).OrderBy(qp => qp.NumQuickpass).ToList();
            return resultado.ToDataResult(request);
        }

        public void EditarOp(PocQuickpass registro)
        {
            var tipoQuickpass = _repTipoQuickpass.Table.FirstOrDefault(x => x.IdTipoQuickpass == registro.IdTipoQuickpass);
            int cantMeses = tipoQuickpass.MesesVigencia == null ? 12 : (int)tipoQuickpass.MesesVigencia;

            TC_Tarjeta tc = null;
            registro.IdPrepago = tipoQuickpass.IdPrepago;
            registro.MonCargoInicial = tipoQuickpass.MonCargaInicial;
            registro.MonMinRecarga = tipoQuickpass.MonMinRecargar;
            registro.MonMinListaNegra = tipoQuickpass.MonMinListaNegra;
            registro.IdMonedaPeaje = tipoQuickpass.IdMonedaPeaje;
            registro.IdCargoDiario = tipoQuickpass.IdCargoDiario;
            registro.MonCostoAdm = tipoQuickpass.MonCostoAdm;
            registro.MonCostoUnidad = tipoQuickpass.MonCostoUnidad;
            registro.FecRevisionCostoAdm = DateTime.Now.AddMonths(cantMeses);
            registro.IdMonedaAdm = tipoQuickpass.IdMonedaAdm;
            if (registro.IdPrepago == (int)EnumIdSiNo.Si)
                registro.SaldoPeaje = registro.MonCargoInicial;
            else
                registro.SaldoPeaje = 0;


            if (!registro.IdCuenta.HasValue)
            {
                throw new CoreException("Datos faltantes", "Debe de seleccionar una cuenta");
            }

            if (registro.IdTarjeta.HasValue)
            {
                tc = _repTarjeta.Table.FirstOrDefault(x => x.IdTarjeta == registro.IdTarjeta);
                registro.NumTarjeta = tc.NumTarjeta;
            }
            else
            {
                var criteria = new Criteria<TC_Tarjeta>();
                tc = _repTarjeta.Table.FirstOrDefault(x =>
                    x.IdPersona == registro.IdPersona &&
                    x.IdEstadoTarjeta == (int)EnumEstadoTarjeta.Activa);

                if (tc == null)
                {
                    tc = _repTarjeta.Table.FirstOrDefault(x => x.IdPersona == registro.IdPersona);
                }

                if (tc.IdRelacion != (int)EnumRelacionTarjeta.Titular ||
                    tc.IdEstadoTarjeta != (int)EnumEstadoTarjeta.Activa)
                {
                    tc = _repTarjeta.Table.FirstOrDefault(x =>
                        x.IdCuenta == tc.IdCuenta &&
                        x.IdRelacion == (int)EnumRelacionTarjeta.Titular &&
                        x.IdEstadoTarjeta == (int)EnumEstadoTarjeta.Activa);
                }

                registro.NumTarjeta = tc.NumTarjeta;
            }

            if (registro.IdFinanciado != null)
            {
                registro.SaldoDispositivo = registro.MonCostoUnidad;
                if (registro.IdFinanciado == (int)EnumIdSiNo.Si)
                    registro.MesesFinanciamiento = tipoQuickpass.MesesFinanciamiento;
                else registro.MesesFinanciamiento = 1;
            }

            if (registro.IdEstado == (int)EnumEstadoQuickpass.Registrado ||
                registro.IdEstadoOperativo == null)
            {
                registro.IdEstado = (int)EnumEstadoQuickpass.Asignado;
                registro.IdEstadoOperativo = registro.IdEstadoOperativo == null ?
                    (int)EnumEstadOperativoQP.ListaBlanca : registro.IdEstadoOperativo;
            }

            if (registro.IdPrepago == (int)EnumIdSiNo.Si)
            {
                var tarjeta = _repTarjeta.Table.FirstOrDefault(x => x.IdTarjeta == registro.IdTarjeta);
                var monPeaje = _repMoneda.Table.FirstOrDefault(x => x.IdMoneda == registro.IdMonedaPeaje);
                double montoPagoQP = (double)registro.MonCargoInicial;
                var tipMov = _repCatalogo.SelectById((int)EnumTipoMovimientoTC.PeajeQuickpass).Referencia1;
                var res = GenerarCargo(tarjeta.NumTarjeta, DateTime.Now, Math.Round(montoPagoQP, 2), monPeaje.CodAlpha2, tipMov, null); //MISC
            }

            var entrada = registro.ProyectarComo<QP_Quickpass>();
            _repQuickpass.UpdateExclude(entrada,
                x => x.NumDocumento,
                x => x.IdUsuarioIncluyeAud,
                x => x.FecInclusionAud,
                x => x.FecVencimiento);
            _repQuickpass.UnidadTbjo.Save();
        }

        public void Modificar(PocQuickpass registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    if (registro.IdTipoQuickpass != null)
                    {
                        registro.IdEstado = (int)EnumEstadoQuickpass.Asignado;
                        registro.IdEstadoOperativo = (int)EnumEstadOperativoQP.ListaBlanca;

                        TC_Tarjeta tc = null;
                        var tipoQuickpass = _repTipoQuickpass.Table.FirstOrDefault(x => x.IdTipoQuickpass == registro.IdTipoQuickpass);
                        int cantMeses = tipoQuickpass.MesesVigencia == null ? 12 : (int)tipoQuickpass.MesesVigencia;

                        registro.IdPrepago = tipoQuickpass.IdPrepago;
                        registro.MonCargoInicial = tipoQuickpass.MonCargaInicial;
                        registro.MonMinRecarga = tipoQuickpass.MonMinRecargar;
                        registro.MonMinListaNegra = tipoQuickpass.MonMinListaNegra;
                        registro.IdMonedaPeaje = tipoQuickpass.IdMonedaPeaje;
                        registro.IdCargoDiario = tipoQuickpass.IdCargoDiario;
                        registro.MonCostoAdm = tipoQuickpass.MonCostoAdm;
                        registro.MonCostoUnidad = tipoQuickpass.MonCostoUnidad;
                        registro.FecRevisionCostoAdm = DateTime.Now.AddMonths(cantMeses);
                        registro.IdMonedaAdm = tipoQuickpass.IdMonedaAdm;

                        if (registro.IdPrepago == (int)EnumIdSiNo.Si)
                            registro.SaldoPeaje = registro.MonCargoInicial;
                        else
                            registro.SaldoPeaje = 0;

                        if (!string.IsNullOrEmpty(registro.NumTarjeta))
                        {
                            tc = _repTarjeta.Table.FirstOrDefault(x => x.IdTarjeta == registro.IdTarjeta);
                            registro.IdTarjeta = tc.IdTarjeta;
                            registro.IdCuenta = tc.IdCuenta;
                        }
                        else
                        {
                            var criteria = new Criteria<TC_Tarjeta>();
                            tc = _repTarjeta.Table.FirstOrDefault(x =>
                                x.IdPersona == registro.IdPersona &&
                                x.IdEstadoTarjeta == (int)EnumEstadoTarjeta.Activa);

                            if (tc == null)
                            {
                                tc = _repTarjeta.Table.FirstOrDefault(x => x.IdPersona == registro.IdPersona);
                            }

                            if (tc.IdRelacion != (int)EnumRelacionTarjeta.Titular ||
                                tc.IdEstadoTarjeta != (int)EnumEstadoTarjeta.Activa)
                            {
                                tc = _repTarjeta.Table.FirstOrDefault(x =>
                                    x.IdCuenta == tc.IdCuenta &&
                                    x.IdRelacion == (int)EnumRelacionTarjeta.Titular &&
                                    x.IdEstadoTarjeta == (int)EnumEstadoTarjeta.Activa);
                            }

                            registro.IdTarjeta = tc.IdTarjeta;
                            registro.IdCuenta = tc.IdCuenta;
                        }

                        //if (registro.IdFinanciado != null)
                        //{
                        //    //registro.SaldoDispositivo = registro.MonCostoUnidad;
                        //    if (registro.IdFinanciado == (int)EnumIdSiNo.Si)
                        //        registro.MesesFinanciamiento = tipoQuickpass.MesesFinanciamiento;
                        //    else registro.MesesFinanciamiento = 1;
                        //}

                        //var monAdm = _repMoneda.Table.FirstOrDefault(x => x.IdMoneda == registro.IdMonedaAdm);

                        //if (registro.MesesFinanciamiento == null || registro.MesesFinanciamiento == 0)
                        //{
                        //    registro.MesesFinanciamiento = 1;
                        //}
                        //double montoPagoQP = (double)registro.MonCostoUnidad / (double)registro.MesesFinanciamiento;
                        //montoPagoQP = Math.Round(montoPagoQP, 2);

                        //var tipMov = _repCatalogo.SelectById((int)EnumTipoMovimientoTC.AdquisicionQuickpass).Referencia1;
                        //var res = GenerarCargo(tc.NumTarjeta, DateTime.Now, montoPagoQP, monAdm.CodAlpha2, tipMov, null); //MISC

                        //if (res == "00")
                        //{
                        //    registro.SaldoDispositivo = registro.SaldoDispositivo ?? 0;
                        //    registro.SaldoDispositivo = decimal.Round((decimal)registro.SaldoDispositivo, 2,
                        //        MidpointRounding.AwayFromZero) - decimal.Round((decimal)montoPagoQP, 2, MidpointRounding.AwayFromZero);
                        //}

                        //if (registro.IdPrepago == (int)EnumIdSiNo.Si)
                        //{
                        //    var monPeaje = _repMoneda.Table.FirstOrDefault(x => x.IdMoneda == registro.IdMonedaPeaje);
                        //    montoPagoQP = (double)registro.MonCargoInicial;
                        //    tipMov = _repCatalogo.SelectById((int)EnumTipoMovimientoTC.PeajeQuickpass).Referencia1;
                        //    res = GenerarCargo(tc.NumTarjeta, DateTime.Now, Math.Round(montoPagoQP, 2), monPeaje.CodAlpha2, tipMov, null); //MISC
                        //}
                    }

                    var entrada = registro.ProyectarComo<QP_Quickpass>();
                    _repQuickpass.UpdateExclude(entrada,
                                                    x => x.NumDocumento,
                                                    x => x.FecVencimiento,
                                                    x => x.IdUsuarioIncluyeAud,
                                                    x => x.FecInclusionAud);
                    InsertarBitacoraQP(entrada, null);
                    _repQuickpass.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocQuickpass));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public string GenerarCargo(string numTarjeta, DateTime fechaConsumo, double montoTransaccion,
            string monedaTransaccion, string codMovimiento, string numReferencia)
        {
            var res = "00";
            if (montoTransaccion <= 0 || string.IsNullOrEmpty(numTarjeta))
            {
                return res;
            }
            PocEntradaMiscelaneosTarjeta cargaQPTarjeta = new PocEntradaMiscelaneosTarjeta();
            cargaQPTarjeta.NumTarjeta = numTarjeta;
            cargaQPTarjeta.FechaConsumo = fechaConsumo;
            cargaQPTarjeta.MontoTransaccion = (decimal)montoTransaccion;
            cargaQPTarjeta.MonedaTransaccion = monedaTransaccion;
            cargaQPTarjeta.CodMovimiento = codMovimiento; //Movimiento Real
            var numRef = numReferencia ?? _fGenerales.ObtieneSgteSecuencia("SEQ_IdRefProcesador").ToString();
            cargaQPTarjeta.NumReferencia = numRef;

            var resultado = _appOperacionesATH.AplicaMiscelaneosTarjeta(cargaQPTarjeta);
            res = resultado.CodigoRespuesta;

            return res;
        }

        private ValidationResponse Validar(PocQuickpass registro, bool inserta)
        {
            var validacion = new ValidationResponse();

            if (registro.NumQuickpass == "") { validacion.AddError("Err_0035"); }

            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }

        public void ModificarEstadoQP(List<PocQuickpass> lista, Boolean validaEstado)
        {
            foreach (var item in lista)
            {
                if (validaEstado)
                {
                    var qp = _repQuickpass.SelectById(item.IdQuickpass);
                    if (qp.IdPersona.HasValue && item.IdEstado == (int)EnumEstadoQuickpass.Registrado)
                    {
                        var validation = new ValidationResponse();
                        validation.AddError("Msj_QpEstadoNoValido");
                        throw new CoreException("Error al cambiar de estado", validation: validation);
                    }

                    switch (item.IdEstado)
                    {
                        case ((int)EnumEstadoQuickpass.Registrado):
                            item.IdEstadoOperativo = null;
                            break;
                        case ((int)EnumEstadoQuickpass.Activo):
                        case ((int)EnumEstadoQuickpass.Asignado):
                            item.IdEstadoOperativo = (int)EnumEstadOperativoQP.ListaBlanca;
                            break;
                        case ((int)EnumEstadoQuickpass.Vencido):
                        case ((int)EnumEstadoQuickpass.Defectuoso):
                        case ((int)EnumEstadoQuickpass.Devuelto):
                        case ((int)EnumEstadoQuickpass.Perdido):
                            item.IdEstadoOperativo = (int)EnumEstadOperativoQP.ListaNegra;
                            break;
                    }
                }

                var entrada = item.ProyectarComo<QP_Quickpass>();
                _repQuickpass.Update(entrada,
                    x => x.IdEstado,
                    x => x.IdEstadoOperativo);
                InsertarBitacoraQP(entrada, item.Detalle);
            }
            _repQuickpass.UnidadTbjo.Save();
        }

        private void InsertarBitacoraQP(QP_Quickpass entrada, String detalle)
        {
            var bitacora = new QP_Bitacora
            {
                IdQuickpass = entrada.IdQuickpass,
                IdEstado = entrada.IdEstado,
                IdEstadoOperativo = entrada.IdEstadoOperativo,
                Detalle = detalle
            };
            _repBitacora.Insert(bitacora);
        }

        public void ActualizaEstadosOPxTarjeta()
        {
            var criteria = new Criteria<QP_Quickpass>();
            var lista = _repQuickpass.Table
                .Join(_repCuenta.Table,
                    qp => qp.IdCuenta,
                    c => c.IdCuenta,
                    (qp, c) => new { qp, c })
                .Join(_repCatalogo.List("LESTADOCUENTA"),
                    tmp => tmp.c.IdEstadoCta,
                    catEstTarj => catEstTarj.IdCatalogo,
                    (tmp, catEstTarj) => new { tmp.qp, tmp.c, catEstTarj })
                .Where(sel => sel.qp.IdEstado == (int)EnumEstadoQuickpass.Asignado)
                .OrderBy(sel => sel.qp.NumQuickpass).ToList()
                .Select(sel => new PocQuickpass
                {
                    IdQuickpass = sel.qp.IdQuickpass,
                    IdEstado = sel.qp.IdEstado,
                    IdEstadoOperativo = sel.catEstTarj.Referencia3 != null ?
                        Int32.Parse(sel.catEstTarj.Referencia3) : sel.qp.IdEstadoOperativo,
                    Detalle = String.Format("Proceso automático tarjeta en estado {0}",
                        sel.catEstTarj.Descripcion)
                }).ToList();

            var listaEstActuales = _repQuickpass.Table
                .Join(_repCuenta.Table,
                    qp => qp.IdCuenta,
                    c => c.IdCuenta,
                    (qp, c) => new { qp, c })
                .Where(sel=> sel.qp.IdEstado == (int)EnumEstadoQuickpass.Asignado)
                .OrderBy(sel => sel.qp.NumQuickpass)
                .Select(sel => new PocQuickpass
                {
                    IdQuickpass = sel.qp.IdQuickpass,
                    IdEstado = sel.qp.IdEstado,
                    IdEstadoOperativo = sel.qp.IdEstadoOperativo
                }).ToList();

            /* Valida que los estados hayan cambiado */
            if (lista.Count == listaEstActuales.Count)
            {
                for (int i = lista.Count - 1; i > -1; i--)
                {
                    if (lista[i].IdEstadoOperativo == listaEstActuales[i].IdEstadoOperativo)
                    {
                        lista.Remove(lista[i]);
                    }
                }
            }
            if (lista.Count > 0)
            {
                ModificarEstadoQP(lista, false);
            }
        }
    }
}

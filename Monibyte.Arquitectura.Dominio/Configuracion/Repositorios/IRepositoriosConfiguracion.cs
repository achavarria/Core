﻿
namespace Monibyte.Arquitectura.Dominio.Configuracion.Repositorios
{    
    public interface IRepositorioPlantilla : IRepositorioMnb<MB_Plantilla>
    {
        bool ExistePlantilla(string nombre, int idEmpresa);
    }

    public interface IRepositorioTipoComercio : IRepositorioMnb<MB_TipoComercio>
    {
        MB_TipoComercio ObtenerPorId(int IdMcc);
    }    
}


﻿using Monibyte.Arquitectura.Aplicacion.Notificacion;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Poco.Notificacion;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Notificacion
{
    public class ServiciosNotificacion : ServicioUnity, IServiciosNotificacion
    {
        public IEnumerable<PocConfigNotificacion> ObtenerNotificaciones(int? idEmpresa, int? idContexto, int? idEsActivo)
        {
            using (var app = GestorUnity.CargarUnidad<IAppNotificacion>())
        {
                return app.ObtenerNotificaciones(idContexto, idEmpresa, idEsActivo);
            }
        }

        public IEnumerable<PocConfigNotificacion> EstablecerNotificacion(List<PocConfigNotificacion> notificaciones)
        {
            using (var app = GestorUnity.CargarUnidad<IAppNotificacion>())
            {
                return app.EstablecerNotificacion(notificaciones);
            }
        }

        public IEnumerable<PocDestinatarioDefault> ObtenerDestinatariosDefault(int idEmpresa)
        {
            using (var app = GestorUnity.CargarUnidad<IAppNotificacion>())
        {
                return app.ObtenerDestinatarios(idEmpresa);
            }
        }

        public void ProcesarNotificaciones(PocoQueue pocoQueue)
        {
            using (var app = GestorUnity.CargarUnidad<IAppNotificacion>())
        {
                app.ProcesarNotificaciones(pocoQueue);
            }
        }
    }
}

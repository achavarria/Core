using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Notificacion;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Clientes
{
    public partial class CL_Persona : EntidadBase
    {
        public int IdPersona { get; set; }
        public string NumIdentificacion { get; set; }
        public System.DateTime FecVenceIdentificacion { get; set; }
        public int IdTipoPersona { get; set; }
        public int IdIdentificacion { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string NombreCompleto { get; set; }
        public string RazonSocial { get; set; }
        public Nullable<System.DateTime> FecNacimiento { get; set; }
        public Nullable<int> IdGenero { get; set; }
        public Nullable<int> IdEstadoCivil { get; set; }
        public int IdPaisNacimiento { get; set; }
        public System.DateTime FecIngreso { get; set; }
        public Nullable<int> IdTipoRelacion { get; set; }
        public Nullable<int> IdEjecutivo { get; set; }
        public Nullable<int> IdProfesion { get; set; }
        public int IdFallecido { get; set; }
        public Nullable<System.DateTime> FecFallecido { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_UsuarioMap : EntityTypeConfiguration<GL_Usuario>
    {
        public GL_UsuarioMap()
        {
            // Primary Key
            this.HasKey(t => t.IdUsuario);

            // Properties
            this.Property(t => t.IdUsuario)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodUsuario)
                .HasMaxLength(20);

            this.Property(t => t.AliasUsuario)
                .HasMaxLength(100);

            this.Property(t => t.CodUsuarioEmpresa)
                .HasMaxLength(20);

            this.Property(t => t.Correo)
                .IsRequired()
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("GL_Usuario", "General");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.AliasUsuario).HasColumnName("AliasUsuario");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdCompania).HasColumnName("IdCompania");
            this.Property(t => t.IdUnidad).HasColumnName("IdUnidad");
            this.Property(t => t.CodUsuarioEmpresa).HasColumnName("CodUsuarioEmpresa");
            this.Property(t => t.Correo).HasColumnName("Correo");
            this.Property(t => t.IdIdiomaPreferencia).HasColumnName("IdIdiomaPreferencia");
        }
    }
}

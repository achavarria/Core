﻿using System.Collections.Generic;
using Monibyte.Arquitectura.Aplicacion.Configuracion;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Poco.Configuracion;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Servicios.Nucleo;
using Monibyte.Arquitectura.Poco.Tarjetas;

namespace Monibyte.Arquitectura.Servicios.Web.Configuracion
{
    public class ServicioConfiguracion : ServicioUnity, IServicioConfiguracion
    {
        private IAppConfiguracionGrps _appControlGrupos;
        private IAppConfiguracionApl _appConfiguracionApl;
        private IAppConfiguracionCtrl _appConfiguracionCtrl;
        private IAppConfiguracionMisc _appConfiguracionMisc;
        private IAppReportes _repReportes;
        private IAppConfiguracionLicPer _appConfiguracionLicPer;
        private IAppConfiguracionTipoLic _appConfiguracionTipoLic;
        private IAppPrivilegios _appPrivilegios;

        public ServicioConfiguracion(
            IAppConfiguracionGrps appControlGrupos,
            IAppConfiguracionApl appConfiguracionApl,
            IAppConfiguracionCtrl appConfiguracionCtrl,
            IAppConfiguracionMisc appConfiguracionMisc,
            IAppReportes repReportes,
            IAppConfiguracionLicPer appConfiguracionLicPer,
            IAppConfiguracionTipoLic appConfiguracionTipoLic,
            IAppPrivilegios appPrivilegios)
        {
            _appControlGrupos = appControlGrupos;
            _appConfiguracionApl = appConfiguracionApl;
            _appConfiguracionCtrl = appConfiguracionCtrl;
            _appConfiguracionMisc = appConfiguracionMisc;
            _repReportes = repReportes;
            _appConfiguracionLicPer = appConfiguracionLicPer;
            _appConfiguracionTipoLic = appConfiguracionTipoLic;
            _appPrivilegios = appPrivilegios;
        }

        #region control de grupos

        public Transporte ObtenerGrupoEmpresa(Transporte transporte)
        {
            var idGrupoEmpresa = transporte.GetValor<int>("idGrupoEmpresa");
            var resultado = _appControlGrupos.ObtenerGrupoEmpresa(idGrupoEmpresa);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerGruposEmpresa(Transporte transporte)
        {
            var idEmpresa = (int)InfoSesion.Info.IdEmpresa;
            var idTipoUsuario = transporte.GetValor<int>("idTipoUsuario");
            var resultado = _appControlGrupos.ObtenerGruposEmpresa(idEmpresa, idTipoUsuario);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte RegistrarConfiguracionGrupo(Transporte transporte)
        {
            var datosUsuario = transporte.GetDatos<PocGruposEmpresa>();
            var pocoPrevio = transporte.GetValor<PocCondicion>("CONDICIONPREVIA");
            InfoSesion.IncluirSesion("CONDICIONPREVIA", pocoPrevio);
            var resultado = _appControlGrupos.RegistrarConfiguracionGrupo(datosUsuario);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte DuplicarConfiguracionGrupo(Transporte transporte)
        {
            var datosPlantilla = transporte.GetDatos<PocGrupoDuplica>();
            _appControlGrupos.DuplicarConfiguracionGrupo(datosPlantilla);
            return transporte;
        }

        public Transporte EliminarConfiguracionGrupo(Transporte transporte)
        {
            var idGrupo = transporte.GetValor<int>("idGrupo");
            var idTipoGrupo = transporte.GetValor<int>("idTipoGrupo");
            _appControlGrupos.EliminarConfiguracionGrupo(idGrupo, idTipoGrupo);
            return transporte;
        }

        #region definir administradores de grupo

        //public Transporte ObtenerUsuariosNoAdm(Transporte transporte)
        //{
        //    var idEmpresa = (int)InfoSesion.Info.IdEmpresa;
        //    var idGrupoEmpresa = transporte.GetValor<int>("IDGRUPOEMPRESA");
        //    var resultado = _appControlGrupos.ObtenerUsuariosNoAdm(idEmpresa, idGrupoEmpresa);
        //    transporte.SetDatos(resultado);
        //    return transporte;
        //}
        public Transporte ObtenerUsuariosGrupo(Transporte transporte)
        {
            var idEmpresa = (int)InfoSesion.Info.IdEmpresa;
            var idGrupoEmpresa = transporte.GetValor<int>("IDGRUPOEMPRESA");
            var resultado = _appControlGrupos.ObtenerUsuariosGrupo(idEmpresa, idGrupoEmpresa);
            transporte.SetDatos(resultado);
            return transporte;
        }
        public Transporte DefinirAdministradores(Transporte transporte)
        {
            var entrada = transporte.GetDatos<List<PocUsuarioEmpresa>>();
            var idGrupo = transporte.GetValor<int>("IDGRUPOEMPRESA");
            var idTipoGrupo = transporte.GetValor<int>("idTipoGrupo");
            _appControlGrupos.DefinirAdministradores(idGrupo, idTipoGrupo, entrada);
            return transporte;
        }

        #endregion

        #region definir agrupacion de tarjetas

        //public Transporte ObtenerTarjetasNoAgrupadas(Transporte transporte)
        //{
        //    var idGrupoEmpresa = transporte.GetValor<int>("IDGRUPOEMPRESA");
        //    var resultado = _appControlGrupos.ObtenerTarjetasNoAgrupadas(idGrupoEmpresa);
        //    transporte.SetDatos(resultado);
        //    return transporte;
        //}

        //public Transporte ObtenerTarjetasSiAgrupadas(Transporte transporte)
        //{
        //    var idEmpresa = (int)InfoSesion.Info.IdEmpresa;
        //    var resultado = _appControlGrupos.ObtenerTarjetasSiAgrupadas(idEmpresa);
        //    transporte.SetDatos(resultado);
        //    return transporte;
        //}

        public Transporte ObtenerTarjetasPorGrupo(Transporte transporte)
        {
            var idGrupoEmpresa = transporte.GetValor<int?>("IDGRUPOEMPRESA");
            var soloAgrupadas = transporte.GetValor<bool>("SOLOAGRUPADAS");
            var idTipoGrupo = transporte.GetValor<int>("idTipoGrupo");
            var resultado = _appControlGrupos.ObtenerTarjetasPorGrupo(idGrupoEmpresa, idTipoGrupo, soloAgrupadas);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte AgruparTarjetas(Transporte transporte)
        {
            var resultado = transporte.GetDatos<List<PocTarjetaEmpresa>>();
            var idGrupoEmpresa = transporte.GetValor<int>("IDGRUPOEMPRESA");
            var idTipoGrupo = transporte.GetValor<int>("idTipoGrupo");
            var datosSalida = _appControlGrupos.AgruparTarjetas(resultado, idGrupoEmpresa, idTipoGrupo);
            transporte.SetDatos(datosSalida);
            return transporte;
        }

        public Transporte DesagruparTarjeta(Transporte transporte)
        {
            var resultado = transporte.GetDatos<PocAgrupacionTarjetasItem>();
            _appControlGrupos.DesagruparTarjeta(resultado);
            return transporte;
        }

        #endregion

        #endregion

        #region control configuracion apl

        public Transporte ObtenerPlantilla(Transporte transporte)
        {
            var idGrupo = transporte.GetValor<int>("idGrupo");
            var resultado = _appConfiguracionApl.ObtenerCondicion(idGrupo);
            transporte.SetDatos(resultado);
            return transporte;
        }
        public Transporte ObtenerPlantillasEmpresa(Transporte transporte)
        {
            var idEmpresa = (int)InfoSesion.Info.IdEmpresa;
            var resultado = _appConfiguracionApl.ObtenerPlantillasEmpresa(idEmpresa);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerPlantillasDisponibles(Transporte transporte)
        {
            var idEmpresa = (int)InfoSesion.Info.IdEmpresa;
            var resultado = _appConfiguracionApl.ObtenerPlantillasDisponibles(idEmpresa);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EstablecerPlantilla(Transporte transporte)
        {
            var resultado = transporte.GetDatos<PocCondicion>();
            _appConfiguracionApl.EstablecerCondicion(resultado);
            return transporte;
        }

        public Transporte EliminarPlantilla(Transporte transporte)
        {
            var idPlantilla = transporte.GetDatos<int>();
            _appConfiguracionApl.EliminarPlantilla(idPlantilla);
            return transporte;
        }

        public Transporte CambioEstadoPlantilla(Transporte transporte)
        {
            var idPlantilla = transporte.GetValor<int>("idPlantilla");
            var idEstado = transporte.GetValor<int>("idEstado");
            _appConfiguracionApl.CambioEstadoPlantilla(idPlantilla, idEstado);
            return transporte;
        }

        #endregion

        #region control configuracion ctrl

        public Transporte ObtenerGruposEmpresaCatalogo(Transporte transporte)
        {
            var data = transporte.GetDatos<PocConsultaGrupoEmpresa>();
            var resultado = _appConfiguracionCtrl.ObtenerGruposEmpresaCatalogo(data, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerTarjetasGrupoCatalogo(Transporte transporte)
        {
            var data = transporte.GetDatos<PocConsultaGrupoEmpresa>();
            var resultado = _appConfiguracionCtrl.ObtenerTarjetasGrupoCatalogo(data, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerCondicionTarjeta(Transporte transporte)
        {
            var numTarjeta = transporte.GetDatos<string>();
            var resultado = _appConfiguracionCtrl.ObtenerCondicion(numTarjeta);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EstablecerCondicionTarjeta(Transporte transporte)
        {
            var poco = transporte.GetDatos<PocCondicion>();
            var pocoPrevio = transporte.GetValor<PocCondicion>("CONDICIONPREVIA");
            InfoSesion.IncluirSesion("CONDICIONPREVIA", pocoPrevio);
            _appConfiguracionCtrl.EstablecerCondicion(poco);
            return transporte;
        }

        #endregion

        #region control configuracion misc


        public Transporte ObtenerRegiones(Transporte transporte)
        {
            var resultado = _appConfiguracionMisc.ObtenerRegiones();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerPaisesRegion(Transporte transporte)
        {
            var idRegion = transporte.GetDatos<int>();
            var resultado = _appConfiguracionMisc.ObtenerPaisesRegion(idRegion);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerListaCategoriaComercio(Transporte transporte)
        {
            var resultado = _appConfiguracionMisc.ObtenerListaCategoriaComercio();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerListaComercio(Transporte transporte)
        {
            var idCategoria = transporte.GetDatos<int>();
            var resultado = _appConfiguracionMisc.ObtenerListaComercio(idCategoria);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerBitacoraPanelCrtl(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocFiltroBitacoraPanelCtrl>();
            var resultado = _appConfiguracionMisc.ObtenerBitacoraPanelCrtl(
                entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerUsuariosPorGrupo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocUsuariosGrupos>();
            var resultado = _appConfiguracionMisc.ObtenerUsuariosPorGrupo(entrada,
                transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        #endregion

        #region Reportes

        public Transporte GenerarReporteGrupos(Transporte transporte)
        {
            var idGrupo = transporte.GetValor<int>("idGrupo");
            var idEntidad = InfoSesion.ObtenerSesion<int>("IDPROCESADOR");
            var resultado = _repReportes.GenerarReporteGrupos(InfoSesion.Info.IdEmpresa, idGrupo, idEntidad, InfoSesion.Info.CodUsuario);
            transporte.SetDatos(resultado);
            return transporte;
        }

        #endregion

        #region control configuracion licenciapersona

        public Transporte IncluirLicenciaPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocLicenciaPersona>();
            _appConfiguracionLicPer.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarLicenciaPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocLicenciaPersona>();
            var resultado = _appConfiguracionLicPer.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaLicenciaPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocLicenciaPersona>();
            _appConfiguracionLicPer.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaLicenciaPersona(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocLicenciaPersona>();
            _appConfiguracionLicPer.Modificar(entrada);
            return transporte;
        }

        #endregion

        #region control configuracion tipolicencia
        public Transporte IncluirTipoLicencia(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoLicencia>();
            _appConfiguracionTipoLic.Insertar(entrada);
            return transporte;
        }

        public Transporte ListarTipoLicencia(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoLicencia>();
            var resultado = _appConfiguracionTipoLic.Listar(entrada, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminaTipoLicencia(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoLicencia>();
            _appConfiguracionTipoLic.Eliminar(entrada);
            return transporte;
        }

        public Transporte ModificaTipoLicencia(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocTipoLicencia>();
            _appConfiguracionTipoLic.Modificar(entrada);
            return transporte;
        }

        public Transporte ListadoTipoLicencia(Transporte transporte)
        {
            var resultado = _appConfiguracionTipoLic.ListadoTipoLicencia();
            transporte.SetDatos(resultado);
            return transporte;
        }
        #endregion

        public Transporte PermitirConsumoInternet(Transporte transporte)
        {
            var idCondicion = transporte.GetValor<int>("idCondicion");
            var consumeInternet = transporte.GetValor<bool>("consumeInternet");
            _appConfiguracionCtrl.PermitirConsumoInternet(idCondicion, consumeInternet);
            return transporte;
        }

        #region Grupos de Liquidación

        public Transporte GuardarGrupoOperativo(Transporte transporte)
        {
            var entrada = transporte.GetDatos<PocGruposEmpresa>();
            _appControlGrupos.GuardarGrupoOperativo(entrada);
            return transporte;
        }

        public Transporte AutorizacionGrupoPendiente(Transporte transporte)
        {
            var idGrupo = transporte.GetValor<int>("idGrupo");
            var resultado = _appControlGrupos.AutorizacionGrupoPendiente(idGrupo);
            transporte.SetDatos(resultado);
            return transporte;
        }

        #endregion


        #region Privilegios

        public IEnumerable<PocPrivilegiosCuenta> ObtenerPrivilegiosCuenta()
        {
            var result = _appPrivilegios.ObtenerPrivilegiosCuenta();
            return result;
        }
        public void GuardarAgrupacionesCuenta(List<PocAdmAgrupacion> adms,
            int[] admPrevios, int idCuenta)
        {
            _appPrivilegios.GuardarAgrupacionesCuenta(adms, admPrevios, idCuenta);
        }
        public void EliminarPrivilegio(PocPrivilegiosCuenta datosEntrada)
        {
            _appPrivilegios.EliminarPrivilegio(datosEntrada);
        }
        public void ValidarCuentasGrupo(int idGrupo)
        {
            _appPrivilegios.ValidarCuentasGrupo(idGrupo);
        }
        #endregion


        public void ResetCategorias(List<PocMccCondicion> condiciones, int idMoneda)
        {
            _appConfiguracionCtrl.ResetCategorias(condiciones, idMoneda);
        }

        public bool EsAutorizadorGrupo(int idUsuario, int idGrupo)
        {
            return _appControlGrupos.EsAutorizadorGrupo(idUsuario, idGrupo);
        }
    }
}

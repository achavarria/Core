using Monibyte.Arquitectura.Dominio.Gestiones;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_UnidadNegocio : EntidadBase
    {
        public int IdUnidad { get; set; }
        public int IdEntidad { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int IdPais { get; set; }
        public int IdEsPrincipal { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

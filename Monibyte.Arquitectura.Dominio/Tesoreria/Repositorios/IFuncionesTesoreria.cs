﻿using System.Collections.Generic;
using Monibyte.Arquitectura.Dominio.Nucleo;
using System;

namespace Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios
{
    public interface IFuncionesTesoreria : IRepositorioBase
    {
        decimal ConvierteMonto(int? idMonedaOrigen, decimal? monto, int? idMonedaDestino, 
            decimal? cambioOrigen=null, decimal? cambioDestino=null, int? idEntidadFinanciera=null);
        decimal ConvierteMontoHistorico(ParamsConvierteMontoHistoricoTS parameters);
        decimal ObtieneTipoCambio(ParamsObtieneTipoCambioTS parameters);
    }
}

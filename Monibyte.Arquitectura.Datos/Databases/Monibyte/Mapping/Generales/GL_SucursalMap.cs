using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_SucursalMap : EntityTypeConfiguration<GL_Sucursal>
    {
        public GL_SucursalMap()
        {
            // Primary Key
            this.HasKey(t => t.IdSucursal);

            // Properties
            this.Property(t => t.IdSucursal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Codigo)
                .HasMaxLength(50);

            this.Property(t => t.Descripcion)
                .HasMaxLength(100);

            this.Property(t => t.Direccion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Telefono)
                .HasMaxLength(20);

            this.Property(t => t.Correo)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.EncargadoOficina)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Horario)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GL_Sucursal", "General");
            this.Property(t => t.IdSucursal).HasColumnName("IdSucursal");
            this.Property(t => t.IdEntidad).HasColumnName("IdEntidad");
            this.Property(t => t.Codigo).HasColumnName("Codigo");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.Direccion).HasColumnName("Direccion");
            this.Property(t => t.Telefono).HasColumnName("Telefono");
            this.Property(t => t.Correo).HasColumnName("Correo");
            this.Property(t => t.EncargadoOficina).HasColumnName("EncargadoOficina");
            this.Property(t => t.Horario).HasColumnName("Horario");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

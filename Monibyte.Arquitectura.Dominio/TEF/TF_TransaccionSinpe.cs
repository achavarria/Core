﻿using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.TEF
{
    public partial class TF_TransaccionSinpe : EntidadBase
    {
        public int IdTransaccion { get; set; }
        public int IdOrigenInterno { get; set; }
        public Nullable<int> CodServicio { get; set; }
        public string CodEntidadOrigen { get; set; }
        public string IdentificacionOrigen { get; set; }
        public string CuentaClienteOrigen { get; set; }
        public Nullable<int> IdMonedaOrigen { get; set; }
        public string CodEntidadDestino { get; set; }
        public string IdentificacionDestino { get; set; }
        public string CuentaClienteDestino { get; set; }
        public int IdMonedaDestino { get; set; }
        public decimal Comision { get; set; }
        public decimal MontoPago { get; set; }
        public decimal MontoTrasaccion { get; set; }
        public string Detalle { get; set; }
        public string CodReferenciaSinpe { get; set; }
        public Nullable<int> CodRefCoreBancario { get; set; }
        public string UsuarioRegistra { get; set; }
        public System.DateTime FecValor { get; set; }
        public Nullable<System.DateTime> FecLiquidacion { get; set; }
        public int IdEstadoTrans { get; set; }
        public int EstadoSINPE { get; set; }
        public string DetalleError { get; set; }
        public Nullable<int> CodMotivoRechazo { get; set; }
        public string DescripcionRechazo { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<decimal> MontoNoDistribuido { get; set; }
        public Nullable<int> IdEsDistribuible { get; set; }
    }
}

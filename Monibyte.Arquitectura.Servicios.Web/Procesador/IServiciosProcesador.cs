﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;

namespace Monibyte.Arquitectura.Servicios.Web.Procesador
{
    [ServiceContract]
    public interface IServiciosProcesador : IServicioUnity
    {
        
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObetenerDenegaciones")]
        Transporte ObetenerDenegaciones(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EnviarReporteEmpresa")]
        Transporte EnviarReporteEmpresa(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtieneReporteDenegacion")]
        Transporte ObtieneReporteDenegacion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtieneDetalleDenegacion")]
        string ObtieneDetalleDenegacion(string denailDetail);
    }
}

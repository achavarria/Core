﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Contabilidad;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Contabilidad;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Contabilidad
{
    public class AppConfAsientoContabilidad : AplicacionBase, IAppConfAsientoContabilidad
    {
        private IRepositorioMoneda _repMoneda;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CG_Parametro> _repParametro;
        private IRepositorioMnb<CG_ConfiguracionAsiento> _repConfAsiento;

        public AppConfAsientoContabilidad(
            IRepositorioMoneda repMoneda,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CG_Parametro> repParametro,
            IRepositorioMnb<CG_ConfiguracionAsiento> repConfAsiento)
        {
            _repMoneda = repMoneda;
            _repCatalogo = repCatalogo;
            _repParametro = repParametro;
            _repConfAsiento = repConfAsiento;
        }

        public void Eliminar(PocConfiguracionAsiento registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CG_ConfiguracionAsiento>();
                _repConfAsiento.Delete(entrada);
                _repConfAsiento.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocConfiguracionAsiento registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CG_ConfiguracionAsiento>();
                    _repConfAsiento.Insert(entrada);
                    _repConfAsiento.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocConfiguracionAsiento));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocConfiguracionAsiento registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CG_ConfiguracionAsiento>();
                    _repConfAsiento.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repConfAsiento.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocConfiguracionAsiento));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocConfiguracionAsiento> Listar(PocConfiguracionAsiento filtro, DataRequest request)
        {
            var criteria = new Criteria<CG_ConfiguracionAsiento>();
            var resultado =
               from x in _repConfAsiento.SelectBy(criteria)
               join mov in _repCatalogo.List("LTIPOMOVIMIENTOTC")
                 on x.IdTipoMovimiento equals mov.IdCatalogo
               join orig in _repCatalogo.List("LORIGENMOVIMIENTOTC")
                 on x.IdOrigenMovimiento equals orig.IdCatalogo
               join cust in _repParametro.Table
                 on x.IdCustomer equals cust.IdParametro
               join acco in _repParametro.Table
                 on x.IdAccount equals acco.IdParametro
               join item in _repParametro.Table
                 on x.IdItem equals item.IdParametro
               join mone in _repMoneda.Table
                 on x.IdMoneda equals mone.IdMoneda
               select new PocConfiguracionAsiento
               {
                   IdConfAsiento = x.IdConfAsiento,
                   IdTipoMovimiento = x.IdTipoMovimiento,
                   IdOrigenMovimiento = x.IdOrigenMovimiento,
                   IdCustomer = x.IdCustomer,
                   IdAccount = x.IdAccount,
                   IdItem = x.IdItem,
                   IdMoneda = x.IdMoneda,
                   Orden = x.Orden,
                   TipoMovimiento = mov == null ? "" : mov.Descripcion,
                   OrigenMovimiento = orig == null ? "" : orig.Descripcion,
                   Customer = cust == null ? "" : cust.Descripcion,
                   Account = acco == null ? "" : acco.Descripcion,
                   Item = item == null ? "" : item.Descripcion,
                   Moneda = mone == null ? "" : mone.Descripcion,
                   DescMemo = x.DescMemo

               };
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocConfiguracionAsiento registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.IdOrigenMovimiento < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdTipoMovimiento < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdAccount < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdCustomer < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdItem < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdMoneda < 0) { validacion.AddError("Err_0035"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }
    }
}

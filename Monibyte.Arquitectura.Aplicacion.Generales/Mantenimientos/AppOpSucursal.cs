﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Generales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Generales.Mantenimientos
{
    public class AppOpSucursal : AplicacionBase, IAppOpSucursal
    {
        private IRepositorioMnb<GL_Entidad> _repEntidad;
        private IRepositorioMnb<GL_Sucursal> _repSucursal;

        public AppOpSucursal(
            IRepositorioMnb<GL_Entidad> repEntidad,
            IRepositorioMnb<GL_Sucursal> repSucursal)
        {
            _repEntidad = repEntidad;
            _repSucursal = repSucursal;
        }

        public void Eliminar(PocSucursal registro)
        {
            using (var ts = new TransactionScope())
            {
                _repSucursal.DeleteOn(p => p.IdSucursal == registro.IdSucursal);
                _repSucursal.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocSucursal registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<GL_Sucursal>();
                    entrada.IdSucursal = _fGenerales.ObtieneSgteSecuencia("SEQ_IdSucursal");
                    entrada.IdEntidad = registro.IdEntidad;
                    entrada.Codigo = registro.Codigo;
                    entrada.Descripcion = registro.Descripcion;
                    entrada.Direccion = registro.Direccion;
                    entrada.Telefono = registro.Telefono;
                    entrada.Correo = registro.Correo;
                    entrada.EncargadoOficina = registro.EncargadoOficina;
                    entrada.Horario = registro.Horario;

                    _repSucursal.Insert(entrada);
                    _repSucursal.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocSucursal));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocSucursal registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<GL_Sucursal>();
                    _repSucursal.UpdateExclude(entrada,
                        ex => ex.IdUsuarioIncluyeAud,
                        ex => ex.FecInclusionAud);
                    _repSucursal.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocSucursal));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocSucursal> Listar(PocSucursal filtro, DataRequest request)
        {
            var critSucursal = new Criteria<GL_Sucursal>();
            var tipoU = Context.UserType;

            if (!tipoU.MultiEntidad)
            {
                var compania = InfoSesion.Info.IdCompania;
                critSucursal.And(m => m.IdEntidad == compania);
                if (tipoU.PorUnidad)
                {
                    var sucursal = InfoSesion.Info.IdUnidad;
                    critSucursal.And(m => m.IdSucursal == sucursal);
                }
            }
            if (filtro != null)
            {
                if (filtro.IdEntidad > 0)
                {
                    critSucursal.And(m => m.IdEntidad == filtro.IdEntidad);
                }
                if (!string.IsNullOrEmpty(filtro.Descripcion))
                {
                    critSucursal.And(x => x.Descripcion.Contains(filtro.Descripcion));
                }
                if (!string.IsNullOrEmpty(filtro.Direccion))
                {
                    critSucursal.And(x => x.Direccion.Contains(filtro.Direccion));
                }
                if (!string.IsNullOrEmpty(filtro.EncargadoOficina))
                {
                    critSucursal.And(x => x.EncargadoOficina.Contains(filtro.EncargadoOficina));
                }
            }
            var resultado = _repSucursal
                .SelectBy(critSucursal)
                .Join(_repEntidad.Table,
                scl => scl.IdEntidad,
                ent => ent.IdEntidad,
                (scl, ent) => new { scl, ent })
                .Select(tmp => new PocSucursal
                {
                    IdSucursal = tmp.scl.IdSucursal,
                    IdEntidad = (int)tmp.scl.IdEntidad,
                    DescripcionEntidad = tmp.ent.Descripcion,
                    Codigo = tmp.scl.Codigo,
                    Descripcion = tmp.scl.Descripcion,
                    Direccion = tmp.scl.Direccion,
                    Telefono = tmp.scl.Telefono,
                    Correo = tmp.scl.Correo,
                    EncargadoOficina = tmp.scl.EncargadoOficina,
                    Horario = tmp.scl.Horario
                });
            if (resultado.Any())
            {
                return resultado.ToDataResult(request);
            }
            return null;
        }

        public IEnumerable<PocSucursal> ObtenerSucursales()
        {
            var sucursales = _repSucursal.Table
                .Where(x => x.IdEntidad == InfoSesion.Info.IdCompania)
                .OrderBy(m => m.Descripcion).ToList();
            return sucursales.ProyectarComo<PocSucursal>();
        }

        public PocSucursal ObtenerSucursal(int idSucursal)
        {
            var sucursales = _repSucursal.SelectById(idSucursal);
            return sucursales.ProyectarComo<PocSucursal>();
        }

        private ValidationResponse Validar(PocSucursal registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (inserta)
            {
                if (registro.IdEntidad <= 0) { validacion.AddError("Err_0024"); }
                if (String.IsNullOrEmpty(registro.Correo)) { validacion.AddError("Err_0008"); }
                if (String.IsNullOrEmpty(registro.Direccion)) { validacion.AddError("Err_0090"); }
                if (String.IsNullOrEmpty(registro.EncargadoOficina)) { validacion.AddError("Err_0091"); }
                if (String.IsNullOrEmpty(registro.Horario)) { validacion.AddError("Err_0092"); }
            }
            else
            {
                if (registro.IdSucursal <= 0) { validacion.AddError("Err_0078"); }
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }
    }
}

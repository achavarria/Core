﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Efectivo;
using Monibyte.Arquitectura.Dominio.Efectivo.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Monibyte.Arquitectura.Datos.Efectivo.Repositorios
{
    public class RepositorioMovimientos : RepositorioMnb<EF_Movimiento>, IRepositorioMovimientos
    {
        public RepositorioMovimientos(IUnidadTbjoMonibyte unidadTbjo) : base(unidadTbjo) { }

        public IEnumerable<EF_vwMovimiento> ObtenerMovimientos(ParamMovimientosEf parameters)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", parameters.IdEmpresa));
            listaParams.Add(new SqlParameter("p2", (object)parameters.IdPersona ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p3", (object)parameters.FecDesde ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p4", (object)parameters.FecHasta ?? DBNull.Value));
            var _buscarMovimientos = parameters.FiltroGestiones != null ?
                parameters.FiltroGestiones.MatchExists == true ? 1 as int? :
                parameters.FiltroGestiones.MatchExists == false ? 0 as int? : null : null;
            listaParams.Add(new SqlParameter("p5", (object)_buscarMovimientos ?? DBNull.Value));
            var _intgestiones = parameters.FiltroGestiones != null ?
                parameters.FiltroGestiones.Gestiones : null;
            var _gestiones = _intgestiones != null ? _intgestiones.ToJson() : null;
            listaParams.Add(new SqlParameter("p6", (object)_gestiones ?? DBNull.Value));
            var _intmovs = parameters.FiltroGestiones != null ?
               parameters.FiltroGestiones.IdMovimientos : null;
            var movs = _intmovs != null ? _intmovs.ToJson() : null;
            listaParams.Add(new SqlParameter("p7", (object)movs ?? DBNull.Value));

            var resultado = (this.UnidadTbjo as ISql).ExecuteQuery<EF_vwMovimiento>
               (@"Efectivo.P_MovimientosEfectivo @p1, @p2, @p3, @p4, @p5, @p6, @p7",
               listaParams.ToArray());
            return resultado;
        }
    }
}

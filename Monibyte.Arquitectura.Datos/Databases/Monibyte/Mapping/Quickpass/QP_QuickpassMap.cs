using Monibyte.Arquitectura.Dominio.Quickpass;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Quickpass
{
    public class QP_QuickpassMap : EntityTypeConfiguration<QP_Quickpass>
    {
        public QP_QuickpassMap()
        {
            // Primary Key
            this.HasKey(t => t.IdQuickpass);

            // Properties
            this.Property(t => t.IdQuickpass)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumQuickpass)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.NumDocumento)
                .HasMaxLength(20);

            this.Property(t => t.NumPlaca)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("QP_Quickpass", "Quickpass");
            this.Property(t => t.IdQuickpass).HasColumnName("IdQuickpass");
            this.Property(t => t.IdTipoQuickpass).HasColumnName("IdTipoQuickpass");
            this.Property(t => t.NumQuickpass).HasColumnName("NumQuickpass");
            this.Property(t => t.NumDocumento).HasColumnName("NumDocumento");
            this.Property(t => t.FecVencimiento).HasColumnName("FecVencimiento");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.IdTarjeta).HasColumnName("IdTarjeta");
            this.Property(t => t.IdCuenta).HasColumnName("IdCuenta");
            this.Property(t => t.NumPlaca).HasColumnName("NumPlaca");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdEstadoOperativo).HasColumnName("IdEstadoOperativo");
            this.Property(t => t.FecRevisionCostoAdm).HasColumnName("FecRevisionCostoAdm");
            this.Property(t => t.IdFinanciado).HasColumnName("IdFinanciado");
            this.Property(t => t.SaldoDispositivo).HasColumnName("SaldoDispositivo");
            this.Property(t => t.IdPrepago).HasColumnName("IdPrepago");
            this.Property(t => t.MonCostoAdm).HasColumnName("MonCostoAdm");
            this.Property(t => t.MonCargoInicial).HasColumnName("MonCargoInicial");
            this.Property(t => t.MonMinRecarga).HasColumnName("MonMinRecarga");
            this.Property(t => t.MonCostoUnidad).HasColumnName("MonCostoUnidad");
            this.Property(t => t.IdMonedaAdm).HasColumnName("IdMonedaAdm");
            this.Property(t => t.IdMonedaPeaje).HasColumnName("IdMonedaPeaje");
            this.Property(t => t.IdCargoDiario).HasColumnName("IdCargoDiario");
            this.Property(t => t.SaldoPeaje).HasColumnName("SaldoPeaje");
            this.Property(t => t.MonMinListaNegra).HasColumnName("MonMinListaNegra");
            this.Property(t => t.MesesFinanciamiento).HasColumnName("MesesFinanciamiento");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

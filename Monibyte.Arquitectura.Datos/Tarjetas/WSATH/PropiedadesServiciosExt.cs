﻿using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using System;
using System.Configuration;

namespace Monibyte.Arquitectura.Datos.Tarjetas.WSATH
{
    public class PropiedadesServiciosExt
    {
            Configuration conf;
            ClientSettingsSection confSection;

            private const string ConfigGroup = "ConfiguracionServiciosExternos";

            public PropiedadesServiciosExt(string fileConfig, string sectionName, bool web = true)
            {
                conf = ConfigHelper.GetConfig(fileConfig, web);
                confSection = ConfigHelper.GetConfigSection(conf, ConfigGroup, sectionName);
            }

            public T ObtieneValPropiedad<T>(string propertyName, string sectionName = "")
            {
                var tempSection = string.IsNullOrEmpty(sectionName) ? confSection : ConfigHelper.GetConfigSection(conf, ConfigGroup, sectionName);
                var valor = tempSection.Settings.Get(propertyName).Value.ValueXml.InnerText;
                T result = (T)Convert.ChangeType(valor, typeof(T));
                return result;
            }
        }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;

namespace Monibyte.Arquitectura.Servicios.Web
{
    public class AppSessionWriter : SessionWriter
    {
        public override void AddHeaders(System.Net.WebHeaderCollection headers)
        {
            if (InfoSesion.ObtenerSesion<int?>("IDSISTEMA").HasValue)
            {
            headers.Add("IDSISTEMA", InfoSesion.ObtenerSesion<int>("IDSISTEMA").ToJson().CompressStr());
            }
            if (InfoSesion.ObtenerSesion<int?>("SEGURIDADACTIVA").HasValue)
            {
            headers.Add("SEGURIDADACTIVA", InfoSesion.ObtenerSesion<int>("SEGURIDADACTIVA").ToJson().CompressStr());
        }
    }
}}
﻿using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Poco
{
    static class Extension
    {
        public static bool IsEqualTo<T>(this IEnumerable<T> list1, IEnumerable<T> list2)
        {
            if (list1 == null && list2 == null)
                return true;
            if ((list1 != null && list2 == null) || (list1 == null && list2 != null))
                return false;
            if (list1.Count() != list2.Count())
                return false;
            return list1.SequenceEqual(list2);
        }
    }
}

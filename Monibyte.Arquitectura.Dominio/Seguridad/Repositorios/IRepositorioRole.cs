﻿
namespace Monibyte.Arquitectura.Dominio.Seguridad.Repositorios
{
    public interface IRepositorioRole : IRepositorioMnb<SC_Role>
    {
        SC_Role ObtenerRolPorCodigo(int codRol);
    }
}

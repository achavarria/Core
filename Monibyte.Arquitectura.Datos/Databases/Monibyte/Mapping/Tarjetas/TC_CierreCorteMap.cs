using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_CierreCorteMap : EntityTypeConfiguration<TC_CierreCorte>
    {
        public TC_CierreCorteMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCorte);

            // Properties
            this.Property(t => t.IdCorte)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TC_CierreCorte", "TCredito");
            this.Property(t => t.IdCorte).HasColumnName("IdCorte");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdUsuarioCorte).HasColumnName("IdUsuarioCorte");
            this.Property(t => t.FecCorte).HasColumnName("FecCorte");
            this.Property(t => t.FecCierre).HasColumnName("FecCierre");
            this.Property(t => t.HoraCierre).HasColumnName("HoraCierre");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

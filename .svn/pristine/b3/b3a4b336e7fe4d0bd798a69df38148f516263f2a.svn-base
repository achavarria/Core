﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Tesoreria.Repositorios
{
    public class FuncionesTesoreria : RepositorioBase, IFuncionesTesoreria
    {
        ISql _unidadTbjo;
        public FuncionesTesoreria(IUnidadTbjoMonibyte unidadTbjo)
            : base(unidadTbjo)
        {
            _unidadTbjo = unidadTbjo;
        }

        public decimal ConvierteMonto(int? idMonedaOrigen, decimal? monto, int? idMonedaDestino,
             decimal? cambioOrigen = null, decimal? cambioDestino = null, int? idEntidadFinanciera = null)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", (object)idMonedaOrigen ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p2", (object)monto ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p3", (object)idMonedaDestino ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p4", (object)cambioOrigen ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p5", (object)cambioDestino ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p6", (object)idEntidadFinanciera ?? DBNull.Value));

            var resultado = _unidadTbjo.ExecuteQuery<decimal>
               (@"Tesoreria.P_ConvierteMonto @p1, @p2, @p3, @p4, @p5, @p6", listaParams.ToArray()).FirstOrDefault();

            return resultado;
        }

        public decimal ConvierteMontoHistorico(ParamsConvierteMontoHistoricoTS parameters)
        {
            var listaParams = new List<SqlParameter>(6);
            listaParams.Add(new SqlParameter("p1", (object)parameters.idMonedaOrigen ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p2", (object)parameters.monto ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p3", (object)parameters.idMonedaDestino ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p4", (object)parameters.fecReferencia));
            listaParams.Add(new SqlParameter("p5", (object)parameters.idEntidadFinanciera ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p6", (object)parameters.idTipoCambio ?? DBNull.Value));

            var resultado = (this.UnidadTbjo as ISql).ExecuteQuery<decimal>(@"Tesoreria.P_ConvierteMontoHistorico 
                @p1, @p2, @p3, @p4, @p5, @p6", listaParams.ToArray()).FirstOrDefault();

            return resultado;
        }

        public decimal ObtieneTipoCambio(ParamsObtieneTipoCambioTS parameters)
        {
            var listaParams = new List<SqlParameter>(6);
            listaParams.Add(new SqlParameter("p1", (object)parameters.IdTipoCambio));
            listaParams.Add(new SqlParameter("p2", (object)parameters.FecCambio));
            listaParams.Add(new SqlParameter("p3", (object)parameters.IdMoneda));
            listaParams.Add(new SqlParameter("p4", (object)parameters.IdEntidad ?? DBNull.Value));

            var resultado = (this.UnidadTbjo as ISql).ExecuteQuery<decimal>(@"Tesoreria.P_ObtieneTipoCambio 
                @p1, @p2, @p3, @p4", listaParams.ToArray()).FirstOrDefault();

            return resultado;
        }
    }
}

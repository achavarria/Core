using Monibyte.Arquitectura.Dominio.Tarjetas;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tarjetas
{
    public class TC_ProspectacionMasivaMap : EntityTypeConfiguration<TC_ProspectacionMasiva>
    {
        public TC_ProspectacionMasivaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdSolicitud);

            // Properties
            this.Property(t => t.IdSolicitud)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TipoRegistro)
                .HasMaxLength(5);

            this.Property(t => t.NumCedula)
                .HasMaxLength(30);

            this.Property(t => t.Personeria)
                .HasMaxLength(5);

            this.Property(t => t.NombreCliente)
                .HasMaxLength(30);

            this.Property(t => t.SegundoNombre)
                .HasMaxLength(30);

            this.Property(t => t.PrimerApellido)
                .HasMaxLength(30);

            this.Property(t => t.SegundoApellido)
                .HasMaxLength(30);

            this.Property(t => t.ApellidoCasada)
                .HasMaxLength(30);

            this.Property(t => t.EstadoCivil)
                .HasMaxLength(5);

            this.Property(t => t.MonedaIngreso)
                .HasMaxLength(5);

            this.Property(t => t.CodProfesion)
                .HasMaxLength(5);

            this.Property(t => t.Nacionalidad)
                .HasMaxLength(5);

            this.Property(t => t.Sexo)
                .HasMaxLength(5);

            this.Property(t => t.ProvinciaTh)
                .HasMaxLength(5);

            this.Property(t => t.CantonTh)
                .HasMaxLength(5);

            this.Property(t => t.DistritoTh)
                .HasMaxLength(5);

            this.Property(t => t.DirTh)
                .HasMaxLength(110);

            this.Property(t => t.TelefonoHabitacion)
                .HasMaxLength(30);

            this.Property(t => t.NumFaxPersonal)
                .HasMaxLength(30);

            this.Property(t => t.NumTelefonoCelular)
                .HasMaxLength(30);

            this.Property(t => t.NumRadioLocalizador)
                .HasMaxLength(30);

            this.Property(t => t.Correo)
                .HasMaxLength(60);

            this.Property(t => t.CodPostalTh)
                .HasMaxLength(20);

            this.Property(t => t.ApartadoPostalTh)
                .HasMaxLength(30);

            this.Property(t => t.NombrePatrono)
                .HasMaxLength(60);

            this.Property(t => t.TelefonoOficina)
                .HasMaxLength(30);

            this.Property(t => t.NumFaxTrabajo)
                .HasMaxLength(30);

            this.Property(t => t.CodProvinciaTrabajo)
                .HasMaxLength(5);

            this.Property(t => t.CodCantonTrabajo)
                .HasMaxLength(5);

            this.Property(t => t.CodDistritoTrabajo)
                .HasMaxLength(5);

            this.Property(t => t.DirPatrono)
                .HasMaxLength(90);

            this.Property(t => t.ZonaPostalPatrono)
                .HasMaxLength(20);

            this.Property(t => t.ApartadoPostalPatrono)
                .HasMaxLength(30);

            this.Property(t => t.CodParentescoPariente)
                .HasMaxLength(5);

            this.Property(t => t.MonedaEmisor)
                .HasMaxLength(5);

            this.Property(t => t.SucursalRetiroTarjeta)
                .HasMaxLength(5);

            this.Property(t => t.CicloCorte)
                .HasMaxLength(5);

            this.Property(t => t.Emisor)
                .HasMaxLength(20);

            this.Property(t => t.NombreAEmbozar)
                .HasMaxLength(30);

            this.Property(t => t.Filler12)
                .HasMaxLength(20);

            this.Property(t => t.TipoGarantia)
                .HasMaxLength(5);

            this.Property(t => t.Filler13)
                .HasMaxLength(20);

            this.Property(t => t.TipoCliente)
                .HasMaxLength(5);

            this.Property(t => t.Filler14)
                .HasMaxLength(30);

            this.Property(t => t.Filler15)
                .HasMaxLength(20);

            this.Property(t => t.IndicadorPlan)
                .HasMaxLength(5);

            this.Property(t => t.Filler16)
                .HasMaxLength(20);

            this.Property(t => t.CedulaConyuge)
                .HasMaxLength(20);

            this.Property(t => t.NombreConyuge)
                .HasMaxLength(60);

            this.Property(t => t.NumCuentaValor)
                .HasMaxLength(30);

            this.Property(t => t.TipoPoliza)
                .HasMaxLength(5);

            this.Property(t => t.CodVendedor)
                .HasMaxLength(5);

            this.Property(t => t.DirEnvioEstadoCuenta)
                .HasMaxLength(110);

            this.Property(t => t.CiudadEnvio)
                .HasMaxLength(30);

            this.Property(t => t.CodPostalEnvio)
                .HasMaxLength(20);

            this.Property(t => t.ApartadoPostalEnvio)
                .HasMaxLength(30);

            this.Property(t => t.NombrePariente)
                .HasMaxLength(30);

            this.Property(t => t.PrimerApellidoPariente)
                .HasMaxLength(30);

            this.Property(t => t.SegundoApellidoPariente)
                .HasMaxLength(30);

            this.Property(t => t.TelefonoPariente)
                .HasMaxLength(30);

            this.Property(t => t.DirPariente)
                .HasMaxLength(60);

            this.Property(t => t.CiudadPariente)
                .HasMaxLength(30);

            this.Property(t => t.CodPostalThPariente)
                .HasMaxLength(20);

            this.Property(t => t.ApartadoPostalThPariente)
                .HasMaxLength(30);

            this.Property(t => t.ProgramaPremiacion)
                .HasMaxLength(5);

            this.Property(t => t.CodSuperfranquicia)
                .HasMaxLength(5);

            this.Property(t => t.TipoTarjeta)
                .HasMaxLength(5);

            this.Property(t => t.CodDiseno)
                .HasMaxLength(5);

            this.Property(t => t.CorreoEstadosCta)
                .HasMaxLength(60);

            this.Property(t => t.Filler01)
                .HasMaxLength(5);

            this.Property(t => t.Filler02)
                .HasMaxLength(5);

            this.Property(t => t.Filler03)
                .HasMaxLength(5);

            this.Property(t => t.Filler04)
                .HasMaxLength(5);

            this.Property(t => t.Filler05)
                .HasMaxLength(20);

            this.Property(t => t.Filler06)
                .HasMaxLength(20);

            this.Property(t => t.TipoIdentificacion)
                .HasMaxLength(5);

            this.Property(t => t.CodEmisionDocIdentidad)
                .HasMaxLength(20);

            this.Property(t => t.NumeroNit)
                .HasMaxLength(30);

            this.Property(t => t.LugarEmisionNit)
                .HasMaxLength(5);

            this.Property(t => t.CategoriaCliente)
                .HasMaxLength(5);

            this.Property(t => t.Campana)
                .HasMaxLength(60);

            this.Property(t => t.LocalidadLaCuenta)
                .HasMaxLength(5);

            this.Property(t => t.NumeroCliente)
                .HasMaxLength(30);

            this.Property(t => t.IndicadorCuotaFija)
                .HasMaxLength(5);

            this.Property(t => t.CuotaFija)
                .HasMaxLength(20);

            this.Property(t => t.CodPostalEstCuenta)
                .HasMaxLength(20);

            this.Property(t => t.CodPostalApartado)
                .HasMaxLength(20);

            this.Property(t => t.Region)
                .HasMaxLength(5);

            this.Property(t => t.Zona)
                .HasMaxLength(5);

            this.Property(t => t.Sector)
                .HasMaxLength(5);

            this.Property(t => t.CodTipoPlanPoliza)
                .HasMaxLength(5);

            this.Property(t => t.NumPoliza)
                .HasMaxLength(30);

            this.Property(t => t.Dir3Th)
                .HasMaxLength(60);

            this.Property(t => t.Dir3Ec)
                .HasMaxLength(60);

            this.Property(t => t.DirShippingAddress)
                .HasMaxLength(120);

            this.Property(t => t.PlanBaseCompras)
                .HasMaxLength(20);

            this.Property(t => t.PlanBaseAdelantos)
                .HasMaxLength(20);

            this.Property(t => t.RazonCreacionCta)
                .HasMaxLength(5);

            this.Property(t => t.AplicaRecargoCuotaVencida)
                .HasMaxLength(5);

            this.Property(t => t.CobraComisionAdelanto)
                .HasMaxLength(5);

            this.Property(t => t.CobraComisionCuasiCash)
                .HasMaxLength(5);

            this.Property(t => t.AplicaCargoSobregiro)
                .HasMaxLength(5);

            this.Property(t => t.CobraInteres)
                .HasMaxLength(5);

            this.Property(t => t.AplicaCargoChequeDevuelto)
                .HasMaxLength(5);

            this.Property(t => t.PermiteRebajoPagoAutomatico)
                .HasMaxLength(5);

            this.Property(t => t.RebajoPagoFijo)
                .HasMaxLength(5);

            this.Property(t => t.RebajoPagoContado)
                .HasMaxLength(5);

            this.Property(t => t.NumCuenta)
                .HasMaxLength(30);

            this.Property(t => t.SeguroSocial)
                .HasMaxLength(20);

            this.Property(t => t.Calificativo)
                .HasMaxLength(5);

            this.Property(t => t.TelefonoConyugue)
                .HasMaxLength(20);

            this.Property(t => t.TelefonoTrabajoConyugue)
                .HasMaxLength(20);

            this.Property(t => t.LugarTrabajoConyugue)
                .HasMaxLength(60);

            this.Property(t => t.CobraImpuestosFeci)
                .HasMaxLength(5);

            this.Property(t => t.Filler07)
                .HasMaxLength(20);

            this.Property(t => t.Filler08)
                .HasMaxLength(5);

            this.Property(t => t.Filler09)
                .HasMaxLength(5);

            this.Property(t => t.Filler10)
                .HasMaxLength(20);

            this.Property(t => t.Filler11)
                .HasMaxLength(20);

            this.Property(t => t.CodAutonomia)
                .HasMaxLength(5);

            this.Property(t => t.CodUsuarioAutonomia)
                .HasMaxLength(5);

            this.Property(t => t.EmpresaCobro)
                .HasMaxLength(20);

            this.Property(t => t.RegistroTributarioNacional)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("TC_ProspectacionMasiva", "TCredito");
            this.Property(t => t.IdSolicitud).HasColumnName("IdSolicitud");
            this.Property(t => t.TipoRegistro).HasColumnName("TipoRegistro");
            this.Property(t => t.NumCedula).HasColumnName("NumCedula");
            this.Property(t => t.Personeria).HasColumnName("Personeria");
            this.Property(t => t.NombreCliente).HasColumnName("NombreCliente");
            this.Property(t => t.SegundoNombre).HasColumnName("SegundoNombre");
            this.Property(t => t.PrimerApellido).HasColumnName("PrimerApellido");
            this.Property(t => t.SegundoApellido).HasColumnName("SegundoApellido");
            this.Property(t => t.ApellidoCasada).HasColumnName("ApellidoCasada");
            this.Property(t => t.EstadoCivil).HasColumnName("EstadoCivil");
            this.Property(t => t.FecNacimiento).HasColumnName("FecNacimiento");
            this.Property(t => t.MontoIngreso).HasColumnName("MontoIngreso");
            this.Property(t => t.MonedaIngreso).HasColumnName("MonedaIngreso");
            this.Property(t => t.CodProfesion).HasColumnName("CodProfesion");
            this.Property(t => t.Nacionalidad).HasColumnName("Nacionalidad");
            this.Property(t => t.Sexo).HasColumnName("Sexo");
            this.Property(t => t.ProvinciaTh).HasColumnName("ProvinciaTh");
            this.Property(t => t.CantonTh).HasColumnName("CantonTh");
            this.Property(t => t.DistritoTh).HasColumnName("DistritoTh");
            this.Property(t => t.DirTh).HasColumnName("DirTh");
            this.Property(t => t.TelefonoHabitacion).HasColumnName("TelefonoHabitacion");
            this.Property(t => t.NumFaxPersonal).HasColumnName("NumFaxPersonal");
            this.Property(t => t.NumTelefonoCelular).HasColumnName("NumTelefonoCelular");
            this.Property(t => t.NumRadioLocalizador).HasColumnName("NumRadioLocalizador");
            this.Property(t => t.Correo).HasColumnName("Correo");
            this.Property(t => t.CodPostalTh).HasColumnName("CodPostalTh");
            this.Property(t => t.ApartadoPostalTh).HasColumnName("ApartadoPostalTh");
            this.Property(t => t.NombrePatrono).HasColumnName("NombrePatrono");
            this.Property(t => t.TelefonoOficina).HasColumnName("TelefonoOficina");
            this.Property(t => t.NumFaxTrabajo).HasColumnName("NumFaxTrabajo");
            this.Property(t => t.CodProvinciaTrabajo).HasColumnName("CodProvinciaTrabajo");
            this.Property(t => t.CodCantonTrabajo).HasColumnName("CodCantonTrabajo");
            this.Property(t => t.CodDistritoTrabajo).HasColumnName("CodDistritoTrabajo");
            this.Property(t => t.DirPatrono).HasColumnName("DirPatrono");
            this.Property(t => t.ZonaPostalPatrono).HasColumnName("ZonaPostalPatrono");
            this.Property(t => t.ApartadoPostalPatrono).HasColumnName("ApartadoPostalPatrono");
            this.Property(t => t.CodParentescoPariente).HasColumnName("CodParentescoPariente");
            this.Property(t => t.LimiteCredito).HasColumnName("LimiteCredito");
            this.Property(t => t.MonedaEmisor).HasColumnName("MonedaEmisor");
            this.Property(t => t.SucursalRetiroTarjeta).HasColumnName("SucursalRetiroTarjeta");
            this.Property(t => t.CicloCorte).HasColumnName("CicloCorte");
            this.Property(t => t.Emisor).HasColumnName("Emisor");
            this.Property(t => t.NombreAEmbozar).HasColumnName("NombreAEmbozar");
            this.Property(t => t.Filler12).HasColumnName("Filler12");
            this.Property(t => t.TipoGarantia).HasColumnName("TipoGarantia");
            this.Property(t => t.Filler13).HasColumnName("Filler13");
            this.Property(t => t.TipoCliente).HasColumnName("TipoCliente");
            this.Property(t => t.Filler14).HasColumnName("Filler14");
            this.Property(t => t.Filler15).HasColumnName("Filler15");
            this.Property(t => t.IndicadorPlan).HasColumnName("IndicadorPlan");
            this.Property(t => t.Filler16).HasColumnName("Filler16");
            this.Property(t => t.CedulaConyuge).HasColumnName("CedulaConyuge");
            this.Property(t => t.NombreConyuge).HasColumnName("NombreConyuge");
            this.Property(t => t.NumCuentaValor).HasColumnName("NumCuentaValor");
            this.Property(t => t.TipoPoliza).HasColumnName("TipoPoliza");
            this.Property(t => t.CodVendedor).HasColumnName("CodVendedor");
            this.Property(t => t.DirEnvioEstadoCuenta).HasColumnName("DirEnvioEstadoCuenta");
            this.Property(t => t.CiudadEnvio).HasColumnName("CiudadEnvio");
            this.Property(t => t.CodPostalEnvio).HasColumnName("CodPostalEnvio");
            this.Property(t => t.ApartadoPostalEnvio).HasColumnName("ApartadoPostalEnvio");
            this.Property(t => t.NombrePariente).HasColumnName("NombrePariente");
            this.Property(t => t.PrimerApellidoPariente).HasColumnName("PrimerApellidoPariente");
            this.Property(t => t.SegundoApellidoPariente).HasColumnName("SegundoApellidoPariente");
            this.Property(t => t.TelefonoPariente).HasColumnName("TelefonoPariente");
            this.Property(t => t.DirPariente).HasColumnName("DirPariente");
            this.Property(t => t.CiudadPariente).HasColumnName("CiudadPariente");
            this.Property(t => t.CodPostalThPariente).HasColumnName("CodPostalThPariente");
            this.Property(t => t.ApartadoPostalThPariente).HasColumnName("ApartadoPostalThPariente");
            this.Property(t => t.ProgramaPremiacion).HasColumnName("ProgramaPremiacion");
            this.Property(t => t.CodSuperfranquicia).HasColumnName("CodSuperfranquicia");
            this.Property(t => t.TipoTarjeta).HasColumnName("TipoTarjeta");
            this.Property(t => t.CodDiseno).HasColumnName("CodDiseno");
            this.Property(t => t.CorreoEstadosCta).HasColumnName("CorreoEstadosCta");
            this.Property(t => t.Filler01).HasColumnName("Filler01");
            this.Property(t => t.Filler02).HasColumnName("Filler02");
            this.Property(t => t.Filler03).HasColumnName("Filler03");
            this.Property(t => t.Filler04).HasColumnName("Filler04");
            this.Property(t => t.Filler05).HasColumnName("Filler05");
            this.Property(t => t.Filler06).HasColumnName("Filler06");
            this.Property(t => t.TipoIdentificacion).HasColumnName("TipoIdentificacion");
            this.Property(t => t.CodEmisionDocIdentidad).HasColumnName("CodEmisionDocIdentidad");
            this.Property(t => t.NumeroNit).HasColumnName("NumeroNit");
            this.Property(t => t.LugarEmisionNit).HasColumnName("LugarEmisionNit");
            this.Property(t => t.CategoriaCliente).HasColumnName("CategoriaCliente");
            this.Property(t => t.Campana).HasColumnName("Campana");
            this.Property(t => t.LocalidadLaCuenta).HasColumnName("LocalidadLaCuenta");
            this.Property(t => t.NumeroCliente).HasColumnName("NumeroCliente");
            this.Property(t => t.IndicadorCuotaFija).HasColumnName("IndicadorCuotaFija");
            this.Property(t => t.CuotaFija).HasColumnName("CuotaFija");
            this.Property(t => t.CodPostalEstCuenta).HasColumnName("CodPostalEstCuenta");
            this.Property(t => t.CodPostalApartado).HasColumnName("CodPostalApartado");
            this.Property(t => t.Region).HasColumnName("Region");
            this.Property(t => t.Zona).HasColumnName("Zona");
            this.Property(t => t.Sector).HasColumnName("Sector");
            this.Property(t => t.CodTipoPlanPoliza).HasColumnName("CodTipoPlanPoliza");
            this.Property(t => t.NumPoliza).HasColumnName("NumPoliza");
            this.Property(t => t.Dir3Th).HasColumnName("Dir3Th");
            this.Property(t => t.Dir3Ec).HasColumnName("Dir3Ec");
            this.Property(t => t.DirShippingAddress).HasColumnName("DirShippingAddress");
            this.Property(t => t.PlanBaseCompras).HasColumnName("PlanBaseCompras");
            this.Property(t => t.PlanBaseAdelantos).HasColumnName("PlanBaseAdelantos");
            this.Property(t => t.RazonCreacionCta).HasColumnName("RazonCreacionCta");
            this.Property(t => t.AplicaRecargoCuotaVencida).HasColumnName("AplicaRecargoCuotaVencida");
            this.Property(t => t.CobraComisionAdelanto).HasColumnName("CobraComisionAdelanto");
            this.Property(t => t.CobraComisionCuasiCash).HasColumnName("CobraComisionCuasiCash");
            this.Property(t => t.AplicaCargoSobregiro).HasColumnName("AplicaCargoSobregiro");
            this.Property(t => t.CobraInteres).HasColumnName("CobraInteres");
            this.Property(t => t.AplicaCargoChequeDevuelto).HasColumnName("AplicaCargoChequeDevuelto");
            this.Property(t => t.PermiteRebajoPagoAutomatico).HasColumnName("PermiteRebajoPagoAutomatico");
            this.Property(t => t.RebajoPagoFijo).HasColumnName("RebajoPagoFijo");
            this.Property(t => t.MontoPagoFijo).HasColumnName("MontoPagoFijo");
            this.Property(t => t.RebajoPagoContado).HasColumnName("RebajoPagoContado");
            this.Property(t => t.NumCuenta).HasColumnName("NumCuenta");
            this.Property(t => t.SeguroSocial).HasColumnName("SeguroSocial");
            this.Property(t => t.Calificativo).HasColumnName("Calificativo");
            this.Property(t => t.TelefonoConyugue).HasColumnName("TelefonoConyugue");
            this.Property(t => t.TelefonoTrabajoConyugue).HasColumnName("TelefonoTrabajoConyugue");
            this.Property(t => t.LugarTrabajoConyugue).HasColumnName("LugarTrabajoConyugue");
            this.Property(t => t.CobraImpuestosFeci).HasColumnName("CobraImpuestosFeci");
            this.Property(t => t.FecCobroMembresi).HasColumnName("FecCobroMembresi");
            this.Property(t => t.Filler07).HasColumnName("Filler07");
            this.Property(t => t.Filler08).HasColumnName("Filler08");
            this.Property(t => t.Filler09).HasColumnName("Filler09");
            this.Property(t => t.Filler10).HasColumnName("Filler10");
            this.Property(t => t.Filler11).HasColumnName("Filler11");
            this.Property(t => t.CodAutonomia).HasColumnName("CodAutonomia");
            this.Property(t => t.CodUsuarioAutonomia).HasColumnName("CodUsuarioAutonomia");
            this.Property(t => t.EmpresaCobro).HasColumnName("EmpresaCobro");
            this.Property(t => t.RegistroTributarioNacional).HasColumnName("RegistroTributarioNacional");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdLote).HasColumnName("IdLote");
        }
    }
}

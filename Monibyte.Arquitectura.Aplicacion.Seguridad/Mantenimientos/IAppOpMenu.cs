﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Seguridad;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Seguridad.Mantenimientos
{
    public interface IAppOpMenu : IAplicacionBase
    {
        List<PocMenu> ListaConsulta();
    }
}

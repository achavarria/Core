﻿
namespace Monibyte.Arquitectura.Dominio.Generales
{
    public class DetallePolitica
    {
        public int LongitudMinima { get; set; }
        public int LongitudMaxima { get; set; }
        public int MinNumericos { get; set; }
        public int MinCaracteres { get; set; }
        public int MaxIntentosBloqueo { get; set; }
        public int MinutosBloqueoTemp { get; set; }
        public int IntentosBloqueoTemp { get; set; }
        public int DiasNotificaPassword { get; set; }
        public string NumerosPermitidos { get; set; }
        public string CaracteresPermitidos { get; set; }
    }
}

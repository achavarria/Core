using Monibyte.Arquitectura.Dominio.Tesoreria;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tesoreria
{
    public class TS_TipoDeCambioMap : EntityTypeConfiguration<TS_TipoDeCambio>
    {
        public TS_TipoDeCambioMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCambio);

            // Properties
            this.Property(t => t.IdCambio)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TS_TipoDeCambio", "Tesoreria");
            this.Property(t => t.IdCambio).HasColumnName("IdCambio");
            this.Property(t => t.IdEntidadFinanciera).HasColumnName("IdEntidadFinanciera");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.IdTipoCambio).HasColumnName("IdTipoCambio");
            this.Property(t => t.FecCambio).HasColumnName("FecCambio");
            this.Property(t => t.MonTipoCambio).HasColumnName("MonTipoCambio");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdMonedaConversion).HasColumnName("IdMonedaConversion");
            this.Property(t => t.HoraCambio).HasColumnName("HoraCambio");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

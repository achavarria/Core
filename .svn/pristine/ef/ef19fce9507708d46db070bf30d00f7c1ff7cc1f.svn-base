﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Generales
{
    public class ParametroEmpresa : AplicacionBase, IParametroEmpresa
    {
        private IRepositorioMnb<GL_ParametroEmpresa> _repParametroEmpresa;
        private IRepositorioMnb<GL_ParametroEmpresaLista> _repParametroEmpresaLista;
        private IRepositorioMnb<GL_ParametroEmpresaContexto> _repParametroEmpresaContexto;
        private IRepositorioMnb<GL_Reportes> _repReportes;
        private IRepositorioMnb<GL_CamposReporte> _repCamposReporte;
        private IRepositorioMnb<GL_CamposContexto> _repCamposContext;
        private IRepositorioMnb<CL_PerfilEmpresa> _repPerfilEmpresa;
        private IRepositorioMnb<GL_ParametroSistema> _repParamSistema;

        public ParametroEmpresa(
            IRepositorioMnb<GL_ParametroEmpresa> repParametroEmpresa,
            IRepositorioMnb<GL_ParametroEmpresaLista> repParametroEmpresaLista,
            IRepositorioMnb<GL_ParametroEmpresaContexto> repParametroEmpresaContexto,
            IRepositorioMnb<GL_Reportes> repReportes,
            IRepositorioMnb<GL_CamposReporte> repCamposReporte,
            IRepositorioMnb<GL_CamposContexto> repCamposContext,
            IRepositorioMnb<CL_PerfilEmpresa> repPerfilEmpresa,
            IRepositorioMnb<GL_ParametroSistema> repParamSistema
            )
        {
            _repParametroEmpresa = repParametroEmpresa;
            _repParametroEmpresaLista = repParametroEmpresaLista;
            _repParametroEmpresaContexto = repParametroEmpresaContexto;
            _repReportes = repReportes;
            _repCamposReporte = repCamposReporte;
            _repCamposContext = repCamposContext;
            _repPerfilEmpresa = repPerfilEmpresa;
            _repParamSistema = repParamSistema;
        }

        public IEnumerable<PocParametroEmpresa> ObtenerParametros(PocFiltroParametroEmpresa filtro)
        {
            var criteria = new Criteria<GL_ParametroEmpresa>();

            criteria.And(m => m.IdEmpresa == filtro.IdEmpresa);

            if (filtro.IdTipo.HasValue)
            {
                criteria.And(m => m.IdTipo == filtro.IdTipo);
            }
            if (filtro.ListaParametros != null)
            {
                criteria.And(m => filtro.ListaParametros.Contains(m.IdParametro));
            }

            var resultado =
                (from param in _repParametroEmpresa.SelectBy(criteria)
                 join lista in _repParametroEmpresaLista.Table
                     on param.IdParametro equals lista.IdParametroEmpresa
                     into paramLista
                 join context in _repParametroEmpresaContexto.Table
                     on param.IdParametro equals context.IdParametro into paramContext
                 from context in paramContext.DefaultIfEmpty()
                 where filtro.IdContexto == null || (context != null
                     && context.IdContexto == filtro.IdContexto)
                 orderby context != null ? context.IdOrden ?? 999 : 0
                 select new { p = param, l = paramLista, c = context }).ToList()
                 .Select(item => new PocParametroEmpresa
                 {
                     IdParametro = item.p.IdParametro,
                     IdEmpresa = item.p.IdEmpresa,
                     Nombre = item.p.Nombre,
                     IdTipo = item.p.IdTipo,
                     Descripcion = item.p.Descripcion,
                     LongitudMax = item.c != null ? item.c.LongitudMax : null,
                     IdEstado = item.c != null ? item.c.IdEstado : (int)EnumIdEstado.Activo,
                     IdEsRequerido = item.c != null ? item.c.IdEsRequerido : (int)EnumIdSiNo.No,
                     ValorDefecto = item.c != null ? item.c.ValorDefecto : null,
                     IdOrdenadoPor = item.p.IdOrdenadoPor,
                     Ascendente = item.p.Ascendente,
                     Lista = item.l != null ? OrdenarParamLista(item.p, item.l) : null,
                     IdParametroInterno = item.p.IdParametroInterno,
                     IdOrden = item.c != null ? item.c.IdOrden : null,
                     IdDependeDe = item.p.IdDependeDe,
                     ListaPadres = item.p.IdDependeDe.HasValue ?
                         ObtenerListasPadre(item.p.IdDependeDe).ToList() : null
                 });
            return resultado;
        }

        public IEnumerable<int> ObtenerListasPadre(int? idLista)
        {
            if (idLista.HasValue)
            {
                var datLista = _repParametroEmpresa.Table
                      .Where(x => x.IdParametro == idLista &&
                          x.IdEmpresa == InfoSesion.Info.IdEmpresa)
                    .Select(s => new
                    {
                        IdLista = s.IdParametro,
                        IdListaPadre = s.IdDependeDe
                    })
                    .FirstOrDefault();
                var listaAnterior = new List<int> { idLista.Value };
                if (datLista != null && datLista.IdListaPadre != null)
                {
                    var result = ObtenerListasPadre(datLista.IdListaPadre.Value);
                    listaAnterior.InsertRange(0, result);

                }
                return listaAnterior;
            }
            return null;
        }

        public void MantenimientoParametrosAlContexto(List<PocParametroEmpresa> parametros, int idContexto)
        {
            using (var ts = new TransactionScope())
            {
                foreach (var param in parametros)
                {
                    if (param.IdOrdenadoPor != (int)EnumTipoOrdenLista.Descripcion)
                    {
                        param.IdOrdenadoPor = (int)EnumTipoOrdenLista.Codigo;
                    }
                    if (param.Ascendente != (int)EnumIdSiNo.No)
                    {
                        param.Ascendente = (int)EnumIdSiNo.Si;
                    }
                    if (param.IdParametro == 0)
                    {
                        IncluirParametrosAlContexto(param, idContexto);
                    }
                    else
                    {
                        ActualizarParametros(param, idContexto);
                    }
                }
                _repParametroEmpresa.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void EliminarParametroDelContexto(PocParametroEmpresa parametro, int idContexto)
        {
            var parametrosContexto = _repParametroEmpresaContexto
                .Table.Where(m => m.IdParametro == parametro.IdParametro).ToList();
            if (parametrosContexto != null)
            {
                using (var ts = new TransactionScope())
                {
                    var _param = parametrosContexto.FirstOrDefault(m => m.IdContexto == idContexto);
                    if (_param.IdBloqueado == (int)EnumIdEstado.Inactivo)
                    {
                        _repParametroEmpresaContexto.Delete(_param);
                    }
                    if (!parametrosContexto.Any(m => m.IdBloqueado == (int)EnumIdEstado.Activo))
                    {
                        _repParametroEmpresa.DeleteOn(m => m.IdParametro == parametro.IdParametro);
                    }
                    _repParametroEmpresa.UnidadTbjo.Save();
                    ts.Complete();
                }
            }
        }

        private void IncluirParametrosAlContexto(PocParametroEmpresa parametro, int idContexto)
        {
            var validacion = new ValidationResponse();
            if (parametro == null) { validacion.AddError("Err_0076"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("El parámetro es requerido", validation: validacion);
            }
            if (idContexto <= 0) { validacion.AddError("Err_0075"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("El contexto es requerido", validation: validacion);
            }
            using (var ts = new TransactionScope())
            {
                if (parametro.IdParametro == 0)
                {
                    parametro.IdParametro = _fGenerales.ObtieneSgteSecuencia("SEQ_IdParametroEmp");
                    var item = parametro.ProyectarComo<GL_ParametroEmpresa>();
                    item.IdOrdenadoPor = parametro.IdOrdenadoPor;
                    item.Ascendente = parametro.Ascendente;
                    item.IdDependeDe = parametro.IdDependeDe;
                    item.IdParametroInterno =
                        item.IdParametroInterno == (int)EnumParametrosMONIBYTE.CentroCosto ||
                        item.IdParametroInterno == (int)EnumParametrosMONIBYTE.CuentaContable ?
                        item.IdParametroInterno : null;
                    _repParametroEmpresa.Insert(item);
                    if (item.IdTipo == (int)EnumTipoParamEmpresa.Lista)
                    {
                        //verifica si es una lista e inserta las opciones
                        if (parametro.Lista != null)
                        {
                            foreach (var opcion in parametro.Lista)
                            {
                                opcion.IdParametroEmpresa = item.IdParametro;
                                opcion.Accion = "I";
                                ActualizarParametroLista(opcion);
                            }
                        }
                    }
                    InsertaParametroAlContexto(idContexto, parametro);
                }
                ts.Complete();
            }
        }

        private void ActualizarParametros(PocParametroEmpresa parametro, int idContexto)
        {
            var validacion = new ValidationResponse();
            if (parametro == null) { validacion.AddError("Err_0076"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("El parámetro es requerido", validation: validacion);
            }
            using (var ts = new TransactionScope())
            {
                parametro.LongitudMax = parametro.LongitudMax != 0 ? parametro.LongitudMax : null;
                if (parametro.IdParametro != 0)
                {
                    var item = parametro.ProyectarComo<GL_ParametroEmpresa>();
                    item.IdParametroInterno =
                        item.IdParametroInterno == (int)EnumParametrosMONIBYTE.CentroCosto ||
                        item.IdParametroInterno == (int)EnumParametrosMONIBYTE.CuentaContable ?
                        item.IdParametroInterno : item.IdTipo == (int)EnumTipoParamEmpresa.ListaPredefinida ?
                        item.IdParametroInterno : null;
                    _repParametroEmpresa.Update(item,
                        p => p.Nombre, p => p.Descripcion,
                        p => p.IdTipo, p => p.Ascendente, p => p.IdOrdenadoPor,
                        p => p.IdParametroInterno, p => p.IdDependeDe);
                    if (item.IdTipo == (int)EnumTipoParamEmpresa.Lista)
                    {
                        //verifica si es una lista e inserta las opciones
                        if (parametro.Lista != null)
                        {
                            foreach (var opcion in parametro.Lista)
                            {
                                opcion.IdParametroEmpresa = item.IdParametro;
                                ActualizarParametroLista(opcion);
                            }
                        }
                    }
                    if (parametro.Importado)
                    {
                        InsertaParametroAlContexto(idContexto, parametro);
                    }
                    else
                    {
                        _repParametroEmpresaContexto.UpdateOn(m => m.IdParametro == parametro.IdParametro &&
                            m.IdContexto == idContexto,
                            up => new GL_ParametroEmpresaContexto
                            {
                                IdEstado = parametro.IdEstado,
                                IdEsRequerido = parametro.IdEsRequerido,
                                ValorDefecto = parametro.ValorDefecto,
                                LongitudMax = parametro.LongitudMax,
                                IdOrden = parametro.IdOrden
                            });
                    }
                }
                ts.Complete();
            }
        }

        private void ActualizarParametroLista(PocParametroEmpresaLista paramLista)
        {
            var validacion = new ValidationResponse();
            if (paramLista == null) { validacion.AddError("Err_0076"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("El parámetro es requerido", validation: validacion);
            }
            using (var ts = new TransactionScope())
            {
                //verifica si es una lista e inserta las opciones
                paramLista.CodigoDependencia = (paramLista.CodigoDependencia != null &&
                    paramLista.CodigoDependencia.Contains(null)) ? null : paramLista.CodigoDependencia;
                var opcion = paramLista.ProyectarComo<GL_ParametroEmpresaLista>();
                if (paramLista.Accion == Acciones.Update)
                {
                    _repParametroEmpresaLista.UpdateOn(x =>
                        x.IdParametroEmpresa == paramLista.IdParametroEmpresa &&
                        x.Codigo == paramLista.CodigoAnterior,
                        z => new GL_ParametroEmpresaLista
                        {
                            Codigo = opcion.Codigo,
                            Descripcion = opcion.Descripcion,
                            CodigoDependencia = string.IsNullOrEmpty(opcion.CodigoDependencia)
                                ? null : opcion.CodigoDependencia,
                            CodigoContrapartida = string.IsNullOrEmpty(opcion.CodigoContrapartida)
                                ? null : opcion.CodigoContrapartida,
                            ConfiguracionDinamica = string.IsNullOrEmpty(opcion.ConfiguracionDinamica)
                                ? null : opcion.ConfiguracionDinamica
                        });
                }
                else if (paramLista.Accion == Acciones.Delete)
                {
                    _repParametroEmpresaLista.Delete(opcion);
                }
                else if (paramLista.Accion == Acciones.Insert)
                {
                    if (opcion.CodigoDependencia != null && opcion.CodigoDependencia != "")
                    {
                        var dependeDe = _repParametroEmpresa.Table
                        .Where(x => x.IdParametro == opcion.IdParametroEmpresa).FirstOrDefault();
                        if (dependeDe.IdDependeDe != null && dependeDe.IdDependeDe != 0)
                        {
                            _repParametroEmpresaLista.Insert(opcion);
                        }
                    }
                    else
                    {
                        _repParametroEmpresaLista.Insert(opcion);
                    } 
                }
                ts.Complete();
            }
        }

        private void InsertaParametroAlContexto(int idContexto, PocParametroEmpresa item)
        {
            //inserta el param al contexto indicado
            _repParametroEmpresaContexto.Insert(new GL_ParametroEmpresaContexto
            {
                IdContexto = idContexto,
                IdParametro = item.IdParametro,
                IdEsRequerido = item.IdEsRequerido,
                IdEstado = item.IdEstado,
                ValorDefecto = item.ValorDefecto,
                LongitudMax = item.LongitudMax,
                IdOrden = item.IdOrden
            });
        }

        public IEnumerable<PocReporteBase> ObtenerReportesEmpresa(int?[] contextos,
            int? idTipoReporte, bool soloReportesConCampos, bool incluirReporteBase = false)
        {
            var criteria = new Criteria<GL_Reportes>();
            criteria.And(x => x.IdEmpresa == InfoSesion.Info.IdEmpresa);
            if (contextos != null)
            {
                criteria.And(x => contextos.Contains(x.IdContexto));
            }
            if (idTipoReporte.HasValue)
            {
                criteria.And(x => x.IdTipoReporte == idTipoReporte);
            }
            var query = _repReportes.SelectBy(criteria);
            if (soloReportesConCampos)
            {
                query = query
                    .GroupJoin(_repCamposReporte.Table,
                        rep => rep.IdReporte,
                        crp => crp.IdReporte,
                        (rep, crp) => new { rep, crp })
                    .Where(x => x.crp.Any())
                    .Select(item => item.rep);
                if (incluirReporteBase && contextos != null)
                {
                    var entidadBase = _repParamSistema.Table.FirstOrDefault(
                        x => x.IdParametro == "ID_PERSONA_ENTIDAD_BASE");
                    var idEmpresaBase = int.Parse(entidadBase.ValParametro);

                    var criteriaBase = new Criteria<GL_Reportes>();
                    criteriaBase.And(x => x.IdEmpresa == idEmpresaBase);
                    criteriaBase.And(x => contextos.Contains(x.IdContexto));
                    query = query.Concat(_repReportes.SelectBy(criteriaBase))
                        .OrderBy(x => x.IdEmpresa == idEmpresaBase ? "AAAAA" : x.Descripcion);
                }
            }
            return query.ProyectarComo<PocReporteBase>();
        }

        public IEnumerable<PocCampoReporteContexto> ObtenerCamposReporte(int idReporte, bool mostrarTodos, bool mostrarPersonalizados)
        {
            var perfil = _repPerfilEmpresa.Table.FirstOrDefault(x =>
                x.IdEmpresa == InfoSesion.Info.IdEmpresa);

            var queryUnion1 = _repReportes.Table.ToList()
                .Join(_repCamposContext.Table,
                    rep => rep.IdContexto,
                    cc => cc.IdContexto,
                    (rep, cc) => new { rep, cc })
                .GroupJoin(_repCamposReporte.Table,
                    tmp => new { tmp.rep.IdReporte, tmp.cc.IdCampo, IdOrigen = (int)EnumLOrigen.Fisico },
                    cr => new { cr.IdReporte, cr.IdCampo, cr.IdOrigen },
                    (tmp, cr) => new { tmp.rep, tmp.cc, cr })
                .Where(tmp =>
                    tmp.rep.IdReporte == idReporte &&
                    (mostrarTodos || (!mostrarTodos && tmp.cr.Any())))
                .SelectMany(item => item.cr.DefaultIfEmpty(),
                (item, cr) => new
                {
                    IdOrigen = (int)EnumLOrigen.Fisico,
                    Campo = item.cc.Campo,
                    Descripcion = item.cc.Descripcion,
                    IdTipo = item.cc.IdTipo,
                    IdCampo = item.cc.IdCampo,
                    LongitudMax = null as int?,
                    ValorDefecto = null as string,
                    CampoRelacionado = item.cc.CampoRelacionado,
                    Lista = null as List<PocParametroEmpresaLista>,
                    cr
                });
            if (mostrarPersonalizados)
            {
                var queryUnion2 = _repReportes.Table.ToList()
                     .Join(_repParametroEmpresaContexto.Table,
                         rep => rep.IdContexto,
                         pec => pec.IdContexto,
                         (rep, pec) => new { rep, pec })
                     .Join(_repParametroEmpresa.Table,
                         tmp => tmp.pec.IdParametro,
                         pe => pe.IdParametro,
                         (tmp, pe) => new { tmp.rep, tmp.pec, pe })
                     .GroupJoin(_repParametroEmpresaLista.Table,
                         tmp => tmp.pe.IdParametro,
                         lista => lista.IdParametroEmpresa,
                         (tmp, lista) => new { tmp.rep, tmp.pec, tmp.pe, lista })
                     .GroupJoin(_repCamposReporte.Table,
                         tmp => new { tmp.rep.IdReporte, IdCampo = tmp.pec.IdParametro, IdOrigen = (int)EnumLOrigen.Personalizado },
                         cr => new { cr.IdReporte, cr.IdCampo, cr.IdOrigen },
                         (tmp, cr) => new { tmp.rep, tmp.pec, tmp.pe, tmp.lista, cr })
                     .Where(tmp =>
                         tmp.rep.IdReporte == idReporte &&
                         tmp.pe.IdEmpresa == InfoSesion.Info.IdEmpresa &&
                         (mostrarTodos || (!mostrarTodos && tmp.cr.Any())))
                     .SelectMany(item => item.cr.DefaultIfEmpty(),
                     (item, cr) => new
                     {
                         IdOrigen = (int)EnumLOrigen.Personalizado,
                         Campo = item.pe.Nombre,
                         Descripcion = item.pe.Nombre,
                         IdTipo = item.pe.IdTipo,
                         IdCampo = item.pe.IdParametro,
                         LongitudMax = item.pec.LongitudMax,
                         ValorDefecto = item.pec.IdEsRequerido == (int)EnumIdSiNo.Si ||
                             item.pec.IdEstado == (int)EnumIdEstado.Inactivo ?
                             item.pec.ValorDefecto : null as string,
                         CampoRelacionado = null as string,
                         Lista = item.lista != null ? OrdenarParamLista(item.pe, item.lista) : null,
                         cr
                     });
                queryUnion1 = queryUnion1.Concat(queryUnion2);
            }

            var resultado = queryUnion1.Select(item => new PocCampoReporteContexto
            {
                Campo = item.Campo,
                Descripcion = item.Descripcion,
                Lista = item.Lista,
                IdTipo = item.IdTipo,
                IdCampo = item.IdCampo,
                IdOrigen = item.IdOrigen,
                EsValido = item.cr != null,
                LongitudMax = item.LongitudMax,
                ValorDefecto = item.ValorDefecto,
                CampoRelacionado = item.CampoRelacionado,
                Orden = item.cr != null ? item.cr.Orden : 0,
                Formato = ObtenerFormatoParam(item.IdTipo, perfil),
                ValorContrapartida = item.cr != null ? item.cr.ValorContrapartida : null,
                IdSeparador = item.cr != null && item.cr.IdSeparador == (int)EnumIdSiNo.Si,
                SumaContrapartida = item.cr != null && item.cr.IdSumaContrapartida == (int)EnumIdSiNo.Si,
                AgrupaContrapartida = item.cr != null && item.cr.IdAgrupaContrapartida == (int)EnumIdSiNo.Si,
                Alias = item.cr != null ? item.cr.Alias : string.Empty
            }).ToList();
            if (mostrarTodos)
            {
                return resultado.OrderByDescending(m => m.EsValido)
                    .ThenBy(m => m.Orden).ThenBy(m => m.IdOrigen);
            }
            resultado.Update(item =>
            {
                var _type = item.IdTipo;
                string _value = item.ValorContrapartida;
                if (!string.IsNullOrEmpty(_value) && !_value.Contains("@@"))
                {
                    switch (_type)
                    {
                        case (int)EnumTipoParamEmpresa.Fecha:
                            item.ValorContrapartida = _value.ToDate();
                            break;
                        case (int)EnumTipoParamEmpresa.Cantidad:
                            item.ValorContrapartida = long.Parse(_value, NumberStyles.Any);
                            break;
                        case (int)EnumTipoParamEmpresa.Monto:
                            item.ValorContrapartida = double.Parse(_value, NumberStyles.Any);
                            break;
                    }
                }
            });
            return resultado.OrderBy(m => m.Orden);
        }

        public void GuardarCamposReporte(PocReporteGeneral reporte)
        {
            using (var ts = new TransactionScope())
            {
                var listaExistente = _repCamposReporte.Table
                    .FirstOrDefault(x => x.IdReporte == reporte.IdReporte);
                if (listaExistente != null)
                {
                    _repCamposReporte.DeleteOn(x => x.IdReporte == reporte.IdReporte);
                }
                if (reporte.Campos != null && reporte.Campos.Any())
                {
                    if (reporte != null && reporte.IdReporte > 0)
                    {
                        var idIncluyeEncabTxt = reporte.IdIncluyeEncabTxt ?
                            (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
                        var idIncluyeContrapartida = reporte.IdIncluyeContrapartida ?
                            (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
                        _repReportes.UpdateOn(x => x.IdReporte == reporte.IdReporte,
                            z => new GL_Reportes
                            {
                                IdIncluyeEncabTxt = idIncluyeEncabTxt,
                                IdIncluyeContrapartida = idIncluyeContrapartida,
                                TipoMovimiento = reporte.TipoMovimiento
                            });
                    }
                    foreach (var item in reporte.Campos)
                    {
                        var camposReport = new GL_CamposReporte();
                        camposReport.IdCampoReporte = (int)_fGenerales.
                            ObtieneSgteSecuencia("SEQ_IdCampoReporte");
                        camposReport.IdCampo = item.IdCampo;
                        camposReport.IdOrigen = item.IdOrigen;
                        camposReport.Orden = item.Orden;
                        camposReport.IdReporte = reporte.IdReporte;
                        camposReport.IdSeparador = item.IdSeparador ?
                            (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
                        camposReport.IdSumaContrapartida = item.SumaContrapartida ?
                            (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
                        camposReport.IdAgrupaContrapartida = item.AgrupaContrapartida ?
                            (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
                        var contrapartida = item.ValorContrapartida as string;
                        camposReport.ValorContrapartida = !string.IsNullOrEmpty(contrapartida) ?
                            contrapartida as string : null;
                        camposReport.Alias = item.Alias;
                        _repCamposReporte.Insert(camposReport);
                        if (item.Lista != null)
                        {
                            foreach (var counter in item.Lista)
                            {
                                ActualizarParametroLista(counter);
                            }
                        }
                    }
                }
                _repCamposReporte.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        //public List<PocParametroEmpresa> ListaParametrosEmpresa()
        //{
        //    return _repParametroEmpresa.Table
        //        .Where(x => x.IdEmpresa == InfoSesion.Info.IdEmpresa && x.IdEstado == (int)EnumIdEstado.Activo)
        //        .Select(item => new PocParametroEmpresa
        //        {
        //            IdParametro = item.IdParametro,
        //            Nombre = item.Nombre,
        //            Descripcion = item.Descripcion,
        //            IdTipo = item.IdTipo
        //        }).ToList();
        //}

        public int? IncluirReporte(PocReporteBase reporte)
        {
            if (reporte.Actualizar)
            {
                var idIncluyeEncabTxt = reporte.IdIncluyeEncabTxt ?
                  (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
                var idIncluyeContrapartida = reporte.IdIncluyeContrapartida ?
                    (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No;
                var actual = ObtenerReporte(reporte.IdReporte);
                if (actual.IdContexto != reporte.IdContexto)
                {
                    _repCamposReporte.DeleteOn(x => x.IdReporte == reporte.IdReporte);
                }
                _repReportes.UpdateOn(x => x.IdReporte == reporte.IdReporte,
                    z => new GL_Reportes
                    {
                        IdIncluyeEncabTxt = idIncluyeEncabTxt,
                        Descripcion = reporte.Descripcion,
                        IdContexto = reporte.IdContexto,
                        IdIncluyeContrapartida = idIncluyeContrapartida,
                        IdTipoReporte = (int)EnumTipoReporte.Archivo
                    });
                _repReportes.UnidadTbjo.Save();
                return reporte.IdReporte;
            }
            else
            {
                using (var ts = new TransactionScope())
                {
                    var glreporte = reporte.ProyectarComo<GL_Reportes>();
                    glreporte.IdReporte = _fGenerales.ObtieneSgteSecuencia("SEQ_IdReporte");
                    glreporte.IdEmpresa = InfoSesion.Info.IdEmpresa;
                    glreporte.IdTipoReporte = (int)EnumTipoReporte.Archivo;
                    _repReportes.Insert(glreporte);
                    _repReportes.UnidadTbjo.Save();
                    ts.Complete();
                    return glreporte.IdReporte;
                }
            }
        }

        public PocReporteBase ObtenerReporte(int idReporte)
        {
            var reporte = _repReportes.Table.FirstOrDefault
                (x => x.IdReporte == idReporte);
            var resultado = new PocReporteBase();
            resultado.IdContexto = reporte.IdContexto;
            resultado.IdEmpresa = reporte.IdEmpresa;
            resultado.IdReporte = reporte.IdReporte;
            resultado.TipoMovimiento = reporte.TipoMovimiento;
            resultado.IdIncluyeEncabTxt = reporte.IdIncluyeEncabTxt == (int)EnumIdSiNo.Si;
            resultado.IdIncluyeContrapartida = reporte.IdIncluyeContrapartida == (int)EnumIdSiNo.Si;
            return resultado;
        }

        private string ObtenerFormatoParam(int tipo, CL_PerfilEmpresa perfil)
        {
            string formato = null;
            if (tipo == (int)EnumTipoParamEmpresa.Fecha)
            {
                formato = perfil != null ? perfil.FormatoFecha : "";
            }
            else if (tipo == (int)EnumTipoParamEmpresa.Monto)
            {
                var sepDecimal = perfil != null ? perfil.SepDecimal :
                    Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                var numDecimales = perfil != null ? perfil.NumDecimales :
                    Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalDigits;
                formato = string.Format("0{0}{1}", sepDecimal, "".PadLeft(numDecimales, '0'));
            }
            return formato;
        }

        public List<PocParametroEmpresaLista> OrdenarParamLista(GL_ParametroEmpresa param,
            IEnumerable<GL_ParametroEmpresaLista> paramlista)
        {
            if (param.IdTipo == (int)EnumTipoParamEmpresa.ListaPredefinida)
            {
                if (param.IdParametroInterno.HasValue)
                {
                    var datosPB = _fGenerales.ObtenerListaPredefinida(param.IdParametroInterno.Value);
                    paramlista = datosPB.ProyectarComo<GL_ParametroEmpresaLista>();
                }
            }
            else if (!param.IdDependeDe.HasValue && paramlista != null && paramlista.Any(m => m != null))
            {
                paramlista.Where(x => x.CodigoDependencia != null)
                    .Update(x => x.CodigoDependencia = null);
            }
            if (paramlista != null && paramlista.Any(m => m != null))
            {
                var _resultado = paramlista.ProyectarComo<PocParametroEmpresaLista>();
                _resultado = _resultado.Sort(x =>
                    param.IdOrdenadoPor == (int)EnumTipoOrdenLista.Codigo ?
                        x.Codigo : x.Descripcion,
                    param.Ascendente == (int)EnumIdSiNo.Si).ToList();
                _resultado.Update(u => u.CodigoAnterior = u.Codigo);
                return _resultado;
            }
            return null;
        }

        public void ActualizarIdReporteTC(int idReporte)
        {
            using (var ts = new TransactionScope())
            {
                _repPerfilEmpresa.UpdateOn(x => x.IdEmpresa == InfoSesion.Info.IdEmpresa, z => new CL_PerfilEmpresa
                {
                    IdRepMovTC = idReporte
                });
                _repPerfilEmpresa.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public List<PocParametroEmpresaLista> ObtenerParametroEmpresaLista(int idParametroEmpesa)
        {
            var result = _repParametroEmpresa.Table
                .GroupJoin(_repParametroEmpresaLista.Table,
                    p => p.IdParametro,
                    l => l.IdParametroEmpresa,
                    (param, lista) => new { param, lista })
                .FirstOrDefault(x =>
                    x.param.IdParametro == idParametroEmpesa &&
                    x.param.IdEmpresa == InfoSesion.Info.IdEmpresa);
            if (result != null)
            {
                return OrdenarParamLista(result.param, result.lista);
            }
            return null;
        }

        public List<PocParametroEmpresaLista> ObtenerListaPredefinida(int idParametroInterno)
        {
            var paramEmpresa = _repParametroEmpresa.Table.FirstOrDefault(x =>
                x.IdParametroInterno == idParametroInterno && x.IdEmpresa == InfoSesion.Info.IdEmpresa);
            if (paramEmpresa != null)
            {
                return OrdenarParamLista(paramEmpresa, null);
            }
            var listaPre = _fGenerales.ObtenerListaPredefinida(idParametroInterno);
            var resultado = listaPre.ProyectarComo<GL_ParametroEmpresaLista>();
            return resultado.ProyectarComo<PocParametroEmpresaLista>();
        }

        public IEnumerable<PocParametroEmpresa> ObtenerPosiblesDependencias(PocFiltroParametroEmpresa filtro,
            int idParametro)
        {
            var resultado = this.ObtenerParametros(filtro);
            var listaDependientes = ObtenerParametrosDependientes(idParametro);
            resultado = resultado.Where(x => !listaDependientes.Contains(x.IdParametro));
            return resultado;
        }

        private IEnumerable<int> ObtenerParametrosDependientes(int? idParametro)
        {
            if (idParametro.HasValue)
            {
                var datLista = _repParametroEmpresa.Table
                    .Where(x => x.IdDependeDe == idParametro &&
                        x.IdEmpresa == InfoSesion.Info.IdEmpresa)
                    .Select(s => new
                    {
                        IdParametro = s.IdParametro
                    })
                    .FirstOrDefault();
                var listaAnterior = new List<int> { idParametro.Value };
                if (datLista != null)
                {
                    var result = ObtenerParametrosDependientes(datLista.IdParametro);
                    listaAnterior.InsertRange(0, result);
                }
                return listaAnterior;
            }
            return null;
        }
    }
}

using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Logs
{
    public partial class LG_LogError : EntidadBase
    {
        public int IdError { get; set; }
        public int IdTipoError { get; set; }
        public Nullable<System.DateTime> FecError { get; set; }
        public string Usuario { get; set; }
        public string DescError { get; set; }
        public string Capa { get; set; }
        public string NombreObjeto { get; set; }
        public Nullable<int> IdSistema { get; set; }
        public System.DateTime HoraError { get; set; }
    }
}

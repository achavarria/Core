﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Configuracion
{
    public interface IAppPrivilegios : IAplicacionBase
    {
        IEnumerable<PocPrivilegiosCuenta> ObtenerPrivilegiosCuenta();
        void GuardarAgrupacionesCuenta(List<PocAdmAgrupacion> adms, int[] admPrevios, int idCuenta);
        void EliminarPrivilegio(PocPrivilegiosCuenta datosEntrada);
        void ValidarCuentasGrupo(int idGrupo);
    }
}

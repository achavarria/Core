﻿using Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador;
using Monibyte.Arquitectura.Dominio.Autorizador;
using System.Data.Entity;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte
{
    public partial class DbContextCtrlMonibyte : DbContextBase
    {
        public DbSet<MB_CategoriasEmpresa> MB_CategoriasEmpresa { get; set; }
        public DbSet<MB_CondicionTarjeta> MB_CondicionTarjeta { get; set; }
        public DbSet<MB_Emisor> MB_Emisor { get; set; }
        public DbSet<MB_HorarioTarjeta> MB_HorarioTarjeta { get; set; }
        public DbSet<MB_MccTarjeta> MB_MccTarjeta { get; set; }
        public DbSet<MB_Moneda> MB_Moneda { get; set; }
        public DbSet<MB_Pais> MB_Pais { get; set; }
        public DbSet<MB_PaisesRegion> MB_PaisesRegion { get; set; }
        public DbSet<MB_RangoCategoriaTipoComercio> MB_RangoCategoriaTipoComercio { get; set; }
        public DbSet<MB_Region> MB_Region { get; set; }
        public DbSet<MB_RegionPaisTarjeta> MB_RegionPaisTarjeta { get; set; }
        public DbSet<MB_TipoTarjeta> MB_TipoTarjeta { get; set; }

        void CreateContextModelCtrlMB(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MB_CategoriasEmpresaMap());
            modelBuilder.Configurations.Add(new MB_CondicionTarjetaMap());
            modelBuilder.Configurations.Add(new MB_EmisorMap());
            modelBuilder.Configurations.Add(new MB_HorarioTarjetaMap());
            modelBuilder.Configurations.Add(new MB_MccTarjetaMap());
            modelBuilder.Configurations.Add(new MB_MonedaMap());
            modelBuilder.Configurations.Add(new MB_PaisMap());
            modelBuilder.Configurations.Add(new MB_PaisesRegionMap());
            modelBuilder.Configurations.Add(new MB_RangoCategoriaTipoComercioMap());
            modelBuilder.Configurations.Add(new MB_RegionMap());
            modelBuilder.Configurations.Add(new MB_RegionPaisTarjetaMap());
            modelBuilder.Configurations.Add(new MB_TipoTarjetaMap());
        }
    }
}

using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Quickpass
{
    public partial class QP_Quickpass : EntidadBase
    {
        public int IdQuickpass { get; set; }
        public Nullable<int> IdTipoQuickpass { get; set; }
        public string NumQuickpass { get; set; }
        public string NumDocumento { get; set; }
        public Nullable<System.DateTime> FecVencimiento { get; set; }
        public Nullable<int> IdPersona { get; set; }
        public Nullable<int> IdTarjeta { get; set; }
        public Nullable<int> IdCuenta { get; set; }
        public string NumPlaca { get; set; }
        public int IdEstado { get; set; }
        public Nullable<int> IdEstadoOperativo { get; set; }
        public Nullable<System.DateTime> FecRevisionCostoAdm { get; set; }
        public Nullable<int> IdFinanciado { get; set; }
        public Nullable<decimal> SaldoDispositivo { get; set; }
        public Nullable<int> IdPrepago { get; set; }
        public Nullable<decimal> MonCostoAdm { get; set; }
        public Nullable<decimal> MonCargoInicial { get; set; }
        public Nullable<decimal> MonMinRecarga { get; set; }
        public Nullable<decimal> MonCostoUnidad { get; set; }
        public Nullable<int> IdMonedaAdm { get; set; }
        public Nullable<int> IdMonedaPeaje { get; set; }
        public Nullable<int> IdCargoDiario { get; set; }
        public Nullable<decimal> SaldoPeaje { get; set; }
        public Nullable<decimal> MonMinListaNegra { get; set; }
        public Nullable<int> MesesFinanciamiento { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public Nullable<int> IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

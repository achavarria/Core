﻿using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public interface IReporteadorDinamico
    {
        List<Cplx_GL_CamposReporte> ObtenerCamposReporte(int idReporte);
        Reporte CrearReporte(ReportConfig config, List<ReportDataSet> dataSets);
    }
}

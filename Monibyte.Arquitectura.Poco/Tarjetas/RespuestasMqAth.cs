﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public static class RespuestasMqAth
    {
        public static readonly string TRANSACCION_EXITOSA = "00";
        public static readonly string TARJETA_NO_EXISTE = "01";
        public static readonly string TARJETA_INVALIDA = "02";
        public static readonly string ESTADO_TARJETA_INVALIDO = "03";
        public static readonly string MONEDA_NO_VALIDA = "04";
        public static readonly string CODIGO_TRANSACCION_INCORRECTO = "14";
        public static readonly string MONTO_MENOR_O_IGUAL_A_CERO = "15";
        public static readonly string SUPERFRANQUICIA_INCORRECTA = "16";
        public static readonly string CUENTA_NO_EXISTE = "22";
        public static readonly string LOCALIDAD_NO_EXISTE = "25";
        public static readonly string LOCALIDAD_INCORRECTA = "26";
        public static readonly string USUARIO_NO_EXISTE = "27";
        public static readonly string LOCALIDAD_DEL_USUARIO_NO_EXISTE = "28";
        public static readonly string SUPERFRANQUICIA_DEL_USUARIO_INCORRECTA = "29";
        public static readonly string FECHA_NO_PUEDE_SER_BLANCOS = "32";
        public static readonly string FECHA_NO_VALIDA = "33";
        public static readonly string FECHA_NO_PUEDE_SER_MAYOR_A_HOY = "34";
        public static readonly string TARJETA_NO_ES_TITULAR = "49";
        public static readonly string TARJETA_NO_ES_ADICIONAL = "A1";
        public static readonly string NO_EXISTE_PARAMETROS_DE_RECARGA_X_EMISOR = "B0";
        public static readonly string MONTO_MENOR_AL_MINIMO_DE_RECARGA_X_TRANS = "B1";
        public static readonly string EXCEDE_MONTO_MAXIMO_DE_RECARGA_X_TRANSAC = "B2";
        public static readonly string EXCEDE_CANTIDAD_TOTAL_DE_RECARGAS = "B3";
        public static readonly string EXCEDE_CANT_TOTAL_DE_RECARGAS_POR_DIA = "B4";
        public static readonly string EXCEDE_MONTO_TOTAL_DE_RECARGAS_X_DIA = "B5";
        public static readonly string CUENTA_NO_TIENE_DISPONIBLE_PARA_RECARGA = "B6";
        public static readonly string CUENTA_SOBREGIRADA_O_PUEDE_SOBREGIRARSE = "B7";
        public static readonly string CTA_NO_TIENE_DISPONIBLE_PARA_REV_RECARGA = "B8";
        public static readonly string NO_EXISTE_MOV_DE_RECARGA_PARA_REVERSION = "B9";
        public static readonly string NO_EXISTE_MONEDA_PARA_MOVIMIENTO_DESEADO = "C0";
        public static readonly string NO_EXISTE_TIPO_DE_MOVIMIENTO = "C1";
        public static readonly string TARJETA_INACTIVA_PARA_MOV_DE_RECARGA = "C2";
        public static readonly string NO_EXISTE_PARAM_RECARGA_X_EMISOR_TCLTE = "C3";
        public static readonly string NUMTARJETA_DISTINTA_AL_MOV_RECARGA = "C4";
        public static readonly string TARJETA_INACTIVA_PARA_ESTE_MOVIMIENTO = "C5";
        public static readonly string MOVIMIENTO_NO_PERMITIDO = "D0";
        public static readonly string MOVIMIENTO_NO_PERMITIDO_NO_ES_DoC = "D1";
        public static readonly string COD_MOV_NO_PERMITIDO_PARA_EL_USUARIO = "D2";
        public static readonly string MOV_DE_EXTRAFINANCIAMIENTO_NO_PERMITIDO = "D3";
        public static readonly string MOV_DE_COBRO_JUDICIAL_NO_PERMITIDO = "D4";
        public static readonly string CUENTA_EN_CARTERA_X = "D5";
        public static readonly string USUARIO_NO_AUTORIZADO_PARA_ESTE_EMISOR = "D6";
        public static readonly string EMISOR_NO_EXISTE = "D7";
        public static readonly string CODIGO_DE_MOVIMIENTO_EN_BLANCO = "D8";
        public static readonly string TARJETA_EN_BLANCO = "D9";
        public static readonly string REFERENCIA_EN_BLANCO = "E0";
        public static readonly string NUMERO_DE_DOCUMENTO_NO_EXISTE = "E1";
    }
}

﻿namespace Monibyte.Arquitectura.Dominio.Notificacion.Repositorios
{
    public interface IRepositorioPlantillaNotificacion : IRepositorioMnb<NT_PlantillaNotificacion>
    {
        NT_PlantillaNotificacion ObtenerPlantillaNot(int idPlantilla);
        NT_PlantillaNotificacion ObtenerPlantillaNot(int idContexto, int idOrigen);
        NT_PlantillaNotificacion ObtenerPlantillaNot(int idContexto, int idOrigen, 
            int idIdioma, string subContexto = null);
    }
}

using Monibyte.Arquitectura.Dominio.Clientes;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Distrito : EntidadBase
    {
        public int IdDistrito { get; set; }
        public Nullable<int> IdCanton { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

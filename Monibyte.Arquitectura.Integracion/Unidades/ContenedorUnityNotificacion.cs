﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Notificacion;
    using Monibyte.Arquitectura.Datos.Notificacion.Repositorios;
    using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;

    public class ContenedorUnityNotificacion
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            _unityContainer.RegisterType<IAppNotificacion, AppNotificacion>();
            #endregion

            #region Repositorios
            _unityContainer.RegisterType<IRepositorioEmail, RepositorioEmail>(
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(new object[] { "smtp-monibyte-cesand", true }));
            _unityContainer.RegisterType<IRepositorioPlantillaNotificacion, RepositorioPlantillaNotificacion>();
            _unityContainer.RegisterType<IRepositorioNotificacion, RepositorioNotificacion>();
            #endregion
        }
    }
}

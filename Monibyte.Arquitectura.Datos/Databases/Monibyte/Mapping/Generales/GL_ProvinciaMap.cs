using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ProvinciaMap : EntityTypeConfiguration<GL_Provincia>
    {
        public GL_ProvinciaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdProvincia);

            // Properties
            this.Property(t => t.IdProvincia)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .HasMaxLength(100);

            this.Property(t => t.Codigo)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("GL_Provincia", "General");
            this.Property(t => t.IdProvincia).HasColumnName("IdProvincia");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.Codigo).HasColumnName("Codigo");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

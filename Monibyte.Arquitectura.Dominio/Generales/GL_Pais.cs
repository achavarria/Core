using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Configuracion;
using Monibyte.Arquitectura.Dominio.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Pais : EntidadBase
    {
        public int IdPais { get; set; }
        public string Descripcion { get; set; }
        public string Nacionalidad { get; set; }
        public string CodISO { get; set; }
        public string CodISOAlpha2 { get; set; }
        public string CodISOAlpha3 { get; set; }
        public Nullable<int> IdMoneda { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

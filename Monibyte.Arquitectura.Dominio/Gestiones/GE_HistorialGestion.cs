using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Gestiones
{
    public partial class GE_HistorialGestion : EntidadBase
    {
        public int Id { get; set; }
        public int IdGestion { get; set; }
        public int IdEstado { get; set; }
        public Nullable<int> IdUsuario { get; set; }
        public System.DateTime FecEstado { get; set; }
        public System.DateTime HoraEstado { get; set; }
        public string Detalle { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

using Monibyte.Arquitectura.Dominio.Autorizador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador
{
    public class MB_PaisesRegionMap : EntityTypeConfiguration<MB_PaisesRegion>
    {
        public MB_PaisesRegionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPaisRegion);

            // Properties
            this.Property(t => t.IdPaisRegion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodPaisISO)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("MB_PaisesRegion", "Mnb");
            this.Property(t => t.IdPaisRegion).HasColumnName("IdPaisRegion");
            this.Property(t => t.IdRegion).HasColumnName("IdRegion");
            this.Property(t => t.CodPaisISO).HasColumnName("CodPaisISO");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");


        }
    }
}

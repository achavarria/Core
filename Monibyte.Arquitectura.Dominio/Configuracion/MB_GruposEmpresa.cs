using Monibyte.Arquitectura.Dominio.Clientes;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Configuracion
{
    public partial class MB_GruposEmpresa : EntidadBase
    {
        public int IdGrupo { get; set; }
        public int IdEmpresa { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<decimal> LimiteCredito { get; set; }
        public int IdTipoGrupo { get; set; }
    }
}

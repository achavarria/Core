﻿using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios;
using Monibyte.Arquitectura.Dominio.Tarjetas.SatGeo;
using System;

namespace Monibyte.Arquitectura.Datos.Tarjetas.Repositorios
{
    public class ServiciosSatGeo : IServiciosSatGeo
    {
        public SatGeoOut ObtieneInfoMovimiento(SatgeoIn entrada)
        {
            try
            {
                var datos = new SatGeo.FuelPaymentControlIn
                {
                    TransactionID = entrada.TransactionID,
                    CardNumber = entrada.CardNumber,
                    CurrencyType = entrada.CurrencyType,
                    Date = entrada.Date,
                    Detail = entrada.Detail,
                    NameCardHolder = entrada.NameCardHolder,
                    Plate = entrada.Plate,
                    Value = entrada.Value,
                    IsTransaction = true
                };
                var client = new SatGeo.FuelPaymentControlClient();
                var expect100 = System.Net.ServicePointManager.Expect100Continue;
                System.Net.ServicePointManager.Expect100Continue = false;
                var resultado = client.SendInfoFuelPayment(datos);
                System.Net.ServicePointManager.Expect100Continue = expect100;
                if (resultado.Data == null)
                {
                    Logger.Log(string.Format("{0}: {1} / JsonData: {2}",
                        resultado.Err.Code, resultado.Err.Desc, datos.ToJson()));
                    return new SatGeoOut
                    {
                        CodError = resultado.Err.Code,
                        Mensaje = resultado.Err.Desc
                    };
                }
                return new SatGeoOut
                {
                    AmountFuel = resultado.Data.AmountFuel,
                    DateLocation = resultado.Data.DateLocation,
                    Distance = resultado.Data.Distance,
                    DriverName = resultado.Data.DriverName,
                    Latitude = resultado.Data.Latitude,
                    Location = resultado.Data.Location,
                    Longitude = resultado.Data.Longitude,
                    Plate = resultado.Data.Plate,
                    isSensorFuel = resultado.Data.isSensorFuel,
                    AmountFuelRecope = resultado.Data.AmountFuelRecope,
                    PriceFuelRecope = resultado.Data.PriceFuelRecope,
                    Mensaje = null
                };
            }
            catch (Exception ex)
            {
                return new SatGeoOut
                {
                    Mensaje = ex.FullMessage()
                };
            }
        }
    }
}

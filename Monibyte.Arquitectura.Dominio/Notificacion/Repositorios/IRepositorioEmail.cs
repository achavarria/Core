﻿using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;

namespace Monibyte.Arquitectura.Dominio.Notificacion.Repositorios
{
    public interface IRepositorioEmail
    {
        void EnviarInfoEmail(EmailConfig config);
        void EnviarEmail(SenderConfig sender, EmailConfig config);
        void EnviarEmailSync(SenderConfig sender, EmailConfig config);
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Quickpass;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Quickpass;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Quickpass
{
    public class AppTipoQuickpass : AplicacionBase, IAppTipoQuickpass
    {
        private IRepositorioMoneda _repMoneda;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<QP_TipoQuickpass> _repTipoQuickpass;

        public AppTipoQuickpass(
            IRepositorioMoneda repMoneda,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<QP_TipoQuickpass> repTipoQuickpass)
        {
            _repMoneda = repMoneda;
            _repCatalogo = repCatalogo;
            _repTipoQuickpass = repTipoQuickpass;
        }

        public IEnumerable<PocTipoQuickpass> ListaTipoQuickpass()
        {
            var parametros = _repTipoQuickpass.Table.ToList();
            return parametros.ProyectarComo<PocTipoQuickpass>();
        }

        public void Eliminar(PocTipoQuickpass registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<QP_TipoQuickpass>();
                _repTipoQuickpass.Delete(entrada);
                _repTipoQuickpass.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocTipoQuickpass registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<QP_TipoQuickpass>();
                    entrada.IdTipoQuickpass = _fGenerales.ObtieneSgteSecuencia("SEQ_IdTipoQuickpass");
                    _repTipoQuickpass.Insert(entrada);
                    _repTipoQuickpass.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocTipoQuickpass));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocTipoQuickpass registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<QP_TipoQuickpass>();
                    _repTipoQuickpass.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repTipoQuickpass.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocTipoQuickpass));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocTipoQuickpass> Listar(PocTipoQuickpass filtro, DataRequest request)
        {
            var criteria = new Criteria<QP_TipoQuickpass>();
            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.Descripcion))
                {
                    criteria.And(m => m.Descripcion.Contains(filtro.Descripcion));
                }
            }

            var resultado =
                from x in _repTipoQuickpass.SelectBy(criteria)
                join pre in _repCatalogo.List("LSINO")
                    on x.IdPrepago equals pre.IdCatalogo
                join monPe in _repMoneda.Table
                    on x.IdMonedaPeaje equals monPe.IdMoneda
                join monAd in _repMoneda.Table
                    on x.IdMonedaAdm equals monAd.IdMoneda
                join carg in _repCatalogo.List("LSINO")
                    on x.IdCargoDiario equals carg.IdCatalogo
                join est in _repCatalogo.List("LACTINACTIVA")
                    on x.IdEstado equals est.IdCatalogo
                select new PocTipoQuickpass
                {
                    IdTipoQuickpass = x.IdTipoQuickpass,
                    Descripcion = x.Descripcion,
                    IdPrepago = x.IdPrepago,
                    MonCargaInicial = x.MonCargaInicial,
                    MonMinRecargar = x.MonMinRecargar,
                    MonMinListaNegra = x.MonMinListaNegra,
                    IdMonedaPeaje = x.IdMonedaPeaje,
                    IdCargoDiario = x.IdCargoDiario,
                    MonCostoAdm = x.MonCostoAdm,
                    MonCostoUnidad = x.MonCostoUnidad,
                    MesesVigencia = x.MesesVigencia,
                    IdMonedaAdm = x.IdMonedaAdm,
                    IdEstado = x.IdEstado,
                    Prepago = pre == null ? "" : pre.Descripcion,
                    MonedaAdm = monAd == null ? "" : monAd.Descripcion,
                    MonedaPeaje = monPe == null ? "" : monPe.Descripcion,
                    CargoDiario = carg == null ? "" : carg.Descripcion,
                    Estado = est == null ? "" : est.Descripcion,
                    MesesFinanciamiento = x.MesesFinanciamiento

                };
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocTipoQuickpass registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.MonCargaInicial < 0) { validacion.AddError("Err_0035"); }
            if (registro.MonMinRecargar < 0) { validacion.AddError("Err_0035"); }
            if (registro.MonMinListaNegra < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdCargoDiario < 0) { validacion.AddError("Err_0035"); }
            if (registro.MonCostoAdm < 0) { validacion.AddError("Err_0035"); }
            if (registro.MonCostoUnidad < 0) { validacion.AddError("Err_0035"); }
            if (registro.MesesVigencia < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdMonedaAdm < 0) { validacion.AddError("Err_0035"); }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }
    }
}

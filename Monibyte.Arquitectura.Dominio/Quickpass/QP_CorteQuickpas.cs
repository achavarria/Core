﻿using System;

namespace Monibyte.Arquitectura.Dominio.Quickpass
{
    public partial class QP_CorteQuickpas : EntidadBase
    {
        public int IdQuickpass { get; set; }
        public int? IdTipoQuickpass { get; set; }
        public string NumQuickpass { get; set; }
        public int? IdPersona { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdCuenta { get; set; }
        public string NumPlaca { get; set; }
        public int IdEstado { get; set; }
        public DateTime? FecRevisionCostoAdm { get; set; }
        public int? IdFinanciado { get; set; }
        public decimal? SaldoDispositivo { get; set; }
        public decimal? SaldoPeaje { get; set; }
        public int? IdPrepago { get; set; }
        public decimal? MonCostoAdm { get; set; }
        public decimal? MonCargoInicial { get; set; }
        public decimal? MonMinRecarga { get; set; }
        public decimal? MonCostoUnidad { get; set; }
        public decimal? MonMinListaNegra { get; set; }
        public int? IdMonedaAdm { get; set; }
        public int? IdMonedaPeaje { get; set; }
        public int? IdCargoDiario { get; set; }
        public int? MesesFinanciamiento { get; set; }
        public string DescEstado { get; set; }
        public string DescEstadoOperativo { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreCliente { get; set; }
        public string Financiado { get; set; }
        public string DescPrepago { get; set; }
        public string MonedaAdm { get; set; }
        public string MonedaPeaje { get; set; }
        public string CargoDiario { get; set; }
        public string DescTipoQuickpass { get; set; }
        public string NumCuenta { get; set; }
        public string NumDocumento { get; set; }
        public int DiaCorte { get; set; }
        public int? IdEstadoOperativo { get; set; }
        public DateTime? FecVencimiento { get; set; }
    }

    public partial class QP_MovimientosQuickpass : EntidadBase
    {
        public int IdQuickpass { get; set; }
        public string NumQuickpass { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdEstado { get; set; }
        public int CodMovimiento { get; set; }
        public decimal Monto { get; set; }
        public int IdMoneda { get; set; }
        public int? IdTipoMovimiento { get; set; }
        public DateTime FecConsumo { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreCompleto { get; set; }
    }
}

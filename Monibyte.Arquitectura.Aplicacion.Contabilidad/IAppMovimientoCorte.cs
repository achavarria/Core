﻿using Monibyte.Arquitectura.Dominio;
using System;

namespace Monibyte.Arquitectura.Aplicacion.Contabilidad
{
    public interface IAppMovimientoCorte : IAplicacionBase
    {
        string GenerarArchivoInvoice(DateTime fecMovimientoDesde, DateTime fecMovimientoHasta);
    }
}

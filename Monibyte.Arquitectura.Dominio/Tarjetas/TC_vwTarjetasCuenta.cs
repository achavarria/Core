using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_vwTarjetasCuenta : EntidadBase
    {
        public long Id { get; set; }
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdTipoTarjeta { get; set; }
        public int IdTipoPlastico { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public int IdEstadoCta { get; set; }
        public string CodEstadoCta { get; set; }
        public string CodEstadoPago { get; set; }
        public Nullable<int> IdEstadoTarjeta { get; set; }
        public string CodEstadoTarjeta { get; set; }
        public Nullable<int> IdGrupoEmpresa { get; set; }
        public Nullable<int> IdEmpresa { get; set; }
        public int IdPersona { get; set; }
        public int IdRelacion { get; set; }
        public string CamposDinamicos { get; set; }
        public Nullable<int> IdPersonaTitular { get; set; }
        public string NombreImpreso { get; set; }
        public int IdTipoLimite { get; set; }
        public int IdEsCuentaMadre { get; set; }
        public Nullable<int> IdCuentaMadre { get; set; }
        public System.DateTime FecVencimiento { get; set; }
        public System.DateTime FechaVctoLineaCredito { get; set; }
        public Nullable<System.DateTime> FecUltCorte { get; set; }
        public decimal SaldoLocal { get; set; }
        public decimal SaldoInternacional { get; set; }
        public Nullable<decimal> SaldoTarjetaLocal { get; set; }
        public Nullable<decimal> SaldoTarjetaInter { get; set; }
        public Nullable<decimal> LimiteCreditoCtaGlobal { get; set; }
        public decimal LimiteCreditoCtaLocal { get; set; }
        public decimal LimiteCreditoCtaInter { get; set; }
        public Nullable<decimal> LimiteCreditoTarjetaLocal { get; set; }
        public Nullable<decimal> LimiteCreditoTarjetaInter { get; set; }
        public int IdCompania { get; set; }
        public int IdUnidad { get; set; }
        public Nullable<System.DateTime> FecActivacion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public string NombrePersona { get; set; }
        public string NumIdentificacionPersona { get; set; }
        public string NombreEmpresa { get; set; }
        public string NumIdentificacionEmpresa { get; set; }
        public string CorreoPersonal { get; set; }
        public string CorreoLaboral { get; set; }
        public string CorreoEstCuenta { get; set; }
        public Nullable<int> IdTipoGrupo { get; set; }
        public Nullable<decimal> LimiteCreditoAjustado { get; set; }
    }
}

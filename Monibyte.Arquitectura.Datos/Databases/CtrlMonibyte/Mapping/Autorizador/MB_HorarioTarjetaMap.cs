using Monibyte.Arquitectura.Dominio.Autorizador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador
{
    public class MB_HorarioTarjetaMap : EntityTypeConfiguration<MB_HorarioTarjeta>
    {
        public MB_HorarioTarjetaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdHorario);

            // Properties
            this.Property(t => t.IdHorario)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MB_HorarioTarjeta", "Mnb");
            this.Property(t => t.IdHorario).HasColumnName("IdHorario");
            this.Property(t => t.IdCondicionTarjeta).HasColumnName("IdCondicionTarjeta");
            this.Property(t => t.IdDia).HasColumnName("IdDia");
            this.Property(t => t.IdRestriccion).HasColumnName("IdRestriccion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.HoraDesde).HasColumnName("HoraDesde");
            this.Property(t => t.HoraHasta).HasColumnName("HoraHasta");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");

        }
    }
}

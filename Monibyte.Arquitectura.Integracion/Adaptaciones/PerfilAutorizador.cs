﻿using AutoMapper;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Autorizador;
using Monibyte.Arquitectura.Poco.Configuracion;

namespace Monibyte.Arquitectura.Servicios.Web.AdaptadorPerfiles
{
    class PerfilAutorizador : Profile
    {
        protected override void Configure()
        {
            /***********DEL POCO AL ENTITY***********/
            var pocCondicion_mb = Mapper.CreateMap<PocCondicion, MB_CondicionTarjeta>();
            pocCondicion_mb.ForMember(dto => dto.IdCondicionTarjeta, opt => opt.MapFrom(mod => mod.IdCondicion));
            pocCondicion_mb.ForMember(dto => dto.EsVip, opt =>
                opt.MapFrom(mod => BoolToInt(mod.EsVip, mod.EsVip)));
            pocCondicion_mb.ForMember(dto => dto.SinRestriccion, opt =>
                opt.MapFrom(mod => BoolToInt(mod.SinRestriccion)));
            pocCondicion_mb.ForMember(dto => dto.ConsumeLocal, opt =>
                opt.MapFrom(mod => BoolToInt(mod.ConsumeLocal, mod.RestringeRangoConsumeLocal)));
            pocCondicion_mb.ForMember(dto => dto.ConsumeInternacional, opt =>
                opt.MapFrom(mod => BoolToInt(mod.ConsumeInternacional, mod.RestringeRangoConsumeInternacional)));
            pocCondicion_mb.ForMember(dto => dto.PermiteAvanceEfectivo, opt =>
                opt.MapFrom(mod => mod.PermiteSoloAvanceEfectivo ? (int)EnumEstadoRestriccion.SoloAvance :
                    BoolToInt(mod.PermiteAvanceEfectivo, mod.RestringeRangoPermiteAvanceEfectivo)));
            pocCondicion_mb.ForMember(dto => dto.ConsumeInternet, opt =>
                opt.MapFrom(mod => BoolToInt(mod.ConsumeInternet, mod.RestringeRangoConsumeInternet)));
            pocCondicion_mb.ForMember(dto => dto.RestringeHorario, opt =>
                opt.MapFrom(mod => BoolToInt(mod.RestringeHorario)));
            pocCondicion_mb.ForMember(dto => dto.RestringeMcc, opt =>
                opt.MapFrom(mod => BoolToInt(mod.RestringeMcc)));
            pocCondicion_mb.ForMember(dto => dto.RestringeRegion, opt =>
                opt.MapFrom(mod => BoolToInt(mod.RestringeRegion)));

            //PARA EFECTOS DEL APP
            pocCondicion_mb.ForMember(dto => dto.SinRestriccion, opt =>
               opt.MapFrom(mod => mod.DesactivarRestriccionesPorLapso ? (int)EnumEstadoRestriccion.DesactivacionTemporal :
                   BoolToInt(mod.SinRestriccion)));

            var pocMccCondicion_mb = Mapper.CreateMap<PocMccCondicion, MB_MccTarjeta>();
            pocMccCondicion_mb.ForMember(dto => dto.IdCondicionTarjeta, opt => opt.MapFrom(mod => mod.IdCondicion));
            pocMccCondicion_mb.ForMember(dto => dto.CodMccDesde, opt => opt.MapFrom(mod => mod.CodMcc));
            pocMccCondicion_mb.ForMember(dto => dto.CodMccHasta, opt => opt.MapFrom(mod => mod.CodMcc));
            var pocHorarioCondicion_mb = Mapper.CreateMap<PocHorarioCondicion, MB_HorarioTarjeta>();
            pocHorarioCondicion_mb.ForMember(dto => dto.IdCondicionTarjeta, opt => opt.MapFrom(mod => mod.IdCondicion));
            var pocRegionPaisCondicion_mb = Mapper.CreateMap<PocRegionPaisCondicion, MB_RegionPaisTarjeta>();
            pocRegionPaisCondicion_mb.ForMember(dto => dto.IdCondicionTarjeta, opt => opt.MapFrom(mod => mod.IdCondicion));
            /***********DEL POCO AL ENTITY***********/

            /***********DEL ENTITY AL POCO***********/
            var mb_pocCondicion = Mapper.CreateMap<MB_CondicionTarjeta, PocCondicion>();
            mb_pocCondicion.ForMember(dto => dto.IdCondicion, opt =>
                opt.MapFrom(mod => mod.IdCondicionTarjeta));
            mb_pocCondicion.ForMember(dto => dto.EsVip, opt =>
                opt.MapFrom(mod => mod.EsVip == (int)EnumEstadoRestriccion.ActivoRango));
            //Sin restricciones
            mb_pocCondicion.ForMember(dto => dto.SinRestriccion, opt =>
                opt.MapFrom(mod => mod.SinRestriccion != (int)EnumEstadoRestriccion.Inactivo));
            //Consumos locales
            mb_pocCondicion.ForMember(dto => dto.ConsumeLocal, opt =>
                opt.MapFrom(mod => mod.ConsumeLocal == (int)EnumEstadoRestriccion.Activo ||
                    mod.ConsumeLocal == (int)EnumEstadoRestriccion.ActivoRango));
            mb_pocCondicion.ForMember(dto => dto.RestringeRangoConsumeLocal, opt =>
                opt.MapFrom(mod => mod.ConsumeLocal == (int)EnumEstadoRestriccion.ActivoRango));
            //Consumos internacionales
            mb_pocCondicion.ForMember(dto => dto.ConsumeInternacional, opt =>
                opt.MapFrom(mod => mod.ConsumeInternacional == (int)EnumEstadoRestriccion.Activo ||
                    mod.ConsumeInternacional == (int)EnumEstadoRestriccion.ActivoRango));
            mb_pocCondicion.ForMember(dto => dto.RestringeRangoConsumeInternacional, opt =>
                opt.MapFrom(mod => mod.ConsumeInternacional == (int)EnumEstadoRestriccion.ActivoRango));
            //Avance de efectivo
            mb_pocCondicion.ForMember(dto => dto.PermiteAvanceEfectivo, opt =>
                opt.MapFrom(mod => mod.PermiteAvanceEfectivo == (int)EnumEstadoRestriccion.Activo ||
                    mod.PermiteAvanceEfectivo >= (int)EnumEstadoRestriccion.ActivoRango));
            mb_pocCondicion.ForMember(dto => dto.PermiteSoloAvanceEfectivo, opt =>
                opt.MapFrom(mod => mod.PermiteAvanceEfectivo == (int)EnumEstadoRestriccion.SoloAvance));
            mb_pocCondicion.ForMember(dto => dto.RestringeRangoPermiteAvanceEfectivo, opt =>
                opt.MapFrom(mod => mod.PermiteAvanceEfectivo >= (int)EnumEstadoRestriccion.ActivoRango));
            //Compras por internet
            mb_pocCondicion.ForMember(dto => dto.ConsumeInternet, opt =>
                opt.MapFrom(mod => mod.ConsumeInternet == (int)EnumEstadoRestriccion.Activo ||
                    mod.ConsumeInternet == (int)EnumEstadoRestriccion.ActivoRango));
            mb_pocCondicion.ForMember(dto => dto.RestringeRangoConsumeInternet, opt =>
                opt.MapFrom(mod => mod.ConsumeInternet == (int)EnumEstadoRestriccion.ActivoRango));
            //Sin restricciones de MCC            
            mb_pocCondicion.ForMember(dto => dto.RestringeMcc, opt =>
                opt.MapFrom(mod => mod.RestringeMcc == (int)EnumEstadoRestriccion.Activo));
            //Sin restricciones de horario
            mb_pocCondicion.ForMember(dto => dto.RestringeHorario, opt =>
                opt.MapFrom(mod => mod.RestringeHorario == (int)EnumEstadoRestriccion.Activo));
            //Sin restricciones de región
            mb_pocCondicion.ForMember(dto => dto.RestringeRegion, opt =>
                opt.MapFrom(mod => mod.RestringeRegion == (int)EnumEstadoRestriccion.Activo));


            //PARA EFECTOS DEL APP
            mb_pocCondicion.ForMember(dto => dto.DesactivarRestriccionesPorLapso, opt =>
                opt.MapFrom(mod => mod.SinRestriccion == (int)EnumEstadoRestriccion.DesactivacionTemporal));


            var mb_pocMccCondicion = Mapper.CreateMap<MB_MccTarjeta, PocMccCondicion>();
            mb_pocMccCondicion.ForMember(dto => dto.IdCondicion, opt => opt.MapFrom(mod => mod.IdCondicionTarjeta));
            var mb_pocHorarioCondicion = Mapper.CreateMap<MB_HorarioTarjeta, PocHorarioCondicion>();
            mb_pocHorarioCondicion.ForMember(dto => dto.IdCondicion, opt => opt.MapFrom(mod => mod.IdCondicionTarjeta));
            var mb_pocRegionPaisCondicion = Mapper.CreateMap<MB_RegionPaisTarjeta, PocRegionPaisCondicion>();
            mb_pocRegionPaisCondicion.ForMember(dto => dto.IdCondicion, opt => opt.MapFrom(mod => mod.IdCondicionTarjeta));
            /***********DEL ENTITY AL POCO***********/
        }

        private int BoolToInt(params bool[] values)
        {
            int counter = 0;
            foreach (bool value in values)
            {
                counter += value ? 1 : 0;
            }
            return counter;
        }
    }
}
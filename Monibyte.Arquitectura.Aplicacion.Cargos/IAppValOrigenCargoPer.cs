﻿using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Cargos;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public interface IAppValOrigenCargoPer : IAplicacionBase
    {
        DataResult<PocValOrigenCargoPer> ListarOrigenCargo
            (PocValOrigenCargoPer filtro, DataRequest request);
    }
}

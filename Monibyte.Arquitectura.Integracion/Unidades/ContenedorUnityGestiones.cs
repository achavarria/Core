﻿namespace Monibyte.Arquitectura.Integracion.Unidades
{
    using Microsoft.Practices.Unity;
    using Monibyte.Arquitectura.Aplicacion.Gestiones;
    using Monibyte.Arquitectura.Datos.Gestiones.Repositorios;
    using Monibyte.Arquitectura.Dominio.Gestiones.Repositorios;

    public class ContenedorUnityGestiones
    {
        public static void Configurar(IUnityContainer _unityContainer)
        {
            #region Servicios
            _unityContainer.RegisterType<IAppGestiones, AppGestiones>();
            _unityContainer.RegisterType<IAppGestionesTarjeta, AppGestionesTarjeta>();
            _unityContainer.RegisterType<IAppReportes, AppReportes>();
            _unityContainer.RegisterType<IAppGestionesGenerales, AppGestionesGenerales>();
            _unityContainer.RegisterType<IAppGestionesEfectivo, AppGestionesEfectivo>();
            #endregion

            #region Repositorios
            _unityContainer.RegisterType<IRepositorioGestion, RepositorioGestion>();
            _unityContainer.RegisterType<IRepositorioMovimientosGestion, RepositorioMovimientosGestion>();
            _unityContainer.RegisterType<IFuncionesGestion, FuncionesGestion>();

            #endregion
        }
    }
}

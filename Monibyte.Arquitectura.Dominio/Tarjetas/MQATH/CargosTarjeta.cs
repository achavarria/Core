﻿using System;
using Monibyte.Arquitectura.Comun.Nucleo.Trama;
namespace Monibyte.Arquitectura.Dominio.Tarjetas.MQATH
{  
    [Serializable]
    [TramaSpec(length: 123)]
    public class EntradaCargoAutomaticoTarjeta {
        string _blancos = string.Empty.PadRight(29, ' ');

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        private string TipoMensaje { get { return TipoEntradaMsj.CARGO_AUTOMATICO; } set { value = TipoEntradaMsj.CARGO_AUTOMATICO; } }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 5, characterFill: Constantes.CARACTERBLANCO)]
        public string CodComercio { get; set; }
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERBLANCO)]
        public string CodSucursal { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumDocumento { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40, characterFill: Constantes.CARACTERBLANCO)]
        public string NombreDuenoCargo { get; set; }
        [TramaColumnSpec(required: true, maxLength: 29)]
        private string Blancos { get { return _blancos; } set { value = _blancos; } }
    }

    [Serializable]
    [TramaSpec(length: 1000)]
    public class SalidaCargoAutomaticoTarjeta : RespuestaBase
    {

        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public override string TipoMensaje { get; set; }
        [TramaColumnSpec(required: true, maxLength: 16, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumTarjeta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 10, characterFill: Constantes.CARACTERBLANCO)]
        public string Usuario { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2, characterFill: Constantes.CARACTERBLANCO)]
        public string SuperFranquicia { get; set; }
        [TramaColumnSpec(required: true, maxLength: 5, characterFill: Constantes.CARACTERBLANCO)]
        public string CodComercio { get; set; }
        [TramaColumnSpec(required: true, maxLength: 4, characterFill: Constantes.CARACTERBLANCO)]
        public string CodSucursal { get; set; }
        [TramaColumnSpec(required: true, maxLength: 13, characterFill: Constantes.CARACTERCERO, alignTo: "RIGHT")]
        public string NumDocumento { get; set; }
        [TramaColumnSpec(required: false, maxLength: 40, characterFill: Constantes.CARACTERBLANCO)]
        public string NombreDuenoCargo { get; set; }
        [TramaColumnSpec(required: true, maxLength: 2)]
        public override string CodigoRespuesta { get; set; }
        [TramaColumnSpec(required: true, maxLength: 40)]
        public override string DescripcionCodRespuesta { get; set; }
        [TramaColumnSpec(required: false, maxLength: 864)]
        public string Blancos { get; set; }
    }

}

﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Procesador;
using Monibyte.Arquitectura.Dominio.Procesador;
using System.Data.Entity;
namespace Monibyte.Arquitectura.Datos.Databases.Monibyte
{
    public partial class DbContextMonibyte : DbContextBase
    {
        public DbSet<PT_Autorizaciones> PT_Autorizaciones { get; set; }
        public DbSet<PT_AutorizacionesHist> PT_AutorizacionesHist { get; set; }
        public DbSet<PT_AutorizacionesPBA> PT_AutorizacionesPBA { get; set; }
        public DbSet<PT_InformacionCola> PT_InformacionCola { get; set; }
        public DbSet<PT_InformacionColaHist> PT_InformacionColaHist { get; set; }
        public DbSet<PT_ProspectacionMasiva_PROSPE> PT_ProspectacionMasiva_PROSPE { get; set; }
        public DbSet<PT_AutorizacionesGeo> PT_AutorizacionesGeo { get; set; }

        void CreateContextModelPT(DbModelBuilder modelBuilder) 
        {
            modelBuilder.Configurations.Add(new PT_AutorizacionesMap());
            modelBuilder.Configurations.Add(new PT_AutorizacionesHistMap());
            modelBuilder.Configurations.Add(new PT_AutorizacionesPBAMap());
            modelBuilder.Configurations.Add(new PT_InformacionColaMap());
            modelBuilder.Configurations.Add(new PT_InformacionColaHistMap());
            modelBuilder.Configurations.Add(new PT_ProspectacionMasiva_PROSPEMap());
            modelBuilder.Configurations.Add(new PT_AutorizacionesGeoMap());
        }
    }
}

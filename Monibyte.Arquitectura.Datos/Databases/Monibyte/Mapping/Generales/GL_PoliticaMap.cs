using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_PoliticaMap : EntityTypeConfiguration<GL_Politica>
    {
        public GL_PoliticaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPolitica);

            // Properties
            this.Property(t => t.IdPolitica)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.DetallePolitica)
                .IsRequired()
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("GL_Politica", "General");
            this.Property(t => t.IdPolitica).HasColumnName("IdPolitica");
            this.Property(t => t.IdTipoPolitica).HasColumnName("IdTipoPolitica");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.DetallePolitica).HasColumnName("DetallePolitica");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
        }
    }
}

using Monibyte.Arquitectura.Dominio.Autorizador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador
{
    public class MB_RegionPaisTarjetaMap : EntityTypeConfiguration<MB_RegionPaisTarjeta>
    {
        public MB_RegionPaisTarjetaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdRegionPais);

            // Properties
            this.Property(t => t.IdRegionPais)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodPaisISO)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("MB_RegionPaisTarjeta", "Mnb");
            this.Property(t => t.IdRegionPais).HasColumnName("IdRegionPais");
            this.Property(t => t.IdCondicionTarjeta).HasColumnName("IdCondicionTarjeta");
            this.Property(t => t.CodPaisISO).HasColumnName("CodPaisISO");
            this.Property(t => t.IdRegion).HasColumnName("IdRegion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");


        }
    }
}

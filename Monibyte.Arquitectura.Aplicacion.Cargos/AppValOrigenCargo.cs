﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Poco.Cargos;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public class AppValOrigenCargo : AplicacionBase, IAppValOrigenCargo
    {
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CA_ValOrigenCargo> _repValOrigenCargo;

        public AppValOrigenCargo(
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CA_ValOrigenCargo> repValOrigenCargo)
        {
            _repCatalogo = repCatalogo;
            _repValOrigenCargo = repValOrigenCargo;
        }

        public void Eliminar(PocValOrigenCargo registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CA_ValOrigenCargo>();
                _repValOrigenCargo.Delete(entrada);
                _repValOrigenCargo.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocValOrigenCargo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_ValOrigenCargo>();
                    _repValOrigenCargo.Insert(entrada);
                    _repValOrigenCargo.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocValOrigenCargo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocValOrigenCargo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_ValOrigenCargo>();
                    _repValOrigenCargo.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repValOrigenCargo.UnidadTbjo.Save();
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocValOrigenCargo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocValOrigenCargo> Listar(PocValOrigenCargo filtro, DataRequest request)
        {
            var critcata = new Criteria<GL_Catalogo>();
            var criteria = new Criteria<CA_ValOrigenCargo>();
            if (filtro != null)
            {
                if (filtro.IdTipoCargo >= 0)
                {
                    criteria.And(m => m.IdTipoCargo == filtro.IdTipoCargo);
                }
            }
            var resultado = _repValOrigenCargo
               .SelectBy(criteria)
               .Join(_repCatalogo.List(critcata),
                   v => v.IdValOrigen,
                   c => c.IdCatalogo,
                   (v, c) => new { v, c })
               .Select(item => new PocValOrigenCargo
               {
                   IdTipoCargo = item.v.IdTipoCargo,
                   IdValOrigen = item.v.IdValOrigen,
                   Descripcion = item.c.Descripcion
               });
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocValOrigenCargo registro, bool inserta)
        {
            var validacion = new ValidationResponse();

            if (registro.IdTipoCargo < 0) { validacion.AddError("Err_0035"); }
            if (registro.IdValOrigen < 0) { validacion.AddError("Err_0035"); }

            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            else
            {
                return validacion;
            }
        }

        public IEnumerable<PocCatalogo> ObtieneInfoLista(int idLista, int?[] excluir)
        {
            string nombreLista = _repCatalogo.SelectById(idLista).Codigo;
            var repLista = _repCatalogo.List(nombreLista)
                .Where(x => x.IdEstado != (int)EnumIdEstado.Inactivo)
                .ToList()
                .ProyectarComo<PocCatalogo>();
            if (excluir != null)
            {
                repLista.RemoveAll(m => excluir.Any(p => p.Value == m.IdCatalogo));
            }
            return repLista;
        }
    }
}

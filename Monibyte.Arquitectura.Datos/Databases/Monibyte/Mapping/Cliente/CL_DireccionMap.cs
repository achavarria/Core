using Monibyte.Arquitectura.Dominio.Clientes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Clientes
{
    public class CL_DireccionMap : EntityTypeConfiguration<CL_Direccion>
    {
        public CL_DireccionMap()
        {
            // Primary Key
            this.HasKey(t => t.IdDireccion);

            // Properties
            this.Property(t => t.IdDireccion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Direccion)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("CL_Direccion", "Cliente");
            this.Property(t => t.IdDireccion).HasColumnName("IdDireccion");
            this.Property(t => t.IdTipo).HasColumnName("IdTipo");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.IdProvincia).HasColumnName("IdProvincia");
            this.Property(t => t.IdCanton).HasColumnName("IdCanton");
            this.Property(t => t.IdDistrito).HasColumnName("IdDistrito");
            this.Property(t => t.Direccion).HasColumnName("Direccion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdPersona).HasColumnName("IdPersona");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdOrigenProcesador).HasColumnName("IdOrigenProcesador");
        }
    }
}

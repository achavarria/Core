using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Dominio.Generales;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Configuracion
{
    public partial class MB_LicenciaPersona : EntidadBase
    {
        public int IdPersona { get; set; }
        public int IdTipoLicencia { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

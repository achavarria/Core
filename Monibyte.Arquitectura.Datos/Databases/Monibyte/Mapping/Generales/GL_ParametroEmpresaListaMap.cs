using Monibyte.Arquitectura.Dominio.Generales;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Generales
{
    public class GL_ParametroEmpresaListaMap : EntityTypeConfiguration<GL_ParametroEmpresaLista>
    {
        public GL_ParametroEmpresaListaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdParametroEmpresa, t.Codigo });

            // Properties
            this.Property(t => t.IdParametroEmpresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Codigo)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.CodigoContrapartida)
                .HasMaxLength(100);

            this.Property(t => t.CodigoDependencia)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("GL_ParametroEmpresaLista", "General");
            this.Property(t => t.IdParametroEmpresa).HasColumnName("IdParametroEmpresa");
            this.Property(t => t.Codigo).HasColumnName("Codigo");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.Orden).HasColumnName("Orden");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.CodigoContrapartida).HasColumnName("CodigoContrapartida");
            this.Property(t => t.CodigoDependencia).HasColumnName("CodigoDependencia");
            this.Property(t => t.ConfiguracionDinamica).HasColumnName("ConfiguracionDinamica");
        }
    }
}

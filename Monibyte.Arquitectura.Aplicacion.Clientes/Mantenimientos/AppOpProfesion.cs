﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Clientes;
using Monibyte.Arquitectura.Poco.Clientes;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Clientes.Mantenimientos
{
    public class AppOpProfesion : AplicacionBase, IAppOpProfesion
    {
        private IRepositorioMnb<CL_Profesion> _repProfesion;

        public AppOpProfesion(
            IRepositorioMnb<CL_Profesion> repProfesion)
        {
            _repProfesion = repProfesion;
        }

        public List<PocProfesion> ListaProfesiones()
        {
            var profesiones = _repProfesion.Table
                .OrderBy(x => x.Profesion)
                .ToList();
            profesiones.Update(m =>
            {
                m.Profesion = _fGenerales.Traducir(m.Profesion,
                        "CL_Profesion", m.IdProfesion, Context.Lang.Id, 0,
                        Context.DefaultLang.Id).ToLower().Capitalize();
            });
            return profesiones.ProyectarComo<PocProfesion>();
        }
    }
}

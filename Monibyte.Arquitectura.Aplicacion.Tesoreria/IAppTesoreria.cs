﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Tesoreria;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Tesoreria
{
    public interface IAppTesoreria : IAplicacionBase
    {
        IEnumerable<PocMoneda> ListaMonedas();
        void InsertaMoneda(PocMoneda pocEntrada);
        void ActualizaMoneda(PocMoneda pocEntrada);
        void EliminaMoneda(PocMoneda pocEntrada);
        decimal ConvierteMonto(PocMontoConvertir poco);
        PocMoneda ObtieneMoneda(int idMoneda);
        decimal? ObtieneCambioBCCR(DateTime fecCambio, bool esCompra);
        decimal ObtieneUltimoTipoCambio(int IdTipoCambio, System.DateTime FecCambio, int IdMoneda, int? IdEntidad = null);
        decimal ConvertirMontoHistorico(int idMonedaOrigen, decimal monto, int idMonedaDestino,
              System.DateTime fecReferencia, int? idEntidadFinanciera = null, int idTipoCambio = (int)EnumIdTipoDeCambio.Compra);

    }
}

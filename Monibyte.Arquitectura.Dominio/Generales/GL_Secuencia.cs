using System;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_Secuencia : EntidadBase
    {
        public string IdSecuencia { get; set; }
        public string Descripcion { get; set; }
        public int ValorActual { get; set; }
        public int IdEstado { get; set; }
        public int Incremento { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using System;
using System.Xml;
using System.Xml.Linq;

namespace Monibyte.Arquitectura.Datos.Tesoreria.Repositorios
{
    public class ServiciosEconomicos : IServiciosEconomicos
    {
        public object ObtenerTipoCambioCompra(DateTime? fecTipoCambio = null) 
        {
            var wsIndicadores = new IndicadoresEconomicosBCCR.wsIndicadoresEconomicosSoapClient();
            var fecCambio = (fecTipoCambio.HasValue ? fecTipoCambio.Value : DateTime.Now.Date).ToString("dd/MM/yyyy");
            var resultado = wsIndicadores.ObtenerIndicadoresEconomicosXML("317",fecCambio, fecCambio, "Compra", "N");
            var doc = XDocument.Parse(resultado);
            decimal valor = XmlConvert.ToDecimal(doc.Root.Element("INGC011_CAT_INDICADORECONOMIC").Element("NUM_VALOR").Value);
            return valor; 
        }

        public object ObtenerTipoCambioVenta(DateTime? fecTipoCambio = null)
        {
            var wsIndicadores = new IndicadoresEconomicosBCCR.wsIndicadoresEconomicosSoapClient();
            var fecCambio = (fecTipoCambio.HasValue ? fecTipoCambio.Value : DateTime.Now.Date).ToString("dd/MM/yyyy");
            var resultado = wsIndicadores.ObtenerIndicadoresEconomicosXML("318", fecCambio, fecCambio, "Venta", "N");
            var doc = XDocument.Parse(resultado);
            decimal valor = XmlConvert.ToDecimal(doc.Root.Element("INGC011_CAT_INDICADORECONOMIC").Element("NUM_VALOR").Value);
            Console.WriteLine(valor);
            return valor;
        }
    }
}

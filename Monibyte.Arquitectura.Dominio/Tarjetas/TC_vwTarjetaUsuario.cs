using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_vwTarjetaUsuario : EntidadBase
    {
        public long Id { get; set; }
        public int IdUsuario { get; set; }
        public string CodUsuario { get; set; }
        public string AliasUsuario { get; set; }
        public int IdEmpresa { get; set; }
        public int IdTipoUsuario { get; set; }
        public Nullable<int> IdUsuarioTarjeta { get; set; }
        public string CodUsuarioTarjeta { get; set; }
        public string CodUsuarioEmpresa { get; set; }
        public int IdTipoPersonaEmpresa { get; set; }
        public int IdCuenta { get; set; }
        public string NumCuenta { get; set; }
        public int IdTipoTarjeta { get; set; }
        public int IdEstadoCta { get; set; }
        public int IdEsCuentaMadre { get; set; }
        public Nullable<int> IdCuentaMadre { get; set; }
        public System.DateTime FechaVctoLineaCredito { get; set; }
        public Nullable<System.DateTime> FecUltCorte { get; set; }
        public Nullable<int> DiaCorte { get; set; }
        public int IdCompania { get; set; }
        public int IdUnidad { get; set; }
        public int IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public int IdRelacion { get; set; }
        public int IdTipoLimite { get; set; }
        public Nullable<int> IdEstadoTarjeta { get; set; }
        public int IdPersona { get; set; }
        public string NumIdentificacion { get; set; }
        public string NombreImpreso { get; set; }
        public string IdTarjetahabiente { get; set; }
        public Nullable<int> IdGrupoEmpresa { get; set; }
        public int IdTipoPlastico { get; set; }
        public string CamposDinamicos { get; set; }
        public System.DateTime FecVencimiento { get; set; }
        public Nullable<decimal> LimiteCreditoCtaGlobal { get; set; }
        public decimal LimiteCreditoCtaLocal { get; set; }
        public decimal LimiteCreditoCtaInter { get; set; }
        public Nullable<decimal> LimiteCreditoTarjetaLocal { get; set; }
        public Nullable<decimal> LimiteCreditoTarjetaInter { get; set; }
        public decimal SaldoLocal { get; set; }
        public decimal SaldoInternacional { get; set; }
        public Nullable<decimal> SaldoTarjetaLocal { get; set; }
        public Nullable<decimal> SaldoTarjetaInter { get; set; }
        public int IdSoloConsulta { get; set; }
        public Nullable<int> IdAutoriza { get; set; }
        public string CorreoEstCuenta { get; set; }
        public string CopiaCorreoEstCuenta { get; set; }
        public string Descripcion { get; set; }
        public int FueAgregada { get; set; }
        public Nullable<int> IdAplicaMillas { get; set; }
        public int IdTarjetaTitular { get; set; }
        public string NumTarjetaTitular { get; set; }
        public string NombreImpresoTitular { get; set; }
    }
}

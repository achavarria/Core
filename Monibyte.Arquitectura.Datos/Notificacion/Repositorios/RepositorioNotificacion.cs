﻿using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Notificacion;
using Monibyte.Arquitectura.Dominio.Notificacion.Repositorios;
using Monibyte.Arquitectura.Poco.Notificacion;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Notificacion.Repositorios
{
    public class RepositorioNotificacion : RepositorioMnb<NT_Configuracion>,
        IRepositorioNotificacion
    {
        ISql _unidadTbjo;
        public RepositorioNotificacion(IUnidadTbjoMonibyte unidadTbjo)
            : base(unidadTbjo)
        {
            _unidadTbjo = unidadTbjo;
        }

        public IEnumerable<NT_Configuracion> ObtenerNotificaciones(int? idContexto, int? idEmpresa, int? idEsActivo, int ExcluirAdminInt)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", (object)idEmpresa ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p2", (object)idEsActivo ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p3", (object)idContexto ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p4", Context.Lang.Id));
            listaParams.Add(new SqlParameter("p5", Context.DefaultLang.Id));
            listaParams.Add(new SqlParameter("p6", (object)ExcluirAdminInt));
            var resultado = (this.UnidadTbjo as ISql).ExecuteQuery<NT_Configuracion>
                ("Notificacion.P_ObtieneNotificacionesEmpresa @p1, @p2, @p3, @p4, @p5, @p6", listaParams.ToArray())
                .ToList();
            return resultado;
        }

        public int ValidarFiltroNotificacion(string pWhere)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", (object)pWhere ?? DBNull.Value));
            return (this.UnidadTbjo as ISql).ExecuteQuery<int>
                ("Notificacion.P_ValidarFiltro @p1", listaParams.ToArray())
                .FirstOrDefault();
        }

        public IEnumerable<PocDestinatarioDefault> ObtenerDestinatarios(int idEmpresa,
            string codUsuario, string numTarjeta, bool notAdmSms, bool notAdmEmail,
            bool notTarSms, bool notTarEmail, bool notUsrSms, bool notUsrEmail)
        {
            var listaParams = new List<SqlParameter>();
            listaParams.Add(new SqlParameter("p1", idEmpresa));
            listaParams.Add(new SqlParameter("p2", (object)codUsuario ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p3", (object)numTarjeta ?? DBNull.Value));
            listaParams.Add(new SqlParameter("p4", notAdmSms ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            listaParams.Add(new SqlParameter("p5", notAdmEmail? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            listaParams.Add(new SqlParameter("p6", notTarSms ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            listaParams.Add(new SqlParameter("p7", notTarEmail ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            listaParams.Add(new SqlParameter("p8", notUsrSms ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            listaParams.Add(new SqlParameter("p9", notUsrEmail ? (int)EnumIdSiNo.Si : (int)EnumIdSiNo.No));
            var resultado = (this.UnidadTbjo as ISql).ExecuteQuery<PocDestinatarioDefault>
                ("Notificacion.P_ObtieneDireccionEnvio @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9", listaParams.ToArray())
                .ToList();
            return resultado;
        }
    }
}
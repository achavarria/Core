﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Monibyte.Arquitectura.Poco.Documentacion
{
   public class PocDocumentacion
    {
        public int IdAyuda { get; set; }
        public int IdAccion { get; set; }
        public int IdIdioma { get; set; }
        public int IdOrden { get; set; }
        public DateTime VigenciaDesde { get; set; }
        public DateTime VigenciaHasta { get; set; }
        public string Parrrafo { get; set; }
        public int IdEstado { get; set; }
        public string Imagen { get; set; }
        public string Titulo { get; set; }
        public int? IdAlineacionImagen { get; set; }
        public int IdTipoParrafo { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime? FecActualizaAud { get; set; }
        public int? IdUsuarioActualizaAud { get; set; }
    }
}

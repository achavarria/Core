﻿using System;

namespace Monibyte.Arquitectura.Poco.Tarjetas
{
    public class PocCuentaTarjeta
    {
        public int IdCuenta { get; set; }
        public string NumCuenta { get; set; }
        public int IdTipoTarjeta { get; set; }
        public decimal LimiteCredito { get; set; }
        public System.DateTime FechaVctoLineaCredito { get; set; }
        public System.DateTime FecApertura { get; set; }
        public Nullable<System.DateTime> FecActivacion { get; set; }
        public Nullable<System.DateTime> FecBloqueo { get; set; }
        public Nullable<int> IdUsuarioBloqueo { get; set; }
        public Nullable<int> IdUsuarioActivacion { get; set; }
        public int IdUsuarioApertura { get; set; }
        public decimal SaldoLocal { get; set; }
        public int IdEstadoCta { get; set; }
        public Nullable<int> IdEnvioEstCuenta { get; set; }
        public string CuentaClienteLocal { get; set; }
        public Nullable<decimal> PorcAvanceEfectivo { get; set; }
        public decimal SaldoInternacional { get; set; }
        public Nullable<decimal> DispAvanceEfectivoInter { get; set; }
        public Nullable<decimal> DispAvanceEfectivoLocal { get; set; }
        public decimal MillasAcumuladas { get; set; }
        public Nullable<int> DiasAtrasoLocal { get; set; }
        public Nullable<int> IdDiaCorte { get; set; }
        public Nullable<int> DiasAtrasoInter { get; set; }
        public Nullable<decimal> PagoMinLocal { get; set; }
        public Nullable<decimal> PagoMinInter { get; set; }
        public Nullable<decimal> PagoContadoLocal { get; set; }
        public Nullable<decimal> PagoContadoInter { get; set; }
        public Nullable<System.DateTime> FecUltPagoLocal { get; set; }
        public Nullable<System.DateTime> FecUltPagoInter { get; set; }
        public Nullable<System.DateTime> FecProxPagoContado { get; set; }
        public Nullable<System.DateTime> FecProxPagoMinimo { get; set; }
        public string CuentaClienteInter { get; set; }
        public decimal TasaInteresLocal { get; set; }
        public decimal TasaInteresInter { get; set; }
        public decimal TasaIntMoraLocal { get; set; }
        public decimal TasaIntMoraInter { get; set; }
        public Nullable<decimal> InteresMoratorioLocal { get; set; }
        public Nullable<decimal> InteresMoratorioInter { get; set; }
        public decimal TasaIntCJLocal { get; set; }
        public Nullable<decimal> TasaIntCJInter { get; set; }
        public int IdCalificacion { get; set; }
        public decimal PlazoMeses { get; set; }
        public decimal MonDisponibleLocal { get; set; }
        public decimal MonDisponibleInter { get; set; }
        public Nullable<decimal> MonCargosBonifLocal { get; set; }
        public Nullable<decimal> MonCargosBonifInter { get; set; }
        public Nullable<decimal> MonCreditosLocal { get; set; }
        public Nullable<decimal> MonCreditosInter { get; set; }
        public Nullable<decimal> SaldoTotalCorteLocal { get; set; }
        public Nullable<decimal> SaldoTotalCorteInter { get; set; }
        public Nullable<int> CantImpVencidosLocal { get; set; }
        public Nullable<int> CantImpVencidosInter { get; set; }
        public Nullable<decimal> MonImpVencidosLocal { get; set; }
        public Nullable<decimal> DebitosTransitoLocal { get; set; }
        public Nullable<decimal> DebitosTransitoInter { get; set; }
        public Nullable<decimal> CreditosTransitoLocal { get; set; }
        public Nullable<decimal> CreditosTransitoInter { get; set; }
        public Nullable<decimal> LimiteExtraFinLocal { get; set; }
        public Nullable<decimal> LimiteExtraFinInter { get; set; }
        public Nullable<decimal> SaldoExtraFinLocal { get; set; }
        public Nullable<decimal> SaldoExtraFinInter { get; set; }
        public Nullable<decimal> MonDispExtraFinLocal { get; set; }
        public Nullable<decimal> MonDispExtraFinInter { get; set; }
        public Nullable<decimal> MonImpVencidosInter { get; set; }
    }

    public class PocBloqueoTarjeta
    {
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdPersona { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public int IdTipoBloqueo { get; set; }
        public string CodBloqueoProcesador { get; set; }
        public string RazonBloqueo { get; set; }
        public string DetalleGestion { get; set; }
    }

    public class PocEstadoTarjeta
    {
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public int IdPersona { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public int IdEstado { get; set; }
        public string Correo { get; set; }
        public string Horario { get; set; }
        public string RazonActivacion { get; set; }
        public int? IdMotivoCancelacion { get; set; }
        public string DetalleGestion { get; set; }
    }

    public class PocReposicionTarjeta
    {
        public int IdCuenta { get; set; }
        public int IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public DateTime FecEntrega { get; set; }
        public string Sucursal { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFinal { get; set; }
        public string Correo { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public int RazonReposicion { get; set; }
        public int IdTipoGestion { get; set; }
        public int IdSucursal { get; set; }
        public int? IdEjecutivo { get; set; }
    }

    public class PocModificaLimite
    {
        public int? IdGestion { get; set; }
        public string Token { get; set; }
        public int? IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }
        public int? IdEjecutivo { get; set; }
        public string NumTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public string DetalleGestion { get; set; }
        public string Observaciones { get; set; }
        public decimal MonLimiteNuevo { get; set; }
        public decimal MonLimiteActual { get; set; }
        public DateTime? VigenteHasta { get; set; }
        public bool EsAutorizacion { get; set; }
        public bool ModificaLimiteAutoriza { get; set; }
        public int IdEstado { get; set; }
        public bool Editando { get; set; }
    }

    public class PocModificaFecCorteTarjeta
    {
        public int IdCuenta { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public int IdPersona { get; set; }
        public DateTime FecCorte { get; set; }
        public DateTime? FecPagoMinimo { get; set; }
        public int IdDiaCorte { get; set; }
        public int DiaCorte { get; set; }
        public string DetalleGestion { get; set; }
        public int HorasFechaCorte { get; set; }
        public bool HabilitadoPeriodo { get; set; }
        public bool? HabilitadoPagoMinimo { get; set; }
    }

    public class PocListaMoneda
    {
        public int IdMoneda { get; set; }
        public string Descripcion { get; set; }
        public string Simbolo { get; set; }
        public string CodInternacional { get; set; }
        public int IdEstado { get; set; }
        public string OperadorConversion { get; set; }
        public int CodigoSUGEF { get; set; }
        public int NumInternacional { get; set; }
        public string CosAlpha2 { get; set; }
    }

    public class PocTarjetasPersona
    {
        public int IdTarjeta { get; set; }
        public string NumTarjeta { get; set; }
        public string NombreImpreso { get; set; }
        public int IdEstado { get; set; }
        public string DetalleEstado { get; set; }
    }


    public class PocMovimientoTarjeta
    {
        public int IdMovimiento { get; set; }
        public int IdUsuario { get; set; }
        public int IdTarjeta { get; set; }
        public string IdTipoMovimiento { get; set; }
        public string NumTarjeta { get; set; }
        public DateTime FecTransaccion { get; set; }
        public DateTime HoraTransaccion { get; set; }
        public string Descripcion { get; set; }
        public decimal MonTransaccion { get; set; }
        public int IdMoneda { get; set; }
    }


    public class PocModificaVip
    {
        public int? IdGestion { get; set; }
        public string DetalleGestion { get; set; }
        public int? IdEstadoGestion { get; set; }
        public string Token { get; set; }
        public int? IdCuenta { get; set; }
        public int? IdTarjeta { get; set; }
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public int? IdPersona { get; set; }
        public bool IndicadorVipActual { get; set; }
        public bool IndicadorVipNuevo { get; set; }
        public DateTime? VigenteDesde { get; set; }
        public DateTime? VigenteHasta { get; set; }
        public bool EsAutorizacion { get; set; }
        public int? IdCondicion { get; set; }
        public string NombreCompleto { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
        public string Observaciones { get; set; }
        public string Accion { get; set; }
    }
    public class PocParametrosVip
    {
        public string NumCuenta { get; set; }
        public string NumTarjeta { get; set; }
        public string LocalidadTramite { get; set; }
        public bool ValidaVigencia { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public string CodigoRespuesta { get; set; }
        public string DescripcionCodRespuesta { get; set; }
    }

    public class PocReporteModificacionesLimite
    {
        public int IdGestion { get; set; }
        public string UsuarioIngresa { get; set; }
        public string UsuarioActualiza { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaActualiza { get; set; }
        public string NumCuenta { get; set; }
        public string NombreTitular { get; set; }
        public decimal MonLimiteAnterior { get; set; }
        public decimal MonLimiteActual { get; set; }
        public string DesEstado { get; set; }
        public int? IdEstado { get; set; }
        public string Detalle { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
    }
}

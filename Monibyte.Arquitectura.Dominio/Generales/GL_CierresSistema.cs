using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_CierresSistema : EntidadBase
    {
        public int IdCierre { get; set; }
        public int IdSistema { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int OrdenEjecucion { get; set; }
        public Nullable<System.TimeSpan> HoraEjecucion { get; set; }
        public Nullable<int> IdDependeDe { get; set; }
        public string EjecutarMetodo { get; set; }
        public Nullable<System.DateTime> FecUltCierre { get; set; }
        public int IdEstadoEjecucion { get; set; }
        public int IdPeriodicidad { get; set; }
        public decimal ValPeriodicidad { get; set; }
        public Nullable<System.DateTime> HoraUltEjecucion { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<System.DateTime> FecInicio { get; set; }
        public Nullable<System.DateTime> FecFinaliza { get; set; }
    }
}

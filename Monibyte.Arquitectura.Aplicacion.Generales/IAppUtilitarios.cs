﻿using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Generales
{
    public interface IAppUtilitarios : IAplicacionBase
    {
        void EnviarEmail(SenderConfig sender, EmailConfig emailConfig);
        void EnviarInfoEmail(EmailConfig emailConfig);
        IEnumerable<PocIdioma> ListaIdiomas();
        IEnumerable<PocPais> ListaPaises();
        IEnumerable<PocTipoIdentificacion> ListaTipoIdentificacion();
        IEnumerable<PocCatalogo> ObtieneInfoLista(string lista, int?[] excluir, string ref1 = "",
            string ref2 = "", string ref3 = "", string ref4 = "", string ref5 = "");
        PocCatalogo ObtieneInfoValorLista(int idLista);
        int RolPorTipoUsuario(int idTipoUsuario);
        void EnviarNotificacion(string from, string to, string cc = null, string bcc = null,
                    int? idPlantilla = null, string subject = null, string body = null,
                    object[] subjectParams = null, object[] bodyParams = null);
        int ObtenerSistemaPorTipoUsuario(int idTipoUsuario);
        void ObtieneCamposContextoExcluirCriteria(ref IEnumerable<PocCampoContexto> listCamposContexto, string ref1 = null, string ref2 = null,
            string ref3 = null, string ref4 = null, string ref5 = null);
    }
}

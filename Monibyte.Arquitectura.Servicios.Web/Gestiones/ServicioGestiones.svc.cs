﻿using Monibyte.Arquitectura.Aplicacion.Gestiones;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Gestiones;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Servicios.Web.Gestiones
{
    public class ServicioGestiones : ServicioUnity, IServicioGestiones
    {
        private IAppReportes _reportes;
        private IAppGestiones _gestion;
        private IAppGestionesGenerales _gestionesGenerales;
        public ServicioGestiones(
            IAppReportes reportes,
            IAppGestiones paramGestion,
            IAppGestionesGenerales gestionesGenerales)
        {
            _reportes = reportes;
            _gestion = paramGestion;
            _gestionesGenerales = gestionesGenerales;
        }

        public Transporte ObtieneGestion(Transporte transporte)
        {
            PocDetalleGestion resultado = null;
            var idGestion = transporte.GetDatos<int?>();
            if (idGestion.HasValue)
            {
                resultado = _gestion.ObtenerGestion(idGestion.Value);
            }
            else
            {
                resultado = _gestion.ObtenerUltimaGestionPorPersona(InfoSesion.Info.IdUsuario);
            }
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtieneListaGestion(Transporte transporte)
        {
            var parametros = transporte.GetDatos<PocParametrosGestion>();
            var resultado = _gestion.ObtenerGestionesEmpresa(parametros, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtieneHistorialGestion(Transporte transporte)
        {
            var idGestion = transporte.GetValor<int>("idGestion");
            var resultado = _gestion.ObtenerHistorial(idGestion, transporte.GetDataRequest());
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaTipoGestion(Transporte transporte)
        {
            var resultado = _gestion.ListaTipoGestion();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtieneGestionesRealizadas(Transporte transporte)
        {
            var parametros = transporte.GetDatos<PocParametrosGestion>();
            var resultado = _gestion.ConsultarGestionesRealizadas(parametros);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminarGestionRegistrada(Transporte transporte)
        {
            var idGestion = transporte.GetValor<int>("idGestion");
            _gestion.EliminarGestionRegistrada(idGestion);
            return transporte;
        }

        public Transporte CorreoContactenos(Transporte transporte)
        {
            var correo = transporte.GetValor<string>("correo");
            var msj = transporte.GetValor<string>("msj");
            var nombreCompleto = transporte.GetValor<string>("nombreCompleto");
            //_gestion.CorreoContactenos(msj, correo, nombreCompleto);
            return transporte;
        }

        public Transporte ObtenerParametrosEmpresaGestion(Transporte transporte)
        {
            var contexto = transporte.GetValor<EnumContexto>("contexto");
            var tipoGestion = transporte.GetValor<EnumTipoGestion>("tipoGestion");
            var resultado = _gestionesGenerales.ObtenerParametrosEmpresaGestion(tipoGestion, contexto);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerMovimientosLiquidacion(Transporte transporte)
        {
            var parametros = transporte.GetDatos<PocParametrosLiquidacion>();
            var resultado = _gestionesGenerales.ObtenerMovimientosLiquidacion(parametros);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte GuardarLiquidacionGastos(Transporte transporte)
        {
            var parametros = transporte.GetDatos<PocLiquidacionGastos>();
            var idReporte = transporte.GetValor<int?>("idReporte");
            var resultado = _gestionesGenerales.GuardarLiquidacionGastos(parametros, idReporte);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte EliminarMovimientoLiquidacion(Transporte transporte)
        {
            var idGestion = transporte.GetValor<int>("idGestion");
            var movimientos = transporte.GetDatos<PocMovimientosDeLiquidacion[]>();
            _gestionesGenerales.EliminarMovimientoLiquidacion(idGestion, movimientos);
            return transporte;
        }

        public Transporte ObtenerReporteLiquidacionKilometraje(Transporte transporte)
        {
            var json = transporte.GetValor<string>("json");
            var resultado = _reportes.ObtenerGestionKilometraje(json);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ObtenerParamGestionEmpresa(Transporte transporte)
        {
            var idTipoGestion = transporte.GetValor<int?>("idTipoGestion");
            var reqTodo = transporte.GetValor<bool>("reqTodo");
            var resultado = _gestionesGenerales.ObtenerParamGestionEmpresa(idTipoGestion, reqTodo);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte GuardarParametrosEmpresa(Transporte transporte)
        {
            var datosEntrada = transporte.GetValor<List<PocParametrosGestionEmpresa>>("datosSalida");
            var datosActuales = transporte.GetValor<List<PocParametrosGestionEmpresa>>("datosPrevios");
            _gestionesGenerales.GuardarParametrosEmpresa(datosEntrada, datosActuales);
            return transporte;
        }

        public Transporte ObtieneReporteLiq(Transporte transporte)
        {
            var idGestion = transporte.GetValor<int>("idGestion");
            var idReporte = transporte.GetValor<int?>("idReporte");
            var resultado = _reportes.ObtenerReporteLiquidacion(idGestion, idReporte);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ModificarValorDefecto(Transporte transporte)
        {
            var datosEntrada = transporte.GetDatos<List<PocParametrosGestionEmpresa>>();
            _gestionesGenerales.ModificarValorDefecto(datosEntrada);
            return transporte;
        }

        public void CerrarLiqGastos(int idGestion)
        {
            _gestionesGenerales.CerrarLiqGastos(idGestion);
        }

        public Transporte ObtenerUltimoRangoLiquidacion(Transporte transporte)
        {
            var idPersona = transporte.GetValor<int>("idPersona");
            var resultado = _gestionesGenerales.ObtenerUltimoRangoLiquidacion(idPersona);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ModificarEstadoLiquidacion(Transporte transporte)
        {
            var poco = transporte.GetDatos<PocCambioEstadoGestionTarjeta>();
            _gestionesGenerales.ModificarEstadoLiquidacion(poco);
            return transporte;
        }

        public Transporte EliminarLiquidacion(Transporte transporte)
        {
            var idGestion = transporte.GetValor<int>("idGestion");
            _gestionesGenerales.EliminarLiquidacion(idGestion);
            return transporte;
        }

        #region Reporte Gestiones por Usuario

        public PocReporte ObtenerReporteGestiones(DateTime fecDesde,
            DateTime fecHasta, bool esExcel)
        {
            var resultado = _gestion.ObtenerReporteGestiones(fecDesde, fecHasta, esExcel);
            return resultado;
        }

        #endregion

        public void ModificaLimiteCuenta(PocModificaLimite cambioLimite)
        {
            var app = GestorUnity.CargarUnidad<IAppGestiones>();
            app.ModificaLimiteGlobal(cambioLimite);
        }

        public List<PocReporteModificacionesLimite> ObtenerModificacionesLimite(PocReporteModificacionesLimite filtro)
        {
            var app = GestorUnity.CargarUnidad<IAppGestiones>();
            return app.ObtenerModificacionesLimite(filtro);
        }
    }
}

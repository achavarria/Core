using Monibyte.Arquitectura.Dominio.Autorizador;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.CtrlMonibyte.Mapping.Autorizador
{
    public class MB_EmisorMap : EntityTypeConfiguration<MB_Emisor>
    {
        public MB_EmisorMap()
        {
            // Primary Key
            this.HasKey(t => t.IdEmisor);

            // Properties
            this.Property(t => t.IdEmisor)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodEmisor)
                .IsRequired()
                .HasMaxLength(8);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(120);

            this.Property(t => t.CodPaisLocal)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.CodMonedaLocal)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.CodMonedaInternacional)
                .IsRequired()
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("MB_Emisor", "Mnb");
            this.Property(t => t.IdEmisor).HasColumnName("IdEmisor");
            this.Property(t => t.CodEmisor).HasColumnName("CodEmisor");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.CodPaisLocal).HasColumnName("CodPaisLocal");
            this.Property(t => t.CodMonedaLocal).HasColumnName("CodMonedaLocal");
            this.Property(t => t.CodMonedaInternacional).HasColumnName("CodMonedaInternacional");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdValPais).HasColumnName("IdValPais");
            this.Property(t => t.IdValPostEntryMode).HasColumnName("IdValPostEntryMode");
            this.Property(t => t.IdValPostCondCode).HasColumnName("IdValPostCondCode");
            this.Property(t => t.IdValMCC).HasColumnName("IdValMCC");
            this.Property(t => t.IdValBIN).HasColumnName("IdValBIN");
            this.Property(t => t.IdValChip).HasColumnName("IdValChip");
            this.Property(t => t.IdValFactorInt).HasColumnName("IdValFactorInt");
            this.Property(t => t.IdValCliente).HasColumnName("IdValCliente");
        }
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Procesador
{
    public partial class PT_AutorizacionesHist : EntidadBase
    {
        public int IdAutorizacion { get; set; }
        public string NumeroCuenta { get; set; }
        public string NumeroTarjeta { get; set; }
        public string CodFranquicia { get; set; }
        public string NombreComercio { get; set; }
        public string NombreCliente { get; set; }
        public Nullable<System.DateTime> FechaCompra { get; set; }
        public string HoraTransaccion { get; set; }
        public Nullable<decimal> MontoAutorizacion { get; set; }
        public string Moneda { get; set; }
        public string FormaEntrada { get; set; }
        public string Respuesta { get; set; }
        public string Mcc { get; set; }
        public string CodigoRespuesta { get; set; }
        public Nullable<System.DateTime> FechaEmisionTarjet { get; set; }
        public string Autoriza { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
    }
}

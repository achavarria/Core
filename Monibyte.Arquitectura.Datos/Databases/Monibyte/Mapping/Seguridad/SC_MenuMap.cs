using Monibyte.Arquitectura.Dominio.Seguridad;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Seguridad
{
    public class SC_MenuMap : EntityTypeConfiguration<SC_Menu>
    {
        public SC_MenuMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMenu);

            // Properties
            this.Property(t => t.IdMenu)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("SC_Menu", "Seguridad");
            this.Property(t => t.IdMenu).HasColumnName("IdMenu");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.Nivel).HasColumnName("Nivel");
            this.Property(t => t.Orden).HasColumnName("Orden");
            this.Property(t => t.IdMenuPadre).HasColumnName("IdMenuPadre");
            this.Property(t => t.IdSistema).HasColumnName("IdSistema");
            this.Property(t => t.IdAccion).HasColumnName("IdAccion");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.IdRestringeEmpresa).HasColumnName("IdRestringeEmpresa");
            this.Property(t => t.IdCompania).HasColumnName("IdCompania");
        }
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Generales
{
    public partial class GL_CamposReporte : EntidadBase
    {
        public int IdCampoReporte { get; set; }
        public int IdReporte { get; set; }
        public int IdOrigen { get; set; }
        public int IdSeparador { get; set; }
        public int IdCampo { get; set; }
        public Nullable<int> Orden { get; set; }
        public string ValorContrapartida { get; set; }
        public int IdSumaContrapartida { get; set; }
        public int IdAgrupaContrapartida { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public Nullable<System.DateTime> FecActualizaAud { get; set; }
        public Nullable<int> IdUsuarioActualizaAud { get; set; }
        public Nullable<int> IdEsEncabezado { get; set; }
        public Nullable<int> IdSumariza { get; set; }
        public Nullable<int> IdAgrupable { get; set; }
        public Nullable<int> IdContexto { get; set; }
        public Nullable<int> LongitudMax { get; set; }
        public int IdDescripcion { get; set; }
        public string Alias { get; set; }
    }
}

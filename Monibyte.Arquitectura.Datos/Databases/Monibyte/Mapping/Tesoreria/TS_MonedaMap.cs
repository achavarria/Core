using Monibyte.Arquitectura.Dominio.Tesoreria;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Monibyte.Arquitectura.Datos.Databases.Monibyte.Mapping.Tesoreria
{
    public class TS_MonedaMap : EntityTypeConfiguration<TS_Moneda>
    {
        public TS_MonedaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMoneda);

            // Properties
            this.Property(t => t.IdMoneda)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Simbolo)
                .HasMaxLength(5);

            this.Property(t => t.CodInternacional)
                .HasMaxLength(5);

            this.Property(t => t.OperadorConversion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.CodigoNormativa)
                .HasMaxLength(5);

            this.Property(t => t.NumInternacional)
                .HasMaxLength(5);

            this.Property(t => t.CodAlpha2)
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("TS_Moneda", "Tesoreria");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.Simbolo).HasColumnName("Simbolo");
            this.Property(t => t.CodInternacional).HasColumnName("CodInternacional");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.OperadorConversion).HasColumnName("OperadorConversion");
            this.Property(t => t.CodigoNormativa).HasColumnName("CodigoNormativa");
            this.Property(t => t.NumInternacional).HasColumnName("NumInternacional");
            this.Property(t => t.FecInclusionAud).HasColumnName("FecInclusionAud");
            this.Property(t => t.IdUsuarioIncluyeAud).HasColumnName("IdUsuarioIncluyeAud");
            this.Property(t => t.FecActualizaAud).HasColumnName("FecActualizaAud");
            this.Property(t => t.IdUsuarioActualizaAud).HasColumnName("IdUsuarioActualizaAud");
            this.Property(t => t.CodAlpha2).HasColumnName("CodAlpha2");
        }
    }
}

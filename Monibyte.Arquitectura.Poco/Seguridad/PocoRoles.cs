﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monibyte.Arquitectura.Poco.Seguridad
{
    public class PocRol
    {
        public int IdRole { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int IdEsGrupo { get; set; }
    }
}

﻿
using System.Collections.Generic;
namespace Monibyte.Arquitectura.Poco.Clientes
{
    public class PocEjecutivo
    {
        public int IdEjecutivo { get; set; }
        public int IdPersona { get; set; }
        public string Nombre { get; set; }  
        public int? IdEstado { get; set; }
        public string NumIdentificacion { get; set; }
        public string DesEstado { get; set; }
        public int IdEntidad { get; set; }
        public IEnumerable<PocInfoPersona> Empresas { get; set; }
    }
}

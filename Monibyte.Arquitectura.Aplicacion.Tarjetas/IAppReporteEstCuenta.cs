﻿using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Tarjetas;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Tarjetas;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public interface IAppReporteEstCuenta : IAplicacionBase
    {
        void GenerarEstCuenta(PocCuentasCorte cuentasCorte, int idMonedaLocal, int idMonedaInter, string tablaDetalle, string tablaEncabezado);
        void GenerarReporteEstCuenta(List<PocCuentasCorte> cuentasCorte, int idMonedaLocal, int idMonedaInter, string tablaDetalle = "TCredito.TC_DetalleEstCuenta", string tablaEncabezado = "TCredito.TC_EncabezadoEstCuenta");
        bool EnviarCorreoEstCuenta(List<PocCuentasCorte> cuentasCorte, int idMonedaLocal, int idMonedaInter, string tablaDetalle = "TCredito.TC_DetalleEstCuenta", string tablaEncabezado = "TCredito.TC_EncabezadoEstCuenta", bool generarEstCta = false);
        byte[] ObtenerEstCuentaComerciosTarjeta(PocComerciosTarjeta comercioTarjeta, int idMonedaLocal, int idMonedaInter);
        byte[] DescargarEstCuenta(string anno, string mes, int idCuenta, int esCuentaMadre);
        byte[] DescargarResumenEstCuenta(PocResumenEstCuenta datosEntrada);
        IEnumerable<PocMovimientosTarjeta> ObtenerMovtsDetEstadoCuenta(PocParametrosTarjeta parametros);
        byte[] GenerarEstCuentaIndividual(int idCuenta, int mes,
            int anno, int idTarjeta, int idCompania);
        bool ReenviarEstCuenta(PocConsultaEstadoCuenta datosEntrada);
        PocReporte GenerarEstMultiCuenta(PocGeneraEstCuenta entrada);
        void EnvioCorreos(PocCuentasCorte item, string[] adjuntos, byte[] rEstCuenta,
            byte[] rEstCtaMadre, string asunto, string mensaje, bool esIndividual = false);
        EncabezadoEstCuenta ObtieneEncabezadoEsteCuenta(PocCuentasCorte entrada);
        List<PocInfoDetalleEstCuenta> ObtieneDetalleEstCuenta(PocCuentasCorte entrada);
        PocInfoEstCuenta ObtieneEstCuenta(PocCuentasCorte entrada);
    }
}
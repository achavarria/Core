﻿using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Poco.Generales;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Monibyte.Arquitectura.Servicios.Nucleo;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Monibyte.Arquitectura.Servicios.Web.Gestiones
{
    [ServiceContract]
    public interface IServicioGestiones : IServicioUnity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtieneGestion")]
        Transporte ObtieneGestion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtieneListaGestion")]
        Transporte ObtieneListaGestion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtieneHistorialGestion")]
        Transporte ObtieneHistorialGestion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ListaTipoGestion")]
        Transporte ListaTipoGestion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtieneGestionesRealizadas")]
        Transporte ObtieneGestionesRealizadas(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminarGestionRegistrada")]
        Transporte EliminarGestionRegistrada(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "CorreoContactenos")]
        Transporte CorreoContactenos(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerParametrosEmpresaGestion")]
        Transporte ObtenerParametrosEmpresaGestion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerMovimientosLiquidacion")]
        Transporte ObtenerMovimientosLiquidacion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "GuardarLiquidacionGastos")]
        Transporte GuardarLiquidacionGastos(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminarMovimientoLiquidacion")]
        Transporte EliminarMovimientoLiquidacion(Transporte transporte);

        //[OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
        //    BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerGestionesPorTipo")]
        //Transporte ObtenerGestionesPorTipo(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerReporteLiquidacionKilometraje")]
        Transporte ObtenerReporteLiquidacionKilometraje(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerParamGestionEmpresa")]
        Transporte ObtenerParamGestionEmpresa(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "GuardarParametrosEmpresa")]
        Transporte GuardarParametrosEmpresa(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtieneReporteLiq")]
        Transporte ObtieneReporteLiq(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificarValorDefecto")]
        Transporte ModificarValorDefecto(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CerrarLiqGastos",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        void CerrarLiqGastos(int idGestion);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ObtenerUltimoRangoLiquidacion")]
        Transporte ObtenerUltimoRangoLiquidacion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificarEstadoLiquidacion")]
        Transporte ModificarEstadoLiquidacion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "EliminarLiquidacion")]
        Transporte EliminarLiquidacion(Transporte transporte);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ObtenerReporteGestiones",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        PocReporte ObtenerReporteGestiones(DateTime fecDesde,
            DateTime fecHasta, bool esExcel);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "ModificaLimiteCuenta")]
        void ModificaLimiteCuenta(PocModificaLimite cambioLimite);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "ObtenerModificacionesLimite")]
        List<PocReporteModificacionesLimite> ObtenerModificacionesLimite(PocReporteModificacionesLimite filtro);
    }
}

﻿using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using Monibyte.Arquitectura.Datos.Databases.Monibyte;
using Monibyte.Arquitectura.Datos.Nucleo;
using Monibyte.Arquitectura.Dominio.Seguridad;
using Monibyte.Arquitectura.Dominio.Seguridad.Repositorios;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Monibyte.Arquitectura.Datos.Seguridad.Repositorios
{
    public class FuncionesSeguridad : RepositorioBase, IFuncionesSeguridad
    {
        ISql _unidadTbjo;
        public FuncionesSeguridad(IUnidadTbjoMonibyte unidadTbjo)
            : base(unidadTbjo)
        {
            _unidadTbjo = unidadTbjo;
        }

        public IEnumerable<Cplx_SC_MenusRole> ObtenerMenuUsuario()
        {
            if (InfoSesion.Info != null)
            {
                var listaParams = new List<SqlParameter>();
                listaParams.Add(new SqlParameter("p1", InfoSesion.Info.IdUsuario));
                listaParams.Add(new SqlParameter("p2", InfoSesion.Info.IdEmpresa));
                listaParams.Add(new SqlParameter("p3", InfoSesion.ObtenerSesion<int>("IDSISTEMA")));
                listaParams.Add(new SqlParameter("p4", InfoSesion.Info.IdPersona));
                listaParams.Add(new SqlParameter("p5", InfoSesion.Info.IdTipoUsuario));
                listaParams.Add(new SqlParameter("p6", InfoSesion.Info.IdCompania));
                listaParams.Add(new SqlParameter("p7", InfoSesion.Lang.Id));
                return _unidadTbjo.ExecuteQuery<Cplx_SC_MenusRole>(@"Seguridad.P_ObtieneMenuUsuario 
                    @p1, @p2, @p3, @p4, @p5, @p6, @p7", listaParams.ToArray())
                    .ToList().OrderBy(x => x.IdOrden);
            }
            return null;
        }

        public IEnumerable<Cplx_SC_Accion> ObtenerPermisosUsuario()
        {
            if (InfoSesion.Info != null)
            {
                var listaParams = new List<SqlParameter>();
                listaParams.Add(new SqlParameter("p1", InfoSesion.Info.IdUsuario));
                listaParams.Add(new SqlParameter("p2", InfoSesion.Info.IdEmpresa));
                listaParams.Add(new SqlParameter("p3", InfoSesion.Info.IdPersona));
                listaParams.Add(new SqlParameter("p4", InfoSesion.Info.IdTipoUsuario));
                return _unidadTbjo.ExecuteQuery<Cplx_SC_Accion>
                    ("Seguridad.P_ObtienePermisosUsuario @p1, @p2, @p3, @p4", listaParams.ToArray())
                    .ToList();
            }
            return null;
        }
    }
}

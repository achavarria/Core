﻿using Monibyte.Arquitectura.Aplicacion.Generales;
using Monibyte.Arquitectura.Comun.Nucleo.Rest;
using Monibyte.Arquitectura.Servicios.Nucleo;

namespace Monibyte.Arquitectura.Servicios.Web.Generales
{
    public class ServiciosDistribucionGeografica : ServicioUnity, IServiciosDistribucionGeografica
    {
        private IAppGenerales _appGenerales;
        public ServiciosDistribucionGeografica(IAppGenerales appGenerales)
        {
            _appGenerales = appGenerales;
        }

        public Transporte ListaProvincias(Transporte transporte)
        {
            var resultado = _appGenerales.ListaProvincias();
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaCantones(Transporte transporte)
        {
            var provincia = transporte.GetValor<int>("idProvincia");
            var resultado = _appGenerales.ListaCantones(provincia);
            transporte.SetDatos(resultado);
            return transporte;
        }

        public Transporte ListaDistritos(Transporte transporte)
        {
            var idCanton = transporte.GetValor<int>("idCanton");
            var resultado = _appGenerales.ListaDistritos(idCanton);
            transporte.SetDatos(resultado);
            return transporte;
        }

       
    }
}

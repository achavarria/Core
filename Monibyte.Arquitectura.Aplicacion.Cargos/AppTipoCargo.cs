﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Data.Extensions;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Cargos;
using Monibyte.Arquitectura.Dominio.Generales;
using Monibyte.Arquitectura.Dominio.Generales.Repositorios;
using Monibyte.Arquitectura.Dominio.Nucleo.Especificacion;
using Monibyte.Arquitectura.Dominio.Tesoreria.Repositorios;
using Monibyte.Arquitectura.Poco.Cargos;
using Monibyte.Arquitectura.Poco.Generales;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Monibyte.Arquitectura.Aplicacion.Cargos
{
    public class AppTipoCargo : AplicacionBase, IAppTipoCargo
    {
        private IRepositorioMoneda _repMoneda;
        private IRepositorioCatalogo _repCatalogo;
        private IRepositorioMnb<CA_TipoCargo> _repTipoCargo;
        private IRepositorioMnb<CA_RangosTabla> _repRangosTabla;
        private IRepositorioMnb<CA_ValOrigenCargo> _repValOrigenCargo;

        public AppTipoCargo(
            IRepositorioMoneda repMoneda,
            IRepositorioCatalogo repCatalogo,
            IRepositorioMnb<CA_TipoCargo> repTipoCargo,
            IRepositorioMnb<CA_RangosTabla> repRangosTabla,
            IRepositorioMnb<CA_ValOrigenCargo> repValOrigenCargo)
        {
            _repMoneda = repMoneda;
            _repCatalogo = repCatalogo;
            _repTipoCargo = repTipoCargo;
            _repRangosTabla = repRangosTabla;
            _repValOrigenCargo = repValOrigenCargo;
        }

        public void Eliminar(PocTipoCargo registro)
        {
            using (var ts = new TransactionScope())
            {
                var entrada = registro.ProyectarComo<CA_TipoCargo>();
                _repValOrigenCargo.DeleteOn(det => det.IdTipoCargo == entrada.IdTipoCargo);
                _repTipoCargo.Delete(entrada);
                _repTipoCargo.UnidadTbjo.Save();
                ts.Complete();
            }
        }

        public void Insertar(PocTipoCargo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, true);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_TipoCargo>();
                    entrada.IdTipoCargo = _fGenerales.ObtieneSgteSecuencia("SEQ_IdTipoCargo");
                    _repTipoCargo.Insert(entrada);
                    _repTipoCargo.UnidadTbjo.Save();

                    if (registro.ListaOrigenCargos != null)
                    {
                        foreach (PocValOrigenCargo detalle in registro.ListaOrigenCargos)
                        {
                            detalle.IdTipoCargo = entrada.IdTipoCargo;

                            var entradaOrigCargo = detalle.ProyectarComo<CA_ValOrigenCargo>();
                            _repValOrigenCargo.Insert(entradaOrigCargo);
                        }
                        _repValOrigenCargo.UnidadTbjo.Save();
                    }
                }
                else
                {
                    var msj = string.Format("error al insertar {0}", typeof(PocTipoCargo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public void Modificar(PocTipoCargo registro)
        {
            using (var ts = new TransactionScope())
            {
                var validacion = Validar(registro, false);
                if (validacion.IsValid)
                {
                    var entrada = registro.ProyectarComo<CA_TipoCargo>();
                    _repTipoCargo.UpdateExclude(entrada,
                        x => x.IdUsuarioIncluyeAud,
                        x => x.FecInclusionAud);
                    _repTipoCargo.UnidadTbjo.Save();

                    if (registro.ListaOrigenCargos != null)
                    {
                        _repValOrigenCargo.DeleteOn(det => det.IdTipoCargo == entrada.IdTipoCargo);
                        foreach (PocValOrigenCargo detalle in registro.ListaOrigenCargos)
                        {
                            detalle.IdTipoCargo = entrada.IdTipoCargo;

                            var entradaOrigCargo = detalle.ProyectarComo<CA_ValOrigenCargo>();
                            _repValOrigenCargo.Insert(entradaOrigCargo);
                        }
                        _repValOrigenCargo.UnidadTbjo.Save();
                    }
                }
                else
                {
                    var msj = string.Format("error al actualizar {0}", typeof(PocTipoCargo));
                    throw new CoreException(msj, validation: validacion);
                }
                ts.Complete();
            }
        }

        public DataResult<PocTipoCargo> Listar(PocTipoCargo filtro, DataRequest request)
        {
            var criteria = new Criteria<CA_TipoCargo>();
            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.Descripcion))
                {
                    criteria.And(m => m.Descripcion.Contains(filtro.Descripcion));
                }
                if (filtro.IdTipoCargo > 0)
                {
                    criteria.And(m => m.IdTipoCargo == filtro.IdTipoCargo);
                }
            }
            var resultado =
                from x in _repTipoCargo.SelectBy(criteria)
                join mone in _repMoneda.Table
                  on x.IdMoneda equals mone.IdMoneda
                join orig in _repCatalogo.List("LCONTEXTO")
                  on x.IdOrigenCargo equals orig.IdCatalogo
                join cobr in _repCatalogo.List("LTIPOCOBROCARGO")
                  on x.IdTipoCobro equals cobr.IdCatalogo
                join rang in _repRangosTabla.Table
                  on x.IdTablaRangos equals rang.IdTablaRangos
                join peri in _repCatalogo.List("LPERIODICIDAD")
                  on x.IdPeriodicidad equals peri.IdCatalogo
                join est in _repCatalogo.List("LACTINACTIVA")
                  on x.IdEstado equals est.IdCatalogo
                join mov in _repCatalogo.List("LTIPOMOVIMIENTOTC")
                  on x.IdMovimientoTC equals mov.IdCatalogo
                join reve in _repCatalogo.List("LTIPOMOVIMIENTOTC")
                   on x.IdMovReversaTC equals reve.IdCatalogo
                select new PocTipoCargo
                {
                    IdTipoCargo = x.IdTipoCargo,
                    Descripcion = x.Descripcion,
                    IdMoneda = x.IdMoneda,
                    DescMoneda = mone == null ? "" : mone.Descripcion,
                    IdOrigenCargo = x.IdOrigenCargo,
                    DescOrigenCargo = orig == null ? "" : orig.Descripcion,
                    IdTipoCobro = x.IdTipoCobro,
                    DescTipoCobro = cobr == null ? "" : cobr.Descripcion,
                    MonCargo = x.MonCargo,
                    IdTablaRangos = x.IdTablaRangos,
                    DescTablaRangos = rang == null ? "" : rang.Descripcion,
                    IdPeriodicidad = x.IdPeriodicidad,
                    DescPeriodicidad = peri == null ? "" : peri.Descripcion,
                    ValPeriodicidad = x.ValPeriodicidad,
                    IdMovimientoTC = x.IdMovimientoTC,
                    DescMovimientoTC = mov == null ? "" : mov.Descripcion,
                    IdMovReversaTC = x.IdMovReversaTC,
                    DescMovReversaTC = reve == null ? "" : reve.Descripcion,
                    IdEstado = x.IdEstado,
                    DescEstado = est == null ? "" : est.Descripcion
                };
            return resultado.ToDataResult(request);
        }

        private ValidationResponse Validar(PocTipoCargo registro, bool inserta)
        {
            var validacion = new ValidationResponse();
            if (registro.Descripcion == "")
            {
                validacion.AddError("Err_0035");
            }
            if (!validacion.IsValid)
            {
                throw new CoreException("Datos faltantes", validation: validacion);
            }
            return validacion;
        }

        public IEnumerable<PocCatalogo> ListadoOrigenCargo()
        {
            var criteria = new Criteria<GL_Catalogo>();
            criteria.And(c => c.Lista == "LCONTEXTO");
            criteria.And(c => c.Referencia1.Contains("~AC~"));
            var parametros = _repCatalogo.List(criteria)
                .Select(item => new PocCatalogo
                {
                    Codigo = item.Codigo,
                    Descripcion = item.Descripcion,
                    IdCatalogo = item.IdCatalogo,
                    IdEstado = item.IdEstado,
                    Lista = item.Lista,
                    Referencia1 = item.Referencia1,
                    Referencia2 = item.Referencia2
                });
            return parametros.ProyectarComo<PocCatalogo>();
        }

        public IEnumerable<PocTipoCargo> ListadoTipoCargo()
        {
            var parametros = _repTipoCargo.Table.ToList();
            return parametros.ProyectarComo<PocTipoCargo>();
        }

        public PocTipoCargo ObtenerTipoCargo(PocTipoCargo filtro)
        {
            var criteria = new Criteria<CA_TipoCargo>();
            if (filtro != null)
            {
                if (!string.IsNullOrEmpty(filtro.Descripcion))
                {
                    criteria.And(m => m.Descripcion.Contains(filtro.Descripcion));
                }
                if (filtro.IdTipoCargo > 0)
                {
                    criteria.And(m => m.IdTipoCargo == filtro.IdTipoCargo);
                }
            }
            var resultado =
                from x in _repTipoCargo.SelectBy(criteria)
                join mone in _repMoneda.Table
                  on x.IdMoneda equals mone.IdMoneda
                join orig in _repCatalogo.List("LCONTEXTO")
                  on x.IdOrigenCargo equals orig.IdCatalogo
                join cobr in _repCatalogo.List("LTIPOCOBROCARGO")
                  on x.IdTipoCobro equals cobr.IdCatalogo
                join rang in _repRangosTabla.Table
                  on x.IdTablaRangos equals rang.IdTablaRangos
                join peri in _repCatalogo.List("LPERIODICIDAD")
                  on x.IdPeriodicidad equals peri.IdCatalogo
                join est in _repCatalogo.List("LACTINACTIVA")
                  on x.IdEstado equals est.IdCatalogo
                join mov in _repCatalogo.List("LTIPOMOVIMIENTOTC")
                  on x.IdMovimientoTC equals mov.IdCatalogo
                join reve in _repCatalogo.List("LTIPOMOVIMIENTOTC")
                   on x.IdMovReversaTC equals reve.IdCatalogo
                select new PocTipoCargo
                {
                    IdTipoCargo = x.IdTipoCargo,
                    Descripcion = x.Descripcion,
                    IdMoneda = x.IdMoneda,
                    DescMoneda = mone == null ? "" : mone.Descripcion,
                    IdOrigenCargo = x.IdOrigenCargo,
                    DescOrigenCargo = orig == null ? "" : orig.Descripcion,
                    IdTipoCobro = x.IdTipoCobro,
                    DescTipoCobro = cobr == null ? "" : cobr.Descripcion,
                    MonCargo = x.MonCargo,
                    IdTablaRangos = x.IdTablaRangos,
                    DescTablaRangos = rang == null ? "" : rang.Descripcion,
                    IdPeriodicidad = x.IdPeriodicidad,
                    DescPeriodicidad = peri == null ? "" : peri.Descripcion,
                    ValPeriodicidad = x.ValPeriodicidad,
                    IdMovimientoTC = x.IdMovimientoTC,
                    DescMovimientoTC = mov == null ? "" : mov.Descripcion,
                    IdMovReversaTC = x.IdMovReversaTC,
                    DescMovReversaTC = reve == null ? "" : reve.Descripcion,
                    IdEstado = x.IdEstado,
                    DescEstado = est == null ? "" : est.Descripcion
                };
            return resultado.FirstOrDefault();
        }
    }
}

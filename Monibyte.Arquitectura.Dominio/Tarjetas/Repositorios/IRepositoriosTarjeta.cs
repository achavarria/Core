﻿using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas.Repositorios
{

    public interface IRepositorioTarjeta : IRepositorioMnb<TC_Tarjeta>
    {
        IEnumerable<TC_Tarjeta> TarjetasPorCuenta(int idCuenta, bool soloActivas = false);
        IEnumerable<TC_Tarjeta> TarjetasPorCliente(int idCliente);
        void MigrarTarjeta(string numTcOrigen, string numTcDestino);
    }

   
    
}

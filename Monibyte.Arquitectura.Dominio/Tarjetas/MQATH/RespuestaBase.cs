﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monibyte.Arquitectura.Dominio.Tarjetas.MQATH
{
    public class RespuestaBase
    {
        public virtual string TipoMensaje { get; set; }
        public virtual string CodigoRespuesta { get; set; }
        public virtual string DescripcionCodRespuesta { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace Monibyte.Arquitectura.Dominio.Tarjetas
{
    public partial class TC_EstCuentaIndividual : EntidadBase
    {
        public int IdEstIndividual { get; set; }
        public int IdTarjeta { get; set; }
        public int IdCuenta { get; set; }
        public int IdEstado { get; set; }
        public string CorreoEstCuenta { get; set; }
        public string Detalle { get; set; }
        public string CopiaCorreoEstCuenta { get; set; }
        public System.DateTime FecCorte { get; set; }
        public System.DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
    }
}

﻿using Monibyte.Arquitectura.Adaptador.Nucleo;
using Monibyte.Arquitectura.Dominio;
using Monibyte.Arquitectura.Dominio.Documentacion;
using Monibyte.Arquitectura.Poco.Documentacion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Monibyte.Arquitectura.Aplicacion.Documentacion
{
    public class AppDocumentacion : AplicacionBase, IAppDocumentacion
    {
        private IRepositorioMnb<DC_Ayuda> _repDocumentacion;

        public AppDocumentacion(IRepositorioMnb<DC_Ayuda> repDocumentacion)
        {
            _repDocumentacion = repDocumentacion;
        }

        public List<PocDocumentacion> ObtenerAyuda(int idAccion, bool novedades = false)
        {
            var parrafos = _repDocumentacion.Table
                .Where(x => x.IdAccion == idAccion && (!novedades ||
                    DateTime.Now >= x.VigenciaDesde &&
                    DateTime.Now <= x.VigenciaHasta))
                .OrderBy(x => x.IdOrden);
            return parrafos.ProyectarComo<PocDocumentacion>();
        }
    }
}

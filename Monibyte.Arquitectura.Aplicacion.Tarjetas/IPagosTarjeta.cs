﻿using Monibyte.Arquitectura.Aplicacion.Nucleo;
using Monibyte.Arquitectura.Poco.Tarjetas;
using Monibyte.Arquitectura.Poco.TEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monibyte.Arquitectura.Aplicacion.Tarjetas
{
    public interface IPagosTarjeta : IAplBase
    {
        PocPagoTransaccion PagarTarjetaTEF(PocPagoTransaccion pocEntrada, bool EsPagoDirecto = true);
        PocPagoTransaccion PagarExtraIntra(PocPagoTransaccion pago);

        PocSalidaPagoTarjeta PagarExtraIntraDirigido(PocPagoTransaccion pago);
        PocSalidaPagoTarjeta ReversarExtraIntraDirigido(PocPagoTransaccion pago);
    }
}
